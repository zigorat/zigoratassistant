/*
*********************************************************************************
*         DisplaySystem.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: DisplaySystem handles all display targets within application       *
*            window. This system allocates a camera for each of displays.       *
*            Also allows for picture in picture outputs.                        *
*                                                                               *
*********************************************************************************
*/

/*! \file DisplaySystem.cpp
<pre>
<b>File:</b>          DisplaySystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      DisplaySystem handles all display targets within application
               window. This system allocates a camera for each of displays.
               Also allows for picture in picture outputs.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#include "DisplaySystem.h"
#include <GraphicSystem/GraphicSystem.h>
#include <PluginSystem/PluginSystem.h>


template<> Klaus::Graphics::DisplaySystem* Klaus::Singleton<Klaus::Graphics::DisplaySystem>::mInstance = 0;

namespace Klaus
{


namespace Graphics
{


/*****************************************************************************/
/**************************   Class DisplaySystem   **************************/
/*****************************************************************************/

/**
  This is the constructor of DisplaySystem. It initializes the main
  camera's instance and will set it's properties to default ones.
*/
DisplaySystem::DisplaySystem()
{
  mLog = Logger::getSingleton();
  mMaxZOrder = 0;
  mDeactivated = false;

  setupConfig("DisplaySystem");
  setRecipientName("DisplaySystem");

  // register for resize and close events
  EV_OPENNED = registerEvent("MainWindow\\Openned");
  EV_CLOSED = registerEvent("MainWindow\\Closed");
  EV_RESIZED = registerEvent("MainWindow\\Resized");
}

/**
  This is the destructor of DisplaySystem. Frees up the memories
*/
DisplaySystem::~DisplaySystem()
{
  removeAllDisplays();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
DisplaySystem * DisplaySystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new DisplaySystem;
    Logger::getSingleton()->log(1, "(DisplaySystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void DisplaySystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(DisplaySystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(DisplaySystem) removed singleton");
  }
}

/**
  This method sets up default congfiguration for this system
*/
void DisplaySystem::_loadDefaultConfig()
{
  mDisplays.clear();

  DisplayInfo inf;
  inf.mLeft = 0.0;
  inf.mTop = 0.0;
  inf.mLength = 1.0;
  inf.mWidth = 1.0;
  inf.mName = "MainDisplay";
  inf.mCamera = "FreeCamera";
  mDisplays.push_back(inf);
}

/**
  This method loads configuration from a binary stream.
  @param t The configuration type of the steam
  @return True if successfully read configuration, false otherwise
*/
bool DisplaySystem::_readConfig(const Config::ConfigType & t)
{
  try
  {
    mDisplays.clear();

    if(t == Config::BinaryConfig)
    {
      unsigned count = readUnsigned();
      DisplayInfo inf;

      for(unsigned i = 0; i < count; ++i)
      {
        inf.mName = readString();
        inf.mLeft = readDouble();
        inf.mTop = readDouble();
        inf.mLength = readDouble();
        inf.mWidth = readDouble();
        inf.mCamera = readString();
        mDisplays.push_back(inf);
      }
    }
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      for(size_t i = 1; i < expr.size(); ++i)
      {
        SToken t = expr[i];

        DisplayInfo inf;
        inf.mName = t[1].str();
        inf.mLeft = t[2];
        inf.mTop = t[3];
        inf.mLength = t[4];
        inf.mWidth = t[5];
        inf.mCamera = t[6].str();

        mDisplays.push_back(inf);
      }
    }
    else
    {
      mLog->log(18, "(DisplaySystem) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    Logger::getSingleton()->log(18, "(DisplaySystem) exception: %s", str);
    return false;
  }

  return setupDisplays();
}

/**
  This method writes the configuration of display system to the given output stream.
  @param type The configuration type to save the stream
*/
void DisplaySystem::_writeConfig(const Config::ConfigType & type)
{
  if(type == Config::BinaryConfig)
  {
    writeUnsigned(mDisplays.size());
    for(unsigned i = 0; i < mDisplays.size(); ++i)
    {
      writeString(mDisplays[i].mName);
      writeDouble(mDisplays[i].mLeft);
      writeDouble(mDisplays[i].mTop);
      writeDouble(mDisplays[i].mLength);
      writeDouble(mDisplays[i].mWidth);
      writeString(mDisplays[i].mCamera);
    }
  }
  else if(type == Config::SExpConfig)
  {
    for(size_t i = 0; i < mDisplays.size(); ++i)
      writeText(
                 "(addDisplay %s %2.2f %2.2f %2.2f %2.2f %s)",
                 mDisplays[i].mName.c_str(),
                 mDisplays[i].mLeft,
                 mDisplays[i].mTop,
                 mDisplays[i].mLength,
                 mDisplays[i].mWidth,
                 mDisplays[i].mCamera.c_str()
               );
  }
  else
    mLog->log(18, "(DisplaySystem) write: configuration type %d not supported", type);
}

/**
  This method adds a display window to application with the desired camera agent
  @param strName Name of the display. If a duplicate name exists, the display will not be built
  @param strCamera Name of the desired camera agent. If it was not found, NULL will be returned.
  @param dLeft Starting x coordinate of the display. It's value shall reside between [0..1)
  @param dTop Starting y coordinate of the display. It's value shall reside between [0..1)
  @param dLength Length of the display. It's value shall reside between [0..1]
  @param dWidth Width of the display. It's value shall reside between [0..1]
  @return Camera agent instance created for the display. If anything goes wrong NULL will be returned
*/
CameraPlugin * DisplaySystem::addDisplay(const std::string& strName,
                               const std::string& strCamera,
                               const double& dLeft,
                               const double& dTop,
                               const double& dLength,
                               const double& dWidth)
{
  // displays with duplicate window names can not co exist.
  for(size_t i = 0; i < mViewPortNames.size(); i++)
    if(mViewPortNames[i] == strName)
      return NULL;

  // Create camera plugin first
  PluginFactory * factory = PluginSystem::getSingleton()->findPlugin(strCamera, "CameraPlugin");
  if(!factory)
  {
    mLog->log(18, "(DisplaySystem) plugin not found: %s", strCamera.c_str());
    return NULL;
  }

  Plugin* raw_plugin = factory->createPluginInstance();
  if(!raw_plugin)
  {
    mLog->log(18, "(DisplaySystem) camera plugin factory returned no instance for %s", strCamera.c_str());
    return NULL;
  }

  CameraPlugin* plugin = dynamic_cast<CameraPlugin*>(raw_plugin);
  if(!plugin)
  {
    factory->freePluginInstance(raw_plugin);
    mLog->log(18, "(DisplaySystem) cast failed for camera plugin %s", strCamera.c_str());
    return NULL;
  }

  mPlugins.push_back(plugin);
  mPluginFactories.push_back(factory);
  Logger::getSingleton()->log(18, "(DisplaySystem) loaded plugin %s", strCamera.c_str());

  // create ogre camera
  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  Ogre::Camera * cam = mgr->createCamera(strName);
  mCameras.push_back(cam);

  // create ogre view port
  Ogre::RenderWindow * win = GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow();
  Ogre::Viewport* vp = win->addViewport(cam, mMaxZOrder++, (Ogre::Real)dLeft, (Ogre::Real)dTop, (Ogre::Real)dLength, (Ogre::Real)dWidth);
  mViewPorts.push_back(vp);
  mViewPortNames.push_back(strName);

  // Now activate the camera
  plugin->_setCameraInstance(cam);
  if(!plugin->activate())
  {
    mLog->log(18, "(DisplaySystem) camera plugin not activated");
    removeDisplay(strName);
    return NULL;
  }

  updateAspectRatios();
  return plugin;
}

/**
  This method sets up all displays in system's configuration.
  @return True if all displays were set up successfully, false otherwise
*/
bool DisplaySystem::setupDisplays()
{
  // check for configuration to be loaded
  if(mDisplays.empty())
  {
    mLog->log(18, "(DisplaySystem) no displays to setup");
    return false;
  }

  // check for GraphicSystem's complete intialization
  if(!GraphicSystem::getSingleton()->getSceneManager())
  {
    mLog->log(18, "(DisplaySystem) GraphicSystem not completely initialized. Postponing setup");
    return false;
  }

  // first remove all displays
  removeAllDisplays();

  // then add all displays one by one
  for(size_t i = 0; i < mDisplays.size(); i++)
    if(!addDisplay(mDisplays[i].mName,
                   mDisplays[i].mCamera,
                   mDisplays[i].mLeft,
                   mDisplays[i].mTop,
                   mDisplays[i].mLength,
                   mDisplays[i].mWidth))
      return false;

  return true;
}

/**
  This method removes the display with the given name. It also removes the camera agent for the display
  @param strName Name of the display to remove
  @return True if the display was found false otherwise
*/
bool DisplaySystem::removeDisplay(const std::string& strName)
{
  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  Ogre::RenderWindow * win = Graphics::GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow();

  for(size_t i = 0; i < mViewPortNames.size(); i++)
    if(mViewPortNames[i] == strName)
    {
      // Deactivate the plugin first
      mPlugins[i]->deactivate();

      // remove the view port
      mViewPortNames.erase(mViewPortNames.begin()+i);
      win->removeViewport(mViewPorts[i]->getZOrder());
      mViewPorts.erase(mViewPorts.begin() + i);

      // remove the ogre camera
      mgr->destroyCamera(mCameras[i]);
      mCameras.erase(mCameras.begin() + i);

      // remove the camera plugin
      mPluginFactories[i]->freePluginInstance(mPlugins[i]);
      mLog->log(18, "(DisplaySystem) unloaded plugin %s", mPluginFactories[i]->getName());
      mPlugins.erase(mPlugins.begin() + i);
      mPluginFactories.erase(mPluginFactories.begin() + i);

      return true;
    }

  return false;
}

/**
  This method removes all the displays within system. This method is usually called in destructor.
  @note: This method shall always be called prior to SceneSystem::freeSingleton, since scene system
  removes all cameras in the entire scene graph.
*/
void DisplaySystem::removeAllDisplays()
{
  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  Ogre::RenderWindow * win = Graphics::GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow();

  for(size_t i = 0; i < mViewPortNames.size(); i++)
  {
    // Deactivate the plugin first
    mPlugins[i]->deactivate();
    mPluginFactories[i]->freePluginInstance(mPlugins[i]);

    // remove the ogre camera
    mViewPorts[i]->setCamera(NULL);
    mgr->destroyCamera(mCameras[i]);

    // remove the view port. this also deletes it
    win->removeViewport(mViewPorts[i]->getZOrder());

    // log it
    mLog->log(18, "(DisplaySystem) unloaded plugin %s", mPluginFactories[i]->getName());
  }

  mPlugins.clear();
  mPluginFactories.clear();
  mViewPortNames.clear();
  mViewPorts.clear();
}

/**
 * This method calls upon all camera agents to update them between frames.
 */
void DisplaySystem::updateCameras()
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    mPlugins[i]->update();
}

/**
  This method updates all camera's aspect ratios. Aspect ratio is the ratio
  of length of the display to width of the display. This makes sure the final
  render on each display is not stretched. This method is also called in
  FrameManager::windowResized
*/
void DisplaySystem::updateAspectRatios()
{
  Graphics::GraphicConfig * config = Graphics::GraphicConfig::getSingleton();
  Graphics::RendererBase * renderer = config->getActiveRenderer();
  if(!renderer)
  {
    mLog->log(18, "(DisplaySystem) Renderer not initialized, skipping aspect ratio update");
    return;
  }

  bool bFullScreen = config->getOption("Full Screen") == "Yes";
  int max_width, max_height;
  renderer->getMaxResolution(max_width, max_height);
  double dOriginalAspectRatio = (double)max_width / max_height;

  for(size_t i = 0; i < mViewPorts.size(); i++)
  {
    mCameras[i]->setAutoAspectRatio(false);
    if(bFullScreen)
      mCameras[i]->setAspectRatio((Ogre::Real)dOriginalAspectRatio);
    else
      mCameras[i]->setAspectRatio((Ogre::Real)(1.0 * mViewPorts[i]->getActualWidth() / mViewPorts[i]->getActualHeight()));
  }
}

/**
  This method returns the camera agent plugin for the given display name
  @param strDisplay Name of the display to return it's camera agent
  @return If found, instance of the camera agent. Otherwise NULL
*/
CameraPlugin* DisplaySystem::getCameraPlugin(const std::string & strDisplay)
{
  for(size_t i = 0; i < mViewPortNames.size(); i++)
    if(strDisplay == mViewPortNames[i])
      return mPlugins[i];

  return NULL;
}

/**
  This method changes the active camera agent for a display. This method
  is very cautios, so that upon any error, the previous camera agent will
  be restored.
  @param strDisplay Name of the display to change it's camera agent
  @param strCamera Name of the new camera agent
  @return If operation succeeded, the newly activated camera agent will
  be return. Otherwise NULL will be returned.
*/
CameraPlugin* DisplaySystem::changeDisplayCamera(const std::string& strDisplay, const std::string& strCamera)
{
  int index = -1;
  for(size_t i = 0; i < mViewPortNames.size(); i++)
    if(mViewPortNames[i] == strDisplay)
    {
      index = (int)i;
      break;
    }

  if(index == -1)
  {
    mLog->log(18, "(DisplaySystem) no such display: %s", strDisplay.c_str());
    return NULL;
  }

  // now that the display index has been located, time to change it's camera.
  // create the plugin first
  PluginFactory * factory = PluginSystem::getSingleton()->findPlugin(strCamera, "CameraPlugin");
  if(!factory)
  {
    mLog->log(18, "(DisplaySystem) camera plugin not found: %s", strCamera.c_str());
    return NULL;
  }

  Plugin* raw_plugin = factory->createPluginInstance();
  if(!raw_plugin)
  {
    mLog->log(18, "(DisplaySystem) camera plugin factory returned no instance");
    return NULL;
  }

  CameraPlugin* plugin = dynamic_cast<CameraPlugin*>(raw_plugin);
  if(!plugin)
  {
    factory->freePluginInstance(raw_plugin);
    mLog->log(18, "(DisplaySystem) cast failed for camera plugin %s", strCamera.c_str());
    return false;
  }

  plugin->_setCameraInstance(mCameras[index]);
  if(!plugin->activate())
  {
    mLog->log(18, "(DisplaySystem) camera plugin refused activation: %s", strCamera.c_str());
    factory->freePluginInstance(plugin);

    mLog->log(18, "(DisplaySystem) activating previous camera agent");
    mPlugins[index]->activate();
    return NULL;
  }

  // camera plugin has been created with no errors
  // forcefully remove the previous plugin
  mPlugins[index]->_setCameraInstance(NULL);
  mPlugins[index]->deactivate();
  mPluginFactories[index]->freePluginInstance(mPlugins[index]);
  mPlugins[index] = plugin;
  mPluginFactories[index] = factory;
  Logger::getSingleton()->log(6, "(DisplaySystem) loaded plugin %s for display %s", strCamera.c_str(), strDisplay.c_str());
  return plugin;
}

/**
  This method analyzes sent events to this system and processes them.
  @param ev The event to process
*/
void DisplaySystem::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_CLOSED)
    removeAllDisplays();
  else if(ev == EV_RESIZED)
    updateAspectRatios();
  else if(ev == EV_OPENNED)
    setupDisplays();
  else
    mLog->log(18, "(DisplaySystem) recieved unknown event %s",
                Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}



}; // end namespace Graphics


}; // end namespace Klaus
