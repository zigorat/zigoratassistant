/*
*********************************************************************************
*           CameraPlugin.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Camera plugin base class                                           *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin.h
<pre>
<b>File:</b>          CameraPlugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Key bindings translator and handler
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CAMERA_PLUGIN
#define CAMERA_PLUGIN

#include <PluginSystem/PluginBase.h> // needed for Plugin
#include <OgreCamera.h>              // needed for Ogre::Camera
#include <OgreSceneNode.h>           // needed for Ogre::SceneNode
#include <libZigorat/Logger.h>       // needed for Logger


namespace Klaus
{

  namespace Graphics
  {

    /*! CameraPlugin is the plugin base for camera plugins. It contains method
        declerations for which the camera plugins shall implement.
    */
    class _DLLExport CameraPlugin: public Plugin
    {
      protected:
        Ogre::Camera  * mCamera;         /*!< Camera instance */
        Logger        * mLog;            /*!< Logger instance */

        /**
          Resets all properties of the camera to their default values.
        */
        void resetCamera()
        {
          if(!mCamera)
          {
            mLog->log(19, "(CameraPlugin) camera instance not set");
            return;
          }

          mCamera->setAutoAspectRatio(false);
          mCamera->setAutoTracking(false);
          mCamera->setFixedYawAxis(false);
          mCamera->setLodBias(1.0f);
          mCamera->setOrientation(Ogre::Quaternion::IDENTITY);
          mCamera->setPolygonMode(Ogre::PM_SOLID);
          mCamera->setPosition(0, 0, 0);
          mCamera->setUseRenderingDistance(true);
          mCamera->setFarClipDistance(200.0f);
          mCamera->setNearClipDistance(0.2f);
          mCamera->setFocalLength(1.0f);
          mCamera->setProjectionType(Ogre::PT_PERSPECTIVE);
          mCamera->setCastShadows(true);
          mCamera->setDebugDisplayEnabled(false);
          mCamera->setRenderingDistance(0.0f);

          Ogre::SceneNode * node = dynamic_cast<Ogre::SceneNode *>(mCamera->getParentNode());
          if(node)
            node->detachObject(mCamera);
        }

      public:
        /**
          Constructor of CameraPlugin. Initializes logger and and ogre's camera instance
        */
        CameraPlugin()
        {
          mCamera = NULL;
          mLog = Logger::getSingleton();
        }

        /**
          Destructor of CameraPlugin. Does nothing.
        */
        virtual ~ CameraPlugin()
        {
        }

        /**
          Internal usage. Sets the camera instance before activating the camera
          @param cam The ogre camera instance
        */
        void _setCameraInstance( Ogre::Camera * cam )
        {
          mCamera = cam;
          resetCamera();
        }

        /**
          This method shall activate the camera.
        */
        virtual bool activate() = 0;

        /**
          This method deactivates the camera.
        */
        virtual void deactivate() = 0;

        /**
          Updates the camera between every cycle
        */
        virtual void update() = 0;
    };

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // CAMERA_PLUGIN
