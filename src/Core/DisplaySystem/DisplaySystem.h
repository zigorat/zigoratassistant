/*
*********************************************************************************
*          DisplaySystem.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: DisplaySystem handles all display targets within application       *
*            window. This system allocates a camera for each of displays.       *
*            Allows for picture in picture outputs.                             *
*                                                                               *
*********************************************************************************
*/

/*! \file DisplaySystem.h
<pre>
<b>File:</b>          DisplaySystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      DisplaySystem handles all display targets within application
               window. This system allocates a camera for each of displays.
               Allows for picture in picture outputs.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#ifndef DISPLAY_SYSTEM
#define DISPLAY_SYSTEM

#include <OgreViewport.h>               // needed for Ogre::ViewPort
#include <OgreCamera.h>                 // needed for Ogre::Camera
#include <EventSystem/EventRecipient.h> // needed for Klaus::Event::EventRecipient
#include <ConfigSystem/Configurable.h>  // needed for Klaus::Config::Configurable
#include <libZigorat/Parse.h>           // needed for StringArray
#include "CameraPlugin.h"               // needed for Klaus::Graphics::CameraPlugin


namespace Klaus
{


  namespace Graphics
  {


    /*! DisplaySystem handles all display targets within application window. This
        system allocates a camera agent for each display. Also allows for picture
        in picture effects.
        Camera agents are implemented as external plugins bringing the ability to
        switch agents on real time.
    */
    class _DLLExportControl DisplaySystem: public Singleton<DisplaySystem>,
                                          public Events::EventRecipient,
                                          public Config::Configurable
    {
      private:
        /*! PluginList is a dynamic array of CameraPlugins */
        typedef std::vector<CameraPlugin*> PluginList;

        /*! DisplayList is a dynamic array of Ogre viewports */
        typedef std::vector<Ogre::Viewport*> DisplayList;

        /*! CameraList is a dynamic array of Ogre cameras */
        typedef std::vector<Ogre::Camera*> CameraList;

        /*! DisplayInfo contains information on displays. It's used to save
            and load configurations from binary streams.*/
        struct DisplayInfo
        {
          double      mLeft;   /*!< Left of the display relative to the window   */
          double      mTop;    /*!< Top of the display relative to the window    */
          double      mLength; /*!< Length of the display relative to the window */
          double      mWidth;  /*!< Width of the display relative to the window  */
          std::string mName;   /*!< Name of the display                          */
          std::string mCamera; /*!< Camera plugin to use for the display         */

          /*! List is a dynamic array of display informations */
          typedef std::vector<DisplayInfo> Array;
        };


        EventID EV_OPENNED;  /*!< Open event ID */
        EventID EV_RESIZED;  /*!< Resize event ID */
        EventID EV_CLOSED;   /*!< Close event ID */

        PluginList               mPlugins;            /*!< All camera plugins */
        PluginFactory::List      mPluginFactories;    /*!< All plugin factories */
        DisplayList              mViewPorts;          /*!< All viewports */
        StringArray              mViewPortNames;      /*!< Names of all view ports */
        CameraList               mCameras;            /*!< All camera instances */
        DisplayInfo::Array       mDisplays;           /*!< All camera names to load */
        int                      mMaxZOrder;          /*!< Current Maximum z order */
        Logger                 * mLog;                /*!< Logger instance */
        bool                     mDeactivated;        /*!< Whether system is active */

        // configurations
        virtual void             _loadDefaultConfig   (                                );
        virtual bool             _readConfig          ( const Config::ConfigType & t   );
        virtual void             _writeConfig         ( const Config::ConfigType & t   );

                                DisplaySystem        (                                );
                              ~ DisplaySystem        (                                );
      public:
        // Singleton operations
        static DisplaySystem   * getSingleton         (                                );
        static void              freeSingleton        (                                );

        CameraPlugin           * addDisplay           ( const std::string & strName,
                                                        const std::string & strCamera,
                                                        const double & dLeft,
                                                        const double & dTop,
                                                        const double & dLength,
                                                        const double & dWidth          );
        bool                     setupDisplays        (                                );

        bool                     removeDisplay        ( const std::string & strName    );
        void                     removeAllDisplays    (                                );

        void                     updateCameras        (                                );
        void                     updateAspectRatios   (                                );

        CameraPlugin           * getCameraPlugin      ( const std::string & strDisplay );
        CameraPlugin           * changeDisplayCamera  ( const std::string & strDisplay,
                                                        const std::string & strCamera  );

        // events
        virtual void             processEvent         ( const Events::Event & ev       );
    };


  }; // end namespace Graphics


}; // end namespace Klaus


#endif // DISPLAY_SYSTEM
