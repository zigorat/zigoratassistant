/*
*********************************************************************************
*        WorldModelUpdate.cpp : Robocup 3D Soccer Simulation Team Zigorat       *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the agents memory updater of outside world                 *
*                                                                               *
*********************************************************************************
*/

/*! \file WorldModelUpdate.cpp
<pre>
<b>File:</b>          WorldModelUpdate.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the agents memory updater of outside world
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#include "WorldModel.h"


/**
  This method parses a time message. The time message contains simulator
  time (and possibly the simulation step)
  ex: (time (now 2.42) (step 0.02))
  @param token The token containing the time message
  @return Number of errors in parsing the message
*/
int Klaus::WorldModel::parseTime(SToken token)
{
  int iError = 0;
  double dTime = -1;
  double dStep = -1;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "now")
        dTime = token[i][1].number();
      else if(token[i][0] == "step")
        dStep = token[i][1].number();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in time: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in time: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  // set the current simulator time
  if(dTime == -1)
  {
    mLog->log(35, "(WorldModel) no time percept in time message: '%s'", token.str().c_str());
    ++iError;
    return iError;
  }

  // set the correct time step if specified
  if(dStep == -1)
    dStep = dTime - mGameInfo.mSimulatorTime;

  // adjust current cycle
  if(dTime > mGameInfo.mSimulatorTime)
    ++mGameInfo.mCurrentCycle;

  // set the time related values
  mGameInfo.mSimulatorTime = dTime;
  mGameInfo.mSimulatorStep = dStep;
  mGameInfo.mTimeLastMsg = dTime;

  return iError;
}

/**
  This method parses a  simspark message. This message contains current play
  mode and match time of the game. The first game state message also contains
  the team side and uniform number of the agent
  ex: (GS (unum 6) (team right) (t 0.00) (pm BeforeKickOff))
  ex: (GS (t 5.4) (pm play_on))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseGameState(SToken token)
{
  int iError = 0;
  double dTime = -1;
  int unum = -1;
  SideType side = UnknownSide;
  PlayMode pm = UnknownPlayMode;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "unum")
        unum = token[i][1].getAsInt();
      else if(token[i][0] == "team")
        side = SoccerTypes::getSideFromStr(token[i][1].str());
      else if(token[i][0] == "pm")
        pm = SoccerTypes::getPlayModeFormStr(token[i][1].str());
      else if(token[i][0] == "t")
        dTime = token[i][1].number();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in game_state: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in game_state: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  // set the current simulator time
  if(dTime == -1 || pm == UnknownPlayMode)
  {
    mLog->log(35, "(WorldModel) no time percept or play mode in game_state message: '%s'", token.str().c_str());
    ++iError;
    return iError;
  }

  // set the time related values
  mGameInfo.mGameTime = dTime;
  mGameInfo.mPlayMode = pm;

  // set our side
  if(side != UnknownSide)
    mGameInfo.mTeamSide = side;

  // set uniform number and install agent in teammates
  if(unum != -1)
  {
    mAgent->setPlayerNumber(unum);
    mTeammates[unum] = mAgent;
  }

  return iError;
}

/**
  This method parses a game state simspark message. This message contains current
  play mode and match time of the game. The first game state message also contains
  the team side and uniform number of the agent
  ex: (GS (unum 6) (team right) (t 0.00) (pm BeforeKickOff))
  ex: (GS (t 5.4) (pm play_on))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseGyro(SToken token)
{
  VecPosition rt = VecPosition::Unknown;
  std::string name;
  int iError = 0;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "rt" && token[i].size() == 4)
      {
        rt[0] = token[i][1].number();
        rt[1] = token[i][2].number();
        rt[2] = token[i][3].number();
      }
      else if(token[i][0] == "n")
        name = token[i][1].str();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in gyro: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in gyro: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  if(rt == VecPosition::Unknown)
  {
    mLog->log(35, "(WorldModel) no rate given for gyro '%s'", name.c_str());
    iError++;
  }
  else
  {
    for(size_t i = 0; i < mGyros.size(); ++i)
      if(mGyros[i]->getName() == name)
      {
        mGyros[i]->percept(rt * mGameInfo.mSimulatorStep);
        return iError;
      }

    mLog->log(35, "(WorldModel) gyro does not exist: '%s'", name.c_str());
    iError++;
  }

  return iError;
}

/**
  This method parses a see simspark message. This message contains visual information of
  all objects in the agent's view cone relative to the agent with possible vision noise.
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseSee(SToken token)
{
  int iErrors = 0;

  for(size_t i = 1; i < token.size(); ++i)
    if(token[i].isNormal() && token[i].empty() == false)
    {
      if(token[i][0] == "B") // ball
        iErrors += parseBallVision(token[i]);
      else if(token[i][0] == "P") // players
        iErrors += parsePlayerVision(token[i]);
      else if(SoccerTypes::getObjectFromStr(token[i][0]) != UnknownObject) // flags
        iErrors += parseFlagVision(token[i]);
      else
      {
        mLog->log(35, "(WorldModel) unknown visual node '%s'", token[i].str().c_str());
        ++iErrors;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown visual node in '%s'", token[i].str().c_str());
      ++iErrors;
    }

  mGameInfo.mTimeLastSeeMsg = mGameInfo.mSimulatorTime;
  return iErrors;
}

/**
  This method parses a hear simspark message. This message contains say messages
  sent by other players. This message contains direction from which the message
  is sent, the time message was sent and the said message.
  ex: (hear 120.32 -40.21 hello)
  ex: (hear 240 self hello)
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseHear(SToken token)
{
  if(token.size() < 4)
  {
    mLog->log(35, "(WorldModel) hear tokens less than expected: '%s'", token.str().c_str());
    return 1;
  }

  // no need to take any action when talker is the agent himself
  if(token[1] == "self")
    return 0;

  HearData data;
  data.mTime = token[1].number();
  data.mSpeakAngle = token[2].number();
  data.mMsg = token[3].str();
  mHeardMessages.push_back(data);

  mGameInfo.mTimeLastHearMsg = mGameInfo.mSimulatorTime;
  return 0;
}

/**
  This method parses a hinge joint perceptor simspark message. This message contains current
  angle  (and possibly angular speed) in degrees of the corresponding hinge joint.
  ex: (HJ (n llj3) (ax 12.3) (rt 2.2))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseHingeJoint(SToken token)
{
  Degree ax = Degree::Unknown, rt = Degree::Unknown;
  std::string name;
  int iError = 0;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "ax" && token[i].size() == 2)
        ax = token[i][1].number();
      else if(token[i][0] == "rt" && token[i].size() == 2)
        rt = token[i][1].number();
      else if(token[i][0] == "n")
        name = token[i][1].str();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in hinge joint: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in hinge joint: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  if(ax == UnknownDoubleValue)
  {
    mLog->log(35, "(WorldModel) no angle value given for hinge joint '%s' : '%s'", name.c_str(), token.str().c_str());
    iError++;
  }
  else
  {
    RSG2::Joint::Map::iterator it = mJoints.find(name);
    if(it == mJoints.end())
    {
      mLog->log(35, "(WorldModel) hinge joint does not exist: '%s'", name.c_str());
      iError++;
    }
    else
    {
      if(rt == UnknownDoubleValue)
        rt = (it->second->getAngle() - ax) / mGameInfo.mSimulatorStep;

      it->second->getPerceptor()->overrideAngle(ax);
      it->second->getPerceptor()->overrideRate(rt);
    }
  }

  return iError;
}

/**
  This method parses a universal joint perceptor simspark message. This message contains
  current angle (and possibly angular speed) in degrees of the corresponding universal
  joint.
  ex: (UJ (n llj3) (ax1 12.3) (rt1 2.2) (ax2 22.1) (rt2 -20))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseUniversalJoint(SToken token)
{
  Degree ax1 = Degree::Unknown, rt1 = Degree::Unknown;
  Degree ax2 = Degree::Unknown, rt2 = Degree::Unknown;
  std::string name;
  int iError = 0;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "ax1" && token[i].size() == 2)
        ax1 = token[i][1].number();
      else if(token[i][0] == "rt1" && token[i].size() == 2)
        rt1 = token[i][1].number();
      else if(token[i][0] == "ax2" && token[i].size() == 2)
        ax2 = token[i][1].number();
      else if(token[i][0] == "rt2" && token[i].size() == 2)
        rt2 = token[i][1].number();
      else if(token[i][0] == "n")
        name = token[i][1].str();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in universal joint: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in universal joint: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  if(ax1 == UnknownDoubleValue || ax2 == UnknownDoubleValue)
  {
    mLog->log(35, "(WorldModel) no angle value given for universal joint '%s' : '%s'", name.c_str(), token.str().c_str());
    iError++;
  }
  else
  {
    RSG2::Joint::Map::iterator it = mJoints.find(name);
    if(it == mJoints.end())
    {
      mLog->log(35, "(WorldModel) universal joint does not exist: '%s'", name.c_str());
      iError++;
    }
    else
    {
      if(rt1 == UnknownDoubleValue)
        rt1 = (it->second->getAngle(0) - ax1) / mGameInfo.mSimulatorStep;
      if(rt2 == UnknownDoubleValue)
        rt2 = (it->second->getAngle(1) - ax2) / mGameInfo.mSimulatorStep;

      it->second->getPerceptor()->overrideAngle(ax1, ax2);
      it->second->getPerceptor()->overrideRate(rt1, rt2);
    }
  }

  return iError;
}

/**
  This method parses a frp simspark message. This message contains the
  sensor name, sensed force center and magnitude.
  ex: (FRP (n lf) (c 0.02 0.01 0.02) (f 2 3 4))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseFRP(SToken token)
{
  VecPosition f = VecPosition::Unknown, c = VecPosition::Unknown;
  std::string name;
  int iError = 0;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "f" && token[i].size() == 4)
      {
        f[0] = token[i][1].number();
        f[1] = token[i][2].number();
        f[2] = token[i][3].number();
      }
      else if(token[i][0] == "c" && token[i].size() == 4)
      {
        c[0] = token[i][1].number();
        c[1] = token[i][2].number();
        c[2] = token[i][3].number();
      }
      else if(token[i][0] == "n")
        name = token[i][1].str();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in FRP: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in FRP: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  if(f == VecPosition::Unknown || c == VecPosition::Unknown)
  {
    mLog->log(35, "(WorldModel) no force or center is given for FRP '%s' : '%s'", name.c_str(), token.str().c_str());
    iError++;
  }
  else
  {
    for(size_t i = 0; i < mFRPs.size(); ++i)
      if(mFRPs[i]->getName() == name)
      {
        mFRPs[i]->setCenter(c);
        mFRPs[i]->setForce(f);
        return iError;
      }

    mLog->log(35, "(WorldModel) FRP does not exist: '%s'", name.c_str());
    iError++;
  }

  return iError;
}

/**
  This method parses an acceleromter simspark message. This message contains
  the sensor name and sensed acceleration vector relative to the accelerometer
  node's global transformation. Consideres gravity acceleration too
  ex: (ACC (n torso) (a 0.00 0.00 9.88))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseAccelerometer(SToken token)
{
  VecPosition rt = VecPosition::Unknown;
  std::string name;
  int iError = 0;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() == true && token[i].size() > 1)
    {
      if(token[i][0] == "a" && token[i].size() == 4)
      {
        rt[0] = token[i][1].number();
        rt[1] = token[i][2].number();
        rt[2] = token[i][3].number();
      }
      else if(token[i][0] == "n")
        name = token[i][1].str();
      else
      {
        mLog->log(35, "(WorldModel) unknown percept in accelerometer: '%s'", token[i].str().c_str());
        ++iError;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown percept in accelerometer: '%s'", token[i].str().c_str());
      ++iError;
    }
  }

  if(rt == VecPosition::Unknown)
  {
    mLog->log(35, "(WorldModel) no rate given for accelerometer '%s'", name.c_str());
    iError++;
  }
  else
  {
    for(size_t i = 0; i < mAccs.size(); ++i)
      if(mAccs[i]->getName() == name)
      {
        mAccs[i]->setAcceleration(Quaternion(90, VecPosition(0, 0, 1)) * rt);
        return iError;
      }

    mLog->log(35, "(WorldModel) accelerometer does not exist: '%s'", name.c_str());
    iError++;
  }

  return iError;
}

/**
  This method parses a ball visual information message. The message contains
  relative position of the ball to the agent in polar coordinates.
  ex: (B (pol 2.2 45 10))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseBallVision(SToken token)
{
  int iErrors = 0;
  for(size_t i = 1; i < token.size(); ++i)
    if(token[i].isNormal() && token[i].size() == 4 && token[i][0] == "pol")
    {
      VecPosition p(token[i][1].number(), token[i][2].number(), token[i][3].number());
      mBall->setRelativePosition(p, mGameInfo.mSimulatorTime);
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown vision predicate for ball: '%s'", token[i].str().c_str());
      ++iErrors;
    }

  return iErrors;
}

/**
  This method parses a flag visual information message. The message contains
  the flag name and its relative position to the agent in polar coordinates.
  ex: (F1R (pol 2.2 45 10))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parseFlagVision(SToken token)
{
  int iErrors = 0;
  VecPosition p = VecPosition::Unknown;
  Object * o = getObjectInstance(SoccerTypes::getObjectFromStr(token[0]));
  if(!o)
  {
    mLog->log(35, "(WorldModel) could not acquire flag instance for '%s'", token[0].str().c_str());
    return 1;
  }

  for(size_t i = 1; i < token.size(); ++i)
    if(token[i].isNormal() && token[i].size() == 4 && token[i][0] == "pol")
      p.setVector(token[i][1].number(), token[i][2].number(), token[i][3].number());
    else
    {
      mLog->log(35, "(WorldModel) unknown vision predicate for flag: '%s'", token[i].str().c_str());
      ++iErrors;
    }

  if(p == VecPosition::Unknown)
  {
    mLog->log(35, "(WorldModel) no update for flag in '%s'", token.str().c_str());
    return ++iErrors;
  }
  else
    o->setRelativePosition(p, mGameInfo.mSimulatorTime);

  return iErrors;
}

/**
  This method parses a player node visual information message. The message contains
  player's object state information (arms and legs) to find out its pose.
  ex: (larm (pol 2.2 45 10))
  @param token The token containing the message
  @param lst List to insert the visual node's position and name
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parsePlayerVisualNode(SToken token, NamedPos & lst)
{
  if(token[0].isAtom() == false)
  {
    mLog->log(35, "(WorldModel) error parsing player visual node: '%s'", token.str().c_str());
    return 1;
  }

  SToken t = token[1];
  if(t.size() != 4 || t[0] != "pol" || t[1].isNormal() || t[2].isNormal() || t[3].isNormal())
    lst[token[0]].setVector(t[1].number(), t[2].number(), t[3].number());

  return 0;
}

/**
  This method parses a player visual information message. The message contains
  number of the player, its team name, its relative position and it's body
  parts to the agent in polar coordinates.
  ex: (B (pol 2.2 45 10) (larm (pol 2 2 2)))
  @param token The token containing the message
  @return Number of errors in parse. 0 means no errors
*/
int Klaus::WorldModel::parsePlayerVision(SToken token)
{
  int iErrors = 0;
  int iNum = UnknownIntValue;
  std::string team_name;
  VecPosition p = VecPosition::Unknown;
  NamedPos parts;

  for(size_t i = 1; i < token.size(); ++i)
  {
    if(token[i].isNormal() && token[i].empty() == false)
    {
      if(token[i][0] == "id" && token[i].size() == 2)
        iNum = token[i][1].getAsInt();
      else if(token[i][0] == "team" && token[i].size() == 2)
        team_name = token[i][1].str();
      else if(token[i][0] == "pol" && token[i].size() == 4)
        p.setVector(token[i][1].number(), token[i][2].number(), token[i][3].number());
      else if(token[i].size() == 2)
        iErrors += parsePlayerVisualNode(token[i], parts);
      else
      {
        mLog->log(35, "(WorldModel) unknown node in player visual msg: '%s'", token[i].str().c_str());
        ++iErrors;
      }
    }
    else
    {
      mLog->log(35, "(WorldModel) unknown player vision token: '%s'", token[i].str().c_str());
      ++iErrors;
    }
  }

  // sanity check for player number
  if(iNum < 1 || iNum > 11)
  {
    if(iNum == UnknownIntValue)
      mLog->log(35, "(WorldModel) no number given in player vision: '%s'", token.str().c_str());
    else
      mLog->log(35, "(WorldModel) number out of range in player visual '%s'", token.str().c_str());

    return ++iErrors;
  }

  PlayerObject * player = NULL;
  if(team_name == mTeamName)
  {
    PlayerObject::Map::iterator it = mTeammates.find(iNum);
    if(it == mTeammates.end())
    {
      player = new PlayerObject;
      player->setType(SoccerTypes::getTeammateTypeFromNumber(iNum));
      player->setPlayerNumber(iNum);
      player->setTeamSide(SideLeft);
      mTeammates[iNum] = player;
    }
    else
      player = it->second;
  }
  else
  {
    mOpponentTeamName = team_name;
    PlayerObject::Map::iterator it = mOpponents.find(iNum);
    if(it == mOpponents.end())
    {
      player = new PlayerObject();
      player->setType(SoccerTypes::getOpponentTypeFromNumber(iNum));
      player->setPlayerNumber(iNum);
      player->setTeamSide(SideRight);
      mOpponents[iNum] = player;
    }
    else
      player = it->second;
  }

  // set its visual position
  player->setRelativePosition(p, mGameInfo.mGameTime);

  // now set its seen body parts
  for(NamedPos::iterator it = parts.begin(); it != parts.end(); ++it)
     player->setBodyPart(it->first, it->second);

  // return number of errors that occured
  return iErrors;
}

/**
  This method localizes the agent in field with the percepted information from simspark.
  When control reaches this function, all relative positions of objects are updated via
  see message. This method first calculates the agent orientation, then calculates the
  agent position.
  @return Shows whether localization was successful.
*/
bool Klaus::WorldModel::localize()
{
  if(getTimeLastSeeMessage() != mGameInfo.mSimulatorTime)
  {
    mLog->log(42, "(Localize) no see message in %2.2f", mGameInfo.mSimulatorTime);
    return true;
  }

  ObjectArray flags;
  if(getObjectsInSet(flags, Flags, 1.0) < 3)
  {
    mLog->log(42, "(Localize) less than three flags in %2.2f", mGameInfo.mSimulatorTime);
    return false;
  }

  // calculate orientation
  // Pr (Rotated flags matrix)
  VecPosition f1 = getRelativePosition(flags[0]);
  VecPosition f2 = getRelativePosition(flags[1]);
  VecPosition f3 = getRelativePosition(flags[2]);

  // derive three vectors from them and normalize them
  f2 = f2 - f1;
  f2.normalize();
  f3 = f3 - f1;
  f3.normalize();
  f1 = f2.cross(f3);
  f1.normalize();

  // derive rotated matrix
  Matrix3 matRotated = Matrix3::createFrom3Points(f2,f3, f2.cross(f3));
  if(fabs(matRotated.getDeterminant()) < SMALL_EPSILON)
  {
    mLog->log(42, "(Localize) rotated matrix has zero determinant (%2.2f)", mGameInfo.mSimulatorTime);
    return false;
  }

  // derive unrotated matrix
  f1 = getGlobalPosition(flags[0]);
  f2 = getGlobalPosition(flags[1]);
  f3 = getGlobalPosition(flags[2]);
  f2 = f2 - f1;
  f2.normalize();
  f3 = f3 - f1;
  f3.normalize();
  f1 = f2.cross(f3);
  f1.normalize();
  Matrix3 matDefault = Matrix3::createFrom3Points(f2,f3, f2.cross(f3));

  // matRotated = matRotation * matDefault => matRotation = matRotated x matDefault^-1
  Matrix3 matRotation = matRotated / matDefault;

  /// The above operations derive the agent's rotation matrix which upon that a
  /// quaternion can be built very easily and no singularity or other types of
  /// Problems, like gimbal lock, etc.
  mAgent->setOrientation(matRotation);

  // Then calculate position
  // flag_relative = q * (flag_global - agent_global)
  VecPosition p = getRelativePosition(flags[0]);
  p = mAgent->getOrientation() * p;
  p = getGlobalPosition(flags[0]) - p;

  mAgent->setGlobalPosition(p, getTime());
  mLog->log(42, "(Localize) calculated agent position %s in %2.2f", p.str().c_str(), mGameInfo.mSimulatorTime);
  return 0;
}

/**
  This method updates after simspark message. The update process includes several
  phases:
    - Update kick off side
    - Localization
    - Updaing global coordinates from relative coordinates
    - Finding out talked players in last cycle
*/
int Klaus::WorldModel::update()
{
  int iErrors = 0;

  // update kick off team side
  static PlayMode lastMode = UnknownPlayMode;
  PlayMode pm = mGameInfo.mPlayMode;

  if((lastMode == BeforeKickOff) && (pm == KickOffLeft || pm == KickOffRight))
  {
    // update for the next before kick of state
    if(pm == KickOffLeft)
      mGameInfo.mSideKickOff = SideRight;
    else
      mGameInfo.mSideKickOff = SideLeft;
  }
  lastMode = pm;

  // forward kinematics
  mAgentGraph->updateForwardKinematics();

  // localize
  if(!localize())
    ++iErrors;

  // update balance state
  VecPosition acc = mAccs[0]->getAcceleration();
  if(mBalanceState != Normal)
  {
    if(acc[2] > 9.2)
      mBalanceState = Normal;
  }
  else
  {
    if(acc[0] > 9.2)
      mBalanceState = FallenFront;
    else if(acc[0] < -9.2)
      mBalanceState = FallenBack;
    else if(acc[1] > 9.2)
      mBalanceState = FallenRight;
    else if(acc[1] < -9.2)
      mBalanceState = FallenLeft;
  }

  // updateglobalfromrelative

  // update hear messages data

  return iErrors;
}

/**
  Logs world model. Is used for ZigoratAssistant logging and visual debugging mode.
*/
void Klaus::WorldModel::logWorldModel()
{
}

/**
  This method parses a given simspark message and updates the world model.
  @param strMsg The simspark message to parse and update from
*/
void Klaus::WorldModel::parseSimSparkMessage(const std::string & strMsg)
{
  try
  {
    int iErrorCount = 0;

    SExpression msg(strMsg);
    for(size_t i = 0; i < msg.size(); ++i)
      if(msg[i].isNormal() && msg[i].size() > 1)
      {
        if(msg[i][0] == "time")
          iErrorCount += parseTime(msg[i]);
        else if(msg[i][0] == "GS")
          iErrorCount += parseGameState(msg[i]);
        else if(msg[i][0] == "GYR")
          iErrorCount += parseGyro(msg[i]);
        else if(msg[i][0] == "See")
          iErrorCount += parseSee(msg[i]);
        else if(msg[i][0] == "Hear")
          iErrorCount += parseHear(msg[i]);
        else if(msg[i][0] == "HJ")
          iErrorCount += parseHingeJoint(msg[i]);
        else if(msg[i][0] == "UJ")
          iErrorCount += parseUniversalJoint(msg[i]);
        else if(msg[i][0] == "FRP")
          iErrorCount += parseFRP(msg[i]);
        else if(msg[i][0] == "ACC")
          iErrorCount += parseAccelerometer(msg[i]);
        else
        {
          mLog->log(35, "(WorldModel) unknown perceptor information '%s'", msg[i].str().c_str());
          ++iErrorCount;
        }
      }

    // set last message time
    mGameInfo.mTimeLastMsg = mGameInfo.mSimulatorTime;

    // Update the world model
    iErrorCount += update();

    if(iErrorCount != 0)
      mLog->log(35, "(WorldModel) %d erros occured when updating WorldModel", iErrorCount);

    // parse finished. now update
    logWorldModel();
  }
  catch(SLangException e)
  {
    mLog->log(35, "(WorldModel) can not parse simspark message");
    mLog->log(35, "%s", e.str());
  }
  catch(...)
  {
    mLog->log(35, "(WorldModel) unknown error occured in parsing simspark message\n'%s'", strMsg.c_str());
  }
}




/*********************************************************************************/
/**************************** Testing purposes ***********************************/
/*********************************************************************************/

// #define TEST_WMUPDATE  /*!< define this to test worldmodel updating */

#ifdef TEST_WMUPDATE
int main()
{
  string strMsg;

  strMsg += "(GYR (n torso) (rt -11.15 -0.84 0.03))";
  strMsg += "(HJ (n hj1) (ax -0.00))";
  strMsg += "(HJ (n hj2) (ax -0.00))";
  strMsg += "(time (now 1.78))";
  strMsg += "(GS (unum 6) (team right) (t 4.57) (pm BeforeKickOff))";
  strMsg += "(See ";
  strMsg += " (G1L (pol 3.66 -102.77 11.12))";
  strMsg += " (G2L (pol 5.02 -99.28 8.02))";
  strMsg += " (G2R (pol 12.24 -23.67 3.47))";
  strMsg += " (G1R (pol 11.76 -17.30 3.69))";
  strMsg += " (F1L (pol 0.88 -165.90 19.55))";
  strMsg += " (F2L (pol 8.25 -95.58 2.07))";
  strMsg += " (F1R (pol 11.20 -1.15 1.67))";
  strMsg += " (P (team BATS) (id 2) (pol 24.00 0.00 -0.09))";
  strMsg += " (F2R (pol 13.92 -35.86 1.72))";
  strMsg += " (B (pol 6.69 -39.04 3.05))";
  strMsg += ")";
  strMsg += "(HJ (n raj1) (ax 0.10))";
  strMsg += "(HJ (n raj2) (ax 0.09))";
  strMsg += "(HJ (n raj3) (ax 0.04))";
  strMsg += "(HJ (n raj4) (ax 0.03))";
  strMsg += "(HJ (n laj1) (ax -0.09))";
  strMsg += "(HJ (n laj2) (ax 0.12))";
  strMsg += "(HJ (n laj3) (ax 0.02))";
  strMsg += "(HJ (n laj4) (ax 0.01))";
  strMsg += "(HJ (n rlj1) (ax 0.11))";
  strMsg += "(HJ (n rlj2) (ax 0.15))";
  strMsg += "(HJ (n rlj3) (ax -0.06))";
  strMsg += "(HJ (n rlj4) (ax 0.00))";
  strMsg += "(HJ (n rlj5) (ax 0.03))";
  strMsg += "(FRP (n rf) (c -0.02 -0.02 -0.57) (f -1.65 12.37 3807.08))";
  strMsg += "(HJ (n rlj6) (ax 0.04))";
  strMsg += "(HJ (n llj1) (ax -0.08))";
  strMsg += "(HJ (n llj2) (ax 0.15))";
  strMsg += "(HJ (n llj3) (ax -0.03))";
  strMsg += "(HJ (n llj4) (ax -0.00))";
  strMsg += "(HJ (n llj5) (ax -0.02))";
  strMsg += "(FRP (n lf) (c 0.02 -0.02 -0.57) (f 0.99 11.75 3828.97))";
  strMsg += "(HJ (n llj6) (ax 0.02))";
  strMsg.insert(strMsg.begin(),'(');
  strMsg.append(1,')');

  WorldModel& wm = *WorldModel::getSingleton();
  wm.setAgentModel( MODEL_NAO );

  if( !wm.updateFromServerMsg( strMsg ) )
  {
    cout << "WorldModel update failed" << endl;
    return 1;
  }

  cout << "Simulator Time: " << wm.getTime() << ", GameTime: " << wm.getTime();
  cout << ", CurrentStep: " << wm.getSimulatorStep() << endl;

  Joint j;
  for(int t = 0; t < MAX_JOINTS; t++)
    cout << wm.getJoint((JointT)t) << endl;

  cout << "Field Length = " << wm.getFieldLength() << " | Field Width = " << wm.getFieldWidth() << endl;
  cout << "Agent gyroscope:" << wm.getGyroscope( "torso" ) << endl;
  cout << "Agent number: " << wm.getAgentNumber() << ", Team Side: " << SoccerTypes::getSideStr( wm.getSide() ) << endl;
  cout << "Agent Position: " << wm.getAgentPosition() << endl;
  cout << "Agent Direction: " << wm.getAgentDirection() << endl;
  cout << "Ball Position: " << wm.getBallPosition() << endl;
  cout << "Current PlayMode: " << SoccerTypes::getPlayModeStr( wm.getPlayMode() ) << endl;

  return 0;
}
#endif // TEST_WMUPDATE

