/*
*********************************************************************************
*          BasicAgent.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definition BasicAgent which contains      *
*            several routines needed for thinkin' module                        *
*                                                                               *
*********************************************************************************
*/

/** \file BasicAgent.cpp
<pre>
<b>File:</b>          BasicAgent.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definition BasicAgent which contains
               several routines needed for thinkin' module
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#include "BasicAgent.h"
#include "PIDController.h"

// needed to use scene graphs
using namespace Klaus::RSG2;


/**
  This is the constructor of BasicAgent, initilizes self
  variables, sends identification signal to the server
  and starts the agent
*/
Klaus::BasicAgent::BasicAgent()
{
  mTerminate       = false;
  mRequestedNumber = 0;
  mConnection      = SimSpark::getSingleton();
  mWM              = WorldModel::getSingleton();
  mQueue           = ActHandler::getSingleton();
  mSettings        = AgentSettings::getSingleton();
  mLog             = Logger::getSingleton();
  mFormation       = FormationSystem::getSingleton();
}

/**
  Destructor of the BasicAgent claass
*/
Klaus::BasicAgent::~BasicAgent()
{
}

/**
  This method beams agent to his current strategic position
  @param angBody Body angle when beamed to the strategic position
*/
void Klaus::BasicAgent::beamToStrategicPosition(Degree angBody)
{
  VecPosition posBeam = mWM->getStrategicPosition();
  beam(posBeam[0], posBeam[1], angBody);
}

/**
  This method sends init command to simspark. This command initializes the
  agent in simspark. Initialization is done with specifing agent team name
  and uniform number. This is different with scene command in which that
  command specifies the agent's body model, and this will set soccer
  information of the agent.
*/
void Klaus::BasicAgent::sendPlayerInformation()
{
  mQueue->sendCommand(SoccerCommand(mRequestedNumber, mWM->getTeamName()));
  mLog->log(37, "(BasicAgent) requested number %d for team %s", mRequestedNumber, mWM->getTeamName().c_str());
}

/**
  This method just says something to indicate that agent
  has finished execution in the normal way.
*/
void Klaus::BasicAgent::sayGoodbye()
{
  if(mSettings->isTraining())
    return;

  if(mWM->getAgentNumber() == 1)
    std::cout << "goalie out." << std::endl;
  else
    std::cout << "#" << mWM->getAgentNumber() << " out." << std::endl;

  mLog->log(37, "#%d out", mWM->getAgentNumber());
}

/**
  This method sends a beam command to server: A beam command directly moves
  the robot to the given position so that it's torso will be onn the specified
  location. This is not allowed in play on game play mode.
  @param dx X-Coordinate of desired move location
  @param dy Y-Coordinate of desired move location
  @param dAng Agent direction when has moved to the deisred location
*/
void Klaus::BasicAgent::beam(const double & dx, const double & dy, const Degree & dAng)
{
  if(mWM->canBeam())
  {
    mQueue->addCommand(SoccerCommand(VecPosition(dx, dy, dAng.getDegree())));
    AgentObject::getSingleton()->setGlobalPosition(VecPosition(dx, dy, 0.57), mWM->getTime() + mWM->getTimeStep());
    mLog->log(37, "(BasicAgent) beaming to [%2.2f, %2.2f] at %2.2f degrees", dx, dy, dAng.getDegree());

    // reset gyro
    RSG2::Node * n = AgentObject::getSingleton()->getSceneGraph()->getNodeByType("GyroRatePerceptor", "torso");
    RSG2::GyroRatePerceptor * g = dynamic_cast<RSG2::GyroRatePerceptor*>(n);
    if(g)
      g->reset();
  }
  else
    mLog->log(37, "(BasicAgent) can not beam now");
}

/**
  This method sends a string to be broadcasted to all teammates
  @param str String containing the talk message
*/
void Klaus::BasicAgent::say(const std::string & str)
{
  mQueue->addCommand(SoccerCommand(str));
  mLog->log(37, "(BasicAgent) %s", str.c_str());
}

/**
  This function directly adjustes a hinge joint angular speeds by the values given.
  The speeds here are considered to be degree/second so they are implicitely converted
  to radians/seconds. So be carefull to not send speeds here with the dimentions of
  radian per second.
  @param strName The joint's perceptor name to identify the joint
  @param dSpeed1 Angular speed of the first motor of the joint
*/
void Klaus::BasicAgent::adjustJoint(const std::string & strName, const Radian & dSpeed1)
{
  mQueue->addCommand(SoccerCommand(strName, dSpeed1.getRadian()));
}

/**
  This function directly adjustes a universal joint's angular speeds by the values
  given. The speeds here are considered to be degree/second so they are implicitely
  converted to radians/seconds. So be carefull to not send speeds here with the
  dimentions of radian per second.
  @param strName The joint's perceptor name to identify the joint
  @param dSpeed1 Angular speed of the first motor of the joint
  @param dSpeed2 Angular speed of the second motor of the joint
*/
void Klaus::BasicAgent::adjustJoint(const std::string& strName, const Radian & dSpeed1, const Radian& dSpeed2)
{
  mQueue->addCommand(SoccerCommand(strName, dSpeed1.getRadian(), dSpeed2.getRadian()));
}

/**
  This method changes the head joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::head_up(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("hj2");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("he2", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the side head joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::head_side(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("hj1");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("he1", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left arm joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_rotate_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("laj3");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lae3", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right arm joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_rotate_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("raj3");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rae3", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left arm joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("laj1");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lae1", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right arm joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("raj1");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rae1", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left arm side joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_side_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("laj2");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lae2", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right arm side joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::arm_side_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("raj2");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rae2", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left elbow joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::elbow_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("laj4");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lae4", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right elbow joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::elbow_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("raj4");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rae4", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj3");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle3", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj3");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle3", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the side left hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_side_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj2");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle2", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the side right hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_side_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj2");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle2", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the side left hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_rotate_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj1");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle1", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the side right hip joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::hip_rotate_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj1");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle1", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left knee joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::knee_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj4");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle4", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right knee joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::knee_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj4");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle4", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left ankle joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::ankle_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj5");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle5", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right ankle joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::ankle_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj5");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle5", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the left side ankle joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::ankle_side_l(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("llj6");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("lle6", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method changes the right side ankle joint to go to the given angle.
  @param ang Desired angle for the joint
*/
void Klaus::BasicAgent::ankle_side_r(Degree ang)
{
  static PIDController pid;
  const Joint * joint = mWM->getJoint("rlj6");
  ang.restrict(joint->getMinAngle(), joint->getMaxAngle());
  adjustJoint("rle6", pid.calculateNeededMV(joint->getAngle().getDegree(), ang.getDegree(), mWM->getTimeStep()));
}

/**
  This method turns neck to the specified position
  @param pos Position to look at
*/
void Klaus::BasicAgent::turnNeckToPoint(const VecPosition & pos)
{
  VecPosition x(1,0,0);
  x = mWM->getAgentOrientation() * x;
  VecPosition p = pos - mWM->getAgentPosition();
  head_side(p.getDirection() - x.getDirection());
  head_up(p.getPhi() - x.getPhi());
}

/**
  This method turns neck to the given object
  @param obj The object to look at
*/
void Klaus::BasicAgent::turnNeckToObject(const ObjectType & obj)
{
  turnNeckToPoint(mWM->getGlobalPosition(obj));
}

/**
  This method searches for the ball
*/
void Klaus::BasicAgent::searchBall()
{
  // get angles and speeds
  const Joint * j = mWM->getJoint("hj1");
  Degree min_side, max_side;
  min_side = j->getMinAngle();
  max_side = j->getMaxAngle();
  Degree ang_side = j->getAngle();
  Degree spd_side = j->getSpeed();

  const Joint *j2 = mWM->getJoint("hj2");
  Degree min = j2->getMinAngle();
  Degree max = j2->getMaxAngle();
  Degree ang = j->getAngle();
  Degree spd = j->getSpeed();

  mLog->log(37, "(BasicAgent) search ball: [up:%2.2f, side:%2.2f]", ang.getDegree(), ang_side.getDegree());

  // calculate destination angles
  if(ang_side > max_side - 3)
    ang_side = -180;
  else if(ang_side < min_side + 3)
    ang_side = 180;
  else
    ang_side = sign(spd_side.getDegree())* 180;

  if(ang > max - 3)
    ang = -180;
  else if(ang < min + 3)
    ang = 180;
  else
    ang = sign(spd.getDegree())* 180;

  // execute them
  head_up(ang);
  head_side(ang_side);
}

/**
  This method inits agents information by loading agents scene graph
  into WorldModel and then it will send scene command to the simulator
  @param name Name of the team
  @param iNum Requested number of the player
  @return True on successful initialization, false otherwise
*/
bool Klaus::BasicAgent::initialize(const std::string & name, const int & iNum)
{
  // Load agent information like joints and sizes into WorldModel.
  AgentModelType model = mSettings->getAgentModel();
  if(!AgentObject::getSingleton()->setAgentModel(model))
    return false;

  // This may take a while, so do it before sending init command to simulator
  if(!mWM->initialize(name))
    return false;

  // set the requested number
  mRequestedNumber = iNum;

  // Send scene command to init the agent
  // use the scene effector to build the agent and beam to strategic position
  mQueue->sendCommand(SoccerCommand(model));
  mLog->log(37, "(BasicAgent) sent scene command");
  return true;
}
