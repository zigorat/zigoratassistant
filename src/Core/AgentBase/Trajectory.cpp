/*
*********************************************************************************
*          Trajectory.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/07/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class declarations for Trajectories.                               *
*                                                                               *
*********************************************************************************
*/

/*! \file Trajectory.cpp
<pre>
<b>File:</b>          Trajectory.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/07/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class declarations for Trajectories.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/07/2011       Mahdi           Initial version created
</pre>
*/

#include "Trajectory.h"


/***************************************************************************/
/**************************  Class Trajectory  *****************************/
/***************************************************************************/

/**
  Construtor of the Trajectory. Initializes default values
*/
Klaus::Trajectory::Trajectory()
{
  mRange = 0.0;
}

/**
  Constructor of Trajectory. Creates the trajectory with the given values
  @param p Starting position on of the projectile on the trajectroy
  @param q Orientation of the trajectory
  @param range Maximum range of the trajectory
*/
Klaus::Trajectory::Trajectory(const Klaus::VecPosition& p, const Klaus::Quaternion& q, const double& range)
{
  mStartPos = p;
  mOrientation = q;
  mRange = range;
}

/**
  This method sets the parameters of the trajectory
  @param p Starting position of the trajectory
  @param q Orientation of the trajectory
  @param range Maximum range of the trajectory
*/
void Klaus::Trajectory::setTrajectory(const Klaus::VecPosition& p, const Klaus::Quaternion& q, const double& range)
{
  mStartPos = p;
  mOrientation = q;
  mRange = range;

  postUpdateInternal();
}


/**
  This method returns the range of the trajectory
  @return Range of the trajectory
*/
double Klaus::Trajectory::getRange() const
{
  return mRange;
}

/**
  This method sets the range of the trajectory
  @param range The range of the trajectory
*/
void Klaus::Trajectory::setRange(const double& range)
{
  mRange = range;
  postUpdateInternal();
}

/**
  This method returns the orientation of the trajectory
  @return Orientation of the trajectory
*/
Klaus::Quaternion Klaus::Trajectory::getOrientation() const
{
  return mOrientation;
}

/**
  This method sets the orientation of the trajectory
  @param q The orientation of the trajectory
*/
void Klaus::Trajectory::setOrientation(const Quaternion & q)
{
  mOrientation = q;
  postUpdateInternal();
}

/**
  This method returns the starting position of the projectile
  @return Starting position of the projectile
*/
Klaus::VecPosition Klaus::Trajectory::getStartingPos() const
{
  return mStartPos;
}

/**
  This method sets the starting position of the projectile
  @param pos The starting position of the projectile
*/
void Klaus::Trajectory::setStartingPos(const Klaus::VecPosition& pos)
{
  mStartPos = pos;
  postUpdateInternal();
}




/******************************************************************/
/*******************  Class PeriodicTrajectory  *******************/
/******************************************************************/

/**
  Constructor of PeriodicTrajectory. Initializes period and delta phase
  @param period Time length of a full period movment
  @param delta_phi Delay in starting the phase
*/
Klaus::PeriodicTrajectory::PeriodicTrajectory(const double& period, const Degree& delta_phi)
{
  mPeriod = period;
  mPhaseDelay = delta_phi;
}

/**
  This method calculates the current phase of the period using the given time
  @param time Current time of the projectile
  @return Current phase of the movement
*/
Klaus::Degree Klaus::PeriodicTrajectory::getPhase(double time)
{
  // deduce phase delay
  double phase = time / mPeriod * 360.0 - mPhaseDelay.getDegree();
  while(phase < 0)
    phase += 360;
  while(phase >= 360)
    phase -= 360;

  return phase;
}

/**
  This method calculates number of cycles which have passed in the given time for projetile
  @param time Current time of projectile
  @return Number of cycles pass till the given time
*/
int Klaus::PeriodicTrajectory::getNumberOfCycles(double time)
{
  int iCycles = 0;

  // deduce phase
  double phase = time / mPeriod * 360.0 - mPhaseDelay.getDegree();

  // use phase to get the current cycle
  while(phase >= 360.0)
  {
    phase -= 360.0;
    ++iCycles;
  }

  while(phase < 0)
  {
    phase += 360.0;
    --iCycles;
  }

  return iCycles;
}

/**
  This method returns the period of the trajectory
  @return Period of the trajectory
*/
double Klaus::PeriodicTrajectory::getPeriod() const
{
  return mPeriod;
}

/**
  This method sets the period of the trajectory
  @param period Period of the trajectory
  @param delta_phi Delay in starting the trajectory phase
*/
void Klaus::PeriodicTrajectory::setPeriod(const double& period, const Degree & delta_phi)
{
  mPeriod = period;
  mPhaseDelay = delta_phi;

  postUpdateInternal();
}

/**
  This method returns the phase delay of the trajectory
  @return Trajectory's phase delay
*/
Klaus::Degree Klaus::PeriodicTrajectory::getPhaseDelay() const
{
  return mPhaseDelay;
}

/**
  This method sets the phase delay in trajectory's movement
  @param phase_delay The delay in trajectory's phase calculation
*/
void Klaus::PeriodicTrajectory::setPhaseDelay(const Degree& phase_delay)
{
  mPhaseDelay = phase_delay;
  postUpdateInternal();
}
