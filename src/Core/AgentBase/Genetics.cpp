/*
*********************************************************************************
*            Genetics.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 01/30/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class definitions for classes and storages used in evolutionary    *
*            programming techniques                                             *
*                                                                               *
*********************************************************************************
*/

/** \file Genetics.cpp
<pre>
<b>File:</b>          Genetics.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/30/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class definitions for classes and storages used in evolutionary
                  programming techniques
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/30/2009       Mahdi           Modifies version of initial source (2/08) created
</pre>
*/

#include "Genetics.h"
#include <libZigorat/Geometry.h>  // needed for random
#include <libZigorat/Logger.h>    // needed for Logger


/** Used to indicate an unknown or not initialzied value */
#define UnknownDoubleValue -1000

#include <cstdlib>

// A simple utility method
int compare_organisms( const void * o1, const void * o2 );


namespace Klaus
{



/**********************************************************************************/
/************************************  Gene  **************************************/
/**********************************************************************************/

/**
  Default constructor of the Gene class. Empty, does nothing
*/
Gene::Gene()
{
}

/**
  Default destructor of the Gene class. Empty, does nothing
*/
Gene::~Gene()
{
}

/**
  This method forks and returns an exact copy of the gene
  @return An exact copy of the gene
*/
Gene * Gene::fork()
{
  Gene * result = newInstance();
  *result = this;

  return result;
}

/**
  This method is used to exchange genes from two chromosomes
  @param first Fist gene of exchange
  @param second Second gene of exchange
*/
void Gene::exchangeGenes( Gene* first, Gene * second )
{
  Gene * temp = newInstance();
  *temp = first;
  *first = second;
  *second = temp;
  delete temp;
}





/**********************************************************************************/
/*********************************  Chromosome  ***********************************/
/**********************************************************************************/

/**
  Default constructor of the Chromosome class. Empty, does nothing
*/
Chromosome::Chromosome()
{
}

/**
  Default destructor of the Chromosome class. Cleans up genes list
*/
Chromosome::~Chromosome()
{
  cleanUp();
}

/**
  This method clean up the genes in the chromosome from memory and list
*/
void Chromosome::cleanUp()
{
  for(size_t i = 0; i < mGenes.size(); i++)
    delete mGenes[i];

  mGenes.clear();
}

/**
  This method exposes all the genes to the threat  of being mutated. The
  threats are eg solar high energy rays, radioactive material exposures,
  free radicals of chemical reaction byproducts and ...
  @param dMutationRate Rate at which mutation may occur
  @return Number of mutated genes
*/
int Chromosome::mutate( double dMutationRate )
{
  int iMutatations = 0;

  for(size_t i = 0; i < mGenes.size(); i++)
    if(drand() < dMutationRate)
    {
      mGenes[i]->mutate();
      iMutatations++;
    }

  return iMutatations;
}

/**
  This static method, exchanges a number of chromosome's genes with
  another same chromosome genes of another organism
  @param first The first chromosome
  @param second The second Chromosome
*/
void Chromosome::xOver( Chromosome * first, Chromosome * second )
{
  // get the xover locus
  // This is a two point crossover
  unsigned iMinIndex = (int)(drand() * first->getGenesCount());
  unsigned iMaxIndex = (int)(drand() * (first->getGenesCount() - iMinIndex)) + iMinIndex;

  for(unsigned i = iMinIndex; i <= iMaxIndex; i++)
    first->getGene(i)->exchangeGenes(first->getGene(i), second->getGene(i));
}

/**
  This method creates and returns an exact copy of the current chromosome
  @return Exact copy of the chromosome
*/
Chromosome * Chromosome::fork()
{
  Chromosome * result = newInstance();

  for(size_t i = 0; i < mGenes.size(); i++)
    result->addGene(mGenes[i]->fork());

  return result;
}

/**
  This method returns number of genes residing in the current chromosome
  @return Number of genes in the current chromosome
*/
size_t Chromosome::getGenesCount()
{
  return mGenes.size();
}

/**
  This method returns pointer to the gene specified by its index
  @param iIndex Index of the desired gene
  @return A pointer to the desired gene
*/
Gene * Chromosome::getGene(size_t iIndex)
{
  if(iIndex < mGenes.size())
    return mGenes[iIndex];
  else
    return NULL;
}

/**
  This method adds a gene to the chromosome
  @param gene Gene to be added to the chromosome
*/
void Chromosome::addGene(Gene * gene)
{
  mGenes.push_back(gene);
}





/**********************************************************************************/
/***********************************  Organism  ***********************************/
/**********************************************************************************/

/**
  Default constructor of the Organism class. Sets the fitness to an unspecified one
*/
Organism::Organism()
{
  mFitness = UnknownDoubleValue;
  mMaxChromosomes = -1;
}

/**
  Default destructor of the Organism class. Cleans up the chromosomes in the organism
*/
Organism::~Organism()
{
  cleanUp();
}

/**
  This method clean up the chromosomes in this organism
*/
void Organism::cleanUp()
{
  for(size_t i = 0; i < mChromosomes.size(); i++)
    delete(mChromosomes[i]);

  mChromosomes.clear();
}

/**
  This method exposes all the chromosomes to the threat of being mutated.
  The threats are solar high energy rays, radioactive material exposures,
  free radicals of chemical reaction byproducts and ...
  @param dMutationRate Rate at which mutation may occur
  @return Number of mutated genes
*/
int Organism::mutate( double dMutationRate )
{
  int iMutatedGenes = 0;

  for(size_t i = 0; i < mChromosomes.size(); i++)
    iMutatedGenes += mChromosomes[i]->mutate(dMutationRate);

  return iMutatedGenes;
}

/**
  It could be said that the main distinguishing feature of a GA is the use of crossover. Single−point crossover
  is the simplest form: a single crossover position is chosen at random and the parts of two parents after the
  crossover position are exchanged to form two offspring. The idea here is, of course, to recombine building
  blocks (schemas) on different strings. Single−point crossover has some shortcomings, though. For one thing,
  it cannot combine all possible schemas. For example, it cannot in general combine instances of 11*****1 and
  ****11** to form an instance of 11**11*1.
  To reduce positional bias and this "endpoint" effect, many GA practitioners use two−point crossover, in which
  two positions are chosen at random and the segments between them are exchanged. Two−point crossover is
  less likely to disrupt schemas with large defining lengths and can combine more schemas than single−point
  crossover. In addition, the segments that are exchanged do not necessarily contain the endpoints of the strings.
  @param first The first organism for xover
  @param second The second organism for xover
  @param dXOverRate Probabilty at which xover happens
  @return Number of xovers happend
*/
int Organism::xOver( Organism * first, Organism * second, double dXOverRate )
{
  int iResult = 0;
  if(first->getChromosomeCount() != second->getChromosomeCount())
  {
    Logger::getSingleton()->log(9, "(Organism) xOver: chromosome count do not match");
    return 0;
  }

  const size_t size = first->getChromosomeCount();
  for(size_t i = 0; i < size; i++)
  {
    // XOver the genes
    if(drand() < dXOverRate)
    {
      Chromosome * c1 = first->getChromosome(i);
      Chromosome * c2 = second->getChromosome(i);

      size_t length = c1->getGenesCount();
      size_t start  = (size_t)(drand() * length);
      size_t end    = (size_t)(drand() * (length - start - 1)) + start;
      Gene *g1, *g2;

      for(unsigned j = start; j <= end; j++)
      {
        g1 = c1->getGene(j);
        g2 = c2->getGene(j);
        g1->exchangeGenes(g1, g2);
      }

      iResult++;
    }
  }

  return iResult;
}

/**
  This method returns the fitness an organism in it's nature
  @return The fitness associated with the organism
*/
double Organism::getFitness() const
{
  return mFitness;
}

/**
  This method sets the fitness of organism to the specified derived one
  @param dFitness The fitness of the organism in action
*/
void Organism::setFitness( double dFitness )
{
  mFitness = dFitness;
}

/**
  This method returns the number of chromosomes in the organism
  @return Organism's chromosome count
*/
size_t Organism::getChromosomeCount()
{
  return mChromosomes.size();
}

/**
  This method returns a pointer to the specified chromosome
  @param iIndex Index of the chromosome
  @return Pointer to the desired chromosome
*/
Chromosome * Organism::getChromosome( size_t iIndex )
{
  if(iIndex < mChromosomes.size())
    return mChromosomes[iIndex];
  else
    return NULL;
}

/**
  This method adds a chromosome to the organism
  @param chromosome The chromosome to be added to organism
*/
void Organism::addChromosome( Chromosome * chromosome )
{
  mChromosomes.push_back(chromosome);
}

/**
  This method forks the organism and creates and returns an exact copy
  @return An exact copy of the organism
*/
Organism * Organism::fork()
{
  Organism * result = newInstance();

  for(size_t i = 0; i < mChromosomes.size(); i++)
    result->addChromosome(mChromosomes[i]->fork());

  result->mFitness = mFitness;
  return result;
}




/**********************************************************************************/
/*********************************  Generation  ***********************************/
/**********************************************************************************/

/**
  This is constructor of Generation class. Currently does nothing
*/
Generation::Generation()
{
}

/**
  This destructor cleans the organisms from the memory upon exit.
*/
Generation::~Generation()
{
  cleanUp();
}

/**
  This method forks the current generation and returns an exact copy
  @return Exact copy of this generation
*/
Generation * Generation::fork()
{
  Generation * result = newInstance();

  for(size_t i = 0; i < mOrganisms.size(); i++)
    result->addOrganism(mOrganisms[i]->fork());

  return result;
}

/**
  This method creates a generation forked from the current generation, sorted
  on rank if needed and have their expected fitness values set up to simplify
  use by roulette wheel and SUS selections
  @param dMinFitness Minimum fitness any organism can have
  @param exp_val[] Expected values of each individual
  @param max_val Sum of all the expected values
  @return A ready generation for fitness proportionate selection
*/
Generation * Generation::createFPGeneration(double dMinFitness, double exp_val[], double & max_val )
{
  Generation * result = fork();
  size_t iSize = result->getOrganismCount();
  if(iSize == 0)
  {
    delete result;
    return NULL;
  }

  // Calculate expected values
  max_val = 0;
  for(size_t i = 0; i < iSize; i++)
  {
    exp_val[i] = result->getOrganism(i)->getFitness() - dMinFitness;
    max_val += exp_val[i];
  }

  return result;
}

/**
  This method implements one types of selection operators in genetic algorithms, named
  'Roulette Wheel'. In this method each individual is assigned a slice of a circular
  "roulette wheel," the size of the slice being proportional to the individual's fitness.
  The wheel is spun N times, where N is thenumber of individuals in the population. On
  each spin, the individual under the wheel's marker is selected to be in the pool of
  parents for the next generation.
  @param dMinFitness Minimum fitness organisms can achieve.
  @return A set of organisms for the next generation
*/
Generation * Generation::selectByRouletteWheel(double dMinFitness)
{
  const size_t size = mOrganisms.size();
  double *exp_val, max_val;
  exp_val = new double [size];

  Generation * temp = createFPGeneration(dMinFitness, exp_val, max_val);

  if(!temp)
  {
    delete [] exp_val;
    return NULL;
  }

  Generation * result = newInstance();
  double rand_val, sum;

  for(size_t i = 0; i < size; i++)
  {
    rand_val = max_val * drand();
    sum = 0;

    for(size_t j = 0; j < size; j++)
    {
      sum += exp_val[j];
      if(sum > rand_val)
      {
        result->addOrganism(temp->getOrganism(j)->fork());
        break;
      }
    }
  }

  delete temp;
  delete [] exp_val;
  return result;
}

/**
  James Baker (1987) proposed a different sampling method—"stochastic universal sampling"
  (SUS)—to minimize this "spread" (the range of possible actual values, given an expected
  value). Rather than spin the roulette wheel N times to select N parents, SUS spins  the
  wheel once—but with N equally spaced pointers, which are used to selected the N parents.
  @param dMinFitness Minimum fitness organisms can achieve.
  @return A set of selected organisms for a new generation
*/
Generation * Generation::selectBySUS(double dMinFitness)
{
  const size_t size = mOrganisms.size();
  double * exp_val, max_val;
  exp_val = new double [size];

  Generation * temp = createFPGeneration(dMinFitness, exp_val, max_val);

  if(!temp)
  {
    delete [] exp_val;
    return NULL;
  }

  Generation * result = newInstance();
  double slice, sum;

  for(size_t i = 0; i < size; i++)
  {
    slice = 1.0 * i / size;
    sum = 0;

    for(size_t j = 0; j < size; j++)
    {
      sum += exp_val[j];
      if((sum/max_val) > slice)
      {
        result->addOrganism(temp->getOrganism(j)->fork());
        break;
      }
    }
  }

  delete temp;
  delete [] exp_val;
  return result;
}

/**
  The fitness−proportionate methods described above require two passes through the population at each
  generation: one pass to compute the mean fitness (and, for sigma scaling, the standard deviation)
  and one pass to compute the expected value of each individual. Rank scaling requires sorting the
  entire population by rank—a potentially time−consuming procedure. Tournament selection is similar
  to rank selection in terms of selection pressure, but it is computationally more efficient and more
  amenable to parallel implementation.
  @param dFitterWinProbabilty The K paramater
  @return A selected set of organisms to create new offsprings
*/
Generation * Generation::selectByTournamenting(double dFitterWinProbabilty)
{
  if(mOrganisms.empty())
    return NULL;

  const size_t size = mOrganisms.size();
  Organism *o1, *o2;
  Generation *result = newInstance();

  for(size_t i = 0; i < size; i++)
  {
    o1 = mOrganisms[Random(size)];

    // To select two different individuals
    do
    {
      o2 = mOrganisms[Random(size)];
    }
    while( o2 == o1 && size != 1 );

    if(drand() < dFitterWinProbabilty)
      result->addOrganism(o1->fork());
    else
      result->addOrganism(o2->fork());
  }

  return result;
}

/**
  This method applies XOvers on a new generation
  @param dXOverWillingness Willingness of offsprings to crossover
  @param dXOverRate Rate at which cross over happens
  @return Number of XOvers happenned
*/
int Generation::applyXOvers( double dXOverWillingness, double dXOverRate )
{
  int iResult = 0;
  Organism * o;

  // A generation with only one organism can not have any crossovers.
  if(mOrganisms.size() == 1)
    return 0;

  for(size_t i = 0; i < mOrganisms.size(); i++)
  {
    if(drand() < dXOverWillingness)
    {
      // To prevent crossover with itself
      do
      {
        o = mOrganisms[Random(mOrganisms.size())];
      }while(o != mOrganisms[i]);

      iResult += Organism::xOver(mOrganisms[i], o, dXOverRate);
    }
  }

  return iResult;
}

/**
  This method exposes the created offsprings to mutation
  More details on Organism::applyMutation
  @param dMutationRate Rate at which mutation happens
  @return Number of mutations happend
*/
int Generation::applyMutations( double dMutationRate )
{
  int iResult = 0;

  for(size_t i = 0; i < mOrganisms.size(); i++)
    iResult += mOrganisms[i]->mutate(dMutationRate);

  return iResult;
}

/**
  This method cleans up the organisms
*/
void Generation::cleanUp()
{
  for(size_t i = 0; i < mOrganisms.size(); i++)
    delete mOrganisms[i];

  mOrganisms.clear();
}

/**
  This method removes all organisms from this generation
  This method will not clean them from memory, just clear them from the list
*/
void Generation::removeAllOrganisms()
{
  mOrganisms.clear();
}

/**
  This method returns the number of organisms currently in this genration
  @return Number of organisms in the generation
*/
unsigned Generation::getOrganismCount()
{
  return mOrganisms.size();
}

/**
  This method reutrns the specified organism pointer by its index
  @param iIndex The index of the organism
  @return Pointer to the specfied organism
*/
Organism * Generation::getOrganism( unsigned iIndex )
{
  if( iIndex < mOrganisms.size() )
    return mOrganisms[iIndex];
  else
    return NULL;
}

/**
  This method adds an organism to the organisms of the genration
  @param organism The new organism to be added
*/
void Generation::addOrganism( Organism * organism )
{
  mOrganisms.push_back( organism );
}

/**
  This method clears all fitnesses in the generation to enable
  the newly created organisms to be tested for their fitnesses.
*/
void Generation::clearAllFitnesses()
{
  for(size_t i = 0; i < mOrganisms.size(); i++)
    mOrganisms[i]->setFitness(UnknownDoubleValue);
}

/**
  This method returns the most fitting organism in the generation
  @return Most fitting organism in generation
*/
Organism * Generation::getBestOrganism()
{
  if( mOrganisms.size() == 0 )
    return NULL;

  Organism * result = mOrganisms[0];
  for(size_t i = 1; i < mOrganisms.size(); i++)
    if(mOrganisms[i]->getFitness() > result->getFitness())
      result = mOrganisms[i];

  return result;
}

/**
  This method returns the next organism which is not tested for its fitness in the environment
  @return Next test case organism
*/
Organism * Generation::getNextTestCase()
{
  for(size_t i = 0; i < mOrganisms.size(); i++)
    if(mOrganisms[i]->getFitness() == UnknownDoubleValue)
      return mOrganisms[i];

  return NULL;
}

/**
  This method is responsible to select parents for creating offsprings, and then create offsprings;
  thus making another generation for testing
  @param dMutationRate Rate at which mutation happens
  @param dXOverWillingness Willingness off offsprings to crossover
  @param dXOverRate Rate at which crossover happens
  @param selMethod Method on which parents are selected to produce offsprings
  @param dWinnerProb Probability at which the winner becomes parent (tournament selection)
  @param dMinFitness Minimum fitness of organisms. Used to find out slice area of roulette wheel and SUS
  @return A new generation
*/
Generation * Generation::reproduce( double dMutationRate, double dXOverWillingness,
                                    double dXOverRate, SelectionMethodT selMethod,
                                    double dWinnerProb, double dMinFitness )
{
  Generation * result = NULL;
  if(selMethod == RouletteWheel)
    result = selectByRouletteWheel(dMinFitness);
  else if(selMethod == Tournament)
    result = selectByTournamenting(dWinnerProb);
  else if(selMethod == SUS)
    result = selectBySUS(dMinFitness);
  else
    Logger::getSingleton()->log(9, "(Generation) selection method not specified. can not reproduce");

  if(!result)
    return NULL;

  result->clearAllFitnesses();
  int iXOvers   = result->applyXOvers(dXOverWillingness, dXOverRate);
  int iMutation = result->applyMutations(dMutationRate);
  Logger::getSingleton()->log(9, "(Generation) reproduced a new generation. Mutation count: %d, XOver: %d", iMutation, iXOvers);
  return result;
}




/**********************************************************************************/
/***********************************  Nature  *************************************/
/**********************************************************************************/

/**
  This is the constructor of the Nature class.
*/
Nature::Nature()
{
}

/**
  This is the destructor of Nature, cleans up
*/
Nature::~Nature()
{
  cleanUp();
}

/**
  This method cleans up the Nature and frees all the memories allocated
*/
void Nature::cleanUp()
{
  for(size_t i = 0; i < mGenerations.size(); i++)
    delete mGenerations[i];

  mGenerations.clear();
}

/**
  This method shall load default values for genetic iterations. If derived
  classes want to use their own values, they shall implement this method.
*/
void Nature::initializeDefaultValues()
{
  mXOverRate = 0.6;
  mXOverWillingness = 0.6;
  mMutationRate = 0.02;
  mSelectionMethod = RouletteWheel;
  mMinFitness = 0;
  mWinProbability = 0.8;
}

/**
  This method loads previous generations. The generations which have been
  set up and tested previously and save to any desired location. Derived
  classes shall implement this method in order to have the ability to load
  their previous generaitons.
*/
void Nature::loadGenerations()
{
  Logger::getSingleton()->log(9, "(Nature) not loading any generations. method is not defined");
}

/**
  This method saves all generations in cache. Derived classes shall implement
  this method in order to have the ability to save their data.
*/
void Nature::saveGenerations()
{
  Logger::getSingleton()->log(9, "(Nature) not saving any generations. method is not defined");
}

/**
  This method initializes nature.
*/
void Nature::setupNature()
{
  initializeDefaultValues();
  loadGenerations();

  if(mGenerations.empty())
    initializeFirstGeneration();
}

/**
  This method returns the best organism in all the history of the organisms
  @return The best organism
*/
Organism * Nature::getBestOrganism()
{
  Organism * result = NULL;
  Organism * temp;

  for(size_t i = 0; i < mGenerations.size(); i++)
  {
    temp = mGenerations[i]->getBestOrganism();
    if(!result || (temp->getFitness() > result->getFitness()))
      result = temp;
  }

  return result;
}

/**
  This method returns the next test case of organisms
  @return The next organism to be tested
*/
Organism * Nature::getNextTestCase()
{
  static unsigned iGen = 0;
  Organism * result;

  for(size_t i = iGen; i < mGenerations.size(); i++)
    if((result = mGenerations[i]->getNextTestCase()))
    {
      // Speed up the search for next time
      iGen = i;
      return result;
    }

  // If the control reached here, it means that no organism has
  // been left for testing, and all have been tested
  // So it is the time to advance generations and have the last
  // generation produce new offsprings

  // Note that there must be a generation at the very first of
  // learning process, so that the next statement will not segfault
  if(mGenerations.empty())
  {
    Logger::getSingleton()->log(9, "(Nature) no untested testcases and no generations to reproduce upon");
    return NULL;
  }

  Generation * gen = mGenerations[mGenerations.size() - 1];
  gen = gen->reproduce(mMutationRate, mXOverWillingness, mXOverRate, mSelectionMethod, mWinProbability, mMinFitness);
  if(gen)
    mGenerations.push_back(gen);
  else
    return NULL;

  return gen->getNextTestCase();
}


};




/**********************************************************************************/
/******************************* Utility Methods  *********************************/
/**********************************************************************************/

/**
  This method is for comparing 2 organisms based on their fitness. This method
  is used in Generation::rank method, to enable sorting via SFL's qsort function
  @param o1 First organism
  @param o2 Second Organism
  @return -1 if o1 << o2; 0 if o1 == o2; 1 if o1 > o2
*/
int compare_organisms( const void * o1, const void * o2 )
{
  using namespace Klaus;

  const Organism * first  = (Organism*)(o1);
  const Organism * second = (Organism*)(o2);

  if( first->getFitness() < second->getFitness() )
    return -1;
  else if( first->getFitness() == second->getFitness() )
    return 0;
  else
    return 1;
}

