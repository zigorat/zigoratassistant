/*
*********************************************************************************
*         PIDController.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/17/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class decleration for PIDController which controls and handles     *
*            joint manipulation                                                 *
*                                                                               *
*********************************************************************************
*/

/*! \file PIDController.h
<pre>
<b>File:</b>          PIDController.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/17/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class decleration for PIDController which controls and handles
            joint manipulation.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/17/2010       Mahdi           Initial version created
</pre>
*/

#ifndef PID_CONTROLLER
#define PID_CONTROLLER


namespace Klaus
{

  /*! A proportional–integral–derivative controller (PID controller) is a generic control loop feedback mechanism
     (controller) widely used in industrial control systems. A PID controller calculates an "error" value as the
     difference between a measured process variable and a desired setpoint. The controller attempts to minimize
     the error by adjusting the process control inputs. The PID parameters used in the calculation must be tuned
     according to the nature of the system.
     The PID controller calculation (algorithm) involves three separate parameters; the proportional, the integral
     and derivative values. The proportional value determines the reaction to the current error, the integral value
     determines the reaction based on the sum of recent errors, and the derivative value determines the reaction
     based on the rate at which the error has been changing. The weighted sum of these three actions is used to
     adjust the process via a control element such as the position of a control valve or the power supply of a
     heating element.
  */
  class PIDController
  {
    private:
      double       mKp;             /*!< Proportional gain                  */
      double       mKi;             /*!< Integral gain                      */
      double       mKd;             /*!< Derivative gain                    */
      double       mIMax;           /*!< Maximum allowable integrator state */
      double       mIMin;           /*!< Minimum allowable integrator state */
      double       mLastError;      /*!< Last error                         */
      double       mIntegralError;  /*!< Integrator error                   */

    public:
                   PIDController    (                              );
                   PIDController    ( const double & p,
                                      const double & i,
                                      const double & d,
                                      const double & min,
                                      const double & max           );
                 ~ PIDController    (                              );

      double       calculateNeededMV( const double & pv,
                                      const double & setpoint,
                                      const double & dLastDuration );
      void         reset            (                              );
  };


}; // end namespace Klaus


#endif // PID_CONTROLLER
