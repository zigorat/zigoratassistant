/*
*********************************************************************************
*          AgentSettings.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 08/01/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains AgentSettings, a utility to load and use agent  *
*            configurations and thresholds                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file AgentSettings.h
<pre>
<b>File:</b>          AgentSettings.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/01/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains AgentSettings, a utility to load and use agent
                        configurations and thresholds
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/01/2007       Mahdi           Initial version created
</pre>
*/

#ifndef AGENT_SETTINGS
#define AGENT_SETTINGS

#include <string>


#include "SoccerTypes.h"
#include <libZigorat/Singleton.h>  // needed for singleton
#include <libZigorat/SLang.h>      // needed for SToken, SExpression

namespace Klaus
{


  /*! This class implements configurations and thresholds for agent behaviour
      control, enabling to save, load and use */
  class AgentSettings : public Singleton<AgentSettings>
  {
    private:
      // Agent Specific Parameters
      AgentModelType mAgentModel;                 /*!< Agent Model to use                  */

      // GA Walkng Parameters
      double         mWalkingMutationRateGenes;   /*!< Mutation Rate for genes             */
      double         mWalkingMutationRate;        /*!< Mutation Rate in Walking            */
      double         mWalkingXOverRateGenes;      /*!< XOver Rate for genes                */
      double         mWalkingXOverRate;           /*!< XOver Rate in Walking               */
      int            mWalkingStepTest;            /*!< Walking steps done to eval fitness  */
      int            mWalkingPopulation;          /*!< Walking GA's population             */

      // Walking Parameters
      Degree         mWalkingWhenToTurn;          /*!< Turning threshold when walking      */

      // Thresholds
      double         mPlayerConfThr;              /*!< Confidence threshold of a plyer     */
      double         mPlayerHighConfThr;          /*!< High confidence threshold of player */

      // Misc Parameters
      bool           mTraining;                   /*!< State of agent: Training or playing */

      // Simulator Parameters
      double         mFieldLength;                /*!< Length of soccer field              */
      double         mFieldWidth;                 /*!< Width of soccer field               */
      double         mGoalWidth;                  /*!< Goal width                          */
      double         mGoalHeight;                 /*!< Goal height                         */
      double         mFlagHeight;                 /*!< height of the corner flags          */
      int            mMaxSaySize;                 /*!< Maximum say message size            */

      bool           parseToken                   ( SToken token                           );

      // Constructor and destructor to be private for singleton pattern
                     AgentSettings                (                                        );
                   ~ AgentSettings                (                                        );

    public:
      static         AgentSettings * getSingleton (                                        );
      static void    freeSingleton                (                                        );

      bool           loadFromFile                 ( const std::string & strFileName        );
      void           saveToFile                   ( const std::string & strFileName        );

      // Agent Specific Parameters
      AgentModelType getAgentModel                (                                        ) const;

      // GA Walkng Parameters
      double         getWalkingMutationRateGenes  (                                        ) const;
      double         getWalkingMutationRate       (                                        ) const;
      double         getWalkingXOverRateGenes     (                                        ) const;
      double         getWalkingXOverRate          (                                        ) const;
      int            getWalkingStepTest           (                                        ) const;
      int            getWalkingPopulation         (                                        ) const;

      // Walking Thresholds
      Degree         getWalkingWhenToTurn         (                                        ) const;

      // Thresholds
      double         getPlayerConfThr             (                                        ) const;
      double         getPlayerHighConfThr         (                                        ) const;

      // Misc. Parameters
      bool           isTraining                   (                                        ) const;

      // Simulator Parameters
      double         getFieldLength               (                                        ) const;
      double         getFieldWidth                (                                        ) const;
      double         getGoalWidth                 (                                        ) const;
      double         getGoalHeight                (                                        ) const;
      double         getFlagHeight                (                                        ) const;
      int            getMaxSaySize                (                                        ) const;
  };


}; // end namespace Klaus

#endif //AGENT_SETTINGS
