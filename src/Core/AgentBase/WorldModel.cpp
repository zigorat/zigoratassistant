/*
*********************************************************************************
*          WorldModel.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the agents memory of outside world                         *
*                                                                               *
*********************************************************************************
*/

/*! \file WorldModel.cpp
<pre>
<b>File:</b>          WorldModel.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the agents memory of outside world
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#include "WorldModel.h"


/*! WorldModel singleton instance */
template<> Klaus::WorldModel* Klaus::Singleton<Klaus::WorldModel>::mInstance = 0;


/**
  This constructor creates the world model, all variables are initialized by
  default values. First all singleton instances are cached, then field information
  is initialized internally in world model and then game information is reset. All
  flags are given their names and their global positions.
*/
Klaus::WorldModel::WorldModel()
{
  mLog = Logger::getSingleton();
  mAgent = AgentObject::getSingleton();
  mAgentSettings = AgentSettings::getSingleton();
  mFormation = FormationSystem::getSingleton();
  mBall = Ball::getSingleton();

  size_t i;

  // set up field information
  mFieldInfo.mFieldLength = mAgentSettings->getFieldLength();
  mFieldInfo.mFieldWidth  = mAgentSettings->getFieldWidth();
  mFieldInfo.mGoalWidth   = mAgentSettings->getGoalWidth();
  mFieldInfo.mGoalHeight  = mAgentSettings->getGoalHeight();
  mFieldInfo.mFlagHeight  = mAgentSettings->getFlagHeight();

  // initialize game information
  mGameInfo.mCurrentCycle = 0;
  mGameInfo.mGameTime = 0.0;
  mGameInfo.mTimeLastSeeMsg = -1;
  mGameInfo.mTimeLastHearMsg = -1;
  mGameInfo.mInitOK = false;
  mGameInfo.mPlayMode = BeforeKickOff;
  mGameInfo.mTeamSide = UnknownSide;
  mGameInfo.mSideKickOff = SideLeft;
  mGameInfo.mSimulatorStep = 0.02;
  mGameInfo.mSimulatorTime = 0.0;

  // set up all flag names
  Flag f;
  for(i = ObjectFlagGLT; i < ObjectFlagUnknown; ++i)
  {
    ObjectType o = (ObjectType)i;
    mFlags[o] = new Flag;
    mFlags[o]->setType((ObjectType)i);
  }

  // now set up their global positions
  double half_length = mFieldInfo.mFieldLength / 2.0;
  double half_goal_width = mFieldInfo.mGoalWidth / 2.0;
  double half_width = mFieldInfo.mFieldWidth / 2.0;
  mFlags[ObjectFlagGLT]->setGlobalPosition(VecPosition(-half_length,  half_goal_width, mFieldInfo.mFlagHeight), -1);
  mFlags[ObjectFlagGLB]->setGlobalPosition(VecPosition(-half_length, -half_goal_width, mFieldInfo.mFlagHeight), -1);
  mFlags[ObjectFlagGRT]->setGlobalPosition(VecPosition( half_length,  half_goal_width, mFieldInfo.mFlagHeight), -1);
  mFlags[ObjectFlagGRB]->setGlobalPosition(VecPosition( half_length, -half_goal_width, mFieldInfo.mFlagHeight), -1);
  mFlags[ObjectFlagCLT]->setGlobalPosition(VecPosition(-half_length,  half_width,      0.0                   ), -1);
  mFlags[ObjectFlagCLB]->setGlobalPosition(VecPosition(-half_length, -half_width,      0.0                   ), -1);
  mFlags[ObjectFlagCRT]->setGlobalPosition(VecPosition( half_length,  half_width,      0.0                   ), -1);
  mFlags[ObjectFlagCRB]->setGlobalPosition(VecPosition( half_length, -half_width,      0.0                   ), -1);

  // no need to set up players

  // other properties
  mBalanceState = Normal;
}

/**
  Destructor currently does nothing
*/
Klaus::WorldModel::~WorldModel()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::WorldModel * Klaus::WorldModel::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new WorldModel;
    Logger::getSingleton()->log(1, "(WorldModel) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::WorldModel::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(WorldModel) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(WorldModel) removed singleton");
  }
}

/**
  This method returns the instance of the object specified by its name. The
  instance is then used to update its local and global information.
  @param object The object's name
  @return The object's instance
*/
Klaus::Object* Klaus::WorldModel::getObjectInstance(ObjectType object)
{
  if(object == ObjectBall)
    return mBall;
  else if(mAgent && object == mAgent->getType())
    return mAgent;
  else if(SoccerTypes::isTeammate(object))
  {
    int nr = SoccerTypes::getPlayerNumber(object);
    PlayerObject::Map::const_iterator it = mTeammates.find(nr);
    if(it != mTeammates.end())
      return mTeammates[nr];
  }
  else if(SoccerTypes::isOpponent(object))
  {
    int nr = SoccerTypes::getPlayerNumber(object);
    PlayerObject::Map::const_iterator it = mOpponents.find(nr);
    if(it != mOpponents.end())
      return mOpponents[nr];
  }
  else if(SoccerTypes::isFlag(object))
  {
    Flag::Map::const_iterator it = mFlags.find(object);
    if(it != mFlags.end())
      return it->second;
  }

  mLog->log(34, "(WorldModel) given object has no instance: %s", SoccerTypes::getObjectStr(object).c_str());
  return NULL;
}

/**
  This method returns constant instance to the specified object in game.
  @param object The object name
  @return object constant instance
*/
const Klaus::Object* Klaus::WorldModel::getObjectInstance(ObjectType object) const
{
  if(object == ObjectBall)
    return mBall;
  else if(mAgent && object == mAgent->getType())
    return mAgent;
  else if(SoccerTypes::isTeammate(object))
  {
    PlayerObject::Map::const_iterator it = mTeammates.find(SoccerTypes::getPlayerNumber(object));
    if(it != mTeammates.end())
      return it->second;
  }
  else if(SoccerTypes::isOpponent(object))
  {
    PlayerObject::Map::const_iterator it = mOpponents.find(SoccerTypes::getPlayerNumber(object));
    if(it != mOpponents.end())
      return it->second;
  }
  else if(SoccerTypes::isFlag(object))
  {
    Flag::Map::const_iterator it = mFlags.find(object);
    if(it != mFlags.end())
      return it->second;
  }

  mLog->log(34, "(WorldModel) given object has no instance: %s", SoccerTypes::getObjectStr(object).c_str());
  return NULL;
}

/**
  This method checks whether the specified player exists in the field or not. A
  player is considered to be existing on field if it was seen at least once.
  @param o The player's object name
  @return True if the player exists in the field, false otherwise
*/
bool Klaus::WorldModel::isPlayerValid(const ObjectType & o) const
{
  int iNum = SoccerTypes::getPlayerNumber(o);

  if(SoccerTypes::isTeammate(o))
    return mTeammates.find(iNum) != mTeammates.end();
  else
    return mOpponents.find(iNum) != mOpponents.end();
}

/**
  This method initializes the world model. Loads the agent model and
  it's perceptor, and then sets our team's name.
  @param strTeamName Name of the team
  @return True on successful initialization of world model, false otherwise
*/
bool Klaus::WorldModel::initialize(const std::string & strTeamName)
{
  // assign the team name
  mTeamName = strTeamName;

  // get the graph
  mAgentGraph = mAgent->getSceneGraph();

  using namespace RSG2;
  // now update all perceptors cache
  Node::Array nodes;
  mAgentGraph->getAllNodes(nodes);
  for(size_t i = 0; i < nodes.size(); ++i)
  {
    Joint *j = dynamic_cast<Joint*>(nodes[i]);
    if(j)
      mJoints[j->getPerceptor()->getName()] = j;
    else if(dynamic_cast<GyroRatePerceptor*>(nodes[i]))
      mGyros.push_back(dynamic_cast<GyroRatePerceptor*>(nodes[i]));
    else if(dynamic_cast<Accelerometer*>(nodes[i]))
      mAccs.push_back(dynamic_cast<Accelerometer*>(nodes[i]));
    else if(dynamic_cast<FRP*>(nodes[i]))
      mFRPs.push_back(dynamic_cast<FRP*>(nodes[i]));
  }

  return true;
}

/**
  This method finds out a joint in the agent's scene graph by its
  perceptor name and returns its instance.
  @param strName name of the joint's perceptor
  @return constant joint instance from the given perceptor name
*/
const Klaus::RSG2::Joint* Klaus::WorldModel::getJoint(const std::string& strName) const
{
  RSG2::Joint::Map::const_iterator it = mJoints.find(strName);
  if(it == mJoints.end())
  {
    mLog->log(34, "(WorldModel) no joint with perceptor '%s'", strName.c_str());
    return NULL;
  }

  return it->second;
}

/**
  This method finds a joint with the given perceptor name and returns its current
  angle. The given number specifies the requested joint motor number.
  @param strName Perceptor name of the joint
  @param iNumber Motor number of the joint
  @return Angle of the specified motor number of joint in degrees
*/
Klaus::Degree Klaus::WorldModel::getJointAngle(const std::string& strName, const int & iNumber)
{
  RSG2::Joint::Map::iterator it = mJoints.find(strName);
  if(it != mJoints.end())
    return it->second->getAngle(iNumber);

  Logger::getSingleton()->log(34, "(WorldModel) no joint with perceptor name '%s'", strName.c_str());
  return 0.0;
}

/**
  This method returns a joint's speed. The joint is found using it perceptor name
  and the speed for the specified motor number will be returned.
  @param strName Perceptor name of the joint
  @param iNumber Joint's motor number
  @return Speed of the joint in degrees/second
*/
Klaus::Degree Klaus::WorldModel::getJointSpeed(const std::string& strName, const int & iNumber)
{
  RSG2::Joint::Map::iterator it = mJoints.find(strName);
  if(it != mJoints.end())
    return it->second->getSpeed(iNumber);

  Logger::getSingleton()->log(34, "(WorldModel) no joint with perceptor name '%s'", strName.c_str());
  return 0.0;
}

/**
  This method finds out a gyro in the agent structure from its
  name and returns its current rate.
  @param strName name of the gyroscope
  @return The gyro's rate. If the gyro is not found, VecPosition::Unknown will be returned
*/
Klaus::VecPosition Klaus::WorldModel::getGyro(const std::string& strName) const
{
  for(size_t i = 0; i < mGyros.size(); ++i)
    if(mGyros[i]->getName() == strName)
      return mGyros[i]->getRate();

  Logger::getSingleton()->log(34, "(WorldModel) no gyro with name '%s'", strName.c_str());
  return VecPosition();
}

/**
  This method finds out an accelerometer with its given name and returns
  its acceleration vector. The returned value is local to the accelerometer
  node and is evaluated considering the gravity acceleration.
  @param strName Name of the accelerometer
  @return Its percepted acceleration. If the accelerometer is not found,
          VecPosition::Unknown will be returned
*/
Klaus::VecPosition Klaus::WorldModel::getAccelerometer(const std::string& strName) const
{
  for(size_t i = 0; i < mAccs.size(); ++i)
    if(mAccs[i]->getName() == strName)
      return mAccs[i]->getAcceleration();

  Logger::getSingleton()->log(34, "(WorldModel) no accelerometer with name '%s'", strName.c_str());
  return VecPosition();
}

/**
  This method finds out a force resistance perceptor in the agent's foot and
  returns it's percepted force center and magnitude.
  @param strName name of the FRP
  @param c The center element of the FRP perception will be copied here
  @param f The force element of the FPR perception will be copied here
  @return True if the requested FTP existed in the agent structure, false otherwise
*/
bool Klaus::WorldModel::getFRP(const std::string& strName, VecPosition& c, VecPosition& f)
{
  for(size_t i = 0; i < mFRPs.size(); ++i)
    if(mFRPs[i]->getName() == strName)
    {
      c = mFRPs[i]->getCenter();
      f = mFRPs[i]->getForce();
      return true;
    }

  Logger::getSingleton()->log(34, "(WorldModel) no FRP with name '%s'", strName.c_str());
  return false;
}

/**
  This method returns the length of soccer field
  @return Field's length
*/
double Klaus::WorldModel::getFieldLength() const
{
  return mFieldInfo.mFieldLength;
}

/**
  This method returns the width of soccer field
  @return Width of the field
*/
double Klaus::WorldModel::getFieldWidth() const
{
  return mFieldInfo.mFieldWidth;
}

/**
  This method returns the width of the goal
  @return Goal width (distance of its two poles)
*/
double Klaus::WorldModel::getGoalWidth() const
{
  return mFieldInfo.mGoalWidth;
}

/**
  This method returns the goal height
  @return Goal height (height of the horizontal beam)
*/
double Klaus::WorldModel::getGoalHeight() const
{
  return mFieldInfo.mGoalHeight;
}

/**
  This method returns the height flags which are located above the field
  @return The flag's height
*/
double Klaus::WorldModel::getFlagHeight() const
{
  return mFieldInfo.mFlagHeight;
}

/**
  This method returns the simulator's time. This is different with game time,
  like when not kicked off yet, still the simulation time advances although
  it is not evaluated as the matches duration.
  @return Time of the simulation
*/
Klaus::Time Klaus::WorldModel::getTime() const
{
  return mGameInfo.mSimulatorTime;
}

/**
  This method returns the current time in match.
*/
Klaus::Time Klaus::WorldModel::getGameTime() const
{
  return mGameInfo.mGameTime;
}

/**
  This method returns the current cycle number. When agent
  connects, this will be zero, each simulator cycle this
  will advance 1 unit.
  @return Current cycle number of game
*/
size_t Klaus::WorldModel::getCycle() const
{
  return mGameInfo.mCurrentCycle;
}

/**
  This method returns the time step between simulation cycles.
  @return Time step between simulation cycles.
*/
double Klaus::WorldModel::getTimeStep() const
{
  return mGameInfo.mSimulatorStep;
}

/**
  This method returns the actual side of the team in the field.
  @return Actual side of team in the field (though agent
          always considers himself in the left half of the field)
*/
Klaus::SideType Klaus::WorldModel::getTeamSide() const
{
  return mGameInfo.mTeamSide;
}

/**
  This method returns the current play mode of the game
  @return Current play mode of the game
*/
Klaus::PlayMode Klaus::WorldModel::getPlayMode() const
{
  return mGameInfo.mPlayMode;
}

/**
  This method checks whether the play mode indicates that we have
  a free kick. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a free kick.
*/
bool Klaus::WorldModel::isFreeKickUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == FreeKickLeft   && mGameInfo.mTeamSide == SideLeft) ||
         (pm == FreeKickRight  && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the play mode indicates that the other
  team has a free kick. When the specified PlayMode equals
  UnknownPlayMode (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team has a free kick.
*/
bool Klaus::WorldModel::isFreeKickThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == FreeKickRight && mGameInfo.mTeamSide == SideLeft) ||
         (pm == FreeKickLeft && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that we have
  a corner kick. When the specified PlayMode equals UnknownPlayMode (default),
  the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a corner kick.
*/
bool Klaus::WorldModel::isCornerKickUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == CornerKickLeft  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == CornerKickRight && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that the other
  team has a corner kick. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team has a corner kick.
*/
bool Klaus::WorldModel::isCornerKickThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == CornerKickRight  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == CornerKickLeft   && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that we stood
  offside. When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we stood offside.
*/
bool Klaus::WorldModel::isOffsideUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == OffsideRight  && mGameInfo.mTeamSide == SideRight) ||
         (pm == OffsideLeft   && mGameInfo.mTeamSide == SideLeft);
}

/**
  This method checks whether the current play mode indicates that the other
  team stood offside. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team stood offside.
*/
bool Klaus::WorldModel::isOffsideThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == OffsideLeft  && mGameInfo.mTeamSide == SideRight) ||
         (pm == OffsideRight && mGameInfo.mTeamSide == SideLeft);
}

/**
  This method checks whether the current play mode indicates that we have
  a kick in. When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a kick in.
*/
bool Klaus::WorldModel::isKickInUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == KickInLeft  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == KickInRight && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that the other
  team has a kick in. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team has a kick in.
*/
bool Klaus::WorldModel::isKickInThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == KickInRight  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == KickInLeft   && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that we have
  a kick off. When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a kick off.
*/
bool Klaus::WorldModel::isKickOffUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == KickOffLeft  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == KickOffRight && mGameInfo.mTeamSide == SideRight) ||
         (pm == GoalRight    && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == GoalLeft     && mGameInfo.mTeamSide == SideRight);
}

/**
  This method checks whether the current play mode indicates that the other
  team has a kick off. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used. When the specified PlayMode
  equals UnknownPlayMode (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team has a kick off.
*/
bool Klaus::WorldModel::isKickOffThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == KickOffRight  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == KickOffLeft   && mGameInfo.mTeamSide == SideRight) ||
         (pm == GoalLeft      && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == GoalRight     && mGameInfo.mTeamSide == SideRight);
}

/**
  This method checks whether the current play mode indicates that we have
  a goal kick. When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a goal kick.
*/
bool Klaus::WorldModel::isGoalKickUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == GoalKickLeft  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == GoalKickRight && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the current play mode indicates that the other
  team has a kick off. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether the other team has a kick off.
*/
bool Klaus::WorldModel::isGoalKickThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return (pm == GoalKickRight  && mGameInfo.mTeamSide == SideLeft ) ||
         (pm == GoalKickLeft   && mGameInfo.mTeamSide == SideRight) ;
}

/**
  This method checks whether the play mode indicates that there is (or will be)
  a before kick off situation. This is the case when the play mode equals
  BeforeKickOff or either GoalLeft or GoalRight since after the goal the play
  mode will go to BeforeKickOff. When the specified PlayMode equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether there is a before kick off situation.
*/
bool Klaus::WorldModel::isBeforeKickOff(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return pm == BeforeKickOff  || pm == GoalLeft  || pm == GoalRight || pm == KickOffRight || pm == KickOffLeft;
}

/**
  This method checks whether the play mode indicates that there is (or
  will be) a before kick off situation for our team. This is the case
  when the play mode equals BeforeKickOff and kick off side is the same
  as our own team's side. When the specified PlayModeT equals UnknownPlayMode
  (default), the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether there is a before kick off situation.
*/
bool Klaus::WorldModel::isBeforeKickOffUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  if(pm == BeforeKickOff)
    return mGameInfo.mSideKickOff == mGameInfo.mTeamSide;
  else
    return isKickOffUs(pm);
}

/*!
  This method checks whether the play mode indicates that there is (or will be)
  a before kick off situation for their team. This is the case when the play mode
  equals BeforeKickOff and kick off side is left or either GoalLeft or kick off
  side is right. When the specified PlayMode equals UnknownPlayMode (default),
  the current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether there is a before kick off situation.
*/
bool Klaus::WorldModel::isBeforeKickOffThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  if(pm == BeforeKickOff)
    return mGameInfo.mSideKickOff != mGameInfo.mTeamSide;
  else
    return isKickOffThem(pm);
}

/**
  This method checks whether the play mode indicates that there is
  a dead ball situation and our team is allowed to perform the action.
  That is our team has either a free kick, kick in, corner kick or a kick in.
  When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether we have a dead ball situation.
*/
bool Klaus::WorldModel::isDeadBallUs(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return isKickInUs (pm) || isFreeKickUs (pm) || isCornerKickUs(pm)
      || isKickOffUs(pm) || isOffsideThem(pm) || isGoalKickUs  (pm) ;
}


/**
  This method checks whether the current play mode indicates that there is
  a dead ball situation and their team is allowed to perform the action.
  That is their team has either a free kick, kick in, corner kick or a kick
  in. When the specified PlayMode equals UnknownPlayMode (default), the
  current play mode is used.
  @param pm play mode to check. In default case (UnknownPlayMode) the current play mode is used.
  @return bool indicating whether they have a dead ball situation.
*/
bool Klaus::WorldModel::isDeadBallThem(PlayMode pm)
{
  if(pm == UnknownPlayMode)
    pm = getPlayMode();

  return isFreeKickThem(pm) || isKickInThem  (pm) || isCornerKickThem(pm)
     ||  isKickOffThem (pm) || isGoalKickThem(pm) || isOffsideUs     (pm);
}

/**
  This method returns the agent's global position in field
  @return Global position of the agent inn field
*/
Klaus::VecPosition Klaus::WorldModel::getAgentPosition() const
{
  return mAgent->getGlobalPosition();
}

/**
  This method returns the agent's global orientation in field
  @return Global orientation of the agent
*/
Klaus::Quaternion Klaus::WorldModel::getAgentOrientation() const
{
  return mAgent->getOrientation();
}

/**
  This method returns the approximate direction of the agent on the soccer field
  @return Approximate direction of the agent in the field
*/
Klaus::Degree Klaus::WorldModel::getAgentHeading() const
{
  return mAgent->getOrientation().getZAxis().getPhi();
}

/**
  This method returns approximate angle of agent outside the soccer
  field. (The phi element of it's polar orientation representation)
  @return Approximate angle outside the soccer field
*/
Klaus::Degree Klaus::WorldModel::getAgentAttitude() const
{
  return mAgent->getOrientation().getZAxis().getTheta();
}

/**
  This method returns the agent number
  @return Agen't uniform number
*/
int Klaus::WorldModel::getAgentNumber() const
{
  return mAgent->getPlayerNumber();
}

/**
  This method returns the agent's object type
  @return Agent object type
*/
Klaus::ObjectType Klaus::WorldModel::getAgentObjectType() const
{
  return SoccerTypes::getTeammateTypeFromNumber(mAgent->getPlayerNumber());
}

/**
  This method returns the team name of the agent
  @return Our team name
*/
std::string Klaus::WorldModel::getTeamName() const
{
  return mTeamName;
}

/**
  This method returns the opponent team name
  @return Name of the opponent team
*/
std::string Klaus::WorldModel::getOpponentTeamName() const
{
  return mOpponentTeamName;
}

/**
  This method returns the current balance state of agent
  @return Current balance state of agent. It is updated in WorldModel::update()
*/
Klaus::BalanceState Klaus::WorldModel::getAgentBalanceState() const
{
  return mBalanceState;
}

/**
  This method returns the global ball position in field
  @return Ball position in global coordinates
*/
Klaus::VecPosition Klaus::WorldModel::getBallPos() const
{
  return mBall->getGlobalPosition();
}

/**
  This method finds out the distance between the ball and the agent.
  @param bOnField If this is true, z-coordinate of the agent and the
         ball will be discarded in calculation.
  @return Distance of the ball to the agent
*/
double Klaus::WorldModel::getBallDistance(bool bOnField)
{
  if(bOnField)
    return mBall->getGlobalPosition().getDistanceOnPlane(mAgent->getGlobalPosition());
  else
    return mBall->getGlobalPosition().getDistanceTo(mAgent->getGlobalPosition());
}

/**
  This method returns the linear speed of ball
  @return Ball's linear speed
*/
double Klaus::WorldModel::getBallSpeed() const
{
  return mBall->getVelocity().getMagnitude();
}

/**
  This method returns direction of the ball
  @return Ball direction
*/
Klaus::Degree Klaus::WorldModel::getBallDirection() const
{
  return mBall->getVelocity().getDirection();
}

/**
  This method returns the last seen velocity of ball
  @return Last seen velocity of the ball
*/
Klaus::VecPosition Klaus::WorldModel::getBallVelocity() const
{
  return mBall->getVelocity();
}

/**
  This method returns the last time the specified object was seen
  @param object The object to check its last vision time
  @return The last time the percepted see message contained the object
*/
Klaus::Time Klaus::WorldModel::getTimeLastSeen(const Klaus::ObjectType& object) const
{
  const Object * o = getObjectInstance(object);
  if(o)
    return o->getTimeLastSeen();
  else
    return -1;
}

/**
  This method returns last seen global position of the object. Use this considering the object confidence.
  @param object The object to get its global position
  @return Global position of the requested object
*/
Klaus::VecPosition Klaus::WorldModel::getGlobalPosition(const Klaus::ObjectType& object) const
{
  const Object * o = getObjectInstance(object);
  if(o)
    return o->getGlobalPosition();
  else
    return VecPosition::Unknown;
}

/**
  This method returns the specified objects global speed
  @param object The object to get it's speed
  @return Speed of the object
*/
double Klaus::WorldModel::getGlobalSpeed(const Klaus::ObjectType& object) const
{
  const DynamicObject * o = dynamic_cast<const DynamicObject*>(getObjectInstance(object));

  if(o)
    return o->getVelocity().getMagnitude();
  else
    return UnknownDoubleValue;
}

/**
  This method returns the global direction of the specified object
  @param object The object to get it's global direction
  @return Global direction of the objects movement
*/
Klaus::Degree Klaus::WorldModel::getGlobalDirection(const Klaus::ObjectType& object) const
{
  const DynamicObject * o = dynamic_cast<const DynamicObject*>(getObjectInstance(object));

  if(o)
    return o->getVelocity().getDirection();
  else
    return Degree::Unknown;
}

/**
  This method returns the global linear velocity of the given object
  @param object The object to get it's velocity
  @return Global velocity of the given object
*/
Klaus::VecPosition Klaus::WorldModel::getGlobalVelocity(const Klaus::ObjectType& object) const
{
  const DynamicObject * o = dynamic_cast<const DynamicObject*>(getObjectInstance(object));

  if(o)
    return o->getVelocity();
  else
    return VecPosition::Unknown;
}

/**
  This method returns the global acceleration of the given object
  @param object The object to get its global linear acceleration
  @return Global linear acceleration of the object
*/
Klaus::VecPosition Klaus::WorldModel::getGlobalAcceleration(const Klaus::ObjectType& object) const
{
  const DynamicObject * o = dynamic_cast<const DynamicObject*>(getObjectInstance(object));

  if(o)
    return o->getAcceleration();
  else
    return VecPosition::Unknown;
}

/**
  This method returns the relative position of the object to the agent.
  This will be in cartesian coordinates.
  @param object The object to get its relative position
  @return Relative position of the object to agent in cartesian coordinates.
*/
Klaus::VecPosition Klaus::WorldModel::getRelativePosition(const Klaus::ObjectType& object) const
{
  const Object * o = getObjectInstance(object);
  if(o)
    return o->getRelativePosition();
  else
    return VecPosition::Unknown;
}

/**
  This method gets the relative position of the given point to the agent
  @param position The point on field in global coordinates
  @return Relative position of the given point to agent
*/
Klaus::VecPosition Klaus::WorldModel::getRelativePosition(const Klaus::VecPosition& position) const
{
  return position - mAgent->getGlobalPosition();
}

/**
  This method finds out the relative distance of the given object
  @param object The object to get its relative distance
  @return Relative distance of the given object with agent
*/
double Klaus::WorldModel::getRelativeDistance(const Klaus::ObjectType& object) const
{
  VecPosition p = getRelativePosition(object);
  return sqrt(p[0]*p[0] + p[1]*p[1]);
}

/**
  This method returns the relative distance of the given position on field to the agent
  @param position The position to get its distance with agent
  @return Relative distance of the given position with the agent
*/
double Klaus::WorldModel::getRelativeDistance(const Klaus::VecPosition& position) const
{
  return mAgent->getGlobalPosition().getDistanceOnPlane(position);
}

/**
  This method returns the relative direction of the given object with the agent
  @param object The object to get it's relative direction to agent
  @return Relative direction of the object to agent
*/
Klaus::Degree Klaus::WorldModel::getRelativeAngle(const Klaus::ObjectType& object) const
{
  const Object * o = getObjectInstance(object);
  if(o)
    return o->getRelativePosition().getDirection();
  else
    return Degree::Unknown;
}

/**
  This method returns the relative direction of the given position to the agent
  @param position The point to get it's relative direction to agent
  @return Relative direction of the given point to the agent
*/
Klaus::Degree Klaus::WorldModel::getRelativeAngle(const Klaus::VecPosition& position) const
{
  return (position - mAgent->getGlobalPosition()).getDirection() - getAgentAttitude();
}

/**
  This method returns the confidence value for the given object. This value
  represent how confident is the data for the given object.
  @param object The object to get its confidence value
  @return It's confidence value
*/
double Klaus::WorldModel::getConfidence(ObjectType object) const
{
  const Object * o = getObjectInstance(object);

  if(o)
    return o->getConfidence(mGameInfo.mSimulatorTime);
  else
    return 0.0;
}

/**
  This method checks whether the confidence value for the given object is good
  or not. This will be checked with the threshold defined in AgentSettings.
  @param object The object to check its threshold
  @return True if the object's confidence is considered good, false otherwise
*/
bool Klaus::WorldModel::isConfidenceGood(ObjectType object) const
{
  return getConfidence(object) >= mAgentSettings->getPlayerConfThr();
}

/**
  This method checks whether the confidence value for the given object is considered
  very good or not. This will be checked with the threshold defined in AgentSettings.
  @param object The object to check its threshold
  @return True if the object's confidence is considered good, false otherwise
*/
bool Klaus::WorldModel::isConfidenceVeryGood(ObjectType object) const
{
  return getConfidence(object) >= mAgentSettings->getPlayerHighConfThr();
}

/**
  This method returns the last time which see message arrived.
  @return Last simulation time which see message arrived
*/
Klaus::Time Klaus::WorldModel::getTimeLastSeeMessage() const
{
  return mGameInfo.mTimeLastSeeMsg;
}

/**
  This method returns the last time which hear message arrived
  @return Last simulation time which hear message arrived
*/
Klaus::Time Klaus::WorldModel::getTimeLastHearMessage() const
{
  return mGameInfo.mTimeLastHearMsg;
}

/**
  This method returns our own goalie type
  @return Our own goalie type
*/
Klaus::ObjectType Klaus::WorldModel::getOwnGoalieType() const
{
  return ObjectTeammate1;
}

/**
  This method returns the opponent team's goalie type (object type)
  @return Opponent's goalie type (object type)
*/
Klaus::ObjectType Klaus::WorldModel::getTheirGoalieType() const
{
  return ObjectOpponent1;
}

/**
  This method creates a list of objects in the given object set type which
  have confidence value over the given threshold.
  @param objects The list will be created here
  @param g The object set to create the list from
  @param dConf Confidence threshold which only object's above it will be inserted in list
  @return Number of the objects created in list
*/
int Klaus::WorldModel::getObjectsInSet(ObjectArray& objects, ObjectSetType g, double dConf) const
{
  objects.clear();
  ObjectType o, objGoalie = ObjectTeammate1;

  if( g == TeammatesNoGoalie)
    objGoalie = getOwnGoalieType();
  else if(g == OpponentsNoGoalie)
    objGoalie = getTheirGoalieType();

  int i = 0;

  // when dConf is not specified it has the default value of -1.0, in this
  // case set it to the confidence threshold defined in AgentSettings.
  // It is done for all object sets, not only players.
  if(dConf == -1.0)
  {
    if(g == Flags)
      dConf = 0.0; // return all flags
    else
      dConf = mAgentSettings->getPlayerConfThr();
  }

  do
  {
    o = (ObjectType)i;
    if(SoccerTypes::isInSet(o, g, objGoalie))
    {
      if(SoccerTypes::isPlayer(o) && !isPlayerValid(o))
      {
        // if the player object is not in the game, do not waste time on him
        // do nothing
      }
      else
      {
        // confidence of 1.0 can only be in same cycle as see
        // message. Therefore first test should succeed normally;
        // in cases where this method is called after see message,
        // but new game state has already arrived, confidence is lowered
        // but we want to return object that was seen in last see
        // message; this compensates for those cases.

        if(getConfidence(o) >= dConf || getTimeLastSeen(o) == getTimeLastSeeMessage())
          objects.push_back(o);
      }
    }

    i++;
  } while(i < UnknownObject);

  return objects.size();
}

/**
  This method finds out the strategic position of the requested player according
  to the specified formation and ball position.
  @param iNum Player number to get it's strategic position
  @param ft The formation to use for gettings the strategic position
  @return The position of the player in the current game status
*/
Klaus::VecPosition Klaus::WorldModel::getStrategicPosition(int iNum, FormationType ft) const
{
  if(iNum == -1)
    iNum = mAgent->getPlayerNumber();

  if(ft == UnknownFormation)
    ft = mFormation->getCurrentFormation();

  return mFormation->getStrategicPosition(
                                           mAgent->getPlayerNumber(),
                                           mBall->getGlobalPosition(),
                                           mAgentSettings->getFieldLength() / 2.0,
                                           ft
                                         );
}

/**
  This method checks whether in the current moment the agent can beam or not.
  @return True if the play mode is before kick off, false otherwise
*/
bool Klaus::WorldModel::canBeam() const
{
  return mAgent->getAgentModel() == LeggedSphere || mGameInfo.mPlayMode != BeforeKickOff ? false : true;
}


/*********************************************************************************/
/**************************** Testing purposes ***********************************/
/*********************************************************************************/

// #define TEST_WORLDMODEL  /*!< define this to test worldmodel */

#ifdef TEST_WORLDMODEL
int main()
{
  WorldModel wm(0);
  wm.setAgentModel(MODEL_NAO);

  return 0;
}
#endif // TEST_WORLDMODEL

