/*
*********************************************************************************
*          ConvexHull.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/17/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class definition for ConvexHull which handles generating convex    *
*            hull and it's related functions.                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file ConvexHull.cpp
<pre>
<b>File:</b>          ConvexHull.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/17/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class definition for ConvexHull which handles generating convex
            hull and it's related functions.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/17/2010       Mahdi           Initial version created
</pre>
*/

#include "ConvexHull.h"
#include <libZigorat/Logger.h> // needed for logger

namespace Klaus
{

/**
  Constructor of ConvexHull. Does nothing
*/
ConvexHull::ConvexHull()
{
}

/**
  Destructor of ConvexHull. Does nothing
*/
ConvexHull::~ConvexHull()
{
}

/**
  This method find the best point to start as the first point to derive the hull.
  @param points The sets of the points to choose the first point from
  @param target Target list of the points to add the found first point to.
  @return True if any point was found as the first point, false otherwise
*/
bool ConvexHull::deriveStartingPoint(VecPosition::Array & points, VecPosition::Array & target) const
{
  int index = -1;
  for(int i = 0; i < (int)points.size(); i++)
    if( index == -1 )
      index = i;
    else if( points[i][1] < points[index][1] )
      index = i;
    else if( points[i][1] == points[index][1] && points[i][0] < points[index][0] )
      index = i;

  if(index != -1)
  {
    target.push_back(points[index]);

    /// Shall not erase the first point so that it remains in the set and
    /// if it was chosen as the net point in any stages of generating the
    /// hull, it means that the hull generation is finished.
    return true;
  }

  return false;
}

/**
  This method gets the next point in the hull. It checks if the derived point
  is the first point in the set, then the hull has been generated.
  @param points Points from which the hull shall be extracted
  @param target The list which contains the hull points.
  @return True if the hull derivation is finished, false if it shall continue.
*/
bool ConvexHull::deriveNextPoint(VecPosition::Array & points, VecPosition::Array & target) const
{
  int size = target.size();

  /// First get the direction of the previous point
  VecPosition curPoint   = target[size - 1];
  VecPosition firstPoint = target[0];
  VecPosition lastPoint;
  Degree angPrev;

  if(target.size() < 2)
    angPrev = 0.0;
  else
  {
    lastPoint = target[size - 2];
    angPrev = curPoint.getDirectionFrom(lastPoint);
    angPrev.normalize360();
  }

  /// Now check for the point that has the minimum angle difference with the current point
  Degree angMin = 1000;
  int index = -1;
  size = points.size();
  for (size_t i = 0; (int)i < size; i++)
    if(points[i] != curPoint)
    {
      Degree ang = points[i].getDirectionFrom(curPoint) - angPrev;
      ang.normalize360();
      if(ang < angMin)
      {
        index = i;
        angMin = ang;
      }
    }

  if((index != -1) && (points[index] != firstPoint))
  {
    target.push_back(points[index]);
    points.erase(points.begin() + index);
    return true;
  }

  return false;
}

/**
  This method clears all the points in the class in order to create
  another convex hull from a different set of point
*/
void ConvexHull::clear()
{
  m_AllPoints.clear();
  m_ConvexPoints.clear();
}

/**
  This method adds a point to the set of the points which the convex
  hull shall be derived from
  @param pos The point to add to the set of points
*/
void ConvexHull::addPoint( const VecPosition & pos )
{
  m_AllPoints.push_back(pos);
}

/**
  This method calculates the hull. First finds out the starting point in the lower
  bottom corender of the shape and then start to iterate on all points to get the
  right neighbour vertex.
  @param target The ordered convex points will be copied in the target.
  @return True on successfull calculation, false otherwise
*/
bool ConvexHull::calculateHull( VecPosition::Array * target )
{
  if(target == NULL)
    target = &m_ConvexPoints;

  /// First clear the previously created hull
  target->clear();

  /// Then create the copy of the points
  VecPosition::Array points;
  for(VecPosition::Array::const_iterator it = m_AllPoints.begin(); it != m_AllPoints.end(); it++)
    points.push_back(*it);

  /// Derive the first point, and start based on that
  if(!deriveStartingPoint(points, *target))
  {
    Logger::getSingleton()->log(11, "Could not derive first point of the hull");
    return false;
  }

  /// Get all the next points
  while(deriveNextPoint(points, *target))
    ;

  return target->size() >= 3;
}

/**
  This method checks whether a given point lies within the convex hull or out of it.
  The algorithm is this: With the previous points which were used to derive the hull
  and the given point a new hull will be generated. If the point is in the newly
  created set of convex hull points then it had to lie out of the hull. Otherwise it
  is located in the hull.
  @param pos Location to test whether it is in the hull
  @return True if the given point is in the hull, false otherwise
*/
bool ConvexHull::isPointWithinHull(const VecPosition & pos)
{
  m_AllPoints.push_back(pos);

  /// Add the point the point set
  VecPosition::Array temp;

  /// Calculate another hull
  calculateHull(&temp);

  /// remove the point from the point set
  m_AllPoints.pop_back();

  /// Now check if it is in the generated hull or not
  for(VecPosition::Array::const_iterator it = temp.begin(); it != temp.end(); it++)
    if(*it == pos)
      return false;

  return true;
}

/**
  This method returns the iterator of the first point of the derived convex hull
  @return Iterator of the first point of the derived convex hull
*/
VecPosition::Array::const_iterator ConvexHull::begin() const
{
  return m_ConvexPoints.begin();
}

/**
  This method returns the iterator of the last point of the derived convex hull
  @return Iterator of the last point of the derived convex hull
*/
VecPosition::Array::const_iterator ConvexHull::end() const
{
  return m_ConvexPoints.end();
}



}; // end namespace Klaus;
