/*
*********************************************************************************
*           Kinematics.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/09/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Kinematic type infromation and utilities                           *
*                                                                               *
*********************************************************************************
*/

/*! \file Kinematics.h
<pre>
<b>File:</b>          Kinematics.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/09/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Kinematic type infromation and utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/09/2011       Mahdi           Initial version created
</pre>
*/


#include <libZigorat/PlatformMacros.h> // needed for _DLLExportControl
#include <libZigorat/Algebra.h>        // needed for VecPosition, Quaternion
#include <map>                         // needed for std::map

namespace Klaus
{

  /**
    This structure holds dynamic properties of a physical body. This is used
    to gather information of all bodies of a single scene (or robot)
  */
  struct _DLLExportControl BodyInfo
  {
    Matrix4     mTransform;           /*!< Transform in global coordinates */
    VecPosition mLinearVelocity;      /*!< Linear velocity in global coordinates*/
    double      mMass;                /*!< Mass of the body */

    BodyInfo    ( const Matrix4 & tr    );
    BodyInfo    ( const BodyInfo & info );
    BodyInfo    (                       );
  };


  /*! This type is used i providing kinematics information for bodies which
      is used in forward and inverse kinematics calculations. */
  typedef _DLLExportControl std::map<std::string, BodyInfo> Kinematics;


  /**
    Contains information of a joint
  */
  struct _DLLExportControl JointInformation
  {
    Degree      mAngles[2];   /*!< Angle of motors */
    Degree      mSpeeds[2];   /*!< Angular speed of motors */
    VecPosition mPivot;       /*!< Pivot position of the joint */
  };

  /*! This holds all the information needed to utilize inverse kinematics */
  typedef _DLLExportControl std::map<std::string, JointInformation> InverseKinematics;


  /*! Core of the Kinematics calculation. Calculates CoM, ZMP and inverse kinematics.
      Forward kinematics is calculated purely based on the scene graph, so it is
      not included here.
  */
  class _DLLExportControl Physics
  {
    public:
      static VecPosition getCenterOfMass     ( const Kinematics & sheet );
      static VecPosition getZeroMomentumPoint( const Kinematics & sheet );
  };

}; // end namespace Klaus
