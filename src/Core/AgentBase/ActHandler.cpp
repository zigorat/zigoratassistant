/*
*********************************************************************************
*         ActHandler.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definitions for Acthandler which handles  *
*            command sending to simulation server                               *
*                                                                               *
*********************************************************************************
*/

/*! \file ActHandler.cpp
<pre>
<b>File:</b>          ActHandler.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definitions for Acthandler
               which handles command sending to server.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#include "ActHandler.h"
#include <libZigorat/Logger.h>
#include <sstream>               // needed for stringstream

/*! ActHandler singleton instance */
template<> Klaus::ActHandler* Klaus::Singleton<Klaus::ActHandler>::mInstance = 0;


/**
  This is the constructor for the ActHandler class. All the variables are
  initialized.
*/
Klaus::ActHandler::ActHandler()
{
  mConnection = SimSpark::getSingleton();
  emptyQueue();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::ActHandler * Klaus::ActHandler::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new ActHandler;
    Logger::getSingleton()->log(1, "(ActHandler) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::ActHandler::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(ActHandler) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(ActHandler) removed singleton");
  }
}

/**
  This method puts a SoccerCommand in the queue. The added commands will
  be sent to the soccerserver when the method sendCommands is performed.
  Normally this is done when thinkin' finishes. This method also checks
  if the command is subject to a joint which previously in the cycle had
  a command assigned to it. If this is the case the previous command will
  be updated with new parameters otherwise the command will just be added
  to the list of commands.
  @param cmd SoccerCommand that should be put in the queue.
  @return always true when command is added
*/
bool Klaus::ActHandler::addCommand(const SoccerCommand & cmd)
{
  if(cmd.mCommandType == CommandSay)
    mSayCommand = cmd;
  else if(cmd.mCommandType == CommandBeam)
    mBeamCommand = cmd;
  else if(cmd.mCommandType == CommandInit)
    mInitCommand = cmd;
  else if(cmd.mCommandType == CommandJoint)
  {
    // first search if a joint command with the joint which this command changes exists
    // in the list or not. If it exists it shall be updated, otherwise it will be added
    // to the list.
	  for(size_t i = 0; i < mJointCommands.size(); ++i)
      if(mJointCommands[i].mEffector == cmd.mEffector)
      {
        mJointCommands[i] = cmd;
        return true;
      }

    mJointCommands.push_back(cmd);
  }
  else
    return false;

  return true;
}

/**
  This method empties the queue in which all the commands are stored.
*/
void Klaus::ActHandler::emptyQueue()
{
  mSayCommand.clear();
  mBeamCommand.clear();
  mJointCommands.clear();
  mInitCommand.clear();
}

/**
  This method converts all commands in the queue to text strings and sends
  these text strings to the server (connected by Connection). This  method
  is normally called after agent has thaught what to do, at the end of main
  loop
  @return true when sending of messages succeeded, false otherwise
*/
bool Klaus::ActHandler::sendCommands()
{
  std::stringstream strMsg;
  std::string strCmd;

  for(size_t i = 0; i < mJointCommands.size(); ++i)
    if(mJointCommands[i].getCommandString(strCmd))
      strMsg << strCmd;

  if(mSayCommand.getCommandString(strCmd))
    strMsg.write(strCmd.data(), strCmd.size());

  if(mBeamCommand.getCommandString(strCmd))
    strMsg << strCmd;

  if(mInitCommand.getCommandString(strCmd))
    strMsg << strCmd;

  // send the accumulated message and empty the queue
  sendMessage(strMsg.str());
  emptyQueue();

  return true;
}

/**
  This method sends a single command directly to the server. First a string
  is made from the SoccerCommand and afterwards this string is send to the
  server using the method sendMessage.
  @param soc SoccerCommand that should be send to the server.
  @return true when message was sent, false otherwise
*/
bool Klaus::ActHandler::sendCommand(const SoccerCommand & soc) const
{
  std::string strCommand;

  if(soc.getCommandString(strCommand))
    return sendMessage(strCommand);
  else
    return false;
}

/**
  This method sends a single string directly to the server.
  @param strMsg string that should be sent to the server
  @return true when message was sent, false otherwise
*/
bool Klaus::ActHandler::sendMessage(const std::string & strMsg) const
{
  if(mConnection)
    return mConnection->sendCommand(strMsg);

  std::cerr << "(Klaus::ActHandler::sendMessage) No connection to simulator" << std::endl;
  return false;
}
