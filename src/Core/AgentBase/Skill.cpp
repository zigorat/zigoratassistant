/*
*********************************************************************************
*              Skill.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 02/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for implementing skills for robots.             *
*                                                                               *
*********************************************************************************
*/

/*! \file Skill.cpp
<pre>
<b>File:</b>          Skill.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       02/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for implementing skills for robots.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
02/24/2011       Mahdi           Initial version created
</pre>
*/

#include "Skill.h"
#include "Objects.h"  // needed for AgentObject


/**
  Constructor of Skill. Initializes singleton instances
  and creates all needed PIDControllers.
*/
Klaus::Skill::Skill()
{
  mLog   = Logger::getSingleton();
  mAct   = ActHandler::getSingleton();
  mWM    = WorldModel::getSingleton();
  mGraph = NULL;
}

/**
  This method evaluates joint speeds from all calculated joint
  angles and sends them via the action handler.
*/
void Klaus::Skill::sendActions()
{
  for(PIDMap::iterator it = mPIDs.begin(); it != mPIDs.end(); ++it)
  {
    const RSG2::Joint * joint = mWM->getJoint(it->first);
    Degree angTurn = mState[it->first].mAngles[0];
    angTurn.restrict(joint->getMinAngle(), joint->getMaxAngle());
    angTurn = it->second->calculateNeededMV(joint->getAngle().getDegree(), angTurn.getDegree(), mWM->getTimeStep());
    mAct->addCommand(SoccerCommand(joint->getEffector()->getName(), angTurn.getRadian()));
  }
}

/**
  This method is called to initialize the subclassed skill if needed.
*/
void Klaus::Skill::initializeInternal()
{
}

/**
  This method initializes skill by getting the instance of agent scene
  graph and initializing the agent state. After that the skill's own
  initialization procedure will be called.
*/
void Klaus::Skill::initialize()
{
  mGraph = AgentObject::getSingleton()->getSceneGraph();
  mGraph->getInverseKinematics(mState);

  initializeInternal();
}

/**
  This method executes the skill. First it updates the internal state of robot and
  then the skill's operation will be done. In the end all the joint changes will be
  sent to simulator.
*/
void Klaus::Skill::execute()
{
  mGraph->getInverseKinematics(mState);
  executeInternal();
  sendActions();
}
