/*
*********************************************************************************
*               Skill.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 02/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for implementing skills for robots.             *
*                                                                               *
*********************************************************************************
*/

/*! \file Skill.h
<pre>
<b>File:</b>          Skill.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       02/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for implementing skills for robots.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
02/24/2011       Mahdi           Initial version created
</pre>
*/

#ifndef SKILLS
#define SKILLS


#include <SceneGraphSystem/Graph.h>  // needed for RSG2::Graph
#include <libZigorat/Logger.h>       // needed for Logger
#include "ActHandler.h"              // needed for ActHandler
#include "WorldModel.h"              // needed for WorldModel
#include "PIDController.h"           // needed for PIDController

namespace Klaus
{

  /**
    This class is a base for creating skills. When a skill's execution
    is done, The agent's state will be updated and executeInternal will
    be called. Afterwards the newly generated state will be sent to ActHandler.
  */
  class _DLLExportControl Skill
  {
    private:
      typedef std::map<std::string, PIDController*> PIDMap;

    protected:
      InverseKinematics   mState;           /*!< Evaluated state of the Skill. */
      PIDMap              mPIDs;            /*!< PID Controllers for all joints */
      Logger            * mLog;             /*!< Logger instance */
      RSG2::Graph       * mGraph;           /*!< Agent's scene graph instance */
      ActHandler        * mAct;             /*!< ActionHandler instance to send commands to */
      WorldModel        * mWM;              /*!< WorldModel instance */

      void                sendActions       (                );
      virtual void        initializeInternal(                );
      virtual void        executeInternal   (                ) = 0;

    public:
                          Skill             (                );

      void                initialize        (                );
      void                execute           (                );
  };

}; // end namespace Klaus


#endif // SKILS
