/*
*********************************************************************************
*           SimSpark.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/21/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: SimSpark connection manager.                                       *
*                                                                               *
*********************************************************************************
*/

/*! \file SimSpark.cpp
<pre>
<b>File:</b>          SimSpark.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/21/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      SimSpark connection manager
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/21/2011       Mahdi           Initial version created
</pre>
*/

#include "SimSpark.h"
#include <cstring>    // needed for strlen

/*! SimSpark connection manager singleton instance */
template<> Klaus::SimSpark* Klaus::Singleton<Klaus::SimSpark>::mInstance = 0;

/**
  SimSpark constructor to create and initialize connection
  to simulation server
*/
Klaus::SimSpark::SimSpark() : Connection(Klaus::TCP)
{
  mLog = Logger::getSingleton();
}

/**
  Destructor of SimSpark connection manager. Does nothing
*/
Klaus::SimSpark::~SimSpark()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::SimSpark * Klaus::SimSpark::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new SimSpark;
    Logger::getSingleton()->log(1, "(SimSpark) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::SimSpark::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(SimSpark) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(SimSpark) removed singleton");
  }
}

/**
  This method connects to a remote or local simulator
  @param strHost Host address
  @param iPort Host port number
  @return True on successfull connect, false otherwise
*/
bool Klaus::SimSpark::connect(const char * strHost, int iPort)
{
  return openConnection(strHost, iPort);
}

/**
  This method is the only part needed for sending commands to
  simulation server, when a connection is ready
  @param msg message to send to the server
  @return bool whether command was sent or not
*/
bool Klaus::SimSpark::sendCommand(const std::string & msg) const
{
  if(!connected())
    return false;

  // prefix the message with it's payload length
  uint32 len = htonl((long)msg.size());
  std::string str((const char*)&len, sizeof(uint32));
  str.append(msg, 0, msg.size());

  mLog->log(40, "Agent: \"%s\"", msg.data());
  return sendData(str.data(), str.size()) > 0;
}


/**
  This method attempts to read just the specified amount of data from
  connection. Nor less than specified, neighter more than specified.
  @param buf The buffer to put the received data to
  @param size Size of the needed data
  @return True if connection remained valid, false otherwise
*/
bool Klaus::SimSpark::getSizedData(char *buf, int size) const
{
  int bytesRead = 0;
  while(bytesRead < size)
  {
    int readResult = mSocket->recvMessage(buf+bytesRead, size - bytesRead);

    // If any errors occured, then get out
    if(readResult < 0)
      return false;

    bytesRead += readResult;
  }

  return true;
}

/**
  This method is used to get the simulator's messages
  @param msg place to write revieved message
  @return bool indicating whether any message is recieved
*/
bool Klaus::SimSpark::getMessage(std::string & msg) const
{
  const unsigned iBufSize = 16 * 1024;
  static char buffer[iBufSize];

  // msg is prefixed with it's total length, first recv it's length
  // Includes -1 and 0, means that on error and timeouts application would quit
  if(!getSizedData(buffer, sizeof(uint32)))
    return false;

  // analyse the length
  uint32 msgLen = ntohl(*(uint32*)buffer);

  // If the received length of the message is more than size of the big buffer, this indicates a serious error
  if(msgLen > iBufSize - 3)
  {
    Logger::getSingleton()->log(40,
         "(SimSpark::getMessage) Message size more than BIG buffer: %d", msgLen);
    std::cerr << "(SimSpark::getMessage) Message size more than BIG buffer: " << msgLen << std::endl;

    // Clear the message so that WorldModel won't break
    msg = "()";
    return true;
  }

  buffer[0] = '(';
  // Now get the message
  if(!getSizedData(buffer+1, msgLen))
    return false;

  // zero terminate the received data
  *(buffer + msgLen + 1) = ')';
  *(buffer + msgLen + 2) = '\0';
  msg.assign(buffer, msgLen + 3);

  mLog->log(40, "Simulator: \"%s\"", msg.c_str());
  return true;
}


