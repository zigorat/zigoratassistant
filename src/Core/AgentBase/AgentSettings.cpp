/*
*********************************************************************************
*         AgentSettings.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 08/01/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains AgentSettings, a utility to load and use agent  *
*            configurations and thresholds                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file AgentSettings.cpp
<pre>
<b>File:</b>          AgentSettings.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/01/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains AgentSettings, a utility to load and use agent
                        configurations and thresholds
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/01/2007       Mahdi           Initial version created
</pre>
*/

#include <iostream>
#include <fstream>
#include <deque>

#include "AgentSettings.h"
#include <libZigorat/Logger.h>              // needed for Logger
#include <libZigorat/ApplicationSettings.h> // needed for ApplicationSettings


/*! AgentSettings singleton instance */
template<> Klaus::AgentSettings* Klaus::Singleton<Klaus::AgentSettings>::mInstance = 0;


/**
  This the constrcutor of AgentSettings, initializes all the configurations
  and thresholds with default values
*/
Klaus::AgentSettings::AgentSettings()
{
  mAgentModel               = Nao;

  mWalkingMutationRateGenes =  0.02;
  mWalkingMutationRate      =  0.20;
  mWalkingPopulation        =    50;
  mWalkingStepTest          =     1;
  mWalkingWhenToTurn        =    30;
  mPlayerConfThr            =  0.94;
  mPlayerHighConfThr        =  0.84;
  mTraining                 =  true;
  mFieldLength              = 18.00;
  mFieldWidth               = 12.00;
  mGoalWidth                =  2.10;
  mGoalHeight               =  0.80;
  mFlagHeight               = 0.375;
  mMaxSaySize               = 10;
}

/**
  Regular destructor for AgentSettings
*/
Klaus::AgentSettings::~AgentSettings()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::AgentSettings * Klaus::AgentSettings::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new AgentSettings;
    Logger::getSingleton()->log(1, "(AgentSettings) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::AgentSettings::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(AgentSettings) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(AgentSettings) removed singleton");
  }
}

/**
  This method parses a given token as a setting for agent.
  @param token Token containing the setting value
  @return True on successful parse, false otherwise
*/
bool Klaus::AgentSettings::parseToken(SToken token)
{
  if(token[0] == "training")
    mTraining = token[1].boolean();
  else if(token[0] == "walking_mutation_rate_genes")
    mWalkingMutationRateGenes = token[1].number();
  else if(token[0] == "walking_mutation_rate")
    mWalkingMutationRate = token[1].number();
  else if(token[0] == "walking_crossover_rate_genes")
    mWalkingXOverRateGenes = token[1].number();
  else if(token[0] == "walking_crossover_rate")
    mWalkingXOverRate = token[1].number();
  else if(token[0] == "walking_step_test")
    mWalkingStepTest = Round(token[1].number());
  else if(token[0] == "walking_population")
    mWalkingPopulation = Round(token[1].number());
  else if(token[0] == "walking_when_to_turn")
    mWalkingWhenToTurn = token[1].number();
  else if(token[0] == "player_conf_thr")
    mPlayerConfThr = token[1].number();
  else if(token[0] == "player_high_conf_thr")
    mPlayerHighConfThr = token[1].number();
  else if(token[0] == "field_length")
    mFieldLength = token[1].number();
  else if(token[0] == "field_width")
    mFieldWidth = token[1].number();
  else if(token[0] == "goal_width")
    mGoalWidth = token[1].number();
  else if(token[0] == "goal_height")
    mGoalHeight = token[1].number();
  else if(token[0] == "flag_height")
    mFlagHeight = token[1].number();
  else if(token[0] == "max_say_size")
    mMaxSaySize = Round(token[1].number());
  else if(token[0] == "agent_model")
  {
    AgentModelType am = SoccerTypes::getAgentModelFromString(token[1].str());
    if(am == UnknownModel)
    {
      Logger::getSingleton()->log(32, "(AgentSettings) Unknown agent model '%s'", token[1].str().c_str());
      return false;
    }
    else
      mAgentModel = am;
  }
  else
    return false;

  return true;
}

/**
  This method loads configurations from the supplied file
  @param strFileName File name and addresss to load values from
  @return Indicates loading was successfull or not
*/
bool Klaus::AgentSettings::loadFromFile( const std::string & strFileName )
{
  // load the file
  try
  {
    SExpression file(strFileName, 4*1024);

    int start = file.getFirstNonComment();
    if(start == -1)
    {
      Logger::getSingleton()->log(32, "(AgentSettings) error reading file '%s'", strFileName.c_str());
      return false;
    }

    SToken tokens = file[(size_t)start];
    for(size_t i = 0; i < tokens.size(); ++i)
      if(tokens[i].isComment())
        continue;
      else if(tokens[i].isNormal() && parseToken(tokens[i]))
        continue;
      else
        Logger::getSingleton()->log(32, "(AgentSettings) unknown configuration or threshold '%s'", tokens[i].str().c_str());
  }
  catch(SLangException e)
  {
    Logger::getSingleton()->log(32, "(AgentSettings) error loading '%s'", strFileName.c_str());
    Logger::getSingleton()->log(32, "%s", e.str());
    return false;
  }

  return true;
}

/**
  This method saves configurations and properties to the file specified
  @param strFileName File name and address to save information to
*/
void Klaus::AgentSettings::saveToFile( const std::string & strFileName )
{
  std::ofstream file( strFileName.c_str() );

  file << "\n;\n; Agent Configuration and thresholds\n; Zigorat 3D Soccer Simulation Team\n;\n\n" << std::endl;
  file << "(\n";

  // Here goes the variables in this format: file << "  (<variable_name> " << <value> << ")\n";
  file << "  (agent_model " << SoccerTypes::getAgentModelString(mAgentModel) << ")\n\n";

  file << "  (walking_mutation_rate_genes " << mWalkingMutationRateGenes << ")\n";
  file << "  (walking_mutation_rate " << mWalkingMutationRate << ")\n";
  file << "  (walking_crossover_rate_genes " << mWalkingXOverRateGenes << ")\n";
  file << "  (walking_crossover_rate " << mWalkingXOverRate << ")\n";
  file << "  (walking_step_test " << mWalkingStepTest << ")\n";
  file << "  (walking_population " << mWalkingPopulation << ")\n";
  file << "  (walking_when_to_turn " << mWalkingWhenToTurn << ")\n";
  file << "  (player_conf_thr " << mPlayerConfThr << ")\n";
  file << "  (player_high_conf_thr " << mPlayerHighConfThr << ")\n";
  file << "  (field_length " << mFieldLength << ")\n";
  file << "  (field_width " << mFieldWidth << ")\n";
  file << "  (goal_width " << mGoalWidth << ")\n";
  file << "  (goal_height " << mGoalHeight << ")\n";
  file << "  (flag_height " << mFlagHeight << ")\n";
  file << "  (max_say_size " << mMaxSaySize << ")\n";
  file << "  (training " << (mTraining ? "yes" : "no") << ")\n";

  file << ")\n\n;\n; End of file\n;\n";

  file.close();
}

/**
  This method returns the agent model to use and parse
  @return Agent Model to load from
*/
Klaus::AgentModelType Klaus::AgentSettings::getAgentModel() const
{
  return mAgentModel;
}

/**
  This method returns the mutation rate for agent's walking procedure used via GA
  This property only has effect on the genes of organisms selected for mutation
  @return Walking Mutation Rate for genes
*/
double Klaus::AgentSettings::getWalkingMutationRateGenes() const
{
  return mWalkingMutationRateGenes;
}

/**
  This method returns the mutation rate for agnet's walking procedure used via GA
  @return Walking Mutation Rate
*/
double Klaus::AgentSettings::getWalkingMutationRate() const
{
  return mWalkingMutationRate;
}

/**
  This method returns the crossover rate for agent's walking procedure used via GA
  This property only has effect on the genes of organisms selected for crossover
  @return Walking CrossOver Rate for genes
*/
double Klaus::AgentSettings::getWalkingXOverRateGenes() const
{
  return mWalkingXOverRateGenes;
}

/**
  This method returns the crossover rate for agnet's walking procedure used via GA
  @return Walking CrossOver Rate
*/
double Klaus::AgentSettings::getWalkingXOverRate() const
{
  return mWalkingXOverRate;
}

/**
  This method returns number of steps needed for agent to walk before fitness evaluation
  @return number of steps need for fitness evaluation
*/
int Klaus::AgentSettings::getWalkingStepTest() const
{
  return mWalkingStepTest;
}

/**
  This method returns walkings population per evolution
  @return Walking population
*/
int Klaus::AgentSettings::getWalkingPopulation() const
{
  return mWalkingPopulation;
}

/**
  This method returns player confidence threshold which below that
  the seen player's information will be considered too old
  @return Player confidence threshold
*/
double Klaus::AgentSettings::getPlayerConfThr() const
{
  return mPlayerConfThr;
}

/**
  This method returns the high confidence threshold of a players information
  @return High confidence of player
*/
double Klaus::AgentSettings::getPlayerHighConfThr() const
{
  return mPlayerHighConfThr;
}

/**
  This method returns the angle which should agent consider
  for turning when walking towards a point
  @return Walking angle for turning threshold
*/
Klaus::Degree Klaus::AgentSettings::getWalkingWhenToTurn() const
{
  return mWalkingWhenToTurn;
}

/**
  This method returns current state of agent for acting: Being trained or used
  @return current state of agent
*/
bool Klaus::AgentSettings::isTraining() const
{
  return mTraining;
}

/**
  This method returns the length of soccer field. Since it is not sent y
  simulator and it shall not be calculated, because of vision error, the
  only option is to hand code it.
  @return Soccer Field Length
*/
double Klaus::AgentSettings::getFieldLength() const
{
  return mFieldLength;
}

/**
  This method returns the width of soccer field. Since it is not sent by
  simulator and it shall not be calculated, because of vision error, the
  only option is to hand code it.
  @return Soccer Field Width
*/
double Klaus::AgentSettings::getFieldWidth() const
{
  return mFieldWidth;
}

/**
  This method returns gaol width of the simulator soccer field. Since the
  value is not sent by the simulator and agent's vision is accompanied with
  errors, best option is to hand code it.
  @return Goal width of soccer field
*/
double Klaus::AgentSettings::getGoalWidth() const
{
  return mGoalWidth;
}

/**
  This method returns gaol height of the simulator soccer field. Since the
  value is not sent by the simulator and agent's vision is accompanied with
  errors, best option is to hand code it.
  @return Goal height of soccer field
*/
double Klaus::AgentSettings::getGoalHeight() const
{
  return mGoalHeight;
}

/**
  This method returns the flags height. This values is not sent via simulator.
  @return Height of the corner flags
*/
double Klaus::AgentSettings::getFlagHeight() const
{
  return mFlagHeight;
}

/**
  This method returns maximum number of characters acceptable in a say effector.
  @return Maximum number f acceptable characters in say messages.
*/
int Klaus::AgentSettings::getMaxSaySize() const
{
  return mMaxSaySize;
}
