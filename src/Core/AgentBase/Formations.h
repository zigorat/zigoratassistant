/*
*********************************************************************************
*           Formations.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 09/30/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class declerations for PlayerTypeInfo,FormationTypeInfo,Formations *
*            utilities for formations manipulation                              *
*            This file is based upon UvA simulation team by                     *
*              Jelle Kok and Remco deBoer                                       *
*                                                                               *
*********************************************************************************
*/

/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file Formations.h
<pre>

<b>File:</b>          Formations.h
<b>Project:</b>       Robocup Soccer Simulation Team: UvA Trilearn
<b>Authors:</b>       Jelle Kok
<b>Created:</b>       05/02/2001
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Header file for different classes associated with
               Formations.

- PlayerTypeInfo contains the different information for
one playertype (PT_DEFENDER, PT_ATTACKER, etc.). These
should not be confused with the player_types used in the
soccerserver from version 7.xx.  The information consists
of the attraction to the ball, minimal and maximal x
coordinates, etc.
- FormationTypeInfo contains information for all the
roles in one specific formation. This information
consists of the current formation, the home position for
all the roles, the player_type (PT_DEFENDER, PT_ATTACKER,
etc.) for all the roles and the information about all
player_types
- Formations itself. This class contains all the
information of the different Formations. Furthermore it
contains the current formation that is used and the role
in the formation that is associated with the current
agent.

This formation system is based on the formations used by
the simple FC Portugal team, released in 2000.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/02/2001       Jelle Kok       Initial version created
</pre> */

#ifndef FORMATIONS
#define FORMATIONS

#include <libZigorat/Singleton.h> // needed for Singleton
#include <libZigorat/SLang.h>     // needed for SToken, SExpression
#include <libZigorat/Logger.h>    // needed for Logger
#include "SoccerTypes.h"          // needed for ObjectT, VecPosition

#define MAX_PLAYERS 11 /*!< Maximum number of players at each side */


namespace Klaus
{

  /*! The RoleType enumeration contains the different role types for
      players in a formation. The role denotes the kind of player
      (midfielder, attacker) on the field.
  */
  enum RoleType
  {
    Role_Goalie,          /*!< goalkeeper          */
    Role_DefenderCentral, /*!< central defender    */
    Role_DefenderSweeper, /*!< sweeper defender    */
    Role_DefenderWing,    /*!< wing defender       */
    Role_HalfbackCentral, /*!< central midfielder  */
    Role_HalfbackWing,    /*!< wing midfielder     */
    Role_AttackerCentral, /*!< central attacker       */
    Role_AttackerWing,    /*!< wing attacker    */
    UnknownRole           /*!< unknown role */
  };


  /*! The PlayerSetT enumeration contains different sets of playertypes that
      are defined in a formation. Possible sets are all midfielders, etc. */
  enum PlayerSetT
  {
    Set_Defenders,    /*!< all defenders   */
    Set_Halfbacks,    /*!< all midfielders */
    Set_Attackers,    /*!< all attackers   */
    Set_AllPlayers    /*!< all players     */
  } ;


  /*! The FormationType enumeration contains the different formation
      types that are defined for playing. */
  enum FormationType
  {
    Formation_Initial_Defensive,   /*!< initial formation type (defensive one ) */
    Formation_Initial_Offensive,   /*!< initial formation type (offensive one ) */
    Formation_433_Offensive,       /*!< 433 offensive formation                 */
    UnknownFormation     /*!< Maximum formation                       */
  };


  /*! This class contains information for positioning of one individual player.
      The needed information is as follows:
        - x coordinate of the home position of the player
        - y coordinate of the home position of the player
        - x attraction to the ball for the player
        - y attraction to the ball for the player.
        - minimal x coordinate which this player can go to
        - maximal x coordinate which this player can go to
        - bBehindBall - indicating whether this player type should always stay behind
          the ball or not.

      This class contains different get and set methods to change the values
      associated for this class, normally these are changed when the
      Formations class reads in the formation file.
  */
  class PlayerPositioning
  {
    private:
      RoleType    mRole;              /*!< Role of the player in the formation */
      VecPosition mHome;              /*!< home position of the player, the position of player with ball at origin */
      VecPosition mAttraction;        /*!< attraction of the player to ball */
      Rect        mConstraints;       /*!< Clip in which the player can be positioned. */
      bool        mBehindBall;        /*!< should player always stay behind the ball */
      int         mPlayerNumber;      /*!< Number of the player */

      bool        parse               ( SToken token                );

    public:
                  PlayerPositioning   (                             );

      bool        load                ( SToken token                );

      // standard get and set methods to individually set all the values
      int         getPlayerNumber     (                             ) const;
      RoleType    getPlayerRole       (                             ) const;
      VecPosition getHomePosition     (                             ) const;
      VecPosition getAttraction       (                             ) const;
      Rect        getConstraints      (                             ) const;
      bool        isBehindBall        (                             ) const;

      // strategic position retrieval
      VecPosition getStrategicPosition( const VecPosition & posBall ) const;

      std::string str                 ( const std::string &gap = "" ) const;

      /*! Array represents a dynamic array of player positionings */
      typedef std::vector<PlayerPositioning*> Array;
  };


  /*! This class contains information about one specific formation. It
      contains the formation type and information of all players in the
      formation. Furthermore it contains methods to retrieve this information
      for a specific player.
  */
  class Formation
  {
    private:
      FormationType            mFormation;         /*!< type of the formation */
      PlayerPositioning::Array mPlayers;           /*!< player informations for the formation */
      Logger                 * mLog;               /*!< Logger instance */

      PlayerPositioning      * getPlayerInstance   ( const int & iNumber         ) const;
      void                     clear               (                             );
      bool                     parse               ( SToken token                );

    public:
                               Formation           (                             );
                             ~ Formation           (                             );

      bool                     load                ( SToken token                );

      // get and set methods to get information for a player in this formation
      FormationType            getFormationType    (                             ) const;
      VecPosition              getStrategicPosition( const VecPosition & posBall,
                                                     const int & iPlayerNumber   ) const;

      std::string              str                 ( const std::string &gap = "" ) const;

      // static methods to use formation type strings
      static std::string       getFormationStr     ( const FormationType & t     );
      static FormationType     getFromationFromStr ( const std::string & str     );

      /*! Array is a dynamic array of Formations.*/
      typedef std::vector<Formation*> Array;
  };


  /*! This class is a container for all different Formation Types: it
      contains the information of all the formation types. Furthermore it
      contains two other values: the current formation type that is used
      by the agent and the number of the agent in the current formation.
      These two values fully specify the strategic position of the player
      in the field.
  */
  class FormationSystem : public Singleton<FormationSystem>
  {
    private:
      Formation              * mFormations[UnknownFormation]; /*!< All registered formations */
      FormationType            mCurFormation;                 /*!< Index of the current formation */
      Logger                 * mLog;                          /*!< Logger instance */

      void                     clear               (                             );
      bool                     load                ( SToken token                );

                               FormationSystem     (                             );
                             ~ FormationSystem     (                             );

    public:
      static FormationSystem * getSingleton        (                             );
      static void              freeSingleton       (                             );

      bool                     initialize          ( const std::string & strFile,
                                                     FormationType initForm      );

      // standard get and set methods for the different member variables
      FormationType            getCurrentFormation (                             ) const;
      void                     setCurrentFormation ( FormationType formation     );

      // method to get the strategic position depending on different factors
      VecPosition              getStrategicPosition( int           iPlayer,
                                                     VecPosition   posBall,
                                                     double        dMaxX,
                                                     FormationType ft            );

      std::string              str                 (                             ) const;
  };


}; // end namespace Klaus


#endif
