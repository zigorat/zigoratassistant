/*
*********************************************************************************
*        PIDController.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 03/17/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class definition for PIDController which controls and handles      *
*            joint manipulation                                                 *
*                                                                               *
*********************************************************************************
*/

/*! \file PIDController.cpp
<pre>
<b>File:</b>          PIDController.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/17/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class definition for PIDController which controls and handles
            joint manipulation.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/17/2010       Mahdi           Initial version created
</pre>
*/

#include "PIDController.h"

/**
  Constructor of PIDController. Initializes default values
*/
Klaus::PIDController::PIDController()
{
  mKp            =   1;
  mKi            =   1;
  mKd            =   1;
  mIMin          = -10;
  mIMax          =  10;

  mLastError     =   0;
  mIntegralError =   0;
}

/**
  Constructor of PIDController. Initializes the gains
  @param p Proportional gain
  @param i Integral gain
  @param d Derivative gain
  @param min Integrator minimum state
  @param max Integrator maximum state
*/
Klaus::PIDController::PIDController(const double &p, const double &i, const double &d, const double &min, const double &max)
{
  mKp            = p;
  mKi            = i;
  mKd            = d;
  mIMin          = min;
  mIMax          = max;

  mLastError     = 0;
  mIntegralError = 0;
}

/**
  Destructor of PIDController
*/
Klaus::PIDController::~PIDController()
{
}

/**
  This method resets the controller.
*/
void Klaus::PIDController::reset()
{
  mLastError = 0.0;
  mIntegralError = 0.0;
}

/**
  This method calculates manipulated variable of the system from the given
  process variable, setpoint and previous states of the system.
  @param pv Process Variable
  @param setpoint Setpoint to reach
  @param dLastDuration simulation step for adjusting integrator error value
  @return Manipulated variable's value
*/
double Klaus::PIDController::calculateNeededMV(const double &pv, const double & setpoint, const double & dLastDuration)
{
  double out_p, out_i, out_d;
  const double error = setpoint - pv;

  // Proportional MV
  out_p = mKp * error;

  // Integral MV
  // take the time gap in account
  mIntegralError += (error * dLastDuration);
  if(mIntegralError < mIMin)
    mIntegralError = mIMin;
  else if(mIntegralError > mIMax)
    mIntegralError = mIMax;
  out_i = mKi * mIntegralError;

  // Derivative MV
  out_d = mKd * (error - mLastError);
  mLastError = error;

  return out_p + out_i - out_d;
}
