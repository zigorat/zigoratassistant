/*
*********************************************************************************
*           WorldModel.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the agents memory of outside world                         *
*                                                                               *
*********************************************************************************
*/

/*! \file WorldModel.h
<pre>
<b>File:</b>          WorldModel.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the agents memory of outside world
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#ifndef WORLD_MODEL
#define WORLD_MODEL


#include <libZigorat/Logger.h>  // needed for Logger
#include "Objects.h"            // needed for AgentObject, Ball, Flag and PlayerObject
#include "AgentSettings.h"      // needed for AgentSettings
#include "Formations.h"         // needed for FormationSystem

namespace Klaus
{

  /*! The Class WorlModel contains all the RoboCup information that is available
      on the field. It contains information about the players, ball, flags and
      lines. Furthermore it contains methods to extract useful information.
      The (large amount of) attributes can be separated into different groups:
        - Environmental information: specific information about the soccer server
        - Match information: general information about the current state of a match
        - Object information: all the objects on the soccer field
        - Action information: actions that the agent has performed

      The methods can also be divided into different groups:
        - Retrieval methods: directly retrieving information of objects
        - Update methods: update world based on new sensory information
        - Prediction methods: predict future states based on past perceptions
        - High-Level methods: deriving high-level conclusions from basic world state
  */
  class WorldModel : public Singleton<WorldModel>
  {
    private:
      /*! This structure contains Information about the field, like goal width and height, etc.*/
      struct FieldInformation
      {
        double    mGoalWidth;   /*!< Goal width total    */
        double    mGoalHeight;  /*!< Goal height total   */
        double    mFieldLength; /*!< Field length        */
        double    mFieldWidth;  /*!< Field width         */
        double    mFlagHeight;  /*!< Height of the flags */
      };

      /*! This structure contains information related to the game. These information include:
            - Simulator Time: Current time of simulator itslef
            - Game Time : The game time. This differs from simulator time, in the way
                that game may be stopped, but robot can still move, like when the ball has gone out of field
            - Current cycle of game: With each time message (which indicates a new cycle) this counter increases one
            - Simulator step: Difference in real time between two consequent cycles of simulator
            - Play mode: current play mode of the game, eg: Play on, throw in, etc.
            - Side Kick off: Side which should kick off the game
      */
      struct GameInformation
      {
        Time     mSimulatorTime;   /*!< Simulator time in seconds      */
        Time     mGameTime;        /*!< Game time in seconds           */
        Time     mTimeLastMsg;     /*!< Last time any message arrived  */
        Time     mTimeLastSeeMsg;  /*!< Last time see message arrived  */
        Time     mTimeLastHearMsg; /*!< Last time hear message arrived */

        size_t   mCurrentCycle;    /*!< Current cycle of simulation    */
        double   mSimulatorStep;   /*!< Simulator Step in seconds      */

        PlayMode mPlayMode;       /*!< Current play mode               */
        SideType mTeamSide;       /*!< Team side on simulation start   */
        SideType mSideKickOff;    /*!< Side to kick off                */

        bool     mInitOK;         /*!< Has server sent ok on init      */
      };


      /*! This structure holds all the information of a hear message.
          These are the relative direction of talk, the player who
          have talked and the actual message
      */
      struct HearData
      {
        ObjectType   mPlayer;      /*!< Player object who has talked the message         */
        std::string  mMsg;         /*!< The actual text of the message                   */
        Time         mTime;        /*!< Game time at which the message was sent to agent */
        Degree       mSpeakAngle;  /*!< Relative angle of the player who has talked      */

        /*! Array is a dynamic array of heard data */
        typedef std::vector<HearData> Array;
      };


      /*!< Needed to store body part positions */
      typedef std::map<std::string, VecPosition> NamedPos;

      // Attributes
      FieldInformation                mFieldInfo;                  /*!< Field information */
      GameInformation                 mGameInfo;                   /*!< Game information */
      std::string                     mTeamName;                   /*!< Our own team name */
      std::string                     mOpponentTeamName;           /*!< Opponent team name */


      // Objects in field
      Flag::Map                       mFlags;                      /*!< All flags in the field */
      PlayerObject::Map               mTeammates;                  /*!< All seen teammates */
      PlayerObject::Map               mOpponents;                  /*!< All seen opponents */
      Ball                          * mBall;                       /*!< Ball instance */

      // Cached instances
      // Singleton instances
      Logger                        * mLog;                        /*!< Logger instance to log     */
      AgentSettings                 * mAgentSettings;              /*!< Agent configuration */
      FormationSystem               * mFormation;                  /*!< FormationSystem to calculate strategic position */

      // Agent scene graph node instances
      AgentObject                   * mAgent;                      /*!< Agent */
      RSG2::Graph                   * mAgentGraph;                 /*!< Agent model's scene graph */
      RSG2::Joint::Map                mJoints;                     /*!< Cache for all joint perceptors */
      RSG2::GyroRatePerceptor::Array  mGyros;                      /*!< Gyro instances in the agent */
      RSG2::Accelerometer::Array      mAccs;                       /*!< Accelerometer instances in the agent */
      RSG2::FRP::Array                mFRPs;                       /*!< FRPs in the agent */

      // Needed update information
      HearData::Array                 mHeardMessages;              /*!< All heard messages in the last cycle */
      BalanceState                    mBalanceState;               /*!< Balance state of the player */


      // parsers
      int                 parseTime             ( SToken token                          );
      int                 parseGameState        ( SToken token                          );
      int                 parseGyro             ( SToken token                          );
      int                 parseSee              ( SToken token                          );
      int                 parseHear             ( SToken token                          );
      int                 parseHingeJoint       ( SToken token                          );
      int                 parseUniversalJoint   ( SToken token                          );
      int                 parseFRP              ( SToken token                          );
      int                 parseAccelerometer    ( SToken token                          );
      int                 parseBallVision       ( SToken token                          );
      int                 parseFlagVision       ( SToken token                          );
      int                 parsePlayerVisualNode ( SToken token, NamedPos & lst          );
      int                 parsePlayerVision     ( SToken token                          );


      // localization
      bool                localize              (                                       );

      // update methods
      int                 update                (                                       );

      // stub
      void                logWorldModel         (                                       );


      Object            * getObjectInstance     ( ObjectType object                     );
      const Object      * getObjectInstance     ( ObjectType object                     ) const;

      bool                isPlayerValid         ( const ObjectType & o                  ) const;


      // Constructor and destructor of WorldModel are private for enabling singleton
                          WorldModel            (                                       );
                        ~ WorldModel            (                                       );

    public:
      static WorldModel * getSingleton          (                                       );
      static void         freeSingleton         (                                       );

      bool                initialize            ( const std::string & strTeamName       );

      //methods to get of attributes of agent
      const RSG2::Joint * getJoint              ( const std::string & strName           ) const;
      Degree              getJointAngle         ( const std::string & strName,
                                                  const int & iNumber = 0               );
      Degree              getJointSpeed         ( const std::string & strName,
                                                  const int & iNumber = 0               );
      VecPosition         getGyro               ( const std::string & strName = "torso" ) const;
      VecPosition         getAccelerometer      ( const std::string & strName = "torso" ) const;
      bool                getFRP                ( const std::string & strName,
                                                  VecPosition & c,
                                                  VecPosition & f                       );


      // field information
      double              getFieldLength        (                                       ) const;
      double              getFieldWidth         (                                       ) const;
      double              getGoalWidth          (                                       ) const;
      double              getGoalHeight         (                                       ) const;
      double              getFlagHeight         (                                       ) const;


      // game information
      Time                getTime               (                                       ) const;
      Time                getGameTime           (                                       ) const;
      size_t              getCycle              (                                       ) const;
      double              getTimeStep           (                                       ) const;
      SideType            getTeamSide           (                                       ) const;
      PlayMode            getPlayMode           (                                       ) const;


      // methods that return truth values about current play mode
      bool                isFreeKickUs          (  PlayMode pm = UnknownPlayMode        );
      bool                isFreeKickThem        (  PlayMode pm = UnknownPlayMode        );
      bool                isCornerKickUs        (  PlayMode pm = UnknownPlayMode        );
      bool                isCornerKickThem      (  PlayMode pm = UnknownPlayMode        );
      bool                isOffsideUs           (  PlayMode pm = UnknownPlayMode        );
      bool                isOffsideThem         (  PlayMode pm = UnknownPlayMode        );
      bool                isKickInUs            (  PlayMode pm = UnknownPlayMode        );
      bool                isKickInThem          (  PlayMode pm = UnknownPlayMode        );
      bool                isKickOffUs           (  PlayMode pm = UnknownPlayMode        );
      bool                isKickOffThem         (  PlayMode pm = UnknownPlayMode        );
      bool                isGoalKickUs          (  PlayMode pm = UnknownPlayMode        );
      bool                isGoalKickThem        (  PlayMode pm = UnknownPlayMode        );
      bool                isBeforeKickOff       (  PlayMode pm = UnknownPlayMode        );
      bool                isBeforeKickOffUs     (  PlayMode pm = UnknownPlayMode        );
      bool                isBeforeKickOffThem   (  PlayMode pm = UnknownPlayMode        );
      bool                isDeadBallUs          (  PlayMode pm = UnknownPlayMode        );
      bool                isDeadBallThem        (  PlayMode pm = UnknownPlayMode        );


      // agent related information
      VecPosition         getAgentPosition      (                                       ) const;
      Quaternion          getAgentOrientation   (                                       ) const;
      Degree              getAgentHeading       (                                       ) const;
      Degree              getAgentAttitude      (                                       ) const;
      int                 getAgentNumber        (                                       ) const;
      ObjectType          getAgentObjectType    (                                       ) const;
      std::string         getTeamName           (                                       ) const;
      std::string         getOpponentTeamName   (                                       ) const;
      BalanceState        getAgentBalanceState  (                                       ) const;


      // ball information
      VecPosition         getBallPos            (                                       ) const;
      double              getBallDistance       ( bool bOnField = true                  );
      double              getBallSpeed          (                                       ) const;
      Degree              getBallDirection      (                                       ) const;
      VecPosition         getBallVelocity       (                                       ) const;


      // methods that return information about object in field
      Time                getTimeLastSeen       ( const ObjectType & object             ) const;
      VecPosition         getGlobalPosition     ( const ObjectType & object             ) const;
      double              getGlobalSpeed        ( const ObjectType & object             ) const;
      Degree              getGlobalDirection    ( const ObjectType & object             ) const;
      VecPosition         getGlobalVelocity     ( const ObjectType & object             ) const;
      VecPosition         getGlobalAcceleration ( const ObjectType & object             ) const;
      VecPosition         getRelativePosition   ( const ObjectType & object             ) const;
      VecPosition         getRelativePosition   ( const VecPosition & position          ) const;
      double              getRelativeDistance   ( const ObjectType & object             ) const;
      double              getRelativeDistance   ( const VecPosition & position          ) const;
      Degree              getRelativeAngle      ( const ObjectType & object             ) const;
      Degree              getRelativeAngle      ( const VecPosition & position          ) const;
      double              getConfidence         ( ObjectType object                     ) const;
      bool                isConfidenceGood      ( ObjectType object                     ) const;
      bool                isConfidenceVeryGood  ( ObjectType object                     ) const;


      // simspark message information
      Time                getTimeLastSeeMessage (                                       ) const;
      Time                getTimeLastHearMessage(                                       ) const;


      // goalie objects
      ObjectType          getOwnGoalieType      (                                       ) const;
      ObjectType          getTheirGoalieType    (                                       ) const;


      // iteration on objects in field
      int                 getObjectsInSet       ( ObjectArray & objects,
                                                  ObjectSetType g,
                                                  double dConf = -1                     ) const;


      // high level methods
      VecPosition         getStrategicPosition  ( int iNum = -1,
                                                  FormationType ft = UnknownFormation   ) const;
      bool                canBeam               (                                       ) const;


      // update method
      void                parseSimSparkMessage  ( const std::string & strMsg            );
  };


}; // end namespace Klaus

#endif
