/*
*********************************************************************************
*            Genetics.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 01/30/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class declaration for classes and storages used in evolutionary    *
*            programming techniques                                             *
*                                                                               *
*********************************************************************************
*/

/*! \file Genetics.h
<pre>
<b>File:</b>          Genetics.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/30/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class decleration for classes and storages used in evolutionary
                  programming techniques
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/30/2009       Mahdi           Modifies version of initial source (2/08) created
</pre>
*/

#ifndef GENETICS
#define GENETICS

#include <iostream>   // needed for ostream
#include <vector>     // needed for std::vector


namespace Klaus
{

  /*! This enumeration holds different types of supported selection methods of
      reproducing offsprings. */
  enum SelectionMethodT
  {
    RouletteWheel,    /*!< The Roulette Wheel selection  */
    SUS,              /*!< Stochstic Universal sampling  */
    Tournament,       /*!< Tournament selection          */
    UnknownSelection  /*!< Unknown selection             */
  };


  /*! This class represents a gene in Geneteic Algorithm framework used by this source
      Each gene contains information about the very smallest but distinct information
      of a learning process, like a speed change on a specific joint in agent
  */
  class Gene
  {
    protected:
      /**
        This method is responsible to create a new instance of children to this class
        @return new instance of children to this class
      */
      virtual Gene * newInstance  (                             ) = 0;

      /**
        This operator is responsible to copy values from another gene child instance
        @param gene The gene to copy values from
      */
      virtual void   operator =   ( Gene * gene                 ) = 0;

    public:
                     Gene         (                             );
      virtual      ~ Gene         (                             );

      /**
        This method is used to randomly mutate the gene.
      */
      virtual void   mutate       (                             ) = 0;

      Gene         * fork         (                             );
      virtual void   exchangeGenes( Gene * first, Gene * second );


      /*! A dynamic array of Genes */
      typedef std::vector<Gene*> Array;
  };




  /*! This class represents a chromosome in a Genetic Algorithm solvable system.
      It conatins all the genes for a specific part or purpose, and coordinates
      execution, mutation and selection of genes in the whole colony
  */
  class Chromosome
  {
    private:
      void                 cleanUp      (                      );

    protected:
      Gene::Array          mGenes;      /*!< The genes in the chromosome */

      /**
        This method is responsible to create a new instance of children to this class
        @return new instance of children to this class
      */
      virtual Chromosome * newInstance  (                      ) = 0;

    public:
                           Chromosome   (                      );
      virtual            ~ Chromosome   (                      );

      // operator methods
      int                  mutate       ( double dMutationRate );
      static void          xOver        ( Chromosome * first,
                                          Chromosome * second  );

      // utility methods
      Chromosome         * fork         (                      );

      // get methods
      size_t               getGenesCount(                      );
      Gene               * getGene      ( size_t iIndex        );
      void                 addGene      ( Gene * gene          );


      /*! Dynamic list of chromosomes */
      typedef std::vector<Chromosome*> Array;
  };




  /*! This class represents an organism, which contains several chromosomes which
      aims to solve a specific problem in learning space, handling all behaviours
      of a living organism. Methods to enable basic types of Genetic Alogorithm
      operators are partly here, like x-overs and mutation.
  */
  class Organism
  {
    protected:
      Chromosome::Array  mChromosomes;     /*!< Chromosomes of the organism */
      double             mFitness;         /*!< Fitness of the organism     */
      int                mMaxChromosomes;  /*!< Effective chromosome count  */

      /**
        This method is responsible to create a new instance of children to this class
        @return new instance of children to this class
      */
      virtual Organism * newInstance       (                         ) = 0;

    private:
      void               cleanUp           (                         );

    public:
                         Organism          (                         );
      virtual          ~ Organism          (                         );

      // operator methods
      int                mutate            ( double dMutationRate    );
      static  int        xOver             ( Organism * first,
                                             Organism * second,
                                             double dXOverRate       );

      // property get and set methods
      double             getFitness        (                         ) const;
      void               setFitness        ( double dFitness         );

      size_t             getChromosomeCount();
      Chromosome       * getChromosome     ( size_t iIndex           );
      void               addChromosome     ( Chromosome * chromosome );

      // Utility methods
      Organism         * fork              (                         );

      /*! A dynamic list of organisms */
      typedef std::vector<Organism*> Array;
  };




  /*! This class wraps a generation in it's whole entity. A generation has an
      organism which is the best of others in his own generation, has untested
      test cases, is able to reproduce another generation and can apply genetic
      operators on offsprings.
  */
  class Generation
  {
    private:
      // utility methods
      Generation         * fork                 (                              );

      // Selection methods
      Generation         * createFPGeneration   ( double dMinFitness,
                                                  double exp_val[],
                                                  double & max_val             );
      Generation         * selectByRouletteWheel( double dMinFitness           );
      Generation         * selectBySUS          ( double dMinFitness           );
      Generation         * selectByTournamenting( double dFitterWinProbabilty  );

      // Other operators
      int                  applyXOvers          ( double dXOverWillingness,
                                                  double dXOverRate            );
      int                  applyMutations       ( double dMutationRate         );

    protected:
      int                  mMaxOrganismsInGeneration; /*!< Max organisms at a time */
      Organism::Array      mOrganisms;          /*!< List of all the organisms */

      /**
        This method is responsible to create a new instance of children to this class
        @return new instance of children to this class
      */
      virtual Generation * newInstance          (                              ) = 0;

    public:
                           Generation           (                              );
      virtual            ~ Generation           (                              );

      // utility methods
      void                 cleanUp              (                              );
      void                 removeAllOrganisms   (                              );
      unsigned             getOrganismCount     (                              );
      Organism           * getOrganism          ( unsigned iIndex              );
      void                 addOrganism          ( Organism * organism          );
      void                 clearAllFitnesses    (                              );

      // Main methods
      Organism           * getBestOrganism      (                              );
      Organism           * getNextTestCase      (                              );

      // TODO: better to be private. don't think so?
      Generation         * reproduce            ( double dMutationRate,
                                                  double dXOverWillingness,
                                                  double dXOverRate,
                                                  SelectionMethodT selMethod,
                                                  double dWinnerProb,
                                                  double dMinFitness = 0       );

      /*! Dynamic array of Generations */
      typedef std::vector<Generation*> Array;
  };




  /*! This class wraps the mother nature for everything that lives in it. This mother
      provides the children (organisms) with supplies, lets them reproduce offsprings,
      alters their genetics randomly by mutating, kills weak ones and keeps strong
      ones. After a while, mostly strong ones will remain in the nature, thus providing
      us with our purpose. */
  class Nature
  {
    protected:
      double            mXOverRate;              /*!< Rate at which crossovers happen */
      double            mXOverWillingness;       /*!< Willingness to have crossovers */
      double            mMutationRate;           /*!< Rate at which mutation happen */
      SelectionMethodT  mSelectionMethod;        /*!< Method for selecting parents */
      double            mMinFitness;             /*!< Minimum fitness organisms can have */
      double            mWinProbability;         /*!< Probabilty of winning tournament */
      Generation::Array mGenerations;            /*!< All the generations in history */

      virtual void      initializeFirstGeneration(                      ) = 0;
      virtual void      initializeDefaultValues  (                      );
      virtual void      loadGenerations          (                      );
      virtual void      saveGenerations          (                      );

    private:
      void              cleanUp                  (                      );

    public:
                        Nature                   (                      );
      virtual         ~ Nature                   (                      );

     // Main methods
     void               setupNature              (                      );
     Organism         * getBestOrganism          (                      );
     Organism         * getNextTestCase          (                      );
  };

};


#endif //GENETICS
