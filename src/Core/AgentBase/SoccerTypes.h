/*
*********************************************************************************
*          SoccerTypes.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for SoccerCommand             *
*            (Some materials are owned & copyrighted by Jelle Kok)              *
*                                                                               *
*********************************************************************************
*/

/*! \file SoccerTypes.h
<pre>
<b>File:</b>          SoccerTypes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for SoccerCommand
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*!
<pre>
<b>File:</b>          SoccerTypes.h
<b>Project:</b>       Robocup Soccer Simulation Team: UvA Trilearn
<b>Authors:</b>       Jelle Kok
<b>Created:</b>       28/11/2000
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains the different enumerations and
               constants that are important in the Soccer Server.
               Furthermore it contains the class SoccerCommand which is
               used to denote the different possible soccer commands and
               the class SoccerTypes that contains all kind of static
               methods to translate text strings that are received from
               the server into the soccer types (=enumerations) that
               are defined here. Finally it contains the Time class which
               holds a two-tuple that represents the time in the soccer server.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
28/11/2000       Jelle Kok       Initial version created
</pre>
*/

#ifndef SOCCERTYPES
#define SOCCERTYPES

#include <libZigorat/Geometry.h>     // needed for AngDeg


namespace Klaus
{

  /*! The time in Soccer Simulation 3D is a float with cycles of 0.02 second */
  typedef double Time;

  const double  UnknownDoubleValue  = -1000.0; /*!< indicates unknown double   */
  const Time    UnknownTime         = -1000.0; /*!< indicates unknown time     */
  const int     UnknownIntValue     = -1000;   /*!< indicates unknown int      */


  /*! AgentModelType holds the type of agent model which the agent can have and utilize.
      It is used to derive shapes, joints, positions, body parts, command names to
      simulator and everything which is related specifically to an agent model. */
  enum AgentModelType
  {
    LeggedSphere, /*!< Ancient legged sphere model                         */
    SoccerBot055, /*!< First available soccerbot used in IranOpen 2007     */
    SoccerBot056, /*!< Soccerbot version intorduced in Atlanta 2007        */
    SoccerBot058, /*!< Soccerbot version introduced before Suzhou 2008     */
    Nao,          /*!< Nao Model, again introduced and used in Suzhou 2008 */
    UnknownModel  /*!< Unknown Model                                       */
  };


  /*! ObjectT is an enumeration of all possible objects that are part of the
      RoboCup soccer simulation. The class SoccerTypes contains different methods
      to easily work with these objects and convert them to text strings and text
      strings to ObjectT. */
  enum ObjectType
  {
    // don't change order
    ObjectBall,            /*!< Ball                     00 */

    ObjectFlagGLT,         /*!< Left top goal post       01 */   // 4 goals
    ObjectFlagGLB,         /*!< Left bottom goal post    02 */
    ObjectFlagGRT,         /*!< Right top goal post      03 */
    ObjectFlagGRB,         /*!< Right bottom goal post   04 */
    ObjectFlagCLT,         /*!< Flag corner left top     05 */   // 4 flags
    ObjectFlagCLB,         /*!< Flag corner left bottom  06 */
    ObjectFlagCRT,         /*!< Flag corner right top    07 */
    ObjectFlagCRB,         /*!< Flag corner right bottom 08 */
    ObjectFlagUnknown,     /*!< Unknown corner           09 */

    ObjectTeammate1,       /*!< Teammate nr 1            10 */   // 12 teammates
    ObjectTeammate2,       /*!< Teammate nr 2            11 */
    ObjectTeammate3,       /*!< Teammate nr 3            12 */
    ObjectTeammate4,       /*!< Teammate nr 4            13 */
    ObjectTeammate5,       /*!< Teammate nr 5            14 */
    ObjectTeammate6,       /*!< Teammate nr 6            15 */
    ObjectTeammate7,       /*!< Teammate nr 7            16 */
    ObjectTeammate8,       /*!< Teammate nr 8            17 */
    ObjectTeammate9,       /*!< Teammate nr 9            18 */
    ObjectTeammate10,      /*!< Teammate nr 10           19 */
    ObjectTeammate11,      /*!< Teammate nr 11           20 */
    ObjectTeammateUnknown, /*!< Teammate nr unkown       21 */

    ObjectOpponent1,       /*!< Opponent nr 1            22 */   // 12 opponents
    ObjectOpponent2,       /*!< Opponent nr 2            23 */
    ObjectOpponent3,       /*!< Opponent nr 3            24 */
    ObjectOpponent4,       /*!< Opponent nr 4            25 */
    ObjectOpponent5,       /*!< Opponent nr 5            26 */
    ObjectOpponent6,       /*!< Opponent nr 6            27 */
    ObjectOpponent7,       /*!< Opponent nr 7            28 */
    ObjectOpponent8,       /*!< Opponent nr 8            29 */
    ObjectOpponent9,       /*!< Opponent nr 9            30 */
    ObjectOpponent10,      /*!< Opponent nr 10           31 */
    ObjectOpponent11,      /*!< Opponent nr 11           32 */
    ObjectOpponentUnknown, /*!< Opponent nr unknown      33 */

    ObjectPlayerUnknown,   /*!< Unknown player           34 */

    ObjectTeammateGoalie,  /*!< Goalie of your side      35 */

    ObjectOpponentGoalie,  /*!< Goalie of opponent side  36 */

    ObjectCount,           /*!< maximum nr of objects    37 */

    UnknownObject          /*!< illegal object           38 */
  } ;


  /*! Object List is a dynamic array of objects */
  typedef std::vector<ObjectType> ObjectArray;


  /*!The ObjectSetT enumerations holds the different object sets, which
     consists of one or multiple ObjectT types. */
  enum ObjectSetType
  {
    Teammates,         /*!< teammates                    */
    Opponenents,       /*!< opponents                    */
    Players,           /*!< players                      */
    TeammatesNoGoalie, /*!< teammates without the goalie */
    OpponentsNoGoalie, /*!< opponents without the goalie */
    Flags,             /*!< flags                        */
    UnknownSet         /*!< illegal                      */
  };


  /*! The PlayMode enumeration contains all playmodes of the soccer playmode.
      The associated string values can be returned using the methods in the
      SoccerTypes class */
  enum PlayMode
  {
    BeforeKickOff = 0,    /*!< before_kick_off:   before kick off         */
    KickOffLeft,          /*!< kick_off_l:        kick off for left team  */
    KickOffRight,         /*!< kick_off_r:        kick off for right team */
    PlayOn,               /*!< play_on:           play on (during match)  */
    KickInLeft,           /*!< kick_in_l:         kick in for left team   */
    KickInRight,          /*!< kick_in_r:         kick in for right team  */
    CornerKickLeft,       /*!< corner_kick_l:     corner kick left team   */
    CornerKickRight,      /*!< corner_kick_r:     corner kick right team  */
    GoalKickLeft,         /*!< goal_kick_l:       goal kick for left team */
    GoalKickRight,        /*!< goal_kick_r:       goal kick for right team*/
    OffsideLeft,          /*!< offside_l:         offside for left team   */
    OffsideRight,         /*!< offside_r:         offside for right team  */
    Quit,                 /*!< quit                                       */
    GoalLeft,             /*!< goal_l:            goal scored by team left*/
    GoalRight,            /*!< goal_r:            goal scored by team righ*/
    FreeKickLeft,         /*!< free_kick_l:       free kick for left team */
    FreeKickRight,        /*!< free_kick_r:       free kick for right team*/
    UnknownPlayMode       /*!< unknown playmode                           */
  };


  /*!The SideType enumeration contains the two sides */
  enum SideType
  {
    SideUs = 0, /*!< Our Side     */
    SideLeft,   /*!< Left side    */
    SideRight,  /*!< Right side   */
    UnknownSide /*!< Illegal side */
  };


  /*! The CommandType enumeration contains the different types for the SoccerCommand
     that are possible. */
  enum CommandType
  {
    CommandJoint,  /*!< Joint handling command      */
    CommandSay,    /*!< say command                 */
    CommandBeam,   /*!< Beam command                */
    CommandScene,  /*!< Scene command               */
    CommandInit,   /*!< Initialization command      */
    UnknownCommand /*!< maximum number of commands  */
  };


  /*! The BalanceState enum indicates which side has agent fallen */
  enum BalanceState
  {
    FallenBack,    /*!< Fallen back */
    FallenFront,   /*!< Fallen front */
    FallenLeft,    /*!< Fallen left */
    FallenRight,   /*!< Fallen right */
    Normal         /*!< Not fallen */
  };


  /*! This class resembles a SoccerCommand that contains all the information about
      a command that can be sent to the server, but the format is independent from
      the server implementation. A SoccerCommand can be created and before it is
      sent to the server, be converted to the actual string representation
      understood by the server.
  */
  class SoccerCommand
  {
    private:
      // private methods to generate text string to send to server
      bool          makeCommandString   ( std::string & strMsg               ) const;

    public:
      typedef std::vector<SoccerCommand> Array; /*!< Dynamic array of SoccerCommands*/

      // different variables that are used by the different possible commands
      // only the variables that are related to the current commandType have
      // legal values
      CommandType    mCommandType;      /*!< type of this command             */
      std::string    mEffector;         /*!< Joint effector to change         */
      double         mSpeed1;           /*!< speed of first angle of joint    */
      double         mSpeed2;           /*!< speed of second angle joint      */
      std::string    mSayStr;           /*!< str (for say)                    */
      VecPosition    mBeamPosition;     /*!< Position to beam                 */
      AgentModelType mModel;            /*!< Model to load in simspark        */

                     SoccerCommand       (                                    );
                     SoccerCommand       ( const std::string & strSay         );
                     SoccerCommand       ( const VecPosition & pos            );
                     SoccerCommand       ( AgentModelType model               );
                     SoccerCommand       ( const std::string & strEffector,
                                           double d1,
                                           double d2 = UnknownDoubleValue     );
                     SoccerCommand       ( int iNum,
                                           const std::string & team_name      );

      void           clear               (                                    );

      // command to set the different values of the SoccerCommand
      void           makeCommand         ( const std::string & strEffector,
                                           double     d1,
                                           double     d2 = UnknownDoubleValue );
      void           makeCommand         ( const std::string & strSay         );
      void           makeCommand         ( const VecPosition & pos            );
      void           makeCommand         ( AgentModelType model               );
      void           makeCommand         ( int iNum,
                                           const std::string & team_name      );

      bool           isIllegal           (                                    );

      std::string    str                 (                                    ) const;

      // used to return the string representation of this SoccerCommand
      bool          getCommandString     ( std::string &strCmd                ) const;

      const SoccerCommand & operator =   ( const SoccerCommand & cmd          );

      /**
        Overloaded stream output operator for showing a soccercommand on standard outputs
        @param os The output stream to write the command to
        @param cmd The command to write on the output stream
        @return The output stream instance
      */
      friend std::ostream& operator <<   ( std::ostream &os,
                                           const SoccerCommand & cmd          );
  };


  /*! The class SoccerTypes contains different methods to work with the
      different enumerations defined in SoccerTypes.h. It is possible to
      convert soccertypes to strings and strings to soccertypes. It is
      also possible to get more specific information about some of the
      soccertypes. All methods are static so it is possible to call the
      methods without instantiating the class. */
  class SoccerTypes
  {
    public:
      // methods that deal with the different objects
      static std::string       getObjectStr              ( ObjectType    o             );
      static ObjectType        getObjectFromStr          ( const std::string &str      );
      static bool              isInSet                   ( ObjectType    o,
                                                           ObjectSetType o_g,
                                                           ObjectType    objectGoalie =
                                                           ObjectTeammate1             );
      static bool              isGoal                    ( ObjectType    o             );
      static bool              isFlag                    ( ObjectType    o             );
      static bool              isBall                    ( ObjectType    o             );
      static bool              isTeammate                ( ObjectType    o             );
      static bool              isOpponent                ( ObjectType    o             );
      static bool              isGoalie                  ( ObjectType    o             );
      static bool              isPlayer                  ( ObjectType    o             );
      static int               getIndex                  ( ObjectType    o             );
      static int               getPlayerNumber           ( ObjectType    o             );
      static ObjectType        getTeammateTypeFromNumber ( int           iNum          );
      static ObjectType        getOpponentTypeFromNumber ( int           iNum          );
      static bool              isKnownPlayer             ( ObjectType    o             );

      // methods that deal with the different play modes
      static const std::string getPlayModeStr            ( PlayMode      p             );
      static PlayMode          getPlayModeFormStr        ( const std::string & str    );

      // methods that deal with the side information
      static const std::string getSideStr                ( SideType     s             );
      static SideType          getSideFromStr            ( const std::string & str    );

      // dealing with balance state
      static const std::string getBalanceStateStr        ( BalanceState state         );

      // To convert a boolean to string
      static const std::string getBoolString             ( const bool &  b             );

      static const std::string getAgentModelString       ( const AgentModelType & am  );
      static AgentModelType    getAgentModelFromString   ( const std::string & str    );

      static bool              isModelSoccerBot          ( const AgentModelType & am  );

      static double            getConfidence             ( Time t_old,
                                                           Time t_new                 );
  };


}; // end namespace Klaus


#endif // SOCCERTYPES
