/*
*********************************************************************************
*          Kinematics.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/09/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Kinematic type infromation and utilities                           *
*                                                                               *
*********************************************************************************
*/

/*! \file Kinematics.cpp
<pre>
<b>File:</b>          Kinematics.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/09/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Kinematic type infromation and utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/09/2011       Mahdi           Initial version created
</pre>
*/

#include "Kinematics.h"
#include <libZigorat/Logger.h> // needed for Logger
#include <cstring>             // needed for memcp





/***************************************************************************************/
/********************************  Class BodyInfo  *************************************/
/***************************************************************************************/

/**
  Constructor of BodyInfo with given translation
  @param tr The translation of body part
*/
Klaus::BodyInfo::BodyInfo(const Klaus::Matrix4& tr)
{
  mTransform = tr;
}

/**
  Constructor of BodyInfo with the given body information
  @param info Information of the body
*/
Klaus::BodyInfo::BodyInfo(const Klaus::BodyInfo& info)
{
  memcpy(this, &info, sizeof(BodyInfo));
}

/**
  Constructor of BodyInfo with no parameters.
*/
Klaus::BodyInfo::BodyInfo()
{
  mMass = 0;
}




/***************************************************************************************/
/***********************************  Class Physics  ***********************************/
/***************************************************************************************/

/**
  This method calculates center of mass of a given kinematic sheet
  @param sheet The sheet that contains the kinematical information of a graph
  @return The center of mass's position
*/
Klaus::VecPosition Klaus::Physics::getCenterOfMass(const Kinematics& sheet)
{
  double dMass = 0;
  VecPosition p;

  for(Kinematics::const_iterator it = sheet.begin(); it != sheet.end(); ++it)
  {
    dMass += it->second.mMass;
    p += it->second.mTransform.getTranslation() * it->second.mMass;
  }

  return p / dMass;
}

/**
  This method calculates zero momentum point in the scene graph sheet.
  @param sheet The sheet containin the zero momentum point calculation's needed information
  @return The Zero-Momentum-Point
*/
Klaus::VecPosition Klaus::Physics::getZeroMomentumPoint(const Kinematics& sheet)
{
  std::cerr << "(Body) ZMP calculation not implemented yet\n";
  return VecPosition();
}
