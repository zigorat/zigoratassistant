/*
*********************************************************************************
*           Trajectory.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/07/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class declarations for Trajectories.                               *
*                                                                               *
*********************************************************************************
*/

/*! \file Trajectory.h
<pre>
<b>File:</b>          Trajectory.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/07/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class declarations for Trajectories.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/07/2011       Mahdi           Initial version created
</pre>
*/


#ifndef TRAJECTORY
#define TRAJECTORY

#include <libZigorat/Algebra.h>  // needed for Quaternion


namespace Klaus
{

  /*! This class represents a basic trajectory. It shall be implemented by
      specific trajectories to return a specific position for the projectile
      with a given time. */
  class _DLLExportControl Trajectory
  {
    protected:
      double              mRange;           /*!< Range of the tracjetory step */
      Klaus::Quaternion   mOrientation;     /*!< Angle in the X-Y plane */
      VecPosition         mStartPos;        /*!< Starting position of projectile */

      /**
        This method is called whenever a property is changed by calling set methods.
        It shall be used to update route for child classes
      */
      virtual void        postUpdateInternal(                             ) = 0;

    public:
                          Trajectory        (                             );
                          Trajectory        ( const Klaus::VecPosition & p,
                                              const Klaus::Quaternion & q,
                                              const double & range = 0.0  );

      void                setTrajectory     ( const Klaus::VecPosition & p,
                                              const Klaus::Quaternion & q,
                                              const double & range = 0.0  );

      double              getRange          (                             ) const;
      void                setRange          ( const double & range        );

      Klaus::Quaternion   getOrientation    (                             ) const;
      void                setOrientation    ( const Klaus::Quaternion & q );

      VecPosition         getStartingPos    (                             ) const;
      void                setStartingPos    ( const VecPosition & pos     );

      /**
        Overloaded method to get the position of the object on the trajectory
        @param time Time of the movement
        @return Position of the object on the trajectory
      */
      virtual VecPosition getPosition       ( double time                 ) = 0;
  };


  /**
    This class represents a periodic trajectory, A trajectory which repeats
    its trajectory in a constant period of time.
  */
  class _DLLExportControl PeriodicTrajectory : public Trajectory
  {
    protected:
      double              mPeriod;          /*!< Period of the trajectory loop */
      Degree              mPhaseDelay;      /*!< Phase delay */

      Degree              getPhase          ( double time                    );
      int                 getNumberOfCycles ( double time                    );

    public:
                          PeriodicTrajectory( const double & period    = 1.0,
                                              const Degree & delta_phi = 0.0 );

      double              getPeriod         (                                ) const;
      void                setPeriod         ( const double & period,
                                              const Degree & delta_phi = 0.0 );

      Degree              getPhaseDelay     (                                ) const;
      void                setPhaseDelay     ( const Degree & phase_delay     );
  };


}; // end namespace Klaus


#endif // TRAJECTORY
