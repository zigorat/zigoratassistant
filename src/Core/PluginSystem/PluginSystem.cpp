/*
*********************************************************************************
*          PluginSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat        *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Core of the plugin management                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file PluginSystem.cpp
<pre>
<b>File:</b>          PluginSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Core of the plugin management
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/


#include "PluginSystem.h"
#include <libZigorat/Logger.h>              // needed for Logger
#include <libZigorat/ApplicationSettings.h> // needed for ApplicationSettings

#include <fstream>  // for loading file from dist

#if PLATFORM == PLATFORM_WINDOWS
#include <windows.h>
#endif


/** PluginSystem sigleton instance */
template<> Klaus::PluginSystem* Klaus::Singleton<Klaus::PluginSystem>::mInstance = 0;

/**
  Constructor of PluginSystem. Adds default plugin search paths.
*/
Klaus::PluginSystem::PluginSystem()
{
  ApplicationSettings * settings = ApplicationSettings::getSingleton();
  mPluginLocations.push_back(settings->getPluginsPath());
}

/**
  Destructor of PluginSystem
*/
Klaus::PluginSystem::~PluginSystem()
{
  clearPlugins();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::PluginSystem * Klaus::PluginSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new PluginSystem;
    Logger::getSingleton()->log(1, "(PluginSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::PluginSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(PluginSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(PluginSystem) removed singleton");
  }
}

/**
  This method manually adds a location to search for plugins. Once
  added a rescanPlugins will be needed to update the plugins.
  @param strPath Location to look for plugins
*/
void Klaus::PluginSystem::addPluginLocation( const std::string & strPath )
{
  if(!strPath.empty())
    mPluginLocations.push_back(strPath);
}

/**
  This method manually removes a location from plugin search paths.
  @param strPath Location to remove from the search paths.
*/
void Klaus::PluginSystem::removePluginLocation( const std::string & strPath )
{
  for(StringArray::iterator it = mPluginLocations.begin(); it != mPluginLocations.end(); it++)
    if(*it == strPath)
    {
      mPluginLocations.erase(it);
      return;
    }
}

/**
  This method clears all plugin search locations.
*/
void Klaus::PluginSystem::clearLocations()
{
  mPluginLocations.clear();
}

/**
  This method scans a specific plugin location and adds plugins as it
  iterates through all founded shared libraries. The shared library
  should export two functions: getDLTargetApp (for getting the target
  application of the plugin) and getFactory (for getting the factory
  of the plugin)
  @param strPath Location to search for possible plugins
  @return Number of founded plugins
*/
int Klaus::PluginSystem::scanLocation( const std::string & strPath )
{
  StringArray files;
  FileSystem::getFolderContents(strPath, files);

  int iNumber = 0;
  #ifdef WIN32
  const std::string ext = ".dll";
  #else
  const std::string ext = ".so";
  #endif

  for(size_t i = 0; i < files.size(); i++)
  {
    if(files[i].rfind(ext, files[i].size() - ext.size()) != files[i].npos)
    {
      // open the library
      DynamicLibrary * lib = new DynamicLibrary;
      if(!lib->openLibrary(strPath + '/' + files[i]))
      {
        delete lib;
        continue;
      }

      // get plugin information
      std::string getFunc, freeFunc;
      #ifdef WIN32
        getFunc = "?getPluginFactory@@YAPAVPluginFactory@Klaus@@XZ";
        freeFunc = "?freePluginFactory@@YAXPAVPluginFactory@Klaus@@@Z";
      #else
        getFunc = "_Z16getPluginFactoryv";
        freeFunc = "_Z17freePluginFactoryPN5Klaus13PluginFactoryE";
      #endif

      // get the two needed functions in the library
      // PluginFactory* getPluginFactory(void);
      // void freePluginFactory(PluginFactory * factory);
      Klaus::PluginFactory* (*getPluginFactory)();
      *(void **)(&getPluginFactory) = lib->getFunctionAddress(getFunc, true);

      void (*freePluginFactory)(Klaus::PluginFactory*);
      *(void **)(&freePluginFactory) = lib->getFunctionAddress(freeFunc, true);

      // freePluginFactory is not used here, but is searched
      // for in the library to be sure it has one
      if((!getPluginFactory) || (!freePluginFactory))
      {
        delete lib;
        continue;
      }

      // get a factory instance
      Klaus::PluginFactory *factory = getPluginFactory();
      if(!factory)
      {
        delete lib;
        continue;
      }

      // and add it to the list of factories
      mPluginFactories.push_back(factory);
      mDynamicLibraries.push_back(lib);
      Logger::getSingleton()->log(2, "(PluginSystem) registered plugin: %s version %d.%d of type %s",
                                     factory->getName(), factory->getMajorVersion(),
                                     factory->getMinorVersion(), factory->getRole());

      iNumber++;
    }
  }

  return iNumber;
}

/**
  This method rescans all plugin search locations and adds plugins from all the locations
  @return Number of the founded plugins
*/
int Klaus::PluginSystem::rescanPlugins()
{
  int iNumbers = 0;

  for(size_t i = 0; i < mPluginLocations.size(); ++i)
    iNumbers += scanLocation(mPluginLocations[i]);

  return iNumbers;
}

/**
  This method clears all loaded plugins
*/
void Klaus::PluginSystem::clearPlugins()
{
  // free all plugin factories first
  #ifdef WIN32
  const std::string freeFunc = "?freePluginFactory@@YAXPAVPluginFactory@Klaus@@@Z";
  #else
  const std::string freeFunc = "_Z17freePluginFactoryPN5Klaus13PluginFactoryE";
  #endif

  void (*freePluginFactory)(Klaus::PluginFactory*);
  for(size_t i = 0; i < mPluginFactories.size(); i++)
  {
    *(void**)(&freePluginFactory) = mDynamicLibraries[i]->getFunctionAddress(freeFunc);
    freePluginFactory(mPluginFactories[i]);
  }
  mPluginFactories.clear();

  // then free all dynamic libraries
  for(size_t i = 0; i < mDynamicLibraries.size(); i++)
    delete mDynamicLibraries[i];
  mDynamicLibraries.clear();
}

/**
  This method returns plugin info instance of a plugin with the given name and role
  @param strName Name of the plugin
  @param strRole role of the plugin
  @return Plugin info instance
*/
Klaus::PluginFactory* Klaus::PluginSystem::findPlugin( const std::string & strName, const std::string & strRole )
{
  for(size_t i = 0; i < mPluginFactories.size(); i++)
    if(strName == mPluginFactories[i]->getName() && strRole == mPluginFactories[i]->getRole())
      return mPluginFactories[i];

  return NULL;
}

/**
  This method finds the name of all plugins which have the
  given role and store them in the given list
  @param list List to save the plugin names
  @param strRole Role of the desired plugins
*/
void Klaus::PluginSystem::findAllPluginsByRole(StringArray & list, const std::string & strRole)
{
  list.clear();

  for(size_t i = 0; i < mPluginFactories.size(); i++)
    if(mPluginFactories[i]->getRole() == strRole)
      list.push_back(mPluginFactories[i]->getName());
}
