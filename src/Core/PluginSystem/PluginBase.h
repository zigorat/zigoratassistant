/*
*********************************************************************************
*               Plugin.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base of the plugin management                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file PluginSystem/PluginBase.h
<pre>
<b>File:</b>          PluginBase.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base of the plugin management
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/


#ifndef PLUGIN_BASE
#define PLUGIN_BASE

#include <libZigorat/PlatformMacros.h>
#include <vector>


namespace Klaus
{


/**************************************************************/
/************************  Class Plugin  **********************/
/**************************************************************/

/*! This class is the base class for creating plugins. All plugins
    should be child of this class so that plugin manager can create
    their instances */
class _DLLExport Plugin
{
  public:
    /**
     * Constructor of Plugin. Does nothing
     */
    Plugin()
    {
    }

    /**
     * Destructor of Plugin.
     */
    virtual ~ Plugin()
    {
    }
};



/*********************************************************************/
/************************  Class PluginFactory  **********************/
/*********************************************************************/

/*! PluginFactory is a factory which solely creates plugin instances.
    Each plugin created for this application shall have a corresponding
    factory assiciated with it. The application just loads the factories
    from the dynamic libraries and when an instance of a plugin was
    needed, it's factory is responsible to create it.
    PluginFactory also holds information for the plugin. This includes
    name, role, author, version and the factory instance. These information
    are not to be stored on the plugin. They are all stored once, in the
    factory of the plugin.
*/
class _DLLExport PluginFactory
{
  private:
    char          * mName;         /*!< Name of the plugin           */
    char          * mRole;         /*!< Role of the plugin           */
    char          * mAuthor;       /*!< Author of the plugin         */
    int             mMajorVersion; /*!< Major version of plugin      */
    int             mMinorVersion; /*!< Minor version of plugin      */

    /**
     * Overloaded version of plugin info copy contructor. Hides
       the main copy constructor to prevent copy construction.
     */
    PluginFactory( const PluginFactory & plugin )
    {
      throw "Copy constructor for PluginFactory class - this shall not be called anywhere";
    }

    /**
     * Utility function for getting length of c strings
     * @param str The string to get it's total length
     * @return Length of the given string in bytes
     */
    inline unsigned strlen(const char * str)
    {
      unsigned i;
      for(i = 0; str[i]; i++)
        ;
      return i;
    }

    /**
     * Redecleration of strcpy. This is done due to excessive warnings of MSVC for std::strcpy
     * @param dest Destination to copy the given string to
     * @param source to copy string from
     */
    inline void strcpy(char * dest, const char * source)
    {
      unsigned i;
      for(i = 0; source[i]; i++)
        dest[i] = source[i];
      dest[i] = 0;
    }

  public:

    /**
     * Constructor of PluginFactory. Initializes the plugin information database.
     * @param name Name of the plugin
     * @param role Role of the plugin
     * @param author Author name of the plugin
     * @param mj Major version of the plugin
     * @param mi Minor version of the plugin
     */
    PluginFactory( const char *name, const char *role, const char * author, int mj, int mi )
    {
      // copy name
      mName = new char[strlen(name) + 1];
      strcpy(mName, name);

      // copy role
      mRole = new char[strlen(role) + 1];
      strcpy(mRole, role);

      // copy author
      mAuthor = new char[strlen(author) + 1];
      strcpy(mAuthor, author);

      // copy version information
      mMinorVersion = mi;
      mMajorVersion = mj;
    }

    /**
     * Destructor of PluginInfo. Removes all memory allocated so far
     */
    ~PluginFactory()
    {
      // remove name and role
      delete [] mName;
      delete [] mRole;
    }

    /**
     * This method returns the name of the plugin
     * @return Plugin's name
     */
    const char * getName() const
    {
      return mName;
    }

    /**
     * This method returns the plugin's role in application
     * @return Role of the plugin in application.
     */
    const char * getRole() const
    {
      return mRole;
    }

    /**
     * This method returns the author name of the plugin
     * @return Author name of the plugin
     */
    const char * getAuthor() const
    {
      return mAuthor;
    }

    /**
     * This method returns the major version of the plugin. As in "2.3" this is 2.
     * @return Major version of the plugin
     */
    int getMajorVersion() const
    {
      return mMajorVersion;
    }

    /**
     * This method returns minor version of plugin. As in "2.3" the minor version is 3
     * @return Minor version of the plugin
     */
    int getMinorVersion() const
    {
      return mMinorVersion;
    }

    /**
     * This method creates an instance of the plugin which this factory purposes.
     * @return created plugin instance
     */
    virtual Plugin * createPluginInstance() = 0;

    /**
     * This method frees the created plugin instance in the heap
       space of the plugin's dynamic library.
     * @param plugin The plugin instance to destroy
     */
    void freePluginInstance( Plugin * plugin )
    {
      delete plugin;
    }

    /*! List is a dynamic array of plugin factories */
    typedef std::vector<PluginFactory*> List;
};


}; // end namespace Klaus

#endif // PLUGIN_BASE
