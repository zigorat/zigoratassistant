/*
*********************************************************************************
*           PluginSystem.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Core of the plugin management                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file PluginSystem.h
<pre>
<b>File:</b>          PluginSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Core of the plugin management
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/


#ifndef PLUGIN_SYSTEM
#define PLUGIN_SYSTEM

#include <libZigorat/Singleton.h>       // needed for Klaus::Singleton
#include <libZigorat/DynamicLibrary.h>  // needed for DLLList
#include <libZigorat/FileSystem.h>      // needed for StringArray and getFolderContents
#include "PluginBase.h"                 // needed for Plugin


namespace Klaus
{

  /*! PluginSystem is the core of plugins. Given all the paths needed
      to find libraries, a simple call to rescanLibraries will load all
      plugin shared libraries within those folders. Creates instances
      of plugins upon request.
  */
  class _DLLExportControl PluginSystem: public Singleton<PluginSystem>
  {
    private:
      StringArray           mPluginLocations;    /*!< Plugin locations */
      DynamicLibrary::Array mDynamicLibraries;   /*!< List of all loaded DLLs */
      PluginFactory::List   mPluginFactories;    /*!< List of all plugin factories */

                            PluginSystem         (                             );
                          ~ PluginSystem         (                             );

    public:
      static PluginSystem * getSingleton         (                             );
      static void           freeSingleton        (                             );

      void                  addPluginLocation    ( const std::string & strPath );
      void                  removePluginLocation ( const std::string & strPath );
      void                  clearLocations       (                             );
      int                   scanLocation         ( const std::string & strPath );
      int                   rescanPlugins        (                             );
      void                  clearPlugins         (                             );

      PluginFactory       * findPlugin           ( const std::string & strName,
                                                   const std::string & strRole );
      void                  findAllPluginsByRole ( StringArray & list,
                                                   const std::string & strRole );
  };

};


#endif // PLUGIN_SYSTEM
