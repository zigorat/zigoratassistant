/*
*********************************************************************************
*        AuxilaryNodes.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for auxilary scene nodes.     *
*            These nodes provide various utilities which can not be grouped     *
*            in a single definition.                                            *
*                                                                               *
*********************************************************************************
*/

/*! \file AuxilaryNodes.cpp
<pre>
<b>File:</b>          AuxilaryNodes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for auxilary scene nodes. These
               nodes provide various utilities which can not be grouped in a single definition.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#include "AuxilaryNodes.h"

/*********************************************************************/
/**************************  Class Root  *****************************/
/*********************************************************************/

/**
  Constructor of Root node. This node holds all other
  nodes and does not have any node type token.
*/
Klaus::RSG2::Root::Root()
{
  mType = "RootSceneNode";
}
