/*
*********************************************************************************
*         GraphicalNodes.cpp : Robocup 3D Soccer Simulation Team Zigorat        *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for graphical scene nodes.    *
*            These nodes have a graphical representation and are the only nodes *
*            which can be seen in a simulation.                                 *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicalNodes.cpp
<pre>
<b>File:</b>          GraphicalNodes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for graphical scene nodes.
               These nodes have a graphical representation and are the only nodes
               which can be seen in a simulation.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#include "GraphicalNodes.h"
#include <cstring>          // needed for memset

#ifdef IMPLEMENT_GRAPHICS
#include <GraphicSystem/GraphicSystem.h>  // needed to get ogre scene manager
#endif




/**********************************************************************/
/***********************  Class GraphicalNode  ************************/
/**********************************************************************/

/**
  Constructor of the GraphicalNode. Gets instances for
  scene manager and scene node
*/
Klaus::RSG2::GraphicalNode::GraphicalNode()
{
#ifdef IMPLEMENT_GRAPHICS
  mSceneMgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  mOgreNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
#endif
  mGParent = NULL;
  invalidateGraphics();
}

/**
  Destructor of GraphicalNode. Tells the parent the node does not exist anymore
*/
Klaus::RSG2::GraphicalNode::~GraphicalNode()
{
  mGParent = NULL;
  invalidateGraphics();
  removeSceneNode();
}

/**
  This method removes the scene node. Makes sure a parent scene node is
  not removed prior to his child scene node.
*/
void Klaus::RSG2::GraphicalNode::removeSceneNode()
{
  #ifdef IMPLEMENT_GRAPHICS
  // first remove all children
  while(!mGChildren.empty())
    mGChildren[0]->removeSceneNode();

  // now remove itself
  if(!mOgreNode)
    return;

  // first remove all attached objects
  Ogre::MovableObject * obj;
  while(mOgreNode->numAttachedObjects() != 0)
  {
    obj = mOgreNode->getAttachedObject(0);
    mOgreNode->detachObject(obj);

    if(dynamic_cast<Ogre::Entity*>(obj))
      mSceneMgr->destroyEntity(dynamic_cast<Ogre::Entity*>(obj));
    else if(dynamic_cast<Ogre::Light*>(obj))
      mSceneMgr->destroyLight(dynamic_cast<Ogre::Light*>(obj));
    else if(dynamic_cast<Ogre::Camera*>(obj))
      mSceneMgr->destroyCamera(dynamic_cast<Ogre::Camera*>(obj));
    else if(dynamic_cast<Ogre::ParticleSystem*>(obj))
      mSceneMgr->destroyParticleSystem(dynamic_cast<Ogre::ParticleSystem*>(obj));
    else
      error("(GraphicalNode) Unknown attached object");
  }

  // then remove and destroy the node itself
  if(mGParent)
    mGParent->removeGraphicChild(this, true);  // this also sets scene node to null
  else
  {
    mOgreNode->getParentSceneNode()->removeAndDestroyChild(mOgreNode->getName());
    mOgreNode = NULL;
  }

  #endif // IMPLEMENT_GRAPHICS
}

/**
  Called after loading the scene. Initializes the node's graphial parent
*/
void Klaus::RSG2::GraphicalNode::postLoadInternal()
{
  Klaus::RSG2::Node::postLoadInternal();

  invalidateGraphics();
  attachToParent();
}

/**
  This method checks whether the graphical node is a root for graphic
  tree or not. The root does not have any parents.
*/
bool Klaus::RSG2::GraphicalNode::isRoot() const
{
  return mGParent == NULL;
}

/**
  This method hooks up the scene node to a proper parent ogre scene
  node. If it finds out that itself is the root of graphic tree,
  then it will return it's own instance as the root of the tree.
*/
void Klaus::RSG2::GraphicalNode::attachToParent()
{
  #ifdef IMPLEMENT_GRAPHICS
  // first remove from the current parent
  // shall not remove enitities and meshes, so will not call removeSceneNode
  if(mGParent)
    mGParent->removeGraphicChild(this);
  else
    mOgreNode->getParentSceneNode()->removeChild(mOgreNode);

  // then add to the next parent
  Node * node = this;
  while(node->getParent())
  {
    node = node->getParent();
    if(dynamic_cast<GraphicalNode*>(node))
    {
      dynamic_cast<GraphicalNode*>(node)->addGraphicChild(this);
      return; // done. exit
    }
  }

  // if no node found, then this node is the top most graphical
  // node. so attach it to the root scene node of scene manager.
  mSceneMgr->getRootSceneNode()->addChild(mOgreNode);
  mGParent = NULL;
  #endif // IMPLEMENT_GRAPHICS
}

/**
  This method adds a child to the children of this class. Also sets the child's ogre parent node.
  @param child The child to add to children and set it's ogre parent node.
*/
void Klaus::RSG2::GraphicalNode::addGraphicChild(GraphicalNode* child)
{
  mGChildren.push_back(child);
  child->mGParent = this;

  #ifdef IMPLEMENT_GRAPHICS
  mOgreNode->addChild(child->mOgreNode);
  #endif
}

/**
  This method removes a child from the children of this class.
  Also removes the child ogre node from it's own ogre node's children
  @param child The child to remove
  @param bDestroy Whether destroy the child ogre node or not
*/
void Klaus::RSG2::GraphicalNode::removeGraphicChild(GraphicalNode* child, bool bDestroy)
{
  // remove the child from this node's children
  for(size_t i = 0; i < mGChildren.size(); i++)
    if(mGChildren[i] == child)
    {
      mGChildren.erase(mGChildren.begin()+i);
      break; // no further search needed
    }

  // remove the child's parent
  child->mGParent = NULL;

  // remove it's scene node
  // if necessary, destroy it.
  if(bDestroy)
  {
    #ifdef IMPLEMENT_GRAPHICS
    mOgreNode->removeAndDestroyChild(child->mOgreNode->getName());
    #endif
    child->mOgreNode = NULL;
  }
  else
  {
    #ifdef IMPLEMENT_GRAPHICS
    mOgreNode->removeChild(child->mOgreNode);
    #endif
  }
}

/**
  Override method to set parent node for the scene node. This hooks up the node to a parent graphical node.
  @param parent The parent to attach the node to.
*/
void Klaus::RSG2::GraphicalNode::setParent(Node* parent)
{
  Node::setParent(parent);
  attachToParent();
}

#ifdef IMPLEMENT_GRAPHICS
/**
  This method sets ogre parent for the node. Manually sets the parent of the node in graphical scene
  @param parent The ogre parent to set as the father
*/
void Klaus::RSG2::GraphicalNode::setOgreParent(Ogre::SceneNode* parent)
{
  if(mGParent)
  {
    error("(GraphicalNode) setOgreParent called while attached to another node");

    // again, shall not call removeSceneNode, since it needs to keep it's entities
    mGParent->removeGraphicChild(this);
  }

  mOgreNode->getParentSceneNode()->removeChild(mOgreNode->getName());
  parent->addChild(mOgreNode);
}
#endif // IMPLEMENT_GRAPHICS

/**
  This method invalidates graphics. The result would be that the next time update
  is called, the node will update it's render information.
*/
void Klaus::RSG2::GraphicalNode::invalidateGraphics()
{
  mInvalid = true;

  Node * node = this;
  while(node->getParent())
  {
    node = node->getParent();
    if(dynamic_cast<GraphicalNode*>(node))
    {
      dynamic_cast<GraphicalNode*>(node)->invalidateGraphics();
      break;
    }
  }
}

/**
  This method validates graphical data
*/
void Klaus::RSG2::GraphicalNode::validateGraphics()
{
  // If nothing out a date do nothin
  if(!mInvalid)
    return;

  try
  {
    updateGraphics();
    mInvalid = false;
  }
  #ifdef IMPLEMENT_GRAPHICS
  catch(Ogre::Exception & e)
  {
    error("(GraphicalNode) update: ogre exception: %s", e.what());
  }
  #endif
  catch(const char * str)
  {
    error("(GraphicalNode) update: exception: %s", str);
  }
  catch(...)
  {
    error("(GraphicalNode) update: unknown exception thrown" );
  }

  // now update the children
  for(size_t i = 0; i < mGChildren.size(); i++)
    mGChildren[i]->validateGraphics();
}




/**********************************************************************/
/*************************  class MeshBasedNode  **********************/
/**********************************************************************/

/**
  Constructor of MeshBasedNode. Sets the mesh scale variable
  name and initializes class members.
*/
Klaus::RSG2::MeshBasedNode::MeshBasedNode() : mMeshScale("Scale")
{
  mOgreMesh = NULL;
  mMeshScale.mValue.setPosition(1, 1, 1);
}

/**
  This method checks whether the given model name is one of the models used
  in simspark log files. If it is, it converts the model name to an actual
  name which application has provided resources for it.
  @param strModel Model name to check and convert
  @return Converted model name if it is a simspark model, the model name itself if it is not
*/
std::string Klaus::RSG2::MeshBasedNode::convertModelName(const std::string & strModel)
{
  if(strModel == "models/naosoccerfield.obj")
    return "NaoSoccerField.mesh";
  if(strModel == "models/skybox.obj")
    return "NaoSkyBox.mesh";
  if(strModel == "models/leftgoal.obj")
    return "NaoLeftGoal.mesh";
  if(strModel == "models/rightgoal.obj")
    return "NaoRightGoal.mesh";
  if(strModel == "StdUnitBox")
    return "Cube.mesh";
  if(strModel == "StdUnitSphere")
    return "Sphere.mesh";
  if(strModel == "StdCapsule")
    return "CCylinder.mesh";
  if(strModel == "StdUnitCylinder")
    return "Cylinder.mesh";
  if(strModel == "models/soccerball.obj")
    return "NaoSoccerBall.mesh";
  if(strModel == "models/naohead.obj")
    return "NaoHead.mesh";
  if(strModel == "models/naobody.obj")
    return "NaoBody.mesh";
  if(strModel == "models/lupperarm.obj")
    return "NaoLeftUpperArm.mesh";
  if(strModel == "models/rupperarm.obj")
    return "NaoRightUpperArm.mesh";
  if(strModel == "models/llowerarm.obj")
    return "NaoLeftLowerArm.mesh";
  if(strModel == "models/rlowerarm.obj")
    return "NaoRightLowerArm.mesh";
  if(strModel == "models/lthigh.obj")
    return "NaoLeftThigh.mesh";
  if(strModel == "models/rthigh.obj")
    return "NaoRightThigh.mesh";
  if(strModel == "models/lshank.obj")
    return "NaoLeftShank.mesh";
  if(strModel == "models/rshank.obj")
    return "NaoRightShank.mesh";
  if(strModel == "models/lfoot.obj")
    return "NaoLeftFoot.mesh";
  if(strModel == "models/rfoot.obj")
    return "NaoRightFoot.mesh";
  if(strModel == "models/soccerbottorso.obj")
    return "SoccerBotTorso.mesh";

  return strModel;
}

/**
  This method checks whether the given material name is one of the used materials in
  simspark log files. If it is, it converts the material name to an actual name which
  application has provided resources for it.
  @param strMat material name to check it's resource name in application
  @return If the material is one of simspark materials, the application's equivalent
          name is returned, otherwise the material name itself will be returned.
*/
std::string Klaus::RSG2::MeshBasedNode::convertMaterialName(const std::string& strMat)
{
  if(strMat == "None_rcs-naofield.png")
    return "SimSpark/NaoSoccerField";
  else if(strMat == "Material_skyrender_0001.tif")
    return "SimSpark/Sky/01";
  else if(strMat == "Material_skyrender_0002.tif")
    return "SimSpark/Sky/02";
  else if(strMat == "Material_skyrender_0003.tif")
    return "SimSpark/Sky/03";
  else if(strMat == "Material_skyrender_0004.tif")
    return "SimSpark/Sky/04";
  else if(strMat == "Material_skyrender_0005.tif")
    return "SimSpark/Sky/05";
  else if(strMat == "Material_skyrender_0006.tif")
    return "SimSpark/Sky/06";
  else if(strMat == "soccerball_rcs-soccerball.png")
    return "SimSpark/NaoSoccerBall";

  return strMat;
}

/**
  This method sets the model name to use for graphical representation
  @note does not need to invalidate graphics. instantly does everything
  @param strModel Model name to use
*/
void Klaus::RSG2::MeshBasedNode::setModelName(const std::string& strModel)
{
  #ifdef IMPLEMENT_GRAPHICS
  // if the mesh has been changed, update it
  if(!mOgreMesh || mMeshName != strModel)
  {
    if(mConfig->useSimSparkModels())
      mMeshName = convertModelName(strModel);
    else
      mMeshName = strModel;

    if(mOgreMesh)
    {
      mOgreNode->detachObject(mOgreMesh);
      mSceneMgr->destroyEntity(mOgreMesh);
      mOgreMesh = NULL;
    }

    if(!mMeshName.empty())
    {
      mOgreMesh = mSceneMgr->createEntity(mOgreNode->getName(), mMeshName);
      mOgreNode->attachObject(mOgreMesh);
    }
  }
  #else
    mMeshName = strModel;
  #endif // IMPLEMENT_GRAPHICS
}

/**
  This method sets the material of the mesh.
  @param token Token containing the material names
*/
void Klaus::RSG2::MeshBasedNode::setMaterial(SToken token)
{
  mMaterials.clear();
  for(size_t i = 1; i < token.size(); ++i)
    if(mConfig->useSimSparkModels())
      mMaterials.push_back(convertMaterialName(readString(token[i])));
    else
      mMaterials.push_back(readString(token[i]));

  invalidateGraphics();
}

/**
  This method updates graphical attributes of node.
*/
void Klaus::RSG2::MeshBasedNode::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  try
  {
    // update the material if the material is single and an update is needed
    if(!mOgreMesh)
      return;

    if(mMaterials.size() == 1)
    {
      if(mOgreMesh->getSubEntity(0)->getMaterialName() != mMaterials[0])
        mOgreMesh->setMaterialName(mMaterials[0]);
    }

    mOgreNode->setScale((Ogre::Real)mMeshScale.mValue[0], (Ogre::Real)mMeshScale.mValue[1], (Ogre::Real)mMeshScale.mValue[2]);
  }
  catch(Ogre::Exception & e)
  {
    error("(MeshBasedNode) exception: %s", e.what());
    mOgreMesh = NULL;
    return;
  }
  catch(const char * strError)
  {
    error("(MeshBasedNode) exception: %s", strError);
    mOgreMesh = NULL;
    return;
  }

  #endif // IMPLEMENT_GRAPHICS
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::MeshBasedNode::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setMaterial" && cmd.size() > 1)
    setMaterial(cmd);
  else if(cmd[0] == "setScale" && cmd.size() == 4)
    mMeshScale.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd[0] == "setTransparent")
  {} // nothing needs to be done
  else
    return false;

  return true;
}




/**********************************************************************/
/*****************************  Class Box  ****************************/
/**********************************************************************/

/**
  Constructor of the Box. Creates extents variable and sets its default value
*/
Klaus::RSG2::Box::Box() : mExtents("Extents")
{
  mExtents.mValue.setPosition(1, 1, 1);

  setModelName("Cube.mesh");
}

/**
  This method updates graphical information of the box.
*/
void Klaus::RSG2::Box::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Klaus::RSG2::MeshBasedNode::updateGraphics();
  mOgreNode->scale((Ogre::Real)mExtents.mValue[0], (Ogre::Real)mExtents.mValue[1], (Ogre::Real)mExtents.mValue[2]);
  #endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Box::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::MeshBasedNode::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setExtents" && cmd.size() == 4)
    mExtents.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else
    return false;

  return true;
}




/**********************************************************************/
/***************************  Class Sphere  ***************************/
/**********************************************************************/

/**
  Constructor of Sphere. Creates its variables and sets default
  value for radius.
*/
Klaus::RSG2::Sphere::Sphere() : mRadius("Radius")
{
  mRadius.mValue = 1;

  setModelName("Sphere.mesh");
}

/**
  This method updates graphical information of the sphere
*/
void Klaus::RSG2::Sphere::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Klaus::RSG2::MeshBasedNode::updateGraphics();
  mOgreNode->scale((Ogre::Real)mRadius.mValue, (Ogre::Real)mRadius.mValue, (Ogre::Real)mRadius.mValue);
  #endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Sphere::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::MeshBasedNode::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setRadius" && cmd.size() == 2)
    mRadius.mValue = readNumber(cmd[1]);
  else
    return false;

  return true;
}




/**********************************************************************/
/***************************  Class Cylinder  *************************/
/**********************************************************************/

/**
  Constructor of Cylinder. Creates its variables and sets default
  value for length and radius
*/
Klaus::RSG2::Cylinder::Cylinder() : mRadius("Radius"), mLength("Length")
{
  mLength.mValue = 1;
  mRadius.mValue = 1;

  setModelName("Cylinder.mesh");
}

/**
  This method updates graphical information of the cylinder.
*/
void Klaus::RSG2::Cylinder::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Klaus::RSG2::MeshBasedNode::updateGraphics();
  mOgreNode->scale((Ogre::Real)mLength.mValue, (Ogre::Real)mRadius.mValue, (Ogre::Real)mRadius.mValue);
  #endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Cylinder::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::MeshBasedNode::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setParams" && cmd.size() == 3)
  {
    mRadius.mValue = readNumber(cmd[1]);
    mLength.mValue = readNumber(cmd[2]);
  }
  else
    return false;

  return true;
}




/**********************************************************************/
/**************************  Class CCylinder  *************************/
/**********************************************************************/

/**
  Constructor of CCylinder. Creates its variables and sets default
  value for length and radius
*/
Klaus::RSG2::CCylinder::CCylinder() : Cylinder()
{
  setModelName("CCylinder.mesh");
}




/**********************************************************************/
/****************************  Class Capsule **************************/
/**********************************************************************/

/**
  Constructor of Capsule.
*/
Klaus::RSG2::Capsule::Capsule() : Cylinder()
{
  setModelName("Capsule.mesh");
}




/**********************************************************************/
/*************************  Class SingleMatNode  **********************/
/**********************************************************************/

/**
  Constructor of SingleMatNode. Creates its variable.
*/
Klaus::RSG2::SingleMatNode::SingleMatNode() : mMeshDesc("Mesh Name")
{

}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::SingleMatNode::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::MeshBasedNode::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "load" && cmd.size() == 2)
    setModelName(readString(cmd[1]));
  else
    return false;

  return true;
}




/**********************************************************************/
/***************************  Class StaticMesh  ***********************/
/**********************************************************************/

/**
  Constructor of StaticMesh. Creates its variables and sets default
  value for extents and visibility
*/
Klaus::RSG2::StaticMesh::StaticMesh() : mVisible("Visible") , mExtents("Shape Size")
{
  mVisible.mValue = true;
  mExtents.mValue.setPosition(1, 1, 1);
}

/**
  This method parses a load command. This command loads a mesh into
  the static mesh node.
  @param token Token containing the load command and its mesh name
*/
void Klaus::RSG2::StaticMesh::parseLoadCommand(SToken token)
{
  // set model name
  setModelName(readString(token[1]));

  if(!mConfig->useSimSparkModels())
    return;

  // set it's parameters
  if(mMeshName == "CCylinder.mesh")
  {
    mExtents.mValue[0] = readNumber(token[2]);
    mExtents.mValue[1] = readNumber(token[2]);
    mExtents.mValue[2] = readNumber(token[3]);
  }
  else if(mMeshName == "Cylinder.mesh")
  {
    mExtents.mValue[0] = readNumber(token[2]);
    mExtents.mValue[1] = readNumber(token[2]);
    mExtents.mValue[2] = readNumber(token[3]);
  }
  else if(mMeshName == "Sphere.mesh")
  {
    if(token.size() == 3)
      mExtents.mValue[0] = mExtents.mValue[1] = mExtents.mValue[2] = readNumber(token[2]);
  }
  else if(mMeshName == "Cube.mesh")
  {
    if(token.size() == 3)
      mExtents.mValue[0] = mExtents.mValue[1] = mExtents.mValue[2] = readNumber(token[2]);
    else if(token.size() == 5)
    {
      mExtents.mValue[0] = readNumber(token[2]);
      mExtents.mValue[1] = readNumber(token[3]);
      mExtents.mValue[2] = readNumber(token[4]);
    }
  }
}

/**
  This method updates graphical information of the mesh.
*/
void Klaus::RSG2::StaticMesh::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Klaus::RSG2::MeshBasedNode::updateGraphics();
  mOgreNode->scale((Ogre::Real)mExtents.mValue[0], (Ogre::Real)mExtents.mValue[1], (Ogre::Real)mExtents.mValue[2]);
  mOgreNode->setVisible(mVisible.mValue);
  #endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::StaticMesh::parseRSGCommand(SToken cmd)
{
  if(cmd[0] == "load" && cmd.size() > 1)
    parseLoadCommand(cmd);
  else if(Klaus::RSG2::SingleMatNode::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setVisible" && cmd.size() == 2)
    mVisible.setValue(readString(cmd[1]));
  else if(cmd[0] == "resetMaterials")
    setMaterial(cmd);
  else if(cmd[0] == "sMat")
    setMaterial(cmd);
  else if(cmd[0] == "sSc" && cmd.size() == 4)
    mMeshScale.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else
    return false;

  return true;
}

/**
  This method is called whenever an update is available
  @param cmd The tokens containing the update command
  @return True on successful parse, false otherwise
*/
bool Klaus::RSG2::StaticMesh::parseRDSCommand(SToken cmd)
{
  if(cmd[0] == "setVisible" && cmd.size() == 2)
  {
    #ifdef IMPLEMENT_GRAPHICS
    mOgreNode->setVisible(cmd[1].boolean());
    #endif
  }
  else
  {
    error("(StaticMesh) unknown RDS command '%s'", cmd.str().c_str());
    return false;
  }

  return true;
}




/**********************************************************************/
/****************************  Class Light  ***************************/
/**********************************************************************/

/**
  Constructor of Light. Creates its variables and sets default
  value for it's attributes.
*/
Klaus::RSG2::Light::Light()
{
  mLight = NULL;

  memset(mAmbient, 0, 4*sizeof(double));
  mDiffuse[0] = mDiffuse[1] = mDiffuse[2] = mDiffuse[3] = 1.0;
  mSpecular[0] = mSpecular[1] = mSpecular[2] = mSpecular[3] = 1.0;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Light::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setDiffuse" && cmd.size() == 5)
  {
    mDiffuse[0] = readNumber(cmd[1]);
    mDiffuse[1] = readNumber(cmd[2]);
    mDiffuse[2] = readNumber(cmd[3]);
    mDiffuse[3] = readNumber(cmd[4]);
  }
  else if(cmd[0] == "setAmbient" && cmd.size() == 5)
  {
    mAmbient[0] = readNumber(cmd[1]);
    mAmbient[1] = readNumber(cmd[2]);
    mAmbient[2] = readNumber(cmd[3]);
    mAmbient[3] = readNumber(cmd[4]);
  }
  else if(cmd[0] == "setSpecular" && cmd.size() == 5)
  {
    mSpecular[0] = readNumber(cmd[1]);
    mSpecular[1] = readNumber(cmd[2]);
    mSpecular[2] = readNumber(cmd[3]);
    mSpecular[3] = readNumber(cmd[4]);
  }
  else
    return false;

  return true;
}

/**
  This method updates graphical attributes of the light.
*/
void Klaus::RSG2::Light::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  if(!mLight)
  {
    mLight = mSceneMgr->createLight(mOgreNode->getName() + "_light");
    mLight->setAttenuation(1000.0f, 0.5f, 0.02f, 0.0f);
    mOgreNode->attachObject(mLight);
  }

  // Ogre does not support per light ambient colours, so take the
  // global ambient as the maximum ambient of all lights
  Ogre::ColourValue c = mSceneMgr->getAmbientLight();
  if(mAmbient[0]+mAmbient[1]+mAmbient[2] > c.r+c.g+c.b)
    mSceneMgr->setAmbientLight(Ogre::ColourValue((Ogre::Real)mAmbient[0], (Ogre::Real)mAmbient[1], (Ogre::Real)mAmbient[2], (Ogre::Real)mAmbient[3]));

  mLight->setDiffuseColour(Ogre::ColourValue((Ogre::Real)mDiffuse[0], (Ogre::Real)mDiffuse[1], (Ogre::Real)mDiffuse[2], (Ogre::Real)mDiffuse[3]));
  mLight->setSpecularColour(Ogre::ColourValue((Ogre::Real)mSpecular[0], (Ogre::Real)mSpecular[1], (Ogre::Real)mSpecular[2], (Ogre::Real)mSpecular[3]));
  #endif
}

/**
  This method is called whenever an update is available
  @param cmd The tokens containing the update command
  @return True on successful parse, false otherwise
*/
bool Klaus::RSG2::Light::parseRDSCommand(SToken cmd)
{
  error("(Light) unknown command: '%s'", cmd.str().c_str());
  return false;
}




/**********************************************************************/
/****************************  Class Sky  *****************************/
/**********************************************************************/

/**
  Constructor of Sky. Sets sky type to unknown
*/
Klaus::RSG2::Sky::Sky()
{
  mSkyType = Illegal;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Sky::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setSkyBox" && cmd.size() == 3)
  {
    mMaterial = readString(cmd[1]);
    mDistance = readNumber(cmd[2]);
    mType     = SkyBox;
  }
  else if(cmd[0] == "setSkyDome" && cmd.size() == 3)
  {
    mMaterial  = readString(cmd[1]);
    mDistance  = readNumber(cmd[2]);
    mCurvature = readNumber(cmd[3]);
    mTiling    = readNumber(cmd[3]);
    mType      = SkyDome;
  }
  else
    return false;

  return true;
}

/**
  This method updates the sky. The sky can be a sky box or a sky dome.
*/
void Klaus::RSG2::Sky::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Ogre::SceneManager* mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  bool bSkyBox  = mgr->isSkyBoxEnabled();
  bool bSkyDome = mgr->isSkyDomeEnabled();

  if(mSkyType != SkyBox && bSkyBox)
    mgr->setSkyBox(false, "");

  if(mSkyType != SkyDome&& bSkyDome)
    mgr->setSkyDome(false, "");

  if(mSkyType == SkyBox && !bSkyBox )
    mgr->setSkyBox(true, mMaterial, (Ogre::Real)mDistance);
  else if(mSkyType == SkyDome && !bSkyDome )
    mgr->setSkyDome(true, mMaterial, (Ogre::Real)mCurvature, (Ogre::Real)mTiling, (Ogre::Real)mDistance, true, Ogre::Quaternion(Ogre::Degree(90.0f), Ogre::Vector3::UNIT_X));

  #endif
}




/**********************************************************************/
/**************************  Class OgreMesh  **************************/
/**********************************************************************/

/**
  Constructor of OgreMesh. Creates its variable and sets default value for shadow casting.
*/
Klaus::RSG2::OgreMesh::OgreMesh() : mCastShadow("Cast Shadow")
{
  mCastShadow.mValue = true;
}

/**
  This method updates graphical information of the ogre mesh. also
  updates its shadow casting.
*/
void Klaus::RSG2::OgreMesh::updateGraphics()
{
  #ifdef IMPLEMENT_GRAPHICS
  Klaus::RSG2::StaticMesh::updateGraphics();
  mOgreMesh->setCastShadows(mCastShadow.mValue);
  #endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::OgreMesh::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::StaticMesh::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setCastShadow" && cmd.size() == 2)
    mCastShadow.setValue(readString(cmd[1]));
  else
    return false;

  return true;
}
