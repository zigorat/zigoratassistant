/*
*********************************************************************************
*         PhysicalNodes.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for physical scene nodes.     *
*            These nodes provide various utilities for physic related nodes     *
*            like transform and colliders.                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file PhysicalNodes.cpp
<pre>
<b>File:</b>          PhysicalNodes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for physical scene nodes.
               These nodes provide various utilities for physic related nodes
               like transform and colliders.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#include "PhysicalNodes.h"
#include "Joint.h"          // needed for Joint

#ifdef IMPLEMENT_GRAPHICS
#include <OgreSceneNode.h>  // needed for Ogre::SceneNode
#endif // IMPLEMENT_GRAPHICS




/********************************************************************/
/*************************  Class Space  ****************************/
/********************************************************************/

/**
  Constructor of Space. Sets its variable name and it's default value
*/
Klaus::RSG2::Space::Space() : mInnerCollision("Disable Inner Collision")
{
  mInnerCollision.mValue = false;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Space::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "disableInnerCollision")
    mInnerCollision.mValue = true;
  else
    return false;

  return true;
}




/********************************************************************/
/*********************  Class TransformableNode  ********************/
/********************************************************************/

/**
  Constructor of TransformableNode. Default values are set in
  Matrix4 consturctor. Only set default scale to (1, 1, 1) .
*/
Klaus::RSG2::TransformableNode::TransformableNode() :
        mDefaultTranslation("Relative Position"),
        mDefaultScale("Relative Scale"),
        mDefaultRotation("Relative Rotation")
{
  mDefaultScale.mValue.setPosition(1, 1, 1);
  mParentTransform = NULL;
}

/**
  Destrcutor of TransformableNode.
*/
Klaus::RSG2::TransformableNode::~TransformableNode()
{
}

/**
  This method parses a setLocalTransform command. This command sets
  the transform of the node with the indices of a full 4x4
  transformation matrix.
*/
void Klaus::RSG2::TransformableNode::parseSLT( SToken cmd )
{
  mRelativeTransform = Matrix4(
                                cmd[1], cmd[5], cmd[ 9], cmd[13],
                                cmd[2], cmd[6], cmd[10], cmd[14],
                                cmd[3], cmd[7], cmd[11], cmd[15],
                                cmd[4], cmd[8], cmd[12], cmd[16]
                              );
  invalidateGraphics();
}

/**
  This method calculates global transform of the node.
  First it tries to calculate based on the node's children who have a
  previously calculated global transform. If it fails, it will try to
  calculate based on it's parent. If neighter succeeded, an error message
  will be shown but application will continue to execute.
*/
void Klaus::RSG2::TransformableNode::findGlobalTransform()
{
  // try to calculate based on an updated child
  for(size_t i = 0; i < mChildTransforms.size(); ++i)
  {
    TransformableNode *tr = mChildTransforms[i];

    if(!tr->mTransformUpdated)
      tr->findGlobalTransform();

    if(tr->mTransformUpdated)
    {
      mGlobalTransform = tr->getDefaultTransform().getInverseMatrix() * tr->mGlobalTransform;
      mTransformUpdated = true;
      return;
    }
  }

  // also check the parent
  if(mParentTransform)
  {
    mGlobalTransform = mParentTransform->getGlobalTransform() * getDefaultTransform();
    mTransformUpdated = true;
  }
}

/**
  This method updates graphical information of the node. For Transformable
  nodes this updates the nodes relative position to its parents.
*/
void Klaus::RSG2::TransformableNode::updateGraphics()
{
  VecPosition p,s(1, 1, 1);
  Quaternion q;

  mRelativeTransform.dissect(p,q);
  if(mType != "TRF")
    s = mDefaultScale.mValue;

#ifdef IMPLEMENT_GRAPHICS
  mOgreNode->setPosition((Ogre::Real)p[0], (Ogre::Real)p[1], (Ogre::Real)p[2]);
  mOgreNode->setOrientation((Ogre::Real)q[0], (Ogre::Real)q[1], (Ogre::Real)q[2], (Ogre::Real)q[3]);
  mOgreNode->setScale((Ogre::Real)s[0], (Ogre::Real)s[1], (Ogre::Real)s[2]);
#endif
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  For trannsformable nodes this properties can be set:
    - local position (relative to the node's parent)
    - local rotation (relative to the node's parent)
    - scale (relative to the node's parent)
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::TransformableNode::parseRSGCommand(SToken cmd)
{
  if(Node::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setLocalPos" && cmd.size() == 4)
    mDefaultTranslation.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd[0] == "setLocalRotation" && cmd.size() == 4)
    mDefaultRotation.mValue.fromEulerAngles(VecPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3])));
  else if(cmd[0] == "setScale" && cmd.size() == 4)
    mDefaultScale.mValue = VecPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd[0] == "SLT" && cmd.size() == 17)
    parseSLT(cmd);
  else
    return false;

  return true;
}

/**
  Called before physics update process. Resets update flag for use in postPhysicsInternal.
*/
void Klaus::RSG2::TransformableNode::prePhysicsInternal()
{
  mTransformUpdated = false;
}

/**
  Called after physics update to update transform nodes from body node's
  transformation. Forward Kinematics also completes calculation here.
*/
void Klaus::RSG2::TransformableNode::postPhysicsInternal()
{
  if(!mTransformUpdated)
  {
    findGlobalTransform();

    if(!mTransformUpdated)
    {
      error("(TransformableNode) could not update hierarchy for ['%s', '%s']", mType.c_str(), mName.mValue.c_str());
      return;
    }
  }

  // if the control reaches here, which should normally reach
  // it means that the global transformation of the node is
  // calculated. So just update the realtive transformation
  //
  // find the parent transform node
  // and make the node update it's graphics on next frame
  invalidateGraphics();

  Node * node = mParent;
  TransformableNode * tr_parent;
  while(node)
  {
    tr_parent = dynamic_cast<TransformableNode*>(node);
    if(tr_parent)
    {
      mRelativeTransform = tr_parent->getGlobalTransform().getInverseMatrix() * mGlobalTransform;
      return;
    }

    node = node->getParent();
  }

  // for the top most transform node, the relative transform is equal to it's global transform.
  mRelativeTransform = mGlobalTransform;
}

/**
  This method is called after the loading of the scene graph. Now that the
  transform node has it's default values parsed and loaded, it can initialize
  it's relative and global transforms.
 */
void Klaus::RSG2::TransformableNode::postLoadInternal()
{
  GraphicalNode::postLoadInternal();

  // reset mRelativeTransform
  mRelativeTransform = getDefaultTransform();

  // initialize global transform
  if(mParent)
    mGlobalTransform = mRelativeTransform * mParent->getGlobalTransform();
  else
    mGlobalTransform = mRelativeTransform;

  // find out all children which are also transforms
  for(size_t i = 0; i < mChildren.size(); ++i)
    if(dynamic_cast<TransformableNode*>(mChildren[i]))
      mChildTransforms.push_back(dynamic_cast<TransformableNode*>(mChildren[i]));

  // find parent transform
  Node * nd = this;
  while(nd->getParent())
  {
    nd = nd->getParent();
    TransformableNode * tr = dynamic_cast<TransformableNode*>(nd);
    if(tr)
    {
      mParentTransform = tr;
      break;
    }
  }
}

/**
  This method returns default transform of the node
  @return Default trasnformation of the node
*/
Klaus::Matrix4 Klaus::RSG2::TransformableNode::getDefaultTransform() const
{
  if(mDefaultScale.mValue != VecPosition(1, 1, 1))
    return Matrix4::createTransform(mDefaultTranslation.mValue, mDefaultRotation.mValue, mDefaultScale.mValue);
  else
    return Matrix4::createTransform(mDefaultTranslation.mValue, mDefaultRotation.mValue);
}

/**
  This method sets relative transform of the node to its upper transform node
  @param tr The relative transformation matrix of the node to its upper transform node
*/
void Klaus::RSG2::TransformableNode::setRelativeTransform(const Klaus::Matrix4& tr)
{
  mRelativeTransform = tr;
}

/**
  This method sets relative transform of the node to the top most transform node
   Also sets the update flag to updated in order to calculate relative transform
  @param tr The relative transformation matrix of the node to the top most transform node
*/
void Klaus::RSG2::TransformableNode::setGlobalTransform(const Klaus::Matrix4& tr)
{
  mGlobalTransform = tr;
  mTransformUpdated = true;
}

/**
  This method returns the global position of the transformable
  node. Overrides the default Node method.
  @return The global transform of the node.
*/
Klaus::Matrix4 Klaus::RSG2::TransformableNode::getGlobalTransform() const
{
  return mGlobalTransform;
}

/**
  This method returns the local transform of the Transformable node.
  Overrides the default Node class method
  @return The local transform of the node relative to its parent
*/
Klaus::Matrix4 Klaus::RSG2::TransformableNode::getLocalTransform() const
{
  return mRelativeTransform;
}

/**
  This method is called whenever an update is available.
  SLT (setLocalTransform) command: This command sets
  the local transform of the node. Also resets mRelativeTransform. This
  command only exists in a TRF node which are only found in log files and
  simulation data. They are not designed to apply kinematics upon, so
  they can only set their relative transformation.
  @param cmd The tokens containing the update command
  @return True on successful parse, false otherwise
*/
bool Klaus::RSG2::TransformableNode::parseRDSCommand(SToken cmd)
{
  size_t size = cmd.size();
  if((cmd[0] == "SLT") && (size == 17))
    parseSLT(cmd);
  else
  {
    error("(TRF) unknown delta command %s", cmd.str().c_str());
    return false;
  }

  return true;
}




/********************************************************************/
/*************************  Class Body  *****************************/
/********************************************************************/

/**
  Constructor of Body. Sets default values and makes all pointers null.
*/
Klaus::RSG2::Body::Body()
{
  mTotalMass = -1;
  mBodyParent = NULL;
  mJoint = NULL;
  mBodyTransform = NULL;
}

/**
  Destructor of Body. Does nothing
*/
Klaus::RSG2::Body::~Body()
{

}

/**
  This method updates the body's transform parent node with the given transformation.
  @param tr Desired global transformation for the parent transform node
*/
void Klaus::RSG2::Body::setParentTransform(const Klaus::Matrix4& tr)
{
  if(mBodyTransform)
    mBodyTransform->setGlobalTransform(tr);
}

/**
  This method calculates forward kinematics for the body node.
  The global position of the node is calculated using the
  default transform of the node and it's joint's rotation value
  and then it's holding transformable node will be updated with
  the new information.
  @param time_gap The time gap to take in account for joint rotations
  @param body_sheet The sheet to save the body information
*/
void Klaus::RSG2::Body::getForwardKinematics(const double & time_gap, Kinematics & body_sheet)
{
  // update trasnformations
  Quaternion qRot = mJoint->getJointRotation(time_gap);
  Matrix4 tr_local = Matrix4::createTransform(qRot * mPosRelPivot + mPivot, qRot);
  Matrix4 tr_global = body_sheet[mBodyParent->getBodyName()].mTransform * tr_local;

  BodyInfo i;
  i.mMass = mMass;
  i.mTransform = tr_global;
  body_sheet[getBodyName()] = i;

  // update children
  for(size_t i = 0; i < mBodyChildren.size(); ++i)
    mBodyChildren[i]->getForwardKinematics(time_gap, body_sheet);
}

/**
  This method calculates forward kinematics for the body node.
  The global position of the node is calculated using the
  default transform of the node and it's joint's rotation value
  and then it's holding transformable node will be updated with
  the new information.  The joint angle will be considered as given
  in the kinematic sheet
  @param angles The angle values of an inverse kinematics
  @param body_sheet The sheet to save the body information
*/
void Klaus::RSG2::Body::getForwardKinematicsFor(const InverseKinematics & angles, Kinematics & body_sheet)
{
  if(mJoint && mJoint->getPerceptor())
  {
    InverseKinematics::const_iterator it = angles.find(mJoint->getPerceptor()->getName());

    if(it == angles.end())
      error("(Body) angle not existing for %s", mJoint->getPerceptor()->getName().c_str());
    else
    {
      Quaternion qRot;
      if(mJoint->getJointType()== Hinge)
        qRot = Quaternion(it->second.mAngles[0], mJoint->getAxis(0));
      else if(mJoint->getJointType() == Universal)
        qRot = Quaternion(it->second.mAngles[1], mJoint->getAxis(0)) *
               Quaternion(it->second.mAngles[0], mJoint->getAxis(0));  // update trasnformations

      Matrix4 tr_local = Matrix4::createTransform(qRot * mPosRelPivot + mPivot, qRot);
      Matrix4 tr_global = body_sheet[mBodyParent->getBodyName()].mTransform * tr_local;

      BodyInfo i;
      i.mMass = mMass;
      i.mTransform = tr_global;
      body_sheet[getBodyName()] = i;
    }
  }

  // update children
  for(size_t i = 0; i < mBodyChildren.size(); ++i)
    mBodyChildren[i]->getForwardKinematicsFor(angles, body_sheet);
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Body::parseRSGCommand(SToken cmd)
{
  if(RSG2::Node::parseRSGCommand(cmd))
    return true;

  size_t size = cmd.size();
  if(cmd[0] == "setBoxTotal" && size == 5)
  {
    mBodyType = "BoxTotal";
    mMass = readNumber(cmd[1]);        // mass
    mExtents[0] = readNumber(cmd[2]);  // length
    mExtents[1] = readNumber(cmd[3]);  // width
    mExtents[2] = readNumber(cmd[4]);  // height
  }
  else if(cmd[0] == "setBox" && size == 5)
  {
    mBodyType = "Box";
    mExtents[0] = readNumber(cmd[2]);  // length
    mExtents[1] = readNumber(cmd[3]);  // width
    mExtents[2] = readNumber(cmd[4]);  // height
    mMass = readNumber(cmd[1]);        // density
    mMass = mMass * mExtents[0] * mExtents[1] * mExtents[2]; // now it is mass
  }
  else if(cmd[0] == "setSphereTotal" && size == 4)
  {
    mBodyType = "SphereTotal";
    mMass = readNumber(cmd[1]);       // mass
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = mExtents[0];        // radius z
  }
  else if(cmd[0] == "setSphere" && size == 3)
  {
    mBodyType = "Sphere";
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = mExtents[0];        // radius z
    mMass = readNumber(cmd[1]);       // density
    mMass = mMass * 4 * M_PI * mExtents[0] * mExtents[0] * mExtents[0] / 3; // now it is mass
  }
  else if(cmd[0] == "setCylinderTotal" && size == 4)
  {
    mBodyType = "CylinderTotal";
    mMass = readNumber(cmd[1]);       // total mass
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
  }
  else if(cmd[0] == "setCylinder" && size == 4)
  {
    mBodyType = "Cylinder";
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
    double v = M_PI * mExtents[0] * mExtents[0] * mExtents[2]; // volume
    mMass = v * readNumber(cmd[1]);   // mass = volume * density
  }
  else if(cmd[0] == "setCappedCylinderTotal" && size == 4)
  {
    mBodyType = "CCylinderTotal";
    mMass = readNumber(cmd[1]);       // total mass
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
  }
  else if(cmd[0] == "setCappedCylinder" && size == 4)
  {
    mBodyType = "CCylinder";
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
    double v = M_PI * mExtents[0] * mExtents[0] * mExtents[2];  // volume
    mMass = v * readNumber(cmd[1]);   // mass = volume * density
  }
  else if(cmd[0] == "setCapsuleTotal" && size == 4)
  {
    mBodyType = "CapsuleTotal";
    mMass = readNumber(cmd[1]);       // total mass
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
  }
  else if(cmd[0] == "setCapsule" && size == 4)
  {
    mBodyType = "Capsule";
    mExtents[0] = readNumber(cmd[2]); // radius x
    mExtents[1] = mExtents[0];        // radius y
    mExtents[2] = readNumber(cmd[3]); // height z
    double v = M_PI * mExtents[0] * mExtents[0] * mExtents[2] + 4.0/3.0*M_PI*mExtents[0]*mExtents[0]*mExtents[0]; // volume
    mMass = v * readNumber(cmd[1]);   // mass = volume * density
  }
  else
    return false;

  return true;
}

/**
  This method is called after loading the whole scene to find
  out the node's holding transformable node.
*/
void Klaus::RSG2::Body::postLoadInternal()
{
  Klaus::RSG2::Node::postLoadInternal();

  // find the transform node holding the body
  Node * node = this;
  TransformableNode * tr_node;

  while(node->getParent())
  {
    node = node->getParent();
    tr_node = dynamic_cast<TransformableNode*>(node);
    if(tr_node)
    {
      mBodyTransform = tr_node;

      return;
    }
  }

  error("(Body) no transform holding the body");
}

/**
  This method returns mass of the node. It can return the
  sum of masses of the node and it's children in a recursive way.
  @param bRecurse Whether to include the children's mass or not
  @return The mass of the node
*/
double Klaus::RSG2::Body::getMass(bool bRecurse)
{
  if(!bRecurse)
    return mMass;

  if(mTotalMass != -1)
    return mTotalMass;

  mTotalMass = mMass;
  for(size_t i = 0; i < mBodyChildren.size(); i++)
    mTotalMass += mBodyChildren[i]->getMass(true);

  return mTotalMass;
}

/**
  This method returns the parent transform name for the body
  @return Name of the parent transform
*/
std::string Klaus::RSG2::Body::getBodyName() const
{
  return mBodyTransform->getName();
}

/**
  This method starts calculation of forward kinematics with
  providing the the parent transform node with the exact
  global transformation of the body.
  @param tr The transformation of the body in Global perspective
  @param time_gap Time gap to predict joint movements
  @param body_sheet The sheet to save calculated information to
*/
void Klaus::RSG2::Body::getForwardKinematics(const Klaus::Matrix4& tr, const double& time_gap, Kinematics& body_sheet)
{
  BodyInfo i;
  i.mMass = mMass;
  i.mTransform = tr;
  body_sheet[getBodyName()] = i;

  // now update all body children
  for(size_t i = 0; i < mBodyChildren.size(); ++i)
    mBodyChildren[i]->getForwardKinematics(time_gap, body_sheet);
}

/**
  This method starts calculation of forward kinematics with
  providing the the parent transform node with the exact
  global transformation of the body. This method uses angles from
  the given kinematic sheet
  @param tr The transformation of the body in Global perspective
  @param angles Angles to calculate forward kinematics from
  @param body_sheet The sheet to save calculated information to
*/
void Klaus::RSG2::Body::getForwardKinematicsFor(const Klaus::Matrix4& tr, const InverseKinematics & angles, Kinematics& body_sheet)
{
  BodyInfo i;
  i.mMass = mMass;
  i.mTransform = tr;
  body_sheet[getBodyName()] = i;

  // now update all body children
  for(size_t i = 0; i < mBodyChildren.size(); ++i)
    mBodyChildren[i]->getForwardKinematicsFor(angles, body_sheet);
}

/**
  This method sets the global transformation of the parent transform node
  @param sheet The sheet that contains the transformation values
*/
void Klaus::RSG2::Body::applyForwardKinematics(const Klaus::Kinematics& sheet)
{
  // set the parent transform
  Kinematics::const_iterator it = sheet.find(getBodyName());
  if(it == sheet.end())
  {
    error("(Body) can not find out parent transform for '%s' in the given sheet", getBodyName().c_str());
    return;
  }

  // now set it
  setParentTransform(it->second.mTransform);

  // update the children as well
  for(size_t i = 0; i < mBodyChildren.size(); ++i)
    mBodyChildren[i]->applyForwardKinematics(sheet);
}

/**
  This method attaches the body to it's parent body in physical
  graph. The joint which connects the two bodies is used to
  remember it's default transformation.
  @param body The body's parent node
  @param joint The joint that connects the two bodies
*/
void Klaus::RSG2::Body::attachToBody(Body* body, Joint* joint)
{
  mBodyParent = body;
  mJoint = joint;

  // the top most body node does not need to initialize anything.
  if(!mBodyParent || !mJoint)
  {
    error("(Body) what to initialize? nothing given");
    return;
  }

  // add self to the parent's children
  mBodyParent->mBodyChildren.push_back(this);

  // calculate pivot's global position
  VecPosition pGlobalPivot = getGlobalTransform() * mJoint->getAnchor();

  // and use it to find out needed relative stuff
  mPivot = pGlobalPivot - mBodyParent->getGlobalTransform().getTranslation();
  mPosRelPivot = getGlobalTransform().getTranslation() - pGlobalPivot;
}

/**
  This method checks whether the body is a root body node or not
  @return True if the node is the root body for physical graph , false otherwise
*/
bool Klaus::RSG2::Body::isRootBodyNode() const
{
  return mBodyParent == NULL;
}




/********************************************************************/
/***********************  Class DragController  *********************/
/********************************************************************/

/**
  Constructor of DragController. Sets the name of its variables.
*/
Klaus::RSG2::DragController::DragController() :
                           mLinearDrag("Linear Drag"),
                           mAngularDrag("Angular Drag")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::DragController::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  if(cmd[0] == "setLinearDrag" && cmd.size() == 2)
    mLinearDrag.mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setAngularDrag" && cmd.size() == 2)
    mAngularDrag.mValue = readNumber(cmd[1]);
  else
    return false;

  return true;
}




/********************************************************************/
/********************  Class ContactJointHandler  *******************/
/********************************************************************/

/**
  Constructor of ContactJointHandler. Sets the name of its variables.
*/
Klaus::RSG2::ContactJointHandler::ContactJointHandler() :
                         mBounceMode("Bounce Mode"),
                         mBounceValue("Bounce Value"),
                         mMinBounceVel("Minimum Bounce Velocity"),
                         mSlipMode("Slip Mode"),
                         mSlip1("Slip 1"),
                         mSlip2("Slip 2"),
                         mSoftERPMode("Soft ERP Mode"),
                         mSoftERP("Soft ERP"),
                         mSoftCFMMode("Soft CFM Modee"),
                         mSoftCFM("Soft CFM")
{

}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::ContactJointHandler::parseRSGCommand(SToken cmd)
{
  size_t size = cmd.size();

  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setContactBounceMode" && size == 2)
    mBounceMode.mValue = cmd[1].boolean();
  else if(cmd[0] == "setContactBounceValue" && size == 2)
    mBounceValue.mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setMinBounceVel" && size == 2)
    mMinBounceVel.mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setContactSlipMode" && size == 2)
    mSlipMode.mValue = cmd[1].boolean();
  else if(cmd[0] == "setContactSlip" && size >= 2)
  {
    mSlip1.mValue = readNumber(cmd[1]);
    mSlip2.mValue = cmd.size() == 3 ? readNumber(cmd[2]) : 0;
  }
  else if(cmd[0] == "setContactSoftERPMode" && size == 2)
    mSoftERPMode.mValue = cmd[1].boolean();
  else if(cmd[0] == "setContactSoftERP" && size == 2)
    mSoftERP.mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setContactSoftCFM" && size == 2)
  {
    try
    {
      mSoftCFM.mValue = readNumber(cmd[1]);
    }
    catch(SLangException e)
    {
      // the CFM mode is issued...
      mSoftCFMMode.mValue = cmd[1].boolean();
    }

  }
  else
    return false;

  return true;
}




/********************************************************************/
/************************  Class Collider  **************************/
/********************************************************************/

/**
  Constructor of Collider. Sets the name of variables.
*/
Klaus::RSG2::Collider::Collider() :
                mPosition("Local Position"),
                mDisabledColliders("Disabled Colliders")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Collider::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setLocalPosition" && cmd.size() == 4)
    mPosition.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd[0] == "addNotCollideWithColliderName" && cmd.size() == 3)
    mDisabledColliders.mValue.push_back(readString(cmd[1]));
  else
    return false;

  return true;
}




/********************************************************************/
/************************  Class BoxCollider  ***********************/
/********************************************************************/

/**
  Constructor of BoxCollider. Sets the name of its variables.
*/
Klaus::RSG2::BoxCollider::BoxCollider() : mExtents("Extents"), mRotation("Local Rotation")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::BoxCollider::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Collider::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setLocalRotation" && cmd.size() == 4)
    mRotation.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd[0] == "setBoxLengths" && cmd.size() == 4)
    mExtents.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else
    return false;

  return true;
}




/********************************************************************/
/***********************  Class SphereCollider  *********************/
/********************************************************************/

/**
  Constructor of SphereCollider. Sets the name of its variable.
*/
Klaus::RSG2::SphereCollider::SphereCollider() : mRadius("Radius")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::SphereCollider::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Collider::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setRadius" && cmd.size() == 2)
    mRadius.mValue = readNumber(cmd[1]);
  else
    return false;

  return true;
}




/********************************************************************/
/*********************  Class CCylinderCollider  ********************/
/********************************************************************/

/**
  Constructor of CCylinderCollider. Sets the name of variables.
*/
Klaus::RSG2::CCylinderCollider::CCylinderCollider() : mRadius("Radius"), mHeight("Height")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::CCylinderCollider::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setParams" && cmd.size() == 3)
  {
    mRadius.mValue = readNumber(cmd[1]);
    mHeight.mValue = readNumber(cmd[2]);
  }
  else
    return false;

  return true;
}




/********************************************************************/
/**********************  Class CapsuleCollider  *********************/
/********************************************************************/

/**
  Constructor of CapsuleCollider. Sets the name of variables.
*/
Klaus::RSG2::CapsuleCollider::CapsuleCollider() : mRadius("Radius"), mHeight("Height")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::CapsuleCollider::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setParams" && cmd.size() == 3)
  {
    mRadius.mValue = readNumber(cmd[1]);
    mHeight.mValue = readNumber(cmd[2]);
  }
  else
    return false;

  return true;
}
