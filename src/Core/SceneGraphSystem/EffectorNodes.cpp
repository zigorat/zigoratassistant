/*
*********************************************************************************
*         EffectorNodes.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for effector scene nodes.     *
*            These nodes provide command support for agents. Agent can send     *
*            commands specified by the effectors to the server to execute.      *
*                                                                               *
*********************************************************************************
*/

/*! \file EffectorNodes.cpp
<pre>
<b>File:</b>          EffectorNodes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for effector scene nodes.
               These nodes provide command support for agents. Agent can send
               commands specified by the effectors to the server to execute.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/


#include "EffectorNodes.h"
