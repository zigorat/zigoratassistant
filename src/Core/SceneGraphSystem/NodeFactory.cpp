/*
*********************************************************************************
*          NodeFactory.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 10/09/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definitions for NodeFactoryCore,          *
*            BaseNodeFactory and MainNodeFactory. BaseNodeFactory is a factory  *
*            class used to create appropriate instance of node parsers.         *
*            MainNodeFactory is the factory which creates parser instances      *
*            for regular simspark nodes and the NodeFactoryCore manages         *
*            registering and using scene node factories.                        *
*                                                                               *
*********************************************************************************
*/

/*! \file NodeFactory.cpp
<pre>
<b>File:</b>          NodeFactory.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/09/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definitions for NodeFactoryCore,
               BaseNodeFactory and MainNodeFactory. BaseNodeFactory is a factory
               class used to create appropriate instance of node parsers.
               MainNodeFactory is the factory which creates parser instances
               for regular simspark nodes and the NodeFactoryCore manages
               registering and using scene node factories.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/09/2009       Mahdi           Initial version created
</pre>
*/

#include "NodeFactory.h"
#include <libZigorat/Logger.h> // needed for Logger
#include "AuxilaryNodes.h"     // needed for declerations for auxilary nodes
#include "PhysicalNodes.h"     // needed for declerations for physical nodes
#include "PerceptorNodes.h"    // needed for all perceptor nodes
#include "EffectorNodes.h"     // needed for all effector nodes
#include "GraphicalNodes.h"    // needed for all graphical nodes
#include "Joint.h"             // needed for joint


/*! Singleton instance of the NodeFactoryCore. */
template<> Klaus::RSG2::NodeFactoryCore* Klaus::Singleton<Klaus::RSG2::NodeFactoryCore>::mInstance = 0;




/****************************************************************/
/******************  Class MainNodeFactory  *********************/
/****************************************************************/

/**
  Constructor of MainNodeFactory. Sets it's name.
*/
Klaus::RSG2::MainNodeFactory::MainNodeFactory() : BaseNodeFactory("Primary")
{
}

/**
  All parser nodes for regular simspark nodes will be created here
  according to their name.
  @param strNode Name of the node
  @return Parser for the node
*/
Klaus::RSG2::Node* Klaus::RSG2::MainNodeFactory::createParser(const std::string& strNode)
{
  // auxilary nodes
  if(strNode == "RootSceneNode")
    return new Root;
  if(strNode == "AgentState")
    return new AgentState;
  if(strNode == "Camera")
    return new Camera;
  if(strNode == "ObjectState")
    return new ObjectState;

  // physical nodes
  if(strNode == "Transform")
    return new Transform;
  if(strNode == "TRF")
    return new Transform;
  if(strNode == "Space")
    return new Space;
  if(strNode == "AgentAspect")
    return new AgentAspect;
  if(strNode == "Body")
    return new Body;
  if(strNode == "RigidBody")
    return new Body;
  if(strNode == "DragController")
    return new DragController;
  if(strNode == "ContactJointHandler")
    return new ContactJointHandler;
  if(strNode == "TouchPerceptorHandler")
    return new TouchPerceptorHandler;
  if(strNode == "BoxCollider")
    return new BoxCollider;
  if(strNode == "SphereCollider")
    return new SphereCollider;
  if(strNode == "CCylinderCollider")
    return new CCylinderCollider;
  if(strNode == "CapsuleCollider")
    return new CapsuleCollider;
  if(strNode == "TransformCollider")
    return new TransformCollider;

  // perceptor nodes
  if(strNode == "PerceptorHandler")
    return new PerceptorHandler;
  if(strNode == "TimePerceptor")
    return new TimePerceptor;
  if(strNode == "GameStatePerceptor")
    return new GameStatePerceptor;
  if(strNode == "GyroRatePerceptor")
    return new GyroRatePerceptor;
  if(strNode == "Accelerometer")
    return new Accelerometer;
  if(strNode == "TouchPerceptor")
    return new TouchPerceptor;
  if(strNode == "HearPerceptor")
    return new HearPerceptor;
  if(strNode == "ForceResistancePerceptor")
    return new ForceResistancePerceptor;
  if(strNode == "VisionPerceptor")
    return new UnrestrictedVisionPerceptor;
  if(strNode == "RestrictedVisionPerceptor")
    return new RestrictedVisionPerceptor;
  if(strNode == "HingePerceptor")
    return new HingeJointPerceptor;
  if(strNode == "UniversalJointPerceptor")
    return new UniversalJointPerceptor;

  // effector nodes
  if(strNode == "SingleMatInitEffector")
    return new SingleMatInitEffector;
  if(strNode == "BeamEffector")
    return new BeamEffector;
  if(strNode == "SayEffector")
    return new SayEffector;
  if(strNode == "StaticMeshInitEffector")
    return new StaticMeshInitEffector;
  if(strNode == "AgentSyncEffector")
    return new AgentSyncEffector;
  if(strNode == "HingeEffector")
    return new HingeJointEffector;
  if(strNode == "UniversalJointEffector")
    return new UniversalJointEffector;

  // graphical nodes
  if(strNode == "Box")
    return new Box;
  if(strNode == "Sphere")
    return new Sphere;
  if(strNode == "Cylinder")
    return new Cylinder;
  if(strNode == "CCylinder")
    return new CCylinder;
  if(strNode == "Capsule")
    return new Capsule;
  if(strNode == "SingleMatNode")
    return new SingleMatNode;
  if(strNode == "StaticMesh")
    return new StaticMesh;
  if(strNode == "SMN")
    return new StaticMesh;
  if(strNode == "Light")
    return new Light;
  if(strNode == "OgreMesh")
    return new OgreMesh;

  // joint
  if(strNode == "HingeJoint")
    return new Joint;
  if(strNode == "UniversalJoint")
    return new Joint;
  if(strNode == "FixedJoint")
    return new Joint;

  return NULL;
}




/****************************************************************/
/******************  Class NodeFactoryCore  *********************/
/****************************************************************/

/**
  Constructor of the NodeFactoryCore. Register the primary factory.
*/
Klaus::RSG2::NodeFactoryCore::NodeFactoryCore()
{
  registerFactory(new MainNodeFactory);
}

/**
  Destructor of the NodeFactoryCore. Unregisters all factories.
*/
Klaus::RSG2::NodeFactoryCore::~NodeFactoryCore()
{
  unregisterAll();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::RSG2::NodeFactoryCore * Klaus::RSG2::NodeFactoryCore::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new NodeFactoryCore;
    Logger::getSingleton()->log(1, "(NodeFactoryCore) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::RSG2::NodeFactoryCore::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(NodeFactoryCore) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(NodeFactoryCore) removed singleton");
  }
}

/**
  This method registers and adds the given factory to the list of
  factories which can produce nodes.
  @param factory The factory to add to the node factory.
*/
void Klaus::RSG2::NodeFactoryCore::registerFactory(BaseNodeFactory* factory)
{
  mFactories.push_back(factory);
  Logger::getSingleton()->log(28, "(NodeFactoryCore) registered node factory '%s'", factory->getName().c_str());
}

/**
  This method unregisters the requested factory be removing it out of the list of factories.
  @param factory The factory to unregister
*/
bool Klaus::RSG2::NodeFactoryCore::unregisterFactory(BaseNodeFactory* factory)
{
  for(size_t i = 0; i < mFactories.size(); ++i)
    if(mFactories[i] == factory)
    {
      mFactories.erase(mFactories.begin() + i);
      Logger::getSingleton()->log(28, "(NodeFactoryCore) unregistered node factory '%s'", factory->getName().c_str());
      return true;
    }

  return false;
}

/**
  This method unregisters all of the factories.
*/
void Klaus::RSG2::NodeFactoryCore::unregisterAll()
{
  Logger::getSingleton()->log(28, "(NodeFactoryCore) unregistered all node factories");
  mFactories.clear();
}

/**
  This method creates a parser for a specific node by quering all
  of the registered parse factories for the requested parser for
  node type.
  @param strNode Node type
  @return Parser for the requested node type
*/
Klaus::RSG2::Node* Klaus::RSG2::NodeFactoryCore::createInstanceFor(const std::string& strNode)
{
  for(size_t i = 0; i < mFactories.size(); ++i)
  {
    Node * instance = mFactories[i]->createParser(strNode);
    if(instance)
      return instance;
  }

  return NULL;
}
