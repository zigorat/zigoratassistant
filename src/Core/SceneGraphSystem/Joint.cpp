/*
*********************************************************************************
*              Joint.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for Joint. Joint is the       *
*            location where two body nodes connect. It is used to evaluate the  *
*            bodies relative to each other.graphical scene nodes.               *
*                                                                               *
*********************************************************************************
*/

/*! \file Joint.cpp
<pre>
<b>File:</b>          Joint.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for Joint. Joint is the
               location where two body nodes connect. It is used to evaluate the
               bodies relative to each other.graphical scene nodes.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#include "Joint.h"
#include "PhysicalNodes.h"

/*! Names that correspond to joint types */
std::string JointTypeNames[] = {"FixedJoint", "HingeJoint", "UniversalJoint", "?"};

/**
  Constructor of Joint. Set it's variables names.
*/
Klaus::RSG2::Joint::Joint()
{
  mAttachedNode1 = NULL;
  mAttachedNode2 = NULL;
  mJointType.mValue = Unknown;
  mPerceptor = NULL;

  mJointType.setName("Joint Type");
  mAnchor.setName("Pivot point");

  mAxis[0].setName("First Axis");
  mAxis[1].setName("Second Axis");
  mMaxMotorForce[0].setName("Maximum force for motor #1");
  mMaxMotorForce[1].setName("Maximum force for motor #2");
  mMaxSpeed[0].setName("Maximum speed for motor #1");
  mMaxSpeed[1].setName("Maximum speed for motor #2");
  mRanges[0][0].setName("Minimum range for motor #1");
  mRanges[0][1].setName("Maximum range for motor #2");
  mRanges[1][0].setName("Minimum range for motor #1");
  mRanges[1][1].setName("Maximum range for motor #2");
  mBounce[0].setName("Bounce value for motor #1");
  mBounce[1].setName("Bounce value for motor #2");
  mCFM[0].setName("CFM value for motor #1");
  mCFM[1].setName("CFM value for motor #2");
  mStopCFM[0].setName("Stop CFM value for motor #1");
  mStopCFM[1].setName("Stop CFM value for motor #2");
  mStopERP[0].setName("Stop ERP value for motor #1");
  mStopERP[1].setName("Stop ERP value for motor #2");
  mFudgeFactor[0].setName("Fudge factor value for motor #1");
  mFudgeFactor[1].setName("Fudge factor value for motor #2");
}

/**
  Destructor of Joint. Does nothing
*/
Klaus::RSG2::Joint::~Joint()
{
}

/**
  This method executes an attach command. The command will glue
  the given nodes to each other so that the second node is the
  parent of the first node.
  @param node1 The first node
  @param node2 The second body node
  @return True if the two nodes were found and attached, false otherwise
*/
bool Klaus::RSG2::Joint::attach(std::string node1, std::string node2)
{
  // Find the two bodies first and store them
  mAttachedNode1 = dynamic_cast<Body*>(getRelativeNode(node1));
  mAttachedNode2 = dynamic_cast<Body*>(getRelativeNode(node2));

  // check
  if(mAttachedNode1 && mAttachedNode2)
    return true;

  error("(Joint) can not attach '%s' to '%s'", node1.c_str(), node2.c_str());
  return false;
}

/**
  This method checks whether the joint has the given motor number or not
  @param iMotor The motor number
  @return True if the motor existed, false otherwise
*/
bool Klaus::RSG2::Joint::motorExists(const int& iMotor) const
{
  // A fixed joint has no motor to control
  if(mJointType.mValue == Fixed)
    return false;

  // A hinge joint has only one motor
  if(mJointType.mValue == Hinge)
    return iMotor == 0;

  // A universal joint has two motors
  if(mJointType.mValue == Universal)
    return (iMotor == 0) || (iMotor == 1);

  // unknown joint?!
  return false;
}

/**
  This method parses a setAxis command. This command sets the axis vector for the specified joint motor
  @param cmd The token containing the setAxis command
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Joint::parseAxis(SToken cmd)
{
  // get the axis vector
  VecPosition ax;
  if(cmd.size() == 2)
  {
    int t = Round(readNumber(cmd[1]));

    if(t == 0)
      ax.setPosition(1, 0, 0);
    else if(t == 1)
      ax.setPosition(0, 1, 0);
    else if(t == 2)
      ax.setPosition(0, 0, 1);
    else
    {
      error("(Joint) parse axis: unknown single digit axis: '%s'", cmd.str().c_str());
      return false;
    }
  }
  else
    ax.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));

  // now assign it
  if(cmd[0] == "setAxis" && mJointType.mValue == Hinge)
    mAxis[0].mValue = ax;
  else if(cmd[0] == "setAxis1" && mJointType.mValue == Universal)
    mAxis[0].mValue = ax;
  else if(cmd[0] == "setAxis2" && mJointType.mValue == Universal)
    mAxis[1].mValue = ax;
  else
  {
    error("(Joint) error parsing setAxis command: '%s'", cmd.str().c_str());
    return false;
  }

  return true;
}

/**
  This command parses a motor information command. This command sets various
  information about joint motors.
  @param cmd The command that contains joint information
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Joint::parseMotorInfo(SToken cmd)
{
  int index = Round(readNumber(cmd[1]));
  if(!motorExists(index))
  {
    error("(Joint) no such motor index: '%s'", cmd.str().c_str());
    return false;
  }

  if(cmd[0] == "setMaxMotorForce")
    mMaxMotorForce[index].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setCFM")
    mCFM[index].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setStopCFM")
    mStopCFM[index].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setStopERP")
    mStopERP[index].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setFudgeFactor")
    mFudgeFactor[index].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setBounce")
    mBounce[index].mValue = readNumber(cmd[2]) != 0;
  else if(cmd[0] == "setLowStopDeg")
    mRanges[index][0].mValue = readNumber(cmd[2]);
  else if(cmd[0] == "setHighStopDeg")
    mRanges[index][1].mValue = readNumber(cmd[2]);
  else
  {
    error("(Joint) unknown command: '%s'", cmd.str().c_str());
    return false;
  }

  return true;
}

/**
  This method parses a setJointMaxSpeed command. This command sets
  the maximum angular speed the joint can reach.
  @param cmd The token containing the setJointMaxSpeed command
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Joint::parseMaxSpeed(SToken cmd)
{
  if(cmd[0] == "setJointMaxSpeed" && mJointType.mValue == Hinge)
    mMaxSpeed[0].mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setJointMaxSpeed1" && mJointType.mValue == Universal)
    mMaxSpeed[0].mValue = readNumber(cmd[1]);
  else if(cmd[0] == "setJointMaxSpeed2" && mJointType.mValue == Universal)
    mMaxSpeed[1].mValue = readNumber(cmd[1]);
  else
  {
    error("(Joint) error parsing setJointMaxSpeed command: '%s'", cmd.str().c_str());
    return false;
  }

  return true;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Joint::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  if(mJointType.mValue == Unknown)
    mJointType.mValue = getTypeFromString(mType);

  size_t size = cmd.size();
       if((cmd[0].str().compare(0, 16, "setJointMaxSpeed") == 0) && (size == 2))
    return parseMaxSpeed(cmd);
  else if((cmd[0].str().compare(0,  7, "setAxis") == 0) && (size == 4 || size == 2))
    return parseAxis(cmd);
  else if(cmd[0] == "attach" && size == 3)
    return attach(readString(cmd[1]), readString(cmd[2]));
  else if(cmd[0] == "setAnchor" && size == 4)
    mAnchor.mValue.setPosition(readNumber(cmd[1]), readNumber(cmd[2]), readNumber(cmd[3]));
  else if(cmd.size() == 3)
    return parseMotorInfo(cmd);
  else if(cmd[0] == "setFixed" && size == 1)
    mJointType.mValue = Fixed;
  else
    return false;

  return true;
}

/**
  This method initilizes the perceptor and effector caches. Also
  calls the attached nodes to attach to themselves.
*/
void Klaus::RSG2::Joint::postLoadInternal()
{
  Klaus::RSG2::Node::postLoadInternal();

  if(mJointType.mValue == Fixed)
    return;

  for(size_t i = 0; i < mChildren.size(); ++i)
    if(dynamic_cast<JointPerceptor*>(mChildren[i]))
      mPerceptor = dynamic_cast<JointPerceptor*>(mChildren[i]);
    else if(dynamic_cast<JointEffector*>(mChildren[i]))
      mEffector = dynamic_cast<JointEffector*>(mChildren[i]);

  if(!mPerceptor)
    error("(Joint) perceptor not found");
  else if(!mEffector)
    error("(Joint) effector not found");
  else if(mAttachedNode2 && mAttachedNode1)
    mAttachedNode1->attachToBody(mAttachedNode2, this);
}

/**
  This method returns the joint type.
  @return Type of the joint
*/
Klaus::RSG2::JointType Klaus::RSG2::Joint::getJointType() const
{
  return (JointType)(mJointType.mValue);
}

/**
  This method returns the current angle of the joint.
  @param iMotor The motor number for gettings it's angle
  @return Current angle of the specified joint motor
*/
Klaus::Degree Klaus::RSG2::Joint::getAngle(const int& iMotor) const
{
  if(!motorExists(iMotor))
    return 0.0;
  else if(mPerceptor)
    return mPerceptor->getAngle(iMotor);
  else
    return 0.0;
}

/**
  This method sets the angle for the first joint motor
  @param dAngle1 First motor's angle
  @return True on successfull update, false otherwise
*/
bool Klaus::RSG2::Joint::setAngle(const Degree& dAngle1)
{
  if(!mPerceptor)
    return false;

  return setAngle(dAngle1, mPerceptor->getAngle(1));
}

/**
  This method sets the current angle of a universal joint
  @param dAngle1 Current angle of the joint's first motor
  @param dAngle2 Current angle of the joint's second motor
  @return True if the joint is not a fixed joint, false otherwise
*/
bool Klaus::RSG2::Joint::setAngle(const Degree& dAngle1, const Degree & dAngle2)
{
  if(!mPerceptor)
    return false;

  Degree ang[2];
  ang[0] = dAngle1;
  ang[1] = dAngle2;

  // apply joint restriction
  if(ang[0] > mRanges[0][1].mValue)
    ang[0] = mRanges[0][1].mValue;
  else if(ang[0] < mRanges[0][0].mValue)
    ang[0] = mRanges[0][0].mValue;

  if(ang[1] > mRanges[1][1].mValue)
    ang[1] = mRanges[1][1].mValue;
  else if(ang[1] < mRanges[1][0].mValue)
    ang[1] = mRanges[1][0].mValue;

  mPerceptor->overrideAngle(ang[0], ang[1]);
  return true;
}

/**
  This method returns the angular velocity of the joint. The rate at which
  the motor is turning the joint's angle.
  @param iMotor The motor number to return it's rate
  @return The angular velocity of the specified joint motor
*/
Klaus::Degree Klaus::RSG2::Joint::getSpeed(const int& iMotor) const
{
  if(!motorExists(iMotor))
    return 0.0;
  else if(mPerceptor)
    return mPerceptor->getRate(iMotor);
  else
    return 0.0;
}

/**
  This method sets the angular velocity of the joint motor
  @param dSpeed1 Speed for the first joint motor
  @return True on successful update, false otherwise
*/
bool Klaus::RSG2::Joint::setSpeed(const Degree& dSpeed1)
{
  if(!mPerceptor)
    return false;

  return setSpeed(dSpeed1, mPerceptor->getRate(1));
}

/**
  This method sets the current angular velocity of the joint for it's
  specified motor number
  @param dSpeed1 The speed at which currently the joint's first motor is turning
  @param dSpeed2 The speed at which currently the joint's second motor is turning
  @return True if the specified joint motor exists, false otherwise
*/
bool Klaus::RSG2::Joint::setSpeed(const Degree& dSpeed1, const Degree& dSpeed2)
{
  if(!mPerceptor)
    return false;

  Degree ang[2];
  ang[0] = dSpeed1;
  ang[1] = dSpeed2;

  // apply joint restriction
  if(ang[0].abs() > mMaxSpeed[0].mValue)
    ang[0] = sign(ang[0].getDegree()) * mMaxSpeed[0].mValue;

  if(ang[1].abs() > mMaxSpeed[1].mValue)
    ang[1] = sign(ang[1].getDegree()) * mMaxSpeed[1].mValue;

  mPerceptor->overrideRate(ang[0], ang[1]);
  return true;
}

/**
  This method returns the minimum angle that the joint motor can have
  @param iMotor the joint motor number
  @return The minimum supported angle of the joint motor
*/
Klaus::Degree Klaus::RSG2::Joint::getMinAngle(const int& iMotor) const
{
  if(!motorExists(iMotor))
    error("(Joint) no such motor to get it's minimum angle: %d", iMotor);
  else
    return mRanges[iMotor][0].mValue;

  return 0.0;
}

/**
  This method returns the maximum supported angle that the joint motor can have
  @param iMotor The motor number to get it's maximum angle
  @return Maximum angle of the specified joint motor
*/
Klaus::Degree Klaus::RSG2::Joint::getMaxAngle(const int& iMotor) const
{
  if(!motorExists(iMotor))
    error("(Joint) no such motor to get it's maximum angle: %d", iMotor);
  else
    return mRanges[iMotor][1].mValue;

  return 0.0;
}

/**
  This method returns the range in which the specified joint's
  motor can have it's angle
  @param dMin Minimum angle will be copied here
  @param dMax Maximum angle will be copied here
  @param iMotor The motor number to get it's angle range
  @return True if the specified joint motor existed, false otherwise
*/
bool Klaus::RSG2::Joint::getAngleRange(double& dMin, double& dMax, const int& iMotor) const
{
  if(!motorExists(iMotor))
    return false;

  dMin = mRanges[iMotor][0].mValue;
  dMax = mRanges[iMotor][1].mValue;
  return true;
}

/**
  This method returns the relative position of the pivot point for
  the joint to it's first attached body
  @return Joint's pivot point relative to it's first attached body
*/
Klaus::VecPosition Klaus::RSG2::Joint::getAnchor() const
{
  return mAnchor.mValue;
}

/**
  This method returns the global point of the joint anchor
  @return Global position of the pivot
*/
Klaus::VecPosition Klaus::RSG2::Joint::getPivot() const
{
  return mAttachedNode1->getGlobalTransform() * mAnchor.mValue;
}

/**
  This method returns the current rotation quaternion of the joint,
  taking in account all of it's motor's angles
  @param time_gap The gap in time to evaluate the joint rotation
  @return The current joint rotation
*/
Klaus::Quaternion Klaus::RSG2::Joint::getJointRotation(const double & time_gap) const
{
  if(!mPerceptor)
  {
    error("(Joint) no perceptor for joint");
    return Quaternion::identity;
  }
  else if(mJointType.mValue == Fixed)
    return Quaternion::identity;
  else if(mJointType.mValue == Hinge)
    return Quaternion(mPerceptor->getAngle() + mPerceptor->getRate() * time_gap, mAxis[0].mValue);
  else if(mJointType.mValue == Universal)
    return Quaternion(mPerceptor->getAngle(1) + mPerceptor->getRate(1) * time_gap, mAxis[1].mValue) *
           Quaternion(mPerceptor->getAngle(0) + mPerceptor->getRate(0) * time_gap, mAxis[0].mValue);

  error("(Joint) can not calculate joint rotation element for unknown joint type %s", getStringFromType((JointType)mJointType.mValue).c_str());
  return Quaternion::identity;
}

/**
  This method returns the first body node attached to the joint
  @return The first body node attached to the joint
*/
Klaus::RSG2::Body* Klaus::RSG2::Joint::getAttachedBody1() const
{
  return mAttachedNode1;
}

/**
  This method returns the second body node attached to the joint
  @return The second body node attached to the joint
*/
Klaus::RSG2::Body* Klaus::RSG2::Joint::getAttachedBody2() const
{
  return mAttachedNode2;
}

/**
  This method returns the perceptor node of the joint
  @return perceptor node of the joint
*/
Klaus::RSG2::JointPerceptor* Klaus::RSG2::Joint::getPerceptor()
{
  return mPerceptor;
}

/**
  This method returns the perceptor node of the joint
  @return perceptor node of the joint
*/
const Klaus::RSG2::JointPerceptor* Klaus::RSG2::Joint::getPerceptor() const
{
  return mPerceptor;
}

/**
  This method returns the effector node of the joint
  @return effector node of the joint
*/
Klaus::RSG2::JointEffector* Klaus::RSG2::Joint::getEffector()
{
  return mEffector;
}

/**
  This method returns the effector node of the joint
  @return effector node of the joint
*/
const Klaus::RSG2::JointEffector* Klaus::RSG2::Joint::getEffector() const
{
  return mEffector;
}

/**
  This method returns the axis vector for the specified joint motor number
  @param iNumber Number of the motor
  @return Axis vector of the specified motor
*/
Klaus::VecPosition Klaus::RSG2::Joint::getAxis(int iNumber) const
{
  if(iNumber < 0 || iNumber > 1)
  {
    error("(Joint) illegal motor number specified to get axis: %d", iNumber);
    return VecPosition();
  }

  return mAxis[iNumber].mValue;
}

/**
  This method gets a string which contains a joint type id and
  returns it's type as the JointType enumeration
  @param str The string containing the joint type id string
  @return The joint type id as its enumeration
*/
Klaus::RSG2::JointType Klaus::RSG2::Joint::getTypeFromString(const std::string& str)
{
  for(int i = 0; i < Unknown; ++i)
    if(str == JointTypeNames[i])
      return (JointType)i;

  return Unknown;
}

/**
  This method gets a joint type id and returns the type's name in string to be printed.
  @param t The joint type
  @return A string containing the joint type
*/
std::string Klaus::RSG2::Joint::getStringFromType(const Klaus::RSG2::JointType& t)
{
  return JointTypeNames[t];
}

/**
  This method returns a small string containing the joint's vital information in plain text.
  @return A small string describing the joint.
*/
std::string Klaus::RSG2::Joint::str() const
{
  return "";
}
