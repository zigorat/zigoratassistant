/*
*********************************************************************************
*         GraphicalNodes.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for graphical scene nodes.    *
*            These nodes have a graphical representation and are the only nodes *
*            which can be seen in a simulation.                                 *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicalNodes.h
<pre>
<b>File:</b>          GraphicalNodes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for graphical scene nodes.
               These nodes have a graphical representation and are the only nodes
               which can be seen in a simulation.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef GRAPHICAL_SCENE_NODES
#define GRAPHICAL_SCENE_NODES

#include "Node.h"  // needed for GraphicalNode


namespace Klaus
{

  namespace RSG2
  {

    /*! This class serves as a base for all graphical nodes which have a model to be
        shown. It is capable of loading an entire mesh and setting it's material.
        Automatically converts all model names and materials to simspark converted media.*/
    class _DLLExportControl MeshBasedNode : public GraphicalNode
    {
      private:
        std::string    convertModelName    ( const std::string & strModel );
        std::string    convertMaterialName ( const std::string & strMat   );


      protected:
        std::string    mMeshName;          /*!< Model name (mesh name)    */
        StringArray    mMaterials;         /*!< Material names            */
        Ogre::Entity * mOgreMesh;          /*!< Entity to use for mesh    */
        PositionVar    mMeshScale;         /*!< Scale of the mesh         */

        void           setModelName        ( const std::string & strModel );
        void           setMaterial         ( SToken token                 );

        virtual void   updateGraphics      (                              );
        virtual bool   parseRSGCommand     ( SToken cmd                   );

                       MeshBasedNode       (                              );
    };


    /*! Box nodes show a visual box */
    class _DLLExportControl Box : public MeshBasedNode
    {
      private:
        PositionVar  mExtents;      /*!< Size of the box */

      protected:
        virtual void updateGraphics (            );
        bool         parseRSGCommand( SToken cmd );

      public:
                     Box            (            );

    };


    /*! Sphere nodes show a typical sphere*/
    class _DLLExportControl Sphere : public MeshBasedNode
    {
      private:
        RealVar      mRadius;       /*!< Radius of the sphere */

      protected:
        virtual void updateGraphics (            );
        bool         parseRSGCommand( SToken cmd );

      public:
                     Sphere         (            );
    };


    /*! CCylinder shows a cylinder*/
    class _DLLExportControl Cylinder : public MeshBasedNode
    {
      protected:
        RealVar      mRadius; /*!< Radius of the cylinder */
        RealVar      mLength; /*!< Length of the cylinder */

      protected:
        virtual void updateGraphics (            );
        bool         parseRSGCommand( SToken cmd );

      public:
                     Cylinder       (            );
    };


    /*! CCylinder shows a capped cylinder*/
    class _DLLExportControl CCylinder : public Cylinder
    {
      public:
        CCylinder();
    };


    /*! Capsule shows a capsule */
    class _DLLExportControl Capsule : public Cylinder
    {
      public:
        Capsule();
    };


    /*! SingleMatNode is a mesh based node which has only a
         single material assigned to it.*/
    class _DLLExportControl SingleMatNode : public MeshBasedNode
    {
      private:
        StringVar mMeshDesc; /*!< mesh description */

      protected:
        bool      parseRSGCommand( SToken cmd );

      public:
                  SingleMatNode  (            );
    };


    /*! StaticMesh is a mesh based node which can have multiple
        materials and can change them at will.*/
    class _DLLExportControl StaticMesh :
                                public SingleMatNode,
                                public RDSNode
    {
      private:
        BooleanVar   mVisible;  /*!< Is the mesh visible */
        PositionVar  mExtents;  /*!< Size of the mesh */
        void         parseLoadCommand( SToken token );

      protected:
        virtual void updateGraphics  (              );
        bool         parseRSGCommand ( SToken cmd   );

      public:
                     StaticMesh      (              );

        virtual bool parseRDSCommand ( SToken cmd   );
    };


    /*! Light node is a typical spot light in opengl framework */
    class _DLLExportControl Light :
                              public GraphicalNode,
                              public RDSNode
    {
      private:
        Ogre::Light * mLight;        /*!< Ogre light instance */
        double        mDiffuse[4];   /*!< Diffuse color of the light  */
        double        mAmbient[4];   /*!< Ambient color of the light  */
        double        mSpecular[4];  /*!< Specular color of the light */

      protected:
        bool          parseRSGCommand( SToken cmd );
        virtual void  updateGraphics (            );

      public:
                      Light          (            );

        virtual bool  parseRDSCommand( SToken cmd );
    };


    /*! Sky is graphical node which shows eighter a sky box or a sky dome */
    class _DLLExportControl Sky : public Node
    {
      private:
        /*! SkyType is an enumeration of all types of supported skies */
        enum SkyType
        {
          SkyBox,                   /*!< A box sky                */
          SkyDome,                  /*!< A dome sky               */
          Illegal                   /*!< Not specified            */
        };

      protected:
        std::string  mMaterial;     /*!< Material of the sky      */
        double       mDistance;     /*!< Distance of the sky      */
        double       mCurvature;    /*!< Curvature of sky dome    */
        double       mTiling;       /*!< Tiling of the sky dome   */
        SkyType      mSkyType;      /*!< Type of the sky          */

        virtual void updateGraphics (            );
        bool         parseRSGCommand( SToken cmd );

      public:
                     Sky            (            );
    };


    /*! This class loads a typical ogre mesh. */
    class _DLLExportControl OgreMesh : public StaticMesh
    {
      private:
        BooleanVar mCastShadow;  /*!< Whether the node casts shadows */

      protected:
        virtual void updateGraphics (            );
        bool         parseRSGCommand( SToken cmd );

      public:
                     OgreMesh       (            );
    };

  }; // end namespace RSG2

}; // end namespace Klaus


#endif // GRAPHICAL_SCENE_NODES
