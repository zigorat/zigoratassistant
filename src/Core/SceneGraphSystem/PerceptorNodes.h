/*
*********************************************************************************
*         PerceptorNodes.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for all perceptor nodes.      *
*            Perceptors get data from simulator and send them to agents.        *
*                                                                               *
*********************************************************************************
*/

/*! \file PerceptorNodes.h
<pre>
<b>File:</b>          PerceptorNodes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for all perceptor nodes.
               Perceptors get data from simulator and send them to agents.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef PERCEPTOR_NODES
#define PERCEPTOR_NODES

#include "Node.h"  // needed for PerceptorBase

namespace Klaus
{

  namespace RSG2
  {

    /*! PerceptorHandler node. I don't know what it does in simulator. */
    class _DLLExportControl PerceptorHandler: public PerceptorBase
    {
    };


    /*! Wrapper for time perceptor node. This type of node sends
        time information to all agents in the game. */
    class _DLLExportControl TimePerceptor : public PerceptorBase
    {
    };


    /*! Wrapper for AgentState nodes. Gives information about agent uniform number and team side to the agent. */
    class _DLLExportControl AgentState : public PerceptorBase
    {
    };


    /*! ObjectState sends positional information of a node as visual perception in
        see commands. Can be used to determine teammate and opponent gestures.
    */
    class _DLLExportControl ObjectState : public PerceptorBase
    {
      private:
        StringVar    mID;           /*!< Name of the object */
        PositionVar  mSeenPosition; /*!< Seen position of the object */

      protected:
        virtual bool parseRSGCommand( SToken cmd              );

      public:
                     ObjectState    (                         );

        std::string  getID          (                         ) const;

        VecPosition  getSeenPosition(                         ) const;
        void         setSeenPosition( const VecPosition & pos );

        /*! Map is a mapping between object state names and their instances */
        typedef std::map<std::string, ObjectState*> Map;
    };


    /*! I don't know what this perceptor does! */
    class _DLLExportControl GameStatePerceptor : public PerceptorBase
    {
    };


    /*! This perceptor sends gyro rates to agents */
    class _DLLExportControl GyroRatePerceptor : public PerceptorBase
    {
      private:
        VecPosition mRate;     /*!< The current speed of the gyro */
        VecPosition mRotation; /*!< The accumulated rate of the gyro */

      public:
        void        reset      (                        );
        VecPosition getRate    (                        ) const;
        Quaternion  getRotation(                        ) const;
        void        percept    ( const VecPosition & rt );

        /*! Array is a dynamic array of gyro rate perceptors */
        typedef std::vector<GyroRatePerceptor*> Array;
    };


    /*! This perceptor sends accelerometer information to the agent */
    class _DLLExportControl Accelerometer : public PerceptorBase
    {
      private:
        VecPosition mAcceleration; /*!< Percepted acceleration */

      public:
        VecPosition getAcceleration(                       ) const;
        void        setAcceleration( const VecPosition & a );

        /*! Array is a dynamic array of accelerometers */
        typedef std::vector<Accelerometer*> Array;
    };


    /*! This perceptor sends a boolean value indicating a touch is
        stablished between the node which the perceptor is attached
        and ground.
    */
    class _DLLExportControl TouchPerceptor : public PerceptorBase
    {
    };


    /*! This perceptor sends aural information said by other players
        to the agent.
    */
    class _DLLExportControl HearPerceptor : public PerceptorBase
    {
    };


    /*! This perceptor calculates force and the center which the force
        applies on the attached node.
    */
    class _DLLExportControl ForceResistancePerceptor: public PerceptorBase
    {
      private:
        VecPosition mCenter; /*!< Percepted center of the force */
        VecPosition mForce;  /*!< Percepted force vector */

      public:
        VecPosition getCenter(                       ) const;
        void        setCenter( const VecPosition & c );

        VecPosition getForce (                       ) const;
        void        setForce ( const VecPosition & f );

        /*! Array is a dynamic array of FRPs*/
        typedef std::vector<ForceResistancePerceptor*> Array;
    };

    /*! Smaller name for the FRP! */
    typedef ForceResistancePerceptor FRP;


    /*! Base perceptor handler for vision perceptors. Derived perceptor
        sends visual information from the field to agents.*/
    class _DLLExportControl VisionPerceptorBase : public PerceptorBase
    {
      protected:
        BooleanVar   mStaticSenseAxis;    /*!< Whether the vision axis is static */
        BooleanVar   mSenseMyPos;         /*!< Whether agent's position will be given with see */
        BooleanVar   mAddNoise;           /*!< Whether to add noise */

        virtual bool parseRSGCommand      ( SToken cmd );

      public:
                     VisionPerceptorBase  (            );
    };


    /*! This perceptor sends unrestriced visual information to the agent. */
    class _DLLExportControl UnrestrictedVisionPerceptor : public VisionPerceptorBase
    {
    };


    /*! This perceptor sends restriced visual information to the agent.
        Information is restricted to a cone centered on the perceptor
        and sends information in the interval set for the perceptor.
    */
    class _DLLExportControl RestrictedVisionPerceptor : public UnrestrictedVisionPerceptor
    {
      private:
        RealVar mHorAngle;       /*!< Horizontal angle range of the vision perceptor */
        RealVar mVerAngle;       /*!< Vertical angle range of the vision perceptor */
        RealVar mInterval;       /*!< Working interval of the vision perceptor */

      protected:
        bool parseRSGCommand     ( SToken cmd );

      public:
        RestrictedVisionPerceptor(            );
    };


    /*! Base wrapper for joint perceptors. Holds information of the joint. */
    class _DLLExportControl JointPerceptor : public PerceptorBase
    {
      protected:
        AngleVar mAngle1;      /*!< First angle of the joint */
        AngleVar mAngle2;      /*!< Second angle of the joint */
        AngleVar mRate1;       /*!< First motor's angular velocity */
        AngleVar mRate2;       /*!< Second motor's angular velocity */

      public:
                 JointPerceptor(                                   );

        Degree   getAngle      (int iNumber = 0                    );
        Degree   getRate       (int iNumber = 0                    );
        void     overrideAngle (Degree dAngle1, Degree dAngle2 = 0 );
        void     overrideRate  (Degree dRate1, Degree dRate2 = 0   );

        /*! Map is a dynamic mapping between joint perceptor names and their instances */
        typedef std::map<std::string, JointPerceptor*> Map;

        /*! Array is a dynamic array of JointPerceptors */
        typedef std::vector<JointPerceptor*> Array;
    };


    /*! Perceptor for hinge joint. Sends angle (and possible rate) of the joint */
    class _DLLExportControl HingeJointPerceptor : public JointPerceptor
    {
    };

    /*! Perceptor for universal joint. Sends angle (and possible rate) of the joint */
    class _DLLExportControl UniversalJointPerceptor : public JointPerceptor
    {
    };

  }; // end namespace RSG2

}; // end namespace Klaus


#endif // PERCEPTOR_NODES
