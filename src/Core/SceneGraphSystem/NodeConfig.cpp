/*
*********************************************************************************
*           NodeConfig.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 01/08/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class for configuration of node system.                       *
*                                                                               *
*********************************************************************************
*/

/*! \file NodeConfig.cpp
<pre>
<b>File:</b>          NodeConfig.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/08/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class for configuration of node system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/08/2010       Mahdi           Initial version created
</pre>
*/

#include "NodeConfig.h"
#include <libZigorat/Logger.h>  // needed for Logger
#include <libZigorat/SLang.h>   // needed for SToken
#include <sstream>              // needed for stringstream


/*! NodeConfig singleton instance */
template<> Klaus::RSG2::NodeConfig* Klaus::Singleton<Klaus::RSG2::NodeConfig>::mInstance = 0;

/**
  Constructor of NodeConfig. sets up configuration
*/
Klaus::RSG2::NodeConfig::NodeConfig()
{
  #ifdef IMPLEMENT_GRAPHICS
  setupConfig("NodeConfig");
  #else
  _loadDefaultConfig();
  #endif
}

/**
  Destructor of NodeConfig. Does nothing.
*/
Klaus::RSG2::NodeConfig::~NodeConfig()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::RSG2::NodeConfig * Klaus::RSG2::NodeConfig::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new NodeConfig;
    Logger::getSingleton()->log(1, "(NodeConfig) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::RSG2::NodeConfig::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(NodeConfig) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(NodeConfig) removed singleton");
  }
}

/**
  This method sets up default configuration for node system
*/
void Klaus::RSG2::NodeConfig::_loadDefaultConfig()
{
  mOptions = ERRORS_BREAK_PARSE |
             LOG_TO_CERR |
             LOG_TO_LOGGER |
             USE_SIMSPARK_MODELS;
}

#ifdef IMPLEMENT_GRAPHICS
/**
  This method reads node system's configuration from the binary stream.
  @return True on successfull read, false otherwise
*/
bool Klaus::RSG2::NodeConfig::_readConfig( const Config::ConfigType & type )
{
  try
  {
    if(type == Config::BinaryConfig)
      mOptions = readUnsigned();
    else if(type == Config::SExpConfig)
    {
      mOptions = 0;
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      // get the expression
      SToken token = expr["setOptions"];
      if(!token)
      {
        Logger::getSingleton()->log(23, "(NodeConfig) read: no configuration provided", type);
        return true;
      }

      for(size_t i = 1; i < token.size(); ++i)
        if(token[i] == "LoggerEnabled")
          mOptions |= LOG_TO_LOGGER;
        else if(token[i] == "StdOutEnabled")
          mOptions |= LOG_TO_COUT;
        else if(token[i] == "StdErrEnabled")
          mOptions |= LOG_TO_CERR;
        else if(token[i] == "ErrorsAreFatal")
          mOptions |= ERRORS_BREAK_PARSE;
        else if(token[i] == "ExceptionsEnabled")
          mOptions |= LOG_THROW_EXCEPTIONS;
        else if(token[i] == "UseSimSparkModels")
          mOptions |= USE_SIMSPARK_MODELS;
    }
    else
    {
      Logger::getSingleton()->log(23, "(NodeConfig) read: configuration type %d not supported", type);
      return false;
    }
  }
  catch(const char * str)
  {
    Logger::getSingleton()->log(23, "(NodeConfig) exception: %s", str);
    return false;
  }

  return true;
}

/**
  This method write node system's configuration to the binary stream
*/
void Klaus::RSG2::NodeConfig::_writeConfig( const Config::ConfigType & type )
{
  if(type == Config::BinaryConfig)
    writeUnsigned(mOptions);
  else if(type == Config::SExpConfig)
  {
    std::stringstream ss;
    if(loggerEnabled())
      ss << " LoggerEnabled";
    if(stdoutEnabled())
      ss << " StdOutEnabled";
    if(stderrEnabled())
      ss << " StdErrEnabled";
    if(errorsBreakParse())
      ss << " ErrorsAreFatal";
    if(exceptionsEnabled())
      ss << " ExceptionsEnabled";
    if(useSimSparkModels())
      ss << " UseSimSparkModels";

    writeText("(setOptions%s)", ss.str().c_str());
  }
  else
    Logger::getSingleton()->log(23, "(NodeConfig) write: configuration type %d not supported", type);
}
#endif

/**
  This method returns whether logs will go to logger or not
  @return If logs shall be channeled into logger true will be returned, false otherwise
*/
bool Klaus::RSG2::NodeConfig::loggerEnabled() const
{
  return (mOptions & LOG_TO_LOGGER) == LOG_TO_LOGGER;
}

/**
  This method returns whether logs will go to standard output or not
  @return If logs shall be channeled into stdout true will be returned, false otherwise
*/
bool Klaus::RSG2::NodeConfig::stdoutEnabled() const
{
  return (mOptions & LOG_TO_COUT) == LOG_TO_COUT;
}

/**
  This method returns whether logs will go to standard error outputto or not
  @return If logs shall be channeled into stderr true will be returned, false otherwise
*/
bool Klaus::RSG2::NodeConfig::stderrEnabled() const
{
  return (mOptions & LOG_TO_CERR) == LOG_TO_CERR;
}

/**
  This method returns whether on parse errors operation shall break or not
  @return If any errors occured and this is true, operation shall break, otherwise will continue it's job
*/
bool Klaus::RSG2::NodeConfig::errorsBreakParse() const
{
  return (mOptions & ERRORS_BREAK_PARSE) == ERRORS_BREAK_PARSE;
}

/**
  This method returns whether on parse errors exceptions will be thrown or not
  @return If this returns true, on parse errors exceptions will trigger. If false then excpetions are disabled
*/
bool Klaus::RSG2::NodeConfig::exceptionsEnabled() const
{
  return (mOptions & LOG_THROW_EXCEPTIONS) == LOG_THROW_EXCEPTIONS;
}

/**
  This method returns whether simspark models shall be used or not
  @return True if render shall use simspark models or not
*/
bool Klaus::RSG2::NodeConfig::useSimSparkModels() const
{
  return (mOptions & USE_SIMSPARK_MODELS) == USE_SIMSPARK_MODELS;
}
