/*
*********************************************************************************
*            Graph.cpp : Robocup 3D Soccer Simulation Team Zigorat               *
*                                                                               *
*  Date: 13/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for Graph. Graph handles      *
*            simulator's scene graph management. This includes loading, using   *
*            and saving the scene graph. It will create a scene nodes hierarchy *
*            and create a usable API to access them.                            *
*                                                                               *
*********************************************************************************
*/

/*! \file Graph.cpp
<pre>
<b>File:</b>          Graph.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       13/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for Graph. Graph handles simulator's
               scene graph management. This includes loading, using and saving the scene graph.
               It will create a scene nodes hierarchy and create a usable API to access them.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/02/2011       Mahdi           Initial version created
</pre>
*/


#include "Graph.h"
#include <libZigorat/ApplicationSettings.h>  // ApplicationSettings
#include "NodeFactory.h"                     // needed for NodeFactoryCore


/**
  Constructor of Graph. Sets logger instance and the root node to null
*/
Klaus::RSG2::Graph::Graph()
{
  mRoot = NULL;
  mLog = Logger::getSingleton();
}

/**
  Destructor of Graph. Clears the scene
*/
Klaus::RSG2::Graph::~Graph()
{
  clearScene();
}

/**
  This method clears the scene
*/
void Klaus::RSG2::Graph::clearScene()
{
  if(mRoot)
    delete mRoot;

  mRoot = NULL;
}

/**
  This method resets the cache. Queries all nodes in the graph, sets up graphical
  root nodes and finds out the base body node. Also finds out all perceptor nodes.
*/
void Klaus::RSG2::Graph::resetCache()
{
  // clear the cache
  mCacheNodes.clear();
  mCacheGraphicNodes.clear();
  mCachePerceptors.clear();
  mCacheMappedNodes.clear();
  mCacheJoints.clear();

  // get all nodes
  mRoot->queryNodes(mCacheNodes);

  // get all other caches
  for(size_t i = 0; i < mCacheNodes.size(); ++i)
  {
    // multimap for all nodes
    mCacheMappedNodes.insert(Node::MultiMap::value_type(mCacheNodes[i]->getType(), mCacheNodes[i]));

    // graphical nodes
    GraphicalNode * gnode = dynamic_cast<GraphicalNode*>(mCacheNodes[i]);
    if(gnode && gnode->isRoot())
      mCacheGraphicNodes.push_back(gnode);

    // perceptor cache
    if(dynamic_cast<PerceptorBase*>(mCacheNodes[i]))
      mCachePerceptors.push_back(dynamic_cast<PerceptorBase*>(mCacheNodes[i]));

    // effector cache
    if(dynamic_cast<EffectorBase*>(mCacheNodes[i]))
      mCacheEffectors.push_back(dynamic_cast<EffectorBase*>(mCacheNodes[i]));

    // body root
    Body * b = dynamic_cast<Body*>(mCacheNodes[i]);
    if(b && b->isRootBodyNode())
      mRootBody = b;

    Joint * j = dynamic_cast<Joint*>(mCacheNodes[i]);
    if(j && j->getPerceptor())
      mCacheJoints[j->getPerceptor()->getName()] = j;
  }

  if(mCacheGraphicNodes.empty())
    mLog->log(28, "no graphical node found");
}

/**
  This method handles errors withing all derived classes. All errors will be
  sent here with their parameters and they will be logged, thrown or printed
  depending on the configuration of the system.
  @param strFormat Charactor string with possible format specifiers
*/
void Klaus::RSG2::Graph::error(const char* strFormat, ... )
{
  va_list ap;
  char buf[1024] = "(Graph) ";
  va_start(ap, strFormat);
  vsnprintf(buf + 8, 1015, strFormat, ap);
  va_end(ap);

  NodeConfig * conf = NodeConfig::getSingleton();

  if(conf->loggerEnabled())
    mLog->log(28, buf);
  if(conf->stdoutEnabled())
    std::cout << buf << std::endl;
  if(conf->stderrEnabled())
    std::cerr << buf << std::endl;
  if(conf->exceptionsEnabled())
    throw buf;
}

/**
  This method parses header information of the graph file.
  The header includes version number of graph.
  @param token The token containing the graph header
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Graph::parseHeader(SToken token)
{
  if((token.size() != 3) || (token[0] != "RSG"))
    error("invalid header : %s", token.str().c_str());
  else
  {
    mVersionMajor = token[1].getAsInt();
    mVersionMinor = token[2].getAsInt();

    if(mVersionMajor != 0 || mVersionMinor != 1)
      error("invalid version : %s", token.str().c_str());
    else
      return true;
  }

  return false;
}

/**
  This method parses the body of the scene graph file.
  @param token The token containing the graph body
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Graph::parseBody(SToken token)
{
  // create root node instance
  mRoot = NodeFactoryCore::getSingleton()->createInstanceFor("RootSceneNode");
  if(!mRoot)
  {
    error("could not acquire root node parser");
    return false;
  }

  // parse the scene
  if(!mRoot->parse(token))
  {
    error("errors in parsing '%s'", mSceneAddress.c_str());
    if(NodeConfig::getSingleton()->errorsBreakParse())
      return false;
  }

  return true;
}

/**
  This method is called after a scene is loaded. Will
  reset the cache and validate the graphical nodes.
*/
void Klaus::RSG2::Graph::postLoad()
{
  mLog->log(28, "(Graph) successfully loaded '%s'", mSceneAddress.c_str());
  mLog->log(28, "(Graph) loaded version %d.%d", mVersionMajor, mVersionMinor);

  mRoot->postLoad();
  resetCache();

  updateForwardKinematics();
}

/**
  This method loads the graph from a given file.
  @param strFileName Name of the file to load from
  @return True on successful load, false otherwise
*/
bool Klaus::RSG2::Graph::loadFromFile(const std::string& strFileName)
{
  // load the file
  try
  {
    ApplicationSettings * settings = ApplicationSettings::getSingleton();

    // 64 KiloBytes seems fairly enough
    // load the file
    mSceneAddress = strFileName;
    mSceneTokens.parseFile(settings->getRSGPath() + strFileName, 64*1024);

    // find header and body
    int iHeaderIndex = -1;
    int iBodyIndex = -1;
    for(size_t i = 0; i < mSceneTokens.size(); ++i)
      if(!mSceneTokens[i].isComment())
      {
        if(iHeaderIndex == -1)
          iHeaderIndex = i;
        else
        {
          iBodyIndex = i;
          break;
        }
      }

    // sanity check
    if(iHeaderIndex == -1 || iBodyIndex == -1)
    {
      error("header or body not found in '%s'", strFileName.c_str());
      mSceneTokens.clear();
      return false;
    }

    // parse header
    if(!parseHeader(mSceneTokens[iHeaderIndex]))
    {
      mSceneTokens.clear();
      return false;
    }

    // parse scene body
    if(!parseBody(mSceneTokens[iBodyIndex]))
    {
      mSceneTokens.clear();
      return false;
    }

    // finished
    mSceneAddress = strFileName;

    // initialize after load
    postLoad();
  }
  catch(SLangException e)
  {
    error("error in parsing file '%s':\n%s", strFileName.c_str(), e.str());
    return false;
  }

  return true;
}

/**
  This method saves the contents of graph into a given file.
  @param strFileName Name of the file to save
*/
void Klaus::RSG2::Graph::saveToFile(const std::string& strFileName)
{
  error("save not implemented yet. can not save to %s", strFileName.c_str());
}

/**
  This method returns the graph file's address.
  @return The graph file's address
*/
std::string Klaus::RSG2::Graph::getScene()
{
  return mSceneAddress;
}

/**
  This method returns the root node of graph.
  @return Root node of the graph
*/
Klaus::RSG2::Node* Klaus::RSG2::Graph::getRootNode()
{
  return mRoot;
}

void Klaus::RSG2::Graph::getAllNodes(Node::Array& nodes)
{
  nodes.assign(mCacheNodes.begin(), mCacheNodes.end());
}

/**
  This method gets all perceptors in the graph
  @param lst Perceptors will be copied here
*/
void Klaus::RSG2::Graph::getAllPerceptors(Klaus::RSG2::PerceptorBase::Array & lst)
{
  lst.assign(mCachePerceptors.begin(), mCachePerceptors.end());
}

/**
  This method gets all effectors in the graph
  @param lst Effectors will be copied here
*/
void Klaus::RSG2::Graph::getAllEffectors(Klaus::RSG2::EffectorBase::Array & lst)
{
  lst.assign(mCacheEffectors.begin(), mCacheEffectors.end());
}

Klaus::RSG2::Node* Klaus::RSG2::Graph::getNodeByType(const std::string & strType, const std::string& strName)
{
  Node::Iterator it = mCacheMappedNodes.equal_range(strType);
  if(it.first == it.second)
    return NULL;

  for(; it.first != it.second; it.first++)
    if(strName.empty() || it.first->second->getName() == strName)
      return it.first->second;

  return NULL;
}


/**
  This method sets the ogre parent for all root graphical nodes of the scene
  @param node The ogre node to be the parent for graphical nodes
*/
void Klaus::RSG2::Graph::setOgreParent(Ogre::SceneNode* node)
{
  #ifdef IMPLEMENT_GRAPHICS
  for(size_t i = 0; i < mCacheGraphicNodes.size(); ++i)
    mCacheGraphicNodes[i]->setOgreParent(node);
  #else
    mLog->log(28, "(Graph) not compiled to attach to 3d api : %d", node);
  #endif
}

/**
  This method makes all graphical nodes in scene to validate themselves.
*/
void Klaus::RSG2::Graph::updateGraphics()
{
  for(size_t i = 0; i < mCacheGraphicNodes.size(); ++i)
    mCacheGraphicNodes[i]->validateGraphics();
}

/**
  This method sets the global position and orientation
  of the graph and applies forward kinematics.
  @param pos Position of the graph
  @param q Orientation of the graph
*/
void Klaus::RSG2::Graph::setGlobalTransform(const Klaus::VecPosition& pos, const Klaus::Quaternion& q)
{
  // set the current transform
  mGlobalTransform.setTranslation(pos);
  mGlobalTransform.setOrientation(q);

  // apply the kinematics to update positions and orientations
  updateForwardKinematics();
}

/**
  This method applies forward kinematics to the graph.
*/
void Klaus::RSG2::Graph::updateForwardKinematics()
{
  // find out forward kinematics information
  getForwardKinematics(mCurrentState, 0);

  // and apply them to the graph
  applyForwardKinematicSheet(mCurrentState);
}

/**
  This method applies forward kinematics to the graph.
  @param sheet The sheet to save the kinematic information to
  @param time_gap The assumed value for consumed time
*/
void Klaus::RSG2::Graph::getForwardKinematics(Kinematics & sheet, const double & time_gap)
{
  if(!mRootBody)
  {
    error("(SceneGraph) physics tree is not initialized");
    return;
  }

  // set the global position of the top most node and update the tree
  mRootBody->getForwardKinematics(mGlobalTransform, time_gap, sheet);
}

/**
  This method returns finds out the forward kinematics for the given state
  @param angles The angles calculated by inverse kinematics
  @param sheet The sheet that contains the angles for all joints
*/
void Klaus::RSG2::Graph::getForwardKinematicsFor(const InverseKinematics & angles, Kinematics& sheet)
{
  mRootBody->getForwardKinematicsFor(mGlobalTransform, angles, sheet);
}

/**
  This method calculates the center of mass of the robot.
  @return Center of mass in the graph
*/
Klaus::VecPosition Klaus::RSG2::Graph::getCenterOfMass()
{
  return Physics::getCenterOfMass(mCurrentState);
}

/**
  This method applies the given kinematic sheet to the graph
  @param sheet The sheet to copy transformations from
*/
void Klaus::RSG2::Graph::applyForwardKinematicSheet(Kinematics& sheet)
{
  // inform upcoming update
  mRoot->prePhysicsUpdate();

  // set the global transformations of the body nodes
  mRootBody->applyForwardKinematics(sheet);

  // inform of update completion and apply forward kinematics
  mRoot->postPhysicsUpdate();

  // update graphical view of the scene graph tree
  updateGraphics();
}

void Klaus::RSG2::Graph::getInverseKinematics(InverseKinematics& sheet)
{
  for(Joint::Map::iterator it = mCacheJoints.begin(); it != mCacheJoints.end(); ++it)
  {
    std::string strName = it->first;
    sheet[strName].mAngles[0] = it->second->getAngle();
    sheet[strName].mAngles[1] = it->second->getAngle(1);
    sheet[strName].mSpeeds[0] = it->second->getSpeed();
    sheet[strName].mSpeeds[1] = it->second->getSpeed(1);
    sheet[strName].mPivot = it->second->getPivot();
  }
}

/**
  This method applies joint transformations into graph.
  @param sheet The sheet to apply onto the sheet
*/
void Klaus::RSG2::Graph::applyInverseKinematicSheet(InverseKinematics& sheet)
{
  // update joint angles
  for(InverseKinematics::const_iterator it = sheet.begin(); it != sheet.end(); ++it)
  {
    mCacheJoints[it->first]->setAngle(it->second.mAngles[0], it->second.mAngles[1]);
    mCacheJoints[it->first]->setSpeed(it->second.mSpeeds[0], it->second.mSpeeds[1]);
  }

  // recalculate forward kinematics
  updateForwardKinematics();
}
