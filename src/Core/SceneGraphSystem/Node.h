/*
*********************************************************************************
*               Node.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 13/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for SceneNodes. SceneNodes    *
*            are the parsers of every bit of simulators scene graph.            *
*            Simulator's scene graph are where every detail of simulator's      *
*            scene and robots are stored. Their location is in the rsg folder   *
*            of simulator's installation path.                                  *
*                                                                               *
*********************************************************************************
*/

/*! \file Node.h
<pre>
<b>File:</b>          Node.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       13/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for SceneNodes. SceneNodes
               are the parsers of every bit of simulators scene graph.
               Simulator's scene graph are where every detail of simulator's
               scene and robots are stored. Their location is in the rsg folder
               of simulator's installation path.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef SCENE_NODES_V2
#define SCENE_NODES_V2

#include <libZigorat/PlatformMacros.h>

#include "NodeConfig.h"          // needed for NodeConfig
#include <libZigorat/VarTypes.h> // needed for variables, Var::Array, SExpression
#include <map>                   // needed for std::map , std::multimap

/// Forward decleration so that ogre not be included here
/// For compile time reasons
namespace Ogre
{
  class SceneNode;
  class Entity;
  class SceneManager;
  class Light;
}; // end namespace Ogre


namespace Klaus
{

  namespace RSG2
  {

    /*! This class is the base class for all types of nodes in scene graph framework.
        Defines a base for scene node parsers and information holders. The decendant
        classes of this type will be able to parse simulator's scene nodes and store
        their inforamtion. This allows them to be used in places where simulator uses
        data of robot models or soccer field scenes. A normal usage of these classes
        is in robot scenes. They can parse and split robot's data and models and use
        it to get every detail about the robot. They can be used to create robots in
        other applications and save them in a format which simulator understands.
    */
    class _DLLExportControl Node
    {
      public:
        /*! Array is a dynamic array of Node instances */
        typedef std::vector<Node*> Array;

        /*! MultiMap is a multi mapping between node types and their instances */
        typedef std::multimap<std::string, Node*> MultiMap;

        /*! Iterator is a multi map iterator for using MultiMap types */
        typedef std::pair<MultiMap::iterator, MultiMap::iterator> Iterator;

        /*! ConstIterator is a multi map iterator for using MultiMap types */
        typedef std::pair<MultiMap::const_iterator, MultiMap::const_iterator> ConstIterator;

      private:
        StringArray         mComments;          /*!< Parsed tokens of the      */
        Var::Array          mDefinitions;       /*!< Definitions of scene      */

        void                clear               (                              );
        bool                addChild            ( SToken node                  );

        bool                parseSwitch         ( SToken cmd                   );
        bool                parseDefine         ( SToken cmd                   );
        bool                parseImportScene    ( SToken cmd                   );
        double              parseEvaluate       ( SToken token                 ) const;
        double              evaluate            ( SToken token, size_t start   ) const;
        std::string         join                ( SToken token                 ) const;

      protected:
        NodeConfig        * mConfig;            /*!< Configuration of the scene graph system */
        Node              * mParent;            /*!< Parent of the node */
        Array               mChildren;          /*!< Children of the node */
        MultiMap            mChildrenMap;       /*!< Children mapped to their type */
        Var::Array          mProperties;        /*!< All the properties of the node */
        std::string         mType;              /*!< Type of the node */

        StringVar           mName;              /*!< Name of the node */

        virtual bool        parseRSGCommand     ( SToken cmd                   );

        // properties of the nodes will be setup here
        virtual void        addProperty         ( Var * prop                   );

        // iternal methods
        virtual void        prePhysicsInternal  (                              );
        virtual void        physicsInternal     (                              );
        virtual void        postPhysicsInternal (                              );
        virtual void        postLoadInternal    (                              );
        virtual void        syncToProperties    (                              );

        // error output is in this method
        void                error               ( const char * strFormat, ...  ) const;

        // get a relative node
        Node              * getRelativeNode     ( const std::string & address  );

      public:
                            Node                (                              );
        virtual           ~ Node                (                              );

        // vital information
        std::string         getName             (                              ) const;
        std::string         getType             (                              ) const;

        // children related
        size_t              getChildCount       (                              ) const;
        Node              * getChildByIndex     ( const size_t & index         );
        Node              * getChildByName      ( const std::string & strName  );
        Node              * getChildByType      ( const std::string & strType,
                                                  const std::string &strName="");

        // definition usage
        CVar                getDefinition       ( const std::string & strName  ) const;
        void                setDefinition       ( Var * v                      );
        double              readNumber          ( SToken token                 ) const;
        std::string         readString          ( SToken token                 ) const;

        Node              * getParent           (                              ) const;
        virtual void        setParent           ( Node * parent                );

        bool                parse               ( SToken token                 );
        bool                parseToken          ( SToken token                 );

        // comment related
        const StringArray & getComments         (                              ) const;
        void                setComments         ( const StringArray & comments );
        void                addComment          ( const std::string & comment  );
        void                removeComment       ( const size_t & index         );

        // property related
        const Var::Array  & getAllProperties    (                              ) const;
        bool                setProperty         ( const std::string & strName,
                                                  const std::string & strValue );

        // physics related methods
        virtual Matrix4     getGlobalTransform  (                              ) const;
        virtual Matrix4     getLocalTransform   (                              ) const;
        Matrix4             getRelativeTransform( Node * node                  );
        void                prePhysicsUpdate    (                              );
        void                physicsUpdate       (                              );
        void                postPhysicsUpdate   (                              );

        // query
        void                queryNodes          ( Node::Array & nodes          );

        // update related methods
        void                postLoad            (                              );
    }; // end class Node


    /*! This is the base class for all those who support not only RSG protocol,
        but also the RDS protocol too. They shall implement the RDS command
        protocol via the provided command template. <br>
        The RDS protocol manages delta commands. Commands which are sent as
        difference with their original RSG commands.
    */
    class _DLLExportControl RDSNode
    {
      public:
        /**
          This method shall be implemented to parse RDS protocol commands.
          @param cmd The RDS command
          @return True if parse was successfull, false otherwise
        */
        virtual bool parseRDSCommand( SToken cmd ) = 0;
    };


    /*! This class is the base class for all graphical nodes. Provides a visual
        scene node instance and handles parent substitution.<br>
        A few notes on this class:<br>
          - Contructor shall not need neither node parent, nor ogre parent
          - Destructor is the only place where each node's meshes and nodes are deleted.
          - attachToParent does it's job. No possible error scenarios
          - validate and invalidate are bug free. Each class shall write it's own
            validation method. Shall not remove or create the SceneNode.
          - addGraphicChild and removeGraphicChild are also bug free.
          - removeSceneNode is the only responsible method to free allocations for meshes.
          - removeGraphicChild does not call removeSceneNode, only detaches
            (and if called by removeSceneNode, destroys ogre node)
          - addGraphicChild is also bug free.
    */
    class _DLLExportControl GraphicalNode : public Node
    {
      public:
        /*! GList is a dynamic array of GraphicalNode instances */
        typedef std::vector<GraphicalNode*> Array;

      private:
        bool                 mInvalid;         /*!< Is the node invalidated? */

        void                 removeSceneNode   (                          );

      protected:
        Array                mGChildren;       /*!< Graphical children of node */
        GraphicalNode      * mGParent;         /*!< Parent of the node */
        Ogre::SceneNode    * mOgreNode;        /*!< The ogre node instance */
        Ogre::SceneManager * mSceneMgr;        /*!< Ogre scene manager to create ogre nodes and entities */

        virtual void         updateGraphics    (                          ) = 0;
        virtual void         postLoadInternal  (                          );

      public:
                             GraphicalNode     (                          );
        virtual            ~ GraphicalNode     (                          );

        bool                 isRoot            (                          ) const;

        void                 attachToParent    (                          );
        void                 addGraphicChild   ( GraphicalNode* child     );
        void                 removeGraphicChild( GraphicalNode* child,
                                                 bool bDestroy = false    );

        virtual void         setParent         ( Node * parent            );

        #ifdef IMPLEMENT_GRAPHICS
        void                 setOgreParent     ( Ogre::SceneNode * parent );
        #endif

        void                 invalidateGraphics(                          );
        void                 validateGraphics  (                          );
    };


    /*! Base class for perceptor classes. Perceptors are sensors on the robot
        and they send their information to the robot. The robot can perceive
        and distinuish their information by knowing their name.
    */
    class _DLLExportControl PerceptorBase : public Node
    {
      public:
        /*! Map is a standard mapping between perceptor names and their instances */
        typedef std::map<std::string, PerceptorBase*> Map;

        /*!< Array is a dynamic array of perceptors */
        typedef std::vector<PerceptorBase*> Array;
    };


    /*! This class serves as the base for all effector classes. Effectors are
        actuators, they enable the robot to interact with its environment.
        These may include physical, aural and informatical interactions.
    */
    class _DLLExportControl EffectorBase : public Node
    {
      public:
        /*! Map is a standard mapping between perceptor names and their instances */
        typedef std::map<std::string, EffectorBase*> Map;
    };


    /*! This class is the base for all transformable nodes. These nodes
        store transformation information. Their information is used in
        determining position and orientation for all other nodes. For
        example they determine position of graphical nodes and body
        nodes which in turn can be used to calculate CoM and ZMP.
    */
    class _DLLExportControl TransformableNode :
                              public GraphicalNode,
                              public RDSNode
    {
      public:
        /*! Array is a dynamic array of TransformableNodes */
        typedef std::vector<TransformableNode*> Array;

      private:
        void                parseSLT            ( SToken cmd         );
        void                findGlobalTransform (                    );

      protected:
        // transformations
        PositionVar         mDefaultTranslation;/*!< Default translation of the node relative to it's parent */
        PositionVar         mDefaultScale;      /*!< Default scale of the node */
        QuaternionVar       mDefaultRotation;   /*!< Default rotation of the node relative to it's parent's rotation */

        Matrix4             mGlobalTransform;   /*!< Relative trasform of the node to the top most transform node. */
        Matrix4             mRelativeTransform; /*!< Relative trasform to the upper transform node */
        bool                mTransformUpdated;  /*!< Global transform update flag */

        Array               mChildTransforms;   /*!< Children transforms */
        TransformableNode * mParentTransform;   /*!< Parent transform node */


        virtual void        prePhysicsInternal  (                    );
        virtual void        postPhysicsInternal (                    );
        virtual void        postLoadInternal    (                    );
        virtual void        updateGraphics      (                    );
        virtual bool        parseRSGCommand     ( SToken cmd         );


      public:
                            TransformableNode   (                    );
                          ~ TransformableNode   (                    );

        Matrix4             getDefaultTransform (                    ) const;

        void                setRelativeTransform( const Matrix4 & tr );
        void                setGlobalTransform  ( const Matrix4 & tr );

        virtual Matrix4     getGlobalTransform  (                    ) const;
        virtual Matrix4     getLocalTransform   (                    ) const;

        virtual bool        parseRDSCommand     ( SToken cmd         );
    };

  }; // end namespace Klaus

}; // end namespace Klaus


#endif // SCENE_NODES_V2
