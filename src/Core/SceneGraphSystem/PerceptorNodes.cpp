/*
*********************************************************************************
*        PerceptorNodes.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for all perceptor nodes.      *
*            Perceptors get data from simulator and send them to agents.        *
*                                                                               *
*********************************************************************************
*/

/*! \file PerceptorNodes.cpp
<pre>
<b>File:</b>          PerceptorNodes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for all perceptor nodes.
               Perceptors get data from simulator and send them to agents.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#include "PerceptorNodes.h"




/********************************************************************/
/************************  Class ObjectState  ***********************/
/********************************************************************/

/**
  Constructor of ObjectState. Sets the ID name
*/
Klaus::RSG2::ObjectState::ObjectState() : mID("ID"), mSeenPosition("Seen Position")
{
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::ObjectState::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setID" && cmd.size() == 2)
    mID = readString(cmd[1]);
  else
    return false;

  return true;
}

/**
  This method returns the ID of the object
  @return ID of the object
*/
std::string Klaus::RSG2::ObjectState::getID() const
{
  return mID.mValue;
}

/**
  This method returns the seen position of the object
*/
Klaus::VecPosition Klaus::RSG2::ObjectState::getSeenPosition() const
{
  return mSeenPosition.mValue;
}

/**
  This method sets the seen position of the object
*/
void Klaus::RSG2::ObjectState::setSeenPosition(const Klaus::VecPosition& pos)
{
  mSeenPosition.mValue = pos;
}




/********************************************************************/
/********************  Class GyroRatePerceptor  *********************/
/********************************************************************/

/**
  This method resets the gyro rate perceptor's accumulated value
*/
void Klaus::RSG2::GyroRatePerceptor::reset()
{
  mRate.setPosition(0.0, 0.0, 0.0);
  mRotation.setPosition(0.0, 0.0, 0.0);
}

/**
  This method returns the gyro's current rate.
  @return Current rate of the gyro
*/
Klaus::VecPosition Klaus::RSG2::GyroRatePerceptor::getRate() const
{
  return mRate;
}

/**
  This method returns the rotation quaternion of the
  gyro's accumulated rate perceptions
  @return Gyro's rotation
*/
Klaus::Quaternion Klaus::RSG2::GyroRatePerceptor::getRotation() const
{
  Quaternion q = Quaternion(mRotation[0], VecPosition(1, 0, 0)) *
                 Quaternion(mRotation[1], VecPosition(0, 1, 0)) *
                 Quaternion(mRotation[2], VecPosition(0, 0, 1)) *
                 Quaternion(90, VecPosition(0, 0, 1));

  q.normalize();
  return q;
}

/**
  This method records a gyroscope perception. Sets the
  last speed perception and adds it to the accumulated
  cache.
  @param rt The percepted rate of the gyroscope
*/
void Klaus::RSG2::GyroRatePerceptor::percept(const Klaus::VecPosition& rt)
{
  mRate = rt;
  mRotation += rt;
}




/********************************************************************/
/***********************  Class Accelerometer  **********************/
/********************************************************************/

/**
  This method returns the percepted acceleration of the node
  @return Percepted acceleration of the node
*/
Klaus::VecPosition Klaus::RSG2::Accelerometer::getAcceleration() const
{
  return mAcceleration;
}

/**
  This method sets the percepted acceleration of the sensor
  @param a The percepted vector of acceleration
*/
void Klaus::RSG2::Accelerometer::setAcceleration(const Klaus::VecPosition& a)
{
  mAcceleration = a;
}




/********************************************************************/
/*****************  Class ForceResistancePerceptor  *****************/
/********************************************************************/

/**
  This method returns the percepted point of the force's focus
  relative to the node's position
  @return Percepted center of force's focus
*/
Klaus::VecPosition Klaus::RSG2::ForceResistancePerceptor::getCenter() const
{
  return mCenter;
}

/**
  This method sets focus point of force on the FRP
  @param c The center of the force
*/
void Klaus::RSG2::ForceResistancePerceptor::setCenter(const Klaus::VecPosition& c)
{
  mCenter = c;
}

/**
  This method returns the percepted force vector on the sensor
  @return Percepted force vector
*/
Klaus::VecPosition Klaus::RSG2::ForceResistancePerceptor::getForce() const
{
  return mForce;
}

/**
  This method sets the percepted force vector on the sensor
  @param f Percepted force on the FRP
*/
void Klaus::RSG2::ForceResistancePerceptor::setForce(const Klaus::VecPosition& f)
{
  mForce = f;
}




/********************************************************************/
/********************  Class VisionPerceptorBase  *******************/
/********************************************************************/

/**
  Constructor of the VisionPerceptorBase. Sets names
  for the variables in the perceptor.
*/
Klaus::RSG2::VisionPerceptorBase::VisionPerceptorBase() :
     mStaticSenseAxis("Static Sense Axis"),
     mSenseMyPos("Sense My Position"),
     mAddNoise("Add Noise")
{

}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::VisionPerceptorBase::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::Node::parseRSGCommand(cmd))
    return true;

  size_t size = cmd.size();
  if(cmd[0] == "setSenseMyPos" && size == 2)
    mSenseMyPos.mValue = cmd[1].boolean();
  else if(cmd[0] == "setStaticSenseAxis" && size == 2)
    mStaticSenseAxis.mValue = cmd[1].boolean();
  else if(cmd[0] == "addNoise" && size == 2)
    mAddNoise.mValue = cmd[1].boolean();
  else
    return false;

  return true;
}




/********************************************************************/
/******************  Class RestrictedVisionPerceptor  ***************/
/********************************************************************/

/**
  Constructor of RestrictedVisionPerceptor. Sets names for the
  perceptor's variables and initializes them.
*/
Klaus::RSG2::RestrictedVisionPerceptor::RestrictedVisionPerceptor() :
                                      mHorAngle("Horizontal View Angle"),
                                      mVerAngle("Vertical View Angle"),
                                      mInterval("Interval")
{
  mHorAngle.mValue = 360;
  mVerAngle.mValue = 360;
  mInterval.mValue = 1;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::RestrictedVisionPerceptor::parseRSGCommand(SToken cmd)
{
  if(Klaus::RSG2::VisionPerceptorBase::parseRSGCommand(cmd))
    return true;
  else if(cmd[0] == "setViewCones" && cmd.size() == 3)
  {
    mHorAngle.mValue = readNumber(cmd[1]);
    mVerAngle.mValue = readNumber(cmd[2]);
  }
  else if(cmd[0] == "setInterval" && cmd.size() == 2)
    mInterval.mValue = readNumber(cmd[1]);
  else
    return false;

  return true;
}




/********************************************************************/
/********************  Class JointPerceptor  ************************/
/********************************************************************/

/**
  Constructor of JointPerceptor. Sets name of the variables and
  sets their default values.
*/
Klaus::RSG2::JointPerceptor::JointPerceptor() : mAngle1("Angle 1"),
                                                mAngle2("Angle 2"),
                                                mRate1("Speed 1"),
                                                mRate2("Speed 2")
{
  mAngle1.mValue = 0.0;
  mAngle2.mValue = 0.0;
  mRate1.mValue = 0.0;
  mRate2.mValue = 0.0;
}

/**
  This method returns the angle for the given motor number.
  @param iNumber The motor number to get it's current angle
  @return The angle of the perceptor
*/
Klaus::Degree Klaus::RSG2::JointPerceptor::getAngle(int iNumber)
{
  if(iNumber == 0)
    return mAngle1.mValue;
  else if(iNumber == 1)
    return mAngle2.mValue;
  else
    return 0.0;
}

/**
  This method returns the angular speed for the given motor number.
  @param iNumber The motor number to get it's current angular velocity
  @return The angular speed of the perceptor
*/
Klaus::Degree Klaus::RSG2::JointPerceptor::getRate(int iNumber)
{
  if(iNumber == 0)
    return mRate1.mValue;
  else if(iNumber == 1)
    return mRate2.mValue;
  else
    return 0.0;
}

/**
  This method overrides the angle for perceptor. Needed for
  calculation of forward kinematics.
  @param dAngle1 The first angle to set in joint
  @param dAngle2 The second angle to set in joint
*/
void Klaus::RSG2::JointPerceptor::overrideAngle(Degree dAngle1, Degree dAngle2)
{
  mAngle1.mValue = dAngle1;
  mAngle2.mValue = dAngle2;
}

/**
  This method overrides the angular speed for perceptor. Needed for
  calculation of forward kinematics.
  @param dRate1 The first angular velocity of the joint
  @param dRate2 The second angular velocity of the joint
*/
void Klaus::RSG2::JointPerceptor::overrideRate(Degree dRate1, Degree dRate2)
{
  mRate1.mValue = dRate1;
  mRate2.mValue = dRate2;
}
