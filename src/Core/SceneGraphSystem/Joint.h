/*
*********************************************************************************
*              Joint.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for Joint. Joint is the       *
*            location where two body nodes connect. It is used to evaluate the  *
*            bodies relative to each other.graphical scene nodes.               *
*                                                                               *
*********************************************************************************
*/

/*! \file Joint.h
<pre>
<b>File:</b>          Joint.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for Joint. Joint is the
               location where two body nodes connect. It is used to evaluate the
               bodies relative to each other.graphical scene nodes.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef JOINT_SCENE_NODE
#define JOINT_SCENE_NODE

#include "PerceptorNodes.h" // needed for JointPerceptor, Node
#include "EffectorNodes.h"

namespace Klaus
{

  namespace RSG2
  {

    // forward decleration is needed to speed up compile time
    class _DLLExportControl Body;

    /*! This enumeration holds type of a joint. */
    enum JointType
    {
      Fixed,     /*!< Fixed joint */
      Hinge,     /*!< Hinge joint */
      Universal, /*!< Universal joint */
      Unknown    /*!< Unknown joint */
    };

    /*! This class is parser for joints. Joints physically connect two body nodes
        so that when one moves, it will influence on the other one just like a
        hinge. Joints can be of three types.

          - Fixed: Means the two nodes are hardly attached to each other.
          - Hinge: A joint that restricts nodes to moves only around one axel.
          - Universal: Two nodes can move around two axis.

        Joints have speed and motor force restrictions, so that they can not
        have more speed than they can tolerate.
    */
    class _DLLExportControl Joint : public Node
    {
      private:
        IntVar                 mJointType;          /*!< This is the type of the joint      */
        PositionVar            mAnchor;             /*!< This is the anchor the joint       */
        PositionVar            mAxis         [2];   /*!< This is the axis of the motors     */
        RealVar                mMaxMotorForce[2];   /*!< These are the maximum motor force  */
        RealVar                mMaxSpeed     [2];   /*!< Maximum speed of the joint         */
        RealVar                mRanges       [2][2];/*!< Restriction on the joint angle     */
        BooleanVar             mBounce       [2];   /*!< Whether joint uses bounce          */
        RealVar                mCFM          [2];   /*!< Constraint Force Mixing of joint   */
        RealVar                mStopCFM      [2];   /*!< CFM of joint at stopping           */
        RealVar                mStopERP      [2];   /*!< ERP of joint at stopping           */
        RealVar                mFudgeFactor  [2];   /*!< Fudge factor of the joint          */
        Body                 * mAttachedNode1;      /*!< Attached node 1                    */
        Body                 * mAttachedNode2;      /*!< Attached node 2                    */

        JointPerceptor       * mPerceptor;          /*!< Perceptor node of the joint        */
        JointEffector        * mEffector;           /*!< Effector node of the joint         */

        bool                   attach               ( std::string node1,
                                                      std::string node2       );
        bool                   motorExists          ( const int & iMotor      ) const;

        bool                   parseAxis            ( SToken cmd              );
        bool                   parseMotorInfo       ( SToken cmd              );
        bool                   parseMaxSpeed        ( SToken cmd              );

      protected:
        bool                   parseRSGCommand      ( SToken cmd              );
        void                   postLoadInternal     (                         );

      public:
                               Joint                (                         );
                             ~ Joint                (                         );

        JointType              getJointType         (                         ) const;

        Degree                 getAngle             ( const int & iMotor = 0  ) const;
        bool                   setAngle             ( const Degree & dAngle1  );
        bool                   setAngle             ( const Degree & dAngle1,
                                                      const Degree & dAngle2  );

        Degree                 getSpeed             ( const int & iMotor = 0  ) const;
        bool                   setSpeed             ( const Degree & dSpeed1  );
        bool                   setSpeed             ( const Degree & dSpeed1,
                                                      const Degree & dSpeed2  );

        Degree                 getMinAngle          ( const int & iMotor = 0  ) const;
        Degree                 getMaxAngle          ( const int & iMotor = 0  ) const;
        bool                   getAngleRange        ( double & dMin,
                                                      double & dMax,
                                                      const int & iMotor = 0  ) const;

        VecPosition            getAnchor            (                         ) const;
        VecPosition            getPivot             (                         ) const;
        Quaternion             getJointRotation     ( const double & time_gap ) const;
        Body                 * getAttachedBody1     (                         ) const;
        Body                 * getAttachedBody2     (                         ) const;

        JointPerceptor       * getPerceptor         (                         );
        const JointPerceptor * getPerceptor         (                         ) const;

        JointEffector        * getEffector          (                         );
        const JointEffector  * getEffector          (                         ) const;

        VecPosition            getAxis              ( int iNumber = 0         ) const;

        static JointType       getTypeFromString    ( const std::string & str );
        static std::string     getStringFromType    ( const JointType & t     );

        std::string            str                  (                         ) const;

        /*! Array is a dynamic array of joints */
        typedef std::vector<Joint*> Array;

        /*! Map connects joint instances to their perceptor names */
        typedef std::map<std::string, Joint*> Map;
    };

  }; // end namespace RSG2

}; // end namespace Klaus


#endif // JOINT_SCENE_NODE
