/*
*********************************************************************************
*             Graph.h : Robocup 3D Soccer Simulation Team Zigorat               *
*                                                                               *
*  Date: 13/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for Graph. Graph handles      *
*            simulator's scene graph management. This includes loading, using   *
*            and saving the scene graph. It will create a scene nodes hierarchy *
*            and create a usable API to access them.                            *
*                                                                               *
*********************************************************************************
*/

/*! \file Graph.h
<pre>
<b>File:</b>          Graph.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       13/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for Graph. Graph handles simulator's
               scene graph management. This includes loading, using and saving the scene graph.
               It will create a scene nodes hierarchy and create a usable API to access them.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/02/2011       Mahdi           Initial version created
</pre>
*/


#ifndef SCENE_NODES_GRAPH
#define SCENE_NODES_GRAPH


#include "Node.h"
#include <libZigorat/Logger.h>  // needed for Logger
#include "PhysicalNodes.h"      // needed for Body, GraphicalNode, Node
#include "Joint.h"              // needed for Joint::Array

namespace Klaus
{

  namespace RSG2
  {

    /*! Graph loads an entire scene graph and it's nodes. */
    class _DLLExportControl Graph
    {
      private:
        SExpression          mSceneTokens;           /*!< Tokens in the scene file */
        Logger             * mLog;                   /*!< Logger instance */
        int                  mVersionMajor;          /*!< Scene's major version */
        int                  mVersionMinor;          /*!< Scene's minor version */
        std::string          mSceneAddress;          /*!< Scene file address */

        Node               * mRoot;                  /*!< Root node of the graph */
        Node::Array          mCacheNodes;            /*!< All nodes in the graph */
        Node::MultiMap       mCacheMappedNodes;      /*!< Mapping between nodes and their types */
        GraphicalNode::Array mCacheGraphicNodes;     /*!< All root nodes in graphic trees */
        PerceptorBase::Array mCachePerceptors;       /*!< Perceptor nodes in the tree */
        EffectorBase::Array  mCacheEffectors;        /*!< Effector nodes in the tree */
        Joint::Map           mCacheJoints;           /*!< All joints in the tree */
        Body               * mRootBody;              /*!< Root body node */
        Kinematics           mCurrentState;          /*!< Current kinematics state of the graph */

        Matrix4              mGlobalTransform;       /*!< Global transform of the scene */

        void                 clearScene              (                                 );
        void                 resetCache              (                                 );

        // error output is in this method
        void                 error                   ( const char * strFormat, ...     );

        // parsers
        bool                 parseHeader             ( SToken token                    );
        bool                 parseBody               ( SToken token                    );

        // update
        void                 postLoad                (                                 );

      public:
                             Graph                   (                                 );
                           ~ Graph                   (                                 );

        bool                 loadFromFile            ( const std::string & strFileName );
        void                 saveToFile              ( const std::string & strFileName );

        // get methods
        std::string          getScene                (                                 );
        Node               * getRootNode             (                                 );
        void                 getAllNodes             ( Node::Array & nodes             );
        void                 getAllPerceptors        ( PerceptorBase::Array & lst      );
        void                 getAllEffectors         ( EffectorBase::Array & lst       );

        // getting a specific node
        Node               * getNodeByType           ( const std::string & strType,
                                                       const std::string & strName = "");

        // graphical methods
        void                 setOgreParent           ( Ogre::SceneNode * node          );
        void                 updateGraphics          (                                 );

        // kinematis
        void                 setGlobalTransform        ( const VecPosition & pos,
                                                         const Quaternion & q            );
        void                 updateForwardKinematics   (                                 );
        void                 getForwardKinematics      ( Kinematics & sheet,
                                                         const double & time_gap = 0     );
        void                 getForwardKinematicsFor   ( const InverseKinematics & angles,
                                                         Kinematics & sheet              );
        VecPosition          getCenterOfMass           (                                 );
        void                 applyForwardKinematicSheet( Kinematics & sheet              );

        void                 getInverseKinematics      ( InverseKinematics & sheet       );
        void                 applyInverseKinematicSheet( InverseKinematics & sheet       );
    };

  }; // end namespace RSG

}; // end namespace Klaus


#endif // SCENE_NODES_GRAPH
