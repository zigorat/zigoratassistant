/*
*********************************************************************************
*              Node.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 13/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for SceneNodes. SceneNodes    *
*            are the parsers of every bit of simulators scene graph.            *
*            Simulator's scene graph are where every detail of simulator's      *
*            scene and robots are stored. Their location is in the rsg folder   *
*            of simulator's installation path.                                  *
*                                                                               *
*********************************************************************************
*/

/*! \file Node.cpp
<pre>
<b>File:</b>          Node.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       13/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for SceneNodes. SceneNodes
               are the parsers of every bit of simulators scene graph.
               Simulator's scene graph are where every detail of simulator's
               scene and robots are stored. Their location is in the rsg folder
               of simulator's installation path.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/02/2011       Mahdi           Initial version created
</pre>
*/

#include "Node.h"
#include <libZigorat/Parse.h>  // needed to extract a number from a string
#include <libZigorat/Logger.h> // needed for Logger
#include "NodeFactory.h"       // needed for Klaus::RSG2::NodeFactoryCore
#include "Templates.h"         // needed for TemplateManager


using std::cout;
using std::cerr;
using std::endl;




/************************************************************************/
/******************************  Class Node  ****************************/
/************************************************************************/

/**
  Constructor of the Node. Initializes default instances,
  values and property names.
*/
Klaus::RSG2::Node::Node() : mName("Name")
{
  mConfig = NodeConfig::getSingleton();
  mParent = NULL;

  addProperty(&mName);
}

/**
  Destructor of the Node. Removes everything.
*/
Klaus::RSG2::Node::~Node()
{
  clear();
}

/**
  This method removes all children of the node
*/
void Klaus::RSG2::Node::clear()
{
  for(size_t i = 0; i < mChildren.size(); ++i)
    delete mChildren[i];

  mChildren.clear();
  mDefinitions.clear();
}

/**
  This method adds a child to the node.
  @param node The token containing child information
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Node::addChild(SToken node)
{
  int type_index = -1;
  bool b1 = false;
  for(size_t i = 0; i < node.size(); ++i)
    if(node[i].isComment() || node[i].isAtom())
    {
      if(node[i].isAtom())
      {
        if(b1)
        {
          type_index = i;
          break;
        }
        else if(node[i] == "nd")
          b1 = true;
        else
          break;
      }
    }
    else
      break;

  if(type_index == -1)
  {
    error("(Node) unknown node type for '%s'", node.str().c_str());
    return false;
  }

  // create parser for the node
  Node * nd = NodeFactoryCore::getSingleton()->createInstanceFor(node[type_index]);
  if(!nd)
  {
    error("(Node) addChild failed to create parser for type '%s'", node[type_index].str().c_str());
    return false;
  }

  // parse it
  nd->setParent(this);
  if(!nd->parse(node))
  {
    error("(Node) addChild failed");
    delete nd;
    return false;
  }

  // add it to children
  mChildren.push_back(nd);
  mChildrenMap.insert(Klaus::RSG2::Node::MultiMap::value_type(nd->getType(), nd));
  return true;
}

/**
  This method parses a switch statement. This statement selects a
  group of commands and nodes to be executed if a certain condition
  is evaluated as true.
  @param cmd The switch command
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Node::parseSwitch(SToken cmd)
{
  if(cmd.size() < 4)
  {
    error("(Node) switch tokens less than minimum");
    return false;
  }

  CVar cv = getDefinition(cmd[1]);
  if(!cv)
  {
    error("(Node) switch var not defined: '%s'", cmd[1].str().c_str());
    return false;
  }

  // now search all cases to find the matching case
  int iChoice = -1;
  for(size_t i = 2; i < cmd.size(); ++i)
    if(cmd[i].isNormal() && cmd[i].size() > 0 && cmd[i][0].isAtom())
    {
      if((cv->getType() == Var::VarString && readString(cmd[i][0]) == cv->getValue())
         ||
         (cv->getType() == Var::VarReal && readNumber(cmd[i][0]) == dynamic_cast<const RealVar*>(cv)->mValue))
      {
        iChoice = (int)i;
        break;
      }
    }

  if(iChoice == -1)
  {
    error("(Node) no case was true for switch");
    return false;
  }

  // inject all the switches command to parsing
  for(size_t i = 1; i < cmd[iChoice].size(); ++i)
    if((!parseToken(cmd[iChoice][i])) && (mConfig->errorsBreakParse()))
      return false;

  return true;
}

/**
  This method parses a def command. This command defines
  a variable and assigns it a certain value.
  @param cmd The define command
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Node::parseDefine(SToken cmd)
{
  if(cmd.size() < 3)
  {
    error("(Node) error on define command: '%s'", cmd.str().c_str());
    return false;
  }

  // all possibilities:
  // (eval Nao.UseTexture)
  // (eval
  // (join
  // $x
  // 12
  // text

  Var * v = NULL;
  if(cmd[2] == "(eval Nao.UseTexture)")
  {
    v = new StringVar(cmd[1]);
    v->setValue("true");
  }
  else if(cmd[2].isAtom() == false)
  {
    // (eval
    // (join

    if(cmd[2][0].isAtom() == false)
    {
      error("(Node) compound command is also compound in define: '%s'", cmd[2][0].str().c_str());
      return false;
    }

    if(cmd[2][0] == "eval")
    {
      v = new RealVar(cmd[1]);
      ((RealVar*)v)->mValue = parseEvaluate(cmd[2]);
    }
    else if(cmd[2][0] == "join")
    {
      v = new StringVar(cmd[1]);
      v->setValue(join(cmd[2]));
    }
    else
    {
      error("(Node) unknown compound command in define: '%s'", cmd[2].str().c_str());
      return false;
    }
  }
  else
  {
    // $x
    // 12
    // text

    if(cmd[2].str()[0] == '$')
    {
      // it's a definition
      CVar cv = getDefinition(cmd[2]);
      if(cv->getType() == Var::VarReal)
        v = new RealVar(cmd[1]);
      else if(cv->getType() == Var::VarString)
        v = new StringVar(cmd[1]);
      else
      {
        error("(Node) unknown definition type in define: '%s'", cv->getName().c_str());
        return false;
      }

      v->setValue(cv->getValue());
    }
    else
    {
      // its a simple number or a simple string
      int index = 0;
      double result;
      if(Parse::parseFirstReal(cmd[2], result, index))
        v = new RealVar(cmd[1]);
      else
        v = new StringVar(cmd[1]);

      v->setValue(cmd[2]);
    }
  }

  // begin operating on the other tokens
  // now the variable has it's type set up
  // the other tokens shall only implement numerical evaluations
  if(cmd.size() > 3)
  {
    RealVar * rv = dynamic_cast<RealVar*>(v);
    if(!rv)
    {
      error("(Node) definition has more than 3 tokens which are not numerical: '%s'", v->getName().c_str());
      return false;
    }

    rv->mValue = evaluate(cmd, 2);
  }

  setDefinition(v);
  return true;
}

/**
  This method parses an importScene command. This command loads a template file
  directly into the node. The template is a parameteric file which contains nodes
  and data that can be inserted into any other node.
  @param cmd The command
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Node::parseImportScene(SToken cmd)
{
  Template * t = TemplateManager::getSingleton()->getTemplate(readString(cmd[1]));
  if(!t)
  {
    error("(Node) could not acquire template instance for '%s'", cmd[1].str().c_str());
    return mConfig->errorsBreakParse() ? false : true;
  }

  return t->inject(this, cmd);
}

/**
  This method parses an eval command. This command evaluates an arithmetic expression.
  @param token The token containing the evaluation command
  @return The evaluated result
*/
double Klaus::RSG2::Node::parseEvaluate(SToken token) const
{
  if((token[0].isAtom() == false) || (token[0] != "eval") || (token.size() < 2))
  {
    error("(Node) can not evaluate a non evaluate command: '%s'", token.str().c_str());
    return 0;
  }

  return evaluate(token, 1);
}

/**
  This method parses an evaluation. An evaluation is an arithmetic expression
  containin both definitions and inner evaluations.
  @param token The token containing the arithmetic expression
  @param start Start index of the evaluation in the expression
  @return The evaluated result of expression
*/
double Klaus::RSG2::Node::evaluate(SToken token, size_t start) const
{
  double result = 0;
  char c = '+';

  for(size_t i = start; i < token.size(); ++i)
  {
    double num = readNumber(token[i++]);

    if(c == '+')
      result += num;
    else if(c == '-')
      result -= num;
    else if(c == '*')
      result *= num;
    else if(c == '/')
      result /= num;
    else
    {
      error("(Node) unknown operator in evaluation : '%c'", c);
      return result;
    }

    if(i < token.size())
      c = token[i].str()[0];
  }

  return result;
}

/**
  This method joins the given parameters as a string.
  @param token The token containing the strings to join
  @return The joined string
*/
std::string Klaus::RSG2::Node::join(SToken token) const
{
  if((token[0].isAtom() == false) || (token[0] != "join"))
  {
    error("(Node) can not join a non join command: '%s'", token.str().c_str());
    return "";
  }

  std::string s;
  for(size_t i = 1; i < token.size(); ++i)
    s = s + readString(token[i]);

  return s;
}

/**
  This method is responsible to parse RSG commands. These commands describe
  scene nodes based on their type. Each node shall implement their own command
  parser, but they shall also call their parent parser within their own parser
  if they don't want to re-implement derived functionality.
  @param cmd The command to parse
  @return True if parse was successfull, false otherwise
*/
bool Klaus::RSG2::Node::parseRSGCommand(SToken cmd)
{
  if(cmd.size() == 2 && cmd[0] == "setName")
    mName.setValue(readString(cmd[1]));
  else if(cmd[0] == "importScene")
    return parseImportScene(cmd);
  else
    return false;

  return true;
}

/**
  This method is responsible to add a property of the derived
  classes to the list of the node's properties so that it can be
  accessed and known without knowing it's actual class type.
  @param prop The property to add
*/
void Klaus::RSG2::Node::addProperty( Var * prop )
{
  mProperties.push_back(prop);
}

/**
  This method is called to do anything needed to be done prior to a physical update.
*/
void Klaus::RSG2::Node::prePhysicsInternal()
{
  // nothing to be done in this class
}

/**
  This method is called to do anything needed to be done in a physical update interval.
*/
void Klaus::RSG2::Node::physicsInternal()
{
  // nothing to be done in this class
}

/**
  This method is called to do anything needed to be done after a physical update.
*/
void Klaus::RSG2::Node::postPhysicsInternal()
{
  // nothing to be done in this class
}

/**
  This method is called to initialize the node after all the graph has been loaded.
*/
void Klaus::RSG2::Node::postLoadInternal()
{
  // nothing to be done in this class
}

/**
  This method is called whenever a property is changed outside of the class. The
  class is responsible to apply the update on it's behaviour.
*/
void Klaus::RSG2::Node::syncToProperties()
{
  // nothing to be done in this class
}

/**
  This method handles errors withing all derived classes. All errors will be
  sent here with their parameters and they will be logged, thrown or printed
  depending on the configuration of the system.
  @param strFormat Charactor string with possible format specifiers
*/
void Klaus::RSG2::Node::error(const char* strFormat, ... ) const
{
  va_list ap;
  char buf[1024];
  va_start(ap, strFormat);
  vsnprintf(buf, 1023, strFormat, ap);
  va_end(ap);

  if(mConfig->loggerEnabled())
    Logger::getSingleton()->log(28, buf);
  if(mConfig->stdoutEnabled())
    cout << buf << endl;
  if(mConfig->stderrEnabled())
    cerr << buf << endl;
  if(mConfig->exceptionsEnabled())
    throw buf;
}

/**
  This method finds a relative node from a given address. The address
  describe the location of the node in the graph via file system like paths.
  @param address The address to which the node is located
  @return The node's instance if the address was pointing to a valid location, NULL otherwise
*/
Klaus::RSG2::Node* Klaus::RSG2::Node::getRelativeNode(const std::string & address)
{
  Node * result = this;
  StringArray tokens;
  if(!Parse::splitByDelim(address, '/', tokens))
  {
    error("(Node) relative node: could not parse path '%s'", address.c_str());
    return NULL;
  }

  for(size_t i = 0; i < tokens.size(); ++i)
  {
    if(tokens[i] == "..") // Parent folder
    {
      if(result->getParent())
        result = result->getParent();
      else
      {
        error("(Node) relative node: got null parent while going up: %s", address.c_str());
        return NULL;
      }
    }
    else if(tokens[i] == ".") // this folder!
    {
      // Nothing to do!
      // result = result!
    }
    else
    {
      Node * temp = result->getChildByName(tokens[i]);
      if(temp)
        result = temp;
      else  // name was not found
      {
        error("(SceneJoint) relative node: child not found: %s", tokens[i].c_str());
        return NULL;
      }
    }
  }

  return result;
}

/**
  This method returns the name of the node
  @return Name of the node
*/
std::string Klaus::RSG2::Node::getName() const
{
  return mName.mValue;
}

/**
  This method returns the type of the node
  @return Type of the node
*/
std::string Klaus::RSG2::Node::getType() const
{
  return mType;
}

/**
  This method returns number of the node's children
  @return Number of children the node has
*/
size_t Klaus::RSG2::Node::getChildCount() const
{
  return mChildren.size();
}

/**
  This method returns the specified child of the node by its index
  @param index The index of the child
  @return The requested child instance
*/
Klaus::RSG2::Node* Klaus::RSG2::Node::getChildByIndex(const size_t& index)
{
  return index < mChildren.size() ? mChildren[index] : NULL;
}

/**
  This method searches for a spcific node in the caller node's children by checking its name
  @param strName Name of the node
  @return The child node if existed, NULL otherwise
*/
Klaus::RSG2::Node* Klaus::RSG2::Node::getChildByName(const std::string& strName)
{
  for(size_t i = 0; i < mChildren.size(); ++i)
    if(mChildren[i]->mName.mValue == strName)
      return mChildren[i];

  return NULL;
}

/**
  This method finds the specified child by its type and name.
  @param strType Type of the node
  @param strName Name of the node
*/
Klaus::RSG2::Node* Klaus::RSG2::Node::getChildByType(const std::string& strType, const std::string& strName)
{
  Klaus::RSG2::Node::Iterator it = mChildrenMap.equal_range(strType);
  if(it.first == it.second)
    return NULL;

  for(; it.first != it.second; it.first++)
    if(strName.empty() || it.first->second->getName() == strName)
      return it.first->second;

  return NULL;
}

/**
  This method finds a variable with the given name and returns it.
  @param strName Name of the variable
  @return The variable instance. NULL if it was not found
*/
const Klaus::Var* Klaus::RSG2::Node::getDefinition(const std::string& strName) const
{
  // first search the node itself
  for(size_t i = 0; i < mDefinitions.size(); ++i)
    if(mDefinitions[i]->getName() == strName)
      return mDefinitions[i];

  // then search the upper nodes
  if(mParent)
    return mParent->getDefinition(strName);
  else
    return NULL;
}

/**
  This method sets a definition in the list of the node's definitions.
  @param v The variable to add
*/
void Klaus::RSG2::Node::setDefinition(Var* v)
{
  for(size_t i = 0; i < mDefinitions.size(); ++i)
    if(mDefinitions[i]->getName() == v->getName())
    {
      mDefinitions[i]->setValue(v->getValue());
      delete v;
      return;
    }

  mDefinitions.push_back(v);
}

/**
  This method reads a number from the given string. If the string
  was a name for a definition, the definition value will be returned
  instead. If it was an evaluation, it will be evaluated
  @param token Token containing the number
  @return The number in the string
*/
double Klaus::RSG2::Node::readNumber(SToken token) const
{
  if(token.isAtom() == false)
  {
    // if the token is not atom, then it shall only be evaluation
    return parseEvaluate(token);
  }

  // if token is atom, then it can only be a number or a definition
  // it's a definition?
  if(token.str()[0] == '$')
  {
    const RealVar * cv = dynamic_cast<const RealVar*>(getDefinition(token));
    if(!cv)
    {
      error("(Node) readNumber failed for getting definition '%s'", token.str().c_str());
      return 0;
    }

    return ((const RealVar *)cv)->mValue;
  }

  // then it's a simple number
  return token.number();
}

/**
  This method reads a string from the given string. If the string
  was a name for a definition, the definition value will be returned
  instead. If it was a join command, the result will be joined from
  strings in the given token
  @param token The token containing the actual string
  @return The string in the given token
*/
std::string Klaus::RSG2::Node::readString(SToken token) const
{
  if(token.isAtom() == false)
  {
    // it can only be a join command
    return join(token);
  }

  // now it can only be a simple text or a definition
  // a definition?
  if(token.str()[0] == '$')
  {
    CVar cv = getDefinition(token);
    if(!cv)
    {
      error("(Node) readString failed for getting definition '%s'", token.str().c_str());
      return "";
    }

    return cv->getValue();
  }

  // then it's a simple string
  return token;
}

/**
  This method returns the parent of the node.
  @return The parent of the node
*/
Klaus::RSG2::Node* Klaus::RSG2::Node::getParent() const
{
  return mParent;
}

/**
  This method sets the parent of the node. Shall be overriden for
  classes which need to know of parent changes like graphical nodes.
  @param parent The new parent of the node
*/
void Klaus::RSG2::Node::setParent(Node* parent)
{
  mParent = parent;
}

/**
  This method is responsible to parse the given tokens and create the node.
  @param token The tokens containing the node information
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Node::parse(SToken token)
{
  // create a list of available tokens
  // and store the comments
  mComments.clear();

  // now parse them
  for(size_t i = 0; i < token.size(); ++i)
    if(!parseToken(token[i]) && (mConfig->errorsBreakParse()))
      return false;

  return true;
}

/**
  This method parses a scene graph token based on its traits. If it is a node
  command, a subnode will be added otherwise the command will be parsed as
  native command for the node
  @param token The token to be parsed
  @return True on successful parse, false otherwise
*/
bool Klaus::RSG2::Node::parseToken(SToken token)
{
  // add comments
  if(token.isComment())
  {
    mComments.push_back(token);
    return true;
  }

  // set the node type
  if(token.isAtom())
  {
    if(token != "nd")
      mType = token.str();

    return true;
  }

  // should have set the node type till now
  if(mType.empty())
    error("(Node) unknown node type...");

  // parse the token
  if(token.empty())
    error("(Node) empty token in '%s'", token.str().c_str());
  else if((token[0].isAtom()) && (token[0] == "nd"))
    return addChild(token);
  else if(token[0].isAtom() == false)
    error("(Node) command with a non-atom name token: '%s'", token.str().c_str());
  else if(token[0] == "switch")
    return parseSwitch(token);
  else if(token[0] == "def")
    return parseDefine(token);
  else if(token[0] == "importScene")
    return parseImportScene(token);
  else
  {
    bool bResult = parseRSGCommand(token);
    if(!bResult)
      error("(Node) '%s': could not parse RSG command '%s'", mType.c_str(), token.str().c_str());

    return bResult;
  }

  return false;
}

/**
  This method returns all the comments of the node
  @return Comments of the node as a string array
*/
const StringArray& Klaus::RSG2::Node::getComments() const
{
  return mComments;
}

/**
  This method sets all comments to the given array of comments
  @param comments The array of strings which contain the comments
*/
void Klaus::RSG2::Node::setComments(const StringArray& comments)
{
  mComments.clear();
  mComments.assign(comments.begin(), comments.end());
}

/**
  This method adds a comment to the list of node's comments
  @param comment The comment to add to the node
*/
void Klaus::RSG2::Node::addComment(const std::string& comment)
{
  mComments.push_back(comment);
}

/**
  This method removes a comment via it's index.
  @param index The index of the comment
*/
void Klaus::RSG2::Node::removeComment(const size_t& index)
{
  if(index < mComments.size())
    mComments.erase(mComments.begin() + index);
}

/**
  This method returns all the registered properties as a standard vector.
  @return All the registered properties of the node
*/
const Klaus::Var::Array& Klaus::RSG2::Node::getAllProperties() const
{
  return mProperties;
}

/**
  This method sets the requested property to the given value.
  Informs the node of the update afterwards.
  @param strName Name of the property
  @param strValue Value for the property
  @return True if the requested property was found, false otherwise
*/
bool Klaus::RSG2::Node::setProperty(const std::string& strName, const std::string& strValue)
{
  for(size_t i = 0; i < mProperties.size(); ++i)
    if(mProperties[i]->getName() == strName)
    {
      mProperties[i]->setValue(strValue);
      syncToProperties();
      return true;
    }

  error("(Node) no such property: '%s'", strName.c_str());
  return false;
}

/**
  This method returns the global transform of the node.
  This is relative to the top most node in the graph.
  @return Relative transform of the node to the tree's root node
*/
Klaus::Matrix4 Klaus::RSG2::Node::getGlobalTransform() const
{
  if(mParent)
    return mParent->getGlobalTransform();
  else
    return Matrix4::identity;
}

/**
  This method returns the local transform of the node. This is
  relative transformation to the upper node. Basically this is
  identity transformation unless the node is a transform node.
  @return Relative transformation of the node to it's parent.
*/
Klaus::Matrix4 Klaus::RSG2::Node::getLocalTransform() const
{
  return Matrix4::identity;
}

/**
  This method finds out the relative transform between the node and the given node
  @param node The node to find the relative transform
  @return The relative transform
*/
Klaus::Matrix4 Klaus::RSG2::Node::getRelativeTransform(Node * node)
{
  return node->getGlobalTransform() * getGlobalTransform().getInverseMatrix();
}

/**
  This method informs the node and it's children of an incoming physical update.
*/
void Klaus::RSG2::Node::prePhysicsUpdate()
{
  prePhysicsInternal();

  for(size_t i = 0; i < mChildren.size(); ++i)
    mChildren[i]->prePhysicsUpdate();
}

/**
  This method informs the node and it's children that a physical update is in
  progress and they shall update whichever property they need to update.
*/
void Klaus::RSG2::Node::physicsUpdate()
{
  physicsInternal();

  for(size_t i = 0; i < mChildren.size(); ++i)
    mChildren[i]->postPhysicsUpdate();
}

/**
  This method informs the node and it's children that a physical update is finished.
*/
void Klaus::RSG2::Node::postPhysicsUpdate()
{
  postPhysicsInternal();

  for(size_t i = 0; i < mChildren.size(); ++i)
    mChildren[i]->postPhysicsUpdate();
}

/**
  This method adds the node and its children recursively to the given list
  @param nodes The node and it's children will be copied here
*/
void Klaus::RSG2::Node::queryNodes(Klaus::RSG2::Node::Array& nodes)
{
  nodes.push_back(this);
  for(size_t i = 0; i < mChildren.size(); ++i)
    mChildren[i]->queryNodes(nodes);
}

/**
  This method informs the node and it's children that the whole scene graph is
  loaded completely and they can do whatever they need to do for initialization.
*/
void Klaus::RSG2::Node::postLoad()
{
  postLoadInternal();

  for(size_t i = 0; i < mChildren.size(); ++i)
    mChildren[i]->postLoad();
}
