/*
*********************************************************************************
*            NaoSkills.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 02/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for implementing skills for robots.             *
*                                                                               *
*********************************************************************************
*/

/*! \file NaoSkills.h
<pre>
<b>File:</b>          NaoSkills.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       02/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for implementing skills for robots.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
02/24/2011       Mahdi           Initial version created
</pre>
*/

#ifndef NAO_SKILLS
#define NAO_SKILLS

#include <AgentBase/Skill.h>


namespace Klaus
{

  /**
    Base class to implement skills specifically for nao.
  */
  class _DLLExportControl NaoSkill : public Skill
  {
    public:
      NaoSkill();
  };


  /**
    A very simpple walk gesture animation. Created here to
    demonstrate creation and use of skills.
  */
  class _DLLExportControl Walk : public NaoSkill
  {
    private:
      VecPosition  mHip;             /*!< Start position of the hip */

      double       mPeriod;          /*!< Gait interval */
      double       mGaitLength;      /*!< Gait length */
      double       mGaitHeight;      /*!< Gait height */
      double       mThighLength;     /*!< Length of the thigh */
      double       mShankLength;     /*!< Length of the shank */

    protected:
      virtual void initializeInternal(            );
      virtual void executeInternal   (            );
  };

}; // end namespace Klaus


#endif // NAO_SKILLS
