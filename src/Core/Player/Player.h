/*
*********************************************************************************
*             Player.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This files contains class decleration Player which is the main     *
*            thinkin' module                                                    *
*                                                                               *
*********************************************************************************
*/

/*! \file Player.h
<pre>
<b>File:</b>          Player.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This files contains class decleration Player which is the main
               thinkin' module
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#ifndef PLAYER_H
#define PLAYER_H

#include <AgentBase/BasicAgent.h> // needed for BasicAgent
#include "NaoSkills.h"            // needed for nao skills


namespace Klaus
{

  /*! This class is a superclass from BasicPlayer and contains the
      decision procedure to determine the next action, using the
      information provided by the WorldModel.
  */
  class Player : public BasicAgent, public Singleton<Player>
  {
    private:
      Walk            mWalk;              /*!< Walk skill */

    protected:
      /// Play mode decisioning related methods
      void            startGame           (                               );
      void            throwIn             (                               );
      void            takeCorner          (                               );
      void            playOnGoalKick      (                               );
      void            takeFreeKick        (                               );
      void            play                (                               );

      void            decideBallAction    (                               );

      void            makeGoalieDecision  (                               );
      void            makeDecision        (                               );

      /// Skills
      void            walk                (                               );

      /// Constructor and destructor are private, for singleton pattern
                      Player              (                               );
                    ~ Player              (                               );

    public:
      static Player * getSingleton        (                               );
      static void     freeSingleton       (                               );

      /// Initialization
      virtual bool    initialize          ( const std::string & name,
                                            const int & iNum = 0          );

      /// Main method
      void            mainLoop            (                               );

      /// Simple decision method
      void            malavaneAnzali      (                               );
  };

}; // end namespace Klaus

#endif
