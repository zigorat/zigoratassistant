/*
*********************************************************************************
*           NaoSkills.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 02/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for implementing skills for robots.             *
*                                                                               *
*********************************************************************************
*/

/*! \file NaoSkills.cpp
<pre>
<b>File:</b>          NaoSkills.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       02/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for implementing skills for robots.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
02/24/2011       Mahdi           Initial version created
</pre>
*/

#include "NaoSkills.h"


/******************************************************************************/
/****************************  Class NaoSkill  ********************************/
/******************************************************************************/

/**
  Constructor of NaoSkill. Creates all needed PID controllers
*/
Klaus::NaoSkill::NaoSkill()
{
  // create the pid controllers for nao robot
  mPIDs["hj1"]  = new PIDController;
  mPIDs["hj2"]  = new PIDController;

  mPIDs["laj1"] = new PIDController;
  mPIDs["laj2"] = new PIDController;
  mPIDs["laj3"] = new PIDController;
  mPIDs["laj4"] = new PIDController;

  mPIDs["raj1"] = new PIDController;
  mPIDs["raj2"] = new PIDController;
  mPIDs["raj3"] = new PIDController;
  mPIDs["raj4"] = new PIDController;

  mPIDs["llj1"] = new PIDController;
  mPIDs["llj2"] = new PIDController;
  mPIDs["llj3"] = new PIDController;
  mPIDs["llj4"] = new PIDController;
  mPIDs["llj5"] = new PIDController;
  mPIDs["llj6"] = new PIDController;

  mPIDs["rlj1"] = new PIDController;
  mPIDs["rlj2"] = new PIDController;
  mPIDs["rlj3"] = new PIDController;
  mPIDs["rlj4"] = new PIDController;
  mPIDs["rlj5"] = new PIDController;
  mPIDs["rlj6"] = new PIDController;
}




/******************************************************************************/
/*****************************  Class Walk  ***********************************/
/******************************************************************************/

/**
  Initializes the walk skill by getting the length of shank and thigh.
*/
void Klaus::Walk::initializeInternal()
{
  mPeriod = 0.6;
  mGaitLength = 0.35;
  mGaitHeight = 0.13;

  mThighLength = mState["llj3"].mPivot.getDistanceTo(mState["llj4"].mPivot);
  mShankLength = mState["llj4"].mPivot.getDistanceTo(mState["llj5"].mPivot);
  mHip[2] = mShankLength + mThighLength - 0.05;
}

void Klaus::Walk::executeInternal()
{
  // get the current state of robot
  mState["laj1"].mAngles[0] = -90;
  mState["raj1"].mAngles[0] = -90;

  VecPosition hip, foot, sol;
  Degree angKnee, angHip, angFoot, angSol, angTorso = 10;
  Radian angPhase;
  int sign;
  Klaus::Plane p;
  Line3 l;

  // left foot
  // find the phase
  angPhase = 2 * M_PI * mWM->getTime() / mPeriod;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * angPhase.cos();
  foot[2] = Max(mGaitHeight / 2.0 * angPhase.sin(), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + angSol * sign + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;

  // side angles
  mState["llj3"].mAngles[0] = angHip.getDegree();
  mState["llj4"].mAngles[0] = angKnee.getDegree();
  mState["llj5"].mAngles[0] = angFoot.getDegree();



  // right foot
  // find the phase
  angPhase = 2 * M_PI * mWM->getGameTime() / mPeriod - M_PI;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * angPhase.cos();
  foot[2] = Max(mGaitHeight / 2.0 * angPhase.sin(), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + angSol * sign + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;

  // apply inverse kinematics
  mState["rlj3"].mAngles[0] = angHip.getDegree();
  mState["rlj4"].mAngles[0] = angKnee.getDegree();
  mState["rlj5"].mAngles[0] = angFoot.getDegree();
}
