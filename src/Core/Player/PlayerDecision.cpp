/*
*********************************************************************************
*        AgentDecision.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This files contains decision making mechanisms for agent decision  *
*            hierarchy                                                          *
*                                                                               *
*********************************************************************************
*/

/** \file PlayerDecision.cpp
<pre>
<b>File:</b>          PlayerDecision.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This files contains decision making mechanisms for agent decision
               hierarchy
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#include "Player.h"
#include <libZigorat/Logger.h>


/**
  This decision method is used when play mode is "Kick Off" or "Before Kick Off"
*/
void Klaus::Player::startGame()
{
  // Change formation according to the side who is taking the kick
  if(mWM->isBeforeKickOffUs() && mFormation->getCurrentFormation() != Formation_Initial_Defensive)
    mFormation->setCurrentFormation(Formation_Initial_Defensive);
  else if(mWM->isBeforeKickOffThem() && mFormation->getCurrentFormation() != Formation_Initial_Offensive)
    mFormation->setCurrentFormation(Formation_Initial_Offensive);

  // beam if not beamed
  if(mWM->getRelativeDistance(mWM->getStrategicPosition()) > 1 && mWM->getTime() > 4)
    beamToStrategicPosition();
}

/**
  This decision method is used when play mode is "Kick In"
*/
void Klaus::Player::throwIn()
{
  // Nothing to do here by the base
  // Teams should implement their own skills
}

/**
  This decision method is used when play mode is "Corner Kick"
*/
void Klaus::Player::takeCorner()
{
  // Nothing to do here by the base
  // Teams should implement their own skills
}

/**
  This decision method is used when play mode is "Goal Kick"
*/
void Klaus::Player::playOnGoalKick()
{
  // Nothing to do here by the base
  // Teams should implement their own skills
}

/**
  This decision method is used when play mode is "Free Kick"
*/
void Klaus::Player::takeFreeKick()
{
  // Nothing to do here by the base
  // Teams should implement their own skills
}

/**
  This decision method is used when play mode is "Play On"
*/
void Klaus::Player::play()
{
  malavaneAnzali();
}

/**
  This decision method is used when play mode is "Play On" and Ball is rechable for agent
*/
void Klaus::Player::decideBallAction()
{
  // Nothing to do here by the base
  // Teams should implement their own skills
}

/**
  This method is the main decision selection part for the agent all the
  decisions are made here
*/
void Klaus::Player::makeDecision()
{
  // First send player number and team name to the simulator.
  if(mWM->getAgentNumber() == -1)
  {
    sendPlayerInformation();
	  return;
  }

  // After the player's team has been selected, decide based on number and playmode
  // Goalies decisions are not based on play modes

       if(mWM->getAgentNumber() == 1)
    makeGoalieDecision();
  else if(mWM->isBeforeKickOff())
    startGame();
  else if(mWM->isCornerKickThem() || mWM->isCornerKickUs())
    takeCorner();
  else if(mWM->isKickInUs() || mWM->isKickInThem())
    throwIn();
  else if(mWM->isGoalKickThem() || mWM->isGoalKickUs())
    playOnGoalKick();
  else if(mWM->isDeadBallUs() || mWM->isDeadBallThem())
    takeFreeKick();
  else
    play();
}

/*! Walk's biped locomotion */
void Klaus::Player::walk()
{
  mWalk.execute();
}

/**
  This method is the first complete simple agent and defines the actions
  taken by the agent. It is based on highlevel actions which are goin' to
  be coded.
*/
void Klaus::Player::malavaneAnzali()
{
  searchBall();
}
