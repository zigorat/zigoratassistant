/*
*********************************************************************************
*            Player.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This files contains class definition for Agent class. Agent class  *
*            is the core of agent's decision mechanism.                         *
*                                                                               *
*********************************************************************************
*/

/** \file Player.cpp
<pre>
<b>File:</b>          Player.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This files contains class definition for Agent class. Agent class
               is the core of agent's decision mechanism.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#include "Player.h"

/*! Player singleton instance */
template<> Klaus::Player * Klaus::Singleton<Klaus::Player>::mInstance = 0;

/**
  This is the constructor the Agent and calls the constructor of the
  superclass BasicPlayer.
*/
Klaus::Player::Player()
{}

/**
  This is the destructor of agent and currently does nothing.
*/
Klaus::Player::~Player()
{}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Player * Klaus::Player::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new Player;
    Logger::getSingleton()->log(1, "(Agent) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Player::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(Agent) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(Agent) removed singleton");
  }
}

/**
  This method is responsible to initialize the agent
  @return True on succesfull update, false otherwise
*/
bool Klaus::Player::initialize(const std::string& name, const int& iNum)
{
  if(!Klaus::BasicAgent::initialize(name, iNum))
    return false;

  // initialize skills
  mWalk.initialize();

  return true;
}

/**
  This is the main loop of the agent. This method calls the update methods
  of the world model after it is indicated that new information has arrived.
  After this, the main thinkin' loop of the agent is called, which sends
  the best command according to his/her! simple mind :) to the server.
*/
void Klaus::Player::mainLoop()
{
  mLog->log(38, "(Player) player execution started");

  std::string strMsg;
  while(mTerminate == false)
  {
    try
    {
      if(!mConnection->getMessage(strMsg))
        break;

      mWM->parseSimSparkMessage(strMsg);
      mLog->setHeader(mWM->getGameTime());

      makeDecision();
    }
    catch(SLangException e)
    {
      mLog->log(38, "(Player) caught S-Lang exception");
      mLog->log(38, "%s", e.str());
    }
    catch(...)
    {
      mLog->log(38, "(Player) caught unknown exception");
    }

    mLog->setHeader("");
    mQueue->sendCommands();
    mLog->restartTimer();
  }

  sayGoodbye();
}
