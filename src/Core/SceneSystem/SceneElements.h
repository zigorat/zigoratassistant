/*
*********************************************************************************
*          SceneElements.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class declerations Element and Scene.           *
*            These classes manage the data transit between scene specific       *
*            plugins and the scene system.                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file SceneElements.h
<pre>
<b>File:</b>          SceneElements.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class declerations for Element and Scene.
               These classes manage the data transit between scene specific
               plugins and the scene system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/06/2010       Mahdi           Initial version created
</pre>
*/


#ifndef SCENE_ELEMENTS
#define SCENE_ELEMENTS

#include <SceneGraphSystem/Graph.h>       // needed for RSG2::Graph
#include <GraphicSystem/GraphicSystem.h>


namespace Klaus
{


namespace Scene
{



/*******************************************************************************/
/****************************  Class SceneGraphElement  ************************/
/*******************************************************************************/

/*! This class resembles a type of attribute a Element can have which
    is a scene graph. Using this class, a plugin can create scene graph
    instances within the core systems of application. */
class _DLLExportControl SceneGraphElement
{
  private:
    char *        mSceneName;      /*!< Scene name      */
    RSG2::Graph * mSceneGraph;     /*!< Scene tree      */

  public:
                  SceneGraphElement(                    );
                ~ SceneGraphElement(                    );

    const char  * getSceneName     (                    ) const;
    RSG2::Graph * getSceneGraph    (                    );
    bool          setSceneName     ( const char * scene,
                                     Ogre::SceneNode * n);
};




/*******************************************************************************/
/*********************************  Class Element  *****************************/
/*******************************************************************************/

const unsigned NI_OBJECT = 0x0001;  /*!< Indicates graphical information   */
const unsigned NI_NODE   = 0x0002;  /*!< Indicates physical information    */
const unsigned NI_DATA   = 0x0004;  /*!< Indicates data information        */
const unsigned NI_GRAPH  = 0x0008;  /*!< Indicates scene graph information */
const unsigned NI_CHILD  = 0x0010;  /*!< Indicates child updates           */



/*! Element is a class which holds information of an object in simulation. If the
    object has graphical information then the mGraphics will be filled with it's
    attributes. If it has physical information the mTransform will hold them and in
    case of other types of attributes the mSimulation will take care of it.
*/
class _DLLExportControl Element
{
  public:
    /*! Class list holds a dynamic array of Elements. STL containers are not
        trusted completely when used in DLLs in windows. Because some of them
        use static data members which (I do not know why) breaks execution of
        applications with a segmentation violation. */
    class _DLLExportControl List
    {
     private:
        /*! PElement is the difinition for Element */
        typedef Element* PElement;

               PElement *mNodes;     /*!< List of nodes               */
               unsigned  mSize;      /*!< Size of the nodes           */

	    public:
                         List        (                                );
		                   ~ List        (                                );

		           unsigned  size        (                                ) const;
		           void      insert      ( Element * node, unsigned index );
		           void      erase       ( Element* node                  );
		           void      erase       ( unsigned index                 );
		           void      push_back   ( Element * node                 );
		           void      clear       (                                );

		           Element * operator [] ( const unsigned & index         );
		    const  Element * operator [] ( const unsigned & index         ) const;
    };

    List                   mChildren;               /*!< Childrens of the node */

                           Element                  ( Element * parent = NULL   );
                         ~ Element                  (                           );

    Ogre::SceneNode      * getSceneNodeForEditing   (                           );
    VarList              * getDataForEditing        (                           );
    SceneGraphElement    * setSceneGraph            ( const char * scene        );

    Ogre::SceneNode      * getSceneNodeToView       ( bool bClearUpdated = true );
    VarList              * getDataToView            ( bool bClearUpdated = true );
    SceneGraphElement    * getSceneGraphToView      ( bool bClearUpdated = true );

    Element              * createChildNode          (                           );
    void                   removeChildNode          ( Element * node            );

    unsigned               getWhatIsOutOfDate       (                           );

  private:
    Element              * mParent;                 /*!< Parent instance       */

    // Information
    Ogre::SceneNode      * mNode;                   /*!< Ogre scene node       */
    VarList              * mData;                   /*!< Simulation info       */
    SceneGraphElement    * mSceneGraph;             /*!< Scene graph info      */
    unsigned               mOutOfDates;             /*!< Update indicator flag */

    inline void            editedData               ( unsigned data            );
    inline void            updatedData              ( unsigned data            );

    void                   removeEntities           (                          );
    void                   removeChildren           (                          );

    friend class Scene;
};



/************************************************************************************/
/*********************************  Class Scene  ************************************/
/************************************************************************************/

/*! Scene represents a single cycle information of current simulation which
    contains the current cycle information for rendering and training. All
    objects are of type Element. This type records all type of scene elements.
*/
class _DLLExportControl Scene
{
  public:
    int       mFrameNumber;   /*!< Frame number                  */
    Element * mRootElement;   /*!< All the objects in this cycle */

              Scene           (                                  );
            ~ Scene           (                                  );

    void      clear           (                                  );
};


}; // end namespace Scene


}; // end namespace Klaus

#endif // SCENE_ELEMENTS
