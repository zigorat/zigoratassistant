/*
*********************************************************************************
*         SceneElements.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 11/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class declerations Element and Scene.           *
*            These classes manage the data transit between scene specific       *
*            plugins and the scene system.                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file SceneElements.cpp
<pre>
<b>File:</b>          SceneElements.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class declerations for Element and Scene.
               These classes manage the data transit between scene specific
               plugins and the scene system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/06/2010       Mahdi           Initial version created
</pre>
*/


#include "SceneElements.h"
#include <GraphicSystem/GraphicSystem.h>
#include <iostream>
#include <cstring>

using namespace std;


namespace Klaus
{


namespace Scene
{


/*******************************************************************************/
/****************************  Class SceneGraphElement  ************************/
/*******************************************************************************/

SceneGraphElement::SceneGraphElement()
{
  mSceneName = NULL;
  mSceneGraph = NULL;
}

SceneGraphElement::~SceneGraphElement()
{
  if(mSceneGraph)
  {
    delete mSceneGraph;
    mSceneGraph = NULL;
  }

  if(mSceneName)
    delete [] mSceneName;
}

const char * SceneGraphElement::getSceneName() const
{
  return mSceneName;
}

RSG2::Graph * SceneGraphElement::getSceneGraph()
{
  return mSceneGraph;
}

bool SceneGraphElement::setSceneName(const char * scene, Ogre::SceneNode* n)
{
  if(mSceneName && scene && strcmp(mSceneName, scene) == 0)
    return true;

  delete [] mSceneName;

  mSceneName = new char [strlen(scene) + 1];
  mSceneGraph = new RSG2::Graph();
  if(!mSceneGraph->loadFromFile(scene))
  {
    delete mSceneGraph;
    mSceneGraph = NULL;
    return false;
  }

  strcpy(mSceneName, scene);
  mSceneGraph->setOgreParent(n);
  return true;
}




/*******************************************************************************/
/*********************************  Class Element  *****************************/
/*******************************************************************************/

Element::List::List()
{
  mNodes = NULL;
  mSize = 0;
}

Element::List::~List()
{
  clear();
}

unsigned Element::List::size() const
{
  return mSize;
}

void Element::List::insert(Element * node, unsigned index)
{
  PElement *temp = new PElement[mSize];
  for(unsigned i = 0; i < mSize; i++)
    temp[i] = mNodes[i];

  if(mNodes)
    delete mNodes;

  mNodes = new PElement[mSize + 1];

  mSize++;
  for(unsigned i = 0; i < index; i++)
    mNodes[i] = temp[i];

  mNodes[index] = node;
  for(unsigned i = index+1; i < mSize; i++)
   mNodes[i] = temp[i-1];
}

void Element::List::erase(Element* node)
{
  for(unsigned i = 0; i < mSize; i++)
    if(mNodes[i] == node)
    {
      erase(i);
      return;
    }
}

void Element::List::erase(unsigned index)
{
  if(index >= mSize)
  {
    cerr << "(Element::Element::List::erase) index out of bounds: " << index << '(' << mSize << ')' << endl;
    return;
  }

  for(unsigned i = index; i < mSize - 1; i++)
    mNodes[i] = mNodes[i+1];

  mSize--;
}

void Element::List::push_back(Element * node)
{
  insert(node, mSize);
}

void Element::List::clear()
{
  for(unsigned i = 0; i < mSize; i++)
    delete mNodes[i];

  mNodes = NULL;
  mSize = 0;
}

Element * Element::List::operator [] (const unsigned & index)
{
  if(index >= mSize)
  {
    cerr << "(Element::Element::List::operator[]) Index out of range: " << index << '(' << mSize << ')' << endl;
    return NULL;
  }

  return mNodes[index];
}

const Element * Element::List::operator [] (const unsigned & index) const
{
  if(index >= mSize)
  {
    cerr << "(Element::Element::List::operator[]) Index out of range: " << index << '(' << mSize << ')' << endl;
    return NULL;
  }

  return mNodes[index];
}


Element::Element(Element * parent)
{
  mParent     = parent;
  mOutOfDates = 0;
  mData       = NULL;
  mSceneGraph = NULL;

  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();

  mNode = mgr->createSceneNode();
  if(mParent)
    mParent->mNode->addChild(mNode);
  else
    mgr->getRootSceneNode()->addChild(mNode);
}

Element::~Element()
{
  // shall not remove object here
  // whoever created it, shall remove it

  // remove the children first
  removeChildren();

  // remove scene graph
  if(mSceneGraph)
    delete mSceneGraph;

  // remove all entitys first
  removeEntities();

  // remove the node
  if(mNode)
  {
    Ogre::SceneNode* par = dynamic_cast<Ogre::SceneNode*>(mNode->getParent());
    if(par)
      par->removeAndDestroyChild(mNode->getName());
    else
      cerr << "could not acquire parent for node " << mNode->getName() << endl;
  }

  // remove the data
  if(mData)
    delete mData;
}

Ogre::SceneNode * Element::getSceneNodeForEditing()
{
  editedData(NI_NODE);
  return mNode;
}

VarList * Element::getDataForEditing()
{
  if(!mData)
    mData = new VarList;

  editedData(NI_DATA);
  return mData;
}

/**
  This method creates a scene graph element for the SceneElement.
  Allocates a graph, loads it and in case of successfull load, the
  graph will be attached to the ogre node.

  @param scene Name of the scene graph to load
  @return The newly created scene graph element.
*/
SceneGraphElement * Element::setSceneGraph( const char * scene )
{
  // create the scene graph
  if(!mSceneGraph)
    mSceneGraph = new SceneGraphElement;

  // load it
  if(!mSceneGraph->setSceneName(scene, mNode))
    return NULL;

  // initiate and update graphical representation.
  mSceneGraph->getSceneGraph()->updateGraphics();

  // finished.
  editedData(NI_GRAPH);
  return mSceneGraph;
}

Ogre::SceneNode * Element::getSceneNodeToView(bool bClearUpdated)
{
  if(bClearUpdated)
    updatedData(NI_NODE);

  return mNode;
}

VarList * Element::getDataToView(bool bClearUpdated)
{
  if(bClearUpdated)
    updatedData(NI_DATA);

  return mData;
}

SceneGraphElement * Element::getSceneGraphToView(bool bClearUpdated)
{
  if(bClearUpdated)
    updatedData(NI_GRAPH);

  return mSceneGraph;
}

Element * Element::createChildNode()
{
  Element * node = new Element(this);
  mChildren.push_back(node);
  return node;
}

void Element::removeChildNode( Element * node )
{
  mChildren.erase(node);
  delete node;
}

unsigned Element::getWhatIsOutOfDate()
{
  return mOutOfDates;
}

void Element::editedData(unsigned data_name)
{
  mOutOfDates |= data_name;

  if(mParent)
    mParent->editedData(NI_CHILD);
}

void Element::updatedData(unsigned data_name)
{
  mOutOfDates &= ~data_name;
}

void Element::removeEntities()
{
  Ogre::MovableObject * obj;
  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();

  while(mNode->numAttachedObjects() != 0)
  {
    obj = mNode->getAttachedObject(0);

    if(dynamic_cast<Ogre::Entity*>(obj))
    {
      mNode->detachObject(obj);
      mgr->destroyEntity(dynamic_cast<Ogre::Entity*>(obj));
    }
    else if(dynamic_cast<Ogre::Light*>(obj))
    {
      mNode->detachObject(obj);
      mgr->destroyLight(dynamic_cast<Ogre::Light*>(obj));
    }
    else if(dynamic_cast<Ogre::Camera*>(obj))
    {
      mNode->detachObject(obj);
      mgr->destroyCamera(dynamic_cast<Ogre::Camera*>(obj));
    }
    else if(dynamic_cast<Ogre::ParticleSystem*>(obj))
    {
      mNode->detachObject(obj);
      mgr->destroyParticleSystem(dynamic_cast<Ogre::ParticleSystem*>(obj));
    }
    else
    {
      // TODO: implement billboards
      throw "(SceneElement) Unknown attached object";
    }
  }
}

void Element::removeChildren()
{
  mChildren.clear();
}




/************************************************************************************/
/*****************************  Class SimulationCycle  ******************************/
/************************************************************************************/

Scene::Scene()
{
  mFrameNumber = -1;
  mRootElement = new Element;
}

Scene::~Scene()
{
  clear();
}

void Scene::clear()
{
  delete mRootElement;
}


}; // end namespace Scene


}; // end namespace Klaus
