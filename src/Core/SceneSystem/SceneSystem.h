/*
*********************************************************************************
*          SceneSystem.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 07/29/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This system handles all non native simulation by means of their    *
*            own provided plugins.                                              *
*                                                                               *
*********************************************************************************
*/

/*! \file SceneSystem.h
<pre>
<b>File:</b>          SceneSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/29/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is placeholder of all match objects
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/29/2008       Mahdi           Initial version created
</pre>
*/

#ifndef SCENE_SYSTEM
#define SCENE_SYSTEM

#include "ScenePlugin.h"               // needed for ScenePlugin
#include "SceneElements.h"             // needed for Scene and Element
#include <CoreSystem/CoreSystem.h>     // needed for Core::Action
#include <libZigorat/Logger.h>         // needed for Logger and Timing
#include <EventSystem/EventSystem.h>   // needed for EventRecipient
#include <ConfigSystem/Configurable.h> // needed for Configurable


namespace Klaus
{

  namespace Scene
  {

    /*! The Class SceneSystem is the interface of all external simulations with ZigoratAssistant.
        It's job is to load plugins which can communicate with the external simulator and provide
        the simulation informations to the internal systems.
    */
    class _DLLExportControl SceneSystem : public Singleton<SceneSystem>,
                                          public Events::EventRecipient,
                                          public Config::Configurable
    {
      private:
        /*! PluginData is an internal storage to handle all plugins in the scene */
        struct PluginData
        {
          ScenePlugin   * mPlugin;  /*!< Scene plugin instance */
          PluginFactory * mFactory; /*!< Plugin's factory instance */
          bool            mEnabled; /*!< Whether plugin is enabled or not */

          typedef std::vector<PluginData> List;
        };

        PluginData::List      mPlugins;         /*!< Scene plugin instance   */
        Logger              * mLog;             /*!< Logger instance         */
        Scene               * mScene;           /*!< Scene instance          */
        StringArray           mPluginsToLoad;   /*!< Plugins to load         */

        // Configuration related stuff
        virtual void         _loadDefaultConfig (                            );
        virtual bool         _readConfig        ( const Config::ConfigType &t);
        virtual void         _writeConfig       ( const Config::ConfigType &t);

                             SceneSystem        (                            );
                           ~ SceneSystem        (                            );

      public:
        static SceneSystem * getSingleton       (                            );
        static void          freeSingleton      (                            );

        // Core calls these
        void                 setupScene         (                            );
        void                 clearScene         (                            );

        void                 updateScene        (                            );
        Scene*               getScene           (                            );

        // Event related stuff
        virtual void         processEvent       ( const Events::Event & ev   );

        // performance boost
        void                 deactivatePlugin   ( const char * strPlugin     );
        void                 activatePlugin     ( const char * strPlugin     );
    };

  }; // end namespace Scene

}; // end namespace Klaus


#endif // SCENE_SYSTEM
