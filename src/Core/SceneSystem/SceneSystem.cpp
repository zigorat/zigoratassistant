/*
*********************************************************************************
*          SceneSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/29/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This system handles all non native simulation by means of their    *
*            own provided plugins.                                              *
*                                                                               *
*********************************************************************************
*/

/*! \file SceneSystem.cpp
<pre>
<b>File:</b>          SceneSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/29/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is placeholder of all match objects
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/29/2008       Mahdi           Initial version created
</pre>
*/

#include "SceneSystem.h"
#include <PluginSystem/PluginSystem.h>    // needed for PluginSystem


/** SceneSystem singleton instance */
template<> Klaus::Scene::SceneSystem* Klaus::Singleton<Klaus::Scene::SceneSystem>::mInstance = 0;

/**
  This is the constructor of the SceneSystem. It intializes all the objects to
  their default values.
*/
Klaus::Scene::SceneSystem::SceneSystem()
{
  mLog   = Logger::getSingleton();
  mScene = NULL;

  setupConfig("SceneSystem");
  setRecipientName("SceneSystem");
}

/**
  This is the destructor of the WolrdModel. Any cleanup needed before deleting
  SceneSystem will go in here.
*/
Klaus::Scene::SceneSystem::~SceneSystem()
{
  clearScene();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Scene::SceneSystem * Klaus::Scene::SceneSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new SceneSystem;
    Logger::getSingleton()->log(1, "(SceneSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Scene::SceneSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(SceneSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(SceneSystem) removed singleton");
  }
}

/**
  This method sets up default congfiguration for this system
*/
void Klaus::Scene::SceneSystem::_loadDefaultConfig()
{
  mPluginsToLoad.clear();
  mPluginsToLoad.push_back("Sample");
}

/**
  This method loads plugin names to run from a binary stream and initializes them afterwards
  @param t The configuration type of the steam
  @return True on successfull read of configurations, false otherwise
*/
bool Klaus::Scene::SceneSystem::_readConfig(const Config::ConfigType & t)
{
  try
  {
    clearScene();
    mPluginsToLoad.clear();

    if(t == Config::BinaryConfig)
    {
      unsigned count = readUnsigned();
      std::string str;
      for(unsigned i = 0; i < count; i++)
      {
        str = readString();
        mPluginsToLoad.push_back(str);
      }
    }
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      SToken t = expr["loadPlugins"];
      for(size_t i = 1; i < t.size(); ++i)
        mPluginsToLoad.push_back(t[i]);
    }
    else
    {
      mLog->log(20, "(SceneSystem) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    Logger::getSingleton()->log(20, "(SceneSystem) exception: %s", str);
    return false;
  }

  // NOTE: will not setup the scene
  // this shall be done after loading has completed
  return true;
}

/**
  This method writes the configuration of scene system to the given output stream.
  @param t The configuration type to save the stream
*/
void Klaus::Scene::SceneSystem::_writeConfig(const Config::ConfigType & t)
{
  if(t == Config::BinaryConfig)
  {
    writeUnsigned(mPluginsToLoad.size());
    for(unsigned i = 0; i < mPluginsToLoad.size(); i++)
      writeString(mPluginsToLoad[i]);
  }
  else if(t == Config::SExpConfig)
  {
    std::stringstream ss;
    for(size_t i = 0; i < mPluginsToLoad.size(); ++i)
      ss << " " << mPluginsToLoad[i];
    writeText("(loadPlugins%s)", ss.str().c_str());
  }
  else
    mLog->log(20, "(SceneSystem) write: configuration type %d not supported", t);
}

/**
  This method removes all scene plugins and then will try to load
  the newly set plugins specified in the configuration.
*/
void Klaus::Scene::SceneSystem::setupScene()
{
  clearScene();

  mScene = new Scene;

  for(size_t i = 0; i < mPluginsToLoad.size(); ++i)
  {
    PluginFactory* factory = PluginSystem::getSingleton()->findPlugin(mPluginsToLoad[i], "ScenePlugin");
    if(!factory)
      continue;

    Plugin* raw_plugin = factory->createPluginInstance();
    if(!raw_plugin)
      continue;

    ScenePlugin* plugin = dynamic_cast<ScenePlugin*>(raw_plugin);
    if(!plugin)
    {
      factory->freePluginInstance(raw_plugin);
      continue;
    }

    if(!plugin->initialize())
    {
      factory->freePluginInstance(plugin);
      continue;
    }

    // add the plugin into list of plugins
    PluginData data;
    data.mEnabled = true;
    data.mFactory = factory;
    data.mPlugin = plugin;
    mPlugins.push_back(data);

    Logger::getSingleton()->log(20, "(SceneSystem) loaded plugin %s", mPluginsToLoad[i].c_str());
  }
}

/**
  This method removes all scene plugins
*/
void Klaus::Scene::SceneSystem::clearScene()
{
  // remove all plugins
  for(size_t i = 0; i < mPlugins.size(); i++)
  {
    Logger::getSingleton()->log(20, "(SceneSystem) unloaded plugin %s", mPlugins[i].mFactory->getName());
    mPlugins[i].mFactory->freePluginInstance(mPlugins[i].mPlugin);
  }
  mPlugins.clear();

  // then remove the scene
  if(mScene)
  {
    delete mScene;
    mScene = NULL;
  }
}

/**
  This method informs all the scene plugins to update the scene if necessary
*/
void Klaus::Scene::SceneSystem::updateScene()
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    if(mPlugins[i].mEnabled)
      mPlugins[i].mPlugin->updateScene();
}

/**
  This method returns the scene instance for the current frame
  @return The scene instance of the current frame
*/
Klaus::Scene::Scene* Klaus::Scene::SceneSystem::getScene()
{
  return mScene;
}

/**
  This method will process the events sent to SceneSystem
  @param ev The event to process
*/
void Klaus::Scene::SceneSystem::processEvent ( const Events::Event & ev )
{
  mLog->log(20, "(SceneSystem) unknown event received: %s",
                Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}

/**
  This method deactivates the plugin. When a plugin is disabled it will
  not be called to update the scene between frames.
  @param strPlugin The plugin to deactivate and remove from auto updated plugins
*/
void Klaus::Scene::SceneSystem::deactivatePlugin(const char * strPlugin)
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    if(strcmp(mPlugins[i].mFactory->getName(), strPlugin) == 0)
    {
      mPlugins[i].mEnabled = false;
      mLog->log(20, "(SceneSystem) disabled plugin '%s'", strPlugin);
      return;
    }
}

/**
  This method activates a disabled plugin. When activated, the plugin will
  be requested to update the scene on every frame. The whole activation and
  deactivation are usually decided with events.
  @param strPlugin The plugin to activate
*/
void Klaus::Scene::SceneSystem::activatePlugin(const char * strPlugin)
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    if(strcmp(mPlugins[i].mFactory->getName(), strPlugin) == 0)
    {
      mPlugins[i].mEnabled = true;
      mLog->log(20, "(SceneSystem) enabled plugin '%s'", strPlugin);
      return;
    }
}
