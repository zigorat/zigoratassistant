/*
*********************************************************************************
*           ScenePlugin.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 09/07/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the module which handles playback on current available     *
*            plugins for playback, so any kind of time-based simulations can    *
*            be played through this application.                                *
*                                                                               *
*********************************************************************************
*/

/*! \file ScenePlugin.h
<pre>
<b>File:</b>          ScenePlugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       09/07/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the module which handles playback on current available
*            plugins for playback, so any kind of time-based simulations can
*            be played through this application.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
09/07/2008       Mahdi           Initial version created
</pre>
*/

#ifndef SIMULATION_PLUGIN_BASE
#define SIMULATION_PLUGIN_BASE

#include <PluginSystem/PluginBase.h>     // needed for Plugin
#include <GraphicSystem/GraphicSystem.h> // needed to get SceneManager

namespace Klaus
{

  namespace Scene
  {

    /*! This class is specialized plugin base class for simulations */
    class _DLLExport ScenePlugin : public Plugin
    {
      protected:
        Ogre::SceneManager * mSceneMgr; /*!< Ogre scene manager instance */
        Klaus::Logger      * mLog;      /*!< Logger instance */

      public:
        /**
          Constructor of ScenePlugin. Initializes the scene manager.
        */
        ScenePlugin()
        {
          mSceneMgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
          mLog = Logger::getSingleton();
        }

        /**
          This method is responsible to initializes the scene plugin.
          @return Boolean value indicating that initialization was successful or not.
                  If it returns false, then the plugin will be eliminated from the scene.
        */
        virtual bool         initialize (          ) = 0;

        /**
          This method is called between frames to update the scene.
          Each scene plugin creates its own update method.
        */
        virtual void         updateScene(          ) = 0;
    };

  }; // end namespace Scene

}; // end namespace Klaus


#endif // SIMULATION_PLUGIN_BASE
