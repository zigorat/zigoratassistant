/*
*********************************************************************************
*          ConfigSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 07/24/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Headquarters for all configurations.                               *
*                                                                               *
*********************************************************************************
*/

/*! \file ConfigSystem.cpp
<pre>
<b>File:</b>          ConfigSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/24/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Headquarters for all configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/24/2010       Mahdi           Initial version created
</pre>
*/

#include "ConfigSystem.h"
#include <libZigorat/BinaryStream.h>  // needed for BinaryStream
#include <typeinfo>                   // needed for typeinfo


using namespace std;


/** ConfigSystem singleton instance */
template<> Klaus::Config::ConfigSystem* Klaus::Singleton<Klaus::Config::ConfigSystem>::mInstance = 0;


/**
  Constructor of ConfigSystem.
*/
Klaus::Config::ConfigSystem::ConfigSystem()
{
  mLog = Logger::getSingleton();
  mStream = NULL;
}

/**
  Destructor of ConfigSystem.
*/
Klaus::Config::ConfigSystem::~ConfigSystem()
{
  clear();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Config::ConfigSystem * Klaus::Config::ConfigSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new ConfigSystem;
    Logger::getSingleton()->log(1, "(ConfigSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Config::ConfigSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(ConfigSystem ) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(ConfigSystem ) removed singleton");
  }
}

/**
  This method clears the system. removes all positions and closes the stream
*/
void Klaus::Config::ConfigSystem::clear()
{
  // clear out positions
  mSysPositions.clear();

  // remove the stream also
  if(mStream)
  {
    mStream->flush();
    delete mStream;
    mStream = NULL;
  }
}

/**
  This method searches all the registered system for one with the given name.
  @param strSys Name of the system to get it's instance
  @return Instance of the requested system, NULL if not found
*/
Klaus::Config::Configurable* Klaus::Config::ConfigSystem::getSystemByName(const std::string& strSys)
{
  for(size_t i = 0; i < mSystemNames.size(); i++)
    if(mSystemNames[i] == strSys)
      return mSystems[i];

  return NULL;
}

/**
  This method loads configuration from the given binary file. Extracts all
  configurations and gives them their respected systems to load from.
  @param strFile File name to load configuration from
  @return True if the file was loaded, false otherwise
*/
bool Klaus::Config::ConfigSystem::loadBinaryConfig(const std::string& strFile)
{
  mStream = new fstream(strFile.c_str(), std::ios::in | std::ios::out | std::ios::binary);

  if(!(*mStream))
  {
    mLog->log(22, "(ConfigSystem) could not open binary configuration file %s", strFile.c_str());
    clear();
    return false;
  }

  try
  {
    mStream->exceptions(std::ios_base::failbit);

    while(!mStream->eof() && mStream->peek() != EOF)
    {
      // get the system information
      string strSys = BinaryStream::readString(*mStream);
      unsigned length = BinaryStream::readUnsigned(*mStream);

      // save it for future usage
      mSysPositions[strSys] = mStream->tellg();

      // skip the main configuration for now
      mStream->seekg(length, ios::cur);
    }
  }
  catch(const char * strError )
  {
    mLog->log(22, "(ConfigSystem) binary configuration: exception: %s", strError);
    return false;
  }

  // finished. shall not close the configuration file
  mLog->log(22, "(ConfigSystem) loaded binary configuration from %s", strFile.c_str());
  return true;
}

/**
  This method saves the registered systems configuration to the given filename in a binary format
  @param strFile Name of the file to save
  @return True if configuration was saved to the given file, false otherwise
*/
bool Klaus::Config::ConfigSystem::saveBinaryConfig(const std::string& strFile)
{
  mStream = new fstream(strFile.c_str(), ios::in | ios::out | ios::binary | ios::trunc);
  if(((fstream*)(mStream))->is_open() == false)
  {
    mLog->log(22, "(ConfigSystem) could not open binary configuration file to write: %s", strFile.c_str());
    return false;
  }

  ofstream::pos_type pos_len, pos_start, pos_end;

  for(size_t i = 0; i < mSystemNames.size(); i++)
  {
    BinaryStream::writeString(*mStream, mSystemNames[i]);

    // save this position to write the size later
    pos_len = mStream->tellp();
    BinaryStream::writeUnsigned(*mStream, 0);
    pos_start = mStream->tellp();

    // update the configuration position for the system
    mSysPositions[mSystemNames[i]] = mStream->tellp();

    // write the configuration
    mSystems[i]->_setStream(mStream);
    mSystems[i]->_writeConfig(BinaryConfig);

    // now fill the size with the proper value
    pos_end = mStream->tellp();
    mStream->seekp(pos_len);
    BinaryStream::writeUnsigned(*mStream, pos_end - pos_start);

    // move to the end of file
    mStream->seekp(0, ios::end);
  }

  return true;
}

/**
  This method loads configuration from the given text file. Extracts all
  configurations and gives them their respected systems to load from.
  @param strFile File name to load configuration from
  @return True if the file was loaded, false otherwise
*/
bool Klaus::Config::ConfigSystem::loadSExpConfig(const std::string& strFile)
{
  try
  {
    // 16 Kilobytes seems fairly enough for a regular configuration file
    SExpression expr;
    expr.parseFile(strFile, 16 * 1024);
    int index = -1;

    // skip the comments
    for(size_t i = 0; i < expr.size(); ++i)
      if(expr[i].getType() == SExpression::Normal)
      {
        index = i;
        break;
      }

    // check the found index
    if(index == -1)
    {
      mLog->log(22, "(ConfigSystem) no configuration found in file '%s' !", strFile.c_str());
      return false;
    }

    for(size_t i = 0; i < expr[index].size(); ++i)
      mSysPositions[expr[index][i][0].str()] = expr[index][i].getStartingIndex() - 1;
  }
  catch(const char * strError )
  {
    mLog->log(22, "(ConfigSystem) s-expression configuration: exception: %s", strError);
    return false;
  }
  catch(SLangException e)
  {
    mLog->log(22, "(ConfigSystem) s-expression configuration: exception:");
    mLog->log(22, "%s", e.str());
    return false;
  }

  // now open the file
  mStream = new fstream(strFile.c_str(), ios::in | ios::out);
  if(!(*mStream))
  {
    mLog->log(22, "(ConfigSystem) could not open s-expression configuration file %s", strFile.c_str());
    clear();
    return false;
  }
  mStream->exceptions(std::ios_base::failbit);

  // finished. shall not close the configuration file
  mLog->log(22, "(ConfigSystem) loaded s-expression configuration from %s", strFile.c_str());
  return true;
}

/**
  This method saves the registered systems configuration to the given filename in a text format
  @param strFile Name of the file to save
  @return True if configuration was saved to the given file, false otherwise
*/
bool Klaus::Config::ConfigSystem::saveSExpConfig(const std::string& strFile)
{
  mStream = new fstream(strFile.c_str(), ios::in | ios::out | ios::trunc);
  if(((fstream*)(mStream))->is_open() == false)
  {
    mLog->log(22, "(ConfigSystem) could not open text configuration file to write: %s", strFile.c_str());
    return false;
  }

  // write file header
  (*mStream) << "\n;\n; ZigoratAssistant configurations\n;\n\n(\n";

  for(size_t i = 0; i < mSystemNames.size(); i++)
  {
    // update the configuration position for the system
    mSysPositions[mSystemNames[i]] = mStream->tellp();

    // write system section header
    (*mStream) << "  (\n    " << mSystemNames[i].c_str() << "\n";

    // write system configuration
    mSystems[i]->_setStream(mStream);
    mSystems[i]->_writeConfig(SExpConfig);

    // write system section footer
    (*mStream) << "  )\n";
    if(i < mSystemNames.size() - 1)
      (*mStream) << "\n";
  }

  // write file footer
  (*mStream) << ")\n";

  return true;
}

/**
  This method registers a given system so that the system will be a
  part of loading and saving configurations. After registration the
  system will be loaded from it's previous information stored on the
  stream.
  @param sys The system to register for configuring
  @return if the system was already registered false will be returned. Otherwise true will be returned.
*/
bool Klaus::Config::ConfigSystem::registerSystem(Configurable* sys)
{
  string strName = sys->_getSystemName();

  for(size_t i = 0; i < mSystemNames.size(); i++)
    if(mSystemNames[i] == strName)
    {
      mLog->log(22, "(ConfigSystem) tried to re-register %s", strName.c_str());
      return false;
    }

  mSystems.push_back(sys);
  mSystemNames.push_back(strName);
  injectConfigToSys(strName);

  mLog->log(22, "(ConfigSystem) registered %s", strName.c_str());
  return true;
}

/**
  This method unregisters the given system from configuration procedures.
  @param sys The system to remove from configuration
  @return True if the system found and removed, false otherwise
*/
bool Klaus::Config::ConfigSystem::unregisterSystem(Configurable* sys)
{
  for(size_t i = 0; i < mSystems.size(); i++)
    if(mSystems[i] == sys)
    {
      mSystems.erase(mSystems.begin() + i);
      mLog->log(22, "(ConfigSystem) unregistered %s", mSystemNames[i].c_str());
      mSystemNames.erase(mSystemNames.begin() + i);
      return true;
    }

  mLog->log(22, "(ConfigSystem) system not registered and can not be removed: %s", typeid(sys).name());
  return false;
}

/**
  This method acquires configuration for a system and injects it to it's registered system.
  @param strSys The system name to be injected with it's own configuration
*/
void Klaus::Config::ConfigSystem::injectConfigToSys(const std::string & strSys)
{
  if(!mStream)
    return;

  // acquire the system and tell it to read it's configurations
  Configurable * sys = getSystemByName(strSys);
  if(!sys)
  {
    mLog->log(22, "(ConfigSystem) system not registered: %s", strSys.c_str());
    return;
  }

  FilePositions::iterator it = mSysPositions.find(strSys);
  if(it == mSysPositions.end())
  {
    mLog->log(22, "(ConfigSystem) no configuration found for '%s'", strSys.c_str());
    return;
  }

  // move to the exact part of file
  mStream->clear();
  mStream->seekg(it->second, ios::beg);

  bool bSuccess = false;

  try
  {
    sys->_setStream(mStream);
    sys->_readConfig(mConfType);
    bSuccess = true;
  }
  catch(const char * strError)
  {
    mLog->log(22, "(ConfigSystem) exception on loading %s: %s", strSys.c_str(), strError);
  }
  catch(SLangException e)
  {
    mLog->log(22, "(ConfigSystem) exception on loading %s:", strSys.c_str());
    mLog->log(22, "%s", e.str());
  }
  catch(ios_base::failure e)
  {
    cout << e.what() << endl;
    cout.flush();

    // clear the stream fail flag
    mStream->clear();
  }
  catch(...)
  {
    mLog->log(22, "(ConfigSystem) unknown exception thrown while loading %s", strSys.c_str());
  }

  // in case of an exception, load the system's default configuration
  if(!bSuccess)
    sys->_loadDefaultConfig();
}

/**
  This method loads configuration from the given file name. Extracts all
  configurations and gives them their respected systems to load from.
  @param strFile File to load configuration from
  @param type Type of the given configuration file
  @param bBroadcast Whether should broad cast configurations to all systems or just read them
  @return True if the file was loaded, false otherwise
*/
bool Klaus::Config::ConfigSystem::loadConfig(const std::string& strFile, const ConfigType & type, bool bBroadcast)
{
  // close the stream
  if(mStream)
    clear();

  // load based on the given type
  bool bResult = false;
  if(type == BinaryConfig)
    bResult = loadBinaryConfig(strFile);
  else if(type == SExpConfig)
    bResult = loadSExpConfig(strFile);
  else
    mLog->log(22, "(ConfigSystem) load: config type %d not supported", type);

  // The error message is specific to the loader function
  if(!bResult)
    return false;
  else
    mConfType = type;

  // clear the eof bit in mStream::rdbuf. otherwise consequent calls to seekg will fail
  mStream->clear();

  // if shall broad cast, inform all systems of their configurations
  if(bBroadcast)
    for(FilePositions::iterator it = mSysPositions.begin(); it != mSysPositions.end(); it++)
      injectConfigToSys(it->first);

  // finished. shall not close the configuration file
  return true;
}

/**
  This method saves configuration of all registered systems in a file with the given address
  @param strFile File name to write the configuration to
  @param type Type of file. needed to specify method of saving configuration
  @return True on successfull write, false otherwise
*/
bool Klaus::Config::ConfigSystem::saveConfig(const std::string& strFile, const ConfigType & type)
{
  if(mStream)
    clear();

  if(type == BinaryConfig)
    saveBinaryConfig(strFile);
  else if(type == SExpConfig)
    saveSExpConfig(strFile);
  else
  {
    mLog->log(22, "(ConfigSystem) save: config type %d not supported", type);
    return false;
  }

  mLog->log(22, "(ConfigSystem) wrote configuration to %s", strFile.c_str());
  mConfType = type;

  // will not close the file
  mStream->flush();
  return true;
}
