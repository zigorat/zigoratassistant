/*
*********************************************************************************
*           Configurable.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/24/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class for configuration system's affairs. Provides necessary  *
*            configuration manipulation methods.                                *
*                                                                               *
*********************************************************************************
*/

/*! \file Configurable.h
<pre>
<b>File:</b>          Configurable.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/24/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class for configuration system's affairs. Provides necessary
               configuration manipulation methods.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/24/2010       Mahdi           Initial version created
</pre>
*/


#ifndef CONFIGURE_BASE
#define CONFIGURE_BASE


#include <libZigorat/PlatformMacros.h>
#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


#include <libZigorat/SLang.h>  // needed for SExpression


namespace Klaus
{

  namespace Config
  {

    /*! Indicates type of configuration. */
    enum ConfigType
    {
      BinaryConfig, /*!< Binary configuration */
      SExpConfig,   /*!< S-Expression based configuration */
      UnknownConfig  /*!< Unknown configuration type */
    };


    /*! This class is the base class for reading and writing configurations
        from a file. Provides necessary utilities. */
    class _DLLExportControl Configurable
    {
      private:
        bool           mRegistered;      /*!< System is registered?  */
        std::iostream *mStream;          /*!< The stream             */
        std::string    mSystemName;      /*!< Name of the system     */

      protected:
        // Read methods
        bool           readBool          (                           );
        int            readInt           (                           );
        unsigned       readUnsigned      (                           );
        double         readDouble        (                           );
        std::string    readString        (                           );
        bool           readSExpression   ( SExpression & expr        );

        // Write methods
        void           writeBool         ( const bool & var          );
        void           writeInt          ( const int & var           );
        void           writeUnsigned     ( const unsigned & var      );
        void           writeDouble       ( const double & var        );
        void           writeString       ( const std::string & var   );
        void           writeText         ( const char * str, ...     );

        // default configuration
        bool           setupConfig       ( const std::string & str="");
        virtual void   _loadDefaultConfig(                           ) = 0;

        // read and write methods
        virtual bool   _readConfig       ( const ConfigType & type   ) = 0;
        virtual void   _writeConfig      ( const ConfigType & type   ) = 0;


      public:
                       Configurable      (                           );
                     ~ Configurable      (                           );

        void           _setStream        ( std::iostream * stream    );
        const char *   _getSystemName    (                           );

        /*! List is a dynamic array of Configurable classes */
        typedef std::vector<Configurable*> List;

        /*! ConfigSystem is the only class which shall have access to _readConfig and _writeConfig */
        friend class ConfigSystem;
    };


  }; // end namespace Config


}; // end namespace Klaus


#endif // CONFIGURE_BASE
