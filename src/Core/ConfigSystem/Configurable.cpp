/*
*********************************************************************************
*          Configurable.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 07/24/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class for configuration system's affairs. Provides necessary  *
*            configuration manipulation methods.                                *
*                                                                               *
*********************************************************************************
*/

/*! \file Configurable.cpp
<pre>
<b>File:</b>          Configurable.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/24/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class for configuration system's affairs. Provides necessary
               configuration manipulation methods.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/24/2010       Mahdi           Initial version created
</pre>
*/


#include "Configurable.h"
#include "ConfigSystem.h"            // needed for ConfigSystem
#include <libZigorat/BinaryStream.h> // needed for BinaryStream
#include <typeinfo>                  // needed for typeid
#include <stdio.h>                   // needed for vsprintf
#include <stdarg.h>                  // needed for va_list

/**
  Constructor of Configurable. Sets the class is not registered.
*/
Klaus::Config::Configurable::Configurable()
{
  mRegistered = false;
  mStream = NULL;
}

/**
  Destructor of Configurable. Removes the class from ConfigSystem if registered
*/
Klaus::Config::Configurable::~Configurable()
{
  if(mRegistered)
    ConfigSystem::getSingleton()->unregisterSystem(this);
}

/**
  This method reads a boolean value from the binary stream
  @return The read boolean value
*/
bool Klaus::Config::Configurable::readBool()
{
  return BinaryStream::readBool(*mStream);
}

/**
  This method reads an integer value from the binary stream
  @return The read integer value
*/
int Klaus::Config::Configurable::readInt()
{
  return BinaryStream::readInt(*mStream);
}

/**
  This method reads an unsigned value from the binary stream
  @return The read unsigned value
*/
unsigned Klaus::Config::Configurable::readUnsigned()
{
  return BinaryStream::readUnsigned(*mStream);
}

/**
  This method reads a double value from the binary stream
  @return The read double value
*/
double Klaus::Config::Configurable::readDouble()
{
  return BinaryStream::readDouble(*mStream);
}

/**
  This method reads a string value from the binary stream
  @return The read string value
*/
std::string Klaus::Config::Configurable::readString()
{
  return BinaryStream::readString(*mStream);
}

/**
  This method extracts a S-Expression string from the configurable's own
  stream and breaks it into it's constructing tokens.
  @param expr The extracted tokens from the read S-Expression
  @return True on successfull read, false otherwise
*/
bool Klaus::Config::Configurable::readSExpression(SExpression & expr)
{
  try
  {
    // 8 KiloBytes is a logical size for a system's text configuration
    expr.parse(*mStream, 8192);
  }
  catch(SLangException e)
  {
    Logger::getSingleton()->log(22, "(Configurable) error reading s-expression chunk from the config stream:");
    Logger::getSingleton()->log(22, "%s", e.str());
    return false;
  }

  return true;
}

/**
  This method writes a boolean to the binary stream
  @param var The boolean to write in binary form the given output stream
*/
void Klaus::Config::Configurable::writeBool(const bool& var)
{
  return BinaryStream::writeBool(*mStream, var);
}

/**
  This method writes an integer to the binary stream
  @param var The integer to write in binary form the given output stream
*/
void Klaus::Config::Configurable::writeInt(const int& var)
{
  return BinaryStream::writeInt(*mStream, var);
}

/**
  This method writes an unsigned to the binary stream
  @param var The unsigned to write in binary form the given output stream
*/
void Klaus::Config::Configurable::writeUnsigned(const unsigned & var)
{
  return BinaryStream::writeUnsigned(*mStream, var);
}

/**
  This method writes a double to the binary stream
  @param var The double to write in binary form the given output stream
*/
void Klaus::Config::Configurable::writeDouble(const double& var)
{
  return BinaryStream::writeDouble(*mStream, var);
}

/**
  This method writes a string to the binary stream
  @param var The string to write in binary form the given output stream
*/
void Klaus::Config::Configurable::writeString(const std::string& var)
{
  return BinaryStream::writeString(*mStream, var);
}

/**
  This method writes a given string format into the stream.
  @param str The string format
  @param ... Needed parameters to fill in the string
*/
void Klaus::Config::Configurable::writeText(const char* str, ... )
{
  // get the string from the given format
  char buf[1024];
  va_list ap;
  va_start(ap, str);
  if(vsnprintf(buf, 1023, str, ap) == -1)
    std::cerr << "(Configurable) buffer is too small, can not format string completely" << std::endl;
  va_end(ap);

  // write it
  *mStream << "    " << buf << std::endl;
}

/**
  This method sets up configuration automation by registering this system
  in ConfigSystem.
  @return True if the system registered within ConfigSystem, false otherwise
*/
bool Klaus::Config::Configurable::setupConfig( const std::string & strName )
{
  // first load default data
  mSystemName = strName;
  _loadDefaultConfig();

  if(!ConfigSystem::getSingleton()->registerSystem(this))
  {
    Logger::getSingleton()->log(22, "(Configurable) could not register %s", strName.c_str());
    return false;
  }

  mRegistered = true;
  return true;
}

/**
  Internal usage only. Sets the stream to the given one
  @param stream The stream to use for reading and writing
*/
void Klaus::Config::Configurable::_setStream(std::iostream* stream)
{
  mStream = stream;
}

/**
  This method returns the name of system. This is the name which
  configurations were saved under. If the system was setup with no names
  then it will assume that the system is not mutable. So it will assign a
  name itself.
  @return Name of the system
*/
const char * Klaus::Config::Configurable::_getSystemName()
{
  if(mSystemName.empty())
    mSystemName = typeid(*this).name();

  return mSystemName.c_str();
}
