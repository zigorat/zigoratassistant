/*
*********************************************************************************
*           ConfigSystem.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/24/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Headquarters for all configurations.                               *
*                                                                               *
*********************************************************************************
*/

/*! \file ConfigSystem.h
<pre>
<b>File:</b>          ConfigSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/24/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Headquarters for all configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/24/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CONFIG_SYSTEM
#define CONFIG_SYSTEM


#include "Configurable.h"           // needed for Configurable
#include <libZigorat/Logger.h>      // needed for Logger
#include <libZigorat/Parse.h>       // needed for StringArray
#include <map>                      // needed for std::map



namespace Klaus
{

  namespace Config
  {

    /*! This class is the center for all configurations. Loads and saves configurations
        to files. Registers configurable systems and takes care of informing them of
        loading new configurations at run time.
    */
    class _DLLExportControl ConfigSystem : public Singleton<ConfigSystem>
    {
      private:

        /*! FilePositions is a mapping between positions in file and their system names */
        typedef std::map<std::string, long> FilePositions;

        Logger              * mLog;            /*!< Logger instance          */
        Configurable::List    mSystems;        /*!< Registered systems       */
        StringArray           mSystemNames;    /*!< Registered system names  */
        FilePositions         mSysPositions;   /*!< Positions for systems    */
        std::iostream       * mStream;         /*!< Configuration stream     */
        ConfigType            mConfType;       /*!< Type of configuration    */

        void                  clear            (                             );
        Configurable        * getSystemByName  ( const std::string & strSys  );

        bool                  loadBinaryConfig ( const std::string & strFile );
        bool                  saveBinaryConfig ( const std::string & strFile );

        bool                  loadSExpConfig   ( const std::string & strFile );
        bool                  saveSExpConfig   ( const std::string & strFile );

                              ConfigSystem     (                             );
                            ~ ConfigSystem     (                             );

      public:
        static ConfigSystem * getSingleton     (                             );
        static void           freeSingleton    (                             );

        bool                  registerSystem   ( Configurable * sys          );
        bool                  unregisterSystem ( Configurable * sys          );

        void                  injectConfigToSys( const std::string & strSys  );

        bool                  loadConfig       ( const std::string & strFile,
                                                 const ConfigType & type,
                                                 bool bBroadcast = true      );
        bool                  saveConfig       ( const std::string & strFile,
                                                 const ConfigType & type     );
    };

  }; // end namespace Config

}; // end namespace Klaus


#endif // CONFIG_SYSTEM
