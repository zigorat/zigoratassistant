/*
*********************************************************************************
*         GraphicConfig.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 08/09/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicConfig.cpp
<pre>
<b>File:</b>          GraphicConfig.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/09/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/09/2010       Mahdi           Initial version created
</pre>
*/

#include "GraphicConfig.h"
#include "OpenGLRenderer.h"                 // needed for OpenGLRenderer
#include "DirectX9Renderer.h"               // needed for DirectX9Renderer
#include <libZigorat/ApplicationSettings.h> // needed for ApplicationSettings


/** GraphicConfig singleton instance */
template<> Klaus::Graphics::GraphicConfig*
Klaus::Singleton<Klaus::Graphics::GraphicConfig>::mInstance = 0;



/**
  This is the constructor of GraphicConfig.
*/
Klaus::Graphics::GraphicConfig::GraphicConfig()
{
  mLog = Logger::getSingleton();
  mActiveRenderer = NULL;
}

/**
  This is the destructor of GraphicConfig. Removes all renderers.
*/
Klaus::Graphics::GraphicConfig::~GraphicConfig()
{
  clearRenderers();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Graphics::GraphicConfig * Klaus::Graphics::GraphicConfig::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new GraphicConfig;
    Logger::getSingleton()->log(1, "(GraphicConfig) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Graphics::GraphicConfig::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(GraphicConfig) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(GraphicConfig) removed singleton");
  }
}

/**
  This method clears all installed renderers
*/
void Klaus::Graphics::GraphicConfig::clearRenderers()
{
  // clear renderer list
  mRenderers.clear();

  // free all renderer instance singletons
  OpenGraphics::OpenGLRenderer::freeSingleton();
  DX9::DirectX9Renderer::freeSingleton();
}

/**
  This method installs the given renderer.
  @param renderer The renderer to try to initialize and install
*/
void Klaus::Graphics::GraphicConfig::installRenderer(RendererBase* renderer)
{
  int iErrors = renderer->initialize();
  if(iErrors != 0)
    mLog->log(25, "(GraphicConfig) could not initialize %s renderer. number of errors: %d", renderer->getName().c_str(), iErrors);
  else
  {
    mLog->log(25, "(GraphicConfig) initialized and installed %s renderer", renderer->getName().c_str());
    mRenderers.push_back(renderer);
  }
}

/**
  Returns number of available renderers.
  @return Returns number of renderers available
*/
unsigned Klaus::Graphics::GraphicConfig::getRendererCount() const
{
  return mRenderers.size();
}

/**
  Returns the specified renderer instance.
  @param type Type of the renderer, ie. OpenGL, DirectX9
  @return The renderer instance specified by it's type
*/
Klaus::Graphics::RendererBase * Klaus::Graphics::GraphicConfig::getRenderer(const RendererType & type)
{
  for(size_t i = 0; i < mRenderers.size(); i++)
    if(mRenderers[i]->getType() == type)
      return mRenderers[i];

  return NULL;
}

/**
  Returns the specified renderer instance.
  @param index Index of the renderer
  @return The renderer instance specified by it's index
*/
Klaus::Graphics::RendererBase* Klaus::Graphics::GraphicConfig::getRenderer(const unsigned & index)
{
  if(index < mRenderers.size())
    return mRenderers[index];
  else
    return NULL;
}

/**
  Returns the active renderer instance.
  @return The currently active renderer's instance
*/
Klaus::Graphics::RendererBase* Klaus::Graphics::GraphicConfig::getActiveRenderer()
{
  return mActiveRenderer;
}

/**
  This method activates a given render system
  @param type Type of the renderer, ie. OpenGL, DirectX9
  @return True if activatation succeeds, false otherwise
*/
bool Klaus::Graphics::GraphicConfig::activateRenderSystem(const Klaus::Graphics::RendererType& type)
{
  using namespace Ogre;

  // get renderer configuration to activate
  RendererBase * r = getRenderer(type);
  if(!r)
  {
    mLog->log(25, "(GraphicConfig) renderer not exist: %s", RendererBase::typeToString(type).c_str());
    return false;
  }

  // deactivate previous renderer
  if(mActiveRenderer)
    mActiveRenderer->deactivate();

  // active new renderer
  mActiveRenderer = r;
  mActiveRenderer->activate();

  return true;
}

/**
  Initializes all renderer systems.
  @return True if at least one render system can be used for rendering,
          false if no usable render systems were found
*/
bool Klaus::Graphics::GraphicConfig::initialize()
{
  // install available renderers
  installRenderer(OpenGraphics::OpenGLRenderer::getSingleton());
  installRenderer(DX9::DirectX9Renderer::getSingleton());

  // does not need to initialize if already was initialized
  if(mRenderers.empty())
  {
    mLog->log(25, "(GraphicConfig) FATAL: no renderer module installed");
    return false;
  }

  // activate the render system
  return activateRenderSystem(OpenGL);
}

/**
  This method returns the size and position of the active render window
  @return Output rectangle of the render window
*/
Klaus::Rect Klaus::Graphics::GraphicConfig::getWindowExtents() const
{
  if(!mActiveRenderer)
  {
    mLog->log(25, "(GraphicConfig) no active renderer to get it's render window extents");
    return Klaus::Rect();
  }
  else
    return mActiveRenderer->getWindowExtents();
}

/**
  This method returns value of the given option. If the renderer does not
  exist or the value is not found an empty string will be returned.
  @param strOption Name of the option to returns it's value
  @return Value of the requested option
*/
std::string Klaus::Graphics::GraphicConfig::getOption(const std::string& strOption) const
{
  if(mActiveRenderer)
    return mActiveRenderer->getOptionValue(strOption);
  else
  {
    mLog->log(25, "(GraphicConfig) no active renderer to get option for '%s'", strOption.c_str());
    return "";
  }
}

/**
  This method sets the value for the given option in the active renderer.
  If no renderer was active or the renderer could no set the value for
  the option, false will be returned as the method's output.
  @param strOption Name of the option to set it's value
  @param strValue New value for the option
  @return True if the value could be set for the given option, false otherwise
*/
bool Klaus::Graphics::GraphicConfig::setOption(const std::string& strOption, const std::string& strValue)
{
  if(mActiveRenderer)
    return mActiveRenderer->setOption(strOption, strValue);
  else
  {
    mLog->log(25, "(GraphicConfig) no active renderer to set option '%s'", strOption.c_str());
    return false;
  }
}
