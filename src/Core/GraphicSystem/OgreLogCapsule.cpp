/*
*********************************************************************************
*        OgreLogCapsule.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 05/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Channels ogre log intor our own logger class                       *
*                                                                               *
*********************************************************************************
*/

/*! \file OgreLogCapsule.cpp
<pre>
<b>File:</b>          OgreLogCapsule.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Channels ogre log intor our own logger class
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/06/2010       Mahdi           Initial version created
</pre>
*/

#include "OgreLogCapsule.h"
#include <libZigorat/Logger.h>
#include <iostream>

namespace Klaus
{


OgreLogCapsule::OgreLogCapsule()
{
}

OgreLogCapsule::~OgreLogCapsule()
{
}

/**
 * @remarks This is called whenever the log receives a message and is about to write it out
 * @param message The message to be logged
 * @param lml The message level the log is using
 * @param maskDebug If we are printing to the console or not
 * @param logName the name of this log (so you can have several listeners for different logs, and identify them)
 */
void OgreLogCapsule::messageLogged( const Ogre::String& message, Ogre::LogMessageLevel lml, bool maskDebug, const Ogre::String &logName )
{
  Logger::getSingleton()->log(9, "(ogre) %s", message.c_str());
}


}; // end namespace Klaus;
