/*
*********************************************************************************
*          OpenGLRenderer.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 20/01/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file OpenGLRenderer.h
<pre>
<b>File:</b>          OpenGLRenderer.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       20/01/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
20/01/2011       Mahdi           Initial version created
</pre>
*/

#ifndef OPENGL_RENDERER_H
#define OPENGL_RENDERER_H


#include "RendererBase.h"  // needed to subclass children

namespace Klaus
{

  namespace Graphics
  {

    namespace OpenGraphics
    {

      /*! This class handles resolution configuration in OpenGL renderer.
      */
      class _DLLExportControl ResolutionOption : public OptionBase
      {
        private:
          typedef std::vector<int> IntList; /*!< A dynamic array of integers */
          IntList      mLengths;            /*!< List of supported window lengths */
          IntList      mWidths;             /*!< List of supported window widths */

          bool         parseResolution  ( const std::string & str,
                                          int & l, int & w              );
          bool         addNewResolution ( const std::string & str       );
          virtual void clear            (                               );

        public:
                       ResolutionOption (                               );

          virtual bool setActiveValue   ( const std::string & str       );
          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class handles AntiAliasing configuration in OpenGL renderer.
      */
      class _DLLExportControl AAOption : public OptionBase
      {
        public:
                       AAOption         (                               );

          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class handles Anisotropic Filtering configuration in OpenGL renderer.
      */
      class _DLLExportControl AFOption : public OptionBase
      {
        public:
                       AFOption         (                               );

          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class handles FullScreen configuration in OpenGL renderer.
      */
      class _DLLExportControl FSOption : public OptionBase
      {
        public:
                       FSOption         (                               );

          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class handles vertical synchronisation in OpenGL renderer.
      */
      class _DLLExportControl VSOption : public OptionBase
      {
        public:
                       VSOption         (                               );

          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class handles all those configurations which do not need to be handled
      */
      class _DLLExportControl VoidOption: public OptionBase
      {
        public:
                       VoidOption       (                               );

          virtual bool setDefaultValue  (                               );
          virtual bool supportsOption   ( const std::string & conf      ) const;
          virtual int  parseOption      ( const StringArray & options   );
          virtual void configureRenderer(                               );
      };


      /*! This class is a wrapper for OpenGL renderering subsystem. Creates parsers
          and configurators for accessing and changing options.
          Currently these options are supported:
            - window and screen resolution
            - anti-aliasing
            - anisotropic filtering
            - full screen mode
            - vertical synchronisation
      */
      class _DLLExportControl OpenGLRenderer : public Singleton<OpenGLRenderer>
                                             , public RendererBase
      {
        protected:
          virtual bool            supportsType      ( const std::string & name ) const;

          virtual void            _loadDefaultConfig(                          );

                                  OpenGLRenderer    (                          );
                                ~ OpenGLRenderer    (                          );

        public:
          // Singleton operations
          static OpenGLRenderer * getSingleton      (                          );
          static void             freeSingleton     (                          );

          friend class ResolutionOption;
          friend class FSOption;
      };

    }; // end namespace OpenGraphics

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // OPENGL_RENDERER_H
