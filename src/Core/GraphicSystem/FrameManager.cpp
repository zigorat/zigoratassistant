/*
*********************************************************************************
*        FrameManager.cpp : Robocup 3D Soccer Simulation Team Zigorat      *
*                                                                               *
*  Date: 07/29/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Handler of the inter frame update process                          *
*                                                                               *
*********************************************************************************
*/

/*! \file FrameManager.cpp
<pre>
<b>File:</b>          FrameManager.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/29/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Handler of the inter frame update process
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/29/2008       Mahdi           Initial version created
</pre>
*/

#include "FrameManager.h"
#include "GraphicSystem.h"
#include <GUISystem/GUISystem.h>
#include <CoreSystem/CoreSystem.h>
#include <DisplaySystem/DisplaySystem.h>


/** FrameManager singleon instance */
template<> Klaus::Graphics::FrameManager* Klaus::Singleton<Klaus::Graphics::FrameManager>::mInstance = 0;

/**
  This is the constructor of the FrameManager. All the default initial values are set here
*/
Klaus::Graphics::FrameManager::FrameManager()
{
  mQuitApp = false;
}

/**
  Destructor of FrameManager
  Remove ourself as a Window listener
*/
Klaus::Graphics::FrameManager::~FrameManager()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Graphics::FrameManager * Klaus::Graphics::FrameManager::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new FrameManager;
    Logger::getSingleton()->log(1, "(FrameManager) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Graphics::FrameManager::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(FrameManager) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(FrameManager) removed singleton");
  }
}

/**
  Override frameStarted event to process that (don't care about frameEnded)
  @param evt Event sent by the Ogre Core
  @return True if wnat to resume, false otherwise
*/
bool Klaus::Graphics::FrameManager::frameStarted(const Ogre::FrameEvent& evt)
{
  // Set engine timings
  GraphicSystem * sys = GraphicSystem::getSingleton();
  sys->mLastFrameDuration = evt.timeSinceLastFrame;
  sys->mCurrentTime = Timing::now() - sys->mStartTime;

  // update for next frame
  Core::CoreSystem::getSingleton()->updateForNextFrame();

  return true;
}

/**
  This method is called after all render targets have had their rendering commands issued,
  but before render windows have been asked to flip their buffers over.
  @param evt Frame event sent by ogre
  @return True to continue rendering, false to drop out of the rendering loop.
*/
bool Klaus::Graphics::FrameManager::frameRenderingQueued( const Ogre::FrameEvent& evt )
{
  if(GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow()->isClosed())
    mQuitApp = true;

  return !mQuitApp;
}

/**
  Just updates statistics
  @param evt Event sent by the ogre core
  @return True if want to resume, false otherwise
*/
bool Klaus::Graphics::FrameManager::frameEnded(const Ogre::FrameEvent& evt)
{
  return true;
}

/**
  This method sets sets the mQuitApp to true so that application end
*/
void Klaus::Graphics::FrameManager::quit()
{
  mQuitApp = true;
}
