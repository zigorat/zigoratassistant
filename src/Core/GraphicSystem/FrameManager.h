/*
*********************************************************************************
*         FrameManager.h : Robocup 3D Soccer Simulation Team Zigorat       *
*                                                                               *
*  Date: 07/29/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Handler of the inter frame update process                          *
*                                                                               *
*********************************************************************************
*/

/*! \file FrameManager.h
<pre>
<b>File:</b>          FrameManager.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/29/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Handler of the inter frame update process
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/29/2008       Mahdi           Initial version created
</pre>
*/

#ifndef FRAME_MANAGER
#define FRAME_MANAGER

#include <Ogre.h>
#include <libZigorat/Singleton.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <CoreFoundation/CoreFoundation.h>
#endif


namespace Klaus
{


namespace Graphics
{


/**********************************************************************************/
/******************************  Class FrameManager  ******************************/
/**********************************************************************************/

/*! This class is used to be registered in OGRE, so that OGRE would inform us about
    frame rendering starts and endings, to be able to do our parts within a frames
    ending and next frames start*/
class _DLLExportControl FrameManager: public Ogre::FrameListener, public Singleton<FrameManager>
{
  protected:
    bool                     mQuitApp;               /*!< Whether quit or contine  */

                             FrameManager            (                             );
                           ~ FrameManager            (                             );

  public:
    static FrameManager*     getSingleton            (                             );
    static void              freeSingleton           (                             );

    bool                     frameStarted            ( const Ogre::FrameEvent& evt );
    bool                     frameRenderingQueued    ( const Ogre::FrameEvent& evt );
    bool                     frameEnded              ( const Ogre::FrameEvent& evt );

    void                     quit                    (                             );
};


}; // end namespace Graphics


}; // end namespace Klaus


#endif // FRAME_MANAGER
