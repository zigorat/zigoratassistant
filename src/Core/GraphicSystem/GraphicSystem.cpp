/*
*********************************************************************************
*         GraphicSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains implementations of the graphic system. This system wraps  *
*            around ogre, intializes and runs ogre.                             *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicSystem.cpp
<pre>
<b>File:</b>          GraphicSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains implementations of the graphic system. This system wraps
                 around ogre, intializes and runs ogre.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#include "GraphicSystem.h"
#include <libZigorat/ApplicationSettings.h>  // needed for ApplicationSettings
#include "FrameManager.h"                    // needed for FrameManager
#include "GraphicConfig.h"                   // needed for GraphicConfig
#include <OgreConfigFile.h>
//#include <iostream>

/** GraphicSystem singleton instance */
template<> Klaus::Graphics::GraphicSystem* Klaus::Singleton<Klaus::Graphics::GraphicSystem>::mInstance = 0;


/**
  Constructor for GraphicSystem. Sets up event system and default bindings
*/
Klaus::Graphics::GraphicSystem::GraphicSystem()
{
  mRoot= NULL;
  mSceneMgr = NULL;
  mLog = Logger::getSingleton();
  mLogCapsule = NULL;
  mLastFrameDuration = 0;

  setRecipientName("GraphicSystem");

  EV_TAKE_SCREENSHOT = registerEvent("GraphicSystem\\TakeScreenshot");
  EV_QUIT = registerEvent("GraphicSystem\\Quit");
  EV_FULL_SCREEN = registerEvent("GraphicSystem\\FullScreen");

  using namespace Input;
  registerBinding(KeyCombination(OnKeyPress, KC_SYSRQ), EV_TAKE_SCREENSHOT);
  registerBinding(KeyCombination(OnKeyPress, KC_F), EV_FULL_SCREEN);
  registerBinding(KeyCombination(OnKeyUp, KC_Q), EV_QUIT);
}

/**
  Destructor of GraphicSystem. Destroys GraphicConfig
*/
Klaus::Graphics::GraphicSystem::~GraphicSystem()
{
  GraphicConfig::freeSingleton();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Graphics::GraphicSystem * Klaus::Graphics::GraphicSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new GraphicSystem;
    Logger::getSingleton()->log(1, "(GraphicSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Graphics::GraphicSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(GraphicSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(GraphicSystem) removed singleton");
  }
}

/**
  Method which will define the source of resources (other than current folder)
  OS X does not set the working directory relative to the app,
  In order to make things portable on OS X we need to provide
  the loading with it's own bundle path location
*/
void Klaus::Graphics::GraphicSystem::setupResources()
{
  // Load resource paths from config file
  Ogre::ConfigFile cf;
  cf.load(ApplicationSettings::getSingleton()->getDataPath() + "resources.cfg");

  // Go through all sections & settings in the file
  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements())
  {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i)
    {
      typeName = i->first;
      archName = i->second;

#if OGRE_PLATFORM == OGRE_PLATFORMAPPLE
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(String(BUNDLE_PATH + '/' + archName), typeName, secName);
#else
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
#endif
    }
  }

  // now initialise all resources
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

/**
  Optional override method where you can create resource listeners (e.g. for loading screens)
*/
void Klaus::Graphics::GraphicSystem::createResourceListener()
{
}

/**
  This method initializes scene manager with default parameters
*/
void Klaus::Graphics::GraphicSystem::createSceneManager()
{
  mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
  mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  mSceneMgr->setAmbientLight(Ogre::ColourValue(0.8f, 0.8f, 0.8f));
}

/**
  This method sets up logging channel. This will redirect all ogre logs into our own logger.
*/
void Klaus::Graphics::GraphicSystem::setupLoggingChannel()
{
  Ogre::LogManager *mgr = new Ogre::LogManager;
  mgr->setLogDetail(Ogre::LL_NORMAL);
  Ogre::Log *log = Ogre::LogManager::getSingleton().createLog("", true, false, true);
  mLogCapsule = new OgreLogCapsule;
  log->addListener(mLogCapsule);
}

/**
  This method captures current frame and saves it to a file.
*/
void Klaus::Graphics::GraphicSystem::takeScreenShot()
{
  static int mNumScreenShots = 0;
  std::stringstream ss;
  ss << "screenshot_" << ++mNumScreenShots << ".png";
  GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow()->writeContentsToFile(ss.str());
  mLog->log(3, "Saved: " + ss.str() );
}

/**
  This method creates a hardware mesh of desired object with given object parameters
  @param vertices The actual vertices
  @param colours Colours of the vertices
  @param nVertices Count of the vertices
  @param faces Triangles making the mesh
  @param nFaces Number of triangle vertices
  @param strName name of the mesh
  @return Pointer to a created hardware mesh
*/
Ogre::MeshPtr Klaus::Graphics::GraphicSystem::createHardwareMesh(
                                        float vertices[],
                                        Ogre::RGBA colours[],
                                        size_t nVertices,
                                        unsigned short faces[],
                                        size_t nFaces,
                                        const char *strName )
{
  using namespace Ogre;
  /// Create the mesh via the MeshManager
  MeshPtr msh = MeshManager::getSingleton().createManual( strName, "General" );

  /// Create one submesh
  SubMesh* sub = msh->createSubMesh();

  /// Create vertex data structure for 8 vertices shared between submeshes
  msh->sharedVertexData = new Ogre::VertexData();
  msh->sharedVertexData->vertexCount = nVertices;

  /// Create declaration (memory format) of vertex data
  VertexDeclaration* decl = msh->sharedVertexData->vertexDeclaration;
  size_t offset = 0;

  // 1st buffer
  decl->addElement(0, offset, VET_FLOAT3, VES_POSITION);
  offset += VertexElement::getTypeSize(VET_FLOAT3);
  decl->addElement(0, offset, VET_FLOAT3, VES_NORMAL);
  offset += VertexElement::getTypeSize(VET_FLOAT3);
  /// Allocate vertex buffer of the requested number of vertices (vertexCount)
  /// and bytes per vertex (offset)
  HardwareVertexBufferSharedPtr vbuf =
  HardwareBufferManager::getSingleton().createVertexBuffer(
  offset, msh->sharedVertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY);
  /// Upload the vertex data to the card
  vbuf->writeData(0, vbuf->getSizeInBytes(), vertices, true);

  /// Set vertex buffer binding so buffer 0 is bound to our vertex buffer
  VertexBufferBinding* bind = msh->sharedVertexData->vertexBufferBinding;
  bind->setBinding(0, vbuf);

  // 2nd buffer
  offset = 0;
  decl->addElement(1, offset, VET_COLOUR, VES_DIFFUSE);
  offset += VertexElement::getTypeSize(VET_COLOUR);
  /// Allocate vertex buffer of the requested number of vertices (vertexCount)
  /// and bytes per vertex (offset)
  vbuf = HardwareBufferManager::getSingleton().createVertexBuffer(
  offset, msh->sharedVertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY);
  /// Upload the vertex data to the card
  vbuf->writeData(0, vbuf->getSizeInBytes(), colours, true);

  /// Set vertex buffer binding so buffer 1 is bound to our colour buffer
  bind->setBinding(1, vbuf);

  /// Allocate index buffer of the requested number of vertices (ibufCount)
  HardwareIndexBufferSharedPtr ibuf = HardwareBufferManager::getSingleton().
  createIndexBuffer(
  HardwareIndexBuffer::IT_16BIT,
  nFaces,
  HardwareBuffer::HBU_STATIC_WRITE_ONLY);

  /// Upload the index data to the card
  ibuf->writeData(0, ibuf->getSizeInBytes(), faces, true);

  /// Set parameters of the submesh
  sub->useSharedVertices = true;
  sub->indexData->indexBuffer = ibuf;
  sub->indexData->indexCount = nFaces;
  sub->indexData->indexStart = 0;

  return msh;
}

/**
  A sample method to create an actual hardware mesh. Demonstrates the needed steps
*/
void Klaus::Graphics::GraphicSystem::createCube()
{
  const float sqrt13 = 0.577350269f; /* sqrt(1/3) */

  const size_t nVertices = 8;
  const size_t vbufCount = 3*2*nVertices;
  float vertices[vbufCount] =
  {
    -0.5,0.5,-0.5,        //0 position
    -sqrt13,sqrt13,-sqrt13,     //0 normal
    0.5,0.5,-0.5,         //1 position
    sqrt13,sqrt13,-sqrt13,      //1 normal
    0.5,-0.5,-0.5,        //2 position
    sqrt13,-sqrt13,-sqrt13,     //2 normal
    -0.5,-0.5,-0.5,       //3 position
    -sqrt13,-sqrt13,-sqrt13,    //3 normal
    -0.5,0.5,0.5,         //4 position
    -sqrt13,sqrt13,sqrt13,      //4 normal
    0.5,0.5,0.5,          //5 position
    sqrt13,sqrt13,sqrt13,       //5 normal
    0.5,-0.5,0.5,         //6 position
    sqrt13,-sqrt13,sqrt13,      //6 normal
    -0.5,-0.5,0.5,        //7 position
    -sqrt13,-sqrt13,sqrt13,     //7 normal
  };

  /// Define the vertices (8 vertices, each consisting of 2 groups of 3 floats
  Ogre::RenderSystem* rs = Ogre::Root::getSingleton().getRenderSystem();
  Ogre::RGBA colours[nVertices];
  Ogre::RGBA *pColour = colours;

  // Use render system to convert colour value since colour packing varies
  for(unsigned i = 0; i < nVertices; i++)
    rs->convertColourValue(Ogre::ColourValue(1.0,1.0,1.0), pColour++); //0 colour

  /// Define 12 triangles (two triangles per cube face)
  /// The values in this table refer to vertices in the above table
  const size_t nFaces = 36;
  unsigned short faces[nFaces] =
  {
    0,2,3,
    0,1,2,
    1,6,2,
    1,5,6,
    4,6,5,
    4,7,6,
    0,7,4,
    0,3,7,
    0,5,1,
    0,4,5,
    2,7,3,
    2,6,7
  };

  Ogre::MeshPtr msh = createHardwareMesh(vertices, colours, nVertices, faces, nFaces, "Cube" );
  /// Set bounding information (for culling)
  msh->_setBounds(Ogre::AxisAlignedBox(-0.5,-0.5,-0.5,0.5,0.5,0.5));
  msh->_setBoundingSphereRadius(Ogre::Math::Sqrt(1));

  /// Notify Mesh object that it has been loaded
  msh->load();
  Ogre::MeshSerializer * s = new Ogre::MeshSerializer;
  s->exportMesh(msh.get(), "/home/Klaus/Cube.mesh");
  delete s;
}

/**
  This method returns the ogre scene manager instance
  @return The ogre scene manager instance
*/
Ogre::SceneManager * Klaus::Graphics::GraphicSystem::getSceneManager()
{
  return mSceneMgr;
}

/**
  This method returns the duration to render the last frame. This is equal
  to the duration since last update of the core. The returns value is in
  seconds and has the accuracy of microseconds.
  @return Duration since last frame was being rendered.
*/
double Klaus::Graphics::GraphicSystem::getLastFrameDuration() const
{
  return mLastFrameDuration;
}

/**
  This method returns the number of seconds since the engine was started.
  @return Number of seconds since the engine was started
*/
double Klaus::Graphics::GraphicSystem::getTimeSinceStart() const
{
  return mCurrentTime;
}

/**
  This method processes events sent to this system.
  @param ev The event to process
*/
void Klaus::Graphics::GraphicSystem::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_TAKE_SCREENSHOT)
    takeScreenShot();
  else if(ev == EV_QUIT)
    FrameManager::getSingleton()->quit();
  else if(ev == EV_FULL_SCREEN)
  {
    if(mConfig->getOption("Full Screen") == "No")
      mConfig->setOption("Full Screen", "Yes");
    else
      mConfig->setOption("Full Screen", "No");
  }
  else
    mLog->log(3, "(GraphicSystem) unknown event %s",
                 Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}

/**
  This method is responsible to initialize ogre.
*/
bool Klaus::Graphics::GraphicSystem::initializeEngine()
{
  // The attempt to start begins
  mLog->log(3, "(GraphicSystem) starting engine" );

  // Setup logging channel. Channels ogre logs to Klaus::Logger
  setupLoggingChannel();

  // create ogre root
  ApplicationSettings * settings = ApplicationSettings::getSingleton();
  std::string strPath = settings->getDataPath();
  std::string strPluginsPath, strOgreConfPath;
  if(settings->isDebugMode())
  {
    strPluginsPath = strPath + "plugins-debug.cfg";
    strOgreConfPath = strPath + "ogre.cfg";
  }
  else
  {
    strPluginsPath = strPath + "plugins-release.cfg";
    strOgreConfPath = strPath + "ogre.cfg";
  }

  mRoot = new Ogre::Root(strPluginsPath, strOgreConfPath);
  if(!mRoot)
  {
    mLog->log(3, "(GraphicSystem) could not start ogre root");
    return false;
  }

  // set up our own graphic configuration system
  mConfig = GraphicConfig::getSingleton();
  if(!mConfig->initialize())
    return false;

  // Create any resource listeners (for loading screens)
  createResourceListener();

  // Load resources
  setupResources();

  // Initialize the scene manager
  createSceneManager();

  // Start frame listening
  mRoot->addFrameListener(FrameManager::getSingleton());

  // Engine started up.
  mLog->log(3, "(GraphicSystem) engine started");

  // Set up the starting time
  mStartTime = Timing::now();

  // The viewport will be created in DisplaySystem
  return true;
}

/**
  This method disposes of ogre related stuff before removing ogre.
*/
void Klaus::Graphics::GraphicSystem::finalizeEngine()
{
  delete mLogCapsule;
}

/**
  This method starts engine by telling ogre to start rendering loop
*/
void Klaus::Graphics::GraphicSystem::startEngine()
{
  mRoot->startRendering();
}
