/*
*********************************************************************************
*         OgreLogCapsule.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 05/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Channels ogre log intor our own logger class                       *
*                                                                               *
*********************************************************************************
*/

/*! \file OgreLogCapsule.h
<pre>
<b>File:</b>          OgreLogCapsule.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Channels ogre log intor our own logger class
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef OGRE_LOG_CAPSULE
#define OGRE_LOG_CAPSULE

#include <OgreLog.h>

namespace Klaus
{

  /**
    This is created to divert all ogre logs into the zigorat logger singleton.
    Prevents the logs to be written on an arbitrary file or standard output.
  */
  class OgreLogCapsule : public Ogre::LogListener
  {
    public:
                     OgreLogCapsule (                              );
      virtual      ~ OgreLogCapsule (                              );
      virtual void   messageLogged  ( const Ogre::String& message,
                                      Ogre::LogMessageLevel lml,
                                      bool maskDebug,
                                      const Ogre::String &logName  );
  };

}; // end namespace Klaus

#endif // OGRE_LOG_CAPSULE
