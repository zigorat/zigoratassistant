/*
*********************************************************************************
*          GraphicConfig.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 08/09/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicConfig.h
<pre>
<b>File:</b>          GraphicConfig.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/09/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/09/2010       Mahdi           Initial version created
</pre>
*/

#ifndef GRAPHIC_CONFIG
#define GRAPHIC_CONFIG


#include "RendererBase.h"  // needed for RendererBase


namespace Klaus
{

  namespace Graphics
  {

    /*! This class handles all configurations related to graphics. Screen resolution, antialiasing
        levels and other types of graphical configurations are all managed in this class. This class
        provides methods for accessing current render system and render window.
    */
    class _DLLExportControl GraphicConfig : public Klaus::Singleton<GraphicConfig>
    {
      private:
        RendererBase::List     mRenderers;         /*!< List of all available renderers */
        RendererBase         * mActiveRenderer;    /*!< Currently active renderer */
        Logger               * mLog;               /*!< Logger instance used for logging */

        void                   clearRenderers      (                                 );
        void                   installRenderer     ( RendererBase * renderer         );

                               GraphicConfig       (                                 );
                             ~ GraphicConfig       (                                 );

      public:
        static GraphicConfig * getSingleton        (                                 );
        static void            freeSingleton       (                                 );

        unsigned               getRendererCount    (                                 ) const;
        RendererBase         * getRenderer         ( const RendererType & type       );
        RendererBase         * getRenderer         ( const unsigned & index          );
        RendererBase         * getActiveRenderer   (                                 );
        bool                   activateRenderSystem( const RendererType & type       );
        bool                   initialize          (                                 );

        Klaus::Rect            getWindowExtents    (                                 ) const;
        std::string            getOption           ( const std::string & strOption   ) const;
        bool                   setOption           ( const std::string & strOption,
                                                     const std::string & strValue    );
    };

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // GRAPHIC_CONFIG
