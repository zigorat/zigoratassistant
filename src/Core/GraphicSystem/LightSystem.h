/*
*********************************************************************************
*             LightSystem.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains implementations of loading and using lights and light     *
*            systems. storage class types for visual stuff like                 *
*                                                                               *
*********************************************************************************
*/

/*! \file LightSystem.h
<pre>
<b>File:</b>          LightSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains implementations of loading and using lights and light systems.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#ifndef LIGHT_SYSTEM
#define LIGHT_SYSTEM

#include <Ogre.h>                     // needed for Ogre::Light, Ogre::SceneManager
#include <libZigorat/Algebra.h>       // needed for Quaternion and VecPosition
#include <EventSystem/EventSystem.h>  // needed for EventRecipient
#include <libZigorat/SLang.h>         // needed for SToken


namespace Klaus
{

  namespace Graphics
  {


    /*! This class is responsible to load, create, hold and activate light specific
        information, like colours it gives to the scene or position of the light.*/
    class _DLLExportControl Light
    {
      private:
        Ogre::Light            * mLight;              /*!< Ogre light instance   */
        Ogre::SceneNode        * mSceneNode;          /*!< Scene node to attach  */
        Ogre::SceneManager     * mSceneMgr;           /*!< Ogre scene manager    */

      public:
                                 Light                (                          );
                               ~ Light                (                          );

        std::string              getName              (                          ) const;

        Ogre::ColourValue        getDiffuseColor      (                          ) const;
        void                     setDiffuseColor      ( Ogre::ColourValue diff   );

        Ogre::ColourValue        getSpecularColor     (                          ) const;
        void                     setSpecularColor     ( Ogre::ColourValue spec   );

        VecPosition              getPosition          (                          );
        void                     setPosition          ( VecPosition pos          );

        bool                     parse                ( SToken token             );

        void                     activate             (                          );
        void                     deactivate           (                          );

        /*! This is for handling multiple light in one lighting effect */
        typedef std::vector<Light*> List;
    };


    /*! This class is used to store a set of lights for a specific scene. The
        lighting can be enabled, disabled and read from a configuration file.
    */
    class _DLLExportControl Lighting
    {
      private:
        Light::List              mLights;             /*!< List of all lights    */
        std::string              mCurrentLight;       /*!< Current active light  */
        Ogre::ColourValue        mAmbient;            /*!< Ambient light of scene*/
        std::string              mName;               /*!< Name of the lighting  */
        Ogre::SceneManager     * mSceneMgr;           /*!< Ogre scene manager    */

        void                     clear                (                          );

      public:
                                 Lighting             (                          );
                               ~ Lighting             (                          );

        std::string              getName              (                          ) const;
        bool                     parse                ( SToken token             );
        void                     activate             (                          );
        void                     deactivate           (                          );

        /*! This holds multiple lightings for easy handling whithin LightingManager */
        typedef std::vector<Lighting*> List;
    };


    /*! This class holds all the lighting setup that are defined in a scene and
        can traverse between them. Each lighting set contains a number of lights
        and an ambient light.
    */
    class _DLLExportControl LightingSystem : public Singleton<LightingSystem>,
                                             public Events::EventRecipient
    {
      private:
        // events
        EventID                  EV_NEXT_LIGHTING;    /*!< Next lighting event id*/

        Lighting::List           mLightings;          /*!< All lightings in scene*/
        std::string              mDefaultLightName;   /*!< Default lighting name */
        int                      mActiveLighting;     /*!< Current active setup  */

                                 LightingSystem       (                          );
                               ~ LightingSystem       (                          );

      public:
        static LightingSystem  * getSingleton         (                          );
        static void              freeSingleton        (                          );

        void                     removeAllLights      (                          );
        size_t                   getLightingCount     (                          );
        bool                     activateLighting     ( size_t iLighting         );
        bool                     activateLighting     ( const std::string &name  );
        bool                     activateNextLighting (                          );
        bool                     parse                ( SToken token             );
        bool                     parse                ( const std::string & name );

        virtual void             processEvent         ( const Events::Event & ev );
    };

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // LIGHT_SYSTEM
