/*
*********************************************************************************
*          GraphicSystem.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains implementations of the graphic system. This system wraps  *
*            around ogre, intializes and runs ogre.                             *
*                                                                               *
*********************************************************************************
*/

/*! \file GraphicSystem.h
<pre>
<b>File:</b>          GraphicSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains implementations of the graphic system. This system wraps
                 around ogre, intializes and runs ogre.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#ifndef GRAPHIC_SYSTEM
#define GRAPHIC_SYSTEM

#include <Ogre.h>
#include <EventSystem/EventSystem.h>  // needed for EventSystem
#include "GraphicConfig.h"            // needed for GraphicConfig
#include "OgreLogCapsule.h"           // needed for OgreLogCapsule

namespace Klaus
{

  namespace Graphics
  {


    /**
      Graphic subsystem. Manages starting and running ogre. Diverts ogre logs to standard zigorat
      logger and provides important ogre tool instances to all plugins and subsystems.
    */
    class _DLLExportControl GraphicSystem : public Singleton<GraphicSystem>,
                                            public Events::EventRecipient
    {
      private:
        // Event IDs
        EventID               EV_TAKE_SCREENSHOT;   /*!< Take screenshot event id         */
        EventID               EV_FULL_SCREEN;       /*!< Set full screen event id         */
        EventID               EV_QUIT;              /*!< Quit event id                    */

        // Ogre objects
        Ogre::Root          * mRoot;                /*!< Ogre core instance               */
        Ogre::SceneManager  * mSceneMgr;            /*!< Ogre scene manager instance      */
        OgreLogCapsule      * mLogCapsule;          /*!< Ogre log capsulator              */

        Logger              * mLog;                 /*!< Logger instance                  */
        double                mLastFrameDuration;   /*!< Last frame duration              */
        double                mStartTime;           /*!< Strarting time of engine         */
        double                mCurrentTime;         /*!< Current time of engine           */
        GraphicConfig       * mConfig;              /*!< Graphical system configurations  */

        // Resource processing
        void                  setupResources        (                          );
        void                  createResourceListener(                          );

        void                  createSceneManager    (                          );
        void                  setupLoggingChannel   (                          );

        void                  takeScreenShot        (                          );


        Ogre::MeshPtr         createHardwareMesh    ( float vertices[],
                                                      Ogre::RGBA colours[],
                                                      size_t nVertices,
                                                      unsigned short faces[],
                                                      size_t nFaces,
                                                      const char *strName      );
        void                  createCube            (                          );

                              GraphicSystem         (                          );
                            ~ GraphicSystem         (                          );

      public:
        static GraphicSystem* getSingleton          (                          );
        static void           freeSingleton         (                          );

        Ogre::SceneManager  * getSceneManager       (                          );
        double                getLastFrameDuration  (                          ) const;
        double                getTimeSinceStart     (                          ) const;

        virtual void          processEvent          ( const Events::Event & ev );
        bool                  initializeEngine      (                          );
        void                  finalizeEngine        (                          );
        void                  startEngine           (                          );

        friend class FrameManager;
    };

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // GRAPHIC_SYSTEM
