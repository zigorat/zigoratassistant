/*
*********************************************************************************
*          OpenGLRenderer.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 20/01/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file OpenGLRenderer.cpp
<pre>
<b>File:</b>          OpenGLRenderer.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       20/01/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
20/01/2011       Mahdi           Initial version created
</pre>
*/

#include "OpenGLRenderer.h"
#include <sstream>           // needed for stringstream

/*! The singleton instance of the OpenGLRenderer */
template<> Klaus::Graphics::OpenGraphics::OpenGLRenderer*
Klaus::Singleton<Klaus::Graphics::OpenGraphics::OpenGLRenderer>::mInstance = 0;



/***********************************************************************/
/**********************  Class ResolutionOption  ***********************/
/***********************************************************************/

/**
  Constructor of ResolutionOption. Sets it's casual name
*/
Klaus::Graphics::OpenGraphics::ResolutionOption::ResolutionOption() : OptionBase("Resolution")
{
}

/**
  This method will parse the given string and extract a length and width out of it
  @param str The string containing the display mode
  @param l The length of the display mode
  @param w The width of the display mode
  @return True if parse was successfull, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::ResolutionOption::parseResolution(const std::string& str, int & l, int & w)
{
  int index = 0;
  int size = (int)str.size();

  // parse first number (length of window)
  if(str.empty())
    return false;

  Parse::parseFirstInt(str, l, index);
  for(; (index < size) && (str[index] != 'x'); ++index)
    ; // do nothing
  ++index;

  // parse second number (width of window)
  if(index >= size)
    return false;

  Parse::parseFirstInt(str, w, index);
  return (l != 0) && (w != 0);
}

/**
  This method parses a given ogre display mode string and extracts a length
  and width from it.
  @return True if parsed successfull, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::ResolutionOption::addNewResolution(const std::string& str)
{
  int l, w;
  if(!parseResolution(str, l, w))
    return false;

  std::stringstream ss;
  ss << std::setw(4) << l << " x " << std::setw(4) << w;

  mValues.push_back(ss.str());
  mLengths.push_back(l);
  mWidths.push_back(w);
  mLog->log(25, "(Resolution) supports %s", mValues[mValues.size()-1].c_str());
  return true;
}

/**
  This method clears the resolutions.
*/
void Klaus::Graphics::OpenGraphics::ResolutionOption::clear()
{
  Klaus::Graphics::OptionBase::clear();
  mLengths.clear();
  mWidths.clear();
}

/**
  This method sets the active value's index according to the given string.
  Overriden to do resolution specific activation.
  @param str New value for resolution
  @return True if the selected value existed in the list of available values, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::ResolutionOption::setActiveValue(const std::string& str)
{
  int l, w;
  if(!parseResolution(str, l, w))
    return false;

  for(size_t i = 0; i < mLengths.size(); i++)
    if(mLengths[i] == l && mWidths[i] == w)
    {
      mActiveValue = i;
      configureRenderer();
      return true;
    }

  mLog->log(25, "(Resolution) resolution not supported: '%s'", str.c_str());
  return false;
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::ResolutionOption::setDefaultValue()
{
  if(mValues.empty())
  {
    mLog->log(25, "(Resolution) empty display modes");
    return false;
  }

  // get a default mode
  mActiveValue = -1;
  for(size_t i = 0; i < mValues.size(); ++i)
    if(mLengths[i] == 640 && mWidths[i] == 480)
    {
      mActiveValue = (int)i;
      mLog->log(25, "(Resolution) selected %dx%d as default display mode", mLengths[mActiveValue], mWidths[mActiveValue]);
      return true;
    }

  // fallback on the first option
  if(mActiveValue == -1)
    mActiveValue = 0;

  mLog->log(25, "(Resolution) falling back on the first display mode %dx%d", mLengths[0], mWidths[0]);
  return true;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::ResolutionOption::supportsOption(const std::string& conf) const
{
  return conf == "Video Mode";
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::ResolutionOption::parseOption(const StringArray& options)
{
  // parse all video modes
  int iErrors = 0;
  for(size_t i = 0; i < options.size(); ++i)
    if(!addNewResolution(options[i]))
      iErrors++;

  return iErrors;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::ResolutionOption::configureRenderer()
{
  if(mActiveValue == -1)
    mLog->log(25, "(Resolution) no active resolution specified");
  else
  {
    int l = mLengths[mActiveValue];
    int w = mWidths[mActiveValue];

    // set the renderer configuration
    Ogre::String str = Ogre::StringConverter::toString(l, 4) + " x " +
                       Ogre::StringConverter::toString(w, 4);

    mRenderer->setConfigOption("Video Mode", str);

    // update the render window if created
    Ogre::RenderWindow * win = OpenGLRenderer::getSingleton()->getRenderWindow();
    if(!win)
    {
      mLog->log(25, "(Resolution) no RenderWindow, updating renderer configuration only");
      return;
    }

    if(win->isFullScreen())
      win->setFullscreen(true, l, w);
    else
      win->resize(l, w);

    mLog->log(25, "(Resolution) resized to %dx%d", l, w);
    OpenGLRenderer::getSingleton()->windowOpenned(win);
  }
}




/***********************************************************************/
/*************************  Class AA Option  ***************************/
/***********************************************************************/

/**
  Constructor of AAOption. Sets it's casual name
*/
Klaus::Graphics::OpenGraphics::AAOption::AAOption() : OptionBase("AntiAliasing")
{
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::AAOption::setDefaultValue()
{
  if(mValues.empty())
  {
    mLog->log(25, "(AntiAliasing) antialiasing not supported by OpenGL hardware");
    return false;
  }

  mActiveValue = mValues.size() - 1;
  return true;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::AAOption::supportsOption(const std::string& conf) const
{
  return conf == "FSAA";
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::AAOption::parseOption(const StringArray& options)
{
  // parse all video modes
  for(size_t i = 0; i < options.size(); ++i)
  {
    mValues.push_back(options[i]);
    mLog->log(25, "(AntiAliasing) supports %sX antialiasing", mValues[mValues.size() - 1].c_str());
  }

  return 0;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::AAOption::configureRenderer()
{
  if(mActiveValue == -1)
    mLog->log(25, "(AntiAliasing) no active AA levels");
  else if(mRenderer)
    mRenderer->setConfigOption("FSAA", mValues[mActiveValue]);

  // TODO: update AA level in realtime
}




/***********************************************************************/
/*************************  Class AF Option  ***************************/
/***********************************************************************/

/**
  Constructor of AFOption. Sets it's casual name
*/
Klaus::Graphics::OpenGraphics::AFOption::AFOption() : OptionBase("Anisotropic Filtering")
{
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::AFOption::setDefaultValue()
{
  if(mValues.empty())
  {
    mLog->log(25, "(Anisotropic Filtering) anisotropic filtering not supported by OpenGL hardware");
    return false;
  }

  mActiveValue = mValues.size() - 1;
  return true;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::AFOption::supportsOption(const std::string& conf) const
{
  return conf == "Aniso";
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::AFOption::parseOption(const StringArray& options)
{
  // parse all video modes
  for(size_t i = 0; i < options.size(); ++i)
  {
    mValues.push_back(options[i]);
    mLog->log(25, "(Anisotropic Filtering) supports %sX anisotropic filtering", mValues[mValues.size() - 1].c_str());
  }

  return 0;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::AFOption::configureRenderer()
{
  if(mActiveValue == -1)
    mLog->log(25, "(Anisotropic Filtering) no active aniso levels");
  else if(mRenderer)
    mRenderer->setConfigOption("FSAF", mValues[mActiveValue]);

  // TODO: update AF level in realtime
}




/***********************************************************************/
/**************************  Class FSOption  ***************************/
/***********************************************************************/

/**
  Constructor of FSOption. Sets it's casual name
*/
Klaus::Graphics::OpenGraphics::FSOption::FSOption() : OptionBase("Full Screen")
{
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::FSOption::setDefaultValue()
{
  if(mValues.empty())
  {
    mLog->log(25, "(Full Screen) full screen not supported by OpenGL hardware");
    return false;
  }

  for(size_t i = 0; i < mValues.size(); i++)
    if(mValues[i] == "No")
    {
      mActiveValue = (int)i;
      mLog->log(25, "(Full Screen) defaulted to windowed mode");

      return true;
    }

  mActiveValue = 0;
  mLog->log(25, "(Full Screen) falling back to %s", mValues[mActiveValue].c_str());
  return false;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::FSOption::supportsOption(const std::string& conf) const
{
  return conf == "Full Screen";
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::FSOption::parseOption(const StringArray& options)
{
  // parse all video modes
  for(size_t i = 0; i < options.size(); ++i)
    mValues.push_back(options[i]);

  mLog->log(25, "(Full Screen) supports full screen mode");
  return 0;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::FSOption::configureRenderer()
{
  if(mActiveValue == -1)
    mLog->log(25, "(Full Screen) full screen mode not set");
  else
    mRenderer->setConfigOption("Full Screen", mValues[mActiveValue]);

  Ogre::RenderWindow * win = OpenGLRenderer::getSingleton()->getRenderWindow();
  if(!win)
  {
    mLog->log(25, "(Full Screen) no render window, updating only renderer configuration");
    return;
  }

  int w = win->getWidth();
  int h = win->getHeight();

  if(mValues[mActiveValue] == "No")
  {
    win->setFullscreen(false, w, h);
    mLog->log(25, "(Full Screen) deactivating full screen mode");
  }
  else if(mValues[mActiveValue] == "Yes")
  {
    win->setFullscreen(true, w, h);
    mLog->log(25, "(Full Screen) activating full screen mode");
  }
  else
    mLog->log(25, "(Full Screen) Unknown active option %s", mValues[mActiveValue].c_str());

  OpenGLRenderer::getSingleton()->windowResized(win);
  OpenGLRenderer::getSingleton()->windowMoved(win);
}




/***********************************************************************/
/**************************  Class VSOption  ***************************/
/***********************************************************************/

/**
  Constructor of VSOption. Sets it's casual name
*/
Klaus::Graphics::OpenGraphics::VSOption::VSOption() : OptionBase("VSync")
{
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::VSOption::setDefaultValue()
{
  if(mValues.empty())
  {
    mLog->log(25, "(VSync) VSync not supported by OpenGL hardware");
    return false;
  }

  for(size_t i = 0; i < mValues.size(); i++)
    if(mValues[i] == "No")
    {
      mActiveValue = i;
      mLog->log(25, "(VSync) defaulted to not synchronize");

      return true;
    }

  mActiveValue = 0;
  mLog->log(25, "(VSync) falling back to %s", mValues[mActiveValue].c_str());
  return false;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::VSOption::supportsOption(const std::string& conf) const
{
  return conf == "VSync";
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::VSOption::parseOption(const StringArray& options)
{
  // parse all video modes
  for(size_t i = 0; i < options.size(); ++i)
    mValues.push_back(options[i]);

  mLog->log(25, "(VSync) supports vertical synchronization");
  return 0;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::VSOption::configureRenderer()
{
  if(mActiveValue == -1)
    mLog->log(25, "(VSync) VSync mode not set");
  else
    mRenderer->setConfigOption("VSync", mValues[mActiveValue]);

  if(mValues[mActiveValue] == "Yes")
  {
    mRenderer->setWaitForVerticalBlank(true);
    mLog->log(25, "(VSync) enabled vertical synchronization");
  }
  else if(mValues[mActiveValue] == "No")
  {
    mRenderer->setWaitForVerticalBlank(false);
    mLog->log(25, "(VSync) disabled vertical synchronization");
  }
  else
    mLog->log(25, "(VSync) unknown value for VSync: '%s'", mValues[mActiveValue].c_str());
}




/***********************************************************************/
/**************************  Class VoidOption  *************************/
/***********************************************************************/

/**
  Constructor of VoidOption
*/
Klaus::Graphics::OpenGraphics::VoidOption::VoidOption() : OptionBase("")
{
}

/**
  This method is responsible to select a default value for the option.
  The value shall be selected from the parsed values.
  @return True if a default value could be set, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::VoidOption::setDefaultValue()
{
  return true;
}

/**
  Checks whether the given option name is supported for parsing and use in
  renderer. Shall be implemented by the child class.
  @param conf The option name
  @return True if the option is supported, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::VoidOption::supportsOption(const std::string& conf) const
{
  bool bResult =
                  (conf == "Display Frequency") ||
                  (conf == "RTT Preferred Mode") ||
                  (conf == "sRGB Gamma Conversion");
  if(bResult)
    mLog->log(25, "(VoidOption) ignoring option %s", conf.c_str());

  return bResult;
}

/**
  Parses configuration related to the option. List of possible values for
  the option is given by a StringArray.
  @param options The option's values to be parsed and used.
  @return Number of errors occured in parse process
*/
int Klaus::Graphics::OpenGraphics::VoidOption::parseOption(const StringArray& options)
{
  return 0;
}

/**
  Sets the options in the renderer.
*/
void Klaus::Graphics::OpenGraphics::VoidOption::configureRenderer()
{
}




/***********************************************************************/
/***********************  Class OpenGLRenderer  ************************/
/***********************************************************************/

/**
  Constructor of OpenGLRenderer. Creates option configurators.
*/
Klaus::Graphics::OpenGraphics::OpenGLRenderer::OpenGLRenderer()
{
  mType = OpenGL;
  mOptions.push_back(new ResolutionOption);
  mOptions.push_back(new AAOption);
  mOptions.push_back(new AFOption);
  mOptions.push_back(new FSOption);
  mOptions.push_back(new VSOption);
  mOptions.push_back(new VoidOption);

  setupConfig("OpenGLRenderer");
}

/**
  Destructor of OpenGLRenderer. Does nothing
*/
Klaus::Graphics::OpenGraphics::OpenGLRenderer::~OpenGLRenderer()
{

}

/**
 * This method returns the only instance to this class
 * @return singleton instance
 */
Klaus::Graphics::OpenGraphics::OpenGLRenderer * Klaus::Graphics::OpenGraphics::OpenGLRenderer::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new OpenGLRenderer;
    Logger::getSingleton()->log(1, "(OpenGLRenderer) created singleton");
  }

  return mInstance;
}

/**
 * This method frees the only instace of the this class
 */
void Klaus::Graphics::OpenGraphics::OpenGLRenderer::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(OpenGLRenderer) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(OpenGLRenderer) removed singleton");
  }
}

/**
  This method checks whether the class can be used for the given ogre
  renderer system name or not
  @param name Name of the ogre renderer
  @return True if the class supports the given ogre renderer name, false otherwise
*/
bool Klaus::Graphics::OpenGraphics::OpenGLRenderer::supportsType(const std::string& name) const
{
  return name == "OpenGL Rendering Subsystem";
}

/**
 * This method sets up default congfiguration for this system
 */
void Klaus::Graphics::OpenGraphics::OpenGLRenderer::_loadDefaultConfig()
{
  // do it only if the renderer is initialized
  if(mRenderer)
    for(size_t i = 0; i < mOptions.size(); i++)
      mOptions[i]->setDefaultValue();
}
