/*
*********************************************************************************
*           RendererBase.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 29/01/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file RendererBase.h
<pre>
<b>File:</b>          RendererBase.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       20/01/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
29/01/2011       Mahdi           Initial version created
</pre>
*/

#ifndef RENDERER_BASE_H
#define RENDERER_BASE_H


#include <libZigorat/Logger.h>           // needed for Logger
#include <libZigorat/Geometry.h>         // needed for Rect
#include <ConfigSystem/Configurable.h>   // needed for Configurable
#include <CoreSystem/Types.h>            // needed for EventID
#include <Ogre.h>                        // needed for Ogre classes
#include <libZigorat/Parse.h>            // needed for StringArray


namespace Klaus
{

  namespace Graphics
  {

    /*! This enumeration holds all types of rendering subsystems which currently application
        can use. */
    enum RendererType
    {
      OpenGL = 0,
      DirectX9,
      DirectX10,
      UnknownRenderer,
    };


    /*! OptionBase is the base class for all kinds of options which are used in ogre render
        systems. Provides methods for getting all sane values and the currently active value.
        Also provides the API to configure the rendering subsystem with it's values.
    */
    class _DLLExportControl OptionBase
    {
      private:
        std::string          mName;           /*!< Casual name of the option */

      protected:
        StringArray          mValues;         /*!< List of all available options */
        int                  mActiveValue;    /*!< Active option's index in the list */
        Ogre::RenderSystem * mRenderer;       /*!< Renderer instance */
        Logger             * mLog;            /*!< Logger instance */

        virtual void         clear            (                               );

      public:
                             OptionBase       ( const std::string & strName   );
                           ~ OptionBase       (                               );

        int                  getActiveValue   (                               ) const;
        std::string          getActiveValueStr(                               ) const;
        virtual bool         setActiveValue   ( const std::string & str       );

        size_t               getValueCount    (                               ) const;
        std::string          getValue         ( const size_t & index          ) const;

        std::string          getName          (                               ) const;

        void                 _setRenderer     ( Ogre::RenderSystem * renderer );


        /**
          This method is responsible to select a default value for the option.
          The value shall be selected from the parsed values.
          @return True if a default value could be set, false otherwise
        */
        virtual bool setDefaultValue() = 0;

        /**
          Checks whether the given option name is supported for parsing and use in
          renderer. Shall be implemented by the child class.
          @param option The option name
          @return True if the option is supported, false otherwise
        */
        virtual bool supportsOption( const std::string & option ) const = 0;

        /**
          Parses configuration related to the option. List of possible values for
          the option is given by a StringArray.
          @param values The option's values to be parsed and used.
          @return Number of errors occured in parse process
        */
        virtual int parseOption( const StringArray & values ) = 0;

        /**
          Applys the options in the renderer.
        */
        virtual void configureRenderer() = 0;


        /*!< A dynamic array of OptionBases*/
        typedef std::vector<OptionBase*> Array;
    };



    /*! This class is base for all classes who shall implement a renderer configurator.
        This class provides several needed methods for using rendering subsystems,
        including opening and closing render windows, starting up rendering subsystems
        and saving and loading their properties to user config files.
    */
    class _DLLExportControl RendererBase : public Ogre::WindowEventListener,
                                           public Config::Configurable
    {
      protected:
        Ogre::RenderSystem * mRenderer;        /*!< RenderSystem's instance */
        Ogre::RenderWindow * mRenderWindow;    /*!< RenderWindow instance created by ther renderer */
        RendererType         mType;            /*!< Type of the renderer */
        OptionBase::Array    mOptions;         /*!< All renderer options */
        Logger             * mLog;             /*!< Logger instance to write logs */

        // registered events
        EventID              EV_OPENNED;       /*!< EventID registered for emitting window openned events */
        EventID              EV_CLOSING;       /*!< EventID registered for emitting window closing events */
        EventID              EV_CLOSED;        /*!< EventID registered for emitting window closed events */
        EventID              EV_MOVED;         /*!< EventID registered for emitting window moved events */
        EventID              EV_RESIZED;       /*!< EventID registered for emitting window resized events */
        EventID              EV_LOST_FOCUS;    /*!< EventID registered for emitting window losing focus events */
        EventID              EV_GOT_FOCUS;     /*!< EventID registered for emitting window getting focus events */


        void                 clear             (                                 );

        // implementing Ogre::WindowListener event methods
        virtual void         windowOpenned     ( Ogre::RenderWindow * window     );
        virtual bool         windowClosing     ( Ogre::RenderWindow * window     );
        virtual void         windowClosed      ( Ogre::RenderWindow * window     );
        virtual void         windowMoved       ( Ogre::RenderWindow * window     );
        virtual void         windowResized     ( Ogre::RenderWindow * window     );
        virtual void         windowFocusChanged( Ogre::RenderWindow * window     );

        // read and write methods from binary streams
        virtual bool         _readConfig       ( const Config::ConfigType & type );
        virtual void         _writeConfig      ( const Config::ConfigType & type );

        /**
          This method checks whether the class can be used for the given ogre
          renderer system name or not
          @param name Name of the ogre renderer
          @return True if the class supports the given ogre renderer name, false otherwise
        */
        virtual bool         supportsType      ( const std::string & name        ) const = 0;

      public:
                             RendererBase      (                                 );
                           ~ RendererBase      (                                 );

        int                  initialize        (                                 );

        bool                 activate          (                                 );
        bool                 deactivate        (                                 );

        size_t               getOptionCount    (                                 ) const;
        OptionBase         * getOption         ( const std::string & strOption   );
        const OptionBase   * getOption         ( const std::string & strOption   ) const;
        std::string          getOptionValue    ( const std::string & strOption   ) const;
        OptionBase         * getOption         ( const size_t & index            );
        const OptionBase   * getOption         ( const size_t & index            ) const;
        bool                 setOption         ( const std::string & strOption,
                                                 const std::string & strValue    );

        RendererType         getType           (                                 ) const;
        std::string          getName           (                                 ) const;
        Klaus::Rect          getWindowExtents  (                                 ) const;
        void                 getMaxResolution  ( int & width, int & height       ) const;

        Ogre::RenderWindow * getRenderWindow   (                                 );
        Ogre::RenderSystem * getRenderSystem   (                                 );

        static std::string   typeToString      ( const RendererType & type       );

        typedef std::vector<RendererBase*> List; /*!< A dynamic array of RendererBases */
    };

  }; // end namespace Graphics

}; // end namespace Klaus


#endif // RENDERER_BASE_H
