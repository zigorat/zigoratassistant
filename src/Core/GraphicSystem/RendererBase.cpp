/*
*********************************************************************************
*          RendererBase.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 29/01/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains all configurations for graphic system. Provides *
*            methods of realtime changing of graphical configurations.          *
*                                                                               *
*********************************************************************************
*/

/*! \file RendererBase.cpp
<pre>
<b>File:</b>          RendererBase.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       29/01/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains all configurations for graphic system. Provides
               methods of realtime changing of graphical configurations.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
29/01/2011       Mahdi           Initial version created
</pre>
*/


#include "GraphicConfig.h"
#include <libZigorat/ApplicationSettings.h> // needed for ApplicationSettings
#include <ConfigSystem/ConfigSystem.h>      // needed for ConfigSystem
#include <EventSystem/EventSystem.h>        // needed to get EventIDs


namespace Klaus
{

namespace Graphics
{




/***********************************************************************/
/*************************  Class OptionBase  **************************/
/***********************************************************************/

/**
  Constructor of OptionBase. Sets the casual name of the option and
  sets it to be uninitialized.
*/
OptionBase::OptionBase( const std::string & strName)
{
  mLog = Logger::getSingleton();
  mName = strName;
  mActiveValue = -1;
  mRenderer = NULL;
}

/**
  Destructor of the OptionBase. Removes all option values.
*/
OptionBase::~OptionBase()
{
  clear();
}

/**
  This method removes all possible values for the option and sets it to be uninitialized.
*/
void OptionBase::clear()
{
  mValues.clear();
  mActiveValue = -1;
}

/**
  This method returns the index of the active value.
  @return Index of the active option, -1 means there is no active values.
*/
int OptionBase::getActiveValue() const
{
  return mActiveValue;
}

/**
  This method returns the active value.
  @return The active option, Empty std::string if no value was active
*/
std::string OptionBase::getActiveValueStr() const
{
  if(mActiveValue == -1)
    return "";
  else
    return mValues[mActiveValue];
}

/**
  This method sets the active value's index according to the given std::string.
  @param str New value of the option
  @return True if the selected value existed in the list of available values, false otherwise
*/
bool OptionBase::setActiveValue(const std::string& str)
{
  for(size_t i = 0; i < mValues.size(); i++)
    if(mValues[i] == str)
    {
      mActiveValue = i;
      configureRenderer();
      return true;
    }

  Logger::getSingleton()->log(25, "(Optionbase) %s: given value does not exist in supported values: %s", mName.c_str(), str.c_str());
  return false;
}

/**
  This method returns count of the possible values for the option.
  @return Number of available possible values
*/
size_t OptionBase::getValueCount() const
{
  return mValues.size();
}

/**
  This method returns the possible value on the list of values at the given index
  @param index Index of the desired value
  @return The value at the given index
*/
std::string OptionBase::getValue(const size_t& index) const
{
  return mValues[index];
}

/**
  This method returns casual name of the option.
  @return Casual name of the option
*/
std::string OptionBase::getName() const
{
  return mName;
}

/**
  This method sets the ogre renderer instance. It is called automatically by RendererBase.
  @param renderer Ogre renderer instance
*/
void OptionBase::_setRenderer(Ogre::RenderSystem* renderer)
{
  mRenderer = renderer;
}





/***********************************************************************/
/************************  Class RendererBase  *************************/
/***********************************************************************/

/**
  Constructor of RendererBase. Sets logger instance and nullifies renderer
  and window instances, also initializes event identifiers.
*/
RendererBase::RendererBase()
{
  mLog = Logger::getSingleton();
  mRenderer = NULL;
  mRenderWindow = NULL;
  Events::EventSystem * sys = Events::EventSystem::getSingleton();

  // initialize event identifiers
  EV_OPENNED    = sys->registerEvent("MainWindow\\Openned");
  EV_CLOSING    = sys->registerEvent("MainWindow\\Closing");
  EV_CLOSED     = sys->registerEvent("MainWindow\\Closed");
  EV_MOVED      = sys->registerEvent("MainWindow\\Moved");
  EV_RESIZED    = sys->registerEvent("MainWindow\\Resized");
  EV_LOST_FOCUS = sys->registerEvent("MainWindow\\LostFocus");
  EV_GOT_FOCUS  = sys->registerEvent("MainWindow\\GotFocus");
}

/**
  Destructor of RendererBase. Removes all options.
*/
RendererBase::~RendererBase()
{
  clear();
}

/**
  This method removes all options from memory.
*/
void RendererBase::clear()
{
  for(size_t i = 0; i < mOptions.size(); i++)
    delete mOptions[i];

  mOptions.clear();
}

/**
  This method is called whenever the RendererBase has created a new render window
  @param window The render window which sent the event
*/
void RendererBase::windowOpenned(Ogre::RenderWindow* window)
{
  int l, t;
  unsigned w, h, c;
  window->getMetrics(w, h, c, l, t);

  Events::Event ev(EV_OPENNED);
  ev.mArguments.addVar("WindowName", window->getName());
  ev.mArguments.addVar("Left", l);
  ev.mArguments.addVar("Top", t);
  ev.mArguments.addVar("Width", w);
  ev.mArguments.addVar("Height", h);
  ev.broadcast();
}

/**
  This method is called whenever user has clicked on the render window's close button
  @param window The render window which sent the event
  @return The return value will indicate whether the window can be closed or not
*/
bool RendererBase::windowClosing(Ogre::RenderWindow* window)
{
  Events::Event ev(EV_CLOSING);
  ev.mArguments.addVar("WindowName", window->getName());
  ev.broadcast();
  return true;
}

/**
  This method is called whenever the window has been closed
  @param window The render window which sent the event
*/
void RendererBase::windowClosed(Ogre::RenderWindow* window)
{
  Events::Event ev(EV_CLOSED);
  ev.mArguments.addVar("WindowName", window->getName());
  ev.broadcast();
}

/**
  This method is called whenever the render window has been moved
  @param window The render window which sent the event
*/
void RendererBase::windowMoved(Ogre::RenderWindow* window)
{
  int l, t;
  unsigned w, h, c;
  window->getMetrics(w, h, c, l, t);

  Events::Event ev(EV_MOVED);
  ev.mArguments.addVar("WindowName", window->getName());
  ev.mArguments.addVar("Left", l);
  ev.mArguments.addVar("Top", t);
  ev.broadcast();
}

/**
  This method is called whenever the render window has been resized
  @param window The render window which sent the event
*/
void RendererBase::windowResized(Ogre::RenderWindow* window)
{
  int l, t;
  unsigned w, h, c;
  window->getMetrics(w, h, c, l, t);

  Events::Event ev(EV_RESIZED);
  ev.mArguments.addVar("WindowName", window->getName());
  ev.mArguments.addVar("Width", w);
  ev.mArguments.addVar("Height", h);
  ev.broadcast();
}

/**
  This method is called whenever the render window has been raised to front or lost it's focus.
  @param window The render window which sent the event
*/
void RendererBase::windowFocusChanged(Ogre::RenderWindow* window)
{
  Events::Event ev;
  if(window->isActive())
    ev.setEvent(EV_GOT_FOCUS);
  else
    ev.setEvent(EV_LOST_FOCUS);

  ev.mArguments.addVar("WindowName", window->getName());
  ev.broadcast();
}

/**
  This method loads configuration from a stream.
  @param t The configuration type of the steam
  @return True if successfully read configuration, false otherwise
*/
bool RendererBase::_readConfig(const Config::ConfigType & t)
{
  try
  {
    if(t == Config::BinaryConfig)
    {
      std::string s;
      for(size_t i = 0; i < mOptions.size(); i++)
      {
        s = readString();
        if(!s.empty())
          mOptions[i]->setActiveValue(s);
      }
    }
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      for(size_t i = 1; i < expr.size(); ++i)
      {
        SToken t = expr[i];
        if(!t[2].str().empty())
          setOption(t[1], t[2]);
      }
    }
    else
    {
      mLog->log(25, "(RendererBase) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    Logger::getSingleton()->log(25, "(RendererBase) exception: %s", str);
    return false;
  }

  return true;
}

/**
  This method writes the configuration of the system to the given output stream.
  @param type The configuration type to save the stream
*/
void RendererBase::_writeConfig(const Config::ConfigType & type)
{
  if(type == Config::BinaryConfig)
  {
    for(size_t i = 0; i < mOptions.size(); ++i)
      writeString(mOptions[i]->getActiveValueStr());
  }
  else if(type == Config::SExpConfig)
  {
    for(size_t i = 0; i < mOptions.size(); ++i)
      if(!mOptions[i]->getName().empty())
        writeText("(setConfig '%s' '%s')", mOptions[i]->getName().c_str(), mOptions[i]->getActiveValueStr().c_str());
  }
  else
    mLog->log(25, "(RendererBase) write: configuration type %d not supported", type);
}

/**
  This method initializes the renderer subsystem. First the renderer subsystem's instance
  loaded within ogre is searched. Then with the found instance, all renderer configurations
  are read. Their default values will be set upon reading their possible values. The system
  is then marked as initialized.
  @return Number of errors occured
*/
int RendererBase::initialize()
{
  // get ogre root instance
  Ogre::Root * root = Ogre::Root::getSingletonPtr();
  if(!root)
  {
    mLog->log(25, "(RendererBase) ogre not initialized");
    return 1;
  }

  mLog->log(25, "(RendererBase) initializing %s renderer", getName().c_str());

  // find the renderer subsystem
  #if OGRE_VERSION_MAJOR == 1 && OGRE_VERSION_MINOR < 7
  Ogre::RenderSystemList *renderers = root->getAvailableRenderers();
  for(Ogre::RenderSystemList::iterator rs = renderers->begin(); rs != renderers->end (); rs++)
  #else
  const Ogre::RenderSystemList & renderers = root->getAvailableRenderers();
  for(Ogre::RenderSystemList::const_iterator rs = renderers.begin(); rs != renderers.end (); rs++)
  #endif
  {
    if(supportsType((**rs).getName()))
    {
      mRenderer = *rs;
      break;
    }
  }

  if(!mRenderer)
  {
    mLog->log(25, "(RendererBase) render system %s is not installed", typeToString(mType).c_str());
    return 1;
  }

  // now load the render system's sane values
  Ogre::ConfigOptionMap options = mRenderer->getConfigOptions();
  int iErrors = 0;

  // iterate over all available configurations for the render system
  for(Ogre::ConfigOptionMap::iterator it = options.begin(); it != options.end(); it++)
  {
    bool bParsed = false;
    StringArray list;
    Ogre::StringVector::iterator sit;
    for(sit = it->second.possibleValues.begin(); sit != it->second.possibleValues.end(); sit++)
      list.push_back(*sit);

    for(size_t i = 0; i < mOptions.size(); i++)
      if(mOptions[i]->supportsOption(it->first))
      {
        iErrors += mOptions[i]->parseOption(list);
        bParsed = true;
        break;
      }

    if(!bParsed)
      mLog->log(25, "(RendererBase) %s: unknown option '%s'", getName().c_str(), it->first.c_str());
  }

  // now set all options renderer instance
  for(size_t i = 0; i < mOptions.size(); i++)
  {
    mOptions[i]->_setRenderer(mRenderer);
    mOptions[i]->setDefaultValue();
  }

  // load configuration from ConfigSystem
  Config::ConfigSystem::getSingleton()->injectConfigToSys(_getSystemName());

  // return number of errors
  return iErrors;
}

/**
  This method activates the renderer subsystem. First all values for configurations
  will be set on the renderer itself and then the renderer will be set as the render
  system for ogre. Upon success the renderer will be used to create a render window.
  @return True if the renderer activated without failure, false otherwise
*/
bool RendererBase::activate()
{
  if(!mRenderer)
  {
    mLog->log(25, "(RendererBase) %s: not initialized", getName().c_str());
    return false;
  }

  // setup render system configuration
  for(size_t i = 0; i < mOptions.size(); i++)
    mOptions[i]->configureRenderer();

  // set the ogre render system
  Ogre::Root * root = Ogre::Root::getSingletonPtr();
  root->setRenderSystem(mRenderer);

  // initialize ogre if needed
  if(!(root->isInitialised()))
    root->initialise(false);

  // create output window
  std::string strAppName = ApplicationSettings::getSingleton()->getApplicationName();
  mRenderWindow = root->createRenderWindow(strAppName, 640, 480, false);
  mRenderWindow->setDeactivateOnFocusChange(true);
  Config::ConfigSystem::getSingleton()->injectConfigToSys(_getSystemName());

  // register as a Window listener
  Ogre::WindowEventUtilities::addWindowEventListener(mRenderWindow, this);

  // emit openned and resized signal to update all dependant systems
  windowOpenned(mRenderWindow);
  windowResized(mRenderWindow);

  mLog->log(25, "(RendererBase) activated %s renderer", getName().c_str());
  return true;
}

/**
  This method deactivates the renderer. It closes the render window created by
  the renderer and then will set the ogre renderer to NULL.
  @return True if the the renderer was deactivated successfully, false otherwise
*/
bool RendererBase::deactivate()
{
  if(!mRenderWindow || !mRenderer)
  {
    mLog->log(25, "(RendererBase) %s: not activate, can not deactivate", getName().c_str());
    return false;
  }
  else
  {
    // unregister window event listener
    Ogre::WindowEventUtilities::removeWindowEventListener(mRenderWindow, this);

    // no need to deactivate DisplaySystem and InputSystem here.
    // They will be deactivated when close event is broadcasted
    windowClosed(mRenderWindow);

    // detach and destroy the render window
    // TODO: fix here to enable switching render systems at run time
    Ogre::Root * root = Ogre::Root::getSingletonPtr();
    mRenderer->destroyRenderWindow(mRenderWindow->getName());
    root->destroyAllRenderQueueInvocationSequences();
    root->setRenderSystem(NULL);
    mRenderWindow = NULL;

    mLog->log(25, "(RendererBase) deactivated %s", getName().c_str());
    return true;
  }
}

/**
  This method returns number of available options.
  @return Number of available options
*/
size_t RendererBase::getOptionCount() const
{
  return mOptions.size();
}

/**
  This method returns the requested option's instance
  @param strOption The option name to get it's instance
  @return The requested option value
*/
OptionBase* RendererBase::getOption(const std::string& strOption)
{
  for(size_t i = 0; i < mOptions.size(); i++)
    if(mOptions[i]->getName() == strOption)
      return mOptions[i];

  return NULL;
}

/**
  This method returns the requested option's instance
  @param strOption The option name to get it's instance
  @return The requested option value
*/
const Klaus::Graphics::OptionBase* RendererBase::getOption(const std::string& strOption) const
{
  for(size_t i = 0; i < mOptions.size(); i++)
    if(mOptions[i]->getName() == strOption)
      return mOptions[i];

  return NULL;
}

/**
  This method returns the value for the given option name.
  @param strOption Name of the option
  @return Value for the option. If the option was not found, an empty std::string will be returned
*/
std::string RendererBase::getOptionValue(const std::string& strOption) const
{
  for(size_t i = 0; i < mOptions.size(); i++)
    if(mOptions[i]->getName() == strOption)
      return mOptions[i]->getActiveValueStr();

  return "";
}

/**
  This method returns the requested option's instance
  @param index The option index to get it's instance
  @return The requested option value
*/
OptionBase* RendererBase::getOption(const size_t& index)
{
  return mOptions[index];
}

/**
  This method returns the requested option's instance
  @param index The option index to get it's instance
  @return The requested option value
*/
const Klaus::Graphics::OptionBase* RendererBase::getOption(const size_t& index) const
{
  return mOptions[index];
}

/**
  This method finds the requested options and sets its value
  @param strOption Option name to set it's value
  @param strValue Value to set for the option
*/
bool RendererBase::setOption(const std::string& strOption, const std::string& strValue)
{
  OptionBase* o = getOption(strOption);
  if(!o)
  {
    mLog->log(25, "(RendererBase) no such option in %s : %s", getName().c_str(), strOption.c_str());
    return false;
  }

  o->setActiveValue(strValue);
  return true;
}

/**
  This method returns the type of renderer, ie OpenGL, ...
  @return Type of the renderer
*/
RendererType RendererBase::getType() const
{
  return mType;
}

/**
  This method returns the name of the renderer.
  @return Name of the renderer
*/
std::string RendererBase::getName() const
{
  return typeToString(mType);
}

/**
  This method returns the extents of the current active render window
  @return The position and size of the render window
*/
Rect RendererBase::getWindowExtents() const
{
  unsigned w = 100, h = 100, c;
  int l = 0, t = 0;

  if(mRenderWindow)
    mRenderWindow->getMetrics(w, h, c, l ,t);
  else
    mLog->log(25, "(RendererBase) no window to get it's extents");

  return Klaus::Rect(VecPosition(l, t), VecPosition(l+w, t+h));
}

/**
  This method returns the maximum resolution that a renderer can support.
  This resolution is usually the optimum resolution for the output device
  and shows the output device's original aspect ratio.
  @param width The maximum supported width of the output device
  @param height The maximum supported height of the output device
*/
void RendererBase::getMaxResolution(int& width, int& height) const
{
  width = 640;
  height = 480;

  const OptionBase * opt = getOption("Resolution");
  if(!opt)
  {
    mLog->log(25, "(RendererBase) resolution option not found!");
    return;
  }

  int w, h;
  std::string str;
  for(size_t i = 0; i < opt->getValueCount(); i++)
  {
    // parse each resolution
    str = opt->getValue(i);

    int index = 0;
    int size = (int)str.size();

    // parse first number (length of window)
    if(str.empty())
      return;

    Parse::parseFirstInt(str, w, index);
    for(; (index < size) && (str[index] != 'x'); ++index)
      ; // do nothing
    ++index;

    // parse second number (width of window)
    if(index >= size)
      return;

    Parse::parseFirstInt(str, h, index);
    if(w > width && h > height)
    {
      width = w;
      height = h;
    }
  }
}

/**
  This method returns the render window's instance
  @return Render window instance
*/
Ogre::RenderWindow* RendererBase::getRenderWindow()
{
  return mRenderWindow;
}

/**
  This method returns the render system's instance
*/
Ogre::RenderSystem* RendererBase::getRenderSystem()
{
  return mRenderer;
}

/**
  This method converts a given renderer type to a human understanble std::string name
  @param type Type of the renderer
  @return Name of the renderer
*/
std::string RendererBase::typeToString(const Klaus::Graphics::RendererType& type)
{
  if(type == OpenGL)
    return "OpenGL";
  else if(type == DirectX9)
    return "DirectX9";
  else if(type == DirectX10)
    return "DirectX10";
  else
    return "UnknownRenderer";
}


}; // end namespace Graphics


}; // end namespace Klaus
