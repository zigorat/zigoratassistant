/*
*********************************************************************************
*          LightSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 07/09/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains implementations of loading and using lights and light     *
*            systems. storage class types for visual stuff like                 *
*                                                                               *
*********************************************************************************
*/

/*! \file LightSystem.cpp
<pre>
<b>File:</b>          LightSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/09/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains implementations of loading and using lights and light systems.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/09/2008       Mahdi           Initial version created
</pre>
*/

#include "LightSystem.h"
#include <libZigorat/Logger.h>  // needed for Logger
#include "GraphicSystem.h"      // needed to get scene manager instance


/** LightingSystem singleton instance */
template<> Klaus::Graphics::LightingSystem* Klaus::Singleton<Klaus::Graphics::LightingSystem>::mInstance = 0;




/*****************************************************************************/
/****************************   Class Light   ********************************/
/*****************************************************************************/

/**
  Constructor of the Light. Initializes light and scene node instance
*/
Klaus::Graphics::Light::Light()
{
  mLight     = NULL;
  mSceneNode = NULL;
  mSceneMgr  = GraphicSystem::getSingleton()->getSceneManager();
}

/**
  Destructor of the light
*/
Klaus::Graphics::Light::~Light()
{
  if(!mSceneNode)
    return;

  Logger::getSingleton()->log(15, "(Light) removing light %s", mSceneNode->getName().c_str());
  mSceneNode->detachObject(mLight);
  mSceneMgr->destroyLight(mLight);
  mSceneMgr->getRootSceneNode()->removeAndDestroyChild(mSceneNode->getName());
}

/**
  This method returns the diffuse colour of  the light. Diffuse light simulates
  the typical light emenating from light sources and affects the base colour
  of objects together with ambient light.
  @return Diffuse colour of the light
*/
Ogre::ColourValue Klaus::Graphics::Light::getDiffuseColor() const
{
  if(mLight)
    return mLight->getDiffuseColour();
  else
    return Ogre::ColourValue( 0, 0, 0, 0 );
}

/**
  This method returns the diffuse colour of the light. Diffuse light simulates
   the typical light emenating from light sources and affects the base colour
   of objects together with ambient light.
  @param diff Diffuse colour of the light
*/
void Klaus::Graphics::Light::setDiffuseColor( Ogre::ColourValue diff )
{
  if(mLight)
    mLight->setDiffuseColour( diff );
}

/**
  This method returns the specular color of the light. Specular light affects
  the appearance of shiny highlights on objects, and is also dependent on the
  'shininess' value.
  @return Specular color of the light
*/
Ogre::ColourValue Klaus::Graphics::Light::getSpecularColor() const
{
  if(mLight)
    return mLight->getSpecularColour();
  else
    return Ogre::ColourValue( 1, 1, 1, 1 );
}

/**
  This method sets the specular value of the light
  @param spec New specular value of light
*/
void Klaus::Graphics::Light::setSpecularColor( Ogre::ColourValue spec )
{
  if(mLight)
    mLight->setSpecularColour(spec);
}

/**
  This method returns the name of the light
  @return Light name
*/
std::string Klaus::Graphics::Light::getName() const
{
  if(mLight)
    return mLight->getName();
  else
    return "";
}

/**
  This method return the position of the light
  @return Position of the light
*/
Klaus::VecPosition Klaus::Graphics::Light::getPosition()
{
  if(!mSceneNode)
    return VecPosition::Unknown;

  Ogre::Vector3 pos = mSceneNode->getPosition();
  return VecPosition(pos.x, pos.y, pos.z);
}

/**
  This method sets the position of the light in the scene.
  @param pos New position of the light
*/
void Klaus::Graphics::Light::setPosition( VecPosition pos )
{
  if(mSceneNode)
    mSceneNode->setPosition(Ogre::Vector3((Ogre::Real)pos[0], (Ogre::Real)pos[1], (Ogre::Real)pos[2]));
}

/**
  This method parses an addLight command
  This command add a light source to the scene. Light source have
  different types and attributes:
  - Directional:
    like it's coming from a very far distance, like that of sun and
    moon. They have no position, cause they come from infinity
  - Point light
    A light source in which comes from a point to every direction,
    like that of a lamp. It has position and color value.
  @param token The token containing the addLight command
  @return True on successfull parse and initialization, false otherwise
*/
bool Klaus::Graphics::Light::parse(SToken token)
{
  if(token[1] == "DirectionalLight" && token.size() == 11)
  {
    VecPosition dirLight(token[2].number(), token[3].number(), token[4].number());
    dirLight.normalize();

    double dr = token[5].number();
    double dg = token[6].number();
    double db = token[7].number();

    double sr = token[8].number();
    double sg = token[9].number();
    double sb = token[10].number();

    mSceneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
    mLight = mSceneMgr->createLight(mSceneNode->getName() + "_light");
    mLight->setType(Ogre::Light::LT_DIRECTIONAL);
    mLight->setDiffuseColour(Ogre::ColourValue((Ogre::Real)dr, (Ogre::Real)dg, (Ogre::Real)db));
    mLight->setSpecularColour(Ogre::ColourValue((Ogre::Real)sr, (Ogre::Real)sg, (Ogre::Real)sb));
    mLight->setVisible(false);
    mSceneNode->attachObject(mLight);
  }
  else if(token[1] == "PointLight" && token.size() == 11)
  {
    VecPosition posLight(token[2].number(), token[3].number(), token[4].number());

    double dr = token[5].number();
    double dg = token[6].number();
    double db = token[7].number();

    double sr = token[8].number();
    double sg = token[9].number();
    double sb = token[10].number();

    mSceneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(
                    Ogre::Vector3((Ogre::Real)posLight[0], (Ogre::Real)posLight[1], (Ogre::Real)posLight[2]));
    mLight = mSceneMgr->createLight(mSceneNode->getName() + "_light");
    mLight->setType(Ogre::Light::LT_POINT);
    mLight->setDiffuseColour(Ogre::ColourValue((Ogre::Real)dr, (Ogre::Real)dg, (Ogre::Real)db));
    mLight->setSpecularColour(Ogre::ColourValue((Ogre::Real)sr, (Ogre::Real)sg, (Ogre::Real)sb));
    mLight->setVisible(false);
    mSceneNode->attachObject(mLight);
  }
  else
  {
    Logger::getSingleton()->log(15, "(Light) unknown light: '%s'", token.str().c_str());
    return false;
  }

  return true;
}

/**
  This method activate a light source, so that it will lit the scene
*/
void Klaus::Graphics::Light::activate()
{
  if(mLight)
    mLight->setVisible( true );
}

/**
  This method deactivates the light source, so that it would not lit the scene anymore
*/
void Klaus::Graphics::Light::deactivate()
{
  if(mLight)
    mLight->setVisible( false );
}



/*****************************************************************************/
/****************************   Class Lighting   *****************************/
/*****************************************************************************/

/**
  This is the constructor of the lighting. Initializes the SceneManager
*/
Klaus::Graphics::Lighting::Lighting()
{
  mSceneMgr = GraphicSystem::getSingleton()->getSceneManager();
}

/**
  This is destructor of Lighting. Destroys all the lights created.
*/
Klaus::Graphics::Lighting::~Lighting()
{
  clear();
}

/**
  This method destorys and clears the lights in lighting
*/
void Klaus::Graphics::Lighting::clear()
{
  Logger::getSingleton()->log(15, "(Lighting) removing lighting %s", mName.c_str());
  for(size_t i = 0; i < mLights.size(); i++)
    delete mLights[i];

  mLights.clear();
}

/**
  This method returns the name of the lighting
  @return name of the lighting
*/
std::string Klaus::Graphics::Lighting::getName() const
{
  return mName;
}

/**
  This method parses a lighting scene node. The node contains information
  about ambient light and all other lights in a scene. Ambient light is the
  light which is created with no light source, and seems to come from every
  direction. Light sources have different types and attributes:<br>
  - Directional:
    like it's coming from a very far distance, like that of sun and
    moon. They have no position, cause they come from inifinity
  - Point:
    A typical light source from a point light bulb
  @param token The token containing the light scene
  @return True on successfull parse, false otherwise
*/
bool Klaus::Graphics::Lighting::parse( SToken token )
{
  mName = token[1].str();

  for(size_t i = 2; i < token.size(); i++)
  {
    if(token[i].isAtom() || token[i].size() < 3)
    {
      Logger::getSingleton()->log(15, "(Lighting) unknown light node: '%s'", token[i].str().c_str());
      return false;
    }

    if(token[i].size() == 4 || token[i][0] == "setAmbient")
    {
      double r = token[i][1];
      double g = token[i][2];
      double b = token[i][3];

      mAmbient = Ogre::ColourValue((Ogre::Real)r, (Ogre::Real)g, (Ogre::Real)b, 1.0f);
      Logger::getSingleton()->log(15, "(Lighting) added ambient light");
    }
    else if(token[i][0] == "addLight")
    {
      Light * light = new Light;
      if(light->parse(token[i].str()))
      {
        mLights.push_back(light);
        Logger::getSingleton()->log(15, "(Lighting) added light '%s'", light->getName().c_str());
      }
      else
      {
        delete light;
        Logger::getSingleton()->log(15, "(Lighting) error. skipping one light from %s", mName.c_str());
        return false;
      }
    }
    else
    {
      Logger::getSingleton()->log(15, "(Lighting) unknown light node: '%s'", token[i].str().c_str());
      return false;
    }
  }

  return true;
}

/**
  This method actiaves the lighting, meaning that all the lights in this
  lighting effect would be activated
*/
void Klaus::Graphics::Lighting::activate()
{
  Logger::getSingleton()->log(15, "(Lighting) activating lighting set %s", mName.c_str());
  mSceneMgr->setAmbientLight(mAmbient);

  for(size_t i = 0; i < mLights.size(); i++)
    mLights[i]->activate();
}

/**
  This method deactivates the lighting effect by turning off all the lights
*/
void Klaus::Graphics::Lighting::deactivate()
{
  Logger::getSingleton()->log(15, "(Lighting) deactivating lighting set %s", mName.c_str());
  for(size_t i = 0; i < mLights.size(); i++)
    mLights[i]->deactivate();

  mSceneMgr->setAmbientLight(Ogre::ColourValue(0, 0, 0));
}




/*****************************************************************************/
/*************************   Class LightingSystem    *************************/
/*****************************************************************************/

/**
  This is the constructor of LightingSystem. Initializes SceneManager
*/
Klaus::Graphics::LightingSystem::LightingSystem()
{
  mActiveLighting = -1;
  EV_NEXT_LIGHTING = registerEvent("LightingSystem\\NextLighting");

  setRecipientName("LightingSystem");

  using namespace Input;
  registerBinding(KeyCombination(OnKeyPress, KC_L), EV_NEXT_LIGHTING);
}

/**
  This is the destructor of LightingSystem. It destorys all the
  lightings and frees up the memory.
*/
Klaus::Graphics::LightingSystem::~LightingSystem()
{
  removeAllLights();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Graphics::LightingSystem * Klaus::Graphics::LightingSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new LightingSystem;
    Logger::getSingleton()->log(1, "(LightingSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Graphics::LightingSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(LightingSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(LightingSystem) removed singleton");
  }
}

/**
  This method clears out all the lightings and destroys them.
*/
void Klaus::Graphics::LightingSystem::removeAllLights()
{
  Logger::getSingleton()->log(15, "(LightingSystem) removing all lights" );

  for(size_t i = 0; i < mLightings.size(); i++)
    delete mLightings[i];

  mLightings.clear();
  mActiveLighting = -1;
}

/**
  This method returns number of current lighting setups present in app
  @return Number of Lighting setups
*/
size_t Klaus::Graphics::LightingSystem::getLightingCount()
{
  return mLightings.size();
}

/**
  This method activates a lighting setup by index
  @param iLighting Index of the lighting setup
  @return True if lighting setup index's exists, false otherwise
*/
bool Klaus::Graphics::LightingSystem::activateLighting(size_t iLighting)
{
  if(iLighting >= mLightings.size())
  {
    Logger::getSingleton()->log(15, "(LightingSystem) number out of range: %d (%d)", iLighting, mLightings.size());
    return false;
  }

  // First deactivate previous setup
  if( mActiveLighting != -1 )
    mLightings[mActiveLighting]->deactivate();

  // Then activate selected lighting effect
  mLightings[iLighting]->activate();
  mActiveLighting = iLighting;

  return true;
}

/**
  This method selects active lighting effect, so that scene will
  be lit with the all of the desired lights
  @param name name of the lighting effect
  @return True on successfull activating, false otherwise
*/
bool Klaus::Graphics::LightingSystem::activateLighting( const std::string & name)
{
  Lighting *lighting     = NULL;
  int       iNewLighting = -1;

  for(size_t i = 0; i < mLightings.size(); i++)
    if(mLightings[i]->getName() == name)
    {
      lighting = mLightings[i];
      iNewLighting = i;
      break;
    }

  if( !lighting )
  {
    Logger::getSingleton()->log(15, "(LightingSystem) lighting not found: '%s'", name.c_str());
    return false;
  }

  if( mActiveLighting != -1 )
    mLightings[mActiveLighting]->deactivate();

  mActiveLighting = iNewLighting;
  lighting->activate();

  return true;
}

/**
  This method selects the next lighting setup and activates it
  @return True if any setup could be activated, false otherwise
*/
bool Klaus::Graphics::LightingSystem::activateNextLighting()
{
  if( mLightings.size() == 0 )
  {
    Logger::getSingleton()->log(15, "(LightingSystem) no lightings found");
    return false;
  }

  if( mActiveLighting != -1 )
    mLightings[mActiveLighting]->deactivate();
  mActiveLighting++;

  if( mActiveLighting >= (int)mLightings.size() )
    mActiveLighting = 0;

  mLightings[mActiveLighting]->activate();

  return true;
}

/**
  This method parses and loads lightings from a config file and activates
  default lighting effect
  NOTE: This will not clear lights first. Adds all the resources here to the previous pile.
  @param token The token containing the scene graph representation
  @return Number of errors occured
*/
bool Klaus::Graphics::LightingSystem::parse( SToken token )
{
  if(token.size() < 3)
  {
    Logger::getSingleton()->log(15, "(LightingSystem) insufficient data in '%s'", token.str().c_str());
    return false;
  }

  mDefaultLightName = token[1].str();
  for(size_t i = 2; i < token.size(); i++)
  {
    if(token[i].isAtom() || token[i].size() < 3)
    {
      Logger::getSingleton()->log(15, "(LightingSystem) illegal token parsed: '%s'", token[i].str().c_str());
      return false;
    }

    if(token[i][0] == "Lighting")
    {
      Lighting * lighting = new Lighting;
      if(lighting->parse(token[i]))
      {
        mLightings.push_back(lighting);
        Logger::getSingleton()->log(15, "(LightingSystem) added lighting set '%s'", lighting->getName().c_str());
      }
      else
      {
        delete lighting;
        Logger::getSingleton()->log(15, "(LightingSystem) skipping one lighting");
      }
    }
    else
      Logger::getSingleton()->log(15, "(LightingSystem) unknown token: '%s'", token[i][0].str().c_str());
  }

  return true;
}

/**
  This method loads the file with the given name.
  @param name name of the file
  @return True on successful parse, false otherwise
*/
bool Klaus::Graphics::LightingSystem::parse(const std::string& name)
{
  try
  {
    // 1024 Kbs seems fairly enough for a file containing light nodes
    SExpression file(name, 12*1024);

    if(file.getFirstNonComment() == -1)
    {
      Logger::getSingleton()->log(15, "(LightingSystem) no lighting in file '%s'", name.c_str());
      return false;
    }

    return parse(file[file.getFirstNonComment()][0]);
  }
  catch(SLangException e)
  {
    Logger::getSingleton()->log(15, "(LightingSystem) error on loading file '%s'", name.c_str());
    Logger::getSingleton()->log(15, "%s", e.str());
    return false;
  }
}

/**
  This method processes the event sent for LightingSystem.
  @param ev The event to process
*/
void Klaus::Graphics::LightingSystem::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_NEXT_LIGHTING)
    activateNextLighting();
  else
    Logger::getSingleton()->log(15, "(LightingSystem) unknown event %d", ev.mEventID);
}
