/*
*********************************************************************************
*           EventSystem.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Central system which coordinates all events by all systems.        *
*                                                                               *
*********************************************************************************
*/

/*! \file EventSystem.h
<pre>
<b>File:</b>          EventSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Central system which coordinates all events by all systems.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef EVENT_SYSTEM
#define EVENT_SYSTEM


#include <libZigorat/Logger.h>         // needed for Logger
#include "EventRecipient.h"


namespace Klaus
{

  namespace Events
  {

    /*! This class is the center for all events and their registered recipients
        in all application. All classes who want to receive events shall register
        to this system.
    */
    class _DLLExportControl EventSystem : public Singleton<EventSystem>
    {
      private:
        /*! Mapping between event names and their ids */
        typedef std::map<std::string, EventID> EventIDMapping;

        /*! Mapping between event ids and event recipients */
        typedef std::map<EventID, EventRecipient::List> RecipientMapping;


        RecipientMapping    mEventMaps;        /*!< Event mappings     */
        EventIDMapping      mEventNames;       /*!< Event name and id  */
        Logger            * mLog;              /*!< Logger instance    */
        EventID             mMaxID;            /*!< cur max id number  */

                            EventSystem        (                       );
                          ~ EventSystem        (                       );

      public:
        static EventSystem* getSingleton       (                       );
        static void         freeSingleton      (                       );

        EventID             registerEvent      ( const char * strEvent );
        EventID             registerRecipient  ( const char * strEvent,
                                                 EventRecipient * rec  );
        bool                registerRecipient  ( const EventID & id,
                                                 EventRecipient * rec  );
        bool                unregisterRecipient( const EventID & id,
                                                 EventRecipient * rec  );
        bool                broadcastEvent     ( const Event & ev      );

        std::string         getEventName       ( const EventID & id    ) const;
        EventID             getEventID         ( const char * strEvent ) const;
    };

  };  // end namespace Events

}; // end namespace Klaus


#endif // EVENT_SYSTEM
