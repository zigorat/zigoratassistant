/*
*********************************************************************************
*              Event.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A class which holds information of an event.                       *
*                                                                               *
*********************************************************************************
*/

/*! \file Event.h
<pre>
<b>File:</b>          Event.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A class which holds information of an event.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef EVENT_H
#define EVENT_H

#include <CoreSystem/Types.h>   // needed for EventID
#include <libZigorat/VarList.h> // needed for VarList


namespace Klaus
{

  namespace Events
  {

    /*! Class Event holds information for an event. Including the
        event id and the event arguments. */
    class _DLLExportControl Event
    {
      public:
        EventID mEventID;    /*!< Event identifier    */
        VarList mArguments;  /*!< Event arguments     */

                Event        ( const Event & ev       );
                Event        ( const EventID & id = 0 );
                Event        ( const char * strEvent  );
              ~ Event        (                        );

        void    setEvent     ( const EventID & id     );
        void    broadcast    (                        ) const;

        Event & operator =   ( const Event & ev       );
        bool    operator ==  ( const EventID & id     ) const;

        /*! List is a dynamic array of Events */
        typedef std::vector<Event> Array;
    };

  }; // end namespace Events

}; // end namespace Klaus


#endif // EVENT_H
