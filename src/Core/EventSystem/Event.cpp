/*
*********************************************************************************
*            Event.cpp : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A class which holds information of an event.                       *
*                                                                               *
*********************************************************************************
*/

/*! \file Event.cpp
<pre>
<b>File:</b>          Event.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A class which holds information of an event.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#include "EventSystem.h"


/**
  Constructor of Event. Constructs from a given event instance
  @param ev Event instance to copy
*/
Klaus::Events::Event::Event(const Event& ev)
{
  mEventID = ev.mEventID;
  mArguments = ev.mArguments;
}

/**
  Constructor of Event. Constructs from a given event identifier
  @param id The event id to construct the Event class from
*/
Klaus::Events::Event::Event(const EventID & id)
{
  mEventID = id;
}

/**
  Constructor of event with an event name. The event ID will be searched from the event system.
  @param strEvent The event name to initialize the event
*/
Klaus::Events::Event::Event(const char* strEvent)
{
  mEventID = EventSystem::getSingleton()->getEventID(strEvent);
}

/**
  Destructor of Event. Clears the arguments
*/
Klaus::Events::Event::~Event()
{
  mArguments.clear();
}

/**
  This method places the given event identifier into the Event
  class. Also clears the arguments.
  @param id The event to copy
*/
void Klaus::Events::Event::setEvent(const EventID & id)
{
  mEventID = id;
  mArguments.clear();
}

/**
  This method calls the event system to broadcast the event to all of its registered recipients.
*/
void Klaus::Events::Event::broadcast() const
{
  EventSystem::getSingleton()->broadcastEvent(*this);
}

/**
  Overloaded version of assignment operator. Copies the
  given Event instance to the current instance.
  @param ev The event instance to copy from
  @return The event self instance to enable multiple assignments
*/
Klaus::Events::Event& Klaus::Events::Event::operator=(const Events::Event& ev)
{
  mEventID = ev.mEventID;
  mArguments = ev.mArguments;

  return *this;
}

/**
  This overload of operator == is to check a given event is for a specified event id or not.
  @param id The event ID
  @return True if the event is for the specified event ID, false otherwise
*/
bool Klaus::Events::Event::operator==(const Klaus::EventID& id) const
{
  return mEventID == id;
}
