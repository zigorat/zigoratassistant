/*
*********************************************************************************
*         EventRecipient.cpp : Robocup 3D Soccer Simulation Team Zigorat        *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class of all classes who need to receive events.              *
*                                                                               *
*********************************************************************************
*/

/*! \file EventRecipient.cpp
<pre>
<b>File:</b>          EventRecipient.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class of all classes who need to receive events.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/


#include "EventRecipient.h"
#include "EventSystem.h"             // needed for EventSystem
#include <typeinfo>                  // needed for typeid
#include <InputSystem/KeyBindings.h> // needed for KeyBindings


/**
  Destructor of EventRecipient. Will remove all key combination bindings and their
  registered events from the EventSystem.
*/
Klaus::Events::EventRecipient::~EventRecipient()
{
  // unregister all bindings
  // second is the bind_id, first is the event_id
  for(BindingAndEventMap::iterator it = mBindings.begin(); it != mBindings.end(); it++)
    Input::KeyBindings::getSingleton()->unregisterBinding(it->second, this);

  // unregister self from all events
  for(size_t i = 0; i < mRegisteredEvents.size(); i++)
    EventSystem::getSingleton()->unregisterRecipient(mRegisteredEvents[i], this);
}

/**
  This method sets the recipients name. It is used for saving and loading bindings in KeyBindings.
  This method will only set the recipient name once.
  @param str Name of the event recipient system
*/
void Klaus::Events::EventRecipient::setRecipientName(const char* str)
{
  if(mRecipientName.empty())
    mRecipientName = str;
}

/**
  This method returns the string name of the recipient system
  @return Name of the recipient
*/
const char* Klaus::Events::EventRecipient::getRecipientName() const
{
  return mRecipientName.c_str();
}

/**
  This method registers an event name within the EventSystem and returns it's
  event identifier. If the event was already registered, the regisred event's
  id will be returned. This method also registers the caller to the recipients
  of the event id. If you want to just register an event try EventSystem::registerEvent.
  @param strEvent The event name to register
  @return The event identifier from event name registration.
*/
Klaus::EventID Klaus::Events::EventRecipient::registerEvent(const char* strEvent)
{
  // get event id
  EventID id = EventSystem::getSingleton()->registerEvent(strEvent);

  // check if it is alreay been added
  for(size_t i = 0; i < mRegisteredEvents.size(); i++)
    if(mRegisteredEvents[i] == id)
      return id;

  // register as a recipient
  EventSystem::getSingleton()->registerRecipient(id, this);

  // add the event id to id cache
  mRegisteredEvents.push_back(id);

  return id;
}

/**
  This method registers a KeyCombination and an event id within the application. By
  registering the key combination, whenever the keys are pressed, the event will be
  fired to the derived class of EventRecipient registering them.
  @param keys The key combination of binding
  @param event_id The event identifier
  @param bForce Whether forcefully set the key combination or use previous one if found
*/
void Klaus::Events::EventRecipient::registerBinding(const Input::KeyCombination& keys, const EventID & event_id, bool bForce)
{
  // check if it is alreay been added
  int index = -1;
  for(size_t i = 0; i < mRegisteredEvents.size(); i++)
    if(mRegisteredEvents[i] == event_id)
    {
      index = (int)i;
      break;
    }

  if(index == -1)
  {
    // register as a recipient. If the event id did not exist do not do anything
    if(!EventSystem::getSingleton()->registerRecipient(event_id, this))
      return;

    // add the event id to id cache
    mRegisteredEvents.push_back(event_id);
  }

  // assign a recipient name if not set up yet
  if(mRecipientName.empty())
    mRecipientName = typeid(*this).name();

  // store the bind id as the second parameter of map
  std::pair<EventID, BindingID> p;
  p.first = event_id;
  p.second = Input::KeyBindings::getSingleton()->registerBinding(this, keys, event_id, bForce);
  mBindings.insert(mBindings.begin(), p);
}

/**
  This method unregisters all key combination for single event. By unregistering
  the key combination will be removed from the set of actions which can be triggered
  by key presses.
  @param event_id The event identifier to remove all it's key combinations from bindings
*/
void Klaus::Events::EventRecipient::unregisterBinding(const EventID & event_id)
{
  while(true)
  {
    BindingAndEventMap::iterator it = mBindings.find(event_id);
    if(it == mBindings.end())
      return;

    // remove from key bindings
    Input::KeyBindings::getSingleton()->unregisterBinding(it->second, this);

    // remove the key combination
    mBindings.erase(it);
  }
}
