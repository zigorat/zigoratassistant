/*
*********************************************************************************
*          EventSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Central system which coordinates all events by all systems.        *
*                                                                               *
*********************************************************************************
*/

/*! \file EventSystem.cpp
<pre>
<b>File:</b>          EventSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Central system which coordinates all events by all systems.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#include "EventSystem.h"
#include <cstring>

/*! EventSystem singleton instance */
template<> Klaus::Events::EventSystem* Klaus::Singleton<Klaus::Events::EventSystem>::mInstance = 0;


/**
  Constructor of EventSystem. Gets logger instance and nullifies MaxIDs.
*/
Klaus::Events::EventSystem::EventSystem()
{
  mLog = Logger::getSingleton();
  mMaxID = 0;
}

/**
  Destructor of EventSystem. Checks whether all event recipients have unregistered themselves or not.
*/
Klaus::Events::EventSystem::~EventSystem()
{
  for(RecipientMapping::iterator it = mEventMaps.begin(); it != mEventMaps.end(); it++)
    mLog->log(17, "(EventSystem) unregistered events at exit: %s", getEventName(it->first).c_str());

  mLog->log(17, "(EventSystem) all registered events");
  mLog->log(17, "====================================");

  for(EventIDMapping::iterator it = mEventNames.begin(); it != mEventNames.end(); it++)
    mLog->log(17, "  %03d : %s", it->second, it->first.c_str());
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Events::EventSystem * Klaus::Events::EventSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new EventSystem;
    Logger::getSingleton()->log(1, "(EventSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Events::EventSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(EventSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(EventSystem) removed singleton");
  }
}

/**
  This method registers an event name in the system and returns an id for that
  event. If the event has alreay been registered, this will returns the registered
  event's id.
  @param strEvent The event name
  @return The event's registered id
*/
Klaus::EventID Klaus::Events::EventSystem::registerEvent(const char* strEvent)
{
  std::string str(strEvent);
  if(str.empty())
  {
    mLog->log(17, "(EventSystem) tried to register an empty event name");
    return 0;
  }

  // if the event name existed, it's id will be returned
  EventIDMapping::const_iterator it = mEventNames.find(str);
  if(it != mEventNames.end())
    return it->second;

  // otherwise an id will be created for the event name and will be stored
  mEventNames[str] = ++mMaxID;
  return mMaxID;
}

/**
  This method registeres an event recipient in the system. An event can have multiple recipients.
  @param strEvent The event name to register
  @param rec The recipient who wants to get hold of the given event's fired events.
  @return The event ID of the registeration of event name
*/
Klaus::EventID Klaus::Events::EventSystem::registerRecipient(const char * strEvent, EventRecipient* rec)
{
  EventID id = registerEvent(strEvent);
  std::string str = getEventName(id);

  RecipientMapping::iterator it = mEventMaps.find(id);
  if(it != mEventMaps.end())
  {
    for(size_t i = 0; i < it->second.size(); i++)
      if(it->second[i] == rec)
      {
        mLog->log(17, "(EventSystem) the specified recipient has already registered for %s", str.c_str());
        return id;
      }

    it->second.push_back(rec);
  }
  else
    mEventMaps[id].push_back(rec);

  mLog->log(17, "(EventSystem) added a recipient to event %s", str.c_str());
  return id;
}

/**
  This method registeres an event recipient in the system. An event can have multiple recipients.
  @param id The event id to register
  @param rec The recipient who wants to get hold of the given event's fired events.
  @return True if the event existed, false otherwise
*/
bool Klaus::Events::EventSystem::registerRecipient(const EventID & id, EventRecipient* rec)
{
  std::string str = getEventName(id);
  if(str.empty())
  {
    mLog->log(17, "(EventSystem) registerRecipient: no such event: %d", id);
    return false;
  }

  RecipientMapping::iterator it = mEventMaps.find(id);
  if(it != mEventMaps.end())
  {
    for(size_t i = 0; i < it->second.size(); i++)
      if(it->second[i] == rec)
      {
        mLog->log(17, "(EventSystem) the specified recipient has already registered for %s", str.c_str());
        return true;
      }

    it->second.push_back(rec);
  }
  else
    mEventMaps[id].push_back(rec);

  mLog->log(17, "(EventSystem) added a recipient to event %s", str.c_str());
  return true;
}

/**
  This method unregisters an event recipient from the system. The unregistered
  recipient will no longer receive fired events of the given event id.
  @param id The event ID to unregister one of it's recipients
  @param rec The recipient to remove from the list of event's recipients
  @return True if the event existed and rec was one of the recipients, false otherwise
*/
bool Klaus::Events::EventSystem::unregisterRecipient(const EventID & id, EventRecipient* rec)
{
  std::string strEvent = getEventName(id);
  if(strEvent.empty())
  {
    mLog->log(17, "(EventSystem) unregisterRecipient: no such event: %d", id);
    return false;
  }

  RecipientMapping::iterator it = mEventMaps.find(id);
  if(it == mEventMaps.end())
  {
    mLog->log(17, "(EventSystem) no recipient registered for event %s", strEvent.c_str());
    return false;
  }

  for(size_t i = 0; i < it->second.size(); i++)
    if(it->second[i] == rec)
    {
      it->second.erase(it->second.begin() + i);
      mLog->log(17, "(EventSystem) removed a recipient from event %s", strEvent.c_str());

      if(it->second.empty())
        mEventMaps.erase(it);
      return true;
    }

  mLog->log(17, "(EventSystem) event %s does not have the recipient to remove", strEvent.c_str());
  return false;
}

/**
  This method broadcasts the given event to all recipients registered for the event.
  @param ev The event to broadcast to registered recipients of the event
  @return True if the event was registered in the system, false otherwise
*/
bool Klaus::Events::EventSystem::broadcastEvent(const Event& ev)
{
  RecipientMapping::iterator it = mEventMaps.find(ev.mEventID);
  if(it == mEventMaps.end())
  {
    std::string strEvent = getEventName(ev.mEventID);

    if(strEvent.empty())
      mLog->log(17, "(EventSystem) broadcast failed for an unregistered event");
    else
      mLog->log(17, "(EventSystem) event %s has no registered recipients", strEvent.c_str());

    return false;
  }

  for(size_t i = 0; i < it->second.size(); i++)
    it->second[i]->processEvent(ev);

  return true;
}

/**
  This method finds the std::string representation of the given event identifier.
  @param id The event ID to returns it's name
  @return The std::string representation of the event
*/
std::string Klaus::Events::EventSystem::getEventName(const EventID & id) const
{
  for(EventIDMapping::const_iterator it = mEventNames.begin(); it != mEventNames.end(); it++)
    if(it->second == id)
      return it->first;

  return "";
}

/**
  This method gets an event name string and returns its event id
  @param strEvent Name of the event
  @return Event ID for the requested event
*/
Klaus::EventID Klaus::Events::EventSystem::getEventID(const char* strEvent) const
{
  EventIDMapping::const_iterator it = mEventNames.find(strEvent);
  return it == mEventNames.end() ? -1 : it->second;
}
