/*
*********************************************************************************
*          EventRecipient.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class of all classes who need to receive events.              *
*                                                                               *
*********************************************************************************
*/

/*! \file EventRecipient.h
<pre>
<b>File:</b>          EventRecipient.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class of all classes who need to receive events.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef EVENT_RECIPIENT
#define EVENT_RECIPIENT


#include <InputSystem/InputBase.h> // needed for KeyCombination
#include "Event.h"                 // needed for EventID

namespace Klaus
{

  namespace Events
  {

    /*! This class is a base for all classes which receive events. It also takes care of all
        registration and unregistration of events and key combinations to events. Each class
        derived from EventRecipient shall implement the processEvent method.
    */
    class _DLLExportControl EventRecipient
    {
      private:
        /*! BindingAndEventIndex is an internal storage type for storing
            key combination and their event id index in mBindings. */
        typedef std::multimap<EventID, BindingID> BindingAndEventMap;

        BindingAndEventMap mBindings;          /*!< Registered bindings             */
        EventIDList        mRegisteredEvents;  /*!< Registered events               */
        std::string        mRecipientName;     /*!< Name of the recipient           */

      protected:
        void               setRecipientName    ( const char * str                   );

      public:
                         ~ EventRecipient      (                                    );

        const char *       getRecipientName    (                                    ) const;

        EventID            registerEvent       ( const char * strEvent              );
        void               registerBinding     ( const Input::KeyCombination & keys,
                                                 const EventID & event_id,
                                                 bool bForce = false                );
        void               unregisterBinding   ( const EventID & event_id           );

        virtual void       processEvent        ( const Event & event                ) = 0;

        /*! List is a dynamic array of EventRecipients */
        typedef std::vector<EventRecipient*> List;
    };

  }; // end namespace Events

}; // end namespace Klaus


#endif // EVENT_RECIPIENT
