/*
*********************************************************************************
*       ApplicationSettings.h : Robocup 3D Soccer Simulation Team Zigorat       *
*                                                                               *
*  Date: 01/21/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Holds application information, Including name and needed paths.    *
*                                                                               *
*********************************************************************************
*/

/*! \file ApplicationSettings.h
<pre>
<b>File:</b>          ApplicationSettings.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/21/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Holds application information, Including name and needed paths.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/21/2011       Mahdi           Modifies version of initial source (2/08) created
</pre>
*/


#ifndef APPLICATION_SETTINGS
#define APPLICATION_SETTINGS

#include "Singleton.h"
#include <string>

#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  /**
    Provides global settings in Zigorat-Based applications. This includes application
    name, its configuration path, plugins path, whether application is running in debug
    mode and whether it is running in a 64bit environment.
  */
  class _DLLExportControl ApplicationSettings : public Singleton<ApplicationSettings>
  {
    private:
      std::string                  mApplicationName;  /*!< Application name */
      std::string                  mDataPath;         /*!< Address of data files */
      std::string                  mPluginsPath;      /*!< Address of plugins */
      std::string                  mRSGPath;          /*!< Address of ruby scene graph files */
      bool                         mDebugMode;        /*!< Is debug mode? */
      bool                         m64Bit;            /*!< Is compilation 64 bit? */

                                   ApplicationSettings(                               );
                                 ~ ApplicationSettings(                               );
    public:
      static ApplicationSettings * getSingleton       (                               );
      static void                  freeSingleton      (                               );

      void                         initialize         ( const std::string & app_name,
                                                        const std::string & data_root,
                                                        bool is_debug                 );

      std::string                  getApplicationName (                               ) const;
      std::string                  getDataPath        (                               ) const;
      std::string                  getRSGPath         (                               ) const;
      std::string                  getPluginsPath     (                               ) const;

      bool                         isDebugMode        (                               ) const;
  };

}; // end namespace Klaus;


#endif // APPLICATION_SETTINGS
