/*
*********************************************************************************
*          PlatformMacros.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 25/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Compiler, operating system and type definitions on all supported   *
*            platforms.                                                         *
*                                                                               *
*********************************************************************************
*/

/*! \file PlatformMacros.h
<pre>
<b>File:</b>          PlatformMacros.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       25/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Compiler, operating system and type definitions on all supported platforms.
                This source file is based on OgrePrerequisites.h file which is part of OGRE
               (Object-oriented Graphics Rendering Engine)
               For the latest info, see http://www.ogre3d.org/
               License included in source file.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
25/06/2010       Mahdi           Initial version created
</pre>
*/

/*
License exactly as described in the OgreRequisities.h:

-----------------------------------------------------------------------------
Copyright (c) 2000-2006 Torus Knot Software Ltd
Also see acknowledgements in Readme.html

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

You may alternatively use this source under the terms of a specific version of
the OGRE Unrestricted License provided you have obtained such a license from
Torus Knot Software Ltd.
-----------------------------------------------------------------------------
*/

#ifndef PLATFORM_MACROS
#define PLATFORM_MACROS


#define PLATFORM_WINDOWS 1      /*!< Running on Windows */
#define PLATFORM_LINUX   2      /*!< Running on Linux */
#define PLATFORM_APPLE   3      /*!< Running on Apple */

#define COMPILER_MSVC    1      /*!< Compiling using visual studio */
#define COMPILER_GNUC    2      /*!< Compiling using GNU C compiler */
#define COMPILER_BORLAND 3      /*!< Compiling using borland code gear */
#define COMPILER_UNKNOWN 4      /*!< Unknown compiler */

#define CPU_32 1                /*!< A 32 bit cpu */
#define CPU_64 2                /*!< A 64 bit cpu */


/*! Find compiler type and version */
# if defined( _MSC_VER )
#   define COMPILER               COMPILER_MSVC
#   define COMPILER_VERSION       _MSC_VER
# elif defined( __GNUC__ )
#   define COMPILER               COMPILER_GNUC
#   define COMPILER_VERSION       (((__GNUC__)*100) + (__GNUC_MINOR__*10) + __GNUC_PATCHLEVEL__)
# elif defined( __BORLANDC__ )
#   define COMPILER               COMPILER_BORLAND
#   define COMPILER_VERSION       __BCPLUSPLUS__
# else
#   pragma warning "Compiler not detected. Probably not supported"
#   define COMPILER               COMPILER_UNKNOWN
#   define COMPILER_VERSION       0   /*!< Unknown compiler version for an unknown compiler */
# endif

/*! Find current operating system */
# if defined( __WIN32__ ) || defined( _WIN32 )
#   define PLATFORM PLATFORM_WINDOWS
# elif defined( __APPLE_CC__)
#   define PLATFORM PLATFORM_APPLE
# else
#   define PLATFORM PLATFORM_LINUX
# endif

/*! Find CPU architecture */
# if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__ia64__)
#   define CPU_ARCH CPU_64
# else
#   define CPU_ARCH CPU_32
# endif

/*! Windows related settings:
      - On client builds, the information is imported, however on application build these are exported.
*/
# if COMPILER == COMPILER_MSVC
#   define _DLLExport __declspec(dllexport)
#   define _DLLImport __declspec(dllimport)
#   if defined( NON_CLIENT_BUILD )
#     define _DLLExportControl _DLLExport
#   else
#     define _DLLExportControl _DLLImport
#   endif
# endif

/*!
   Linux related settings:
     - Use gcc symbol visibility
     - Include int types

   Windows related settings:
     - define _CRT_SECURE_NO_WARNINGS and _SCL_SECURE_NO_WARNINGS
*/
# if COMPILER == COMPILER_GNUC
#   include <stdint.h> // needed for int types
#   if defined( USE_GCC_VISIBILITY )
#     define _DLLExport __attribute__ ((visibility("default")))
#     define _DLLHidden __attribute__ ((visibility("hidden")))
#     define _DLLExportControl _DLLExport
#   else
#     define _DLLExport
#     define _DLLImport
#     define _DLLExportControl
#   endif
# endif


namespace Klaus
{

  /*! Some data type unifications */
  # if COMPILER == COMPILER_MSVC
      typedef __int32 int32;           /*!< 32 bit integer          */
      typedef __int64 int64;           /*!< 64 bit integer          */
      typedef unsigned __int32 uint32; /*!< 32 bit unsigned integer */
      typedef unsigned __int64 uint64; /*!< 64 bit unsigned integer */
  # else
      typedef int32_t int32;           /*!< 32 bit integer          */
      typedef int64_t int64;           /*!< 64 bit integer          */
      typedef uint32_t uint32; /*!< 32 bit unsigned integer */
      typedef uint64_t uint64; /*!< 64 bit unsigned integer */
  # endif


}; // end namespace Klaus


#endif // Header guard PLATFORM_MACROS
