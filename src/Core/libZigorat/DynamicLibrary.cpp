/*
*********************************************************************************
*        DynamicLibrary.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 09/06/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Wrapper for using DLLs in Linux and Windows                        *
*                                                                               *
*********************************************************************************
*/

/*! \file DynamicLibrary.cpp
<pre>
<b>File:</b>          DynamicLibrary.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       09/06/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Wrapper for using DLLs in Linux and Windows
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
09/06/2008       Mahdi           Initial version created
</pre>
*/

#include "DynamicLibrary.h"

#ifdef WIN32
  #include <strsafe.h>
#else
  #include <dlfcn.h>
#endif

#include <iostream>



/**
  This is the constructor of the DynamicLibrary. It initializes
  the internal variables of the class.
*/
Klaus::DynamicLibrary::DynamicLibrary()
{
  mHandle = NULL;
}

/**
  This is the destrcutor of the DynamicLibrary class. Cleans
  up and closes DLL if it is open.
*/
Klaus::DynamicLibrary::~DynamicLibrary()
{
  closeLibrary();
}

/**
  This method shows the last DLL error
  @param strPrefix Prefix for show error messages
*/
void Klaus::DynamicLibrary::showLastError(const char * strPrefix)
{
  #if PLATFORM == PLATFORM_WINDOWS
  LPVOID lpMsgBuf;
  LPVOID lpDisplayBuf;
  DWORD dw = GetLastError();

  FormatMessage(
                 FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                 NULL,
                 dw,
                 MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                 (LPTSTR) &lpMsgBuf,
                 0,
                 NULL
               );

  // Display the error message
  lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf) + 40) * sizeof(TCHAR));
  StringCchPrintf((LPTSTR)lpDisplayBuf, LocalSize(lpDisplayBuf), TEXT("OpenLibrary error: %s"), lpMsgBuf);
  MessageBox(NULL, (LPCTSTR)lpDisplayBuf, strPrefix, MB_OK);
  LocalFree(lpMsgBuf);
  LocalFree(lpDisplayBuf);
  #else
    std::cerr << strPrefix << ": " << dlerror() << std::endl;
  #endif
}

/**
  This method opens a dynamic link library from the path(or name) given to it.
  In case the library could not be opened, error message will be printed on screen.
  @param strLibrary Name of the library to open
  @return Boolean If the library was opened successfully, true otherwise false
*/
bool Klaus::DynamicLibrary::openLibrary(const std::string & strLibrary)
{
  #ifdef WIN32
  SetLastError(0);
  mHandle = LoadLibrary(strLibrary.c_str());
  #else
  mHandle = dlopen(strLibrary.c_str(), RTLD_LAZY);
  #endif

  if(mHandle)
    mLibraryName = strLibrary;
  else
    showLastError(strLibrary.c_str());

  return mHandle != NULL;
}

/**
  This method closes the currently open library.
  In case library could not be closed properly,
  the error message will be printed
  @return True on successfull closing, false otherwise
*/
bool Klaus::DynamicLibrary::closeLibrary()
{
  if(!mHandle)
    return false;

  int result;
#ifdef WIN32
  result = FreeLibrary((HMODULE)mHandle);
#else
  result = dlclose(mHandle);
#endif

  mHandle = NULL;
  mLibraryName = "";

  return result != 0;
}

/**
  This method returns the function in current library with the name supplied
  @param strFunction Name of the function to return its function pointer
  @param bSuppressOutput Whether to suppress error output or not
  @return Address of the function with specified name
*/
void * Klaus::DynamicLibrary::getFunctionAddress( const std::string& strFunction, bool bSuppressOutput ) const
{
  if(!mHandle)
  {
    std::cerr << "(DynamicLibrary::getFunctionAddress) tried to get function address of '"
              << strFunction << "' when library is not loaded" << std::endl;
    return NULL;
  }

#ifdef WIN32
  return GetProcAddress(mHandle, strFunction.c_str());
#else
  void *pointer = dlsym(mHandle, strFunction.c_str());

  if(!bSuppressOutput)
  {
    char *strError;
    if( (strError = dlerror()) )
      std::cerr << strError << std::endl;
  }

  return pointer;
#endif
}

/**
  This method returns the library name which is currently open
  @return Name of the current open library
*/
std::string Klaus::DynamicLibrary::getLibraryName() const
{
  return mLibraryName;
}

/**
  This method returns whether any dynamic library is open or not
  @return If dynamic library is open true, otherwise false
*/
bool Klaus::DynamicLibrary::isOpen() const
{
  return mLibraryName != "";
}
