/*
*********************************************************************************
*            Algebra.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 10/18/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class definitions of algebra classes Matrix3, Matrix4              *
*            and Quaternion                                                     *
*  Some materials in this file are used from the OGRE project source code.      *
*    See official website: www.ogre3d.org                                       *
*                                                                               *
*********************************************************************************
*/

/*
Copyright 1999-2006 The OGRE Team.

OGRE is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

OGRE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OGRE; see the file COPYING.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/


/*! \file Algebra.cpp
<pre>
<b>File:</b>          Algebra.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/18/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class definitions of algebra classes Matrix3, Matrix4 and Quaternion
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/18/2009       Mahdi           Initial version created
</pre>
*/


#include "Algebra.h"
#include <cstring>
#include <sstream>
#include <iomanip>


/*! This macro controls number of decimal digits printed along the floating point values */
#define setdecimal(x) std::setprecision(x) << std::setiosflags(std::ios::fixed|std::ios::showpoint)



/*****************************************************************************/
/****************************  Class GeomSeries  *****************************/
/*****************************************************************************/

/*! A geometric series is one in which there is a constant ratio between each
    element and the one preceding it. This method determines the
    length of a geometric series given its first element, the sum of the
    elements in the series and the constant ratio between the elements.
    Normally: s = a + ar + ar^2 + ...  + ar^n
    Now: dSum = dFirst + dFirst*dRatio + dFirst*dRatio^2 + .. + dFist*dRatio^n
    \param dFirst first term of the series
    \param dRatio ratio with which the the first term is multiplied
    \param dSum the total sum of all the serie
    \return the length(n in above example) of the series */
double Klaus::GeomSeries::getLength( double dFirst, double dRatio, double dSum )
{
  if( dRatio < 0 )
    std::cerr << "(Geometry:getLengthGeomSeries): negative ratio" << std::endl;

  // s = a + ar + ar^2 + .. + ar^n-1 and thus sr = ar + ar^2 + .. + ar^n
  // subtract: sr - s = - a + ar^n) =>  s(1-r)/a + 1 = r^n = temp
  // log r^n / n = n log r / log r = n = length
  double temp = (dSum * ( dRatio - 1 ) / dFirst) + 1;
  if( temp <= 0 )
    return -1.0;
  return log( temp ) / log( dRatio ) ;
}

/*! A geometric series is one in which there is a constant ratio between each
    element and the one preceding it. This method determines the sum of a
    geometric series given its first element, the ratio and the number of steps
    in the series
    Normally: s = a + ar + ar^2 + ...  + ar^n
    Now: dSum = dFirst + dFirst*dRatio + ... + dFirst*dRatio^dSteps
    \param dFirst first term of the series
    \param dRatio ratio with which the the first term is multiplied
    \param dLength the number of steps to be taken into account
    \return the sum of the series */
double Klaus::GeomSeries::getSum( double dFirst, double dRatio, double dLength)
{
  // s = a + ar + ar^2 + .. + ar^n-1 and thus sr = ar + ar^2 + .. + ar^n
  // subtract: s - sr = a - ar^n) =>  s = a(1-r^n)/(1-r)
  return dFirst * ( 1 - pow( dRatio, dLength ) ) / ( 1 - dRatio ) ;
}

/*! A geometric series is one in which there is a constant ratio between each
    element and the one preceding it. This method determines the sum of an
    infinite geometric series given its first element and the constant ratio
    between the elements. Note that such an infinite series will only converge
    when 0 < r < 1.
    Normally: s = a + ar + ar^2 + ar^3 + ....
    Now: dSum = dFirst + dFirst*dRatio + dFirst*dRatio^2...
    \param dFirst first term of the series
    \param dRatio ratio with which the the first term is multiplied
    \return the sum of the series */
double Klaus::GeomSeries::getSumInf( double dFirst, double dRatio )
{
  if( dRatio > 1 )
    std::cerr << "(Geometry:CalcLengthGeomSeries): series does not converge" <<std::endl;

  // s = a(1-r^n)/(1-r) with n->inf and 0<r<1 => r^n = 0
  return dFirst / ( 1 - dRatio );
}

/*! A geometric series is one in which there is a constant ratio between each
    element and the one preceding it. This method determines the first element
    of a geometric series given its element, the ratio and the number of steps
    in the series
    Normally: s = a + ar + ar^2 + ...  + ar^n
    Now: dSum = dFirst + dFirst*dRatio + ... + dFirst*dRatio^dSteps
    \param dSum sum of the series
    \param dRatio ratio with which the the first term is multiplied
    \param dLength the number of steps to be taken into account
    \return the first element (a) of a serie */
double Klaus::GeomSeries::getFirst( double dSum, double dRatio, double dLength)
{
  // s = a + ar + ar^2 + .. + ar^n-1 and thus sr = ar + ar^2 + .. + ar^n
  // subtract: s - sr = a - ar^n) =>  s = a(1-r^n)/(1-r) => a = s*(1-r)/(1-r^n)
  return dSum *  ( 1 - dRatio )/( 1 - pow( dRatio, dLength ) ) ;
}

/*! A geometric series is one in which there is a constant ratio between each
    element and the one preceding it. This method determines the first element
    of an infinite geometric series given its first element and the constant
    ratio between the elements. Note that such an infinite series will only
    converge when 0<r<1.
    Normally: s = a + ar + ar^2 + ar^3 + ....
    Now: dSum = dFirst + dFirst*dRatio + dFirst*dRatio^2...
    \param dSum sum of the series
    \param dRatio ratio with which the the first term is multiplied
    \return the first term of the series */
double Klaus::GeomSeries::getFirstInf( double dSum, double dRatio )
{
  if( dRatio > 1 )
    std::cerr << "(Geometry:getFirstInfGeomSeries):series does not converge" <<std::endl;

  // s = a(1-r^n)/(1-r) with r->inf and 0<r<1 => r^n = 0 => a = s ( 1 - r)
  return dSum * ( 1 - dRatio );
}





/*****************************************************************************/
/*******************************  Class Algebra  *****************************/
/*****************************************************************************/



/**
 * This method computes factorial by a lookup table to increase speed
   Only till 20 is present in the table, since 21's factorial will lie
   out of boundries for double an is not needed
  @param number Number to returns it's factorial
  @return The factorial of the given number
 */
double Klaus::Algebra::fact( unsigned number )
{
  static const double fact_table[21] =
  {
    1.0,
    1.0,
    2.0,
    6.0,
    24.0,
    120.0,
    720.0,
    5040.0,
    40320.0,
    362880.0,
    3628800.0,
    39916800.0,
    479001600.0,
    6227020800.0,
    87178291200.0,
    1307674368000.0,
    20922789888000.0,
    355687428096000.0,
    6402373705728000.0,
    121645100408832000.0,
    2432902008176640000.0
  };

  if(number < 21)
    return fact_table[number];

  return fact_table[20];
}

/*! This method performs the abc formula (Pythagoras' Theorem) on the given
    parameters and puts the result in *s1 en *s2. It returns the number of
    found coordinates.
    \param a a parameter in abc formula
    \param b b parameter in abc formula
    \param c c parameter in abc formula
    \param *s1 first result of abc formula
    \param *s2 second result of abc formula
    \return number of found x-coordinates */
int Klaus::Algebra::abcFormula(double a, double b, double c, double *s1, double *s2)
{
  double dDiscr = b*b - 4*a*c;       // discriminant is b^2 - 4*a*c
  if (fabs(dDiscr) < SMALL_EPSILON ) // if discriminant = 0
  {
    *s1 = -b / (2 * a);              //  only one solution
    return 1;
  }
  else if (dDiscr < 0)               // if discriminant < 0
    return 0;                        //  no solutions
  else                               // if discriminant > 0
  {
    dDiscr = sqrt(dDiscr);           //  two solutions
    *s1 = (-b + dDiscr ) / (2 * a);
    *s2 = (-b - dDiscr ) / (2 * a);
    return 2;
  }
}




/*****************************************************************************/
/*****************************  Class Statistics  ****************************/
/*****************************************************************************/

/**
 * In statistics, linear regression is a form of regression analysis in which the
   relationship between one or more independent variables and another variable,
   called dependent variable, is modeled by a least squares function, called linear
   regression equation. This function is a linear combination of one or more model
   parameters, called regression coefficients. A linear regression equation with
   one independent variable represents a straight line.

  @param points Data to find the most fitting line between them
  @return The line which shows the linear regression of the given data
 */
Klaus::Line Klaus::Statistics::getLinearRegression( const VecPosition::Array & points )
{
  double x_bar = 0;
  double y_bar = 0;
  int count = points.size();

  //here we calculate the mean
  for(size_t i = 0; i < points.size(); i++)
  {
    x_bar += points[i][0];
    y_bar += points[i][1];
  }
  x_bar /= count;
  y_bar /= count;

  double SXX = 0, SXY = 0;

  for(size_t i = 0; i < points.size(); i++)
  {
    SXY += (points[i][0] - x_bar) * (points[i][1] - y_bar);
    SXX += (points[i][0] - x_bar) * (points[i][0] - x_bar);
  }

  if(fabs(SXX) < SMALL_EPSILON)
    return Line(0, 1, -x_bar);

  double m = SXY/SXX;
  double y0 = y_bar - m*x_bar;

  VecPosition p1(0, y0), p2(1, m + y0);
  return Line::makeLineFromTwoPoints(p1, p2);
}

/**
 * In statistics, linear regression is a form of regression analysis in which the
   relationship between one or more independent variables and another variable,
   called dependent variable, is modeled by a least squares function, called linear
   regression equation. This function is a linear combination of one or more model
   parameters, called regression coefficients. A linear regression equation with
   one independent variable represents a straight line.

  @param points[][] Data to find the most fitting line between them
  @param count Number of the given points
  @return The line which shows the linear regression of the given data
 */
Klaus::Line Klaus::Statistics::getLinearRegression( double points[][2], const int & count )
{
  double x_bar = 0;
  double y_bar = 0;

  //here we calculate the mean
  for(int i = 0; i < count; i++)
  {
      x_bar += points[i][0];
      y_bar += points[i][1];
  }
  x_bar /= count;
  y_bar /= count;

  double SXX = 0, SXY = 0;

  for (int i = 0; i < count; i++)
  {
      SXY += (points[i][0] - x_bar) * (points[i][1] - y_bar);
      SXX += (points[i][0] - x_bar) * (points[i][0] - x_bar);
  }

  if(fabs(SXX) < SMALL_EPSILON)
    return Line(0, 1, -x_bar);

  double m = SXY/SXX;
  double y0 = y_bar - m*x_bar;

  VecPosition p1(0, y0), p2(1, m + y0);
  return Line::makeLineFromTwoPoints(p1, p2);
}




/*****************************************************************************/
/******************************  Class Matrix3  ******************************/
/*****************************************************************************/

const Klaus::Matrix3 Klaus::Matrix3::zero(0,0,0,0,0,0,0,0,0);
const Klaus::Matrix3 Klaus::Matrix3::identity(1,0,0,0,1,0,0,0,1);

/**
 * This is the constructor of Matrix3 class. Initializes the matrix to default values
 */
Klaus::Matrix3::Matrix3()
{
  clearMatrix();
}

/**
 * This is copy constructor of Matrix3 class, initializies the matrix from a
   previous matrix
  @param m The matrix to copy the indices from
 */
Klaus::Matrix3::Matrix3(const Matrix3 & m)
{
  memcpy(mElements, m.mElements, 9*sizeof(double));
}

/**
 * This constructor creates the matrix from the given 4x4 matrix
  @param m The matrix that holds rotation parameters
 */
Klaus::Matrix3::Matrix3( const Matrix4 & m )
{
  *this = m.toMatrix3();
}

/**
 * This constructor creates the matrix from a given quaternion
  @param q Quaternion that holds rotation parameters
 */
Klaus::Matrix3::Matrix3( const Quaternion & q )
{
  *this = q.toMatrix3();
}

/**
 * This constructor copies the values of the matrix elements which are given by
   an array to the matrix elements.
  @param indices[][] The elements of the matrix
 */
Klaus::Matrix3::Matrix3( const double indices[3][3] )
{
  memcpy(mElements, indices, 9*sizeof(double));
}

/**
 * This constructor creates the matrix from the given elements.
  @param d11 First element of the first row
  @param d12 Second element of the first row
  @param d13 Third element of the first row
  @param d21 First element of the second row
  @param d22 Second element of the second row
  @param d23 Third element of the second row
  @param d31 First element of the third row
  @param d32 Second element of the third row
  @param d33 Third element of the third row
 */
Klaus::Matrix3::Matrix3( const double & d11, const double & d12, const double & d13,
                  const double & d21, const double & d22, const double & d23,
                  const double & d31, const double & d32, const double & d33 )
{
  mElements[0][0] = d11;
  mElements[0][1] = d12;
  mElements[0][2] = d13;
  mElements[1][0] = d21;
  mElements[1][1] = d22;
  mElements[1][2] = d23;
  mElements[2][0] = d31;
  mElements[2][1] = d32;
  mElements[2][2] = d33;
}

/**
  This is the destructor of Matrix3 class. Currently does nothing.
*/
Klaus::Matrix3::~Matrix3()
{
}

/**
  This method clears the whole matrix, even places which are
  out of current matrix edge
*/
void Klaus::Matrix3::clearMatrix()
{
  *this = Klaus::Matrix3::identity;
}

/**
 * This method calculates the matrix determinant.
   The determinant is a number which is gotten from the matrix and represents
   some of the characteristics of the matrix, like whether the matrix has an
   inverse matrix or not.
  @return Matrix3 Determinant
 */
double Klaus::Matrix3::getDeterminant() const
{
  double v1 = mElements[0][0] * (mElements[1][1] * mElements[2][2] - mElements[2][1] * mElements[1][2]);
  double v2 = mElements[1][0] * (mElements[0][1] * mElements[2][2] - mElements[2][1] * mElements[0][2]);
  double v3 = mElements[2][0] * (mElements[0][1] * mElements[1][2] - mElements[1][1] * mElements[0][2]);
  return v1 - v2 + v3;
}

/**
 * This method calculates inverse matrix of matrix.
                                                                  -1
   Given a matrix M, then the inverse of that matrix, denoted as M  , is
   the matrix which satisfies the following expression:

          -1
     M . M   = I

   where I is the identity matrix.
   Thus, multiplying a matrix with its inverse will generate the identity matrix.
  @return Inverse matrix of the current matrix
 */
Klaus::Matrix3 Klaus::Matrix3::getInverseMatrix() const
{
  Matrix3 m;

  double det = getDeterminant();
  if( fabs(det) < SMALL_EPSILON )
    std::cerr << "(Klaus::Matrix3::inverseMatrix) Determinant is zero" << std::endl;

  m[0][0] = mElements[1][1] * mElements[2][2] - mElements[1][2] * mElements[2][1];
  m[0][1] = mElements[0][2] * mElements[2][1] - mElements[0][1] * mElements[2][2];
  m[0][2] = mElements[0][1] * mElements[1][2] - mElements[0][2] * mElements[1][1];
  m[1][0] = mElements[1][2] * mElements[2][0] - mElements[1][0] * mElements[2][2];
  m[1][1] = mElements[0][0] * mElements[2][2] - mElements[0][2] * mElements[2][0];
  m[1][2] = mElements[0][2] * mElements[1][0] - mElements[0][0] * mElements[1][2];
  m[2][0] = mElements[1][0] * mElements[2][1] - mElements[1][1] * mElements[2][0];
  m[2][1] = mElements[0][1] * mElements[2][0] - mElements[0][0] * mElements[2][1];
  m[2][2] = mElements[0][0] * mElements[1][1] - mElements[0][1] * mElements[1][0];
  m /= det;

  return m;
}

/**
 * This method returns the transpose of the matrix
   The transpose of matrix is the matrix generated when every element in
   the matrix is swapped with the opposite relative to the major diagonal
   This can be expressed as the mathematical operation:

     M'   = M
       ij    ji

   However, this can only be performed if a matrix has an equal number
   of rows and columns.
   If the matrix is a rotation matrix, then the transpose is guaranteed
   to be the inverse of the matrix.
  @return Matrix3's transpose
 */
Klaus::Matrix3 Klaus::Matrix3::getTranspose() const
{
  return Matrix3(mElements[0][0], mElements[1][0], mElements[2][0],
                 mElements[0][1], mElements[1][1], mElements[2][1],
                 mElements[0][2], mElements[1][2], mElements[2][2]);
}

/**
 * This method orthonormalizes the matrix.
   Algorithm uses Gram-Schmidt orthogonalization.  If 'this' matrix is
   M = [m0|m1|m2], then orthonormal output matrix is Q = [q0|q1|q2],

    q0 = m0/|m0|
    q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
    q2 = (m2-(q0*m2)q0-(q1*m2)q1)/|m2-(q0*m2)q0-(q1*m2)q1|

   where |V| indicates length of vector V and A*B indicates dot
   product of vectors A and B.
 */
void Klaus::Matrix3::orthoNormalize()
{
  double fInvLength = 1.0 /sqrt(mElements[0][0]*mElements[0][0] + mElements[1][0]*mElements[1][0] + mElements[2][0]*mElements[2][0]);

  mElements[0][0] *= fInvLength;
  mElements[1][0] *= fInvLength;
  mElements[2][0] *= fInvLength;

  // compute q1
  double fDot0 = mElements[0][0]*mElements[0][1] + mElements[1][0]*mElements[1][1] + mElements[2][0]*mElements[2][1];

  mElements[0][1] -= fDot0*mElements[0][0];
  mElements[1][1] -= fDot0*mElements[1][0];
  mElements[2][1] -= fDot0*mElements[2][0];

  fInvLength = 1.0 / sqrt(mElements[0][1]*mElements[0][1] + mElements[1][1]*mElements[1][1] + mElements[2][1]*mElements[2][1]);

  mElements[0][1] *= fInvLength;
  mElements[1][1] *= fInvLength;
  mElements[2][1] *= fInvLength;

  // compute q2
  double fDot1 = mElements[0][1]*mElements[0][2] + mElements[1][1]*mElements[1][2] + mElements[2][1]*mElements[2][2];

  fDot0 = mElements[0][0]*mElements[0][2] + mElements[1][0]*mElements[1][2] + mElements[2][0]*mElements[2][2];

  mElements[0][2] -= fDot0*mElements[0][0] + fDot1*mElements[0][1];
  mElements[1][2] -= fDot0*mElements[1][0] + fDot1*mElements[1][1];
  mElements[2][2] -= fDot0*mElements[2][0] + fDot1*mElements[2][1];

  fInvLength = 1.0 / sqrt(mElements[0][2]*mElements[0][2] + mElements[1][2]*mElements[1][2] + mElements[2][2]*mElements[2][2]);

  mElements[0][2] *= fInvLength;
  mElements[1][2] *= fInvLength;
  mElements[2][2] *= fInvLength;
}

/**
 * This method returns the spcefied coloumn of the Matrix
  @param iIndex Index of the desired coloumn of Matrix, in range [0,2]
  @return Desired coloumn of the Matrix
 */
Klaus::VecPosition Klaus::Matrix3::getColoumn( const int & iIndex ) const
{
  if(iIndex < 0 || iIndex > 2)
  {
    std::cerr << "(Klaus::Matrix3::getColoumn) Index out of bounds: " << iIndex << std::endl;
    return VecPosition::Unknown;
  }

  return VecPosition(mElements[0][iIndex],mElements[1][iIndex],mElements[2][iIndex]);
}

/**
 * This method sets the desired coloumn values of Matrix to the specified values
  @param iIndex Index of the desired coloumn of Matrix, in range [0,2]
  @param col The values as a VecPosition
 */
void Klaus::Matrix3::setColoumn( const int & iIndex, const VecPosition & col )
{
  if(iIndex < 0 || iIndex > 2)
    std::cerr << "(Klaus::Matrix3::setColoumn) Index out of bounds: " << iIndex << std::endl;
  else
  {
    mElements[0][iIndex] = col[0];
    mElements[1][iIndex] = col[1];
    mElements[2][iIndex] = col[2];
  }
}

/**
 * This method creates a rotation matrix from the given rotated axes.
  @param xAxis Rotated X-Axis
  @param yAxis Rotated Y-Axis
  @param zAxis Rotated Z-Axis
 */
void Klaus::Matrix3::fromAxes( const VecPosition & xAxis, const VecPosition & yAxis, const VecPosition & zAxis )
{
  setColoumn(0, xAxis);
  setColoumn(1, yAxis);
  setColoumn(2, zAxis);
}

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in XYZ order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesXYZ (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz    -cy*sz     sy
  //  cz*sx*sy+cx*sz  cx*cz-sx*sy*sz -cy*sx
  //   -cx*cz*sy+sx*sz  cz*sx+cx*sy*sz  cx*cy

  pitch = asin(mElements[0][2]);
  if( pitch < M_PI_2)
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(-mElements[1][2],mElements[2][2]);
      roll = atan2(-mElements[0][1],mElements[0][0]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(mElements[1][0],mElements[1][1]);
      roll = Radian(0.0);  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(mElements[1][0],mElements[1][1]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
}

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in XZY order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesXZY (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz    -sz    cz*sy
  //  sx*sy+cx*cy*sz  cx*cz    -cy*sx+cx*sy*sz
  //   -cx*sy+cy*sx*sz  cz*sx     cx*cy+sx*sy*sz

  pitch = asin(-mElements[0][1]);
  if ( pitch < M_PI_2 )
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(mElements[2][1],mElements[1][1]);
      roll = atan2(mElements[0][2],mElements[0][0]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(-mElements[2][0],mElements[2][2]);
      roll = 0.0;  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(-mElements[2][0],mElements[2][2]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
}

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in YXZ order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesYXZ (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz+sx*sy*sz  cz*sx*sy-cy*sz  cx*sy
  //  cx*sz     cx*cz    -sx
  //   -cz*sy+cy*sx*sz  cy*cz*sx+sy*sz  cx*cy

  pitch = asin(-mElements[1][2]);
  if ( pitch < M_PI_2 )
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(mElements[0][2],mElements[2][2]);
      roll = atan2(mElements[1][0],mElements[1][1]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(-mElements[0][1],mElements[0][0]);
      roll = 0.0;  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(-mElements[0][1],mElements[0][0]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
}

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in YZX order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesYZX (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz     sx*sy-cx*cy*sz  cx*sy+cy*sx*sz
  //  sz    cx*cz    -cz*sx
  //   -cz*sy     cy*sx+cx*sy*sz  cx*cy-sx*sy*sz

  pitch = asin(mElements[1][0]);
  if ( pitch < M_PI_2 )
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(-mElements[2][0],mElements[0][0]);
      roll = atan2(-mElements[1][2],mElements[1][1]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(mElements[2][1],mElements[2][2]);
      roll = 0.0;  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(mElements[2][1],mElements[2][2]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
  }

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in ZXY order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesZXY (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz-sx*sy*sz -cx*sz     cz*sy+cy*sx*sz
  //  cz*sx*sy+cy*sz  cx*cz    -cy*cz*sx+sy*sz
  //   -cx*sy     sx    cx*cy

  pitch = asin(mElements[2][1]);
  if ( pitch < M_PI_2 )
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(-mElements[0][1],mElements[1][1]);
      roll = atan2(-mElements[2][0],mElements[2][2]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(mElements[0][2],mElements[0][0]);
      roll = 0.0;  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(mElements[0][2],mElements[0][0]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
}

/**
 * This method converts the matrix (which should be a rotation matrix) to a yaw pitch roll
   sequential rotations in ZYX order.
  @param yaw Yaw parameter of rotation
  @param pitch pitch parameter of rotation
  @param roll roll parameter of rotation
  @return True if the solution was unique, false otherwise
 */
bool Klaus::Matrix3::toEulerAnglesZYX (Radian& yaw, Radian& pitch, Radian& roll) const
{
  // rot =  cy*cz     cz*sx*sy-cx*sz  cx*cz*sy+sx*sz
  //  cy*sz     cx*cz+sx*sy*sz -cz*sx+cx*sy*sz
  //   -sy    cy*sx     cx*cy

  pitch = asin(-mElements[2][0]);
  if ( pitch < M_PI_2 )
  {
    if ( pitch > -M_PI_2 )
    {
      yaw = atan2(mElements[1][0],mElements[0][0]);
      roll = atan2(mElements[2][1],mElements[2][2]);
      return true;
    }
    else
    {
      // WARNING.  Not a unique solution.
      Radian fRmY = atan2(-mElements[0][1],mElements[0][2]);
      roll = 0.0;  // any angle works
      yaw = roll - fRmY;
      return false;
    }
  }
  else
  {
    // WARNING.  Not a unique solution.
    Radian fRpY = atan2(-mElements[0][1],mElements[0][2]);
    roll = 0.0;  // any angle works
    yaw = fRpY - roll;
    return false;
  }
}

/**
 * This method creates a rotation matrix which is rotated along X, Y and Z axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesXYZ (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  *this = kXMat*(kYMat*kZMat);
}

/**
 * This method creates a rotation matrix which is rotated along X, Z and Y axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesXZY (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  *this = kXMat*(kZMat*kYMat);
}

/**
 * This method creates a rotation matrix which is rotated along Y, X and Z axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesYXZ (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  *this = kYMat*(kXMat*kZMat);
}

/**
 * This method creates a rotation matrix which is rotated along Y, Z and X axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesYZX (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  *this = kYMat*(kZMat*kXMat);
}

/**
 * This method creates a rotation matrix which is rotated along Z, X and Y axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesZXY (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  *this = kZMat*(kXMat*kYMat);
}

/**
 * This method creates a rotation matrix which is rotated along Z, Y and X axes with
   regard to the given yaw, pitch and roll parameters.
  @param yaw Yaw value of rotation
  @param pitch Pitch value of rotation
  @param roll roll value of rotation
 */
void Klaus::Matrix3::fromEulerAnglesZYX (const Radian& yaw, const Radian& pitch, const Radian& roll)
{
  double fCos, fSin;

  fCos = yaw.cos();
  fSin = yaw.sin();
  Matrix3 kZMat(fCos,-fSin,0.0,fSin,fCos,0.0,0.0,0.0,1.0);

  fCos = pitch.cos();
  fSin = pitch.sin();
  Matrix3 kYMat(fCos,0.0,fSin,0.0,1.0,0.0,-fSin,0.0,fCos);

  fCos = roll.cos();
  fSin = roll.sin();
  Matrix3 kXMat(1.0,0.0,0.0,0.0,fCos,-fSin,0.0,fSin,fCos);

  *this = kZMat*(kYMat*kXMat);
}

/**
 * This method converts the rotation matrix into an angle-axis pair which denote a unique rotation.
   Let (x,y,z) be the unit-length axis and let A be an angle of rotation.
   The rotation matrix is R = I + sin(A)*P + (1-cos(A))*P^2 where
   I is the identity and

         +-        -+
     P = |  0 -z +y |
         | +z  0 -x |
         | -y +x  0 |
         +-        -+

   If A > 0, R represents a counterclockwise rotation about the axis in
   the sense of looking from the tip of the axis vector towards the
   origin.  Some algebra will show that

     cos(A) = (trace(R)-1)/2  and  R - R^t = 2*sin(A)*P

   In the event that A = pi, R-R^t = 0 which prevents us from extracting
   the axis through P.  Instead note that R = I+2*P^2 when A = pi, so
   P^2 = (R-I)/2.  The diagonal entries of P^2 are x^2-1, y^2-1, and
   z^2-1.  We can solve these for axis (x,y,z).  Because the angle is pi,
   it does not matter which sign you choose on the square roots.
  @param axis The Axis of rotation
  @param angle The angle of rotation
 */
void Klaus::Matrix3::toAxisAngle(VecPosition& axis, Degree & angle) const
{
  double fTrace = mElements[0][0] + mElements[1][1] + mElements[2][2];
  double fCos = 0.5*(fTrace-1.0);
  angle = Degree::acos(fCos);  // in [0,PI]

  if(angle > 0.0)
  {
    if( angle < 180.0 )
    {
      axis[0] = mElements[2][1]-mElements[1][2];
      axis[1] = mElements[0][2]-mElements[2][0];
      axis[2] = mElements[1][0]-mElements[0][1];
      axis.normalize();
    }
    else
    {
      // angle is PI
      double fHalfInverse;
      if ( mElements[0][0] >= mElements[1][1] )
      {
        // r00 >= r11
        if ( mElements[0][0] >= mElements[2][2] )
        {
          // r00 is maximum diagonal term
          axis[0] = 0.5*sqrt(mElements[0][0] - mElements[1][1] - mElements[2][2] + 1.0);
          fHalfInverse = 0.5/axis[0];
          axis[1] = fHalfInverse*mElements[0][1];
          axis[2] = fHalfInverse*mElements[0][2];
        }
        else
        {
          // r22 is maximum diagonal term
          axis[2] = 0.5*sqrt(mElements[2][2] - mElements[0][0] - mElements[1][1] + 1.0);
          fHalfInverse = 0.5/axis[2];
          axis[0] = fHalfInverse*mElements[0][2];
          axis[1] = fHalfInverse*mElements[1][2];
        }
      }
      else
      {
        // r11 > r00
        if ( mElements[1][1] >= mElements[2][2] )
        {
          // r11 is maximum diagonal term
          axis[1] = 0.5*sqrt(mElements[1][1] - mElements[0][0] - mElements[2][2] + 1.0);
          fHalfInverse  = 0.5/axis[1];
          axis[0] = fHalfInverse*mElements[0][1];
          axis[2] = fHalfInverse*mElements[1][2];
        }
        else
        {
          // r22 is maximum diagonal term
          axis[2] = 0.5*sqrt(mElements[2][2] - mElements[0][0] - mElements[1][1] + 1.0);
          fHalfInverse = 0.5/axis[2];
          axis[0] = fHalfInverse*mElements[0][2];
          axis[1] = fHalfInverse*mElements[1][2];
        }
      }
    }
  }
  else
  {
    // The angle is 0 and the matrix is the identity.  Any axis will
    // work, so just use the x-axis.
    axis[0] = 1.0;
    axis[1] = 0.0;
    axis[2] = 0.0;
  }
}

/**
 * This method creates a rotation matrix from a given axis-angle pair.
  @param axis The axis of rotation
  @param angle The angle of rotation around the rotation axis
 */
void Klaus::Matrix3::fromAxisAngle (const VecPosition& axis, const Degree& angle)
{
  double fCos = angle.cos();
  double fSin = angle.sin();
  double fOneMinusCos = 1.0-fCos;
  double fX2 = axis[0]*axis[0];
  double fY2 = axis[1]*axis[1];
  double fZ2 = axis[2]*axis[2];
  double fXYM = axis[0]*axis[1]*fOneMinusCos;
  double fXZM = axis[0]*axis[2]*fOneMinusCos;
  double fYZM = axis[1]*axis[2]*fOneMinusCos;
  double fXSin = axis[0]*fSin;
  double fYSin = axis[1]*fSin;
  double fZSin = axis[2]*fSin;

  mElements[0][0] = fX2*fOneMinusCos+fCos;
  mElements[0][1] = fXYM-fZSin;
  mElements[0][2] = fXZM+fYSin;
  mElements[1][0] = fXYM+fZSin;
  mElements[1][1] = fY2*fOneMinusCos+fCos;
  mElements[1][2] = fYZM-fXSin;
  mElements[2][0] = fXZM-fYSin;
  mElements[2][1] = fYZM+fXSin;
  mElements[2][2] = fZ2*fOneMinusCos+fCos;
}

/**
 * This overloaded assignment operator assigns supplied matrix to current one
  @param mat Matrix3 to copy to the current one
  @return Matrix3 reference to enable recursive assignment.
 */
Klaus::Matrix3 & Klaus::Matrix3::operator = ( const Matrix3 & mat )
{
  memcpy(mElements, mat.mElements, 9*sizeof(double));

  return *this;
}

/**
 * This overloaded operator fills current Matrix3 with the supplied double value
  @param val Value to copy to all elements of Matrix3
  @return Matrix3 reference to enable recursive copying.
 */
Klaus::Matrix3 & Klaus::Matrix3::operator = ( const double & val )
{
  for( unsigned j = 0; j < 3; j++ )
    for( unsigned i = 0; i < 3; i++ )
      mElements[j][i] = val;

  return *this;
}

/**
 * This overloaded operator sums the Matrix3 supplied to the current one
 * and returns the result
  @param mat Matrix3 to sum to current one
  @return Summation result
 */
Klaus::Matrix3 Klaus::Matrix3::operator + ( const Matrix3 & mat ) const
{
  Matrix3 m;

  for( unsigned y = 0; y < 3; y++ )
    for( unsigned x = 0; x < 3; x++ )
      m[y][x] = mElements[y][x] + mat.mElements[y][x];

  return m;
}

/**
 * This overloaded operator sums the value supplied with the current one
 * and returns the result
  @param val Value to sum
  @return Summation result
 */
Klaus::Matrix3 Klaus::Matrix3::operator + ( const double & val ) const
{
  Matrix3 m;

  for( unsigned y = 0; y < 3; y++ )
    for( unsigned x = 0; x < 3; x++ )
      m.mElements[y][x] = mElements[y][x] + val;

  return m;
}

/**
 * This overloaded operator subtracts the Matrix3 supplied from the current one
 * and returns the result
  @param mat Matrix3 to subtract
  @return Subtract result
 */
Klaus::Matrix3 Klaus::Matrix3::operator - ( const Matrix3 & mat ) const
{
  Matrix3 m;

  for( unsigned y = 0; y < 3; y++ )
    for( unsigned x = 0; x < 3; x++ )
      m[y][x] = mElements[y][x] - mat[y][x];

  return m;
}

/**
 * This overloaded operator subtracts the value supplied from the current one
 * and returns the result
  @param val Value to subtract
  @return Subtract result
 */
Klaus::Matrix3 Klaus::Matrix3::operator - ( const double & val ) const
{
  Matrix3 m;

  for( unsigned y = 0; y < 3; y++ )
    for( unsigned x = 0; x < 3; x++ )
      m[y][x] = mElements[y][x] - val;

  return m;
}

/**
 * This method calculates multiplication of a row of a matrix by a coloumn of another matrix.
 * This method is used to calculate 3*3 Matrix3 multiplication
  @param va First element of first matrix's row
  @param vb Second element of first matrix's row
  @param vc Third element of first matrix's row
  @param vd First element of second matrix's coloumn
  @param ve Second element of second matrix's coloumn
  @param vf Third element of second matrix's coloumn
  @return Multiplication of row by column
 */
inline double calculateRowCrossColumn(double va,double vb,double vc,double vd,double ve,double vf)
{
  return ( va*vd + vb*ve + vc*vf );
}

/**
 * This overloaded operator multiplies the Matrix3 supplied with the current one
 * and returns the result
  @param mat Matrix3 to multiply with
  @return Multiplication result
 */
Klaus::Matrix3 Klaus::Matrix3::operator * ( const Matrix3 & mat ) const
{
  Matrix3 m;

  for(unsigned y = 0; y < 3; y++)
    for(unsigned x = 0; x < 3; x++)
      m[y][x] = calculateRowCrossColumn
                      (
                        mElements[y][0],  mElements[y][1],  mElements[y][2],
                        mat.mElements[0][x], mat.mElements[1][x], mat.mElements[2][x]
                      );

  return m;
}

/**
 * This overloaded operator multiplies the value supplied with the current one
 * and returns the result
  @param val value to multiply with
  @return Multiplication result
 */
Klaus::Matrix3 Klaus::Matrix3::operator * ( const double & val ) const
{
  Matrix3 m;

  for( unsigned y = 0; y < 3; y++ )
    for( unsigned x = 0; x < 3; x++ )
      m[y][x] = m[y][x] * val;

  return m;
}

/**
 * This overloaded operator divides this Matrix3 to the supplied one
 * and returns the result
  @param mat Matrix3 to divide this into
  @return Division result
 */
Klaus::Matrix3 Klaus::Matrix3::operator / ( const Matrix3 & mat ) const
{
  return *this * mat.getInverseMatrix();
}

/**
 * This overloaded operator divides this Matrix3 with the supplied value
 * and returns the result
  @param val value to divide this into
  @return Division result
 */
Klaus::Matrix3 Klaus::Matrix3::operator / ( const double & val ) const
{
  Matrix3 m;
  m = *this;
  double dd = 1.0 / val;

  for(unsigned y = 0; y < 3; y++)
    for(unsigned x = 0; x < 3; x++)
      m[y][x] *= dd;

  return m;
}

/*! Overloaded version of the sum-assignment operator for Matrixes. It
    returns the sum of the current Matrix3 and the one supplied by adding
    their elements together. This changes the current Matrix3 itself.
    \param mat Matrix3 to be added */
Klaus::Matrix3 & Klaus::Matrix3::operator += ( const Matrix3 & mat )
{
  *this = *this + mat;
  return *this;
}

/*! Overloaded version of the sum-assignment operator for Matrixes. It
    returns the sum of the current Matrix3 and the value supplied by adding
    the values to elements. This changes the current Matrix3 itself.
    \param val Value to be added */
Klaus::Matrix3 & Klaus::Matrix3::operator += ( const double & val )
{
  *this = *this + val;
  return *this;
}

/*! Overloaded version of the subtract-assignment operator for Matrixes. It
    returns the subtract of the current Matrix3 and the one supplied by subtracting
    their elements from each other. This changes the current Matrix3 itself.
    \param mat Matrix3 to be subtracted */
Klaus::Matrix3 & Klaus::Matrix3::operator -= ( const Matrix3 & mat )
{
  *this = *this - mat;
  return *this;
}

/*! Overloaded version of the subtract-assignment operator for Matrixes.
    It returns the subtract of the current Matrix3 and the value supplied
    by subtracting the values from elements. This changes the current
    Matrix3 itself.
    \param val Value to be subtracted */
Klaus::Matrix3 & Klaus::Matrix3::operator -= ( const double & val )
{
  *this = *this - val;
  return (*this);
}

/*! Overloaded version of the multiply-assignment operator for Matrixes. It
    returns the multiplication of the current Matrix3 and the one supplied.
    This changes the current Matrix3 itself.
    \param mat Matrix3 to be multiplied */
Klaus::Matrix3 & Klaus::Matrix3::operator *= ( const Matrix3 & mat )
{
  *this = *this * mat;
  return *this;
}

/*! Overloaded version of the multiplication-assignment operator for
    Matrixes. It returns the multiplication of the current Matrix3 and
    the value supplied by multiplying the values to elements. This
    changes the current Matrix3 itself.
    \param val Value to be subtracted */
Klaus::Matrix3 & Klaus::Matrix3::operator *= ( const double & val )
{
  *this = *this * val;
  return *this;
}

/*! Overloaded version of the division-assignment operator for Matrixes.
    It returns the division of the current Matrix3 and the one supplied.
    This changes the current Matrix3 itself.
    \param mat Matrix3 to be devided */
Klaus::Matrix3 & Klaus::Matrix3::operator /= ( const Matrix3 & mat )
{
  *this = *this / mat;
  return *this;
}

/*! Overloaded version of the division-assignment operator for Matrixes.
    It returns the division of the current Matrix3 and the value supplied
    by dividing the elements by the value. This changes the current
    Matrix3 itself.
    \param val Value to be subtracted */
Klaus::Matrix3 & Klaus::Matrix3::operator /= ( const double & val )
{
  *this = *this / val;
  return *this;
}

/**
 * This operator provides ez access to member elements
  @param iIndex Index of requested row
  @return Requested row as a double array
 */
const double * Klaus::Matrix3::operator [] ( const int & iIndex ) const
{
  if( iIndex < 0 || iIndex > 2 )
    std::cerr << "(Klaus::Matrix3::operator []) index out of range: " << iIndex << std::endl;

  return mElements[iIndex];
}

/**
 * This operator provides ez access to member elements
  @param iIndex Index of requested row
  @return Requested row as a double array
 */
double * Klaus::Matrix3::operator [] ( const int & iIndex )
{
  if( iIndex < 0 || iIndex > 2 )
    std::cerr << "(Klaus::Matrix3::operator []) index out of range: " << iIndex << std::endl;

  return mElements[iIndex];
}

/**
 * This is the boolean operator for equality check between current
 * matrix and the one supplied
  @param mat Matrix3 to check if unequal to current matrixs
  @return True if matrix supplied is identical to current one, false otherwise
 */
bool Klaus::Matrix3::operator == ( const Matrix3 & mat ) const
{
  for(unsigned y = 0; y < 3; y++)
    for(unsigned x = 0; x < 3; x++)
      if(mElements[y][x] != mat.mElements[y][x])
        return false;

  return true;
}

/**
 * This is the boolean operator for checking unequality between current
 * matrix and the one supplied
  @param mat Matrix3 to check if unequal to current matrixs
  @return True if matrix supplied is identical to current one, false otherwise
 */
bool Klaus::Matrix3::operator != ( const Matrix3 & mat ) const
{
  return ! (*this == mat);
}

/**
 * This method rotates the given point by the Matrix. The matrix should be a rotation matrix.
   matrix * point [3x3 * 3x1 = 3x1]
  @param point The given point to rotate
  @return Rotated position of the point by the matrix
 */
Klaus::VecPosition Klaus::Matrix3::operator * ( const VecPosition & point ) const
{
  VecPosition rot;
  for(size_t iRow = 0; iRow < 3; iRow++)
    rot[iRow] = mElements[iRow][0]*point[0] + mElements[iRow][1]*point[1] + mElements[iRow][2]*point[2];

  return rot;
}

/**
 * This method rotates the given point by the Matrix. The matrix should be a rotation matrix.
   vector * matrix [1x3 * 3x3 = 1x3]
  @param point The given point to rotate
  @param mat The matrix to rotate the given point
  @return Rotated position of the point by the matrix
 */
Klaus::VecPosition operator * ( const Klaus::VecPosition & point, const Klaus::Matrix3 & mat )
{
  Klaus::VecPosition rot;
  for (size_t iRow = 0; iRow < 3; iRow++)
    rot[iRow] = point[0]*mat[0][iRow] + point[1]*mat[1][iRow] + point[2]*mat[2][iRow];

  return rot;
}

/**
 * This is the overloaded output operator.
 * Prints the matrix into the output stream supplied
  @param os Output stream to print matrix to
  @param m Matrix3 to print to stream
  @return Output stream reference
 */
std::ostream & operator << ( std::ostream &os, Klaus::Matrix3 m )
{
  os << m.str();
  return os;
}

/**
 * This method saves the internal elements of the Matrix3 as a std::string and returns them
  @return String containing the matrix information
 */
std::string Klaus::Matrix3::str() const
{
  std::stringstream buf;
  buf << setdecimal(2);
  buf << "(Matrix3 (";
  buf << mElements[0][0] << " " << mElements[0][1] << " " << mElements[0][2] << " " <<
         mElements[1][0] << " " << mElements[1][1] << " " << mElements[1][2] << " " <<
         mElements[2][0] << " " << mElements[2][1] << " " << mElements[2][2] << "))";

  return buf.str();
}

/**
 * This method creates a Matrix3 from the positions given.
 * Each position is put in the corresponding row, in this
 * way coordinates of the positions, create columns of the
 * Matrix3.
  @param pos1 Position to fill the first row
  @param pos2 Position to fill the second row
  @param pos3 Position to fill the third row
  @return A Matrix3 containing the positions
 */
Klaus::Matrix3 Klaus::Matrix3::createFrom3Points( const VecPosition & pos1, const VecPosition & pos2, const VecPosition & pos3 )
{
  return Matrix3( pos1[0], pos2[0], pos3[0],
                  pos1[1], pos2[1], pos3[1],
                  pos1[2], pos2[2], pos3[2] );
}








/*****************************************************************************/
/******************************  Class Matrix4  ******************************/
/*****************************************************************************/

const Klaus::Matrix4 Klaus::Matrix4::zero(
                             0.0, 0.0, 0.0, 0.0,
                             0.0, 0.0, 0.0, 0.0,
                             0.0, 0.0, 0.0, 0.0,
                             0.0, 0.0, 0.0, 0.0
                           );

const Klaus::Matrix4 Klaus::Matrix4::identity(
                                 1.0, 0.0, 0.0, 0.0,
                                 0.0, 1.0, 0.0, 0.0,
                                 0.0, 0.0, 1.0, 0.0,
                                 0.0, 0.0, 0.0, 1.0
                               );

/**
 * This is the constructor of Matrix4 class. Initializes the matrix to default values
 */
Klaus::Matrix4::Matrix4()
{
  operator=(identity);
}

/**
 * This is copy constructor of Matrix4 class, initializies the matrix from a
   previous matrix
  @param m The Matrix4 to copy the indices from
 */
Klaus::Matrix4::Matrix4(const Matrix4 & m)
{
  memcpy(mElements, m.mElements, 16*sizeof(double));
}

/**
 * This method copies the given 4x4 matrix array to the Matrix4
  @param indices[][] The elements of the matrix
 */
Klaus::Matrix4::Matrix4( const double indices[4][4] )
{
  memcpy(mElements, indices, 16*sizeof(double));
}

/**
 * This constructor creates a standard 4x4 transformation matrix from the given Matrix3.
   The matrix will be with a zero translation part from a rotation/scaling 3x3 matrix
  @param mat Matrix3 to copy into the Matrix4
 */
Klaus::Matrix4::Matrix4( const Matrix3 & mat )
{
  operator=(identity);
  fromMatrix3(mat);
}

/**
 * This constructor creates the matrix from the given quaternion
  @param q The quaternion to build the matrix from
 */
Klaus::Matrix4::Matrix4( const Quaternion & q )
{
  operator=(identity);
  fromQuaternion(q);
}

/**
 * This constructor creates the matrix from the given elements.
  @param d11 First element of the first row
  @param d12 Second element of the first row
  @param d13 Third element of the first row
  @param d14 Forth element of the first row
  @param d21 First element of the second row
  @param d22 Second element of the second row
  @param d23 Third element of the second row
  @param d24 Forth element of the second row
  @param d31 First element of the third row
  @param d32 Second element of the third row
  @param d33 Third element of the third row
  @param d34 Forth element of the thrid row
  @param d41 First element of the forth row
  @param d42 Second element of the forth row
  @param d43 Third element of the forth row
  @param d44 Forth element of the forth row
 */
Klaus::Matrix4::Matrix4( const double & d11, const double & d12, const double & d13, const double &d14,
                  const double & d21, const double & d22, const double & d23, const double &d24,
                  const double & d31, const double & d32, const double & d33, const double &d34,
                  const double & d41, const double & d42, const double & d43, const double &d44)
{
  mElements[0][0] = d11;
  mElements[0][1] = d12;
  mElements[0][2] = d13;
  mElements[0][3] = d14;
  mElements[1][0] = d21;
  mElements[1][1] = d22;
  mElements[1][2] = d23;
  mElements[1][3] = d24;
  mElements[2][0] = d31;
  mElements[2][1] = d32;
  mElements[2][2] = d33;
  mElements[2][3] = d34;
  mElements[3][0] = d41;
  mElements[3][1] = d42;
  mElements[3][2] = d43;
  mElements[3][3] = d44;
}


/**
  This is the destructor of Matrix4 class.
*/
Klaus::Matrix4::~Matrix4()
{
}

/**
  This method clears the whole matrix, even places which are
  out of current matrix edge
*/
void Klaus::Matrix4::clearMatrix()
{
  operator=(identity);
}

/**
 * This method calculates the matrix determinant. The determinant is a number
   which is gotten from the matrix and represents some of the characteristics
   of the matrix, like whether the matrix has an inverse matrix or not.
  @return Matrix4 Determinant
 */
double Klaus::Matrix4::getDeterminant() const
{
  double v1 = mElements[0][0] * (mElements[1][1] * mElements[2][2] - mElements[2][1] * mElements[1][2]);
  double v2 = mElements[1][0] * (mElements[0][1] * mElements[2][2] - mElements[2][1] * mElements[0][2]);
  double v3 = mElements[2][0] * (mElements[0][1] * mElements[1][2] - mElements[1][1] * mElements[0][2]);
  return v1 - v2 + v3;
}

/**
 * This method calculates inverse matrix of the current matrix
                                                                  -1
   Given a matrix M, then the inverse of that matrix, denoted as M  , is
   the matrix which satisfies the following expression:

          -1
     M . M   = I

   where I is the identity matrix.
   Thus, multiplying a matrix with its inverse will generate the identity matrix.
  @return Inverse matrix of the current matrix
 */
Klaus::Matrix4 Klaus::Matrix4::getInverseMatrix() const
{
  double m00 = mElements[0][0], m01 = mElements[0][1], m02 = mElements[0][2], m03 = mElements[0][3];
  double m10 = mElements[1][0], m11 = mElements[1][1], m12 = mElements[1][2], m13 = mElements[1][3];
  double m20 = mElements[2][0], m21 = mElements[2][1], m22 = mElements[2][2], m23 = mElements[2][3];
  double m30 = mElements[3][0], m31 = mElements[3][1], m32 = mElements[3][2], m33 = mElements[3][3];

  double v0 = m20 * m31 - m21 * m30;
  double v1 = m20 * m32 - m22 * m30;
  double v2 = m20 * m33 - m23 * m30;
  double v3 = m21 * m32 - m22 * m31;
  double v4 = m21 * m33 - m23 * m31;
  double v5 = m22 * m33 - m23 * m32;

  double t00 = + (v5 * m11 - v4 * m12 + v3 * m13);
  double t10 = - (v5 * m10 - v2 * m12 + v1 * m13);
  double t20 = + (v4 * m10 - v2 * m11 + v0 * m13);
  double t30 = - (v3 * m10 - v1 * m11 + v0 * m12);

  double invDet = 1 / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

  double d00 = t00 * invDet;
  double d10 = t10 * invDet;
  double d20 = t20 * invDet;
  double d30 = t30 * invDet;

  double d01 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
  double d11 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
  double d21 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
  double d31 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

  v0 = m10 * m31 - m11 * m30;
  v1 = m10 * m32 - m12 * m30;
  v2 = m10 * m33 - m13 * m30;
  v3 = m11 * m32 - m12 * m31;
  v4 = m11 * m33 - m13 * m31;
  v5 = m12 * m33 - m13 * m32;

  double d02 = + (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
  double d12 = - (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
  double d22 = + (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
  double d32 = - (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

  v0 = m21 * m10 - m20 * m11;
  v1 = m22 * m10 - m20 * m12;
  v2 = m23 * m10 - m20 * m13;
  v3 = m22 * m11 - m21 * m12;
  v4 = m23 * m11 - m21 * m13;
  v5 = m23 * m12 - m22 * m13;

  double d03 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
  double d13 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
  double d23 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
  double d33 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

  return Matrix4(
                  d00, d01, d02, d03,
                  d10, d11, d12, d13,
                  d20, d21, d22, d23,
                  d30, d31, d32, d33
                );
}

/**
 * This method returns the transpose of the matrix
   The transpose of matrix is the matrix generated when every element in
   the matrix is swapped with the opposite relative to the major diagonal
   This can be expressed as the mathematical operation:

     M'   = M
       ij    ji

   However, this can only be performed if a matrix has an equal number
   of rows and columns.
   If the matrix is a rotation matrix, then the transpose is guaranteed
   to be the inverse of the matrix.
  @return Matrix4's transpose
 */
Klaus::Matrix4 Klaus::Matrix4::getTranspose() const
{
  return Matrix4(mElements[0][0], mElements[1][0], mElements[2][0], mElements[3][0],
                 mElements[0][1], mElements[1][1], mElements[2][1], mElements[3][1],
                 mElements[0][2], mElements[1][2], mElements[2][2], mElements[3][2],
                 mElements[0][3], mElements[1][3], mElements[2][3], mElements[3][3]);
}

/**
 * This method checks whether the given matrix is affine.
    An affine matrix is a 4x4 matrix with row 3 equal to (0, 0, 0, 1),
    e.g. no projective coefficients.
  @return True if the matrix is affine, false otherwise
 */
bool Klaus::Matrix4::isAffine() const
{
  return mElements[3][0] == 0 && mElements[3][1] == 0 && mElements[3][2] == 0 && mElements[3][3] == 1;
}

/**
 * This method transforms the given point by the matrix, but the matrix
   is affine and calculations could be more faster.
  @param pos Point to transform with the matrix
  @return Transformed point
 */
Klaus::VecPosition Klaus::Matrix4::transformAffine( const VecPosition & pos ) const
{
  return VecPosition(
                      mElements[0][0]*pos[0] + mElements[0][1]*pos[1] + mElements[0][2]*pos[2] + mElements[0][3],
                      mElements[1][0]*pos[0] + mElements[1][1]*pos[1] + mElements[1][2]*pos[2] + mElements[1][3],
                      mElements[2][0]*pos[0] + mElements[2][1]*pos[1] + mElements[2][2]*pos[2] + mElements[2][3]
                    );
}

/**
 * This method creates a standard 4x4 transformation from a rotation/scaling Quaternion.
   Will not change the matrix's position elements.
  @param q Quaternion to build the matrix from
 */
void Klaus::Matrix4::fromQuaternion( const Quaternion & q )
{
  operator=(q.toMatrix3());
}

/**
 * This method converts the matrix to a quaternion
  @return Quaternion representation of the matrix
 */
Klaus::Quaternion Klaus::Matrix4::toQuaternion() const
{
  return Quaternion(toMatrix3());
}

/**
 * This method creates the Matrix4 by using a Matrix3
  @param mat The matrix3 to create the Matrix4 based on it
 */
void Klaus::Matrix4::fromMatrix3( const Matrix3 & mat )
{
  operator=(mat);
}

/**
 * This method converts the Matrix4 to a Matrix3
  @return Matrix3 representation of the matrix
 */
Klaus::Matrix3 Klaus::Matrix4::toMatrix3() const
{
  Matrix3 m;
  m.mElements[0][0] = mElements[0][0];
  m.mElements[0][1] = mElements[0][1];
  m.mElements[0][2] = mElements[0][2];
  m.mElements[1][0] = mElements[1][0];
  m.mElements[1][1] = mElements[1][1];
  m.mElements[1][2] = mElements[1][2];
  m.mElements[2][0] = mElements[2][0];
  m.mElements[2][1] = mElements[2][1];
  m.mElements[2][2] = mElements[2][2];

  return m;
}

/**
 * This overloaded assignment operator, assigns the supplied matrix to current one
  @param mat Matrix4 to copy to the current one
  @return Matrix4 reference to enable recursive assignment.
 */
Klaus::Matrix4 & Klaus::Matrix4::operator = ( const Matrix4 & mat )
{
  memcpy(mElements, mat.mElements, 16 * sizeof(double));

  return *this;
}

/**
 * This overloaded assignment operator copies the matrix3 supplied to it to the Matrix4 elements.
   Will not scale, translation and projection components.
  @param mat Matrix3 instance to copy into the Matrix4
  @return Matrix4 reference to enable recursive assignment
 */
Klaus::Matrix4 & Klaus::Matrix4::operator = ( const Matrix3& mat )
{
  mElements[0][0] = mat.mElements[0][0]; mElements[0][1] = mat.mElements[0][1]; mElements[0][2] = mat.mElements[0][2];
  mElements[1][0] = mat.mElements[1][0]; mElements[1][1] = mat.mElements[1][1]; mElements[1][2] = mat.mElements[1][2];
  mElements[2][0] = mat.mElements[2][0]; mElements[2][1] = mat.mElements[2][1]; mElements[2][2] = mat.mElements[2][2];

  return *this;
}

/**
 * This overloaded operator fills current Matrix4 with the supplied double value
  @param val Value to copy to all elements of Matrix4
  @return Matrix4 reference to enable recursive copying.
 */
Klaus::Matrix4 & Klaus::Matrix4::operator = ( const double & val )
{
  for( unsigned j = 0; j < 4; j++ )
    for( unsigned i = 0; i < 4; i++ )
      mElements[j][i] = val;

  return *this;
}

/**
 * This overloaded operator sums the Matrix4 supplied to the current one
 * and returns the result
  @param mat Matrix4 to sum to current one
  @return Summation result
 */
Klaus::Matrix4 Klaus::Matrix4::operator + ( const Matrix4 & mat ) const
{
  Matrix4 m;

  for( unsigned y = 0; y < 4; y++ )
    for( unsigned x = 0; x < 4; x++ )
      m[y][x] = mElements[y][x] + mat.mElements[y][x];

  return m;
}

/**
 * This overloaded operator sums the value supplied with the current one
 * and returns the result
  @param val Value to sum
  @return Summation result
 */
Klaus::Matrix4 Klaus::Matrix4::operator + ( const double & val ) const
{
  Matrix4 m;

  for( unsigned y = 0; y < 4; y++ )
    for( unsigned x = 0; x < 4; x++ )
      m.mElements[y][x] = mElements[y][x] + val;

  return m;
}

/**
 * This overloaded operator subtracts the Matrix4 supplied from the current one
 * and returns the result
  @param mat Matrix4 to subtract
  @return Subtract result
 */
Klaus::Matrix4 Klaus::Matrix4::operator - ( const Matrix4 & mat ) const
{
  Matrix4 m;

  for( unsigned y = 0; y < 4; y++ )
    for( unsigned x = 0; x < 4; x++ )
      m[y][x] = mElements[y][x] - mat[y][x];

  return m;
}

/**
 * This overloaded operator subtracts the value supplied from the current one
 * and returns the result
  @param val Value to subtract
  @return Subtract result
 */
Klaus::Matrix4 Klaus::Matrix4::operator - ( const double & val ) const
{
  Matrix4 m;

  for( unsigned y = 0; y < 4; y++ )
    for( unsigned x = 0; x < 4; x++ )
      m[y][x] = mElements[y][x] - val;

  return m;
}

/**
 * This method calculates multiplication of a row of a matrix by a coloumn of another matrix.
 * This method is used to calculate 4x4 Matrix4 multiplication
  @param va First element of first matrix's row
  @param vb Second element of first matrix's row
  @param vc Third element of first matrix's row
  @param vd Forth element of first matrix's row
  @param ve First element of second matrix's coloumn
  @param vf Second element of second matrix's coloumn
  @param vg Third element of second matrix's coloumn
  @param vh Forth element of second matrix's coloumn
  @return Multiplication of row by column
 */
inline double calculateRowCrossColumn(double va,double vb,double vc,double vd,double ve,double vf, double vg,double vh)
{
  return ( va*ve + vb*vf + vc*vg + vd*vh );
}

/**
 * This overloaded operator multiplies the Matrix4 supplied with the current one
 * and returns the result
  @param mat Matrix4 to multiply with
  @return Multiplication result
 */
Klaus::Matrix4 Klaus::Matrix4::operator * ( const Matrix4 & mat ) const
{
  Matrix4 m;

  for(unsigned y = 0; y < 4; y++)
    for(unsigned x = 0; x < 4; x++)
      m[y][x] = calculateRowCrossColumn
                      (
                            mElements[y][0],     mElements[y][1],     mElements[y][2],     mElements[y][3],
                        mat.mElements[0][x], mat.mElements[1][x], mat.mElements[2][x], mat.mElements[3][x]
                      );

  return m;
}

/**
 * This overloaded operator multiplies the value supplied with the current one
 * and returns the result
  @param val value to multiply with
  @return Multiplication result
 */
Klaus::Matrix4 Klaus::Matrix4::operator * ( const double & val ) const
{
  Matrix4 m;

  for( unsigned y = 0; y < 4; y++ )
    for( unsigned x = 0; x < 4; x++ )
      m[y][x] = m[y][x] * val;

  return m;
}

/**
 * This overloaded operator divides this Matrix4 to the supplied one
 * and returns the result
  @param mat Matrix4 to divide this into
  @return Division result
 */
Klaus::Matrix4 Klaus::Matrix4::operator / ( const Matrix4 & mat ) const
{
  return *this * mat.getInverseMatrix();
}

/**
 * This overloaded operator divides this Matrix4 with the supplied value
 * and returns the result
  @param val value to divide this into
  @return Division result
 */
Klaus::Matrix4 Klaus::Matrix4::operator / ( const double & val ) const
{
  Matrix4 m;
  m = *this;
  double dd = 1.0 / val;

  for(unsigned y = 0; y < 4; y++)
    for(unsigned x = 0; x < 4; x++)
      m[y][x] *= dd;

  return m;
}

/*! Overloaded version of the sum-assignment operator for Matrixes. It
    returns the sum of the current Matrix4 and the one supplied by adding
    their elements together. This changes the current Matrix4 itself.
    \param mat Matrix4 to be added */
Klaus::Matrix4 & Klaus::Matrix4::operator += ( const Matrix4 & mat )
{
  *this = *this + mat;
  return *this;
}

/*! Overloaded version of the sum-assignment operator for Matrixes. It
    returns the sum of the current Matrix4 and the value supplied by adding
    the values to elements. This changes the current Matrix4 itself.
    \param val Value to be added */
Klaus::Matrix4 & Klaus::Matrix4::operator += ( const double & val )
{
  *this = *this + val;
  return *this;
}

/*! Overloaded version of the subtract-assignment operator for Matrixes. It
    returns the subtract of the current Matrix4 and the one supplied by subtracting
    their elements from each other. This changes the current Matrix4 itself.
    \param mat Matrix4 to be subtracted */
Klaus::Matrix4 & Klaus::Matrix4::operator -= ( const Matrix4 & mat )
{
  *this = *this - mat;
  return *this;
}

/*! Overloaded version of the subtract-assignment operator for Matrixes.
    It returns the subtract of the current Matrix4 and the value supplied
    by subtracting the values from elements. This changes the current
    Matrix4 itself.
    \param val Value to be subtracted */
Klaus::Matrix4 & Klaus::Matrix4::operator -= ( const double & val )
{
  *this = *this - val;
  return (*this);
}

/*! Overloaded version of the multiply-assignment operator for Matrixes. It
    returns the multiplication of the current Matrix4 and the one supplied.
    This changes the current Matrix4 itself.
    \param mat Matrix4 to be multiplied */
Klaus::Matrix4 & Klaus::Matrix4::operator *= ( const Matrix4 & mat )
{
  *this = *this * mat;
  return *this;
}

/*! Overloaded version of the multiplication-assignment operator for
    Matrixes. It returns the multiplication of the current Matrix4 and
    the value supplied by multiplying the values to elements. This
    changes the current Matrix4 itself.
    \param val Value to be subtracted */
Klaus::Matrix4 & Klaus::Matrix4::operator *= ( const double & val )
{
  *this = *this * val;
  return *this;
}

/*! Overloaded version of the division-assignment operator for Matrixes.
    It returns the division of the current Matrix4 and the one supplied.
    This changes the current Matrix4 itself.
    \param mat Matrix4 to be devided */
Klaus::Matrix4 & Klaus::Matrix4::operator /= ( const Matrix4 & mat )
{
  *this = *this / mat;
  return *this;
}

/*! Overloaded version of the division-assignment operator for Matrixes.
    It returns the division of the current Matrix4 and the value supplied
    by dividing the elements by the value. This changes the current
    Matrix4 itself.
    \param val Value to be subtracted */
Klaus::Matrix4 & Klaus::Matrix4::operator /= ( const double & val )
{
  *this = *this / val;
  return *this;
}

/**
 * This operator provides ez access to member elements
  @param iIndex Index of requested row
  @return Requested row as a double array
 */
const double * Klaus::Matrix4::operator [] ( const int & iIndex ) const
{
  if( iIndex < 0 || iIndex > 3 )
    std::cerr << "(Klaus::Matrix4::operator []) index out of range: " << iIndex << std::endl;

  return mElements[iIndex];
}

/**
 * This operator provides ez access to member elements
  @param iIndex Index of requested row
  @return Requested row as a double array
 */
double * Klaus::Matrix4::operator [] ( const int & iIndex )
{
  if( iIndex < 0 || iIndex > 3 )
    std::cerr << "(Klaus::Matrix4::operator []) index out of range: " << iIndex << std::endl;

  return mElements[iIndex];
}

/**
 * This is the boolean operator for equality check between current
 * matrix and the one supplied
  @param mat Matrix4 to check if unequal to current matrixs
  @return True if matrix supplied is identical to current one, false otherwise
 */
bool Klaus::Matrix4::operator == ( const Matrix4 & mat ) const
{
  for(unsigned y = 0; y < 4; y++)
    for(unsigned x = 0; x < 4; x++)
      if(mElements[y][x] != mat.mElements[y][x])
        return false;

  return true;
}

/**
 * This is the boolean operator for checking unequality between current
 * matrix and the one supplied
  @param mat Matrix4 to check if unequal to current matrixs
  @return True if matrix supplied is identical to current one, false otherwise
 */
bool Klaus::Matrix4::operator != ( const Matrix4 & mat ) const
{
  return ! (*this == mat);
}

/**
 * This method performs a standard transformation on the given point with the elements of matrix.
   The transformation consists of the following stages:
     - Relative rotation of the point is calculated (this includes scaling as well)
     - Translation is added
     - The result is projected

   matrix * point [3x3 * 3x1 = 3x1]
  @param point The given point to transform
  @return Transformed position of the point by the matrix
 */
Klaus::VecPosition Klaus::Matrix4::operator * ( const VecPosition & point ) const
{
  VecPosition r;
  double fInvW = 1.0 / (mElements[3][0]*point[0] + mElements[3][1]*point[1] + mElements[3][2]*point[2] + mElements[3][3]);

  r[0] = (mElements[0][0]*point[0] + mElements[0][1]*point[1] + mElements[0][2]*point[2] + mElements[0][3]) * fInvW;
  r[1] = (mElements[1][0]*point[0] + mElements[1][1]*point[1] + mElements[1][2]*point[2] + mElements[1][3]) * fInvW;
  r[2] = (mElements[2][0]*point[0] + mElements[2][1]*point[1] + mElements[2][2]*point[2] + mElements[2][3]) * fInvW;

  return r;
}

/**
 * This method rotates the given point by the Matrix. The matrix should be a rotation matrix.
   vector * matrix [1x3 * 3x3 = 1x3]
  @param point The given point to rotate
  @param mat The matrix to rotate the given point
  @return Rotated position of the point by the matrix
 */
Klaus::VecPosition operator * ( const Klaus::VecPosition & point, const Klaus::Matrix4 & mat )
{
  Klaus::VecPosition r;
  double fInvW = 1.0 / ( mat[3][0] * point[0] + mat[3][1] * point[1] + mat[3][2] * point[2] + mat[3][3] );

  r[0] = ( mat[0][0] * point[0] + mat[0][1] * point[1] + mat[0][2] * point[2] + mat[0][3] ) * fInvW;
  r[1] = ( mat[1][0] * point[0] + mat[1][1] * point[1] + mat[1][2] * point[2] + mat[1][3] ) * fInvW;
  r[2] = ( mat[2][0] * point[0] + mat[2][1] * point[1] + mat[2][2] * point[2] + mat[2][3] ) * fInvW;

  return r;
}

/**
 * This is the overloaded output operator.
 * Prints the matrix into the output stream supplied
  @param os Output stream to print matrix to
  @param m Matrix4 to print to stream
  @return Output stream reference
 */
std::ostream & operator << ( std::ostream &os, Klaus::Matrix4 m )
{
  os << m.str();
  return os;
}

/**
 * This method saves the internal elements of the Matrix4 as a std::string and returns them
  @return String containing the matrix information
 */
std::string Klaus::Matrix4::str() const
{
  std::stringstream buf;
  buf << setdecimal(2);

  buf << "(Matrix4 (";
  buf << mElements[0][0] << " " << mElements[0][1] << " " << mElements[0][2] << " " << mElements[0][3] << " " <<
         mElements[1][0] << " " << mElements[1][1] << " " << mElements[1][2] << " " << mElements[1][3] << " " <<
         mElements[2][0] << " " << mElements[2][1] << " " << mElements[2][2] << " " << mElements[2][3] << " " <<
         mElements[3][0] << " " << mElements[3][1] << " " << mElements[3][2] << " " << mElements[3][3] << "))";

  return buf.str();
}

/**
 * This method creates a tranformation matrix from the given translation,
   scaling and rotation parameters. Transform is performed in the order
   rotate, translation, scale. i.e. translation is independent of orientation
   axes, scale does not affect size of translation, rotation and scaling are
   always centered on the origin.
  @param tr Translation of the transform
  @param rot rotation of the transform
  @param sc scaling of the transform
  @return A transformation matrix build by the given parameters
 */
Klaus::Matrix4 Klaus::Matrix4::createTransform( const VecPosition & tr, const Quaternion & rot, const VecPosition & sc )
{
  // rotation
  Matrix4 result = rot;

  // translation
  result.mElements[0][3] = tr[0];
  result.mElements[1][3] = tr[1];
  result.mElements[2][3] = tr[2];

  // scaling
  result.mElements[0][0] = sc[0];
  result.mElements[1][1] = sc[1];
  result.mElements[2][2] = sc[2];

  return result;
}

/**
 * This method creates a tranformation matrix from the given translation
   and rotation parameters. Transform is performed in the order
   rotation, translation, i.e. translation is independent of orientation
   axes and rotation is always centered on the origin.
  @param tr Translation of the transform
  @param rot rotation of the transform
  @return A transformation matrix build by the given parameters
 */
Klaus::Matrix4 Klaus::Matrix4::createTransform(const VecPosition & tr, const Quaternion & rot)
{
  // rotation
  Matrix4 result = rot;

  // translation
  result.mElements[0][3] = tr[0];
  result.mElements[1][3] = tr[1];
  result.mElements[2][3] = tr[2];

  return result;
}

/**
 * This method creates a translation matrix based on the given translation
  @param tr The translation to build a transform matrix based on
  @return A transform matrix which translates by the given value
 */
Klaus::Matrix4 Klaus::Matrix4::getTranslationMatrix( const VecPosition & tr )
{
  return Matrix4(
                  1.0, 0.0, 0.0, tr[0],
                  0.0, 1.0, 0.0, tr[1],
                  0.0, 0.0, 1.0, tr[2],
                  0.0, 0.0, 0.0, 1.0
                );
}

/**
 * This method creates a translation matrix based on the given translation
  @param dx X component of translation
  @param dy Y component of translation
  @param dz Z component of translation
  @return A transform matrix which translates by the given values
 */
Klaus::Matrix4 Klaus::Matrix4::getTranslationMatrix( const double & dx, const double & dy, const double & dz )
{
  return Matrix4(
                  1.0, 0.0, 0.0, dx,
                  0.0, 1.0, 0.0, dy,
                  0.0, 0.0, 1.0, dz,
                  0.0, 0.0, 0.0, 1.0
                );
}

/**
 * This method creates a scale matrix based on the given scale
  @param tr The scale to build a transform matrix based on
  @return A transform matrix which scales by the given value
 */
Klaus::Matrix4 Klaus::Matrix4::getScaleMatrix( const VecPosition & tr )
{
  return Matrix4(
                  tr[0], 0.0,   0.0,   0.0,
                  0.0,   tr[1], 0.0,   0.0,
                  0.0,   0.0,   tr[2], 0.0,
                  0.0,   0.0,   0.0,   1.0
                );
}

/**
 * This method creates a scale matrix based on the given scale
  @param dx X component of scale
  @param dy Y component of scale
  @param dz Z component of scale
  @return A transform matrix which scales by the given values
 */
Klaus::Matrix4 Klaus::Matrix4::getScaleMatrix( const double & dx, const double & dy, const double & dz )
{
  return Matrix4(
                  dx,  0.0, 0.0, 0.0,
                  0.0, dy,  0.0, 0.0,
                  0.0, 0.0, dz,  0.0,
                  0.0, 0.0, 0.0, 1.0
                );
}

/**
 * This method returns translation value of matrix
  @return Translation value of matrix
 */
Klaus::VecPosition Klaus::Matrix4::getTranslation() const
{
  return VecPosition( mElements[0][3], mElements[1][3], mElements[2][3] );
}

/**
 * This method sets the translation part of the matrix
  @param tr Translation part of the matrix
 */
void Klaus::Matrix4::setTranslation( const VecPosition & tr )
{
  mElements[0][3] = tr[0];
  mElements[1][3] = tr[1];
  mElements[2][3] = tr[2];
}

/**
 * This method sets the translation part of the matrix.
  @param x X coordinate of translation
  @param y Y coordinate of translation
  @param z Z coordinate of translation
 */
void Klaus::Matrix4::setTranslation(const double& x, const double& y, const double& z)
{
  mElements[0][3] = x;
  mElements[1][3] = y;
  mElements[2][3] = z;
}

/**
 * This method returns scale value of matrix
  @return Scale value of matrix
*/
Klaus::VecPosition Klaus::Matrix4::getScale() const
{
  return VecPosition( mElements[0][0], mElements[1][1], mElements[2][2] );
}

/**
 * This method sets the scale part of the matrix
  @param sc Scale part of the matrix
*/
void Klaus::Matrix4::setScale( const VecPosition & sc )
{
  mElements[0][0] = sc[0];
  mElements[1][1] = sc[1];
  mElements[2][2] = sc[2];
}

/**
 * This method sets the scale element of the matrix.
  @param x X coordinate of scale
  @param y Y coordinate of scale
  @param z Z coordinate of scale
 */
void Klaus::Matrix4::setScale(const double& x, const double& y, const double& z)
{
  mElements[0][0] = x;
  mElements[1][1] = y;
  mElements[2][2] = z;
}

/**
  This method returns the orientation element of matrix.
  @return Orientation/Rotation element of the matrix
*/
Klaus::Quaternion Klaus::Matrix4::getOrientation() const
{
  return toQuaternion();
}

/**
  This method sets the orientation element of quaternion.
  @param q Orientation element of matrix.
*/
void Klaus::Matrix4::setOrientation(const Klaus::Quaternion& q)
{
  fromQuaternion(q);
}

/**
 * This method normalizes the rotation/scaling matrix inside the transformation matrix.
 */
void Klaus::Matrix4::normalize()
{
  Quaternion q = *this;
  q.normalize();
  fromQuaternion(q);
}

/**
  This method dissects the matrix into it's creating components.
  @param p Translation will be copied here
  @param q Orientation will be copied here
*/
void Klaus::Matrix4::dissect(VecPosition& p, Quaternion& q)
{
  p.setPosition(mElements[0][3], mElements[1][3], mElements[2][3]);
  q = toQuaternion();
}

/**
 * This method dissects the matrix into it's creating components.
  @param p Translation will be copied here
  @param q Orientation will be copied here
  @param s Scale will be copied here
 */
void Klaus::Matrix4::dissect(VecPosition& p, Quaternion& q, VecPosition& s)
{
  p.setPosition(mElements[0][3], mElements[1][3], mElements[2][3]);
  s.setPosition(mElements[0][0], mElements[1][1], mElements[2][2]);
  q = toQuaternion();
}



/*****************************************************************************/
/***************************** Class Quaternion  *****************************/
/*****************************************************************************/



const Klaus::Quaternion Klaus::Quaternion::zero(0.0,0.0,0.0,0.0);
const Klaus::Quaternion Klaus::Quaternion::identity(1.0,0.0,0.0,0.0);

/**
 * This is a constructor for quaternions, which builds from four number
 * One scaler part and three imaginary parts
  @param w Scalar part of quaternion
  @param x X coordinate of imaginary axis
  @param y Y coordinate of imaginary axis
  @param z Z coordinate of imaginary axis
 */
Klaus::Quaternion::Quaternion( double w, double x, double y, double z )
{
  mW = w;
  mX = x;
  mY = y;
  mZ = z;
}

/**
 * This contructor builds the quaternion from a rotation matrix
  @param rot_mat Rotation matrix
 */
Klaus::Quaternion::Quaternion( const Matrix3 & rot_mat )
{
  fromMatrix3( rot_mat );
}

/**
 * This constructor creates the quaternion from the given 4x4 matrix
  @param rot_mat The matrix to build the quaternion from
 */
Klaus::Quaternion::Quaternion(const Matrix4 &rot_mat)
{
  *this = rot_mat.toQuaternion();
}

/**
 * This constructor build the quaternion from an imaginary axis and a real angle
  @param ang The angle
  @param ax The imaginary axis
 */
Klaus::Quaternion::Quaternion( const Degree & ang, const VecPosition & ax )
{
  fromAngleAxis(ang, ax);
}

/**
 * This constructor creates a quaternion from a given vector
  @param pos Position to create a quaternion from
 */
Klaus::Quaternion::Quaternion( const VecPosition & pos )
{
  mW = 0;
  mX = pos[0];
  mY = pos[1];
  mZ = pos[2];
}

/**
 * This is the default constructor of Quaternions, initializes to identity quaternion.
 */
Klaus::Quaternion::Quaternion()
{
  *this = Klaus::Quaternion::identity;
}

/**
 * Overloaded version of + operator
  @param q Quaternion to calculate sum with this one
  @return Sum of the quaternion with the parameter
 */
Klaus::Quaternion Klaus::Quaternion::operator+ (const Quaternion& q) const
{
  return Quaternion(mW+q.mW,mX+q.mX,mY+q.mY,mZ+q.mZ);
}

/**
 * Overloaded version of += operator
  @param q Quaternion to add
  @return Resulting quaternion of addition of current and given quaternion
 */
Klaus::Quaternion& Klaus::Quaternion::operator += ( const Quaternion& q )
{
  *this = *this + q;
  return *this;
}

/**
 * Overloaded version of - operator
  @param q Quaternion to calculate subtract with this one
  @return Subtract of the parameter from the quaternion
 */
Klaus::Quaternion Klaus::Quaternion::operator- (const Quaternion& q) const
{
  return Quaternion(mW-q.mW,mX-q.mX,mY-q.mY,mZ-q.mZ);
}

/**
 * Overloaded version of -= operator
  @param q Quaternion to subtract
  @return Resulting quaternion from subtracting given quaternion from current quaternion
 */
Klaus::Quaternion& Klaus::Quaternion::operator -= ( const Quaternion& q )
{
  *this = *this - q;
  return *this;
}

/**
 * Overloaded version of negating operator
  @return Negation of the current quaternion
 */
Klaus::Quaternion Klaus::Quaternion::operator- () const
{
  return Quaternion(-mW,-mX,-mY,-mZ);
}

/**
 * Overloaded version of multimplication
  @param q The multiplier
  @return Multiplication of the quaternion by the multiplier
 */
Klaus::Quaternion Klaus::Quaternion::operator* (const Quaternion& q) const
{
  /// Note that multiplication is not generally commutative, so in most
  /// cases p*q != q*p.

  return Quaternion
  (
    mW * q.mW - mX * q.mX - mY * q.mY - mZ * q.mZ,
    mW * q.mX + mX * q.mW + mY * q.mZ - mZ * q.mY,
    mW * q.mY + mY * q.mW + mZ * q.mX - mX * q.mZ,
    mW * q.mZ + mZ * q.mW + mX * q.mY - mY * q.mX
  );
}

/**
 * Overloaded version of *= operator
  @param q Quaternion to multiply
  @return Resulting quaternion from multiplication of current and given quaternion
 */
Klaus::Quaternion& Klaus::Quaternion::operator *= ( const Quaternion& q )
{
  *this = *this * q;
  return *this;
}

/**
 * Overloaded version of multimplication
  @param d The multiplier
  @return Multiplication of the quaternion by the multiplier
 */
Klaus::Quaternion Klaus::Quaternion::operator* (double d) const
{
  return Quaternion(d*mW,d*mX,d*mY,d*mZ);
}

/**
 * This method rotates the given vector by the factor of the quaternion
   and returns the result
  @param vec The vector to be rotated
  @return Rotated result of the vector
 */
Klaus::VecPosition Klaus::Quaternion::operator * ( const VecPosition& vec ) const
{
  // nVidia SDK implementation
  VecPosition uv, uuv;
  VecPosition qvec(mX, mY, mZ);
  uv = qvec.cross(vec);
  uuv = qvec.cross(uv);
  uv *= (2.0f * mW);
  uuv *= 2.0f;

  return vec + uv + uuv;
}

/**
 * Overloaded version of division
  @param q The dividor
  @return Division of the quaternion by the dividor
 */
Klaus::Quaternion Klaus::Quaternion::operator / (const Quaternion& q) const
{
  /// Note that multiplication is not generally commutative, so in most
  /// cases p*q != q*p.
  /// So, we can not always write p/q, instead we should write q*q^-1

  return *this * q.getInverse();
}

/**
 * Overloaded version of /= operator
  @param q Dividor
  @return Resulting quaternion from division of current by given quaternion
 */
Klaus::Quaternion& Klaus::Quaternion::operator /= ( const Quaternion& q )
{
  *this = *this / q;
  return *this;
}


/**
 * Index operator for direct access to values in the quaternion
  @param index Index of the value in the quaternion
  @return The value in the quaternion
 */
double Klaus::Quaternion::operator [] ( const int & index ) const
{
  if( index > 3 || index < 0)
  {
    std::cerr << "(Klaus::Quaternion::operator []) index out of range: " << index << std::endl;
    return 0;
  }
  else
    return *(&mW+index);
}

/**
 * Index operator for direct access to values in the quaternion
  @param index Index of the value in the quaternion
  @return The value in the quaternion
 */
double & Klaus::Quaternion::operator [] ( const int & index )
{
  if( index > 3 || index < 0)
  {
    std::cerr << "(Klaus::Quaternion::operator []) index out of range: " << index << std::endl;
    static double dOpenratorBracketTemp = 0;
    return dOpenratorBracketTemp;
  }
  else
    return *(&mW+index);
}

/**
 * Overloaded operator implementation for copying a quaternion to another
  @param q The source quaternion
  @return Quaternion itself to enable recursive usage
 */
Klaus::Quaternion& Klaus::Quaternion::operator = ( const Quaternion & q )
{
  mW = q.mW;
  mX = q.mX;
  mY = q.mY;
  mZ = q.mZ;

  return *this;
}

/**
 * Overlaoded assign operator for quaternions. Copies a position to the quaternion
  @param p The position to copy to the quaternion
  @return Quaternion itself to enable recursive usage
 */
Klaus::Quaternion& Klaus::Quaternion::operator = ( const VecPosition & p )
{
  mW = 0;
  mX = p[0];
  mY = p[1];
  mZ = p[2];

  return *this;
}

/**
 * Overloaded version of equality operator to test two quaternion's equality
  @param q The compare pair
  @return True if the quaternions are alike, false otherwise
 */
bool Klaus::Quaternion::operator == ( const Quaternion & q ) const
{
  return q.mW == mW && q.mX == mX && q.mY == mY && q.mZ == mZ;
}

/**
 * Overloaded version of inequality operator to test two quaternion's inequality
  @param q The compare pair
  @return False if the quaternions are alike, true otherwise
 */
bool Klaus::Quaternion::operator != ( const Quaternion & q ) const
{
  return q.mW != mW || q.mX != mX || q.mY != mY || q.mZ != mZ;
}


/**
 * This method loads the quaternion from a 3 dimentional rotation matrix. It is the algorithm
   in Ken Shoemake's article in 1987 SIGGRAPH course notes, presented in the article "Quaternion
   Calculus and Fast Animation".
  @param mRot The rotation matrix to initializes the quaternion from
 */
void Klaus::Quaternion::fromMatrix3(const Matrix3 & mRot)
{
  double fTrace = mRot[0][0] + mRot[1][1] + mRot[2][2];
  double fRoot;

  if ( fTrace > 0.0 )
  {
    // |w| > 1/2, may as well choose w > 1/2
    fRoot = sqrt(fTrace + 1.0);  // 2w
    mW = 0.5*fRoot;
    fRoot = 0.5/fRoot;  // 1/(4w)
    mX = (mRot[2][1]-mRot[1][2])*fRoot;
    mY = (mRot[0][2]-mRot[2][0])*fRoot;
    mZ = (mRot[1][0]-mRot[0][1])*fRoot;
  }
  else
  {
    // |w| <= 1/2
    static size_t s_iNext[3] = { 1, 2, 0 };
    size_t i = 0;
    if( mRot[1][1] > mRot[0][0] )
      i = 1;
    if( mRot[2][2] > mRot[i][i] )
      i = 2;
    size_t j = s_iNext[i];
    size_t k = s_iNext[j];

    fRoot = sqrt(mRot[i][i]-mRot[j][j]-mRot[k][k] + 1.0);
    double* apkQuat[3] = { &mX, &mY, &mZ };
    *apkQuat[i] = 0.5*fRoot;
    fRoot = 0.5/fRoot;
    mW = (mRot[k][j]-mRot[j][k])*fRoot;
    *apkQuat[j] = (mRot[j][i]+mRot[i][j])*fRoot;
    *apkQuat[k] = (mRot[k][i]+mRot[i][k])*fRoot;
  }

  normalize();
}

/**
 * This method converts the Quaternion as a 3 dimentional rotation matrix and returns the matrix
  @return Rotation matrix which holds the roation parameteres of the quaternion
 */
Klaus::Matrix3 Klaus::Quaternion::toMatrix3() const
{
  Matrix3 mRot;

  double fTx  = 2.0*mX;
  double fTy  = 2.0*mY;
  double fTz  = 2.0*mZ;
  double fTwx = fTx*mW;
  double fTwy = fTy*mW;
  double fTwz = fTz*mW;
  double fTxx = fTx*mX;
  double fTxy = fTy*mX;
  double fTxz = fTz*mX;
  double fTyy = fTy*mY;
  double fTyz = fTz*mY;
  double fTzz = fTz*mZ;

  mRot[0][0] = 1.0-(fTyy+fTzz);
  mRot[0][1] = fTxy-fTwz;
  mRot[0][2] = fTxz+fTwy;
  mRot[1][0] = fTxy+fTwz;
  mRot[1][1] = 1.0-(fTxx+fTzz);
  mRot[1][2] = fTyz-fTwx;
  mRot[2][0] = fTxz-fTwy;
  mRot[2][1] = fTyz+fTwx;
  mRot[2][2] = 1.0-(fTxx+fTyy);

  return mRot;
}

/**
 * This method laods the quaternion from an axis angle pair
  @param dAng Angle of the rotation around the axis
  @param vecAxis Imaginary axis of the rotation
 */
void Klaus::Quaternion::fromAngleAxis(const Degree & dAng, const VecPosition& vecAxis)
{
  // assert:  axis[] is unit length
  //
  // The quaternion representing the rotation is
  //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

  double s = (dAng * 0.5).sin();
  VecPosition v = vecAxis;
  v.normalize();

  mW = (dAng * 0.5).cos();
  mX = s * v[0];
  mY = s * v[1];
  mZ = s * v[2];
}

/**
 * This method saves the quaternion as an axis-angle value.
  @param dAng Angle of rotation
  @param vecAxis Imaginary axis of rotation
 */
void Klaus::Quaternion::toAngleAxis(Degree & dAng, VecPosition& vecAxis) const
{
  // The quaternion representing the rotation is
  //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

  double fSqrLength = mX*mX+mY*mY+mZ*mZ;
  if ( fSqrLength > 0.0 )
  {
    dAng = Degree::acos(mW) * 2.0;
    double fInvLength = 1 / sqrt(fSqrLength);
    vecAxis[0] = mX * fInvLength;
    vecAxis[2] = mY * fInvLength;
    vecAxis[2] = mZ * fInvLength;
  }
  else
  {
    // angle is 0 (mod 2*pi), so any axis will do
    dAng = 0.0;
    vecAxis[0] = 1.0;
    vecAxis[1] = 0.0;
    vecAxis[2] = 0.0;
  }
}


/**
 * This method builds the quaternion from three orthonomental axes
  @param axes The Axes from ehich the quaternion can be built
 */
void Klaus::Quaternion::fromAxes(const VecPosition * axes)
{
  Matrix3 m;

  for (size_t iCol = 0; iCol < 3; iCol++)
  {
    m[0][iCol] = axes[iCol][0];
    m[1][iCol] = axes[iCol][1];
    m[2][iCol] = axes[iCol][2];
  }

  fromMatrix3(m);
}

void Klaus::Quaternion::fromAxes(const Klaus::VecPosition& x_ax, const Klaus::VecPosition& y_ax, const Klaus::VecPosition& z_ax)
{
  Matrix3 m;
  m[0][0] = x_ax[0];
  m[1][0] = x_ax[1];
  m[2][0] = x_ax[2];

  m[0][1] = y_ax[0];
  m[1][1] = y_ax[1];
  m[2][1] = y_ax[2];

  m[0][2] = z_ax[0];
  m[1][2] = z_ax[1];
  m[2][2] = z_ax[2];

  fromMatrix3(m);
}

/**
 * This method saves the quaternion values to three orthonormal axes
  @param axes The axes to which represent the quaternion rotation
 */
void Klaus::Quaternion::toAxes(VecPosition * axes) const
{
  Matrix3 m(*this);

  for(size_t iCol = 0; iCol < 3; iCol++)
  {
    axes[iCol][0] = m[0][iCol];
    axes[iCol][1] = m[1][iCol];
    axes[iCol][2] = m[2][iCol];
    axes[iCol].normalize();
  }
}

/**
 * This method creates the quaternion from three angles call euler angles
  @param ang Euler angles to convert into the quaternion
 */
void Klaus::Quaternion::fromEulerAngles( const VecPosition & ang)
{
  double c1   = cos(Degree::toRadian(ang[1] * 0.5));
  double s1   = sin(Degree::toRadian(ang[1] * 0.5));
  double c2   = cos(Degree::toRadian(ang[2] * 0.5));
  double s2   = sin(Degree::toRadian(ang[2] * 0.5));
  double c3   = cos(Degree::toRadian(ang[0] * 0.5));
  double s3   = sin(Degree::toRadian(ang[0] * 0.5));
  double c1c2 = c1*c2;
  double s1s2 = s1*s2;

  mW = c1c2*c3  - s1s2*s3;
  mX = c1c2*s3  + s1s2*c3;
  mY = s1*c2*c3 + c1*s2*s3;
  mZ = c1*s2*c3 - s1*c2*s3;
}

/**
 * This method converts the quaternion as three angles for consecutive roations called euler angles
  @return The quaternion represented as angles
 */
Klaus::VecPosition Klaus::Quaternion::toEulerAngles() const
{
  VecPosition ang;

  double sqw = mW * mW;
  double sqx = mX * mX;
  double sqy = mY * mY;
  double sqz = mZ * mZ;
  double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
  double test = mX * mY + mZ * mW;

  if(test > 0.499 * unit)
  {
    // singularity at north pole
    ang[1] = 2 * Degree::atan2(mX,mW).getDegree();
    ang[2] = M_PI/2;
    ang[0] = 0;
    return ang;
  }
  if(test < -0.499 * unit)
  {
    // singularity at south pole
    ang[1] = -2 * Degree::atan2(mX,mW).getDegree();
    ang[2] = M_PI/2;
    ang[0] = 0;
    return ang;
  }

  ang[1] = Degree::atan2(2*mY*mW - 2*mX*mZ , sqx - sqy - sqz + sqw).getDegree();
  ang[2] = Degree::asin(2 * test / unit).getDegree();
  ang[0] = Degree::atan2(2*mX*mW - 2*mY*mZ , -sqx + sqy - sqz + sqw).getDegree();

  return ang;
}


/**
 * This method returns a std::string containing quaternion values
  @return std::string containing quaternion values
 */
std::string Klaus::Quaternion::str() const
{
  std::stringstream buf;
  buf << setdecimal(2) << "(" << mW << " " << mX << " " << mY << " " << mZ << ")";
  return buf.str();
}

/**
 * This method returns local x Axis of the quaternion from it's imaginary one
  @return X Axis of the quaternion
 */
Klaus::VecPosition Klaus::Quaternion::getXAxis() const
{
  //double fTx  = 2.0*mX;
  double fTy  = 2.0*mY;
  double fTz  = 2.0*mZ;
  double fTwy = fTy*mW;
  double fTwz = fTz*mW;
  double fTxy = fTy*mX;
  double fTxz = fTz*mX;
  double fTyy = fTy*mY;
  double fTzz = fTz*mZ;

  return VecPosition(1.0-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
}

/**
 * This method returns local y Axis of the quaternion from it's imaginary one
  @return Y Axis of the quaternion
 */
Klaus::VecPosition Klaus::Quaternion::getYAxis() const
{
  double fTx  = 2.0*mX;
  double fTy  = 2.0*mY;
  double fTz  = 2.0*mZ;
  double fTwx = fTx*mW;
  double fTwz = fTz*mW;
  double fTxx = fTx*mX;
  double fTxy = fTy*mX;
  double fTyz = fTz*mY;
  double fTzz = fTz*mZ;

  return VecPosition(fTxy-fTwz, 1.0-(fTxx+fTzz), fTyz+fTwx);
}

/**
 * This method returns local z Axis of the quaternion from it's imaginary one
  @return Z Axis of the quaternion
 */
Klaus::VecPosition Klaus::Quaternion::getZAxis() const
{
  double fTx  = 2.0*mX;
  double fTy  = 2.0*mY;
  double fTz  = 2.0*mZ;
  double fTwx = fTx*mW;
  double fTwy = fTy*mW;
  double fTxx = fTx*mX;
  double fTxz = fTz*mX;
  double fTyy = fTy*mY;
  double fTyz = fTz*mY;

  return VecPosition(fTxz+fTwy, fTyz-fTwx, 1.0-(fTxx+fTyy));
}

/**
 * Calculate the local roll element of this quaternion.
  @param reprojectAxis By default the method returns the 'intuitive' result
	that is, if you projected the local Y of the quaternion onto the X and
	Y axes, the angle between them is returned. If set to false though, the
	result is the actual yaw that will be used to implement the quaternion,
	which is the shortest possible path to get to the same orientation and
	may involve less axial rotation.
  @return Roll angle of the quaternion
 */
Klaus::Degree Klaus::Quaternion::getRoll(bool reprojectAxis) const
{
  if (reprojectAxis)
  {
    // roll = atan2(localx.mY, localx.mX)
    // pick parts of xAxis() implementation that we need
    double fTy  = 2.0*mY;
    double fTz  = 2.0*mZ;
    double fTwz = fTz*mW;
    double fTxy = fTy*mX;
    double fTyy = fTy*mY;
    double fTzz = fTz*mZ;

    // Vector3(1.0-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
    return Degree::atan2(fTxy+fTwz, 1.0-(fTyy+fTzz));
  }
  else
    return Degree::atan2(2*(mX*mY + mW*mZ), mW*mW + mX*mX - mY*mY - mZ*mZ);
}

/**
 * Calculate the local pitch element of this quaternion.
  @param reprojectAxis By default the method returns the 'intuitive' result
	that is, if you projected the local Y of the quaternion onto the X and
	Y axes, the angle between them is returned. If set to false though, the
	result is the actual yaw that will be used to implement the quaternion,
	which is the shortest possible path to get to the same orientation and
	may involve less axial rotation.
  @return Roll angle of the quaternion
 */
Klaus::Degree Klaus::Quaternion::getPitch(bool reprojectAxis) const
{
  if(reprojectAxis)
  {
    // pitch = atan2(localy.mZ, localy.mY)
    // pick parts of yAxis() implementation that we need
    double fTx  = 2.0*mX;
    double fTz  = 2.0*mZ;
    double fTwx = fTx*mW;
    double fTxx = fTx*mX;
    double fTyz = fTz*mY;
    double fTzz = fTz*mZ;

    return Degree::atan2(fTyz+fTwx, 1.0-(fTxx+fTzz));
  }
  else
    return Degree::atan2(2*(mY*mZ + mW*mX), mW*mW - mX*mX - mY*mY + mZ*mZ);
}

/**
 * Calculate the local yaw element of this quaternion.
  @param reprojectAxis By default the method returns the 'intuitive' result
	that is, if you projected the local Y of the quaternion onto the X and
	Y axes, the angle between them is returned. If set to false though, the
	result is the actual yaw that will be used to implement the quaternion,
	which is the shortest possible path to get to the same orientation and
	may involve less axial rotation.
  @return Roll angle of the quaternion
 */
Klaus::Degree Klaus::Quaternion::getYaw(bool reprojectAxis) const
{
  if (reprojectAxis)
  {
    // yaw = atan2(localz.mX, localz.mZ)
    // pick parts of zAxis() implementation that we need
    double fTx  = 2.0*mX;
    double fTy  = 2.0*mY;
    double fTz  = 2.0*mZ;
    double fTwy = fTy*mW;
    double fTxx = fTx*mX;
    double fTxz = fTz*mX;
    double fTyy = fTy*mY;

    return Degree::atan2(fTxz+fTwy, 1.0-(fTxx+fTyy));
  }
  else
    return Degree::asin(-2*(mX*mZ - mW*mY));
}

/**
  This method evaluates dot products between two quaternions
  @param q Quaternion to evaluate dot with
  @return Dot product of the quaternions
*/
double Klaus::Quaternion::dot(const Quaternion& q) const
{
  return mW*q.mW+mX*q.mX+mY*q.mY+mZ*q.mZ;
}

/**
  returns the norm of quaternion, sum of squares
  @return Sum of squares of the values
*/
double Klaus::Quaternion::norm() const
{
  return mW*mW + mX*mX + mY*mY + mZ*mZ;
}

/**
  This method normalizes the quaternion and returns the previous
  norm of the quaternion
  @return Previous norm of quaternion after normalizing it
*/
double Klaus::Quaternion::normalize()
{
  double len = norm();
  if( len == 1.0 )
    return 1.0;

  double factor = 1.0 / sqrt(len);
  *this = *this * factor;
  return len;
}

/**
  This method returns the conjugate of the quaternion
  @return Quaternion conjugate
*/
Klaus::Quaternion Klaus::Quaternion::getConjugate() const
{
  Quaternion q = *this;
  q.normalize();

  return Quaternion( q[0], -q[1], -q[2], -q[3] );
}

/**
  This method returns inverse of the quaternion, that is if multiplied by the
  quaternion itself, will result in identity quaternion.
  @return Inverse of the quaternion
*/
Klaus::Quaternion Klaus::Quaternion::getInverse() const
{
  double fNorm = norm();
  if( fNorm > 0.0 )
  {
    double fInvNorm = 1.0/fNorm;
    return Quaternion(mW*fInvNorm,-mX*fInvNorm,-mY*fInvNorm,-mZ*fInvNorm);
  }
  else
  {
    // return an invalid result to flag the error
    return zero;
  }
}
