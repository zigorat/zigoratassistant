/*
*********************************************************************************
*             Angles.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 04/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for using angles with different units.          *
*                                                                               *
*********************************************************************************
*/

/*! \file Angles.cpp
<pre>
<b>File:</b>          Angles.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       04/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for using angles with different units.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
04/24/2011       Mahdi           Initial version created
</pre>
*/

#include "Angles.h"
#include <cmath>     // needed for M_PI
#include <iomanip>   // needed for std::setprecision

/** Needed to elminate excess digits */
#define setdecimal(x) std::setprecision(x) << std::setiosflags(std::ios::fixed|std::ios::showpoint)


/*********************************************************************/
/************************  Class Degree  *****************************/
/*********************************************************************/

/** Definition of unknown angle in degrees */
Klaus::Degree Klaus::Degree::Unknown(-1000.0);

/**
  Constructor of Degree. Initializes the angle to zero
*/
Klaus::Degree::Degree()
{
  mAngle = 0;
}

/**
  Constructor of Degree. Uses the given angle value as the initialization value for the angle
  @param ang The angle to set as the angle
*/
Klaus::Degree::Degree(const double& ang)
{
  mAngle = ang;
}

/**
  Constructor of Degree. Uses the given angle value as the initialization value for the angle
  @param ang The radian angle to set as the angle. It will be converted to degree
*/
Klaus::Degree::Degree(const Klaus::Radian& ang)
{
  mAngle = ang.getDegree();
}

/**
  Overloaded negated operator to negate the angle value
  @return Negated angle
*/
Klaus::Degree Klaus::Degree::operator-() const
{
  return Degree(-mAngle);
}

/**
  Overloaded operator to return the sum of the angle with the given angle
  @param ang The angle to get its sum with the internal angle
  @return Sum of the angle with the given angle value
*/
Klaus::Degree Klaus::Degree::operator+(const Klaus::Degree& ang) const
{
  return Degree(mAngle + ang.mAngle);
}

/**
  Overloaded operator to return the subtract of the angle with the given angle
  @param ang The angle to get its subtract with the internal angle
  @return Subtract of the internal angle with the given angle value
*/
Klaus::Degree Klaus::Degree::operator-(const Klaus::Degree& ang) const
{
  return Degree(mAngle - ang.mAngle);
}

/**
  Overloaded operator to return the multiplication of the angle with the given angle
  @param ang The angle to get its multiplication with the internal angle
  @return Multiplication of the angle with the given angle value
*/
Klaus::Degree Klaus::Degree::operator*(const Klaus::Degree& ang) const
{
  return Degree(mAngle * ang.mAngle);
}

/**
  Overloaded operator to return the division of the angle with the given angle
  @param ang The angle to get its division with the internal angle
  @return Division of the angle with the given angle value
*/
Klaus::Degree Klaus::Degree::operator/(const Klaus::Degree& ang) const
{
  return Degree(mAngle / ang.mAngle);
}

/**
  Overloaded operator to add the given angle to the internal angle and return the whole instance
  @param ang The angle to add to the internal angle
  @return The instance itself
*/
Klaus::Degree& Klaus::Degree::operator+=(const Klaus::Degree& ang)
{
  mAngle += ang.mAngle;
  return *this;
}

/**
  Overloaded operator to subtract the given angle from the internal angle and return the whole instance
  @param ang The angle to subtract from the internal angle
  @return The instance itself
*/
Klaus::Degree& Klaus::Degree::operator-=(const Klaus::Degree& ang)
{
  mAngle -= ang.mAngle;
  return *this;
}

/**
  Overloaded operator to multiply the given angle by the internal angle and return the whole instance
  @param ang The angle to multiply by the internal angle
  @return The instance itself
*/
Klaus::Degree& Klaus::Degree::operator*=(const Klaus::Degree& ang)
{
  mAngle *= ang.mAngle;
  return *this;
}

/**
  Overloaded operator to divide the internal angle by the given angle and return the whole instance
  @param ang The angle to divide the internal angle
  @return The instance itself
*/
Klaus::Degree& Klaus::Degree::operator/=(const Klaus::Degree& ang)
{
  mAngle /= ang.mAngle;
  return *this;
}

/**
  Overloaded assignment operator to copy the the given angle into the instance
  @param ang The angle to assign to the internal angle
  @return The instance itself
*/
Klaus::Degree& Klaus::Degree::operator=(const Klaus::Degree& ang)
{
  mAngle = ang.mAngle;
  return *this;
}

/**
  Overloaded boolean operator to check equality to the internal angle
  @param ang The angle to check if equal to the internal angle
  @return True if the internal angle is equal to the given angle, false otherwise
*/
bool Klaus::Degree::operator==(const Klaus::Degree& ang) const
{
  return mAngle == ang.mAngle;
}

/**
  Overloaded boolean operator to check inequality to the internal angle
  @param ang The angle to check its inequality to the internal angle
  @return True if the internal angle is not equal to the given angle, false otherwise
*/
bool Klaus::Degree::operator!=(const Klaus::Degree& ang) const
{
  return mAngle != ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its smaller
  @return True if the internal angle is bigger than the given angle, false otherwise
*/
bool Klaus::Degree::operator>(const Klaus::Degree& ang) const
{
  return mAngle > ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its smaller or equal
  @return True if the internal angle is bigger or equal to the given angle, false otherwise
*/
bool Klaus::Degree::operator>=(const Klaus::Degree& ang) const
{
  return mAngle >= ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its bigger
  @return True if the internal angle is smaller than the given angle, false otherwise
*/
bool Klaus::Degree::operator<(const Klaus::Degree& ang) const
{
  return mAngle < ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its bigger or equal
  @return True if the internal angle is smaller or equal to the given angle, false otherwise
*/
bool Klaus::Degree::operator<=(const Klaus::Degree& ang) const
{
  return mAngle <= ang.mAngle;
}

/**
  This method returns the angle value in degree unit
  @return Angle in degrees
*/
double Klaus::Degree::getDegree() const
{
  return mAngle;
}

/**
  This method returns the angle value in radian unit
  @return Angle in radians
*/
double Klaus::Degree::getRadian() const
{
  return Degree::toRadian(mAngle);
}

/**
  This method returns the absolute value for the angle.
  @return Absolute value of the angle
*/
double Klaus::Degree::abs() const
{
  return fabs(mAngle);
}

/**
  This method restricts the angle to the given range.
  @param min The low bound of the angle
  @param max The high bound of the angle
*/
void Klaus::Degree::restrict(const Klaus::Degree& min, const Klaus::Degree& max)
{
  if(mAngle < min.mAngle)
    mAngle = min.mAngle;
  if(mAngle > max.mAngle)
    mAngle = max.mAngle;
}

/**
  This method rotates the angle until it resides inside the range [-180,180)
  @return The degree instance
*/
Klaus::Degree& Klaus::Degree::normalize180()
{
  while(mAngle > 180.0 ) mAngle -= 360.0;
  while(mAngle < -180.0) mAngle += 360.0;

  return *this;
}

/**
  This method rotates the angle until it resides inside the range [0,360)
  @return The degree instance
*/
Klaus::Degree& Klaus::Degree::normalize360()
{
  normalize180();
  if(mAngle < 0)
    mAngle += 360.0;

  return *this;
}

/**
  This method calculates the cosine of the angle
  @return Cosine of the angle
*/
double Klaus::Degree::cos() const
{
  return std::cos(getRadian());
}

/**
  This method calculates the sine of the angle
  @return Sine of the angle
*/
double Klaus::Degree::sin() const
{
  return std::sin(getRadian());
}

/**
  This method calculates the tangent of the angle
  @return Tangent of the angle
*/
double Klaus::Degree::tan() const
{
  return std::tan(getRadian());
}

/**
  This method calculates the cotangent of the angle
  @return Cotangent of the angle
*/
double Klaus::Degree::cot() const
{
  return 1.0 / tan();
}

/**
  This method calculates arc cosine of the given length
  @param x The length of the cosine
  @return Arc cosine of the given length
*/
Klaus::Degree Klaus::Degree::acos(const double& x)
{
  if(x >= 1)
    return Degree(0.0);
  else if(x <= -1)
    return Degree(180.0);

  return Radian::toDegree(std::acos(x));
}

/**
  This method calculates arc sine of the given length
  @param x The length of the arc
  @return Arc sine of the given arc length
*/
Klaus::Degree Klaus::Degree::asin(const double& x)
{
  if(x >= 1)
    return Degree(90.0);
  else if (x <= -1)
    return Degree(-90.0);

  return Radian::toDegree(std::asin(x));
}

/**
  This function returns the principal value of the arc tangent of x
  in degrees using the built-in arc tangent function which returns
  this value in radians.
  @param x a double value
  @return the arc tangent of the given value in degrees
*/
Klaus::Degree Klaus::Degree::atan(const double& x)
{
  return Radian::toDegree(std::atan(x));
}

/**
  This function returns the principal value of the arc tangent of y/x in
  degrees using the signs of both arguments to determine the quadrant of the
  return value. For this the built-in 'atan2' function is used which returns
  this value in radians.
  @param x a double value
  @param y a double value
  @return the arc tangent of y/x in degrees taking the signs of x and y into
  account
*/
Klaus::Degree Klaus::Degree::atan2(const double& x, const double& y)
{
  if(fabs(x) < 0.0001 && fabs(y) < 0.0001)
    return 0.0;

  return Radian::toDegree(std::atan2(y, x));
}

/**
  This method converts the given angle value in degrees to radians
  @param ang The angle in degrees
  @return Radian value of the given angle
*/
double Klaus::Degree::toRadian(const double& ang)
{
  return M_PI * ang / 180.0;
}

/**
  Overloaded stream operator to write the angle into a standard output stream
  @param os The output stream to write the angle into
  @param v The Degree instance to be written in the output stream
  @return The stream instance
*/
std::ostream& Klaus::operator<<(std::ostream& os, const Klaus::Degree& v)
{
  os << setdecimal(2) << v.getDegree();
  return os;
}




/*********************************************************************/
/************************  Class Radian  *****************************/
/*********************************************************************/

/** Definition of unknown angle in degrees */
Klaus::Radian Klaus::Radian::Unknown(-1000.0);

/**
  Constructor of Radian. Initializes the angle to zero
*/
Klaus::Radian::Radian()
{
  mAngle = 0;
}

/**
  Constructor of Radian. Uses the given angle value as the initialization value for the angle
  @param ang The angle to set as the angle
*/
Klaus::Radian::Radian(const double& ang)
{
  mAngle = ang;
}

/**
  Constructor of Radian. Uses the given angle value as the initialization value for the angle
  @param ang The degree angle to set as the angle. It will be converted to radian
*/
Klaus::Radian::Radian(const Klaus::Degree& ang)
{
  mAngle = ang.getRadian();
}

/**
  Negation operator overload to negate the angle.
  @return Negated angle value
*/
Klaus::Radian Klaus::Radian::operator-()
{
  return Radian(-mAngle);
}

/**
  Overloaded operator to return the sum of the angle with the given angle
  @param ang The angle to get its sum with the internal angle
  @return Sum of the angle with the given angle value
*/
Klaus::Radian Klaus::Radian::operator+(const Klaus::Radian& ang)
{
  return Radian(mAngle + ang.mAngle);
}

/**
  Overloaded operator to return the subtract of the angle with the given angle
  @param ang The angle to get its subtract with the internal angle
  @return Subtract of the internal angle with the given angle value
*/
Klaus::Radian Klaus::Radian::operator-(const Klaus::Radian& ang)
{
  return Radian(mAngle - ang.mAngle);
}

/**
  Overloaded operator to return the multiplication of the angle with the given angle
  @param ang The angle to get its multiplication with the internal angle
  @return Multiplication of the angle with the given angle value
*/
Klaus::Radian Klaus::Radian::operator*(const Klaus::Radian& ang)
{
  return Radian(mAngle * ang.mAngle);
}

/**
  Overloaded operator to return the division of the angle with the given angle
  @param ang The angle to get its division with the internal angle
  @return Division of the angle with the given angle value
*/
Klaus::Radian Klaus::Radian::operator/(const Klaus::Radian& ang)
{
  return Radian(mAngle / ang.mAngle);
}

/**
  Overloaded operator to add the given angle to the internal angle and return the whole instance
  @param ang The angle to add to the internal angle
  @return The instance itself
*/
Klaus::Radian& Klaus::Radian::operator+=(const Klaus::Radian& ang)
{
  mAngle += ang.mAngle;
  return *this;
}

/**
  Overloaded operator to subtract the given angle from the internal angle and return the whole instance
  @param ang The angle to subtract from the internal angle
  @return The instance itself
*/
Klaus::Radian& Klaus::Radian::operator-=(const Klaus::Radian& ang)
{
  mAngle -= ang.mAngle;
  return *this;
}

/**
  Overloaded operator to multiply the given angle by the internal angle and return the whole instance
  @param ang The angle to multiply by the internal angle
  @return The instance itself
*/
Klaus::Radian& Klaus::Radian::operator*=(const Klaus::Radian& ang)
{
  mAngle *= ang.mAngle;
  return *this;
}

/**
  Overloaded operator to divide the internal angle by the given angle and return the whole instance
  @param ang The angle to divide the internal angle
  @return The instance itself
*/
Klaus::Radian& Klaus::Radian::operator/=(const Klaus::Radian& ang)
{
  mAngle /= ang.mAngle;
  return *this;
}

/**
  Overloaded assignment operator to copy the the given angle into the instance
  @param ang The angle to assign to the internal angle
  @return The instance itself
*/
Klaus::Radian& Klaus::Radian::operator=(const Klaus::Radian& ang)
{
  mAngle = ang.mAngle;
  return *this;
}

/**
  Overloaded boolean operator to check equality to the internal angle
  @param ang The angle to check if equal to the internal angle
  @return True if the internal angle is equal to the given angle, false otherwise
*/
bool Klaus::Radian::operator==(const Klaus::Radian& ang)
{
  return mAngle == ang.mAngle;
}

/**
  Overloaded boolean operator to check inequality to the internal angle
  @param ang The angle to check its inequality to the internal angle
  @return True if the internal angle is not equal to the given angle, false otherwise
*/
bool Klaus::Radian::operator!=(const Klaus::Radian& ang)
{
  return mAngle != ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its smaller
  @return True if the internal angle is bigger than the given angle, false otherwise
*/
bool Klaus::Radian::operator>(const Klaus::Radian& ang)
{
  return mAngle > ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its smaller or equal
  @return True if the internal angle is bigger or equal to the given angle, false otherwise
*/
bool Klaus::Radian::operator>=(const Klaus::Radian& ang)
{
  return mAngle >= ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its bigger
  @return True if the internal angle is smaller than the given angle, false otherwise
*/
bool Klaus::Radian::operator<(const Klaus::Radian& ang)
{
  return mAngle < ang.mAngle;
}

/**
  Overloaded boolean operator to check state of the internal angle to the given angle
  @param ang The angle to check if its bigger or equal
  @return True if the internal angle is smaller or equal to the given angle, false otherwise
*/
bool Klaus::Radian::operator<=(const Klaus::Radian& ang)
{
  return mAngle <= ang.mAngle;
}

/**
  This method returns the angle value in degree unit
  @return Angle in degrees
*/
double Klaus::Radian::getDegree() const
{
  return Radian::toDegree(mAngle);
}

/**
  This method returns the angle value in radian unit
  @return Angle in radians
*/
double Klaus::Radian::getRadian() const
{
  return mAngle;
}

/**
  This method returns the absolute value for the angle.
  @return Absolute value of the angle
*/
double Klaus::Radian::abs() const
{
  return fabs(mAngle);
}

/**
  This method calculates the cosine of the angle
  @return Cosine of the angle
*/
double Klaus::Radian::cos() const
{
  return std::cos(mAngle);
}

/**
  This method calculates the sine of the angle
  @return Sine of the angle
*/
double Klaus::Radian::sin() const
{
  return std::sin(mAngle);
}

/**
  This method calculates the tangent of the angle
  @return Tangent of the angle
*/
double Klaus::Radian::tan() const
{
  return std::tan(mAngle);
}

/**
  This method calculates the cotangent of the angle
  @return Cotangent of the angle
*/
double Klaus::Radian::cot() const
{
  return 1.0 / tan();
}

/**
  This method rotates the angle until the angle resides in the range [-Pi,Pi)
  @return Angle instance itself
*/
Klaus::Radian& Klaus::Radian::normalizePi()
{
  while(mAngle > M_PI ) mAngle -= M_PI;
  while(mAngle < -M_PI) mAngle += M_PI;

  return *this;
}

/**
  This method rotates the angle until the angle resides in the range [0,2xPi)
  @return Angle instance itself
*/
Klaus::Radian& Klaus::Radian::normalize2Pi()
{
  normalizePi();
  if(mAngle < 0)
    mAngle += 2*M_PI;

  return *this;
}

/**
  This method calculates arc cosine of the given length
  @param x The length of the cosine
  @return Arc cosine of the given length
*/
Klaus::Radian Klaus::Radian::acos(const double& x)
{
  if(x >= 1)
    return Radian(0.0);
  else if(x <= -1)
    return Radian(M_PI);

  return Radian(std::acos(x));
}

/**
  This method calculates arc sine of the given length
  @param x The length of the arc
  @return Arc sine of the given arc length
*/
Klaus::Radian Klaus::Radian::asin(const double& x)
{
  if(x >= 1)
    return Radian(M_PI_2);
  else if (x <= -1)
    return Radian(-M_PI_2);

  return Radian(std::asin(x));
}

/**
  This function returns the principal value of the arc tangent of x
  in degrees using the built-in arc tangent function which returns
  this value in radians.
  @param x a double value
  @return the arc tangent of the given value in degrees
*/
Klaus::Radian Klaus::Radian::atan(const double& x)
{
  return Radian(std::atan(x));
}

/**
  This function returns the principal value of the arc tangent of y/x in
  degrees using the signs of both arguments to determine the quadrant of the
  return value. For this the built-in 'atan2' function is used which returns
  this value in radians.
  @param x a double value
  @param y a double value
  @return the arc tangent of y/x in degrees taking the signs of x and y into
  account
*/
Klaus::Radian Klaus::Radian::atan2(const double& x, const double& y)
{
  if(fabs(x) < 0.0001 && fabs(y) < 0.0001)
    return 0.0;

  return Radian(std::atan2(y, x));
}

/**
  This method converts the given angle value in radians to degree
  @param ang The angle in radian
  @return Degree value of the given angle
*/
double Klaus::Radian::toDegree(const double& ang)
{
  return 180.0 * ang / M_PI;
}

/**
  Overloaded stream operator to write the angle into a standard output stream
  @param os The output stream to write the angle into
  @param v The Radian instance to be written in the output stream
  @return The stream instance
*/
std::ostream& Klaus::operator<<(std::ostream& os, const Klaus::Radian& v)
{
  os << setdecimal(2) << v.getDegree();
  return os;
}
