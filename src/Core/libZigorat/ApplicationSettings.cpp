/*
*********************************************************************************
*      ApplicationSettings.cpp : Robocup 3D Soccer Simulation Team Zigorat      *
*                                                                               *
*  Date: 01/21/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Holds application information, Including name and needed paths.    *
*                                                                               *
*********************************************************************************
*/

/*! \file ApplicationSettings.cpp
<pre>
<b>File:</b>          ApplicationSettings.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/21/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Holds application information, Including name and needed paths.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/21/2011       Mahdi           Modifies version of initial source (2/08) created
</pre>
*/

#include "ApplicationSettings.h"
#include "Logger.h"  // needed for Logger


template<> Klaus::ApplicationSettings* Klaus::Singleton<Klaus::ApplicationSettings>::mInstance = 0;

namespace Klaus
{


/**
  Constructor of ApplicationSettings.
*/
ApplicationSettings::ApplicationSettings()
{
}

/**
  Destructor of ApplicationSettings.
*/
ApplicationSettings::~ApplicationSettings()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
ApplicationSettings * ApplicationSettings::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new ApplicationSettings;
    Logger::getSingleton()->log(1, "(ApplicationSettings) created singleton");
  }

  return mInstance;
}

/**
 * This method frees the only instace of the this class
 */
void ApplicationSettings::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(ApplicationSettings) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(ApplicationSettings) removed singleton");
  }
}

/**
  This method initializes variables that correspond to the application's behaviour
  @param app_name Name of the application
  @param data_root Root folder containing data files and configurations
  @param is_debug whether application is being executed in debug mode or installed mode
*/
void ApplicationSettings::initialize(const std::string & app_name, const std::string & data_root, bool is_debug)
{
  #if PLATFORM == PLATFORM_WINDOWS
    const bool bIsWindows = true;
  #else
    const bool bIsWindows = false;
  #endif

  // Application name
  mApplicationName = app_name;

  // debug mode
  mDebugMode = is_debug;

  // cpu architecture
  #if CPU_ARCH == CPU_64
  m64Bit = true;
  #else
  m64Bit = false;
  #endif

  if(bIsWindows)
  {
    mDataPath = data_root + "/conf/";
    mRSGPath = data_root + "/";

    if(mDebugMode)
      mPluginsPath = data_root + "/plugins/debug/";
    else
      mPluginsPath = data_root + "/plugins/release/";
  }
  else // not windows
  {
    if(mDebugMode)
    {
      mDataPath = data_root + "/conf/";
      mRSGPath = data_root + "/";
      mPluginsPath = data_root + "/plugins/";
    }
    else
    {
      mDataPath = data_root + "/share/" + mApplicationName + "/conf/";
      mRSGPath = data_root + "/share/" + mApplicationName + "/";

      if(m64Bit)
        mPluginsPath = data_root + "/lib64/" + mApplicationName + "/";
      else
        mPluginsPath = data_root + "/lib/" + mApplicationName + "/";
    }
  }
  
  // For ZigoratAssitant, there is a special name change
  if(mApplicationName == "ZigoratAssistant")
    mDataPath.replace(mDataPath.find("conf"), 4, "media");
}

/**
  This method returns the name of application.
  @return Name of the application
*/
std::string ApplicationSettings::getApplicationName() const
{
  return mApplicationName;
}

/**
  This method returns the address of the folder which
  contains application's data files and configurations.
  @return Address of data folder
*/
std::string ApplicationSettings::getDataPath() const
{
  return mDataPath;
}

/**
  This method returns the address of the folder which
  contains agent ruby scene graph descriptors.
  @return Address of agent ruby scene graph descriptors.
*/
std::string ApplicationSettings::getRSGPath() const
{
  return mRSGPath;
}

/**
  This method returns the address which plugins are stored in
  @return Address of all plugins
*/
std::string ApplicationSettings::getPluginsPath() const
{
  return mPluginsPath;
}

/**
  This method returns true if the current build type is a debug build
  @return True if the current build type is debug , false otherwise
*/
bool ApplicationSettings::isDebugMode() const
{
  return mDebugMode;
}


}; // end namespace Klaus
