/*
*********************************************************************************
*            VarList.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 17/04/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for grouping variables and using them           *
*                                                                               *
*********************************************************************************
*/

/*! \file VarList.h
<pre>
<b>File:</b>          VarList.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       17/04/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for grouping variables and using them
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
17/04/2011       Mahdi           Initial version created
</pre>
*/

#ifndef VAR_LIST
#define VAR_LIST

#include "Exception.h" // needed for Klaus::Exception
#include <string>      // needed for std::string
#include <map>         // needed for std::map
#include "Parse.h"     // needed for StringArray
#include "Geometry.h"  // needed for VecPosition

#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{


  /*! This class creates a pack of simple variables and lets them
      to be used easyly by the caller.
  */
  class _DLLExportControl VarList
  {
    private:
      /*! Variable is a mapping between variable names and their values as strings */
      typedef std::map<std::string, std::string> Variables;

      Variables   mVars;     /*!< List of all variables */

    public:
      void        clear      (                             );

      size_t      size       (                             ) const;
      void        getAllNames( StringArray & names         ) const;

      void        addVar     ( const std::string & strName,
                               const double & value        );
      void        addVar     ( const std::string & strName,
                               const std::string & value   );
      void        addVar     ( const std::string & strName,
                               const VecPosition & val     );

      double      getAsNumber( const std::string & strName ) const;
      int         getAsInt   ( const std::string & strName ) const;
      std::string getAsString( const std::string & strName ) const;
      VecPosition getAsPoint ( const std::string & strName ) const;

      double      operator []( const std::string & strName ) const;
  };

}; // end namespace Klaus


#endif // VAR_LIST
