/*
*********************************************************************************
*          BinaryStream.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 01/27/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Utilities for parsing binary data from standard data streams.      *
*                                                                               *
*********************************************************************************
*/

/*! \file BinaryStream.cpp
<pre>
<b>File:</b>          BinaryStream.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/27/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Utilities for parsing binary data from standard data streams.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/27/2011       Mahdi           Initial version created
</pre>
*/

#include "BinaryStream.h"


/**
  This method reads a boolean value from a binary stream
  @param is The input stream to read the value from
  @return The read boolean value
*/
bool Klaus::BinaryStream::readBool(std::istream& is)
{
  bool var;
  is.read((char*)(&var), sizeof(bool));

  if(is.gcount() != sizeof(bool))
    throw "(Configurable) could not read bool value";
  else
    return var;
}

/**
  This method writes a boolean value to a binary stream
  @param os The output stream to write the value to
  @param var The boolean to write in binary form on the given output stream
*/
void Klaus::BinaryStream::writeBool(std::ostream& os, const bool& var)
{
  os.write((char*)(&var), sizeof(bool));
}

/**
  This method reads an integer value from a binary stream
  @param is The input stream to read the value from
  @return The read integer value
*/
int Klaus::BinaryStream::readInt(std::istream& is)
{
  int var;
  is.read((char*)(&var), sizeof(int));

  if(is.gcount() != sizeof(int))
    throw "(Configurable) could not read int value";
  else
    return var;
}

/**
  This method writes an integer value to a binary stream
  @param os The output stream to write the value to
  @param var The boolean to write an integer form on the given output stream
*/
void Klaus::BinaryStream::writeInt(std::ostream& os, const int& var)
{
  os.write((char*)(&var), sizeof(int));
}

/**
  This method reads an unsigned value from a binary stream
  @param is The input stream to read the value from
  @return The read unsigned value
*/
unsigned int Klaus::BinaryStream::readUnsigned(std::istream& is)
{
  unsigned var;
  is.read((char*)(&var), sizeof(unsigned));

  if(is.gcount() != sizeof(unsigned))
    throw "(Configurable) could not read unsigned value";
  else
    return var;
}

/**
  This method writes an unsigned value to a binary stream
  @param os The output stream to write the value to
  @param var The unsigned to write in binary form on the given output stream
*/
void Klaus::BinaryStream::writeUnsigned(std::ostream& os, const unsigned & var)
{
  os.write((char*)(&var), sizeof(unsigned));
}

/**
  This method reads a double value from a binary stream
  @param is The input stream to read the value from
  @return The read double value
*/
double Klaus::BinaryStream::readDouble(std::istream& is)
{
  double var;
  is.read((char*)(&var), sizeof(double));

  if(is.gcount() != sizeof(double))
    throw "(Configurable) could not read double value";
  else
    return var;
}

/**
  This method writes a double value to a binary stream
  @param os The output stream to write the value to
  @param var The double to write in binary form on the given output stream
*/
void Klaus::BinaryStream::writeDouble(std::ostream& os, const double& var)
{
  os.write((char*)(&var), sizeof(double));
}

/**
  This method reads a string from a binary stream
  @param is The input stream to read the value from
  @return The read string
*/
std::string Klaus::BinaryStream::readString(std::istream& is)
{
  unsigned len;
  is.read((char*)(&len), sizeof(unsigned));
  if(is.gcount() != sizeof(unsigned))
    throw "(Configurable) could not read string size";

  char * str = new char[len+1];
  str[len] = '\0';

  if(len)
  {
    is.read(str, len);
    if(is.gcount() != (std::streamsize)len)
    {
      delete [] str;
      throw "(Configurable) could not read string";
    }
    else
    {
      std::string s = str;
      delete [] str;
      return s;
    }
  }
  else // string was empty. no error occured though
  {
    delete [] str;
    return "";
  }
}

/**
  This method writes a string value to a binary stream
  @param os The output stream to write the value to
  @param var The string to write in binary form on the given output stream
*/
void Klaus::BinaryStream::writeString(std::ostream& os, const std::string& var)
{
  writeUnsigned(os, var.size());
  os.write(var.c_str(), var.size());
}
