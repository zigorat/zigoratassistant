/*
*********************************************************************************
*             VarTypes.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 14/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides variable definitions for different kinds of variables     *
*                                                                               *
*********************************************************************************
*/

/*! \file VarTypes.h
<pre>
<b>File:</b>          VarTypes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       14/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides variable definitions for different kinds of variables
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
14/02/2011       Mahdi           Initial version created
</pre>
*/


#ifndef VAR_TYPES
#define VAR_TYPES


#include "Variable.h"  // needed for Variable
#include "Algebra.h"   // needed for Matrix4


namespace Klaus
{


  /*! This class encapsulates a boolean variable. */
  class _DLLExportControl BooleanVar : public Var
  {
    public:
      bool                mValue;   /*!< The boolean value */

                          BooleanVar( const std::string & strName="");

      virtual void        setValue  ( const std::string & strValue  );
      virtual std::string getValue  (                               ) const;
  };


  /*! This class encapsulates an integer variable. */
  class _DLLExportControl IntVar : public Var
  {
    public:
      int                 mValue; /*!< The integer value */

                          IntVar  ( const std::string &strName="");

      virtual void        setValue( const std::string & strValue );
      virtual std::string getValue(                              ) const;
  };


  /*! This class encapsulates a character variable. */
  class _DLLExportControl CharVar : public Var
  {
    public:
      char                mValue; /*!< The character value */

                          CharVar ( const std::string &strName="");

      virtual void        setValue( const std::string & strValue );
      virtual std::string getValue(                              ) const;
  };


  /*! This class encapsulates a real variable. */
  class _DLLExportControl RealVar : public Var
  {
    public:
      double              mValue; /*!< The real value */

                          RealVar ( const std::string &strName="");

      virtual void        setValue( const std::string & strValue );
      virtual std::string getValue(                              ) const;
  };


  /*! This class encapsulates a real variable. */
  class _DLLExportControl AngleVar : public Var
  {
    public:
      Degree              mValue; /*!< The real value */

                          AngleVar( const std::string &strName="");

      virtual void        setValue( const std::string & strValue );
      virtual std::string getValue(                              ) const;
  };


  /*! This class encapsulates a string variable. */
  class _DLLExportControl StringVar : public Var
  {
    public:
      std::string         mValue;  /*!< The string value */

                          StringVar( const std::string &strName="");

      virtual void        setValue ( const std::string & strValue );
      virtual std::string getValue (                              ) const;
  };


  /*! This class encapsulates an array of strings. */
  class _DLLExportControl StringArrayVar : public Var
  {
    public:
      StringArray         mValue;        /*!< The array of strings */

                          StringArrayVar( const std::string &strName="");

      virtual void        setValue      ( const std::string & strValue );
      virtual std::string getValue      (                              ) const;
  };


  /*! This class encapsulates a position variable. */
  class _DLLExportControl PositionVar : public Var
  {
    public:
      VecPosition         mValue;    /*!< The position value */

                          PositionVar( const std::string &strName="");

      virtual void        setValue   ( const std::string & strValue );
      virtual std::string getValue   (                              ) const;
  };


  /*! This class encapsulates a 4x4 matrix. */
  class _DLLExportControl Matrix4Var : public Var
  {
    public:
      Matrix4             mValue;   /*!< The 4x4 matrix */

                          Matrix4Var( const std::string &strName="");

      virtual void        setValue  ( const std::string & strValue );
      virtual std::string getValue  (                              ) const;
  };


  /*! This class encapsulates a quaternion. */
  class _DLLExportControl QuaternionVar : public Var
  {
    public:
      Quaternion          mValue;      /*!< The Quaternion */

                          QuaternionVar( const std::string &strName="");

      virtual void        setValue     ( const std::string & strValue );
      virtual std::string getValue     (                              ) const;
  };


}; // end namespace Klaus


#endif // VAR_TYPES
