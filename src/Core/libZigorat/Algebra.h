/*
*********************************************************************************
*             Algebra.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 10/18/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class declarations of algebra classes Matrix3, Matrix4             *
*            and Quaternion                                                     *
*  Some materials in this file are used from the OGRE project source code.      *
*    See official website: www.ogre3d.org                                       *
*                                                                               *
*********************************************************************************
*/

/*
Copyright 1999-2006 The OGRE Team.

OGRE is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

OGRE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OGRE; see the file COPYING.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/

/*! \file Algebra.h
<pre>
<b>File:</b>          Algebra.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/18/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class declarations of algebra classes Matrix3, Matrix4 and Quaternion
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/18/2009       Mahdi           Initial version created
</pre>
*/

#ifndef ALGEBRA
#define ALGEBRA

#include "Geometry.h"


namespace Klaus
{


  /*! This class contains several static methods dealing with geometry series.*/
  class _DLLExportControl GeomSeries
  {
    public:
      static double getLength          (double dFirst,double dRatio,double dSum  );
      static double getSum             (double dFirst,double dRatio,double dLen  );
      static double getSumInf          (double dFirst,double dRatio              );
      static double getFirst           (double dSum,  double dRatio,double dLen  );
      static double getFirstInf        (double dSum,  double dRatio              );
  };


  /*! This class contains several static methods used for interpolation and
      other calculations */
  class _DLLExportControl Algebra
  {
    public:
      static double fact      ( unsigned number                                  );
      static int    abcFormula(double a,double b, double c, double *s1,double *s2);
  };


  /*! This class contains several static methods which implement statistical
      routines like linear regression and bezier curves. */
  class _DLLExportControl Statistics
  {
    public:
      static Line   getLinearRegression( const VecPosition::Array & points     );
      static Line   getLinearRegression( double points[][2], const int & count );
  };


  class _DLLExportControl Matrix4; // Forward decleration is needed here.
  class _DLLExportControl Quaternion; // Forward decleration is needed here.


  /*! A matrix is a rectangular array of elements which are operated on as a single
      object. Matrices are a key tool in linear algebra. One use of matrices is to
      represent linear transformations. One key feature is matrix multiplication
      corresponds to composition of linear transformations. Also Matrices can also
      keep track of the coefficients in a system of linear equations. For a square
      matrix, the determinant and inverse matrix govern the behavior of solutions
      to the corresponding system of linear equations. Square matrix can represent
      any linear vector translation. A very handy feature of a square orthogonal
      matrix is that it can hold rotations, and when multiplied to (or by) vectors
      actually gives the rotated vector under the rotation parameters.

      This class represents a 3 dimentional mathematical matrix. Matrix operations
      defined in this encapsulation are the most common used of standard ones.

      Know that the (x,y,z) coordinate system is assumed to be right-handed.
      Coordinate axis rotation matrices are of the form

        RX =   | 1       0         0    |
               | 0     cos(t)   -sin(t) |
               | 0     sin(t)    cos(t) |
      where t > 0 indicates a counterclockwise rotation in the yz-plane

        RY =  |  cos(t)    0     sin(t) |
              |    0       1       0    |
              | -sin(t)    0     cos(t) |
      where t > 0 indicates a counterclockwise rotation in the zx-plane

        RZ =  | cos(t)  -sin(t)    0    |
              | sin(t)   cos(t)    0    |
              |   0        0       1    |
      where t > 0 indicates a counterclockwise rotation in the xy-plane.
  */
  class _DLLExportControl Matrix3
  {
    private:
      double         mElements[3][3];  /*!< Matrix indices           */

      void           clearMatrix       (                             );

    public:

                     Matrix3           (                             );
                     Matrix3           ( const Matrix3 & m           );
                     Matrix3           ( const Matrix4 & m           );
                     Matrix3           ( const Quaternion & q        );
                     Matrix3           ( const double indices[3][3]  );
                     Matrix3           ( const double & d11,
                                         const double & d12,
                                         const double & d13,
                                         const double & d21,
                                         const double & d22,
                                         const double & d23,
                                         const double & d31,
                                         const double & d32,
                                         const double & d33          );
                   ~ Matrix3           (                             );

      // Utilities and functions
      double         getDeterminant    (                             ) const;
      Matrix3        getInverseMatrix  (                             ) const;
      Matrix3        getTranspose      (                             ) const;
      void           orthoNormalize    (                             );

      VecPosition    getColoumn        ( const int & iIndex          ) const;
      void           setColoumn        ( const int & iIndex,
                                         const VecPosition & col     );
      void           fromAxes          ( const VecPosition & xAxis,
                                         const VecPosition & yAxis,
                                         const VecPosition & zAxis   );

      bool           toEulerAnglesXYZ  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      bool           toEulerAnglesXZY  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      bool           toEulerAnglesYXZ  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      bool           toEulerAnglesYZX  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      bool           toEulerAnglesZXY  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      bool           toEulerAnglesZYX  ( Radian& yaw,
                                         Radian& pitch,
                                         Radian& roll                ) const;
      void           fromEulerAnglesXYZ( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );
      void           fromEulerAnglesXZY( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );
      void           fromEulerAnglesYXZ( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );
      void           fromEulerAnglesYZX( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );
      void           fromEulerAnglesZXY( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );
      void           fromEulerAnglesZYX( const Radian& yaw,
                                         const Radian& pitch,
                                         const Radian& roll          );

      void           toAxisAngle       ( VecPosition & axis,
                                         Degree & angle              ) const;
      void           fromAxisAngle     ( const VecPosition& axis,
                                         const Degree & angle        );

      Matrix3      & operator =        ( const Matrix3& mat          );
      Matrix3      & operator =        ( const double & val          );
      Matrix3        operator +        ( const Matrix3& mat          ) const;
      Matrix3        operator +        ( const double & val          ) const;
      Matrix3        operator -        ( const Matrix3& mat          ) const;
      Matrix3        operator -        ( const double & val          ) const;
      Matrix3        operator *        ( const Matrix3& mat          ) const;
      Matrix3        operator *        ( const double & val          ) const;
      Matrix3        operator /        ( const Matrix3& mat          ) const;
      Matrix3        operator /        ( const double & val          ) const;
      Matrix3      & operator +=       ( const Matrix3& mat          );
      Matrix3      & operator +=       ( const double & val          );
      Matrix3      & operator -=       ( const Matrix3& mat          );
      Matrix3      & operator -=       ( const double & val          );
      Matrix3      & operator *=       ( const Matrix3& mat          );
      Matrix3      & operator *=       ( const double & val          );
      Matrix3      & operator /=       ( const Matrix3& mat          );
      Matrix3      & operator /=       ( const double & val          );
      const double * operator []       ( const int & iIndex          ) const;
      double       * operator []       ( const int & iIndex          );

      bool           operator ==       ( const Matrix3 & mat         ) const;
      bool           operator !=       ( const Matrix3 & mat         ) const;

      VecPosition    operator *        ( const VecPosition & point   ) const;

      /**
        This method rotates the given point by the Matrix. The matrix should be a rotation matrix.
        vector * matrix [1x3 * 3x3 = 1x3]
        @param point The given point to rotate
        @param mat The matrix to rotate the given point
        @return Rotated position of the point by the matrix
      */
      friend VecPosition operator *    ( const VecPosition & point,
                                           const Matrix3 & mat       );

      /**
        This is the overloaded output operator.
        Prints the matrix into the output stream supplied
        @param os Output stream to print matrix to
        @param m Matrix3 to print to stream
        @return Output stream reference
      */
      friend std::ostream & operator <<( std::ostream &os, Matrix3 m );

      std::string    str               (                             ) const;


      static Matrix3 createFrom3Points ( const VecPosition & pos1,
                                         const VecPosition & pos2,
                                         const VecPosition & pos3    );

      static const Matrix3 zero;       /*!< Zero matrix              */
      static const Matrix3 identity;   /*!< Identity matrix          */

      friend class Matrix4;            /// for faster access
  };


  /*! A matrix is a rectangular array of elements which are operated on as a single
      object. Matrices are a key tool in linear algebra. One use of matrices is to
      represent linear transformations. One key feature is matrix multiplication
      corresponds to composition of linear transformations. Also Matrices can also
      keep track of the coefficients in a system of linear equations. For a square
      matrix, the determinant and inverse matrix govern the behavior of solutions
      to the corresponding system of linear equations. Square matrix can represent
      any linear vector translation. A very handy feature of a square orthogonal
      matrix is that it can hold rotations, and when multiplied to (or by) vectors
      actually gives the rotated vector under the rotation parameters.
      A 4x4 matrix can hold a complete tranfsormation information in it. (Translate,
      scale, rotatation)
        OGRE uses column vectors when applying matrix multiplications, This means a
      vector is represented as a single column, 4-row matrix. This has the effect
      that the transformations implemented by the matrices happens right-to-left e.g.
      if vector V is to be transformed by M1 then M2 then M3, the calculation would
      be M3 * M2 * M1 * V. The order that matrices are concatenated is vital since
      matrix multiplication is not cummatative, i.e. you can get a different result
      if you concatenate in the wrong order.

      This class represents a 4x4 mathematical matrix. Matrix operations defined in
      this encapsulation are the most common used of standard ones.

      Methods starting with from will not clear the matrix before applying their
      operation. Instead, constructors will do this.
  */
  class _DLLExportControl Matrix4
  {
    private:
      double            mElements[4][4];    /*!< Matrix indices           */

      void              clearMatrix         (                             );

    public:

                        Matrix4             (                             );
                        Matrix4             ( const Matrix4 & m           );
                        Matrix4             ( const double indices[4][4]  );
                        Matrix4             ( const Matrix3 & mat         );
                        Matrix4             ( const Quaternion & q        );
                        Matrix4             ( const double & d11,
                                              const double & d12,
                                              const double & d13,
                                              const double & d14,
                                              const double & d21,
                                              const double & d22,
                                              const double & d23,
                                              const double & d24,
                                              const double & d31,
                                              const double & d32,
                                              const double & d33,
                                              const double & d34,
                                              const double & d41,
                                              const double & d42,
                                              const double & d43,
                                              const double & d44          );
                      ~ Matrix4             (                             );

      // Utilities and functions
      double            getDeterminant      (                             ) const;
      Matrix4           getInverseMatrix    (                             ) const;
      Matrix4           getTranspose        (                             ) const;

      bool              isAffine            (                             ) const;
      VecPosition       transformAffine     ( const VecPosition & pos     ) const;

      void              fromQuaternion      ( const Quaternion & q        );
      Quaternion        toQuaternion        (                             ) const;

      void              fromMatrix3         ( const Matrix3 & mat         );
      Matrix3           toMatrix3           (                             ) const;

      Matrix4         & operator =          ( const Matrix4& mat          );
      Matrix4         & operator =          ( const Matrix3& mat          );
      Matrix4         & operator =          ( const double & val          );
      Matrix4           operator +          ( const Matrix4& mat          ) const;
      Matrix4           operator +          ( const double & val          ) const;
      Matrix4           operator -          ( const Matrix4& mat          ) const;
      Matrix4           operator -          ( const double & val          ) const;
      Matrix4           operator *          ( const Matrix4& mat          ) const;
      Matrix4           operator *          ( const double & val          ) const;
      Matrix4           operator /          ( const Matrix4& mat          ) const;
      Matrix4           operator /          ( const double & val          ) const;
      Matrix4         & operator +=         ( const Matrix4& mat          );
      Matrix4         & operator +=         ( const double & val          );
      Matrix4         & operator -=         ( const Matrix4& mat          );
      Matrix4         & operator -=         ( const double & val          );
      Matrix4         & operator *=         ( const Matrix4& mat          );
      Matrix4         & operator *=         ( const double & val          );
      Matrix4         & operator /=         ( const Matrix4& mat          );
      Matrix4         & operator /=         ( const double & val          );
      const double    * operator []         ( const int & iIndex          ) const;
      double          * operator []         ( const int & iIndex          );

      bool              operator ==         ( const Matrix4 & mat         ) const;
      bool              operator !=         ( const Matrix4 & mat         ) const;

      VecPosition       operator *          ( const VecPosition & point   ) const;

      /**
        This method rotates the given point by the Matrix. The matrix should be a rotation matrix.
        matrix * point [3x3 * 3x1 = 3x1]
        @param point The given point to rotate
        @param mat The matrix which rotates the point
        @return Rotated position of the point by the matrix
      */
      friend VecPosition operator *         ( const VecPosition & point,
                                              const Matrix4 & mat         );

      /**
        This is the overloaded output operator.
        Prints the matrix into the output stream supplied
        @param os Output stream to print matrix to
        @param m Matrix4 to print to stream
        @return Output stream reference
       */
      friend std::ostream & operator <<     ( std::ostream &os, Matrix4 m );

      std::string       str                 (                             ) const;

      static Matrix4    createTransform     ( const VecPosition & tr,
                                              const Quaternion  & rot,
                                              const VecPosition & sc      );
      static Matrix4    createTransform     ( const VecPosition & tr,
                                              const Quaternion  & rot     );

      static Matrix4    getTranslationMatrix( const VecPosition & tr      );
      static Matrix4    getTranslationMatrix( const double & dx,
                                              const double & dy,
                                              const double & dz           );
      static Matrix4    getScaleMatrix      ( const VecPosition & sc      );
      static Matrix4    getScaleMatrix      ( const double & dx,
                                              const double & dy,
                                              const double & dz           );

      VecPosition       getTranslation      (                             ) const;
      void              setTranslation      ( const VecPosition & tr      );
      void              setTranslation      ( const double & x,
                                              const double & y,
                                              const double & z            );

      VecPosition       getScale            (                             ) const;
      void              setScale            ( const VecPosition & sc      );
      void              setScale            ( const double & x,
                                              const double & y,
                                              const double & z            );

      Quaternion        getOrientation      (                             ) const;
      void              setOrientation      ( const Quaternion & q        );

      void              normalize           (                             );

      void              dissect             ( VecPosition & p,
                                              Quaternion & q              );
      void              dissect             ( VecPosition & p,
                                              Quaternion & q,
                                              VecPosition & s             );

      static const Matrix4 zero;            /*!< Zero matrix              */
      static const Matrix4 identity;        /*!< Identity matrix          */

      friend class Quaternion; /// For faster access
  };


  /*! Quaternions, are a non-commutative number system that extends the complex
      numbers. They find uses in both theoretical and applied mathematics, in
      particular for calculations involving three-dimensional rotations, such
      as in 3D computer graphics, although they have been superseded in many
      applications by vectors and matrices.
      Unit quaternions provide a convenient mathematical notation for representing
      orientations and rotations of objects in three dimensions. Compared to Euler
      angles they are simpler to compose and avoid the problem of gimbal lock;
      Compared to rotation matrices they are more numerically stable and may be
      more efficient. Quaternions have found their way into applications in computer
      graphics, robotics, navigation, Molecular Dynamics and orbital mechanics of
      satellites.
      For more introduction to quaternions take a look at:
      http://en.wikipedia.org/wiki/Quaternion
  */
  class _DLLExportControl Quaternion
  {
    private:
      double      mW;  /*!< Scalar part of the quaternion                       */
      double      mX;  /*!< Imaginary part of the quaternion, first parameter   */
      double      mY;  /*!< Imaginary part of the quaternion, second parameter  */
      double      mZ;  /*!< Imaginary part of the quaternion, third parameter   */

    public:
      /// Constructors
                  Quaternion      ( double w, double x, double y, double z      );
                  Quaternion      ( const Matrix3 & rot_mat                     );
                  Quaternion      ( const Matrix4 & rot_mat                     );
                  Quaternion      ( const Degree & ang, const VecPosition & ax  );
                  Quaternion      ( const VecPosition & pos                     );
                  Quaternion      (                                             );


      /// Operators
      Quaternion  operator +      ( const Quaternion& q                         ) const;
      Quaternion& operator +=     ( const Quaternion& q                         );
      Quaternion  operator -      ( const Quaternion& q                         ) const;
      Quaternion& operator -=     ( const Quaternion& q                         );
      Quaternion  operator -      (                                             ) const;
      Quaternion  operator *      ( const Quaternion& q                         ) const;
      Quaternion& operator *=     ( const Quaternion& q                         );
      Quaternion  operator *      ( double d                                    ) const;
      VecPosition operator *      ( const VecPosition& vec                      ) const;
      Quaternion  operator /      ( const Quaternion & q                        ) const;
      Quaternion& operator /=     ( const Quaternion & q                        );

      double      operator []     ( const int & index                           ) const;
      double &    operator []     ( const int & index                           );

      Quaternion& operator =      ( const Quaternion & q                        );
      Quaternion& operator =      ( const VecPosition & p                       );

      bool        operator ==     ( const Quaternion & q                        ) const;
      bool        operator !=     ( const Quaternion & q                        ) const;

      /// Conversion Methods

      // Rotation matrix
      void        fromMatrix3     ( const Matrix3 & mRot                        );
      Matrix3     toMatrix3       (                                             ) const;

      // Axis and angle
      void        fromAngleAxis   ( const Degree& dAng,
                                    const VecPosition & vecAxis                 );
      void        toAngleAxis     ( Degree& dAng, VecPosition & vecAxis         ) const;

      // 3 Axes
      void        fromAxes        ( const VecPosition * axes                    );
      void        fromAxes        ( const VecPosition & x_ax,
                                    const VecPosition & y_ax,
                                    const VecPosition & z_ax                    );
      void        toAxes          ( VecPosition * axes                          ) const;

      // Euler angles
      void        fromEulerAngles ( const VecPosition & ang                     );
      VecPosition toEulerAngles   (                                             ) const;

      std::string str             (                                             ) const;

      /// Local Axes
      VecPosition getXAxis        (                                             ) const;
      VecPosition getYAxis        (                                             ) const;
      VecPosition getZAxis        (                                             ) const;

      /// Local rotation
      Degree      getRoll         ( bool reprojectAxis = true                   ) const;
      Degree      getPitch        ( bool reprojectAxis = true                   ) const;
      Degree      getYaw          ( bool reprojectAxis = true                   ) const;

      /// dot multiplication
      double      dot             ( const Quaternion& q                         ) const;
      double      norm            (                                             ) const;
      double      normalize       (                                             );

      Quaternion  getConjugate    (                                             ) const;
      Quaternion  getInverse      (                                             ) const;

      static const Quaternion zero;      /*!< Zero quaternion     */
      static const Quaternion identity;  /*!< Identity quaternion */
  };


}; // end namespace Klaus

#endif // ALGEBRA


