/*
*********************************************************************************
*             SLang.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 05/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for parsing and using S-Expressions.            *
*                                                                               *
*********************************************************************************
*/

/*! \file SLang.cpp
<pre>
<b>File:</b>          SLang.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for parsing and using S-Expressions.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/02/2011       Mahdi           Initial version created
</pre>
*/


#include "SLang.h"
#include <stdarg.h>  // needed for va_start
#include <stdio.h>   // needed for vsnprintf
#include <cmath>     // needed for pow
#include <fstream>  // needed for ifstream
#include <iostream>

/*! Definition of InvalidExpression */
Klaus::SExpression Klaus::SExpression::InvalidExpression;


/**
  Constructor of SExpression. Initializes the token
*/
Klaus::SExpression::SExpression()
{
  mSExpStr = NULL;
  mBegin = 0;
  mEnd = 0;
  mType = Unknown;
}

/**
  Constructor of the SExpression. Parses the given string
  @param str The string to be parsed
*/
Klaus::SExpression::SExpression(const std::string& str)
{
  // initialize
  mSExpStr = NULL;
  mBegin = 0;
  mEnd = 0;
  mType = Unknown;

  // parse the string
  parse(str);
}

/**
  Constructor of the SExpression. Loads a chunk of data containing
  an s-expression and parses it.
  @param is The input stream to read from
  @param buf_size maximum size of the s-expression
*/
Klaus::SExpression::SExpression(std::istream& is, const size_t& buf_size)
{
  // initialize
  mSExpStr = NULL;
  mBegin = 0;
  mEnd = 0;
  mType = Unknown;

  // then parse
  parse(is, buf_size);
}

/**
  Constructor of the SExpression. Loads a whole file with the
  given name and parses it as if it is a huge s-expression.
  @param name The name of the file
  @param buf_size maximum size of the file
*/
Klaus::SExpression::SExpression(const std::string& name, const size_t& buf_size)
{
  // initialize
  mSExpStr = NULL;
  mBegin = 0;
  mEnd = 0;
  mType = Unknown;

  // then parse
  parseFile(name, buf_size);
}

/**
  Overloaded constructor of SExpression. Creates from an existing expression
  @param expr The S-Expression to copy
*/
Klaus::SExpression::SExpression(const Klaus::SExpression& expr)
{
  mBegin = expr.mBegin;
  mEnd = expr.mEnd;
  mSize = expr.mSize;
  mType = expr.mType;

  // now copy the string buffer
  if(mType == Root)
  {
    mSExpStr = new std::string;
    *mSExpStr = *expr.mSExpStr;
  }
  else
    mSExpStr = expr.mSExpStr;

  // now copy the subtokens
  for(size_t i = 0; i < expr.mSubTokens.size(); ++i)
    mSubTokens.push_back(expr.mSubTokens[i]);
}

/**
  Desructor of SExpression. Removes all children and data
*/
Klaus::SExpression::~SExpression()
{
  clear();
}

/**
  This method moves the end point forward till it reaches the end of string,
  or reaches a non space character
*/
bool Klaus::SExpression::gotoFirstNonSpace()
{
  while((mEnd < mSize) && (isspace((*mSExpStr)[mEnd])))
    ++mEnd;

  return mEnd < mSize;
}

/**
  This method extracts an integer from the s-expression, according to the
  range specified for the token.
  @param index Index of the string to start extracting an integer
  @param result The extracted integer will be copied here
  @return True if the extraction was successfull, false otherwise
*/
bool Klaus::SExpression::getAsInt(size_t& index, int & result) const
{
  // zero the result
  result = 0;

  // nan will be returned 0 for now
  if((index < mEnd - 1) && (mSExpStr->compare(index, 3, "NaN") == 0))
    return true;

  // flag to hold negation bit
  bool bIsMin = false;

  // go forward till reaching a numerical character
  while((index <= mEnd) && (!isdigit((*mSExpStr)[index])) && ((*mSExpStr)[index] != '-'))
    ++index;

  // check for a negation sign
  if((index <= mEnd) && ((*mSExpStr)[index] == '-'))       // if it was a minus sign, remember
  {
    bIsMin=true;
    ++index;
  }

  // sanity check
  if(index > mEnd)
    return false;

  // get all digits
  while((index <= mEnd) && ((*mSExpStr)[index]) <= '9' && ((*mSExpStr)[index] >= '0'))
    result = result*10 + ((*mSExpStr)[index++] - '0');  // multiply old res with 10 and add new digit

  // prepare the result
  if(bIsMin)
    result = -result;

  // finished
  return true;
}

/**
  This method parses the token. The string pointer will be set and parsing
  will start from the given position.
  @param str Pointer to the string to parse
  @param begin The begining index for parse operation
  @return The next index that can be parsed
*/
size_t Klaus::SExpression::parse(std::string* str, int begin)
{
  mSExpStr = str;
  mEnd = mBegin = begin;
  mSize = str->size();

  // goto first token
  if(!gotoFirstNonSpace())
    throw SLangException("(SExpression) finished string before finding first token: '%s...'", mSExpStr->substr(mBegin, 30).c_str());

  // start from here
  mBegin = mEnd;
  char c = (*mSExpStr)[mEnd];

  // dealing with a holder node
  if(c == '(')
    return parseHolderNode();

  // dealing with a pure string constant
  else if((c == '\'') || (c == '"'))
    return parsePureString();

  // dealing with a comment?
  else if(c == ';')
    return parseComment();

  // dealing with a string constant
  else
    return parseString();
}

/**
  This method parses the token assuming the token is a normal string.
  A typical string can not have a brace openning character in it. Only
  a pure string can have brace openning characters.
  @return The next index that can be parsed
*/
size_t Klaus::SExpression::parseString()
{
  while(
         (mEnd < mSize) &&
         (!isspace((*mSExpStr)[mEnd])) &&
         ((*mSExpStr)[mEnd] != ')') &&
         ((*mSExpStr)[mEnd] != '(')
       )
    ++mEnd;

  // set the type
  if(mType == Unknown)
    mType = Atom;

  --mEnd;
  return mEnd + 1;
}

/**
  This method parses the token assuming the token is a pure string. A pure
  string is a string which can have space and all other types of characters
  which itself is specified with a single or double quotation mark.
  @return The next index that can be parsed
*/
size_t Klaus::SExpression::parsePureString()
{
  // move forward to skip the opening ' character
  // and get the quotation character
  char cQuote = (*mSExpStr)[mEnd];
  mBegin = ++mEnd;

  // now check for the closing ' character
  while(mEnd < mSize && (*mSExpStr)[mEnd] != cQuote)
    ++mEnd;

  // set the type
  if(mType == Unknown)
    mType = Atom;

  --mEnd;

  return mEnd + 2;
}

/**
  This method parses the token assuming the token is a comment. A comment line
  begins with a semi-colon and ends with a line feed.
  @return The next index that can be parsed
*/
size_t Klaus::SExpression::parseComment()
{
  // move to the next character after the semicolon
  mBegin++;

  // check for carriage return
  while((mEnd < mSize) && (*mSExpStr)[mEnd] != '\n')
    ++mEnd;

  // set the type
  if(mType == Unknown)
    mType = Comment;

  // do not take the carriage return as one of the comment's characters
  mEnd--;

  // return after the carriage return
  return mEnd + 2;
}

/**
  This method parses the token assuming the token is a typical s-expression token
  which can contain other tokens in it.
  @return The next index that can be parsed
*/
size_t Klaus::SExpression::parseHolderNode()
{
  // the current character is '(', skip it
  ++mEnd;
  while(mEnd < mSize)
  {
    // advance to the next token
    gotoFirstNonSpace();

    // check if it is a closing brace
    if((*mSExpStr)[mEnd] == ')')
      break; // finished

    // so it is a sub s-expression
    SExpression child;
    mSubTokens.push_back(child);

    try
    {
      // parse the sub s-expression
      mEnd = mSubTokens[mSubTokens.size() - 1].parse(mSExpStr, mEnd);
    }
    catch(...)
    {
      // remove the child
      mSubTokens.pop_back();

      // throw it again
      throw;
    }
  }

  if(mEnd >= mSize)
    throw SLangException("(SExpression) truncated holder node: %s", mSExpStr->substr(mBegin).c_str());

  // set the type
  if(mType == Unknown)
    mType = Normal;

  return mEnd + 1;
}

/**
  This method extracts an integer value from the begining of the token
  @return The extracted integer value
*/
int Klaus::SExpression::getAsInt() const
{
  if((mType != Atom) && (mType != Root))
    throw SLangException("(SExpression) node can not be converted to int: '%s'", getAsStdString().c_str());

  size_t index = mBegin;
  int result;

  if(!getAsInt(index, result))
    throw SLangException("(SExpression) could not convert cache to integer: '%s'", getAsStdString().c_str());

  return result;
}

/**
  This method extracts a double value from the begining of the token
  @return The double value extracted from the token
*/
double Klaus::SExpression::getAsDouble() const
{
  if((mType != Atom) && (mType != Root))
    throw SLangException("(SExpression) node can not be used as double: '%s'", getAsStdString().c_str());

  // proper intialization check
  if(!mSExpStr)
    throw SLangException("(SExpression) node not inialized");

  // process the number bit by bit
  size_t index = mBegin;

  // nan will be returned 0 for now
  if((index < mEnd - 1) && (mSExpStr->compare(index, 3, "NaN") == 0))
    return 0.0;

  // flag to hold negation bit
  bool bIsMin = false;

  // go forward till reaching a numerical character
  while((index <= mEnd) && (isspace((*mSExpStr)[index])))
    ++index;

  // get the sign bit
  if(index < mEnd && (*mSExpStr)[index] == '-')
  {
    bIsMin = true;
    ++index;
  }

  // get real part
  int iReal;
  if(!getAsInt(index, iReal))
    throw SLangException("(SExpression) could not convert cache to double: '%s'", getAsStdString().c_str());

  // get fractionary part
  double dRes, dFrac = 0.0;
  if((index <= mEnd) && ((*mSExpStr)[index] == '.'))
  {
    int iFrac;
    ++index;
    size_t b = index;
    if(!getAsInt(index, iFrac))
      throw SLangException("(SExpression) could not read fractionary part: '%s'", getAsStdString().c_str());

    dFrac = iFrac;
    while(b++ < index)
      dFrac /= 10;

    // if the number is negative, it's fractionary part shall be negative too
    if(iReal < 0)
      dFrac = -dFrac;
  }
  dRes = iReal + dFrac;

  // check for powers
  if((index < mEnd) && ((*mSExpStr)[index] == 'e'))
  {
    bool bMinusPower = (*mSExpStr)[index+1] == '-' ? true : false;
    index += 2;
    int iPow;
    if(!getAsInt(index, iPow))
      throw SLangException("(SExpression) could not extract power: '%s'", getAsStdString().c_str());

    if(bMinusPower)
      return dRes / pow(10.0, iPow);// multiply with power
    else
      return dRes * pow(10.0, iPow);// divide with power
  }

  return bIsMin ? -dRes : dRes;
}

/**
  This method returns the whole token as a standard string
  @return The token itself
*/
std::string Klaus::SExpression::getAsStdString() const
{
  return mSExpStr->substr(mBegin, mEnd - mBegin + 1);
}

/**
  This method returns the type of token
  @return Type of the token
*/
Klaus::SExpression::TokenType Klaus::SExpression::getType() const
{
  return mType;
}

/**
  This method returns the index which the data starts for the node
  @return Start index for the data corresponding to the node
*/
size_t Klaus::SExpression::getStartingIndex() const
{
  return mBegin;
}

/**
  This method removes all sub nodes and clears all flags
*/
void Klaus::SExpression::clear()
{
  if(mType == Root)
    delete mSExpStr;

  mSubTokens.clear();
  mSExpStr = NULL;
  mType = Unknown;
}

/**
  This method parses the token from the given string. This method is only called
  for the root node, creates a copy of the string and starts recursively parsing.
  @param str The string to be parsed
*/
void Klaus::SExpression::parse(const std::string& str)
{
  mType = Root;
  mSExpStr = new std::string;
  *mSExpStr = str;

  try
  {
    parse(mSExpStr, 0);
  }
  catch(...)
  {
    // free up memory before re-raising the error
    clear();
    throw;
  }
}

/**
  This method reads a s-expression from a given input stream and parses it.
  @param is The input stream to read s-expression from
  @param buf_size size of the buffer used to read from the stream.
*/
void Klaus::SExpression::parse(std::istream& is, const size_t & buf_size)
{
  char * buf = new char[buf_size];

  // first read the chunk of the stream
  try
  {
    is.read(buf, buf_size);
  }
  catch(std::ios_base::failure e)
  {
    // ignore it
    // the exception happens if reading the chunk finishes by eof
  }

  // zero end the string
  buf[is.gcount()] = 0;

  // then parse it
  parse(buf);

  delete [] buf;
}

/**
  This method completely loads a file and parses it as a s-expression string.
  @note Adds an opening and closing brace to the file's contents in memory
  @param name Name and address of the file
  @param buf_size Size of the buffer to load the file to
*/
void Klaus::SExpression::parseFile(const std::string& name, const size_t& buf_size)
{
  char * buf = new char[buf_size];

  // open the file
  std::ifstream is(name.c_str(), std::ios::in | std::ios::binary);
  if(!is)
    throw SLangException("(SExpression) could not open the s-expr file '%s'", name.c_str());

  // add opening brace to data
  buf[0] = '(';

  // read the file
  is.read(buf + 1, buf_size - 3);
  std::streamsize g = is.gcount() + 1;

  // close it
  is.close();

  // add the closing brace
  buf[g] = ')';

  // zero end the buffer
  buf[g+1] = 0;

  // now parse it
  parse(buf);
  delete [] buf;

  return;
}

/**
  This method returns number of sub-tokens which this token is holding
  @return Number of sub tokens in the token
*/
size_t Klaus::SExpression::getSubTokenCount()
{
  return mSubTokens.size();
}

/**
  This method return the sub token specified by it's index
  @param index The index of the sub-token
  @return The sub-token instance
*/
const Klaus::SExpression & Klaus::SExpression::getSubToken(const size_t& index) const
{
  if(index < mSubTokens.size())
    return mSubTokens[index];

  throw SLangException("(SExpression) index out of bounds %d : %d", index, mSubTokens.size());
  return *this;
}

/**
  This method returns the sub-token having the provided name as it's first subtoken
  @param str The 'command' name of the subtoken
  @return The requested sub-token
*/
const Klaus::SExpression & Klaus::SExpression::getSubTokenByName(const std::string& str) const
{
  for(size_t i = 0; i < mSubTokens.size(); i++)
    if((mSubTokens[i].mType == Normal) && (!mSubTokens[i].mSubTokens.empty()) && (mSubTokens[i].mSubTokens[0].getAsStdString() == str))
      return mSubTokens[i];

  return Klaus::SExpression::InvalidExpression;
}

/**
  Short named member method for getting number of sub-tokens in the token
  @return Number of sub-tokens in the token
*/
size_t Klaus::SExpression::size() const
{
  return mSubTokens.size();
}

/**
  This method checks whether the token list is empty or not
  @return True if the expression has no sub-tokens, false otherwise
*/
bool Klaus::SExpression::empty() const
{
  return mSubTokens.empty();
}

/**
  This method return the value of the token as a number
  @return The number extracted from the token
*/
double Klaus::SExpression::number() const
{
  return getAsDouble();
}

/**
  This method returns the value in token as a standard string
  @return The extracted string from the token
*/
std::string Klaus::SExpression::str() const
{
  return getAsStdString();
}

/**
  This method return the value of the token as a boolean
  @return The boolean flag extracted from the token
*/
bool Klaus::SExpression::boolean() const
{
  char c = (*mSExpStr)[mBegin];
  if((c == 'T') || (c == 't') || (c == '1') || (c == 'y') || (c == 'Y'))
    return true;
  else
    return false;
}

/**
  This method checks whether the expression is a root token or not
  @return True if the expression is a root token, false otherwise
*/
bool Klaus::SExpression::isRoot() const
{
  return mType == Root;
}

/**
  This method checks whether the expression is an atom token or not
  @return True if the expression is an atom token, false otherwise
*/
bool Klaus::SExpression::isAtom() const
{
  return mType == Atom;
}

/**
  This method checks whether the expression is a normal token or not
  @return True if the expression is a normal token, false otherwise
*/
bool Klaus::SExpression::isNormal() const
{
  return mType == Normal;
}

/**
  This method checks whether the expression is a comment or not
  @return True if the expression is a comment, false otherwise
*/
bool Klaus::SExpression::isComment() const
{
  return mType == Comment;
}

/**
  This method finds the first non comment token in the tokens subtokens.
  @return Index of the first non comment token, -1 if nothing was found
*/
int Klaus::SExpression::getFirstNonComment() const
{
  for(size_t i = 0; i < mSubTokens.size(); ++i)
    if(mSubTokens[i].isComment() == false)
      return (int)i;

  return -1;
}

/**
  This method searches the s-expression for the next non
  comment token after the given token index.
  @param inum The index to begin the search after
  @return The next non comment token index, -1 if nothing was found
*/
int Klaus::SExpression::getNextNonComment(const int& inum) const
{
  for(int i = inum + 1; i < (int)mSubTokens.size(); ++i)
    if(mSubTokens[i].isComment() == false)
      return i;

  return -1;
}

/**
  Overloaded index operator to access a sub-token by it's index
  @param i Index of the sub-token
  @return The sub-token instance. If index was out of bounds an exception will be thrown
*/
const Klaus::SExpression & Klaus::SExpression::operator [] (const size_t& i) const
{
  return getSubToken(i);
}

/**
  Overloaded index operator to access a sub-token by it's own starting sub-token
  (which in several cases means the command name or the node name of the token)
  @param str The name (command name or node name) of the token
*/
const Klaus::SExpression & Klaus::SExpression::operator [] (const std::string& str) const
{
  return getSubTokenByName(str);
}

/**
  Overloaded check operator for SExpression.
  @return True if the expression was valid, false otherwise
*/
bool Klaus::SExpression::operator!() const
{
  return mType == Unknown;
}

/**
  Equality check operator for checking the expression to be equal to a given string
  @param str The string to check equality to
  @return True if the expression is same as the given str, false otherwise
*/
bool Klaus::SExpression::operator==(const std::string& str) const
{
  return mSExpStr->compare(mBegin, str.size(), str) == 0;
}

/**
  Equality check operator for checking the expression to be equal to a given number
  @param number The number to check equality to
  @return True if the expression is same as the given number, false otherwise
*/
bool Klaus::SExpression::operator==(const double& number) const
{
  return getAsDouble() == number;
}

/**
  Inequality check operator for checking the expression to be inequal to a given string
  @param str The string to check inequality to
  @return True if the expression is not the same as the given str, false otherwise
*/
bool Klaus::SExpression::operator!=(const std::string& str) const
{
  return mSExpStr->compare(mBegin, str.size(), str) != 0;
}

/**
  Inequality check operator for checking the expression to be inequal to a given number
  @param number The number to check inequality to
  @return True if the expression is not the same as the given number, false otherwise
*/
bool Klaus::SExpression::operator!=(const double& number) const
{
  return getAsDouble() != number;
}

/**
  Cast operator for casting the expression to a standard string
*/
Klaus::SExpression::operator std::string() const
{
  return str();
}

/**
  Cast operator for casting the expression to a number
*/
Klaus::SExpression::operator double () const
{
  return getAsDouble();
}

/**
  Overloaded stream operator for printing contents of the token into a stream
  @param os The output stream to write token contents
  @param token The token to write it's content
  @return The given output stream instance for recursive streaming
*/
std::ostream & operator << (std::ostream& os, const Klaus::SExpression& token)
{
  return os << token.getAsStdString();
}

/**
  Overloaded stream operator for printing contents of the token into a stream
  @param os The output stream to write token contents
  @param token The token to write it's content
  @return The given output stream instance for recursive streaming
*/
std::ostream & operator << (std::ostream& os, const Klaus::SExpression * token)
{
  return os << token->getAsStdString();
}
