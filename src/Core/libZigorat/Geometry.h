/*
*********************************************************************************
*            Geometry.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class declarations of different geometry classes                   *
*            Most materials of this files are owned & copyrighted by Jelle Kok  *
*                                                                               *
*********************************************************************************
*/

/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file Geometry.h
<pre>
<b>File:</b>          Geomtery.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class declarations of different geometry classes
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#ifndef GEOMETRY
#define GEOMETRY


#include "Angles.h"     // needed Degree
#include <string>       // needed for string
#include <vector>       // needed for vector
#include <iostream>     // needed for ostream operator overloading


namespace Klaus // To clraify the ambiguity between ogre and this library
{


  #define SMALL_EPSILON 0.0001  /*!< Value used for floating point equality tests. */


  // auxiliary numeric functions for determining the
  // maximum and minimum of two given double values and the sign of a value
  _DLLExportControl double Max     ( double d1, double d2 );
  _DLLExportControl double Min     ( double d1, double d2 );
  _DLLExportControl int    sign    ( double d1            );

  // various goniometric functions
  _DLLExportControl Degree getAngleDifference(Degree ang1, Degree ang2);

  _DLLExportControl double drand   (                      );
  _DLLExportControl int    Round   ( double x             );
  _DLLExportControl void   Randomize(                     );
  _DLLExportControl int    Random  ( int max              );
  _DLLExportControl int    Random  ( int min, int max     );


  /*! Specifies the coordinate system type */
  enum CoordSystemT
  {
    CARTESIAN,  /*!< Cartesian coordinate system */
    POLAR       /*!< Polar coordinate system */
  };


  /*! This class contains an x- and y-coordinate of a position (x,y) as
      member data and methods which operate on this position. The
      standard arithmetic operators are overloaded and can thus be
      applied to positions (x,y). It is also possible to represent a
      position in polar coordinates (r,phi), since the class contains a
      method to convert these into cartesian coordinates (x,y).
  */
  class _DLLExportControl VecPosition
  {
    private:
      double mX;   /*!< x-coordinate of this position */
      double mY;   /*!< y-coordinate of this position */
      double mZ;   /*!< z-coordinate of this position */

    public:
      VecPosition                               ( double            vx = 0,
                                                  double            vy = 0,
                                                  double            vz = 0,
                                                  CoordSystemT      cs =CARTESIAN);

      // overloaded arithmetic operators
      VecPosition        operator -             (                                ) const;
      VecPosition        operator +             ( const double      &d           ) const;
      VecPosition        operator +             ( const VecPosition &p           ) const;
      VecPosition        operator -             ( const double      &d           ) const;
      VecPosition        operator -             ( const VecPosition &p           ) const;
      VecPosition        operator *             ( const double      &d           ) const;
      VecPosition        operator *             ( const VecPosition &p           ) const;
      VecPosition        operator /             ( const double      &d           ) const;
      VecPosition        operator /             ( const VecPosition &p           ) const;
      void               operator =             ( const double      &d           );
      void               operator +=            ( const VecPosition &p           );
      void               operator +=            ( const double      &d           );
      void               operator -=            ( const VecPosition &p           );
      void               operator -=            ( const double      &d           );
      void               operator *=            ( const VecPosition &p           );
      void               operator *=            ( const double      &d           );
      void               operator /=            ( const VecPosition &p           );
      void               operator /=            ( const double      &d           );
      bool               operator !=            ( const VecPosition &p           ) const;
      bool               operator !=            ( const double      &d           ) const;
      bool               operator ==            ( const VecPosition &p           ) const;
      bool               operator ==            ( const double      &d           ) const;
      double             operator []            ( const int & iIndex             ) const;
      double &           operator []            ( const int & iIndex             );

      // methods for producing output
      friend std::ostream&operator <<            ( std::ostream           &os,
                                                  VecPosition       v            );
      std::string        str                    ( CoordSystemT      cs =CARTESIAN) const;

      // set- and get methods for derived position information
      void               setPosition            ( double            dX = 0,
                                                  double            dY = 0,
                                                  double            dZ = 0       );
      void               setVector              ( double dR,
                                                  Degree dTheta,
                                                  Degree dPhi = 0                );
      double             getDistanceTo          ( const VecPosition &p           ) const;
      double             getDistanceOnPlane     ( const VecPosition &p           ) const;
      VecPosition        setMagnitude           ( double            d            );
      double             getMagnitude           (                                ) const;
      Degree             getDirection           (                                ) const;
      Degree             getDirectionFrom       ( const VecPosition & p          ) const;
      Degree             getTheta               (                                ) const;
      Degree             getPhi                 (                                ) const;

      // comparison methods for positions
      bool               isBetweenX             ( const VecPosition &p1,
                                                  const VecPosition &p2          );
      bool               isBetweenX             ( const double      &d1,
                                                  const double      &d2          );
      bool               isBetweenY             ( const VecPosition &p1,
                                                  const VecPosition &p2          );
      bool               isBetweenY             ( const double      &d1,
                                                  const double      &d2          );
      bool               isBetweenZ             ( const VecPosition &p1,
                                                  const VecPosition &p2          );
      bool               isBetweenZ             ( const double      &d1,
                                                  const double      &d2          );

      // conversion methods for positions
      VecPosition        normalize              (                                );
      VecPosition        rotate                 ( Degree            angle        );
      VecPosition        globalToRelative       ( VecPosition       orig,
                                                  Degree            ang          );
      VecPosition        relativeToGlobal       ( VecPosition       orig,
                                                  Degree            ang          );
      VecPosition        getVecPositionOnLineFraction( VecPosition  &p,
                                                  double            dFrac        );

      // 3D vector operations
      double             dot                    ( const VecPosition & vec        ) const;
      VecPosition        cross                  ( const VecPosition & vec        ) const;
      bool               isParallelTo           ( const VecPosition & vec        ) const;

      // static class methods
      static VecPosition getVecPositionFromPolar( double            dMag,
                                                  Degree            theta,
                                                  Degree            phi          );

      /*! A dynamic array of points or vectors */
      typedef std::vector<VecPosition> Array;

      /*! Unknown represents an invalid or uncalculatable position in calculations. */
      static const VecPosition Unknown;
    };


    /*! This class represents a circle. A circle is defined
        by its position (which denotes the center) and radius.
    */
    class _DLLExportControl Circle
    {
        VecPosition mCenter;                  /*!< Center of the circle */
        double      mRadius;                  /*!< Radius of the circle */

    public:
                    Circle                    (                         );
                    Circle                    ( const VecPosition & pos,
                                            const double & dR       );

        // get and set methods
        bool        setCircle                 ( const VecPosition & pos,
                                            const double & dR       );
        bool        setRadius                 ( const double & dR       );
        double      getRadius                 (                         ) const;
        bool        setCenter                 ( const VecPosition & pos );
        VecPosition getCenter                 (                         ) const;
        double      getCircumference          (                         ) const;
        double      getArea                   (                         ) const;
        double      getVolume                 (                         ) const;

        // calculate intersection points and area with other circle
        bool        isInside                  ( VecPosition pos,
                                                bool bForCircle = true  );
        int         getIntersectionWithCircle ( const Circle &c,
                                                VecPosition &p1,
                                                VecPosition &p2         ) const;
        double      getIntersectionArea       ( const Circle & c,
                                                bool bStrict = false    ) const;
    };


    /*! This class represents a sphere. A sphere is defined by one VecPosition
        (which denotes the center) and its radius
    */
    typedef Circle Sphere;


    /// Forward decleration needed for use in Triangle
    class _DLLExportControl Line3;


    /*! This class represents a triangle. A triangle is constructed with three
        points. This class provides various useful methods for triangles.
    */
    class _DLLExportControl Triangle
    {
      private:
        VecPosition   mPoints[3];     /*!< Foci of the triangle        */

      public:
                      Triangle        ( const VecPosition & p1,
                                        const VecPosition & p2,
                                        const VecPosition & p3 );
                      Triangle        ( const VecPosition points[]      );
                      Triangle        ( const VecPosition::Array points );
                      Triangle        (                                 );

        VecPosition   getPoint        ( const int & num                 );
        void          setPoint        ( const int & num,
                                        const VecPosition & p           );
        void          setPoints       ( const VecPosition & p1,
                                        const VecPosition & p2,
                                        const VecPosition & p3          );
        void          setPoints       ( const VecPosition points[]      );
        void          setPoints       ( const VecPosition::Array points );

        Degree        getAngleOnVertex( const int & num                 ) const;
        double        getLength       ( const int & num1,
                                        const int & num2                ) const;

        double        getArea         (                                 ) const;
        double        getCircumference(                                 ) const;

        static Degree getAngle        ( const VecPosition & center,
                                        const VecPosition & p1,
                                        const VecPosition & p2          );
        static Degree getAngle        ( const double & len1,
                                        const double & len2,
                                        const double & len3             );
    };


    /// Forward decleration is needed to prevent nested inclusion
    class _DLLExportControl Quaternion;


    /*! This class represents an ellipse. The ellipse is constructed with providing
        it's total length and width. The ellipse can return a spacial ellipsiod if
        the proper transformation quaternion is given.
    */
    class _DLLExportControl Ellipse
    {
      private:
        double      mA;              /*!< Half of the ellipses length (horizontal radius) */
        double      mC;              /*!< Half of the ellipses width (vertical radius) */
        VecPosition mCenter;         /*!< Center of the ellipse */
        Quaternion *mOrientation;    /*!< Spacial orientation of the ellipse */

      public:
                    Ellipse          (                             );
                    Ellipse          ( const double & length,
                                       const double & width        );
                    Ellipse          ( const double & length,
                                       const double & width,
                                       const VecPosition & c,
                                       const Quaternion & q        );
                  ~ Ellipse          (                             );

        void        setEllipse       ( const double & length,
                                       const double & width,
                                       const VecPosition & c,
                                       const Klaus::Quaternion & q );

        VecPosition getPointWithAngle( const Degree & theta        );

        void        setCenter        ( const VecPosition & c       );
        VecPosition getCenter        (                             ) const;

        void        setOrientation   ( const Klaus::Quaternion & q );
        Quaternion  getOrientation   (                             ) const;

        void        setLength        ( const double & length       );
        double      getLength        (                             ) const;

        void        setWidth         ( const double & width        );
        double      getWidth         (                             ) const;
    };


    /*! This class contains the representation of a line. A line is defined
        by the formula ay + bx + c = 0. The coefficients a, b and c are stored
        and used in the calculations.
    */
    class _DLLExportControl Line
    {
      private:
        // a line is defined by the formula: ay + bx + c = 0
        double      mA; /*!< This is the a coefficient in the line ay + bx + c = 0 */
        double      mB; /*!< This is the b coefficient in the line ay + bx + c = 0 */
        double      mC; /*!< This is the c coefficient in the line ay + bx + c = 0 */

      public:
                    Line                       ( double a, double b, double c      );

        double &    operator []                ( int iIndex                        );
        double      operator []                ( int iIndex                        ) const;

        // print methods
        friend std::ostream& operator <<       ( std::ostream & os, Line l         );
        std::string str                        (                                   ) const;

        // get intersection points with this line
        VecPosition getIntersection            ( Line      line                    ) const;
        int         getCircleIntersectionPoints( Circle      circle,
                                                 VecPosition *posSolution1,
                                                 VecPosition *posSolution2         ) const;
        Line        getTangentLine             ( VecPosition pos                   ) const;
        VecPosition getPointOnLineClosestTo    ( VecPosition pos                   ) const;
        double      getDistanceWithPoint       ( VecPosition pos                   ) const;
        bool        isInBetween                ( VecPosition pos,
                                                 VecPosition point1,
                                                 VecPosition point2                ) const;

        // calculate associated variables in the line
        double      getYGivenX                 ( double      x                     ) const;
        double      getXGivenY                 ( double      y                     ) const;
        double      getACoefficient            (                                   ) const;
        double      getBCoefficient            (                                   ) const;
        double      getCCoefficient            (                                   ) const;

        // static methods to make a line using an easier representation.
        static Line makeLineFromTwoPoints      ( VecPosition pos1,
                                                 VecPosition pos2                  );
        static Line makeLineFromPositionAndAngle( VecPosition vec,
                                                 Degree angle                      );
    };


    /*! This class represents a rectangle. A rectangle is defined by two
        VecPositions the one at the upper left corner and the one at the
        right bottom.
    */
    class _DLLExportControl Rect
    {
      private:
        VecPosition  mPosLeftTop;     /*!< top left position of the rectangle      */
        VecPosition  mPosRightBottom; /*!< bottom right position of the rectangle  */

      public:
        Rect                           ( VecPosition pos, VecPosition pos2 );
        Rect                           (                                   );

        // checks whether point lies inside the rectangle
        bool         isInside          ( VecPosition pos                   ) const;

        // standard get and set methods
        void         setRectPoints     ( VecPosition pos1,
                                         VecPosition pos2                  );
        bool         setPosLeftTop     ( VecPosition pos                   );
        VecPosition  getPosLeftTop     (                                   ) const;

        bool        setPosRightBottom ( VecPosition pos                   );
        VecPosition getPosRightBottom (                                   ) const;

        bool        setLength         ( const double & l                  );
        double      getLength         (                                   ) const;

        bool        setWidth          ( const double & w                  );
        double      getWidth          (                                   ) const;

        std::string str               (                                   );
    };


    /*! Forward decleration of class Line3. Needed for it is used in Plane class */
    class Line3;


    /*! This class represents a plane in 3 Dimensional space. A plane is
        defined by a normal vector and a point in the plane.
    */
    class _DLLExportControl Plane
    {
      private:
        double      mDelta;           /*!< Distance of the plane to origin */
        VecPosition mNormal;          /*!< Plane normal vector */

      public:
        Plane                         (                                   );
        Plane                         ( const VecPosition & norm,
                                        const VecPosition & pos           );

        std::string str               (                                   ) const;

        // checks whether point lies in the plane
        bool        isInside          ( VecPosition pos                   ) const;

        // arbirary point on plane
        VecPosition getArbitraryPoint (                                   ) const;

        // standard get and set methods
        void        setPlane          ( VecPosition newNormal,
                                        VecPosition newPoint              );
        void        setPlaneNormal    ( VecPosition newNormal             );
        VecPosition getPlaneNormal    (                                   ) const;
        void        setPlaneDelta     ( double newDelta                   );
        double      getPlaneDelta     (                                   ) const;

        // Point - Plane methods
        double      getDistanceToPoint( const VecPosition & pos           ) const;
        VecPosition getPointOnPlaneClosestTo( const VecPosition & pos     ) const;
        VecPosition getMirroredPoint  ( const VecPosition & pos           ) const;

        // Line plane methods are all declared in line3 class


        // Plane - Plane methods
        Degree      getDihedralAngle  ( const Plane & p                   ) const;
        bool        getIntersectionWithPlane( const Plane & p, Line3 &l   ) const;


        // some very useful and common planes
        static const Plane XY; /*!< Plane which includes X and Y axis */
        static const Plane YZ; /*!< Plane which includes X and Y axis */
        static const Plane XZ; /*!< Plane which includes X and Y axis */
    };


    /*! This class represents a line in space. A line in space can be described with
        lots of types of descriptions. Here I have chosen to use a vector and a point
        to describe the line, for simplification of implementation.
    */
    class _DLLExportControl Line3
    {
      private:
        VecPosition mVector;                   /*!< Vector of the line             */
        VecPosition mPoint;                    /*!< A point to uniquely identify   */

      public:
                    // Constructors
                    Line3                      ( VecPosition p1, VecPosition p2    );
                    Line3                      (                                   );
                    Line3                      ( const Line & l                    );
                    Line3                      ( VecPosition pos,
                                                 Degree theta,
                                                 Degree phi                        );

        // Creators
        bool        makeLineFromTwoPoints      ( VecPosition p1, VecPosition p2    );
        bool        makeLineFromVectorAndPoint ( VecPosition v, VecPosition p      );
        void        makeLineFromLineAndXYPlane ( const Line & l                    );
        void        makeLineFromPointAndAngle  ( VecPosition pos,
                                                 Degree theta,
                                                 Degree phi                        );

        // Line utilties
        bool        isPointOnLine              ( const VecPosition & p             ) const;
        double      getDistanceWithLine        ( const Line3 & l                   ) const;
        double      getDistanceWithPoint       ( const VecPosition & p             ) const;
        VecPosition getPointOnLineClosestTo    ( const VecPosition & p             ) const;
        bool        getIntersectionWithLine    ( const Line3 & l, VecPosition & p  ) const;
        bool        getIntersectionWithPlane   ( const Plane & p, VecPosition & pos) const;
        bool        getProjectionOnPlane       ( const Plane & p, Line3 & l        ) const;
        bool        getLineLineIntersection    ( const Line3 & l,
                                                 Line3 & result,
                                                 VecPosition *pa = NULL,
                                                 VecPosition *pb = NULL            ) const;

        // Get and Set methods
        VecPosition getVector                  (                                   ) const;
        void        setVector                  ( const VecPosition &v              );

        VecPosition getPoint                   (                                   ) const;
        void        setPoint                   ( const VecPosition &p              );

        // operators
        void         operator =                 ( const Line3       &l              );
        bool         operator !=                ( const Line3       &l              ) const;
        bool         operator ==                ( const Line3       &l              ) const;

        // streaming
        std::string  str                        (                                   ) const;
    };


}; //end of namespace Klaus


#endif // GEOMETRY

