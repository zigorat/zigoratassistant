/*
*********************************************************************************
*           Geometry.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class definitions of different geometry classes                    *
*            Most materials of this files are owned & copyrighted by Jelle Kok  *
*                                                                               *
*********************************************************************************
*/

/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file Geometry.cpp
<pre>
<b>File:</b>          Geomtery.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class definitions of different geometry classes
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created from UvA source code
</pre>
*/

#include "Algebra.h"
#include <cstdlib>
#include <time.h>
#include <sstream>
#include <iomanip>

/** Needed to elminate excess digits */
#define setdecimal(x) std::setprecision(x) << std::setiosflags(std::ios::fixed|std::ios::showpoint)


namespace Klaus
{

/** This function returns the maximum of two given doubles.
    @param d1 first parameter
    @param d2 second parameter
    @return the maximum of these two parameters */
double Max(double d1, double d2)
{
  return (d1>d2)?d1:d2;
}

/** This function returns the minimum of two given doubles.
    @param d1 first parameter
    @param d2 second parameter
    @return the minimum of these two parameters */
double Min(double d1, double d2)
{
  return (d1<d2)?d1:d2;
}

/** This function returns the sign of a give double.
    1 is positive, -1 is negative
    @param d1 first parameter
    @return the sign of this double */
int sign(double d1)
{
  return (d1>0)?1:-1;
}

/** This method returns angular difference between two angles. It deals
    with the boundry  problem, thus  when ang1 is -170 and agn2 is 170,
    it returns 20
    @param ang1 First angle of comparison
    @param ang2 Second Angle of comparison
    @return Degree interval between the angles */
Degree getAngleDifference(Degree ang1, Degree ang2)
{
  Degree angDiff1 = fabs((ang1 - ang2).getDegree());

  if(ang1 < 0)
    ang1 = -180.0 - ang1.getDegree();
  else
    ang1 =  180.0 - ang1.getDegree();

  if(ang2 < 0)
    ang2 = -180.0 - ang2.getDegree();
  else
    ang2 =  180.0 - ang2.getDegree();

  Degree angDiff2 = fabs(ang1.getDegree() - ang2.getDegree());

  return Min(angDiff1.getDegree(), angDiff2.getDegree());
}

/** This method generates a random number between 0 .. 1
    @return a random between 0 .. 1
 */
double drand()
{
  return (1.0 * rand() / RAND_MAX);
}

/** This method rounds a double value supplied as the
    parameter to an integer value
    @param x Double value to round to an integer
    @return int Rounded integer of the supplied double value */
int Round(double x)
{
  return (int)(x + 0.5);
}

/** This method randomizes random seed generator. I should only
    be called once in the whole application */
void Randomize()
{
  srand((unsigned int)(time(NULL)));
}

/** This method returns a random number between 0 and max - 1
    @param max arange end to return a random number
    @return A random number between 0 and max - 1 */
int Random(int max)
{
  return (int)(drand() * max);
}

/** This method returns a random number between 0 and max - 1
    @param min Start of acceptable range for the random number
    @param max End of acceptable range for the random number
    @return A random number between 0 and max - 1 */
int Random(int min, int max)
{
  return Random(abs(max - min)) + min;
}

#define UnknownDoubleValue -1000.0 /**< A versatile definition here is needed, to not recursively include SoccerTypes*/



/*****************************************************************************/
/****************************  Class VecPosition  ****************************/
/*****************************************************************************/

/**
  Definition of static member variable VecPosition::Unknown
*/
const VecPosition VecPosition::Unknown(-1000.0, -1000.0, -1000.0);

/** Constructor for the VecPosition class. When the supplied
    Coordinate System type equals CARTESIAN, the arguments x and y
    denote the x- and y- and z-coordinates of the new position. When it
    equals POLAR however, the arguments x and y and z denote the polar
    coordinates of the new position; in this case x is thus equal to
    the distance r from the origin and y is equal to the angle theta
    that the polar vector makes with the x-axis and z is equal to angle phi.
    @param x the x-coordinate of the new position when cs == CARTESIAN; the
    distance of the new position from the origin when cs = POLAR
    @param y the y-coordinate of the new position when cs = CARTESIAN; the
    angle that the polar vector makes with the x-axis when cs = POLAR
    @param z the z-coordinate of the new position when cs = CARTESIAN; the
    angle that the polar vector makes with the z-axis when cs = POLAR
    @param cs a CoordSystemT indicating whether x and y denote cartesian
    coordinates or polar coordinates
    @return the VecPosition corresponding to the given arguments */
VecPosition::VecPosition(double x, double y, double z, CoordSystemT cs)
{
  if(cs == CARTESIAN)
    setPosition(x, y, z);
  else
    setVector(x, y, z);
}

/** Overloaded version of unary minus operator for VecPositions. It returns the
    negative VecPosition, i.e. both the x- and y-coordinates are multiplied by
    -1. The current VecPosition itself is left unchanged.
    @return a negated version of the current VecPosition */
VecPosition VecPosition::operator - () const
{
  return (VecPosition(-mX, -mY, -mZ));
}

/** Overloaded version of the binary plus operator for adding a given double
    value to a VecPosition. The double value is added to both the x- and
    y-coordinates of the current VecPosition. The current VecPosition itself is
    left unchanged.
    @param d a double value which has to be added to the x- and
    y- and z-coordinates of the current VecPosition
    @return the result of adding the given double value to the current
    VecPosition */
VecPosition VecPosition::operator + (const double &d) const
{
  return (VecPosition(mX + d, mY + d, mZ + d));
}

/** Overloaded version of the binary plus operator for VecPositions. It returns
    the sum of the current VecPosition and the given VecPosition by adding their
    x- and y- and z-coordinates. The VecPositions themselves are left unchanged.
    @param p a VecPosition
    @return the sum of the current VecPosition and the given VecPosition */
VecPosition VecPosition::operator + (const VecPosition &p) const
{
  return (VecPosition(mX + p.mX, mY + p.mY, mZ + p.mZ));
}

/** Overloaded version of the binary minus operator for subtracting a
    given double value from a VecPosition. The double value is
    subtracted from both the x- and y-coordinates of the current
    VecPosition. The current VecPosition itself is left unchanged.
    @param d a double value which has to be subtracted from the x- and
    y- and z-coordinates of the current VecPosition
    @return the result of subtracting the given double value from the current
    VecPosition */
VecPosition VecPosition::operator - (const double &d) const
{
  return (VecPosition(mX - d, mY - d, mZ - d));
}

/** Overloaded version of the binary minus operator for
    VecPositions. It returns the difference between the current
    VecPosition and the given VecPosition by subtracting their x- and
    y-coordinates. The VecPositions themselves are left unchanged.

    @param p a VecPosition
    @return the difference between the current VecPosition and the given
    VecPosition */
VecPosition VecPosition::operator - (const VecPosition &p) const
{
  return (VecPosition(mX - p.mX, mY - p.mY, mZ - p.mZ));
}

/** Overloaded version of the multiplication operator for multiplying a
    VecPosition by a given double value. All the x-, y- and z-coordinates
    of the current VecPosition are multiplied by this value. The current
    VecPosition itself is left unchanged.
    @param d the multiplication factor
    @return the result of multiplying the current VecPosition by the given
    double value */
VecPosition VecPosition::operator * (const double &d ) const
{
  return VecPosition(mX * d, mY * d, mZ * d);
}

/** Overloaded version of the multiplication operator for
    VecPositions. It returns the product of the current VecPosition
    and the given VecPosition by multiplying their x- and
    y- and z-coordinates. The VecPositions themselves are left unchanged.

    @param p a VecPosition
    @return the product of the current VecPosition and the given VecPosition */
VecPosition VecPosition::operator * (const VecPosition &p) const
{
  return (VecPosition(mX * p.mX, mY * p.mY, mZ * p.mZ));
}

/** Overloaded version of the division operator for dividing a
    VecPosition by a given double value. Both the x- and y-coordinates
    of the current VecPosition are divided by this value. The current
    VecPosition itself is left unchanged.

    @param d the division factor
    @return the result of dividing the current VecPosition by the given double
    value */
VecPosition VecPosition::operator / (const double &d) const
{
  double dd = 1.0 / d;
  return (VecPosition(mX * dd, mY * dd, mZ * dd ));
}

/** Overloaded version of the division operator for VecPositions. It
    returns the quotient of the current VecPosition and the given
    VecPosition by dividing their x- and y- and z-coordinates. The
    VecPositions themselves are left unchanged.

    @param p a VecPosition
    @return the quotient of the current VecPosition and the given one */
VecPosition VecPosition::operator / (const VecPosition &p) const
{
  return (VecPosition(mX / p.mX, mY / p.mY, mZ / p.mZ));
}

/** Overloaded version of the assignment operator for assigning a given double
    value to both the x- and y-coordinates of the current VecPosition. This
    changes the current VecPosition itself.
    @param d a double value which has to be assigned to both the x- and
    y-coordinates of the current VecPosition */
void VecPosition::operator = (const double &d)
{
  mX = d;
  mY = d;
  mZ = d;
}

/** Overloaded version of the sum-assignment operator for VecPositions. It
    returns the sum of the current VecPosition and the given VecPosition by
    adding their x- and y- and z- coordinates. This changes the current VecPosition
    itself.
    @param p a VecPosition which has to be added to the current VecPosition */
void VecPosition::operator +=(const VecPosition &p)
{
  mX += p.mX;
  mY += p.mY;
  mZ += p.mZ;
}

/** Overloaded version of the sum-assignment operator for adding a given double
    value to a VecPosition. The double value is added to both the x- and
    y-coordinates of the current VecPosition. This changes the current
    VecPosition itself.
    @param d a double value which has to be added to the x- and
    y- and z-coordinates of the current VecPosition */
void VecPosition::operator += (const double &d)
{
  mX += d;
  mY += d;
  mZ += d;
}

/** Overloaded version of the difference-assignment operator for
    VecPositions.  It returns the difference between the current
    VecPosition and the given VecPosition by subtracting their x- and
    y-coordinates. This changes the current VecPosition itself.

    @param p a VecPosition which has to be subtracted from the current
    VecPosition */
void VecPosition::operator -=(const VecPosition &p)
{
  mX -= p.mX;
  mY -= p.mY;
  mZ -= p.mZ;
}

/** Overloaded version of the difference-assignment operator for
    subtracting a given double value from a VecPosition. The double
    value is subtracted from both the x- and y-coordinates of the
    current VecPosition. This changes the current VecPosition itself.

    @param d a double value which has to be subtracted from the x- and
    y- and z-coordinates of the current VecPosition */
void VecPosition::operator -=(const double &d)
{
  mX -= d;
  mY -= d;
  mZ -= d;
}

/** Overloaded version of the multiplication-assignment operator for
    VecPositions. It returns the product of the current VecPosition
    and the given VecPosition by multiplying their x- and
    y-coordinates. This changes the current VecPosition itself.

    @param p a VecPosition by which the current VecPosition has to be
    multiplied */
void VecPosition::operator *=(const VecPosition &p)
{
  mX *= p.mX;
  mY *= p.mY;
  mZ *= p.mZ;
}

/** Overloaded version of the multiplication-assignment operator for
    multiplying a VecPosition by a given double value. Both the x- and
    y-coordinates of the current VecPosition are multiplied by this
    value. This changes the current VecPosition itself.

    @param d a double value by which the x- and y- and z-coordinates of the
    current VecPosition have to be multiplied */
void VecPosition::operator *=(const double &d)
{
  mX *= d;
  mY *= d;
  mZ *= d;
}

/** Overloaded version of the division-assignment operator for
    VecPositions. It returns the quotient of the current VecPosition
    and the given VecPosition by dividing their x- and
    y- and z-coordinates. This changes the current VecPosition itself.

    @param p a VecPosition by which the current VecPosition is divided */
void VecPosition::operator /=(const VecPosition &p)
{
  mX /= p.mX;
  mY /= p.mY;
  mZ /= p.mZ;
}

/** Overloaded version of the division-assignment operator for
    dividing a VecPosition by a given double value. Both the x- and
    y-coordinates of the current VecPosition are divided by this
    value. This changes the current VecPosition itself.

    @param d a double value by which both the x- and y- and z-coordinates of the
    current VecPosition have to be divided */
void VecPosition::operator /=(const double &d)
{
  double dd = 1.0 / d;
  mX *= dd;
  mY *= dd;
  mZ *= dd;
}

/** Overloaded version of the inequality operator for VecPositions. It
    determines whether the current VecPosition is unequal to the given
    VecPosition by comparing their x- and y-coordinates.

    @param p a VecPosition
    @return true when either the x- or y- or z-coordinates of the given VecPosition
    and the current VecPosition are different; false otherwise */
bool VecPosition::operator !=(const VecPosition &p) const
{
  return ((mX != p.mX) || (mY != p.mY) || (mZ != p.mZ));
}

/** Overloaded version of the inequality operator for comparing a
    VecPosition to a double value. It determines whether either the x-
    or y-coordinate of the current VecPosition is unequal to the given
    double value.

    @param d a double value with which both the x- and y-coordinates of the
    current VecPosition have to be compared.
    @return true when either the x- or y- and z-coordinate of the current VecPosition
    is unequal to the given double value; false otherwise */
bool VecPosition::operator !=(const double &d) const
{
  return ((mX != d) || (mY != d) || (mZ != d));
}

/** Overloaded version of the equality operator for VecPositions. It
    determines whether the current VecPosition is equal to the given
    VecPosition by comparing their x- and y-coordinates.

    @param p a VecPosition
    @return true when the x- and y- and z-coordinates of the given
    VecPosition and the current VecPosition are equal; false
    otherwise */
bool VecPosition::operator ==(const VecPosition &p) const
{
  return ((mX == p.mX) && (mY == p.mY) && (mZ == p.mZ));
}

/** Overloaded version of the equality operator for comparing a
    VecPosition to a double value. It determines whether both the x-
    and y-coordinates of the current VecPosition are equal to the
    given double value.

    @param d a double value with which both the x- and y-coordinates of the
    current VecPosition have to be compared.
    @return true when the x- and y- and z-coordinates of the current VecPosition
    are equal to the given double value; false otherwise */
bool VecPosition::operator ==(const double &d) const
{
  return ((mX == d) && (mY == d) && (mZ ==d));
}

/**
 * Overloaded version of index operator
 * @param iIndex Index of the requested coordinate
 * @return Requested coordinate value
 */
double & VecPosition::operator [](const int & iIndex)
{
  static double m_Value;

  if(iIndex < 0 || iIndex > 2)
  {
    std::cerr << "(VecPosition::operator []) Index out of range: " << iIndex << std::endl;
    m_Value = UnknownDoubleValue;
    return m_Value;
  }

  return *(&mX +iIndex);
}

/**
 * Overloaded version of index operator
 * @param iIndex Index of the requested coordinate
 * @return Requested coordinate value
 */
double VecPosition::operator [](const int & iIndex) const
{
  static double m_Value;

  if(iIndex < 0 || iIndex > 2)
  {
    std::cerr << "(VecPosition::operator []) Index out of range: " << iIndex << std::endl;
    m_Value = UnknownDoubleValue;
    return m_Value;
  }

  return *(&mX +iIndex);
}

/** Overloaded version of the C++ output operator for
    VecPositions. This operator makes it possible to use VecPositions
    in output statements for output streams. The x- and y-coordinates
    of the VecPosition are printed in the format (x,y).

    @param os output stream to which information should be written
    @param v a VecPosition which must be printed
    @return output stream containing (x,y,z) */
std::ostream& operator << (std::ostream &os, VecPosition v)
{
  return (os << "(" << v.mX << ", " << v.mY << ", " << v.mZ << ")");
}

/** This method writes the current VecPosition to a std::string. It can
    also write a polar representation of the current VecPosition.

    @param cs a CoordSystemtT indicating whether a POLAR or CARTESIAN
     representation of the current VecPosition should be written
    @return a std::string containing a polar or Cartesian representation of the
    current VecPosition depending on the value of the boolean argument */
std::string VecPosition::str(CoordSystemT cs) const
{
  std::stringstream buf;

  if(cs == CARTESIAN)
    buf << setdecimal(2) << "(" << mX << " " << mY << " " << mZ << ")";
  else
    buf << setdecimal(2) << "spheric(" << getMagnitude() << " " << getTheta() << " " << getPhi() << ")";

  return buf.str();
}

/** This method (re)sets the coordinates of the current
    VecPosition. The given coordinates is considered to be
    in cartesian coordinate system.

    @param dX a double value indicating a new Cartesian x-coordinate
    @param dY a double value indicating a new Cartesian y-coordinate
    @param dZ a double value indicating a new Cartesian z-coordinate
*/
void VecPosition::setPosition(double dX, double dY, double dZ)
{
  mX = dX;
  mY = dY;
  mZ = dZ;
}

/** This method (re)sets the coordinates of the current
    VecPosition. The given coordinates is considered to be
    in polar coordinate system.

    @param dR distance of the point with origin
    @param dTheta theta coordinate of the point
    @param dPhi Phi coordinate of the point
*/
void VecPosition::setVector(double dR, Degree dTheta, Degree dPhi)
{
  *this = getVecPositionFromPolar(dR, dTheta, dPhi);
}


/** This method determines the distance between the current
    VecPosition and a given VecPosition. This is equal to the
    magnitude (length) of the vector connecting the two positions
    which is the difference vector between them.

    @param p a Vecposition
    @return the distance between the current VecPosition and the given
    VecPosition */
double VecPosition::getDistanceTo(const VecPosition & p) const
{
  return ((*this - p).getMagnitude());
}

/** This method determines the distance between the point and the
    given point on the X-Y plane.
    @param p a VecPosition
    @return The distance of the point and the given point on the X-Y plane */
double VecPosition::getDistanceOnPlane(const VecPosition &p) const
{
  return sqrt((mX - p.mX)*(mX - p.mX) + (mY - p.mY)*(mY - p.mY));
}

/** This method adjusts the coordinates of the current VecPosition in
    such a way that the magnitude of the corresponding vector equals
    the double value which is supplied as an argument. It thus scales
    the vector to a given length by multiplying both the x- and
    y-coordinates by the quotient of the argument and the current
    magnitude. This changes the VecPosition itself.

    @param d a double value representing a new magnitude

    @return the result of scaling the vector corresponding with the
    current VecPosition to the given magnitude thus yielding a
    different VecPosition */
VecPosition VecPosition::setMagnitude(double d)
{
  if(getMagnitude() > SMALL_EPSILON)
     *this *= (d / getMagnitude());

  return *this;
}

/** This method determines the magnitude (length) of the vector
    corresponding with the current VecPosition using the formula of
    Pythagoras.

    @return the length of the vector corresponding with the current
    VecPosition */
double VecPosition::getMagnitude() const
{
  return (sqrt(mX * mX + mY * mY + mZ * mZ));
}

/** This method determines the direction of the vector corresponding
    with the current VecPosition (the phi-coordinate in polar
    representation) using the arc tangent function. Note that the
    signs of x and y have to be taken into account in order to
    determine the correct quadrant.

    @return the direction in degrees of the vector corresponding with
    the current VecPosition */
Degree VecPosition::getDirection() const
{
  return Degree::atan2(mY, mX);
}

Degree VecPosition::getDirectionFrom(const VecPosition & p) const
{
	return Degree::atan2(mY - p[1], mX - p[0]);
}

/** This method determines the theta of the vector corresponding
    with the current VecPosition (the theta-coordinate in polar
    representation) using the arc tangent function. Note that the
    signs of x and y and z have to be taken into account in order to
    determine the correct quadrant.

    @return the theta in degrees of the vector corresponding with
    the current VecPosition */
Degree VecPosition::getTheta() const
{
  return Degree::atan2(mY, mX);
}

/** This method determines the phi of the vector corresponding
    with the current VecPosition (the phi-coordinate in polar
    representation) using the arc tangent function. Note that the
    signs of x and y and z have to be taken into account in order to
    determine the correct quadrant.

    @return the phi in degrees of the vector corresponding with
    the current VecPosition */
Degree VecPosition::getPhi() const
{
  double dXYPict = sqrt(mX * mX + mY * mY);
  return Degree::atan2(mZ, dXYPict);
}

/** This method determines whether the current VecPosition is in
    between two given VecPositions when looking in the x-direction,
    i.e. whether the current VecPosition is in front of the first
    argument and behind the second.

    @param p1 a VecPosition to which the current VecPosition must be
    compared

    @param p2 a VecPosition to which the current VecPosition must be
    compared

    @return true when the current VecPosition is in between the two
    given VecPositions when looking in the x-direction; false
    otherwise */
bool VecPosition::isBetweenX(const VecPosition &p1, const VecPosition &p2)
{
  if(p1[0] < p2[0])
    return mX >= p1[0] && mX <= p2[0];
  else
    return mX >= p2[0] && mX <= p1[0];
}

/** This method determines whether the x-coordinate of the current
    VecPosition is in between two given double values, i.e. whether
    the x-coordinate of the current VecPosition is in front of the
    first argument and behind the second.

    @param d1 a double value to which the current x-coordinate must be
    compared

    @param d2 a double value to which the current x-coordinate must be
    compared

    @return true when the current x-coordinate is in between the two
    given values; false otherwise */
bool VecPosition::isBetweenX(const double &d1, const double &d2)
{
  if(d1 < d2)
    return mX >= d1 && mX <= d2;
  else
    return mX >= d2 && mX <= d1;
}

/** This method determines whether the current VecPosition is in
    between two given VecPositions when looking in the y-direction,
    i.e. whether the current VecPosition is to the right of the first
    argument and to the left of the second.

    @param p1 a VecPosition to which the current VecPosition must be
    compared

    @param p2 a VecPosition to which the current VecPosition must be
    compared

    @return true when the current VecPosition is in between the two
    given VecPositions when looking in the y-direction; false
    otherwise */
bool VecPosition::isBetweenY(const VecPosition &p1, const VecPosition &p2)
{
  if(p1[1] < p2[1])
    return mY >= p1[1] && mY <= p2[1];
  else
    return mY >= p2[1] && mY <= p1[1];
}

/** This method determines whether the y-coordinate of the current
    VecPosition is in between two given double values, i.e. whether
    the y-coordinate of the current VecPosition is to the right of the
    first argument and to the left of the second.

    @param d1 a double value to which the current y-coordinate must be
    compared

    @param d2 a double value to which the current y-coordinate must be
    compared

    @return true when the current y-coordinate is in between the two
    given values; false otherwise */
bool VecPosition::isBetweenY(const double &d1, const double &d2)
{
  if(d1 < d2)
    return mY >= d1 && mY <= d2;
  else
    return mY >= d2 && mY <= d1;
}

/** This method determines whether the current VecPosition is in
    between two given VecPositions when looking in the z-direction,
    i.e. whether the current VecPosition is in up of the first
    argument and down the second.

    @param p1 a VecPosition to which the current VecPosition must be
    compared

    @param p2 a VecPosition to which the current VecPosition must be
    compared

    @return true when the current VecPosition is in between the two
    given VecPositions when looking in the z-direction; false
    otherwise */
bool VecPosition::isBetweenZ(const VecPosition &p1, const VecPosition &p2)
{
  if(p1[2] < p2[2])
    return mZ >= p1[2] && mZ <= p2[2];
  else
    return mZ >= p2[2] && mZ <= p1[2];
}

/** This method determines whether the z-coordinate of the current
    VecPosition is in between two given double values, i.e. whether
    the z-coordinate of the current VecPosition is upper than the
    first argument and down the second.

    @param d1 a double value to which the current z-coordinate must be
    compared

    @param d2 a double value to which the current z-coordinate must be
    compared

    @return true when the current z-coordinate is in between the two
    given values; false otherwise */
bool VecPosition::isBetweenZ(const double &d1, const double &d2)
{
  if(d1 < d2)
    return mZ >= d1 && mZ <= d2;
  else
    return mZ >= d2 && mZ <= d1;
}

/** This method normalizes a VecPosition by setting the magnitude of
    the corresponding vector to 1. This thus changes the VecPosition
    itself.

    @return the result of normalizing the current VecPosition thus
    yielding a different VecPosition */
VecPosition VecPosition::normalize()
{
  return setMagnitude(1.0);
}

/** This method rotates the vector corresponding to the current
    VecPosition over a given angle thereby changing the current
    VecPosition itself. This is done by calculating the polar
    coordinates of the current VecPosition and adding the given angle
    to the phi-coordinate in the polar representation. The polar
    coordinates are then converted back to Cartesian coordinates to
    obtain the desired result.

    @param angle an angle in degrees over which the vector
    corresponding to the current VecPosition must be rotated

    @return the result of rotating the vector corresponding to the
    current VecPosition over the given angle thus yielding a different
    VecPosition */
VecPosition VecPosition::rotate(Degree angle)
{
  // determine the polar representation of the current VecPosition
  double dMag    = getMagnitude();
  Degree dNewDir = getDirection() + angle;  // add rotation angle to theta
  setVector(dMag, dNewDir, getPhi()); // convert back to Cartesian
  return *this;
}

/** This method converts the coordinates of the current VecPosition
    (which are represented in an global coordinate system with the
    origin at (0,0,0)) into relative coordinates in a different
    coordinate system (e.g. relative to a player). The new coordinate
    system is defined by the arguments to the method. The relative
    coordinates are now obtained by aligning the relative coordinate
    system with the global coordinate system using a translation to
    make both origins coincide followed by a rotation to align the
    axes.

    @param origin the origin of the relative coordinate frame

    @param ang the angle between the world frame and the relative
    frame (reasoning from the world frame)

    @return the result of converting the current global VecPosition
    into a relative VecPosition */
VecPosition VecPosition::globalToRelative(VecPosition origin, Degree ang)
{
  // convert global coordinates into relative coordinates by aligning
  // relative frame and world frame. First perform translation to make
  // origins of both frames coincide. Then perform rotation to make
  // axes of both frames coincide (use negative angle since you rotate
  // relative frame to world frame).
  *this -= origin;
  return (rotate(-ang));
}

/** This method converts the coordinates of the current VecPosition
    (which are represented in a relative coordinate system) into
    global coordinates in the world frame (with origin at (0,0,0)). The
    relative coordinate system is defined by the arguments to the
    method. The global coordinates are now obtained by aligning the
    world frame with the relative frame using a rotation to align the
    axes followed by a translation to make both origins coincide.

    @param origin the origin of the relative coordinate frame

    @param ang the angle between the world frame and the relative
    frame (reasoning from the world frame)

    @return the result of converting the current relative VecPosition
    into an global VecPosition */
VecPosition VecPosition::relativeToGlobal(VecPosition origin, Degree ang)
{
  // convert relative coordinates into global coordinates by aligning
  // world frame and relative frame. First perform rotation to make
  // axes of both frames coincide (use positive angle since you rotate
  // world frame to relative frame). Then perform translation to make
  // origins of both frames coincide.
  rotate(ang);
  *this += origin;
  return (*this);
}

/** This method returns a VecPosition that lies somewhere on the
    vector between the current VecPosition and a given
    VecPosition. The desired position is specified by a given fraction
    of this vector (e.g. 0.5 means exactly in the middle of the
    vector). The current VecPosition itself is left unchanged.

    @param p a VecPosition which defines the vector to the current
    VecPosition

    @param dFrac double representing the fraction of the connecting
    vector at which the desired VecPosition lies.

    @return the VecPosition which lies at fraction dFrac on the vector
    connecting p and the current VecPosition */
VecPosition VecPosition::getVecPositionOnLineFraction(VecPosition &p,
                                                       double      dFrac)
{
  // determine point on line that lies at fraction dFrac of whole line
  // example: this --- 0.25 ---------  p
  // formula: this + dFrac * (p - this) = this - dFrac * this + dFrac * p =
  //          (1 - dFrac) * this + dFrac * p
  return ((*this) * (1.0 - dFrac) + (p * dFrac));
}

/**
 * This method calculates dot multiplication between two vectors
 * @param vec Vector to dot
 * @return Dot mulitplication result with the supplied vetor
 */
double VecPosition::dot(const VecPosition & vec) const
{
  return
    (vec[0] * mX) + (vec[1] * mY) + (vec[2] * mZ);
}

/**
 * This method calulates cross multiplication between two vectors
 * @param vec Vector to calculates cross multiplication with
 * @return Result of cross multiplication
 */
VecPosition VecPosition::cross(const VecPosition & vec) const
{
  return
    VecPosition
       (
         mY * vec[2] - mZ * vec[1],
         mZ * vec[0] - mX * vec[2],
         mX * vec[1] - mY * vec[0]
      );
}

/**
 * This method checks whether two vectors are parallel or not
 * @param vec The vector to check if is parellel to this one
 * @return True if the vectors are parallel, false otherwise
 */
bool VecPosition::isParallelTo(const VecPosition & vec) const
{
  VecPosition p1 = *this;
  VecPosition p2 = vec;
  // If any of them were zero, then return false
  if(p1 == VecPosition() || p2 == VecPosition())
    return false;

  p1.normalize();
  p2.normalize();

  // two vectors are parellel if their normal vectors are the same
  return p1 == p2;
}

/** This method converts a polar representation of a VecPosition into
    a Cartesian representation.

    @param dMag a double representing the polar r-coordinate, i.e. the
    distance from the point to the origin

    @param theta the angle that the polar vector makes with the xy-plane,
    i.e. the polar theta-coordinate

    @param phi the angle that the polar vector makes with the z-axis,
    i.e. the polar phi-coordinate

    @return the result of converting the given polar representation
    into a Cartesian representation thus yielding a Cartesian
    VecPosition */
VecPosition VecPosition::getVecPositionFromPolar(double dMag, Degree theta, Degree phi)
{
  double z  = dMag * phi.sin();
  double XY = dMag * phi.cos();
  double x  = XY   * theta.cos();
  double y  = XY   * theta.sin();
  return VecPosition(x, y, z);
}




/*****************************************************************************/
/*******************************  Class Circle  ******************************/
/*****************************************************************************/

/** This is the constructor of a circle.
    @param pos first point that defines the center of circle
    @param dR the radius of the circle
    @return circle with pos as center and radius as radius*/
Circle::Circle(const VecPosition & pos, const double & dR)
{
  setCircle(pos, dR);
}

/** This is the constructor of a circle which initializes a circle with a
    radius of zero. */
Circle::Circle()
{
  setCircle(VecPosition(0.0, 0.0, 0.0), 0);
}

/** This method sets the values of the circle.
    @param pos new center of the circle
    @param dR new radius of the circle (> 0)
    @return bool indicating whether radius was set */
bool Circle::setCircle(const VecPosition & pos, const double & dR)
{
  setCenter(pos);
  return setRadius(dR );
}

/** This method sets the radius of the circle.
    @param dR new radius of the circle (> 0)
    @return bool indicating whether radius was set */
bool Circle::setRadius(const double & dR)
{
  if(dR > 0)
  {
    mRadius = dR;
    return true;
  }
  else
  {
    mRadius = 0.0;
    return false;
  }
}

/** This method returns the radius of the circle.
    @return radius of the circle */
double Circle::getRadius() const
{
  return mRadius;
}

/** This method sets the center of the circle.
    @param pos new center of the circle
    @return bool indicating whether center was set */
bool Circle::setCenter(const VecPosition & pos)
{
  mCenter = pos;
  return true;
}

/** This method returns the center of the circle.
    @return center of the circle */
VecPosition Circle::getCenter() const
{
  return mCenter;
}

/** This method returns the circumference of the circle.
    @return circumference of the circle */
double Circle::getCircumference() const
{
  return 2.0 * M_PI * mRadius;
}

/** This method returns the area inside the circle.
    @return area inside the circle */
double Circle::getArea() const
{
  return M_PI * mRadius * mRadius;
}

/** This method returns the volume of a circle.
    @return volume inside the circle */
double Circle::getVolume() const
{
  return M_PI * 4 / 3 * mRadius * mRadius * mRadius;
}

/**
  This method returns a boolean that indicates whether 'pos' is
  located inside the circle.
  @param pos position of which should be checked whether it is
             located in the circle
  @param bForCircle denotes whether the check is made for a
                    circle or a sphere
  @return bool indicating whether pos lies inside the circle
*/
bool Circle::isInside(VecPosition pos, bool bForCircle)
{
  if(bForCircle)
    pos[2] = mCenter[2];

  return mCenter.getDistanceTo(pos) < mRadius ;
}

/** This method returns the two possible intersection points between two
    circles. This method returns the number of solutions that were found.
    @param c circle with which intersection should be found
    @param p1 will be filled with first solution
    @param p2 will be filled with second solution
    @return number of solutions. */
int Circle::getIntersectionWithCircle(const Circle & c, VecPosition & p1, VecPosition & p2) const
{
    double x0, y0, r0;
    double x1, y1, r1;

    x0 = mCenter[0];
    y0 = mCenter[1];
    r0 = mRadius;
    x1 = c.mCenter[0];
    y1 = c.mCenter[1];
    r1 = c.mRadius;

    double d, dx, dy, h, a, x, y, p2_x, p2_y;

    // first calculate distance between two centers circles P0 and P1.
    dx = x1 - x0;
    dy = y1 - y0;
    d = sqrt(dx*dx + dy*dy);

    // normalize differences
    dx /= d; dy /= d;

    // a is distance between p0 and point that is the intersection point P2
    // that intersects P0-P1 and the line that crosses the two intersection
    // points P3 and P4.
    // Define two triangles: P0,P2,P3 and P1,P2,P3.
    // with distances a, h, r0 and b, h, r1 with d = a + b
    // We know a^2 + h^2 = r0^2 and b^2 + h^2 = r1^2 which then gives
    // a^2 + r1^2 - b^2 = r0^2 with d = a + b ==> a^2 + r1^2 - (d-a)^2 = r0^2
    // ==> r0^2 + d^2 - r1^2 / 2*d
    a = (r0*r0 + d*d - r1*r1) / (2.0 * d);

    // h is then a^2 + h^2 = r0^2 ==> h = sqrt(r0^2 - a^2)
    double arg = r0*r0 - a*a;
    h = (arg > 0.0) ? sqrt(arg) : 0.0;

    // First calculate P2
    p2_x = x0 + a * dx;
    p2_y = y0 + a * dy;

    // and finally the two intersection points
    x =  p2_x - h * dy;
    y =  p2_y + h * dx;
    p1.setPosition(x, y);
    x =  p2_x + h * dy;
    y =  p2_y - h * dx;
    p2.setPosition(x, y);

    return (arg < 0.0) ? 0 : ((arg == 0.0) ? 1 :  2);
}

/** This method returns the size of the intersection area of two circles.
    @param c circle with which intersection should be determined
    @param bStrict whether to check the circles z- intersection
    @return size of the intersection area. */
double Circle::getIntersectionArea(const Circle & c, bool bStrict) const
{
  VecPosition pos1, pos2, pos3;
  double d, h, dArea;
  Degree ang;

  if(bStrict && (c.mCenter[2] != mCenter[2])) // If the circles are not
    return 0.0;                                                // in the same XY plane --> no intersection

  d = mCenter.getDistanceTo(c.mCenter); // dist between two centers
  if(d > c.mRadius + mRadius)           // larger than sum radii
    return 0.0;                                   // circles do not intersect
  if(d <= fabs(c.mRadius - mRadius))   // one totally in the other
  {
    double dR = Min(c.mRadius, mRadius);// return area smallest circ
    return M_PI*dR*dR;
  }

  int iNrSol = getIntersectionWithCircle(c, pos1, pos2);
  if(iNrSol != 2)
    return 0.0;

  // the intersection area of two circles can be divided into two segments:
  // left and right of the line between the two intersection points p1 and p2.
  // The outside area of each segment can be calculated by taking the part
  // of the circle pie excluding the triangle from the center to the
  // two intersection points.
  // The pie equals pi*r^2 * rad(2*ang) / 2*pi = 0.5*rad(2*ang)*r^2 with ang
  // the angle between the center c of the circle and one of the two
  // intersection points. Thus the angle between c and p1 and c and p3 where
  // p3 is the point that lies halfway between p1 and p2.
  // This can be calculated using ang = asin(d / r) with d the distance
  // between p1 and p3 and r the radius of the circle.
  // The area of the triangle is 2*0.5*h*d.

  pos3 = pos1.getVecPositionOnLineFraction(pos2, 0.5);
  d = pos1.getDistanceTo(pos3);
  h = pos3.getDistanceTo(getCenter());
  ang = asin(d / mRadius);

  dArea = ang.getDegree()*mRadius*mRadius;
  dArea = dArea - d*h;

  // and now for the other segment the same story
  h = pos3.getDistanceTo(c.getCenter());
  ang = asin(d / c.mRadius);
  dArea = dArea + ang.getDegree()*c.mRadius*c.mRadius;
  dArea = dArea - d*h;

  return dArea;
}




/*****************************************************************************/
/******************************  Class Triangle  *****************************/
/*****************************************************************************/

/**
  Constructor of Triangle with no parameters. Does nothing
*/
Triangle::Triangle()
{

}

/**
  Constructor of Triangle. Sets the triangle foci
  @param p1 The first focus
  @param p2 The second focus
  @param p3 The third focus
*/
Triangle::Triangle(const Klaus::VecPosition& p1, const Klaus::VecPosition& p2, const Klaus::VecPosition& p3)
{
  setPoints(p1, p2, p3);
}

/**
  Constructor of Triangle. Sets the triangle points
  @param points Array of the points for triangle's foci
*/
Triangle::Triangle(const Klaus::VecPosition::Array points)
{
  setPoints(points);
}

/**
  Constructor of Triangle. Sets the triangle points
  @param points Array of the points for triangle's foci
*/
Triangle::Triangle(const Klaus::VecPosition points[])
{
  setPoints(points);
}

/**
  This method returnst the requested focus point
  @param num The number of the focus
  @return The position of the focus
*/
VecPosition Triangle::getPoint(const int& num)
{
  if(num == 0)
    return mPoints[0];
  else if(num == 1)
    return mPoints[1];
  else if(num == 2)
    return mPoints[2];
  else
    return VecPosition::Unknown;
}

/**
  This method sets on points of the triangle.
  @param num The number of the focus
  @param p The position of the focus
*/
void Triangle::setPoint(const int& num, const Klaus::VecPosition& p)
{
  if(num == 0)
    mPoints[0] = p;
  else if(num == 1)
    mPoints[1] = p;
  else if(num == 2)
    mPoints[2] = p;
}

/**
  This method sets the points of the triangle.
  @param p1 The first focus
  @param p2 The second focus
  @param p3 The third focus
*/
void Triangle::setPoints(const Klaus::VecPosition& p1, const Klaus::VecPosition& p2, const Klaus::VecPosition& p3)
{
  mPoints[0] = p1;
  mPoints[1] = p2;
  mPoints[2] = p3;
}

/**
  This method sets the points of the triangle.
  @param points The array containing the foci of the triangle
*/
void Triangle::setPoints(const Klaus::VecPosition points[])
{
  mPoints[0] = points[0];
  mPoints[1] = points[1];
  mPoints[2] = points[2];
}

/**
  This method sets the points of the triangle.
  @param points The array containing the foci of the triangle
*/
void Triangle::setPoints(const Klaus::VecPosition::Array points)
{
  if(points.size() < 3)
    return;

  mPoints[1] = points[0];
  mPoints[2] = points[1];
  mPoints[3] = points[2];
}

/**
  This method calculates the angle on the given triangle point
  @param num Index of the triangle point
  @return Angle on the requested point
*/
Degree Triangle::getAngleOnVertex(const int& num) const
{
  if(num == 0)
    return getAngle(mPoints[0], mPoints[1], mPoints[2]);
  else if(num == 1)
    return getAngle(mPoints[1], mPoints[0], mPoints[2]);
  else if(num == 2)
    return getAngle(mPoints[2], mPoints[0], mPoints[1]);
  else
    return UnknownDoubleValue;
}

/**
  This method returns the length of the triangle which is connecting the given two foci
  @param num1 Index of the first focus
  @param num2 Index of the second focus
  @return Distance of these two points
*/
double Triangle::getLength(const int& num1, const int& num2) const
{
  return mPoints[num1].getDistanceTo(mPoints[num2]);
}

/**
  This method calculates the area of the triangle.
  @return Area of the triangle
*/
double Triangle::getArea() const
{
  Line3 l(mPoints[1], mPoints[2]);
  double height = l.getDistanceWithPoint(mPoints[0]);
  double length = getLength(1, 2);

  return height * length / 2.0;
}

/**
  This method calculates the circumference of the triangle.
  @return Circumference of the triangle
*/
double Triangle::getCircumference() const
{
  return getLength(0, 1) + getLength(1, 2) + getLength(0, 2);
}

/**
  This method finds out the angel for the given vertices on a single plane
  @param center The base vertex for the angle calculation
  @param p1 One of the other vertices
  @param p2 The other vertex
  @return The calculated angle of the base vertex
*/
Degree Triangle::getAngle(const Klaus::VecPosition& center, const Klaus::VecPosition& p1, const Klaus::VecPosition& p2)
{
  VecPosition v1 = center - p1;
  VecPosition v2 = p2 - center;

  double dInnerProduct = v1.dot(v2);
  return Degree::acos(v1.getMagnitude() * v2.getMagnitude() / dInnerProduct);
}

/**
  This static method finds out an angle of a triangle created by the given lengths.

          theta
            _
  len1     /_\    len 2
          /   \
         .____.

         len3


  @param len1 First length as shown in the figure
  @param len2 Second length as shown in the figure
  @param len3 Third length as shown in the figure. The angle is in front of this line
  @return Angle of the vertex in front of the given third length
*/
Degree Triangle::getAngle(const double& len1, const double& len2, const double& len3)
{
  // The formula is this:
  // len3^2 = len1^2 + len2^2 - 2.len1.len2.cos(theta)
  double d1 = (len3*len3 - len1*len1 - len2*len2);
  double d2 = (-2.0*len1*len2);
  return Degree::acos(d1 / d2);
}



/*****************************************************************************/
/******************************  Class Ellipse  ******************************/
/*****************************************************************************/

/**
  Constructor of Ellipse. Creates the ellipse with radius value 1.
*/
Ellipse::Ellipse()
{
  mOrientation = new Quaternion;
  mA = mC = 1.0;
}

/**
  Constructor of ellipse using only length and width.
  @param length Twice the horizontal radius
  @param width Twice the vertical radius
*/
Ellipse::Ellipse(const double& length, const double& width)
{
  mOrientation = new Quaternion;
  setEllipse(length, width, VecPosition(0, 0, 0), Quaternion::identity);
}

/**
  Constructor of Ellipse. Creates the ellipse with given length, width, center and orientation
  @param length The length of the ellipse
  @param width The width of the ellipse
  @param c The center of the ellipse
  @param q The orientation of the ellipse
*/
Ellipse::Ellipse(const double& length, const double& width, const Klaus::VecPosition& c, const Klaus::Quaternion & q)
{
  mOrientation = new Quaternion;
  setEllipse(length, width, c, q);
}

/**
  Destructor of ellipse. Removes the orientation
*/
Ellipse::~Ellipse()
{
  delete mOrientation;
}

/**
  This method set the ellipse's parameters
  @param length The length of the ellipse
  @param width The width of the ellipse
  @param c The center of the ellipse
  @param q The orientation of the ellipse
*/
void Ellipse::setEllipse(const double& length, const double& width, const Klaus::VecPosition& c, const Klaus::Quaternion & q)
{
  mCenter = c;
  *mOrientation = q;
  mA = length * 0.5;
  mC = width * 0.5;
}

/**
  This method finds out a point on the ellipse with the given angle
  @param theta angle of the point in radians
  @return The point on the ellipse on the given angle
*/
VecPosition Ellipse::getPointWithAngle(const Degree& theta)
{
  VecPosition result;
  result[0] = theta.cos() * mA + mCenter[0];
  result[1] = theta.sin() * mC + mCenter[1];

  result = *mOrientation * result;

  return result;
}

/**
  This method sets the center of the ellipse
  @param c The new center of the ellipse
*/
void Ellipse::setCenter(const Klaus::VecPosition& c)
{
  mCenter = c;
}

/**
  This method returns the center of the ellipse
  @return The center of the ellipse
*/
VecPosition Ellipse::getCenter() const
{
  return mCenter;
}

/**
  This method sets the orientation of the ellipse
  @param q The new orientation of the ellipse
*/
void Ellipse::setOrientation(const Klaus::Quaternion& q)
{
  *mOrientation = q;
}

/**
  This method returns the orientation of the ellipse
*/
Klaus::Quaternion Ellipse::getOrientation() const
{
  return *mOrientation;
}

/**
  This method sets the horizontal radius of the ellipse
  @param length full length of the ellipse
*/
void Ellipse::setLength(const double& length)
{
  mA = length / 2.0;
}

/**
  This method returns the full length of the ellipse. (twice the size of it's horizontal radius)
  @return Length of the ellipse
*/
double Ellipse::getLength() const
{
  return mA * 2.0;
}

/**
  This method sets the vertical radius of the ellipse
  @param width full width of the ellipse
*/
void Ellipse::setWidth(const double& width)
{
  mC = width / 2.0;
}

/**
  This method returns the full width of the ellipse. (twice the size of it's vertical radius)
  @return Width of the ellipse
*/
double Ellipse::getWidth() const
{
  return mC * 2.0;
}




/*****************************************************************************/
/*****************************  Class Line  **********************************/
/*****************************************************************************/

/** This constructor creates a line by given the three coefficents of the line.
    A line is specified by the formula ay + bx + c = 0.
    @param dA a coefficients of the line
    @param dB b coefficients of the line
    @param dC c coefficients of the line */
Line::Line(double dA, double dB, double dC)
{
  mA = dA;
  mB = dB;
  mC = dC;
}

/**
 * This is the index opertator overloaded for Line class
 * @param iIndex Index of the line coeffecient needed
 * @return Line coefficient
 */
double & Line::operator [] (int iIndex)
{
  static double dError = UnknownDoubleValue;

  switch(iIndex)
  {
    case 0: return mA;
    case 1: return mB;
    case 2: return mC;
    default:
      std::cerr << "(Line::operator []) Index out of bounds: " << iIndex << std::endl;
      return dError;
  }
}

/**
 * This is the index opertator overloaded for Line class
 * @param iIndex Index of the line coeffecient needed
 * @return Line coefficient
 */
double Line::operator [] (int iIndex) const
{
  switch(iIndex)
  {
    case 0: return mA;
    case 1: return mB;
    case 2: return mC;
    default:
      std::cerr << "(Line::operator []) Index out of bounds: " << iIndex << std::endl;
      return UnknownDoubleValue;
  }
}

/** This function prints the line to the specified output stream in the
    format y = ax + b.
    @param os output stream to which output is written
    @param l line that is written to output stream
    @return output sream to which output is appended. */
std::ostream& operator << (std::ostream & os, Line l)
{
  double a = l.getACoefficient();
  double b = l.getBCoefficient();
  double c = l.getCCoefficient();

  // ay + bx + c = 0 -> y = -b/a x - c/a
  if(a == 0)
    os << "x = " << -c/b;
  else
  {
    os << "y = ";
    if(b != 0)
      os << -b/a << "x ";
    if(c > 0)
       os << "- " <<  fabs(c/a);
    else if(c < 0)
       os << "+ " <<  fabs(c/a);
  }

  return os;
}

/**
 * This method returns the line in a human readable form to enable logging
 * @return Line as a std::string
 */
std::string Line::str() const
{
  std::stringstream ss;
  ss << *this;
  return ss.str();
}


/** This method returns the intersection point between the current Line and
    the specified line.
    @param line line with which the intersection should be calculated.
    @return VecPosition position that is the intersection point. */
VecPosition Line::getIntersection(Line line) const
{
  VecPosition pos;
  double x, y;
  if((mA / mB) ==  (line.getACoefficient() / line.getBCoefficient()))
    return pos; // lines are parallel, no intersection
  if(mA == 0)            // bx + c = 0 and a2*y + b2*x + c2 = 0 ==> x = -c/b
  {                          // calculate x using the current line
    x = -mC/mB;                // and calculate the y using the second line
    y = line.getYGivenX(x);
  }
  else if(line.getACoefficient() == 0)
  {                         // ay + bx + c = 0 and b2*x + c2 = 0 ==> x = -c2/b2
   x = -line.getCCoefficient()/line.getBCoefficient(); // calculate x using
   y = getYGivenX(x);       // 2nd line and calculate y using current line
  }
  // ay + bx + c = 0 and a2y + b2*x + c2 = 0
  // y = (-b2/a2)x - c2/a2
  // bx = -a*y - c =>  bx = -a*(-b2/a2)x -a*(-c2/a2) - c ==>
  // ==> a2*bx = a*b2*x + a*c2 - a2*c ==> x = (a*c2 - a2*c)/(a2*b - a*b2)
  // calculate x using the above formula and the y using the current line
  else
  {
    x = (mA*line[2] - line[0]*mC) / (line[0]*mB - mA*line[1]);
    y = getYGivenX(x);
  }

  return VecPosition(x, y);
}


/** This method calculates the intersection points between the current line
    and the circle specified with as center 'posCenter' and radius 'dRadius'.
    The number of solutions are returned and the corresponding points are put
    in the third and fourth argument of the method
    @param circle circle with which intersection points should be found
    @param posSolution1 first intersection (if any)
    @param posSolution2 second intersection (if any) */
int Line::getCircleIntersectionPoints(Circle circle,
              VecPosition *posSolution1, VecPosition *posSolution2) const
{
  int    iSol;
  double dSol1=0, dSol2=0;
  double h = circle.getCenter()[0];
  double k = circle.getCenter()[1];

  // line:   x = -c/b (if a = 0)
  // circle: (x-h)^2 + (y-k)^2 = r^2, with h = center.x and k = center.y
  // fill in:(-c/b-h)^2 + y^2 -2ky + k^2 - r^2 = 0
  //         y^2 -2ky + (-c/b-h)^2 + k^2 - r^2 = 0
  // and determine solutions for y using abc-formula
  if(fabs(mA) < SMALL_EPSILON)
  {
    iSol = Algebra::abcFormula(1, -2*k, ((-mC/mB) - h)*((-mC/mB) - h)
              + k*k - circle.getRadius()*circle.getRadius(), &dSol1, &dSol2);
    posSolution1->setPosition((-mC/mB), dSol1);
    posSolution2->setPosition((-mC/mB), dSol2);
    return iSol;
  }

  // ay + bx + c = 0 => y = -b/a x - c/a, with da = -b/a and db = -c/a
  // circle: (x-h)^2 + (y-k)^2 = r^2, with h = center.x and k = center.y
  // fill in:x^2 -2hx + h^2 + (da*x-db)^2 -2k(da*x-db) + k^2 - r^2 = 0
  //         x^2 -2hx + h^2 + da^2*x^2 + 2da*db*x + db^2 -2k*da*x -2k*db
  //                                                         + k^2 - r^2 = 0
  //       (1+da^2)*x^2 + 2(da*db-h-k*da)*x + h2 + db^2  -2k*db + k^2 - r^2 = 0
  // and determine solutions for x using abc-formula
  // fill in x in original line equation to get y coordinate
  double da = -mB/mA;
  double db = -mC/mA;

  double dA = 1 + da*da;
  double dB = 2*(da*db - h - k*da);
  double dC = h*h + db*db -2*k*db + k*k - circle.getRadius()*circle.getRadius();

  iSol = Algebra::abcFormula(dA, dB, dC, &dSol1, &dSol2);

  posSolution1->setPosition(dSol1, da*dSol1 + db);
  posSolution2->setPosition(dSol2, da*dSol2 + db);
  return iSol;

}

/** This method returns the tangent line to a VecPosition. This is the line
    between the specified position and the closest point on the line to this
    position.
    @param pos VecPosition point with which tangent line is calculated.
    @return Line line tangent to this position */
Line Line::getTangentLine(VecPosition pos) const
{
  // ay + bx + c = 0 -> y = (-b/a)x + (-c/a)
  // tangent: y = (a/b)*x + C1 -> by - ax + C2 = 0 => C2 = ax - by
  // with pos.y = y, pos.x = x
  return Line(mB, -mA, mA*pos[0] - mB*pos[1]);
}

/** This method returns the closest point on a line to a given position.
    @param pos point to which closest point should be determined
    @return VecPosition closest point on line to 'pos'. */
VecPosition Line::getPointOnLineClosestTo(VecPosition pos) const
{
  Line l2 = getTangentLine(pos);  // get tangent line
  return getIntersection(l2);     // and intersection between the two lines
}

/** This method returns the distance between a specified position and the
    closest point on the given line.
    @param pos position to which distance should be calculated
    @return double indicating the distance to the line. */
double Line::getDistanceWithPoint(VecPosition pos) const
{
  return pos.getDistanceTo(getPointOnLineClosestTo(pos));
}

/** This method determines whether the projection of a point on the
    current line lies between two other points ('point1' and 'point2')
    that lie on the same line.

    @param pos point of which projection is checked.
    @param point1 first point on line
    @param point2 second point on line
    @return true when projection of 'pos' lies between 'point1' and 'point2'.*/
bool Line::isInBetween(VecPosition pos, VecPosition point1,VecPosition point2) const
{
  pos          = getPointOnLineClosestTo(pos); // get closest point
  double dDist = point1.getDistanceTo(point2); // get distance between 2 pos

  // if the distance from both points to the projection is smaller than this
  // dist, the pos lies in between.
  return pos.getDistanceTo(point1) <= dDist &&
         pos.getDistanceTo(point2) <= dDist;
}

/** This method calculates the y coordinate given the x coordinate
    @param x coordinate
    @return y coordinate on this line */
double Line::getYGivenX(double x) const
{
 if(mA == 0)
 {
   std::cerr << "(Line::getYGivenX) Cannot calculate Y coordinate\n";
   return 0;
 }
  // ay + bx + c = 0 ==> ay = -(b*x + c)/a
  return -(mB * x + mC) / mA;
}

/** This method calculates the x coordinate given the x coordinate
    @param y coordinate
    @return x coordinate on this line */
double Line::getXGivenY(double y) const
{
 if(mB == 0)
 {
   std::cerr << "(Line::getXGivenY) Cannot calculate X coordinate\n" ;
   return 0;
 }
  // ay + bx + c = 0 ==> bx = -(a*y + c)/a
  return -(mA * y + mC) / mB;
}

/** This method creates a line given two points.
    @param pos1 first point
    @param pos2 second point
    @return line that passes through the two specified points. */
Line Line::makeLineFromTwoPoints(VecPosition pos1, VecPosition pos2)
{
  // 1*y + bx + c = 0 => y = -bx - c
  // with -b the direction coefficient (or slope)
  // and c = - y - bx
  double dA, dB, dC;
  double dTemp = pos2[0] - pos1[0]; // determine the slope
  if(fabs(dTemp) < SMALL_EPSILON)
  {
    // ay + bx + c = 0 with vertical slope=> a = 0, b = 1
    dA = 0.0;
    dB = 1.0;
  }
  else
  {
    // y = (-b)x -c with -b the slope of the line
    dA = 1.0;
    dB = -(pos2[1] - pos1[1])/dTemp;
  }
  // ay + bx + c = 0 ==> c = -a*y - b*x
  dC =  - dA*pos2[1]  - dB * pos2[0];
  return Line(dA, dB, dC);
}

/** This method creates a line given a position and an angle.
    @param vec position through which the line passes
    @param angle direction of the line.
    @return line that goes through position 'vec' with angle 'angle'. */
Line Line::makeLineFromPositionAndAngle(VecPosition vec, Degree angle)
{
  // calculate point somewhat further in direction 'angle' and make
  // line from these two points.
  return makeLineFromTwoPoints(vec, vec + VecPosition(1, angle.getDegree(), 0, POLAR));
}

/** This method returns the a coefficient from the line ay + bx + c = 0.
    @return a coefficient of the line. */
double Line::getACoefficient() const
{
  return mA;
}

/** This method returns the b coefficient from the line ay + bx + c = 0.
    @return b coefficient of the line. */
double Line::getBCoefficient() const
{
 return mB;
}

/** This method returns the c coefficient from the line ay + bx + c = 0.
    @return c coefficient of the line. */
double Line::getCCoefficient() const
{
 return mC;
}




/******************************************************************************/
/********************************  Class Rect  ********************************/
/******************************************************************************/

/** This is the constructor of a Rectangle. Two points will be given. The
    order does not matter as long as two opposite points are given (left
    top and right bottom or right top and left bottom).
    @param pos first point that defines corner of rectangle
    @param pos2 second point that defines other corner of rectangle
    @return rectangle with 'pos' and 'pos2' as opposite corners. */
Rect::Rect(VecPosition pos, VecPosition pos2)
{
  setRectPoints(pos, pos2);
}

/** This is the constructor of a Rectangle. The two corners of the
    rectangle will be set to zero. */
Rect::Rect()
{
}

/** This method sets the upper left and right bottom point of the current
    rectangle.
    @param pos1 first point that defines corner of rectangle
    @param pos2 second point that defines other corner of rectangle */
void Rect::setRectPoints(VecPosition pos1, VecPosition pos2)
{
  mPosLeftTop[0] = Min(pos1[0], pos2[0]);
  mPosLeftTop[1] = Max(pos1[1], pos2[1]);
  mPosRightBottom[0] = Max(pos1[0], pos2[0]);
  mPosRightBottom[1] = Min(pos1[1], pos2[1]);
}

/** This method determines whether the given position lies inside the current
    rectangle.
    @param pos position which is checked whether it lies in rectangle
    @return true when 'pos' lies in the rectangle, false otherwise */
bool Rect::isInside(VecPosition pos) const
{
  return pos.isBetweenX(mPosRightBottom[0], mPosLeftTop[0]) &&
         pos.isBetweenY(mPosLeftTop[1],     mPosRightBottom[1]);
}

/** This method sets the top left position of the rectangle
    @param pos new top left position of the rectangle
    @return true when update was successful */
bool Rect::setPosLeftTop(VecPosition pos)
{
  mPosLeftTop = pos;
  return true;
}

/** This method returns the top left position of the rectangle
    @return top left position of the rectangle */
VecPosition Rect::getPosLeftTop() const
{
  return mPosLeftTop;
}

/** This method sets the right bottom position of the rectangle
    @param pos new right bottom position of the rectangle
    @return true when update was succesfull */
bool Rect::setPosRightBottom(VecPosition pos)
{
  mPosRightBottom = pos;
  return true;
}

/** This method returns the right bottom position of the rectangle
    @return top right bottom of the rectangle */
VecPosition Rect::getPosRightBottom() const
{
  return mPosRightBottom;
}

/** This method sets the length of the rectangle
    @param l new length of the rectangle
    @return true when update was succesfull */
bool Rect::setLength(const double& l)
{
  mPosRightBottom[0] = mPosLeftTop[0] + l;
  return true;
}

/** This method returns the length of the rectangle
    @return length of the rectangle */
double Rect::getLength() const
{
  return fabs(mPosRightBottom[0] - mPosLeftTop[0]);
}

/** This method sets the width of the rectangle
    @param w new width of the rectangle
    @return true when update was succesfull */
bool Rect::setWidth(const double& w)
{
  mPosRightBottom[1] = mPosLeftTop[1] + w;
  return true;
}

/** This method returns the width of the rectangle
    @return width of the rectangle */
double Rect::getWidth() const
{
  return fabs(mPosLeftTop[1] - mPosRightBottom[1]);
}

/** This method returns the std::string representation of the rectangle */
std::string Rect::str()
{
  std::stringstream ss;
  ss << setdecimal(2) << "(Rect P[" << mPosLeftTop[0] << "," << mPosLeftTop[1] <<
                         "] E[" << getLength() << "," << getWidth() << "])";

  return ss.str();
}



/******************************************************************************/
/****************************** Class Plane  **********************************/
/******************************************************************************/

/** The X-Y standard plane */
const Plane Plane::XY(VecPosition(0, 0, 1), VecPosition());

/** The Y-Z standard plane */
const Plane Plane::YZ(VecPosition(1, 0, 0), VecPosition());

/** The X-Z standard plane */
const Plane Plane::XZ(VecPosition(0, 1, 0), VecPosition());

/**
  Constructor of plane with no parameters
*/
Plane::Plane()
{
  mNormal.setPosition(0, 0, 1);
  mDelta = 0.0;
}

/** This is the constructor of a Plane. A normal vector and a position on
    the plane will be given.
    @param norm The normal Vector of plane which plane is based on
    @param pos  The position to which the plane is uniquely created
*/
Plane::Plane(const VecPosition & norm, const VecPosition & pos)
{
  setPlane(norm, pos);
}

/**
 * This method returns the plane in a human readable form
 * @return The plane as a std::string
 */
std::string Plane::str() const
{
  std::stringstream buf;
  buf << "(Plane " << setdecimal(2) << mNormal.str() << " " << mDelta << ")";
  return buf.str();
}

/** This method determines whether the given position lies inside the current
    plane.
    @param pos position which is checked whether it lies in plane
    @return true when 'pos' lies in the plane, false otherwise
*/
bool Plane::isInside(VecPosition pos) const
{
  double delta = (pos[0] * mNormal[0] + pos[1] * mNormal[1] + pos[2] * mNormal[2]);
  return delta == -mDelta;
}

/**
 * This method returns a point on the plane. Needed mainly for plane intersections
 * @return An arbitrary point on plane
 */
VecPosition Plane::getArbitraryPoint() const
{
  return getPointOnPlaneClosestTo(VecPosition());
}

/** This method sets the normal vector and delta of the  current plane.
    @param newNormal The normal vector of the plane
    @param newPoint The point in the rectangle
*/
void Plane::setPlane(VecPosition newNormal, VecPosition newPoint)
{
  setPlaneNormal(newNormal);
  setPlaneDelta(-(newPoint[0]*mNormal[0] + newPoint[1]*mNormal[1] + newPoint[2]*mNormal[2]));
}

/**
  This method sets the normal vector of the plane. It also normalizes the vector
   so that the plane will be a hessian-norm plane. In a hessian-norm plane the
   normal vector length is 1, which makes many calculations faster.
    @param newNormal The new normal vector of the plane
*/
void Plane::setPlaneNormal(VecPosition newNormal)
{
  mNormal = newNormal;
  mNormal.normalize();
}

/** This method returns the normal vector of the plane
    @return normal vector of the plane
*/
VecPosition Plane::getPlaneNormal() const
{
  return mNormal;
}

/**
  This method sets the delta of the plane to origin
  @param newDelta The new delta of the plane to origin
*/
void Plane::setPlaneDelta(double newDelta)
{
  mDelta = newDelta;
}

/** This method returns the delta of plane to origin
    @return delta of the plane to the origin
*/
double Plane::getPlaneDelta() const
{
  return mDelta;
}

/**
 * This method returns the distance between a plane and a given point
 * @param pos Point to get it's distance with the plane
 * @return Distance between the point and the plane
 */
double Plane::getDistanceToPoint(const VecPosition & pos) const
{
  return fabs(pos.dot(mNormal) + mDelta);
}

/**
 * This method returns the point on the plane, closest to the given point.
   The procedure is to first create a line from the given point, and the
   plane's normal vector (which will be a perpendicular line to the plane)
   and get the intersection of the line and plane. This intersection point
   will be the closest point on the plane to the given point.
 * @param pos The point to get nearest point on the plane to
 * @return Nearest point on the plane to the given point
 */
VecPosition Plane::getPointOnPlaneClosestTo(const VecPosition & pos) const
{
  Line3 l;
  l.makeLineFromVectorAndPoint(mNormal, pos);
  VecPosition p;
  l.getIntersectionWithPlane(*this, p);

  return p;
}

/**
 * This method returns a point in respect to the plane.
 * @param pos Point to get its mirrored image on the other side of plane
 * @return The mirrored image of the given point in respect to the plane
 */
VecPosition Plane::getMirroredPoint(const VecPosition & pos) const
{
  VecPosition p = getPointOnPlaneClosestTo(pos);
  VecPosition d = pos - p;
  return pos - d*2;
}

/**
 * This method evaluates the dihedral angle between two planes. This angles
   is the angle that the plane normal vectors constitute with each other.
 * @param p Plane to calculate the angle with
 * @return Dihedral angle of two planes
 */
Degree Plane::getDihedralAngle(const Plane & p) const
{
  return Degree::acos(mNormal.dot(p.getPlaneNormal()));
}

/**
 * If two planes are not parellel (their normal vectors should not be parellel)
   then they will intersect, and their intersection will be a line that this
   method evalutes.
 * @param p Plane to check for intersection line
 * @param l Actual line in the intersection area of the plane
 * @return True if the line is valid, ie. planes are not parellel; false otherwise
 */
bool Plane::getIntersectionWithPlane(const Plane & p, Line3 &l) const
{
  // If planes are parellel, they have no intersection, or they are coincident
  // None has intersection represented in with a line
  if(mNormal.isParallelTo(p.getPlaneNormal()))
    return false;

  // interception line's direction vector
  VecPosition intLineVec = mNormal.cross(p.getPlaneNormal());

  // Derive a line from an arbitrary point on plane to intersect the other plane.
  // The direction of the line should not be parellel to the intersection line of
  // the plane, so that in the intersecting the line with the other plane, would
  // result in a point on the intersection line of the two planes.
  // From this point and the direction vector, can the line of intersection be built

  VecPosition tempPoint = getArbitraryPoint();
  VecPosition tempLineVec = mNormal.cross(intLineVec);
  Line3 tempLine;
  tempLine.makeLineFromVectorAndPoint(tempLineVec, tempPoint);
  VecPosition posIntPlanes;

  // This should not anytime be false
  if(!tempLine.getIntersectionWithPlane(p, posIntPlanes))
  {
    // But if it was
    std::cerr << "(Plane::getIntersectionWithPlane) At last, math and reletivity joined together! Thanks Albert!" << std::endl;
    return false;
  }

  // Now build the line
  l.makeLineFromVectorAndPoint(intLineVec, posIntPlanes);
  return true;
}




/******************************************************************************/
/******************************  Class Line3  *********************************/
/******************************************************************************/

/**
 * Constructor of Line3. Creates a line from two points
 * @param p1 First point
 * @param p2 Second point
 */
Line3::Line3(VecPosition p1, VecPosition p2)
{
  makeLineFromTwoPoints(p1, p2);
}

/**
 * Constructor of Line3. Inits from zeros
 */
Line3::Line3()
{
  mVector = VecPosition(1.0, 0.0, 0.0);
}

/**
  Constructor of Line3, creates a line from a 2D line on an arbitrary plane
  Useful to create 3D lines from typical 2D lines on XY plane
  @param l 2D line in the given plane
*/
Line3::Line3(const Line & l)
{
  makeLineFromLineAndXYPlane(l);
}

/**
 * Constructor of Line3, creates a line from a point in space and two angles
 * @param pos Point that which line goes through
 * @param theta Angle of the line in XY plane
 * @param phi Angle of the line in From Z axis and XY plane
 */
Line3::Line3(VecPosition pos, Degree theta, Degree phi)
{
  makeLineFromPointAndAngle(pos, theta, phi);
}

/**
 * This method creates the line from two points.
   The points should not be identical.
 * @param p1 First point
 * @param p2 Second point
 * @return True if the line could be built from the given twp points, false otherwise
 */
bool Line3::makeLineFromTwoPoints(VecPosition p1, VecPosition p2)
{
  if(p1 == p2)
  {
    std::cerr << "(Line3::makeLineFromTwoPoints) Given points are identical" << std::endl;
    return false;
  }

  mVector = p2 - p1;
  mVector.normalize();
  mPoint = p1;
  return true;
}

/**
 * This method makes a line from a given direction vector and point which
   specifies the line uniquely.
 * @param v The direction vector of the line
 * @param p Point position to specify the line uniquely
 * @return True if the line could be built from the given vector and point, false otherwise
 */
bool Line3::makeLineFromVectorAndPoint (VecPosition v, VecPosition p)
{
  return makeLineFromTwoPoints(p - v, p);
}

/**
 * This method makes the line from 2D line on XY plane
 * @param l The 2D line on the XY plane
 */
void Line3::makeLineFromLineAndXYPlane (const Line & l)
{
  VecPosition p1, p2;

  // calculate two points
  double a = l.getACoefficient();
  double b = l.getBCoefficient();
  double c = l.getCCoefficient();

  if(a == 0)  // line is horizontal
  {
    p1[1] = -c / b;
    p2[1] = -c / b;

    p2[0] = 1.0;
  }
  else if(b == 0) // line is vertical
  {
    p1[0] = -c / a;
    p2[0] = -c / a;

    p2[1] = 1.0;
  }
  else // neighter
  {
    p1[1] = l.getYGivenX(0);
    p2[0] = 1;
    p2[1] = l.getYGivenX(1);
  }

  makeLineFromTwoPoints(p1, p2);
}

/**
 * This method creates a line from a given position and two angles
 * @param pos Point that which line goes through
 * @param theta Angle of the line in XY plane
 * @param phi Angle of the line in From Z axis and XY plane
 */
void Line3::makeLineFromPointAndAngle(VecPosition pos, Degree theta, Degree phi)
{
  mVector.setVector(1, theta, phi);
  mVector.normalize();
  mPoint = pos;
}

/**
 * This method checks whether a given point is on the line or not
 * @param p Point to check whethr is on the line or not
 * @return True if the given point is on line, false otherwise
 */
bool Line3::isPointOnLine(const VecPosition & p) const
{
  return getDistanceWithPoint(p) < SMALL_EPSILON;
}

/**
 * This method returns distance with a specified line.
   The distance means the distance between the nearest
   point on the lines to each other
 * @param l Line to get the distance to
 * @return Distance between the lines in space
 */
double Line3::getDistanceWithLine(const Line3 & l) const
{
  VecPosition a = mVector;
  VecPosition b = l.getVector();
  VecPosition c = l.getPoint() - mPoint;

  // The solution is:
  //
  //   |c.(axb)|
  //   ---------
  //     |axb|

  VecPosition cross = a.cross(b);

  // if the lines are identical or
  // if the lines are parellel, then any arbitrary point will do
  if(cross.getMagnitude() < SMALL_EPSILON)
    return getDistanceWithPoint(l.getPoint());

  // if they are not identical, and they are not parellel,
  // then they are skew, or the lines have intersection

  return fabs(c.dot(cross)) / cross.getMagnitude();
}

/**
 * This method returns the distance D of which the given point has with
   its nearest point on the line
 * @param p A point to check for it's distance with the line
 * @return Distance of the given point p with the nearest member point of the line
 */
double Line3::getDistanceWithPoint(const VecPosition & p) const
{
  if(mVector == 0)
  {
    std::cerr << "(Line3::getDistanceWithLine) Direction vector is zero" << std::endl;
    return UnknownDoubleValue;
  }

  VecPosition p1 = mPoint;
  VecPosition p2 = mPoint + mVector;

  // The simplified formula is:
  //
  //   |(p2-p1) x (p1-p)|
  //   ------------------
  //       (p2 -p1)

  VecPosition temp = p2 - p1;
  return temp.cross(p1 - p).getMagnitude() / temp.getMagnitude();
}

/**
 * This method returns the closest point on the line to the given point p
 * @param p point to get the nearest point on line to it
 * @return Nearest point on line to the given point
 */
VecPosition Line3::getPointOnLineClosestTo(const VecPosition & p) const
{
  // The formula is:
  //
  //     (p-p1).(v)
  // t = ----------
  //       |v|^2
  //
  // Line is normalizes, so the denom is 1. it will not be accounted

  double t = (p - mPoint).dot(mVector);
  return (mVector*t + mPoint);
}

/**
  This method finds out the possible intersection point of two lines in space
  If the lines do not meet, then UnknownPosition will be returned
  @param l Line to check for interception point to
  @param p The position of the intersection will be written on this variable instance
  @return Interception point of the lines if they intercept, UnknownPosition otherwise
*/
bool Line3::getIntersectionWithLine(const Line3 & l, VecPosition & p) const
{
  p = VecPosition::Unknown;

  // If lines are far from each other, eighter they are skew or parallel
  // so they have no common point
  if(getDistanceWithLine(l) > SMALL_EPSILON)
    return false;

  // If lines are coincident, any point will do
  // test if they are coincident
  if(l == *this)
  {
    p = mPoint;
    return true;
  }

  Line3 temp;
  VecPosition p1, p2;
  if(!getLineLineIntersection(l, temp, &p1, &p2))
    return false;

  p = (p1 + p2) / 2;
  return true;
}

/**
 * This method intersects line with the given plane and returns the
   intersection point. The intersection point can be derived if the
   line is converted to parametric form, and be put in an equation
   with the plane representation.
 * @param p The plane to check for intersection point with the line
 * @param pos The position of the intersection will be written here
 * @return Intersection point of the line and the plane
 */
bool Line3::getIntersectionWithPlane(const Plane & p, VecPosition & pos) const
{
  pos = VecPosition::Unknown;

  // norm(plane) = [a', b', c']
  // dir(line) = [a, b, c]
  //      -(a'x0 + b'y0 + c'z0 + d)
  // t = ---------------------------
  //          a'a + b'b + c'c
  // x = at + x0
  // y = bt + y0
  // z = ct + z0

  VecPosition plane_norm = p.getPlaneNormal();

  double m = mVector.dot(p.getPlaneNormal());
  if(m == 0)
    return false; // the line and the plane are parellel

  double d = p.getPlaneNormal().dot(mPoint);
  d += p.getPlaneDelta();

  double t = -d / m;
  pos[0] = mVector[0] * t + mPoint[0];
  pos[1] = mVector[1] * t + mPoint[1];
  pos[2] = mVector[2] * t + mPoint[2];

  return true;
}

/**
 * This method projects the line onto a plane and returns the projected line on
   the plane
 * @param p Plane to project the line onto
 * @param l The projected line will be copied into this parameter
 * @return False of the projection on the plane is point, true therwise
 */
bool Line3::getProjectionOnPlane(const Plane & p, Line3 &l) const
{
  // The project will be a point if the line is perpendicular to the plane, not a line
  if(mVector.isParallelTo(p.getPlaneNormal()))
    return false;

  VecPosition p1 = p.getPointOnPlaneClosestTo(mPoint);
  VecPosition p2 = p.getPointOnPlaneClosestTo(mPoint + mVector);

  l.makeLineFromTwoPoints(p1, p2);
  return true;
}

/**
  This method calculates the line segment PaPb that is the shortest route between
  two lines that pass through P1P2 and P3P4. Calculate also the values of mua and
  mub where
     Pa = P1 + mua (P2 - P1)
     Pb = P3 + mub (P4 - P3)
  Return false if no solution exists.

  @param l Line to get the line intersection of it to the given line
  @param result Line instance to copy the derived line
  @param pa If not null, it will be filled with one of the intersection points of the intersecting line
  @param pb If not null, it will be filled with the other intersection points of the intersecting line
  @return True if any result existed, false otherwise
*/
bool Line3::getLineLineIntersection(const Line3 & l, Line3 & result, VecPosition *pa, VecPosition *pb) const
{
  VecPosition p1 = mPoint, p2 = mPoint + mVector;
  VecPosition p3 = l.getPoint(), p4 = l.getPoint() + l.getVector();

  VecPosition p13,p43,p21;
  double d1343,d4321,d1321,d4343,d2121, numer, denom;

  p13 = p1 - p3;
  p43 = p4 - p3;
  p21 = p2 - p1;

  // A very quicker way to find out p43 is smaller than EPSILON
  // This saves a very cpu consuming sqrt function as in:
  // if(p43.getMagnitude() < EPSILON)
  if(fabs(p43[0]) < SMALL_EPSILON &&
      fabs(p43[1]) < SMALL_EPSILON &&
      fabs(p43[2]) < SMALL_EPSILON)
    return false;

  if(fabs(p21[0]) < SMALL_EPSILON &&
      fabs(p21[1]) < SMALL_EPSILON &&
      fabs(p21[2]) < SMALL_EPSILON)
    return false;

  d1343 = p13[0] * p43[0] + p13[1] * p43[1] + p13[2] * p43[2];
  d4321 = p43[0] * p21[0] + p43[1] * p21[1] + p43[2] * p21[2];
  d1321 = p13[0] * p21[0] + p13[1] * p21[1] + p13[2] * p21[2];
  d4343 = p43[0] * p43[0] + p43[1] * p43[1] + p43[2] * p43[2];
  d2121 = p21[0] * p21[0] + p21[1] * p21[1] + p21[2] * p21[2];

  denom = d2121 * d4343 - d4321 * d4321;
  if(fabs(denom) < SMALL_EPSILON)
    return false;
  numer = d1343 * d4321 - d1321 * d4343;

  double mua = numer / denom;
  double mub = (d1343 + d4321 * mua) / d4343;

  VecPosition pA(
                  p1[0] + mua * p21[0],
                  p1[1] + mua * p21[1],
                  p1[2] + mua * p21[2]
               );

  VecPosition pB(
                  p3[0] + mub * p43[0],
                  p3[1] + mub * p43[1],
                  p3[2] + mub * p43[2]
               );

  if(pa != NULL)
    *pa = pA;

  if(pb != NULL)
    *pb = pB;

  if(pA != pB)
    result.makeLineFromTwoPoints(pA, pB);

  return true;
}

/**
 * This method returns the direction vector of the line
 * @return Direction vector of the line
 */
VecPosition Line3::getVector() const
{
  return mVector;
}

/**
 * This method sets a non zero vector as a direction vector of the line
 * @param v New direction vector of the line
 */
void Line3::setVector(const VecPosition &v)
{
  mVector = v;
  mVector.normalize();
}

/**
 * This method returns A point on the line
 * @return Arbitrary point on line
 */
VecPosition Line3::getPoint() const
{
  return mPoint;
}

/**
 * This method sets an arbitrary point as a member point of line to identify the
   line uniquely
 * @param p new member point of the line
 */
void Line3::setPoint(const VecPosition &p)
{
  mPoint = p;
}

/** Overloaded version of the assignment operator for assigning a given Line3
    instance to current Line3. This changes the current Line3 itself.
    @param l A line3 instance to copy to current line
*/
void Line3::operator = (const Line3 &l)
{
  mVector = l.mVector;
  mPoint = l.mPoint;
}

/** Overloaded version of the inequality operator for comparing a
    Line3 to another Line3.

    @param l a Line3 to compare if it is different to current Line3
    @return true when the lines are differenct, false otherwise
*/
bool Line3::operator !=(const Line3 &l) const
{
  return !(*this==l);
}

/** Overloaded version of the equality operator for Line3. It determines whether
    the current Line3 is equal to the given Line3 by testing themselves
    @param l a Line3
    @return true when the Line3s are actuall representing one line, false otherwise
*/
bool Line3::operator ==(const Line3 &l) const
{
  if(!mVector.isParallelTo(l.mVector))
    return false;
  else
    return getDistanceWithPoint(l.mPoint) < SMALL_EPSILON;
}

/**
 * This method prints the line into a std::string so that it cam be logged and read
   Easily
 * @return String containing the line
 */
std::string Line3::str() const
{
  std::stringstream buf;
  buf << "(Line3 V" << setdecimal(2) << mVector.str() << " P" << mPoint.str() << ")";
  return buf.str();
}


}; // End of namespace Klaus

