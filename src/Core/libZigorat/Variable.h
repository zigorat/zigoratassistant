/*
*********************************************************************************
*            Variable.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 14/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for parsing and using variables in nodes        *
*                                                                               *
*********************************************************************************
*/

/*! \file Variable.h
<pre>
<b>File:</b>          Variable.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       14/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for parsing and using variables in nodes
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
14/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef VARIABLES
#define VARIABLES


#include "Exception.h" // needed for Klaus::Exception
#include "SLang.h"     // needed for Klaus::SToken, std::string, std::vector
#include "Parse.h"     // needed for StringArray

#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  /*! This class encapsulates a generic variable. A generic variable has a name
      and a value which can be accessed during runtime via their own specific
      methods
  */
  class _DLLExportControl Var
  {
    public:
      /*! This enumeration holds possible value types for a variable */
      enum VarType
      {
        VarBool,       /*!< A boolean variable    */
        VarChar,       /*!< A character variable  */
        VarInt,        /*!< An integer variable   */
        VarReal,       /*!< A real value variable */
        VarAngle,      /*!< Angle value           */
        VarString,     /*!< A string variable     */
        VarStringArray,/*!< An array of strings   */
        VarPosition,   /*!< A position variable   */
        VarMatrix4,    /*!< A matrix variable     */
        VarQuaternion, /*!< A quaternion variable */
        VarUnknown     /*!< unknown variable      */
      };

      /*!< Array is a dynamic array of Variables */
      typedef std::vector<Var*> Array;

    private:
      std::string         mName;    /*!< Name of the variable */
      VarType             mType;    /*!< Type of the variable */

    public:
                          Var       ( const std::string & strName,
                                      const VarType & type         );
      virtual           ~ Var       (                              );

      std::string         getName   (                              ) const;
      void                setName   ( const std::string & strName  );
      Var::VarType        getType   (                              ) const;

      /**
        This method parses the value for the variable from a given string.
        Throws exception if anything goes wrong.
        @param strValue The string contianing the variable value
      */
      virtual void        setValue  ( const std::string & strValue ) = 0;

      /**
        This method returns the value of the variable as a string.
        @return The value of the variable as a string
      */
      virtual std::string getValue  (                              ) const = 0;
  };


  /*! CVar is a const reference of a variable */
  typedef const Var * CVar;


}; // end namespace Klaus

#endif // VARIABLES
