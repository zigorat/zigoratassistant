/*
*********************************************************************************
*            Variable.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 14/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for parsing and using variables in nodes        *
*                                                                               *
*********************************************************************************
*/

/*! \file Variable.cpp
<pre>
<b>File:</b>          Variable.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       14/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for parsing and using variables in nodes
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
14/02/2011       Mahdi           Initial version created
</pre>
*/

#include "Variable.h"

/**
  Constructor of Var, creates a name and type for the variable
  @param strName Name of the variable
  @param type Type of the variable
*/
Klaus::Var::Var(const std::string & strName, const VarType& type)
{
  mName = strName;
  mType = type;
}

/**
  Destructor of the variable. Does nothing
*/
Klaus::Var::~Var()
{

}

/**
  This method returns the name of the method
  @return The name of the variable
*/
std::string Klaus::Var::getName() const
{
  return mName;
}

/**
  This method sets the name of the variable.
  @param strName Name of the variable
*/
void Klaus::Var::setName(const std::string & strName)
{
  mName = strName;
}

/**
  This method returns the type of the variable
  @return Type of the variable
*/
Klaus::Var::VarType Klaus::Var::getType() const
{
  return mType;
}
