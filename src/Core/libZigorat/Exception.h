/*
*********************************************************************************
*            Exception.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 14/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides a base for exception handling.                            *
*                                                                               *
*********************************************************************************
*/

/*! \file Exception.h
<pre>
<b>File:</b>          Exception.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       14/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides a base for exception handling.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
14/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef KLAUS_EXCEPTION
#define KLAUS_EXCEPTION

#include "PlatformMacros.h" // needed for _DLLExportControl
#include <stdio.h>          // needed for vsnprintf
#include <stdarg.h>         // needed for va_list


namespace Klaus
{


  /*! This class is the base for handling exceptions in Klaus framework */
  class _DLLExportControl Exception
  {
    protected:
      #define      cErrorSize 2048     /*!< Error output buffer size */
      char         mError[cErrorSize]; /*!< Error buffer */

    public:
      /**
        Constructor of the exception. Creates it from a given set of parameters
        @param str The String to use as format provider
        @param ... The parameters
      */
      Exception( const char * str, ... )
      {
        va_list ap;
        va_start(ap, str);
        if(vsnprintf(mError, cErrorSize-1, str, ap) == -1)
          fprintf(stderr, "(Klaus::Exception) buffer is too small!\n");
        va_end(ap);
      }

      /**
        Constructor of the exception with no parameters
      */
      Exception()
      {
        mError[0] = 0;
      }

      /**
        Destructor of the exception. Does nothing
      */
      virtual ~ Exception()
      {
      }

      /**
        This method return the string representation of error
      */
      const char * str () const
      {
        return mError;
      }
  };


  /*! This class is used to handle exceptions within this S-Language parser. Stores an
      error message and shows it.
  */
  class _DLLExportControl SLangException : public Klaus::Exception
  {
    public:
      /**
        Constructor for SLangException. Creates from a set of parameters
        @param strError The error format string
      */
      SLangException( const char * strError, ... )
      {
        va_list ap;
        va_start(ap, strError);
        if(vsnprintf(mError, cErrorSize-1, strError, ap) == -1)
          fprintf(stderr, "(Klaus::SLangException) buffer is too small!\n");
        va_end(ap);
      }
  };



  /*! This class is used to throw exceptions in the scene graph
      framework and distinguish the exceptions from other parts
      of the application.
  */
  class _DLLExportControl SceneGraphException : public Exception
  {
    public:
      /**
        Constructor of the exception. Creates from a given c style string.
        @param strError The error string
      */
      SceneGraphException(const char * strError)
      {
        sprintf(mError, "%s", strError);
      }
  };


  /**
    This class is used to throw exceptions from variable list.
  */
  class _DLLExportControl VarException : public Exception
  {
    public:
      /**
        Constructor of VarException. Creates an exception for variable lists.
        @param strError The error message to be thrown
      */
      VarException( const char * strError, ... )
      {
        va_list ap;
        va_start(ap, strError);
        if(vsnprintf(mError, cErrorSize-1, strError, ap) == -1)
          fprintf(stderr, "(Klaus::VarException) buffer is too small!\n");
        va_end(ap);
      }
  };


};


#endif // KLAUS_EXCEPTION
