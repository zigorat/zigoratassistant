/*
*********************************************************************************
*            VarList.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 17/04/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for grouping variables and using them           *
*                                                                               *
*********************************************************************************
*/

/*! \file VarList.cpp
<pre>
<b>File:</b>          VarList.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       17/04/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for grouping variables and using them
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
17/04/2011       Mahdi           Initial version created
</pre>
*/

#include "VarList.h"
#include <sstream>  // needed for std::stringstream
#include "Parse.h"  // needed for Parse::parseFirstInt()


/**
  This metod removes all variables from the list.
*/
void Klaus::VarList::clear()
{
  mVars.clear();
}

/**
  This method returns number of the variables currently in the list
  @return Number of variables currently in the list
*/
size_t Klaus::VarList::size() const
{
  return mVars.size();
}

/**
  This method fills the given parameter with the names of all variables in the list
  @param names Names of all variables will be filled in this parameter
*/
void Klaus::VarList::getAllNames(StringArray& names) const
{
  for(Variables::const_iterator it = mVars.begin(); it != mVars.end(); ++it)
    names.push_back(it->first);
}

/**
  This method adds a variable to the list.
  @param strName Name of the variable
  @param value The numerical value for the variable
*/
void Klaus::VarList::addVar(const std::string& strName, const double& value)
{
  std::stringstream ss;
  ss << value;
  mVars[strName] = ss.str();
}

/**
  This method adds a variable to the list
  @param strName Name of the variable
  @param value String value of the variable
*/
void Klaus::VarList::addVar(const std::string& strName, const std::string& value)
{
  mVars[strName] = value;
}

/**
  This method adds a position variable to the list
  @param strName Name of the variable
  @param val Positional information of the point
*/
void Klaus::VarList::addVar(const std::string& strName, const Klaus::VecPosition& val)
{
  std::stringstream ss;
  ss << val[0] << ' ' << val[1] << ' ' << val[2];
  mVars[strName] = ss.str();
}

/**
  This method finds out the given variable and returns its value as a number.
  @note if the variable is not found or it can not be evaluated as a number an exception will be thrown
  @param strName Name of the variable
  @return Floating point numerical value of the variable
*/
double Klaus::VarList::getAsNumber(const std::string& strName) const
{
  std::string s = getAsString(strName);
  int index = 0;
  double result;

  if(Parse::parseFirstReal(s, result, index))
    return result;
  else
    throw VarException("(VarList) variable '%s' does not have a number: '%s'", strName.c_str(), s.c_str());
}

/**
  This method finds out the given variable and returns its value as an integer
  @note if the variable is not found or it can not be avaluated as an iteger an exception will be thrown
  @param strName Name of the variable
  @return Integer value of the requested variable
*/
int Klaus::VarList::getAsInt(const std::string& strName) const
{
  std::string s = getAsString(strName);
  int index = 0;
  int result;

  if(Parse::parseFirstInt(s, result, index))
    return result;
  else
    throw VarException("(VarList) variable '%s' does not have an integer: '%s'", strName.c_str(), s.c_str());
}

/**
  This method finds out the given variable and returns its value as a string.
  @note If the variable is not found an exception will be thrown
  @param strName Name of the variable
  @return Value of the variable as a string
*/
std::string Klaus::VarList::getAsString(const std::string& strName) const
{
  Variables::const_iterator it = mVars.find(strName);
  if(it == mVars.end())
    throw VarException("(VarList) variable '%s' not found", strName.c_str());

  return it->second;
}

/**
  This method finds out the given variable name and returns its value as vector/point
  @param strName Name of the variable
  @return Value of the variable as a VecPosition
*/
Klaus::VecPosition Klaus::VarList::getAsPoint(const std::string& strName) const
{
  std::string s = getAsString(strName);
  VecPosition result;
  int index = 0;

  if(!Parse::parseFirstReal(s, result[0], index))
    throw VarException("(VarList) no x coordinate for '%s'", strName.c_str());
  if(!Parse::parseFirstReal(s, result[1], index))
    throw VarException("(VarList) no y coordinate for '%s'", strName.c_str());
  if(!Parse::parseFirstReal(s, result[2], index))
    throw VarException("(VarList) no z coordinate for '%s'", strName.c_str());

  return result;
}

/**
  Overloaded index operator to get numerical value of a variable
  @param strName Name of the variable
  @return Value of the requested variable
*/
double Klaus::VarList::operator[](const std::string& strName) const
{
  return getAsNumber(strName);
}
