/*
*********************************************************************************
*              Angles.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 04/24/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for using angles with different units.          *
*                                                                               *
*********************************************************************************
*/

/*! \file Angles.h
<pre>
<b>File:</b>          Angles.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       04/24/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for using angles with different units.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
04/24/2011       Mahdi           Initial version created
</pre>
*/

#ifndef ANGLES
#define ANGLES

#include "PlatformMacros.h" // needed for _DLLExportControl
#include <iostream>         // needed for std::ostream
#include <cmath>        // needed for M_PI constant

namespace Klaus
{

  class _DLLExportControl Radian;


  /*! Degree holds angle values in degree units. It is created to automatically
     handle conversions from radian angle units.
  */
  class _DLLExportControl Degree
  {
    private:
      double        mAngle;     /*!< The angle in degrees */

    public:
                    Degree      (                    );
                    Degree      ( const double & ang );
                    Degree      ( const Radian & ang );

      Degree        operator -  (                    ) const;
      Degree        operator +  ( const Degree & ang ) const;
      Degree        operator -  ( const Degree & ang ) const;
      Degree        operator *  ( const Degree & ang ) const;
      Degree        operator /  ( const Degree & ang ) const;
      Degree      & operator += ( const Degree & ang );
      Degree      & operator -= ( const Degree & ang );
      Degree      & operator *= ( const Degree & ang );
      Degree      & operator /= ( const Degree & ang );
      Degree      & operator =  ( const Degree & ang );

      bool          operator == ( const Degree & ang ) const;
      bool          operator != ( const Degree & ang ) const;
      bool          operator >  ( const Degree & ang ) const;
      bool          operator >= ( const Degree & ang ) const;
      bool          operator <  ( const Degree & ang ) const;
      bool          operator <= ( const Degree & ang ) const;

      double        getDegree   (                    ) const;
      double        getRadian   (                    ) const;
      double        abs         (                    ) const;

      void          restrict    ( const Degree & min,
                                  const Degree & max );

      Degree      & normalize180(                    );
      Degree      & normalize360(                    );

      double        cos         (                    ) const;
      double        sin         (                    ) const;
      double        tan         (                    ) const;
      double        cot         (                    ) const;

      static Degree acos        ( const double & x   );
      static Degree asin        ( const double & x   );
      static Degree atan        ( const double & x   );
      static Degree atan2       ( const double & x,
                                  const double & y   );

      static double toRadian    ( const double & ang );

      /**
        Overloaded stream operator to write the angle into a standard output stream
        @param os The output stream to write the angle into
        @param v The Degree instance to be written in the output stream
        @return The stream instance
      */
      friend std::ostream & operator << ( std::ostream &os,
                                          const Degree & v );

      /**
        A value of angles representing unknown or illegal angle values
      */
      static Degree Unknown;
  };


  /*! Radian holds angle values in radian units. It is created to automatically
     handle conversions from degree angle units.
  */
  class _DLLExportControl Radian
  {
    private:
      double        mAngle;     /*!< The angle in degrees */

    public:
                    Radian      (                    );
                    Radian      ( const double & ang );
                    Radian      ( const Degree & ang );

      Radian        operator -  (                    );
      Radian        operator +  ( const Radian & ang );
      Radian        operator -  ( const Radian & ang );
      Radian        operator *  ( const Radian & ang );
      Radian        operator /  ( const Radian & ang );
      Radian      & operator += ( const Radian & ang );
      Radian      & operator -= ( const Radian & ang );
      Radian      & operator *= ( const Radian & ang );
      Radian      & operator /= ( const Radian & ang );
      Radian      & operator =  ( const Radian & ang );

      bool          operator == ( const Radian & ang );
      bool          operator != ( const Radian & ang );
      bool          operator >  ( const Radian & ang );
      bool          operator >= ( const Radian & ang );
      bool          operator <  ( const Radian & ang );
      bool          operator <= ( const Radian & ang );

      double        getDegree   (                    ) const;
      double        getRadian   (                    ) const;
      double        abs         (                    ) const;

      double        cos         (                    ) const;
      double        sin         (                    ) const;
      double        tan         (                    ) const;
      double        cot         (                    ) const;

      Radian      & normalizePi (                    );
      Radian      & normalize2Pi(                    );

      static Radian acos        ( const double & x   );
      static Radian asin        ( const double & x   );
      static Radian atan        ( const double & x   );
      static Radian atan2       ( const double & x,
                                  const double & y   );

      static double toDegree    ( const double & ang );

      /**
        Overloaded stream operator to write the angle into a standard output stream
        @param os The output stream to write the angle into
        @param v The Radian instance to be written in the output stream
        @return The stream instance
      */
      friend std::ostream & operator << ( std::ostream &os,
                                          const Radian & v );

      /**
        A value of angles representing unknown or illegal angle values
      */
      static Radian Unknown;
  };

};

#endif // ANGLES
