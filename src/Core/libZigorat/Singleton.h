/*
*********************************************************************************
*            Singleton.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 25/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Singleton class template. For creating classes which can have only *
*            one instance in application.                                       *
*                                                                               *
*********************************************************************************
*/

/*! \file Singleton.h
<pre>
<b>File:</b>          Singleton.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       25/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Singleton class template. For creating classes which can have only one instance in application.
                This source file is based on OgrePrerequisites.h file which is part of OGRE
               (Object-oriented Graphics Rendering Engine)
               For the latest info, see http://www.ogre3d.org/
               License included in source file.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
25/06/2010       Mahdi           Initial version created
</pre>
*/

/*
License exactly as described in the OgreRequisities.h:

-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2006 Torus Knot Software Ltd
Also see acknowledgements in Readme.html

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.

You may alternatively use this source under the terms of a specific version of
the OGRE Unrestricted License provided you have obtained such a license from
Torus Knot Software Ltd.
-----------------------------------------------------------------------------
*/
/* Original version Copyright (C) Scott Bilas, 2000.
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Scott Bilas, 2000"
 */

#ifndef SINGLETONS
#define SINGLETONS


#include "PlatformMacros.h"
#include <assert.h>

/*! Turn off warnings generated by this singleton implementation */
# if COMPILER == COMPILER_MSVC
#   pragma warning (disable : 4311)
#   pragma warning (disable : 4312)
# endif


namespace Klaus
{


  # if COMPILER == COMPILER_GNUC
  #   pragma GCC visibility push(default)
  # endif

  /**
    Singleton template. This is used to manage singleton classes instance in both
    windows and linux in a way that ony one instance of a class exists in the
    application and plugins during application runtime.
  */
  template <typename T> class _DLLExportControl Singleton
  {
    protected:
      static T* mInstance;

      Singleton()
      {
        assert(!mInstance);

        #if defined( _MSC_VER ) && _MSC_VER < 1200
          int offset = (int)(T*)1 - (int)(Singleton <T>*)(T*)1;
          mInstance = (T*)((int)this + offset);
        #else
	        mInstance = static_cast<T*>(this);
        #endif
      }

    ~ Singleton()
      {
        assert(mInstance);
        mInstance = 0;
      }

    public:
      static T* getSingleton()
		  {
        assert(false);
        return 0;
      }

      static void freeSingleton()
      {
        assert(false);
      }
  };

  # if COMPILER == COMPILER_GNUC
  #   pragma GCC visibility pop
  # endif


}; // end namespace Klaus


#endif // HEADER GAURD SINGLETONS
