/*
*********************************************************************************
*            CoreSystem.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: The core of the system                                             *
*                                                                               *
*********************************************************************************
*/

/*! \file CoreSystem.h
<pre>
<b>File:</b>          CoreSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      The core of the system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#include "CoreSystem.h"
#include <libZigorat/ApplicationSettings.h>  // needed for ApplicationSettings
#include <OverlaySystem/OverlaySystem.h>     // needed for OverlaySystem
#include <GUISystem/GUISystem.h>             // needed for GUISystem
#include <PluginSystem/PluginSystem.h>       // needed for PluginSystem
#include <SceneSystem/SceneSystem.h>         // needed for SceneSystem
#include <DisplaySystem/DisplaySystem.h>     // needed for DisplaySystem
#include <GraphicSystem/LightSystem.h>       // needed for LightSystem
#include <ConfigSystem/ConfigSystem.h>       // needed for ConfigSystem


/** CoreSystem singleton instance */
template<> Klaus::Core::CoreSystem* Klaus::Singleton<Klaus::Core::CoreSystem>::mInstance = 0;

/**
  Constructor of CoreSystem.
*/
Klaus::Core::CoreSystem::CoreSystem()
{
  // Setup logging
  mLog = Logger::getSingleton();
  mLogOutput.open((ApplicationSettings::getSingleton()->getDataPath() + "app.log").c_str());
  if(mLogOutput)
    mLog->setOutputStream(&mLogOutput);
  else
  {
    std::cout << "(CoreSystem) could not open log file " <<
    ApplicationSettings::getSingleton()->getDataPath() + "app.log" << std::endl;
  }
  mInitialized = false;

  setupConfig("CoreSystem");
}

/**
  Destructor of CoreSystem
*/
Klaus::Core::CoreSystem::~CoreSystem()
{
  finalize();
  Input::KeyBindings::freeSingleton();
  Events::EventSystem::freeSingleton();
  Config::ConfigSystem::freeSingleton();
  Logger::freeSingleton();

  // flush standard output streams
  std::cout.flush();
  std::cerr.flush();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Core::CoreSystem * Klaus::Core::CoreSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new CoreSystem;
    Logger::getSingleton()->log(1, "(CoreSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Core::CoreSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(CoreSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(CoreSystem) removed singleton");
  }
}

/**
  This method loads all default configuration for GUISystem
*/
void Klaus::Core::CoreSystem::_loadDefaultConfig()
{
  mLog->clearLogLevels();
  mLog->addLogLevel(9);
}

/**
  This method reads GUI system's configuration from a stream. If configurations were read
  successfully then this system will reinitialize itself with the new values.
  @param type The configuration type of the steam
  @return True if all configurations were successfully read, false otherwise
*/
bool Klaus::Core::CoreSystem::_readConfig(const Config::ConfigType & type)
{
  try
  {
    mLog->clearLogLevels();

    if(type == Config::BinaryConfig)
    {
      unsigned s = readUnsigned();
      for(size_t i = 0; i < s; ++i)
        mLog->addLogLevel(readInt());
    }
    else if(type == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      SToken t = expr["addLogLevels"];
      for(size_t i = 1; i < t.size(); ++i)
        mLog->addLogLevel(t[i].getAsInt());
    }
    else
    {
      mLog->log(5, "(CoreSystem) read: configuration type %d not supported", type);
      return false;
    }
  }
  catch(const char * strError)
  {
    mLog->log(5, "(CoreSystem) exception: %s", strError);
    return false;
  }

  // shall not initialized the system
  // this shall be done after all other systems have been initialized
  return true;
}

/**
  This method writes the systems configuration to the given output stream
  @param type The configuration type to save the stream
*/
void Klaus::Core::CoreSystem::_writeConfig(const Config::ConfigType & type)
{
  if(type == Config::BinaryConfig)
  {
    writeUnsigned(0);
  }
  else if(type == Config::SExpConfig)
  {
    writeText("(addLogLevels 9)");
  }
  else
    mLog->log(5, "(CoreSystem) write: configuration type %d not supported", type);
}

void Klaus::Core::CoreSystem::updateForNextFrame()
{
  // First acquire all events
  Input::InputSystem::getSingleton()->processInput();

  // Fire input events
  Input::KeyBindings::getSingleton()->fireEvents();

  // Update cameras
  Graphics::DisplaySystem::getSingleton()->updateCameras();

  // update the scene
  Scene::SceneSystem::getSingleton()->updateScene();

  // Finally signal all the systems to update themselves
  Overlay::OverlaySystem::getSingleton()->updateOverlays();
}

/**
  This method checks whether the core is initialized or not
  @return True if the application core is initialized, false otherwise
*/
bool Klaus::Core::CoreSystem::isInitialized() const
{
  return mInitialized;
}

/**
  This method initializes the core system. Each subsystem is initialized in
  order and application starts.
  @return True if application started successfully, false otherwise
*/
bool Klaus::Core::CoreSystem::initialize()
{
  Randomize();
  using namespace std;

  // initialize all systems
  try
  {
    // Load all default configurations from the application configuration file
    ApplicationSettings * settings = ApplicationSettings::getSingleton();
    Config::ConfigSystem::getSingleton()->loadConfig(settings->getDataPath() + "app.cfg", Config::SExpConfig);

    // scan out all available plugins
    PluginSystem::getSingleton()->rescanPlugins();

    // setup graphic system
    if(!Graphics::GraphicSystem::getSingleton()->initializeEngine())
    {
      mLog->log(10, "(CoreSystem) could not initialize GraphicSystem");
      return false;
    }

    // setup diplay system
    Graphics::DisplaySystem::getSingleton()->setupDisplays();

    // setup input system
    if(!Input::InputSystem::getSingleton()->setupInput())
    {
      mLog->log(10, "(CoreSystem) could not initialize InputSystem");
      return false;
    }

    // setup GUI system
    if(!GUI::GUISystem::getSingleton()->setupUI())
    {
      mLog->log(10, "(CoreSystem) could not initialize GUISystem");
      return false;
    }

    // setup overlay system
    Overlay::OverlaySystem::getSingleton()->setupOverlays();

    // setup scene system
    Scene::SceneSystem::getSingleton()->setupScene();

    // initialization finished
    mLog->log(10, "(CoreSystem) initialization finished");
    mInitialized = true;
  }
  catch(Ogre::Exception ogre_e)
  {
    cerr << "(CoreSystem caught ogre exception) " << ogre_e.getFullDescription() << endl;
    return false;
  }
  catch(VarException var_e)
  {
    cerr << "(CoreSystem caught VarException) " << var_e.str() << endl;
    return false;
  }
  catch(SLangException slang_e)
  {
    cerr << "(CoreSystem caught SLangException) " << slang_e.str() << endl;
    return false;
  }
  catch(char * c_str)
  {
    cerr << "(CoreSystem caught exception) " << c_str << endl;
    return false;
  }
  catch(std::string str)
  {
    cerr << "(CoreSystem caught exception) " << str << endl;
    return false;
  }
  catch(std::ios::failure e)
  {
    cout << "(CoreSystem caught exception) " << e.what() << endl;
    return false;
  }
  catch(...)
  {
    cerr << "(CoreSystem) unknown exception thrown" << endl;
    return false;
  }

  return true;
}

/**
  This method starts the game engine by running the graphic engine.
*/
void Klaus::Core::CoreSystem::run()
{
  Graphics::GraphicSystem::getSingleton()->startEngine();
 }

/**
  This method is called to clear system resources and remove singleton
  instances before stopping execution.
*/
void Klaus::Core::CoreSystem::finalize()
{
  // save all current configurations
  ApplicationSettings * settings = ApplicationSettings::getSingleton();
  Config::ConfigSystem::getSingleton()->saveConfig(settings->getDataPath() + "app.cfg.current", Config::SExpConfig);

  // now free all systems
  Overlay::OverlaySystem::freeSingleton();
  Graphics::LightingSystem::freeSingleton();
  Input::InputSystem::freeSingleton();
  GUI::GUISystem::freeSingleton();
  Graphics::DisplaySystem::freeSingleton();
  Scene::SceneSystem::freeSingleton();
  Graphics::GraphicSystem::freeSingleton();
  PluginSystem::freeSingleton();

  mLog->log(10, "(CoreSystem) finalization finished");
  mInitialized = false;
}
