/*
*********************************************************************************
*              Types.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 11/11/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains decleration of all types used in this project             *
*                                                                               *
*********************************************************************************
*/

/*! \file Types.h
<pre>
<b>File:</b>          Types.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains decleration of all types used in this project
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2008       Mahdi           Initial version created
</pre>
*/


#ifndef ZIGORAT_ASSISTANT_TYPES
#define ZIGORAT_ASSISTANT_TYPES

#include <vector>  // needed for std::vector
#include <map>     // needed for std::map
#include <string>  // needed for std::string


namespace Klaus
{

  /*! EventID is a type definition for event numbers */
  typedef int EventID;

  /*! EventIDList is a dynamic array of event ids */
  typedef std::vector<int> EventIDList;


  namespace GUI
  {

    /*! This enumeration is to identify interface element types */
    enum UIType
    {
      Window,      /*!< A Form                                  */
      Button,      /*!< A typical button                        */
      TextBox,     /*!< A text box to enter strings             */
      ComboBox,    /*!< A combo box to be able to select values */
      Label,       /*!< A simple label to show texts on forms   */
      MenuBar,     /*!< A menubar to on top of form             */
      MenuItem,    /*!< A menu item which attaches to menus     */
      Memo,        /*!< A multiline textbox                     */
      Unknown      /*!< Unknown Component                       */
    };


    /*! This enumeration holds all different types of trigger types of
        events based on UI elements */
    enum EventTriggerType
    {
      OnMouseDown,         /*!< On mouse key pushed down */
      OnMouseClick,        /*!< On mouse key clicked */
      OnMouseMove,         /*!< On mouse key moved on the element */
      OnMouseUp,           /*!< On mouse key got back up off the element */
      OnKeyDown,           /*!< On keyboard key down */
      OnKeyPress,          /*!< On keyboard key press */
      OnKeyUp,             /*!< On keyboard key got back up */
      OnGainFocus,         /*!< On the time the element got input focus */
      OnLostFocus,         /*!< On the time the element lost its input focus */
      UnknownEventTrigger  /*!< Unknown event type */
    };


    /*! This mapping assigns an event id string to a event trigger type of an UI element. */
    typedef std::map<EventTriggerType, std::string> UIEvents;


    /*! This enumeration is used to hold horizontal alignment of GUI controls */
    enum HorizontalAlignment
    {
      HAlignLeft,     /*!< Left alignment   */
      HAlignCenter,   /*!< Center alignment */
      HAlignRight,    /*!< Right alignment  */
    };


    /*! This enumeration is used to hold vertical alignment info of GUI widgets */
    enum VerticalAlignment
    {
      VAlignTop,      /*!< Top alignment    */
      VAlignCenter,   /*!< Center alignment */
      VAlignBottom,   /*!< Bottom alignment */
    };

  }; // end namespace GUI;

}; // end namespace Klaus;


#endif // ZIGORAT_ASSISTANT_TYPES
