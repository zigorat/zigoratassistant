/*
*********************************************************************************
*            CoreSystem.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 06/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: The core of the system                                             *
*                                                                               *
*********************************************************************************
*/

/*! \file CoreSystem.h
<pre>
<b>File:</b>          CoreSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       06/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      The core of the system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
06/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CORE_SYSTEM
#define CORE_SYSTEM

#include <libZigorat/Logger.h>         // needed for logger
#include <ConfigSystem/Configurable.h> // needed for configurable

namespace Klaus
{

  namespace Core
  {

    /**
      Core of the application. Starts and initializes all subsystems and manages runtime updates.
      Not to be edited specifically. It starts all other subsystem which in turn load their own
      plugins. These plugins implement the behavior of application. Core just synchronizes the
      subsystems.
    */
    class _DLLExportControl CoreSystem : public Singleton<CoreSystem>, public Config::Configurable
    {
      private:
        Logger            * mLog;                /*!< Logger instance           */
        std::ofstream       mLogOutput;          /*!< Logger output stream      */
        bool                mInitialized;        /*!< Has core initialized?     */


                            CoreSystem           (                              );
                          ~ CoreSystem           (                              );

        // configuration related methods
        void                _loadDefaultConfig   (                              );
        bool                _readConfig          ( const Config::ConfigType & t );
        void                _writeConfig         ( const Config::ConfigType & t );

      public:
        static CoreSystem*  getSingleton         (                              );
        static void         freeSingleton        (                              );

        void                updateForNextFrame   (                              );
        bool                isInitialized        (                              ) const;

        bool                initialize           (                              );
        void                run                  (                              );
        void                finalize             (                              );
    };

  }; // end namespace Core

}; // end namespace Klaus

#endif // CORE_SYSTEM
