/*
*********************************************************************************
*          OverlaySystem.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/13/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Overlay system interface creates and maintains all overlays        *
*                                                                               *
*********************************************************************************
*/

/*! \file OverlaySystem.h
<pre>
<b>File:</b>          OverlaySystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/13/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Overlay system interface creates and maintains all overlays
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef OVERLAY_SYSTEM
#define OVERLAY_SYSTEM


#include "OverlayPlugin.h"
#include <EventSystem/EventSystem.h>   // needed for EventRecipient
#include <ConfigSystem/Configurable.h> // needed for Configurable
#include <libZigorat/Parse.h>          // needed for StringArray


namespace Klaus
{

  namespace Overlay
  {

  /*! Overlay system handles input from system peripherals via input plugins. */
  class _DLLExportControl OverlaySystem: public Singleton<OverlaySystem>,
                                         public Events::EventRecipient,
                                         public Config::Configurable
  {
    private:
      /*! PluginData is an internal storage to handle all plugins in the scene */
      struct PluginData
      {
        OverlayPlugin * mPlugin;  /*!< Scene plugin instance */
        PluginFactory * mFactory; /*!< Plugin's factory instance */
        bool            mEnabled; /*!< Whether plugin is enabled or not */

        typedef std::vector<PluginData> Array;
      };

      // Events
      EventID                EV_TOGGLE_OVERLAYS;/*!< Overlay togge event id   */

      PluginData::Array      mPlugins;          /*!< Overlay plugin instances */
      StringArray            mPluginsToLoad;    /*!< Plugins to load          */
      bool                   mShowOverlays;     /*!< Show or hide overlays    */
      Logger               * mLog;              /*!< Logger instance          */

      void                   clearPlugins       (                             );

      // configuration related
      virtual void           _loadDefaultConfig (                             );
      virtual bool           _readConfig        (const Config::ConfigType & t );
      virtual void           _writeConfig       (const Config::ConfigType & t );

                             OverlaySystem      (                             );
                           ~ OverlaySystem      (                             );

    public:
      static OverlaySystem * getSingleton       (                             );
      static void            freeSingleton      (                             );

      void                   setupOverlays      (                             );
      void                   updateOverlays     (                             );
      void                   setVisibility      ( bool bShow                  );

      // performance boost
      void                   deactivatePlugin   ( const char * strPlugin      );
      void                   activatePlugin     ( const char * strPlugin      );

      // event related
      virtual void           processEvent       ( const Events::Event & ev    );
    };

  }; // end namespace Overlay

}; // end namespace Klaus


#endif // OVERLAY_SYSTEM
