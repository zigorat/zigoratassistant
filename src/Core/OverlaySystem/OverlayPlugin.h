/*
*********************************************************************************
*          OverlayPlugin.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/11/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base of all plugins for Overlay system                             *
*                                                                               *
*********************************************************************************
*/

/*! \file OverlayPlugin.h
<pre>
<b>File:</b>          OverlayPlugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base of all plugins for Overlay system
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef OVERLAY_PLUGIN_BASE
#define OVERLAY_PLUGIN_BASE

#include <PluginSystem/PluginBase.h>
#include <OgreOverlayManager.h>


namespace Klaus
{

  namespace Overlay
  {

    /*! Class OverlayPlugin is a base class for creating overlays.
        All overlays should implement the abstract functions defined
        here.*/
    class _DLLExport OverlayPlugin : public Plugin
    {
      protected:
        /*! OverlayList is a dynamic array of ogre overlay instances */
        typedef std::vector<Ogre::Overlay *> OverlayList;

        OverlayList  mOverlays;        /*!< Overlay instances   */


        /**
         * This method removes and destroys all created overlays stored in the plugin.
         */
        void removeAllOverlays()
        {
          for(size_t i = 0; i < mOverlays.size(); i++)
            Ogre::OverlayManager::getSingleton().destroy(mOverlays[i]);

          mOverlays.clear();
        }

      public:

        /**
         * Destructor of OverlayPlugin. Cleans up on exit.
         */
        virtual ~ OverlayPlugin()
        {
          removeAllOverlays();
        }

        /**
         * This method initializes the overlay. The overlay plugin shall add resources of
           the overlay to application and update it's instance of ogre overlay.
         * @return True on successfull initialization, false otherwise
         */
        virtual bool initialize        (               ) = 0;

        /**
         * This method finalizes the overlay. The overlay plugin shall remove resources of
           the overlay from ogre and remove the ogre overlay instance from scene manager.
         */
        virtual void finalize          (               ) = 0;

        /**
         * This method is used to update the overlay before each frame render starts
         */
        virtual void update            (               ) = 0;

        /**
         * This method toggles visibility of the overlay
         * @param bVisible Whether to show the overlay or hide it
         */
        virtual void setVisibility     ( bool bVisible ) = 0;

        /*! List is a dynamic array of overlay plugins */
        typedef std::vector<OverlayPlugin*> List;
    };


  }; // end namespace Overlay


}; // end namespace Klaus


#endif // OVERLAY_PLUGIN_BASE
