/*
*********************************************************************************
*          OverlaySystem.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/13/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Overlay system interface creates and maintains all overlays        *
*                                                                               *
*********************************************************************************
*/

/*! \file OverlaySystem.cpp
<pre>
<b>File:</b>          OverlaySystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/13/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Overlay system interface creates and maintains all overlays
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
13/11/2009       Mahdi           Initial version created
</pre>
*/

#include "OverlaySystem.h"
#include <PluginSystem/PluginSystem.h>       // needed for PluginSystem
#include <cstring>
#include <sstream>


/** OverlaySystem singleton instance */
template<> Klaus::Overlay::OverlaySystem* Klaus::Singleton<Klaus::Overlay::OverlaySystem>::mInstance = 0;


/**
  Constructor of OverlaySystem. Initializes KeyBindings instance and
  sets input plugin to uninitialized.
*/
Klaus::Overlay::OverlaySystem::OverlaySystem()
{
  mLog = Logger::getSingleton();

  setupConfig("OverlaySystem");
  setRecipientName("OverlaySystem");

  // register events
  EV_TOGGLE_OVERLAYS = registerEvent("OverlaySystem\\ToggleOverlays");

  // register bindings
  using namespace Input;
  registerBinding(KeyCombination(OnKeyPress, KC_F1), EV_TOGGLE_OVERLAYS);
}

/**
  Destructor of OverlaySystem
*/
Klaus::Overlay::OverlaySystem::~OverlaySystem()
{
  clearPlugins();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Overlay::OverlaySystem * Klaus::Overlay::OverlaySystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new OverlaySystem;
    Logger::getSingleton()->log(1, "(OverlaySystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Overlay::OverlaySystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(OverlaySystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(OverlaySystem) removed singleton");
  }
}

/**
  This method clears up all the loaded plugins
*/
void Klaus::Overlay::OverlaySystem::clearPlugins()
{
  for(size_t i = 0; i < mPlugins.size(); i++)
  {
    mPlugins[i].mPlugin->finalize();
    mPlugins[i].mFactory->freePluginInstance(mPlugins[i].mPlugin);
    mLog->log(6, "(OverlaySystem) unloaded plugin %s", mPlugins[i].mFactory->getName());
  }
  mPlugins.clear();
}

/**
  This method sets up default congfiguration for this system
*/
void Klaus::Overlay::OverlaySystem::_loadDefaultConfig()
{
  mPluginsToLoad.clear();
  mPluginsToLoad.push_back("OgreStats");
  mShowOverlays = true;
}

/**
  This method loads plugin names to be run from a binary stream. It will then intialize all plugins.
  @param t The configuration type of the steam
  @return True if all configurations were successfully read, false otherwise
*/
bool Klaus::Overlay::OverlaySystem::_readConfig(const Config::ConfigType & t)
{
  try
  {
    mPluginsToLoad.clear();

    if(t == Config::BinaryConfig)
    {
      unsigned count = readUnsigned();
      std::string str;
      for(unsigned i = 0; i < count; i++)
      {
        str = readString();
        mPluginsToLoad.push_back(str);
      }
      mShowOverlays = readBool();
    }
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      SToken t = expr["loadPlugins"];
      for(size_t i = 1; i < t.size(); ++i)
        mPluginsToLoad.push_back(t[i]);

      mShowOverlays = expr["showOverlays"][1].boolean();
    }
    else
    {
      mLog->log(6, "(OverlaySystem) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    mLog->log(6, "(OverlaySystem) exception: %s", str);
    return false;
  }

  // will not start any overlay
  // this shall be done after every other system has initialized
  return true;
}

/**
  This method writes the configuration of overlay system to the given output stream.
  @param t The configuration type to save the stream
*/
void Klaus::Overlay::OverlaySystem::_writeConfig(const Config::ConfigType & t)
{
  if(t == Config::BinaryConfig)
  {
    writeUnsigned(mPluginsToLoad.size());
    for(unsigned i = 0; i < mPluginsToLoad.size(); ++i)
      writeString(mPluginsToLoad[i]);
    writeBool(mShowOverlays);
  }
  else if(t == Config::SExpConfig)
  {
    std::stringstream ss;
    for(size_t i = 0; i < mPluginsToLoad.size(); ++i)
      ss << " " << mPluginsToLoad[i];
    writeText("(loadPlugins%s)", ss.str().c_str());

    if(mShowOverlays)
      writeText("(showOverlays true)");
    else
      writeText("(showOverlays false)");
  }
  else
    mLog->log(6, "(OverlaySystem) write: configuration type %d not supported", t);
}

/**
  This method initializes overlay plugins. All the plugins in the list of auto
  creation will be loaded and attached to application
*/
void Klaus::Overlay::OverlaySystem::setupOverlays()
{
  clearPlugins();

  for(size_t i = 0; i < mPluginsToLoad.size(); i++)
  {
    PluginFactory * factory = PluginSystem::getSingleton()->findPlugin(mPluginsToLoad[i], "OverlayPlugin");
    if(!factory)
      continue;

    Plugin* raw_plugin = factory->createPluginInstance();
    if(!raw_plugin)
      continue;

    OverlayPlugin* plugin = dynamic_cast<OverlayPlugin*>(raw_plugin);
    if(!plugin)
    {
      factory->freePluginInstance(raw_plugin);
      continue;
    }

    if(!plugin->initialize())
    {
      factory->freePluginInstance(plugin);
      continue;
    }

    // add the plugin
    PluginData data;
    data.mEnabled = true;
    data.mFactory = factory;
    data.mPlugin = plugin;
    mPlugins.push_back(data);
    mLog->log(6, "(OverlaySystem) loaded plugin %s", mPluginsToLoad[i].c_str());
  }

  // intialize state: hide or show
  setVisibility(mShowOverlays);
}

/**
  This method updates all overlay instances between frame renderings
*/
void Klaus::Overlay::OverlaySystem::updateOverlays()
{
  if(mShowOverlays)
    for(size_t i = 0; i < mPlugins.size(); i++)
      if(mPlugins[i].mEnabled)
        mPlugins[i].mPlugin->update();
}

/**
  This method toggles visibility of all overlays
  @param bShow whether to show the overlays or hide them all.
*/
void Klaus::Overlay::OverlaySystem::setVisibility( bool bShow )
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    mPlugins[i].mPlugin->setVisibility(bShow);

  mShowOverlays = bShow;
}

/**
  This method deactivates the plugin. When a plugin is disabled it will
  not be called to update between frames.
  @param strPlugin The plugin to deactivate and remove from auto updated plugins
*/
void Klaus::Overlay::OverlaySystem::deactivatePlugin(const char * strPlugin)
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    if(strcmp(mPlugins[i].mFactory->getName(), strPlugin) == 0)
    {
      mPlugins[i].mEnabled = false;
      mLog->log(6, "(OverlaySystem) disabled plugin '%s'", strPlugin);
      return;
    }
}

/**
  This method activates a disabled plugin. When activated, the plugin will
  be called to update withing frames. The whole activation and deactivation
  are usually decided with events.
  @param strPlugin The plugin to activate
*/
void Klaus::Overlay::OverlaySystem::activatePlugin(const char * strPlugin)
{
  for(size_t i = 0; i < mPlugins.size(); i++)
    if(strcmp(mPlugins[i].mFactory->getName(), strPlugin) == 0)
    {
      mPlugins[i].mEnabled = true;
      mLog->log(6, "(OverlaySystem) enabled plugin '%s'", strPlugin);
      return;
    }
}

/**
  This method processes events sent to this system.
  @param ev The event to process
*/
void Klaus::Overlay::OverlaySystem::processEvent(const Events::Event& ev)
{
  if(ev == EV_TOGGLE_OVERLAYS)
    setVisibility(!mShowOverlays);
  else
    mLog->log(6, "(OverlaySystem) unknown event %s",
                 Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}
