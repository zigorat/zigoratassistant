/*
*********************************************************************************
*           InputPlugin.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 11/11/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base of all plugins for Input system                               *
*                                                                               *
*********************************************************************************
*/

/*! \file InputPlugin.h
<pre>
<b>File:</b>          InputPlugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base of all plugins for Input system
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef PLUGIN_SYSTEM_INPUT_PLUGIN
#define PLUGIN_SYSTEM_INPUT_PLUGIN

#include <PluginSystem/PluginBase.h>


namespace Klaus
{


  namespace Input
  {

    /*! This class is a specialized class for creating input objects
       (like mouse, keyboard and joysticks) */
    class _DLLExport InputPlugin: public Plugin
    {
      public:
        /**
         * This method shall initialize the input plugin. It passes the created
            window handle and it's size information to the plugin and the plugin
            shall initialize based on these
         * @return True on successfull initialization of input devices, false otherwise
         */
        virtual bool initialize         (                 ) = 0;

        /**
         * This method is the main contact point of input system and the input
           plugin. It will be called between rendered frames and the plugin
           shall capture and store any percepted events on the given array
         */
        virtual void checkForInputEvents(                 ) = 0;
    };


  }; // end namespace Input


}; // end namespace Klaus

#endif // PLUGIN_SYSTEM_INPUT_PLUGIN
