/*
*********************************************************************************
*           KeyBindings.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 11/17/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Key bindings translator and handler                                *
*                                                                               *
*********************************************************************************
*/

/*! \file KeyBindings.cpp
<pre>
<b>File:</b>          KeyBindings.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/17/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Key bindings translator and handler
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/17/2008       Mahdi           Initial version created
</pre>
*/

#include "KeyBindings.h"
#include <EventSystem/EventSystem.h> // needed for getEventName


/** KeyBindings singleton instance */
template<> Klaus::Input::KeyBindings* Klaus::Singleton<Klaus::Input::KeyBindings>::mInstance = 0;


/**
  Constructor of KeyBindings. Sets initial binding id
*/
Klaus::Input::KeyBindings::KeyBindings()
{
  mMaxBindings = 1;
  mLog = Logger::getSingleton();
  mCache = BindingCache::getSingleton();
}

/**
  Destructor of KeyBindings.
*/
Klaus::Input::KeyBindings::~KeyBindings()
{
  BindingCache::freeSingleton();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Input::KeyBindings * Klaus::Input::KeyBindings::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new KeyBindings;
    Logger::getSingleton()->log(1, "(KeyBindings) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Input::KeyBindings::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(KeyBindings) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(KeyBindings) removed singleton");
  }
}

/**
  This method searches for a given binding id and returns it's bind information.
  @param bind_id The id to retrieve it's information
  @return Binding information of the bind id if found, NULL if not found
*/
Klaus::Input::KeyBindings::BindingInfo* Klaus::Input::KeyBindings::getBindingInfo(const BindingID & bind_id)
{
  BindingInfo::Mapping::iterator it = mBindings.find(bind_id);
  if(it == mBindings.end())
    return NULL;
  else
    return it->second;
}

/**
  This method searches for a given key combination in all of the registered
  bindings and returns the binding ID which triggers with the key
  combination.
  @param keys The key combination to search for in all bindings
  @return BindingID of the key combination, zero if not found
*/
Klaus::BindingID Klaus::Input::KeyBindings::getBindingID(const Klaus::Input::KeyCombination& keys)
{
  for(BindingInfo::Mapping::iterator it = mBindings.begin(); it != mBindings.end(); it++)
    if(it->second->mKeys == keys)
      return it->first;

  return 0;
}

/**
  This method injects a key down event to KeyBindings. Adds all
  combination which the pushed down key has to the list of
  probable key combinations.
  @param kc The code of the key which was pushed down
*/
void Klaus::Input::KeyBindings::injectKeyDown(const Klaus::Input::KeyCodeT& kc)
{
  // add all of it's actions to probable actions' list
  for(size_t i = 0; i < mKeyCodesCache[kc].size(); i++)
    mProbableActions.push_back(mKeyCodesCache[kc][i]);

  // NOTE: no need to add the key. This is done in updateKeysState
  // NOTE: filtering is done in the end of cycle
  // NOTE: called by CoreSystem
}

/**
  This method injects a key up event to KeyBindings. If the key which
  was released was a part of an active combination whose trigger type
  is when pushed up, the action will be triggered. Triggered or not,
  the active action will be removed.
  @param kc The code of the key which was released
*/
void Klaus::Input::KeyBindings::injectKeyUp(const Klaus::Input::KeyCodeT& kc)
{
  // remove all active actions which contain the key code
  for(int i = 0; i < (int)mActiveActions.size(); i++)
    if(mActiveActions[i]->mKeys.hasKey(kc))
    {
      // check if the action's trigger type is OnKeyUp
      // if this is the case then fire the action
      if(mActiveActions[i]->mKeys.getTriggerType() == OnKeyUp)
        fireEvent(mActiveActions[i]);

      // remove the action
      mActiveActions.erase(mActiveActions.begin() + i);
      i--;
    }

  // NOTE: no need to remove the key from down keys and cached down keys
  // NOTE: automatically done in updateKeysState
  // NOTE: do not remove it's binding infos from probable bindings. Done in filter
}

/**
  The method removes a binding from a key code's internal cache.
  @param key The key code whom wants to remove a binding from
  @param inf The binding info to be remove from the key's cache
*/
void Klaus::Input::KeyBindings::removeBindingFromKeyCache(const Klaus::Input::KeyCodeT& key, const Klaus::Input::KeyBindings::BindingInfo* inf)
{
  for(size_t i = 0; i < mKeyCodesCache[key].size(); i++)
    if(mKeyCodesCache[key][i] == inf)
    {
      mKeyCodesCache[key].erase(mKeyCodesCache[key].begin() + i);
      return;
    }
}

/**
  This method filters all probable actions for active actions. Extracts the
  definite actions. Bigger key combinations are checked first.
*/
void Klaus::Input::KeyBindings::filterProbableActions()
{
  KeyCombination keys = mDownKeys;

  // sort probable actions based on their size
  for(size_t i = 0; i < mProbableActions.size(); i++)
    for(size_t j = i + 1; j < mProbableActions.size(); j++)
      if(mProbableActions[i]->mKeys.size() < mProbableActions[j]->mKeys.size())
      {
        BindingInfo * temp = mProbableActions[i];
        mProbableActions[i] = mProbableActions[j];
        mProbableActions[j] = temp;
      }

  // shall be int. not size_t. consider that the first action is going to be removed. what happens next?
  for(int i = 0; i < (int)mProbableActions.size(); i++)
  {
    if(mProbableActions[i]->mKeys.isSubSetOf(keys))
    {
      // The action can be built from down keys. Strip the key's
      // cache from it's keys. add the action to definitive actions
      for(size_t j = 0; j < mProbableActions[i]->mKeys.size(); j++)
        keys.removeKey(mProbableActions[i]->mKeys[j]);

      // create the action
      mProbableActions[i]->mFired = false;
      mActiveActions.push_back(mProbableActions[i]);
    }
  }

  // clear out the probable actions.
  mProbableActions.clear();
}

/**
  Fires an event. Broadcasts all of the registered events of the combination
  @param binding The binding to fire
*/
void Klaus::Input::KeyBindings::fireEvent(Klaus::Input::KeyBindings::BindingInfo* binding)
{
  // send events directly to recipients
  for(size_t i = 0; i < binding->mRegisteredEvents.size(); i++)
    binding->mRegisteredSystems[i]->processEvent(binding->mRegisteredEvents[i]);
}

/**
  This method registers a binding.
  @param sys The system to register this for. Events will be
             sent directly to the system. Also useful for
             saving and loading. Shall not be empty or NULL
  @param keys The key combination to register the binding to
  @param event_id The event ID to register the binding to
  @param bUseTheNewKeys Whether or not to use the keys supplied
                        to this method or the last set keys for
                        the action of the system
  @return The binding ID
*/
Klaus::BindingID Klaus::Input::KeyBindings::registerBinding( Events::EventRecipient* sys,
                                        const Klaus::Input::KeyCombination& keys,
                                        const EventID & event_id,
                                        bool bUseTheNewKeys)
{
  if(!sys)
    return 0;

  // get the event name
  std::string str_event = Events::EventSystem::getSingleton()->getEventName(event_id);

  // get the last used key combination for the system and event
  KeyCombination the_keys;
  if(bUseTheNewKeys)
    the_keys = keys;
  else if(!mCache->getBinding(sys->getRecipientName(), str_event, the_keys))
    the_keys = keys; // revert to given keys if not any last used keys was found

  BindingID bind_id = getBindingID(the_keys);
  BindingInfo * inf;

  // set all parameters if the mapping did not exist
  if(bind_id == 0)
  {
    inf = new BindingInfo;
    inf->mKeys = the_keys;
    bind_id = mMaxBindings++;
    mBindings[bind_id] = inf;

    // since here the key combination is being added to all
    // the combinations, add it's pointer to all of it's key
    // code's caches.

    for(size_t i = 0; i < the_keys.size(); i++)
      mKeyCodesCache[the_keys[i]].push_back(getBindingInfo(bind_id));
  }
  else // otherwise add the system and event to the binding info
  {
    inf = getBindingInfo(bind_id);

    // first search if the event id already existed in the binding
    // thus avoiding duplicates
    for(size_t i = 0; i < inf->mRegisteredEvents.size(); i++)
      if(inf->mRegisteredEvents[i] == event_id)
        return bind_id; // nothing to do, the event is already on the list
  }

  inf->mRegisteredEvents.push_back(event_id);
  inf->mRegisteredSystems.push_back(sys);

  // add to cache
  mCache->setBinding(sys->getRecipientName(), str_event, the_keys);

  // log it
  mLog->log(16, "(KeyBindings) added event '%s' to system '%s'", str_event.c_str(), sys->getRecipientName());

  return bind_id;
}

/**
  This method removes an event from a binding. If the binding did
  not have any clients after removing the event, the binding will
  be deleted.
  @param bind_id The binding ID of the binding
  @param sys The system which wants to unregister the binding
  @return True if the binding was found and had the event, false otherwise
*/
bool Klaus::Input::KeyBindings::unregisterBinding(const BindingID& bind_id, Events::EventRecipient * sys)
{
  BindingInfo * inf = getBindingInfo(bind_id);
  if(!inf)
    return false;

  for(size_t i = 0; i < inf->mRegisteredSystems.size(); i++)
    if(inf->mRegisteredSystems[i] == sys)
    {
      mLog->log(16, "(KeyBindings) removed event '%s' from system '%s'",
                Events::EventSystem::getSingleton()->getEventName(inf->mRegisteredEvents[i]).c_str(),
                sys->getRecipientName());

      inf->mRegisteredEvents.erase(inf->mRegisteredEvents.begin() + i);
      inf->mRegisteredSystems.erase(inf->mRegisteredSystems.begin() + i);

      // remove the mapping if it had no other recipient
      if(inf->mRegisteredSystems.empty())
      {
        // get it's iterator
        BindingInfo::Mapping::iterator it = mBindings.find(bind_id);

        // remove the binding info from all of it's composing keys actions
        for(size_t j = 0; j < it->second->mKeys.size(); j++)
          removeBindingFromKeyCache(it->second->mKeys[j], it->second);

        // delete it
        delete inf;
        mBindings.erase(it);
      }

      return true;
    }

  return false;
}

/**
  This method changes a binding's key combination and trigger type
  @param bind_id The ID of binding created when previously registering the binding
  @param sys The recipient system which wants to change its binding
  @param keys The new keys to assign to the binding
  @return True if the previous binding was found, false otherwise
*/
bool Klaus::Input::KeyBindings::changeBinding(const BindingID & bind_id,
                                Events::EventRecipient * sys,
                                const Klaus::Input::KeyCombination& keys)
{
  // acquire binding instance
  BindingInfo * inf = getBindingInfo(bind_id);
  if(!inf)
    return false;

  // get the event id
  EventID event_id = 0;

  for(size_t i = 0; i < inf->mRegisteredEvents.size(); i++)
    if(inf->mRegisteredSystems[i] == sys)
    {
      event_id = inf->mRegisteredEvents[i];
      break;
    }

  if(event_id == 0)
    return false;

  // now remove the binding
  unregisterBinding(bind_id, sys);

  // and register it again
  registerBinding(sys, keys, event_id);

  return true;
}

/**
  This method sets the currently down keys and extracts current fired events.
  @param dkeys Keys are which currently down
*/
void Klaus::Input::KeyBindings::updateKeysState( const KeyCombination & dkeys )
{
  // find out all released keys
  for(size_t i = 0; i < mDownKeys.size(); i++)
    if(!dkeys.hasKey(mDownKeys[i]))
      injectKeyUp(mDownKeys[i]);

  // find out all pushed keys
  for(size_t i = 0; i < dkeys.size(); i++)
    if(!mDownKeys.hasKey(dkeys[i]))
      injectKeyDown(dkeys[i]);

  mDownKeys = dkeys;
}

/**
  This method returns whether a given key currently is held down or not
  @param kc The key to check for if it's down
  @return True if the specified key currently is down, false otherwise
*/
bool Klaus::Input::KeyBindings::isKeyDown( const KeyCodeT & kc )
{
  return mDownKeys.hasKey(kc);
}

/**
  This method is reponsible to extract events from captured input peripherals
  and fire them. This is called within CoreSystem::updateForNextFrame.
*/
void Klaus::Input::KeyBindings::fireEvents()
{
  // do not waste any time if no keys was pushed down
  if(mDownKeys.empty())
    return;

  // extract actual actions from probable actions
  filterProbableActions();

  // send all the OnKeyDown and OnKeyPress events
  for(size_t i = 0; i < mActiveActions.size(); i++)
    if(mActiveActions[i]->mKeys.getTriggerType() == OnKeyUp)
      continue;
    else if(mActiveActions[i]->mKeys.getTriggerType() == OnKeyDown)
      fireEvent(mActiveActions[i]);
    else if(!mActiveActions[i]->mFired)
    {
      fireEvent(mActiveActions[i]);
      mActiveActions[i]->mFired = true;
    }
}
