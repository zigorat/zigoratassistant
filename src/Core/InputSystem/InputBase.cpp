/*
*********************************************************************************
*           InputBase.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 11/17/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Key Utilities                                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file InputBase.cpp
<pre>
<b>File:</b>          InputBase.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/17/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Key utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/17/2008       Mahdi           Initial version created
</pre>
*/

#include "InputBase.h"
#include <libZigorat/Logger.h>  // needed for Logger


/** SingleKey singleton instance */
template<> Klaus::Input::SingleKey * Klaus::Singleton<Klaus::Input::SingleKey>::mInstance = 0;


/**
  This method normalizes modifier keys by merging a left and right modifier
  key to a generic modifier. Like if given left shift, this will normalise
  it to "shift"
  @param key Key to normalize
*/
inline void normalizeKey(Klaus::Input::KeyCodeT & key)
{
  using namespace Klaus::Input;

  if(key == KC_LMENU || key == KC_RMENU)
    key = KC_MENU;
  else if(key == KC_LSHIFT || key == KC_RSHIFT)
    key = KC_SHIFT;
  else if(key == KC_LCONTROL || key == KC_RCONTROL)
    key = KC_CONTROL;
  else if(key == KC_LWIN || key == KC_RWIN)
    key = KC_WIN;
}


/**
  This method trims a given string by reducing all the spaces
  back and front of the string
  @param str String to trim
  @return Trimmed string
*/
inline std::string trim( const std::string & str )
{
  std::string result = str;

  std::string::iterator it = result.begin();
  while(result.size() != 0 && isspace(result[0]))
    result.erase(0, 1);

  size_t size = result.size();
  while(size != 0 && isspace(result[size - 1]))
  {
    result.erase(size - 1, 1);
    size = result.size();
  }

  return result;
}




/********************************************************************************/
/*******************************  Class SingleKey  ******************************/
/********************************************************************************/

/**
  Constructor of SingleKey. Initializes mappings.
*/
Klaus::Input::SingleKey::SingleKey()
{
  initializeMapping();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Input::SingleKey * Klaus::Input::SingleKey::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new SingleKey;
    Logger::getSingleton()->log(1, "(SingleKey) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Input::SingleKey::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(SingleKey) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(SingleKey) removed singleton");
  }
}

/**
  This method extracts a keyboard key code in a string and returns it's enumeration
  @param str String containing the key code string
  @return Key code in the string
*/
Klaus::Input::KeyCodeT Klaus::Input::SingleKey::fromString(const std::string& str)
{
  for( KeyCodeMapping::iterator it = mMappings.begin(); it != mMappings.end(); it++ )
    if( it->second == str )
      return it->first;

  return KC_UNASSIGNED;
}

/**
  This method converts an KeyCodeT enumeration into a human readable string
  @param code Key to convert to string
  @return String containing readable key code
*/
std::string Klaus::Input::SingleKey::toString(const KeyCodeT& code)
{
  KeyCodeMapping::const_iterator it = mMappings.find(code);
  if(it != mMappings.end())
    return it->second;
  else
    return "(Key ?)";
}

/**
  This method initializes mapping between key codes and strings
*/
void Klaus::Input::SingleKey::initializeMapping()
{
  mMappings.clear();

  mMappings[KC_ESCAPE]       = "Escape";
  mMappings[KC_1]            = "1";
  mMappings[KC_2]            = "2";
  mMappings[KC_3]            = "3";
  mMappings[KC_4]            = "4";
  mMappings[KC_5]            = "5";
  mMappings[KC_6]            = "6";
  mMappings[KC_7]            = "7";
  mMappings[KC_8]            = "8";
  mMappings[KC_9]            = "9";
  mMappings[KC_0]            = "0";
  mMappings[KC_MINUS]        = "-";
  mMappings[KC_EQUALS]       = "=";
  mMappings[KC_BACK]         = "BackSpace";
  mMappings[KC_TAB]          = "Tab";
  mMappings[KC_Q]            = "Q";
  mMappings[KC_W]            = "W";
  mMappings[KC_E]            = "E";
  mMappings[KC_R]            = "R";
  mMappings[KC_T]            = "T";
  mMappings[KC_Y]            = "Y";
  mMappings[KC_U]            = "U";
  mMappings[KC_I]            = "I";
  mMappings[KC_O]            = "O";
  mMappings[KC_P]            = "P";
  mMappings[KC_LBRACKET]     = "[";
  mMappings[KC_RBRACKET]     = "]";
  mMappings[KC_RETURN]       = "Enter";
  mMappings[KC_LCONTROL]     = "LeftControl";
  mMappings[KC_A]            = "A";
  mMappings[KC_S]            = "S";
  mMappings[KC_D]            = "D";
  mMappings[KC_F]            = "F";
  mMappings[KC_G]            = "G";
  mMappings[KC_H]            = "H";
  mMappings[KC_J]            = "J";
  mMappings[KC_K]            = "K";
  mMappings[KC_L]            = "L";
  mMappings[KC_SEMICOLON]    = ";";
  mMappings[KC_APOSTROPHE]   = "'";
  mMappings[KC_LSHIFT]       = "LeftShift";
  mMappings[KC_BACKSLASH]    = "\\";
  mMappings[KC_Z]            = "Z";
  mMappings[KC_X]            = "X";
  mMappings[KC_C]            = "C";
  mMappings[KC_V]            = "V";
  mMappings[KC_B]            = "B";
  mMappings[KC_N]            = "N";
  mMappings[KC_M]            = "M";
  mMappings[KC_COMMA]        = ",";
  mMappings[KC_PERIOD]       = ".";
  mMappings[KC_SLASH]        = "/";
  mMappings[KC_RSHIFT]       = "RightShift";
  mMappings[KC_MULTIPLY]     = "*";
  mMappings[KC_LMENU]        = "LeftAlt";
  mMappings[KC_RMENU]        = "RightAlt";
  mMappings[KC_SPACE]        = "Space";
  mMappings[KC_CAPITAL]      = "CapsLock";
  mMappings[KC_F1]           = "F1";
  mMappings[KC_F2]           = "F2";
  mMappings[KC_F3]           = "F3";
  mMappings[KC_F4]           = "F4";
  mMappings[KC_F5]           = "F5";
  mMappings[KC_F6]           = "F6";
  mMappings[KC_F7]           = "F7";
  mMappings[KC_F8]           = "F8";
  mMappings[KC_F9]           = "F9";
  mMappings[KC_F10]          = "F10";
  mMappings[KC_NUMLOCK]      = "NumLock";
  mMappings[KC_SCROLL]       = "ScrollLock";
  mMappings[KC_ADD]          = "Plus";
  mMappings[KC_F11]          = "F11";
  mMappings[KC_F12]          = "F12";
  mMappings[KC_F13]          = "F13";
  mMappings[KC_F14]          = "F14";
  mMappings[KC_F15]          = "F15";
  mMappings[KC_PREVTRACK]    = "PrevTrack";
  mMappings[KC_AT]           = "@";
  mMappings[KC_COLON]        = ":";
  mMappings[KC_UNDERLINE]    = "_";
  mMappings[KC_NEXTTRACK]    = "NextTrack";
  mMappings[KC_RCONTROL]     = "RightControl";
  mMappings[KC_MUTE]         = "Mute";
  mMappings[KC_PLAYPAUSE]    = "PlayPause";
  mMappings[KC_MEDIASTOP]    = "Stop";
  mMappings[KC_VOLUMEDOWN]   = "VolumeDown";
  mMappings[KC_VOLUMEUP]     = "VolumeUp";
  mMappings[KC_NUMPADCOMMA]  = ",";
  mMappings[KC_DIVIDE]       = "/";
  mMappings[KC_SYSRQ]        = "PrintScreen";
  mMappings[KC_PAUSE]        = "PAUSE";
  mMappings[KC_HOME]         = "Home";
  mMappings[KC_UP]           = "Up";
  mMappings[KC_PGUP]         = "PageUp";
  mMappings[KC_LEFT]         = "Left";
  mMappings[KC_RIGHT]        = "Right";
  mMappings[KC_END]          = "End";
  mMappings[KC_DOWN]         = "Down";
  mMappings[KC_PGDOWN]       = "PageDown";
  mMappings[KC_INSERT]       = "Insert";
  mMappings[KC_DELETE]       = "Delete";
  mMappings[KC_LWIN]         = "LeftWin";
  mMappings[KC_RWIN]         = "RightWin";
  mMappings[KC_APPS]         = "Apps";
  mMappings[KC_POWER]        = "Power";
  mMappings[KC_SLEEP]        = "Sleep";
  mMappings[KC_WAKE]         = "Wake";

  mMappings[KC_MOUSE_LEFT]   = "MouseLeft";
  mMappings[KC_MOUSE_RIGHT]  = "MouseRight";
  mMappings[KC_MOUSE_MIDDLE] = "MouseMiddle";
  mMappings[KC_MOUSE_MOVE]   = "MouseMove";

  mMappings[KC_MENU]         = "Alt";
  mMappings[KC_WIN]          = "Win";
  mMappings[KC_CONTROL]      = "Control";
  mMappings[KC_SHIFT]        = "Shift";
}



/********************************************************************************/
/****************************  Class KeyCombination  ****************************/
/********************************************************************************/

/**
  Copy constructor of KeyCombination. Will copy the given KeyCombination to itself
  @param keys The key combination to construct from
*/
Klaus::Input::KeyCombination::KeyCombination(const Klaus::Input::KeyCombination& keys)
{
  *this = keys;
}

/**
  Constructor of KeyCombination. Will create the KeyCombination from the given key codes
  @param trig Trigger type of the key combination. On keys up, keys down or only when first pressed
  @param key1 First KeyCode
  @param key2 Second KeyCode
  @param key3 Third KeyCode
*/
Klaus::Input::KeyCombination::KeyCombination(TriggerType trig, KeyCodeT key1, KeyCodeT key2, KeyCodeT key3)
{
  mTriggerType = trig;

  if(key1 != KC_UNASSIGNED)
    mKeys.push_back(key1);

  if(key2 != KC_UNASSIGNED)
    mKeys.push_back(key2);

  if(key3 != KC_UNASSIGNED)
    mKeys.push_back(key3);
}

/**
  This method clears the key list of all the existing key codes.
*/
void Klaus::Input::KeyCombination::clear()
{
  mKeys.clear();
  mTriggerType = UnknownTrigger;
}

/**
  This method returns the number of key codes this key list currently has
  @return The number of key codes this key list currently has
*/
size_t Klaus::Input::KeyCombination::size() const
{
  return mKeys.size();
}

/**
  This method returns whether the KeyCombination is empty or not
  @return True if no key is assigned to the combination, false otherwise
*/
bool Klaus::Input::KeyCombination::empty() const
{
  return mKeys.empty();
}

/**
  This method adds a key to the KeyCombination
  @param key Key code to add to the list of key codes
*/
void Klaus::Input::KeyCombination::addKey( const KeyCodeT & key )
{
  for(size_t i = 0; i < mKeys.size(); i++)
    if(mKeys[i] == key)
      return;

  mKeys.push_back(key);
}

/**
  This method removes a given key code from the list
  @param key Key to be remove from list
  @return True if the key was found, false otherwise
*/
bool Klaus::Input::KeyCombination::removeKey( KeyCodeT key )
{
  normalizeKey(key);

  for(size_t i = 0; i < mKeys.size(); i++)
    if(mKeys[i] == key)
    {
      mKeys.erase(mKeys.begin() + i);
      return true;
    }

  return false;
}

/**
  This method checks whether key combination is a subset of the given key combination or not
  @param keys The key combination to check if this set is a subset of
  @return True if the combination is a subset of the given combination, false otherwise
*/
bool Klaus::Input::KeyCombination::isSubSetOf(const Klaus::Input::KeyCombination& keys) const
{
  // bigger than the the given set. definitively no subset
  if(mKeys.size() > keys.mKeys.size())
    return false;

  for(size_t i = 0; i < mKeys.size(); i++)
    if(!keys.hasKey(mKeys[i]))
      return false;

  return true;
}

/**
  A simple method to check if the given key is in the key combination or not
  @param kc The key code to check if resides in the key combination
  @return True if the given key is one of the creators of the combination, false otherwise
*/
bool Klaus::Input::KeyCombination::hasKey(const Klaus::Input::KeyCodeT& kc) const
{
  for(size_t i = 0; i < mKeys.size(); i++)
    if(mKeys[i] == kc)
      return true;

  return false;
}

/**
  This method returns the type of trigger of the key combination
  @return trigger type of the combination
*/
Klaus::Input::TriggerType Klaus::Input::KeyCombination::getTriggerType() const
{
  return mTriggerType;
}

/**
  This method sets the trigger type of the combination
  @param trig The trigger type of the combination
*/
void Klaus::Input::KeyCombination::setTriggerType(TriggerType trig)
{
  mTriggerType = trig;
}

/**
  Overload version of assignment operator for KeyCombination to enable copying lists to each other
  @param keys Keys to assign them into the list
  @return Self instance to enable recursive assignments
*/
Klaus::Input::KeyCombination & Klaus::Input::KeyCombination::operator = (const KeyCombination & keys)
{
  mKeys.clear();
  mKeys.assign(keys.mKeys.begin(), keys.mKeys.end());
  mTriggerType = keys.mTriggerType;

  return *this;
}

/**
  Overloaded version of equality check operator for KeyCombinations
  to check if the lists are equal or not
  @param keys Keys to check their equality to the current KeyCombination
  @return True if the two lists are the same, false otherwise
*/
bool Klaus::Input::KeyCombination::operator == (const KeyCombination & keys) const
{
  if(keys.mKeys.size() != mKeys.size() || keys.mTriggerType != mTriggerType)
    return false;

  for(size_t i = 0; i < mKeys.size(); i++)
    if(keys.mKeys[i] != mKeys[i])
      return false;

  return true;
}

/**
  Overloaded version of array operator. To ease access to internal list of keys.
  @param i Index of the key
  @return Key instance of the given index
*/
Klaus::Input::KeyCodeT& Klaus::Input::KeyCombination::operator[] (const size_t & i)
{
  return mKeys[i];
}

/**
  Overloaded version of array operator. Provides constant access to internal list of keys.
  @param i The index of the key
  @return Key const instance
*/
const Klaus::Input::KeyCodeT Klaus::Input::KeyCombination::operator[](const size_t& i) const
{
  return mKeys[i];
}

/**
  Overloaded version of writing to output streams. Writes the
  contents of the KeyCombination to the given output stream
  @param os Output stream to write the KeyCombination to
  @param keys KeyCombination instance to write it's keys
  @return Output stream instance to enable recursive use
*/
std::ostream & operator << (std::ostream & os, const Klaus::Input::KeyCombination & keys)
{
  os << Klaus::Input::KeyCombination::toString(keys);
  return os;
}

/**
  This method extracts a set of keys in a given string as a shortcut
  @param str String containing the keys
  @return Set of the extracted keys
*/
Klaus::Input::KeyCombination Klaus::Input::KeyCombination::fromString(std::string str)
{
  KeyCombination result;
  str = trim(str);
  const size_t size = str.size();
  std::string token;
  size_t i;

  // get the trigger type
  for(i = 0; i < size && str[i] != ','; i++)
    ; // don nothing yet

  // now copy the trigger part
  token = str.substr(0, i);

  // advance the index
  i++;

  // extract the trigger part
  if(token == "OnKeyPress")
    result.mTriggerType = OnKeyPress;
  else if(token == "OnKeyDown")
    result.mTriggerType = OnKeyDown;
  else if(token == "OnKeyUp")
    result.mTriggerType = OnKeyUp;

  // get out if could not extract, there was no more charactor to read the actual keys
  if(i >= size || result.mTriggerType == UnknownTrigger)
    return result;

  // add a + sign to the end of the string and clear the token
  str.append("+");
  token.clear();

  // read all the keys
  while((i < (size+1)) && (!isspace(str[i])))
  {
    if(str[i] == '+')
    {
      result.addKey(Klaus::Input::SingleKey::getSingleton()->fromString(token));
      token.clear();
    }
    else
      token.append(1, str[i]);

    i++;
  }

  return result;
}

/**
  This method returns the given key combination to a string
  @param keys Keys to change to a string
  @return String representation of the keylist
*/
std::string Klaus::Input::KeyCombination::toString(const Klaus::Input::KeyCombination& keys)
{
  std::string str;
  // write the trigger type
  if(keys.mTriggerType == OnKeyPress)
    str = "OnKeyPress,";
  else if(keys.mTriggerType == OnKeyUp)
    str = "OnKeyUp,";
  else if(keys.mTriggerType == OnKeyDown)
    str = "OnKeyDown,";
  else
    str = "UnknownTrigger,";

  for(size_t i = 0; i < keys.mKeys.size(); i++)
  {
    str.append(Klaus::Input::SingleKey::getSingleton()->toString(keys.mKeys[i]));
    if(i < keys.mKeys.size() - 1)
      str.append(1, '+');
  }

  return str;
}
