/*
*********************************************************************************
*            BindingCache.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 08/25/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Cache of all bindings to event for all systems. Enables loading    *
*            and saving bindings to streams.Key bindings translator and handler *
*                                                                               *
*********************************************************************************
*/

/*! \file BindingCache.h
<pre>
<b>File:</b>          BindingCache.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/25/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Cache of all bindings to event for all systems. Enables loading
               and saving bindings to streams.Key bindings translator and handler
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/25/2010       Mahdi           Initial version created
</pre>
*/

#ifndef BINDING_CACHE
#define BINDING_CACHE

#include "InputBase.h"                 // needed for KeyCombination
#include <libZigorat/Logger.h>         // needed for logger
#include <ConfigSystem/Configurable.h> // needed for Configurable

namespace Klaus
{

  namespace Input
  {

    /*! BindingCache handles saving of key bindings for all systems. Stores
        their last selected key combinations for each of their events and
        restores them the next time they become active.
    */
    class _DLLExportControl BindingCache: public Singleton<BindingCache>, public Config::Configurable
    {
      private:

        /*! EventToKeysMap is a mapping between string names of events
            and their key combinations for a particular system. */
        typedef std::map<std::string, KeyCombination> EventToKeysMap;

        /*! BindableSystem is data structure to hold system names and their bindings */
        struct BindableSystem
        {
          std::string mName;          /*!< Name of the system */
          EventToKeysMap mBindings;   /*!< All bindings of the system */

          /*!< List is a dynamic array of pointers to the BindableSystem structure */
          typedef std::vector<BindableSystem*> List;
        };

        Logger               *mLog;          /*!< Logger instance */
        BindableSystem::List  mSystems;      /*!< All registered systems */
        std::string           mBindingsFile; /*!< Name of the file to save bindings to */

        virtual void          _loadDefaultConfig(                             );
        virtual bool          _readConfig       ( const Config::ConfigType & t);
        virtual void          _writeConfig      ( const Config::ConfigType & t);

        void                  clear             (                             );
        void                  parseSystem       ( SToken token                );
        void                  loadFromFile      (                             );
        void                  saveToFile        (                             ) const;

                              BindingCache      (                             );
                            ~ BindingCache      (                             );

      public:
        static BindingCache * getSingleton      (                             );
        static void           freeSingleton     (                             );

        void                  loadFromCacheFile ( const std::string & strFile );

        bool                  getBinding        ( const std::string & strSystem,
                                                  const std::string & strEvent,
                                                  KeyCombination & keys );

        void                  setBinding        ( const std::string & strSystem,
                                                  const std::string & strEvent,
                                                  const KeyCombination & keys );
    }; // end class BindingCache

  }; // end namespace Input

}; // end namespace Klaus

#endif // BINDING_CACHE
