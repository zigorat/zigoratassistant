/*
*********************************************************************************
*           BindingCache.cpp : Robocup 3D Soccer Simulation Team Zigorat        *
*                                                                               *
*  Date: 08/25/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Cache of all bindings to event for all systems. Enables loading    *
*            and saving bindings to streams.Key bindings translator and handler *
*                                                                               *
*********************************************************************************
*/

/*! \file BindingCache.cpp
<pre>
<b>File:</b>          BindingCache.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       08/25/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Cache of all bindings to event for all systems. Enables loading
               and saving bindings to streams.Key bindings translator and handler
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
08/25/2010       Mahdi           Initial version created
</pre>
*/

#include "BindingCache.h"
#include <libZigorat/ApplicationSettings.h>   // needed for ApplicationSettings


/** BindingCache singleton instance */
template<> Klaus::Input::BindingCache* Klaus::Singleton<Klaus::Input::BindingCache>::mInstance = 0;


/**
  Constructor of BindingCache. sets up default configurations.
*/
Klaus::Input::BindingCache::BindingCache()
{
  mLog = Logger::getSingleton();

  setupConfig("KeyBindingsCache");
}

/**
  Destructor of BindingCache. Saves all recorded bindings to the file
*/
Klaus::Input::BindingCache::~BindingCache()
{
  saveToFile();
  clear();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Input::BindingCache * Klaus::Input::BindingCache::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new BindingCache;
    Logger::getSingleton()->log(1, "(BindingCache) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Input::BindingCache::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(BindingCache) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(BindingCache) removed singleton");
  }
}

/**
  This method sets up default congfiguration for this
  system. Also loads binding values from the bindings file.
*/
void Klaus::Input::BindingCache::_loadDefaultConfig()
{
  mBindingsFile = ApplicationSettings::getSingleton()->getDataPath() + "bindings.conf";
  loadFromFile();
}

/**
  This method loads configuration from a binary stream.
  @param t The configuration type of the steam
  @return True if successfully read configuration, false otherwise
*/
bool Klaus::Input::BindingCache::_readConfig( const Config::ConfigType & t )
{
  try
  {
    if(t == Config::BinaryConfig)
      mBindingsFile = readString();
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      mBindingsFile = expr["setCacheFile"][1].str();
    }
    else
    {
      mLog->log(26, "(BindingCache) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    Logger::getSingleton()->log(26, "(BindingCache) exception: %s", str);
    return false;
  }

  return true;
}

/**
  This method writes the configuration of the system to the given output stream.
  @param type The configuration type to save the stream
*/
void Klaus::Input::BindingCache::_writeConfig( const Config::ConfigType & type )
{
  if(type == Config::BinaryConfig)
    writeString(mBindingsFile);
  else if(type == Config::SExpConfig)
    writeText("(setCacheFile '%s')", mBindingsFile.c_str());
  else
    mLog->log(26, "(BindingCache) write: configuration type %d not supported", type);
}

/**
  This method clears the binding cache and it's related data.
*/
void Klaus::Input::BindingCache::clear()
{
  for(size_t i = 0; i < mSystems.size(); i++)
    delete mSystems[i];

  mSystems.clear();
}

/**
  This method parses the given s-expression token for bindings of a specific system
  @param token The token containing the system's binding cache
*/
void Klaus::Input::BindingCache::parseSystem(Klaus::SToken token)
{
  if(token.size() < 2)
  {
    mLog->log(26, "(BindingCache) insufficient tokens: %s", token.str().c_str());
    return;
  }

  BindableSystem * sys = new BindableSystem;
  sys->mName = token[0].str();
  mLog->log(26, "(BindableCache) added system %s", token[0].str().c_str());
  for(size_t i = 1; i < token.size(); ++i)
    if(token[i].isNormal() && token[i].size() == 2)
    {
      // add the command
      std::string cmd = token[i][0].str();
      sys->mBindings[cmd] = KeyCombination::fromString(token[i][1]);
      mLog->log(26, "(BindableCache)   set %s for %s event",
                    KeyCombination::toString(sys->mBindings[cmd]).c_str(), cmd.c_str());
    }

  // add it to all systems
  mSystems.push_back(sys);
}

/**
  This method loads all saved bindings in the application from a previously saved file.
*/
void Klaus::Input::BindingCache::loadFromFile()
{
  try
  {
    // 8 KiloBytes seems fairly enough for a typical key binding cache
    SExpression file(mBindingsFile, 8 * 1024);
    int index = file.getStartingIndex();
    if(index == -1)
    {
      mLog->log(26, "(BindingCache) no valid section in cache file '%s'", mBindingsFile.c_str());
      return;
    }

    SToken tokens = file[(size_t)index];

    // clear the cache
    clear();

    // and read all of them
    for(size_t i = 0; i < tokens.size(); i++)
      if(tokens[i].isNormal())
        parseSystem(tokens[i]);
  }
  catch(SLangException e)
  {
    mLog->log(26, "(BindingCache) error parsing cache file '%s'", mBindingsFile.c_str());
    mLog->log(26, "%s", e.str());
  }
}

/**
  Saves all bindings of all systems to the binding file.
*/
void Klaus::Input::BindingCache::saveToFile() const
{
  using namespace std;

  ofstream file(mBindingsFile.c_str());
  if(!file)
  {
    mLog->log(26, "(BindingCache) could not open file to write '%s'", mBindingsFile.c_str());
    return;
  }

  file << '(' << endl << "  ;" << endl << "  ; Key combinations and their bindings" << endl << "  ;" << endl << endl;

  for(size_t i = 0; i < mSystems.size(); i++)
  {
    file << endl << "  (" << mSystems[i]->mName << endl;

    for(EventToKeysMap::const_iterator it = mSystems[i]->mBindings.begin();
        it != mSystems[i]->mBindings.end();
        it++)
      file << "    ('" << it->first << "\' " << KeyCombination::toString(it->second) << ')' << endl;

    file << "  )" << endl;
  }

  file << endl << "  ; end of file" << endl << ')';

  file.close();
}

/**
  This method sets the bindings cache file.
  @param strFile Address of the file which contains binding cache
*/
void Klaus::Input::BindingCache::loadFromCacheFile(const std::string& strFile)
{
  mBindingsFile = strFile;
  loadFromFile();
}

/**
  This method finds out a previously used key combination for a specific event of a specific system.
  @param strSystem The system name
  @param strEvent The event to return it's previously used key combination
  @param keys The key combination previously used for the system
  @return True if a binding was found for the given system name and event, false otherwise
*/
bool Klaus::Input::BindingCache::getBinding(const std::string& strSystem, const std::string& strEvent, KeyCombination & keys )
{
  BindableSystem * sys = NULL;
  for(size_t i = 0; i < mSystems.size(); i++)
    if(mSystems[i]->mName == strSystem)
      sys = mSystems[i];

  if(!sys)
  {
    mLog->log(26, "(BindableCache) system not found: '%s'", strSystem.c_str());
    return false;
  }

  for(EventToKeysMap::const_iterator it = sys->mBindings.begin(); it != sys->mBindings.end(); it++)
    if(it->first == strEvent)
    {
      keys = it->second;
      return true;
    }

  return false;
}

/**
  This method sets the given key combination as default binding for it's system and event
  @param strSystem The system name to set it's event's binding
  @param strEvent The event name to set it's binding
  @param keys The key combination for the event
*/
void Klaus::Input::BindingCache::setBinding(const std::string& strSystem, const std::string& strEvent, const Klaus::Input::KeyCombination& keys)
{
  // get the systems data instance
  BindableSystem * sys = NULL;
  for(size_t i = 0; i < mSystems.size(); i++)
    if(mSystems[i]->mName == strSystem)
      sys = mSystems[i];

  // create a data instance for the system if not found
  if(!sys)
  {
    sys = new BindableSystem;
    sys->mName = strSystem;
    mSystems.push_back(sys);
    mLog->log(26, "(BindingCache) added new system: %s", strSystem.c_str());
  }

  // find the event in the system and update it
  for(EventToKeysMap::iterator it = sys->mBindings.begin(); it != sys->mBindings.end(); it++)
    if(it->first == strEvent)
    {
      it->second = keys;
      return;
    }

  // set the event
  sys->mBindings[strEvent] = keys;
  mLog->log(26, "(BindingCache) set '%s' event to '%s' for '%s'",
            strEvent.c_str(), KeyCombination::toString(keys).c_str(), strSystem.c_str());
}
