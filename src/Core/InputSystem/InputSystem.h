/*
*********************************************************************************
*           InputSystem.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Input system interface of application with it's input              *
*            handling plugins                                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file InputSystem.h
<pre>
<b>File:</b>          InputSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Input system interface of application with it's input handling plugins
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef INPUT_SYSTEM
#define INPUT_SYSTEM


#include "KeyBindings.h"
#include "InputPlugin.h"
#include <EventSystem/EventSystem.h>
#include <libZigorat/Geometry.h>
#include <ConfigSystem/Configurable.h>


namespace Klaus
{


  namespace Input
  {


    /*! EventInformation holds all needed information for handling input events of mice and keyboards */
    union EventInformation
    {
      /*! MouseButtonEvent holds information of a mouse button up or down event */
      struct MouseButtonEvent
      {
        int X;                              /*!< Absolute X coordinate */
        int Y;                              /*!< Absolute Y coordinate */
        KeyCodeT Button;                    /*!< Button name           */
      } MouseButtonDown, MouseButtonUp;

      /*! MouseMoveEvent holds information of a mouse movement event */
      struct MouseMoveEvent
      {
        int X;                              /*!< Relative X coordinate */
        int Y;                              /*!< Relative Y coordinate */
        int AbsX;                           /*!< Absolute X coordinate */
        int AbsY;                           /*!< Absolute Y coordinate */
      } MouseMove;                          /*!< Mouse movement information */

      /*! KeyBoardEvent holds information of keyboard key presses event */
      struct KeyBoardEvent
      {
        KeyCodeT KeyCode;                   /*!< KeyCode of the event */
      } KeyDown, KeyUp;
    };


    /*! EventType specifies type of input event which includes a mouse button
        event, a mouse movement event or a keyboard event. */
    enum EventType
    {
      EV_MOUSE_DOWN,                        /*!< Mouse button down event     */
      EV_MOUSE_UP,                          /*!< Mouse button up event       */
      EV_MOUSE_MOVE,                        /*!< Mouse move event            */
      EV_KEY_DOWN,                          /*!< Keyboard button down event  */
      EV_KEY_UP,                            /*!< Keyboard button up event    */
      EV_ILLEGAL                            /*!< Unknown event               */
    };


    /*! InputEvent holds all data corresponding to a single specific event.  */
    struct InputEvent
    {
      EventType Type;                       /*!< Type of the event           */
      EventInformation Data;                /*!< Data of the event           */

      typedef std::vector<InputEvent> Array; /*!< Dynamic array of input events */
    };



    /*! Input system handles input from system peripherals via input plugins. */
    class _DLLExportControl InputSystem: public Singleton<InputSystem>,
                                         public Events::EventRecipient,
                                         public Config::Configurable
    {
      private:
        // events
        EventID              EV_OPENNED;      /*!< Window openned event ID    */
        EventID              EV_CLOSED;       /*!< Winow closed event ID      */

        // members
        InputPlugin        * mPlugin;         /*!< Input plugin instance      */
        PluginFactory      * mPluginFactory;  /*!< Input plugin factory       */
        InputEvent::Array    mInputEvents;    /*!< Input events               */
        Logger             * mLog;            /*!< Logger instance            */
        KeyBindings        * mKeyBindings;    /*!< KeyBindings instance       */
        KeyCombination       mDownKeys;       /*!< Down keys                  */
        VecPosition          mCursorPos;      /*!< Cursor absolute pos        */
        VecPosition          mCursorMove;     /*!< Cursor moved position      */
        std::string          mPluginToLoad;   /*!< Plugin name to load        */
        bool                 mFallbackOnFail; /*!< Use fallback plugins       */
        std::string               mSleepingPlugin; /*!< Deactivated plugin name*/
        bool                 mMouseGrab;      /*!< Whether grab the mouse     */

        void                 deletePlugin     (                               );
        bool                 setupInputPlugin ( const std::string & strPlugin );
        void                 processInputEvents(                              );

        // Configuration related
        virtual void         _loadDefaultConfig(                              );
        virtual bool         _readConfig      ( const Config::ConfigType & t  );
        virtual void         _writeConfig     ( const Config::ConfigType & t  );

        bool                 activate         (                               );
        bool                 deactivate       (                               );

                             InputSystem      (                               );
        virtual            ~ InputSystem      (                               );

      public:
        static InputSystem * getSingleton     (                               );
        static void          freeSingleton    (                               );

        bool                 setupInput       (                               );

        void                 processInput     (                               );
        void                 recordInputEvent ( const InputEvent & ev         );

        VecPosition          getMousePosition (                               );
        VecPosition          getMouseDisplace (                               );

        void                 setMouseGrab     ( bool bGrab                    );
        bool                 getMouseGrab     (                               ) const;

        // Event related
        virtual void         processEvent     ( const Events::Event& ev       );
    };


  }; // end namespace Input


}; // end namespace Klaus


#endif // INPUT_SYSTEM
