/*
*********************************************************************************
*         InputSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Input system interface of application with it's input              *
*            handling plugins                                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file InputSystem.cpp
<pre>
<b>File:</b>          InputSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Input system interface of application with it's input handling plugins
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#include "InputSystem.h"
#include <PluginSystem/PluginSystem.h>      // needed for PluginSystem
#include <libZigorat/Logger.h>              // needed for Logger
#include <GUISystem/GUISystem.h>            // needed for GUISystem
#include <sstream>

#if PLATFORM == PLATFORM_WINDOWS
#include <windows.h>
#endif


/** InputSystem singleton instance */
template<> Klaus::Input::InputSystem* Klaus::Singleton<Klaus::Input::InputSystem>::mInstance = 0;


/**
  Constructor of InputSystem. set's up configuration management and hides the
  mouse cursor on windows.
*/
Klaus::Input::InputSystem::InputSystem()
{
  mPlugin = NULL;
  mLog = Logger::getSingleton();
  mKeyBindings = KeyBindings::getSingleton();

  setupConfig("InputSystem");
  setRecipientName("InputSystem");

  EV_OPENNED = registerEvent("MainWindow\\Openned");
  EV_CLOSED = registerEvent("MainWindow\\Closed");

  // NOTE: On windows, the cursor will not hide when mouse is not exclusive!
  // and it will only remain in the app when exclusive
  // So:
  #if PLATFORM == PLATFORM_WINDOWS
    ShowCursor(false);
  #endif
}

/**
  Destructor of InputSystem
*/
Klaus::Input::InputSystem::~InputSystem()
{
  deletePlugin();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::Input::InputSystem * Klaus::Input::InputSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new InputSystem;
    Logger::getSingleton()->log(1, "(InputSystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::Input::InputSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(InputSystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(InputSystem) removed singleton");
  }
}

/**
  This method deletes the input plugin (if initialized)
*/
void Klaus::Input::InputSystem::deletePlugin()
{
  if(mPlugin)
    mPluginFactory->freePluginInstance(mPlugin);

  mPlugin = NULL;

  // mPluginFactory will not be freed here.
  // It will be freed in PluginSystem's destructor
}

/**
  This method attempts to load and initialize an input plugin
  @return True if the input plugin was successfully loaded and installed, false otherwise
*/
bool Klaus::Input::InputSystem::setupInputPlugin(const std::string& strPlugin)
{
  if(mPlugin)
  {
    mLog->log(4, "(InputSystem) setup plugin: a plugin is already active");
    return false;
  }

  mPluginFactory = PluginSystem::getSingleton()->findPlugin(strPlugin, "InputPlugin");
  if(!mPluginFactory)
    return false;

  Plugin *pl = mPluginFactory->createPluginInstance();
  if(!pl)
    return false;

  mPlugin = dynamic_cast<InputPlugin*>(pl);
  if(!mPlugin)
  {
    mPluginFactory->freePluginInstance(pl);
    return false;
  }

  if(!mPlugin->initialize())
  {
    mPluginFactory->freePluginInstance(mPlugin);
    return false;
  }

  return true;
}

/**
  This method sends each event captured and stored by the input plugin to the FrameManager.
  On finish, it will clear all stored events.
*/
void Klaus::Input::InputSystem::processInputEvents()
{
  // reset mouse movement
  mDownKeys.removeKey(KC_MOUSE_MOVE);
  mCursorMove.setPosition(0.0, 0.0, 0.0);

  // can cursor grab the app?
  bool gui_exclusive = false;
  GUI::Element * node = GUI::GUISystem::getSingleton()->getNodeContainingMouse();
  if((node) && (node->getType() != GUI::Window))
    gui_exclusive = true;

  bool gui_input_active = false;
  node = GUI::GUISystem::getSingleton()->getActiveNode();
  if((node) && ((node->getType() == GUI::Memo) || (node->getType() == GUI::TextBox)))
    gui_input_active = true;

  for(size_t i = 0; i < mInputEvents.size(); i++)
    if(mInputEvents[i].Type == EV_KEY_DOWN)
    {
      if(gui_input_active)
        GUI::GUISystem::getSingleton()->injectKeyDown(mInputEvents[i].Data.KeyDown.KeyCode);
      else
        mDownKeys.addKey(mInputEvents[i].Data.KeyDown.KeyCode);
    }
    else if(mInputEvents[i].Type == EV_KEY_UP)
    {
      if(gui_input_active)
        GUI::GUISystem::getSingleton()->injectKeyUp(mInputEvents[i].Data.KeyUp.KeyCode);
      else
        mDownKeys.removeKey(mInputEvents[i].Data.KeyUp.KeyCode);
    }
    else if(mInputEvents[i].Type == EV_MOUSE_DOWN)
    {
      if(!gui_exclusive)
        mDownKeys.addKey(mInputEvents[i].Data.MouseButtonDown.Button);

      mCursorPos.setPosition(mInputEvents[i].Data.MouseButtonDown.X, mInputEvents[i].Data.MouseButtonDown.Y);
      GUI::GUISystem::getSingleton()->injectKeyDown(mInputEvents[i].Data.MouseButtonDown.Button);
    }
    else if(mInputEvents[i].Type == EV_MOUSE_UP)
    {
      mDownKeys.removeKey(mInputEvents[i].Data.MouseButtonUp.Button);
      mCursorPos.setPosition(mInputEvents[i].Data.MouseButtonUp.X, mInputEvents[i].Data.MouseButtonUp.Y);
      GUI::GUISystem::getSingleton()->injectKeyUp(mInputEvents[i].Data.MouseButtonUp.Button);
    }
    else if(mInputEvents[i].Type == EV_MOUSE_MOVE)
    {
      mDownKeys.addKey(KC_MOUSE_MOVE);
      mCursorPos.setPosition(mInputEvents[i].Data.MouseMove.AbsX, mInputEvents[i].Data.MouseMove.AbsY);
      mCursorMove.setPosition(mInputEvents[i].Data.MouseMove.X, mInputEvents[i].Data.MouseMove.Y);
      GUI::GUISystem::getSingleton()->injectMousePosition(Round(mCursorPos[0]), Round(mCursorPos[1]));
    }
    else
      mLog->log(4, "(InputSystem) unknown input event recieved");

  mKeyBindings->updateKeysState(mDownKeys);

  mInputEvents.clear();
}

/**
  This method sets up default congfiguration for this system
*/
void Klaus::Input::InputSystem::_loadDefaultConfig()
{
  mPluginToLoad = "OIS";
  mFallbackOnFail = true;
  mMouseGrab = false;
}

/**
  This method loads configuration from a binary stream. If successfully
  loaded, the system will re initialize to use the configurations.
  @param t The configuration type of the steam
  @return True on sucessfull read of configurations, false otherwise
*/
bool Klaus::Input::InputSystem::_readConfig(const Config::ConfigType & t)
{
  try
  {
    if(t == Config::BinaryConfig)
    {
      mPluginToLoad = readString();
      mFallbackOnFail = readBool();
      mMouseGrab = readBool();
    }
    else if(t == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      mPluginToLoad = expr["loadPlugin"][1].str();
      mFallbackOnFail = expr["fallBackOnFail"][1].boolean();
      mMouseGrab = expr["grabMouse"][1].boolean();
    }
    else
    {
      mLog->log(4, "(InputSystem) read: configuration type %d not supported", t);
      return false;
    }
  }
  catch(const char * str)
  {
    mLog->log(4, "(InputSystem) exception: %s", str);
    return false;
  }

  // shall not initialized the new plugin
  // this shall be done after all systems have been initialized
  return true;
}

/**
  This method writes the configuration of input system to the given output stream.
  @param type The configuration type to save the stream
*/
void Klaus::Input::InputSystem::_writeConfig(const Config::ConfigType & type)
{
  if(type == Config::BinaryConfig)
  {
    writeString(mPluginToLoad);
    writeBool(mFallbackOnFail);
    writeBool(mMouseGrab);
  }
  else if(type == Config::SExpConfig)
  {
    writeText("(loadPlugin %s)", mPluginToLoad.c_str());

    if(mFallbackOnFail)
      writeText("(fallBackOnFail true)");
    else
      writeText("(fallBackOnFail false)");

    if(mMouseGrab)
      writeText("(grabMouse true)");
    else
      writeText("(grabMouse false)");
  }
  else
    mLog->log(4, "(InputSystem) write: configuration type %d not supported", type);
}

/**
  This method actiaves input system by loading the previously deactivated input plugin again.
  @return True if successfully activated the deactivated plugin, false otherwise
*/
bool Klaus::Input::InputSystem::activate()
{
  if(mSleepingPlugin.empty())
  {
    mLog->log(4, "(InputSystem) activate: no sleeping plugin");
    return false;
  }

  if(!setupInputPlugin(mSleepingPlugin))
  {
    mLog->log(4, "(InputSystem) activate: could not activate %s, reseting input system", mSleepingPlugin.c_str());
    return setupInput();
  }

  mLog->log(4, "(InputSystem) activataion sucessfull");
  mSleepingPlugin.clear();
  return true;
}

/**
  This method deactivates input system. This is used when switching or reinitializing render windows
  @return True if the activate plugin was deactivated, false otherwise
*/
bool Klaus::Input::InputSystem::deactivate()
{
  if(!mPlugin)
  {
    mLog->log(4, "(InputSystem) deactivate: system not initialized");
    return false;
  }

  // save the plugin's name
  mSleepingPlugin = mPluginFactory->getName();

  // remove the plugin
  deletePlugin();

  // flush the keyboard buffer
  mDownKeys.clear();
  mKeyBindings->updateKeysState(mDownKeys);

  return true;
}

/**
  This method create's the input plugin. Input plugin will be created according
  to the given sorted list of acceptable plugin names.
  @return True if any input plugin could be initialized, false otherwise
*/
bool Klaus::Input::InputSystem::setupInput()
{
  if(!mSleepingPlugin.empty())
  {
    mLog->log(4, "(InputSystem) system deactivated, activating instead");
    return activate();
  }

  // do not load a previously loaded default plugin
  if(mPlugin && mPluginFactory->getName() == mPluginToLoad)
    return true;

  deletePlugin();

  StringArray plugins;
  if(mFallbackOnFail)
    PluginSystem::getSingleton()->findAllPluginsByRole(plugins, "InputPlugin");
  plugins.insert(plugins.begin(), mPluginToLoad);

  for(size_t i = 0; i < plugins.size(); i++)
    if(setupInputPlugin(plugins[i]))
    {
      mLog->log(4, "(InputSystem) loaded plugin %s", plugins[i].c_str());
      return true;
    }

  mPlugin = NULL;
  return false;
}

/**
  This method is the main access point of InputSystem for application core. Between
  frame renderings this shall be called to inform the application of input peripheral
  events. This method calls the input plugin's checkForInputEvents to capture input
  events and send them back here. The list of events will then be processed by
  processEvents method which in turns informs KeyBindings instance of the input events.
*/
void Klaus::Input::InputSystem::processInput()
{
  if(!mPlugin)
  {
    mLog->log(4, "(InputSystem) input plugin not initialized");
    return;
  }

  // This notifies input plugin to capture input events and store them on the given list
  mPlugin->checkForInputEvents();

  // The plugin has saved input events on mInputEvents
  // Now they will be sent to FrameManager
  processInputEvents();
}

/**
  This method records input events from input responsible plugins.
  @param ev The event sents by the input plugin. (mouse, keyboard and joystick)
*/
void Klaus::Input::InputSystem::recordInputEvent(const InputEvent & ev)
{
  mInputEvents.push_back(ev);
}

/**
  This method returns the current mouse cursor position
  @return The mouse cursor's position
*/
Klaus::VecPosition Klaus::Input::InputSystem::getMousePosition()
{
  return mCursorPos;
}

/**
  This method will returns how many pixels mouse cursor has been moved since last frame
  @return Mouse displacement in last frame's time
*/
Klaus::VecPosition Klaus::Input::InputSystem::getMouseDisplace()
{
  return mCursorMove;
}

/**
  This method sets whether mouse cursor shall not leave the application's window or not
  @param bGrab If true, mouse shall not leave application's window, false means application
               has a normal window which does not confine mouse cursor
*/
void Klaus::Input::InputSystem::setMouseGrab(bool bGrab)
{
  mMouseGrab = bGrab;
}

/**
  This method returns whether mouse shall be grabbed by application or not. if grabbed,
  mouse cursor shall never leave applications window
  @return True if mouse shall be confined in the application's window, false otherwise
*/
bool Klaus::Input::InputSystem::getMouseGrab() const
{
  return mMouseGrab;
}

/**
  This method receives events from the event system and processes them.
  @param ev The event sent by core
*/
void Klaus::Input::InputSystem::processEvent( const Events::Event & ev)
{
  if(ev == EV_CLOSED)
    deactivate();
  else if(ev == EV_OPENNED)
    setupInput();
  else
    mLog->log(4, "(InputSystem) recieved event %s", Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}
