/*
*********************************************************************************
*            KeyBindings.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 11/17/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Key bindings translator and handler                                *
*                                                                               *
*********************************************************************************
*/

/*! \file KeyBindings.h
<pre>
<b>File:</b>          KeyBindings.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/17/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Key bindings translator and handler
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/17/2008       Mahdi           Initial version created
</pre>
*/

#ifndef KEY_BINDINGS
#define KEY_BINDINGS


#include <libZigorat/Logger.h>          // needed for Logger
#include <EventSystem/EventRecipient.h> // needed for EventRecipient
#include "BindingCache.h"               // needed for BindingCache, KeyCombination


namespace Klaus
{

  namespace Input
  {

    /*! KeyBindings class. The core of extracting actions from pressed keys
        and all registered bindings. It uses an iterative algorithm to extract
        definite actions from probable actions of all pressed keys. Fastest
        one I came with. Order is roughly 1. */
    class _DLLExportControl KeyBindings: public Singleton<KeyBindings>
    {
      private:

        /*! BindingInfo holds all the data for binding between a
            key combination and it's binded registered events. */
        struct BindingInfo
        {
          /*! Key combination of the binding */
          KeyCombination mKeys;

          /*! Events registered to the binding */
          EventIDList mRegisteredEvents;

          /*! Names of registered systems */
          Events::EventRecipient::List mRegisteredSystems;

          /*!< For OnKeyPress trigger types, used to trigger the action once the key is pushed down, not more. */
          bool mFired;

          /*! Mapping is a combination of binding IDs and binding informations. */
          typedef std::map<BindingID, BindingInfo*> Mapping;

          /*! List is a dynamic array of BindingInfo pointers. */
          typedef std::vector<BindingInfo*> List;
        };


        Logger              * mLog;                   /*!< Logger isntance */
        BindingCache        * mCache;                 /*!< Bindings cached from configuration file */
        BindingID             mMaxBindings;           /*!< Number of registered bindings */
        BindingInfo::Mapping  mBindings;              /*!< Registered bindings info */
        KeyCombination        mDownKeys;              /*!< Current down keys (both keyboard and mouse) */
        BindingInfo::List     mActiveActions;         /*!< Actions which shall be definitely executed */
        BindingInfo::List     mKeyCodesCache[KC_MAX]; /*!< Each element caches BindingInfo pointers
                                                           whom in their key combination have the
                                                           element's key code */
        BindingInfo::List     mProbableActions;       /*!< Probable actions. Actions which if their key
                                                           combinations are all pressed, can be moved
                                                           into active actions. */

        BindingInfo *         getBindingInfo   ( const BindingID & bind_id   );
        BindingID             getBindingID     ( const KeyCombination & keys );

        void                  injectKeyDown    ( const KeyCodeT & kc         );
        void                  injectKeyUp      ( const KeyCodeT & kc         );
        void                  removeBindingFromKeyCache( const KeyCodeT & key,
                                                const BindingInfo * inf      );
        void                  filterProbableActions(                         );

        void                  fireEvent        ( BindingInfo * binding       );

                              KeyBindings      (                             );
                            ~ KeyBindings      (                             );
      public:
        static KeyBindings  * getSingleton     (                             );
        static void           freeSingleton    (                             );

        // binding interactions
        BindingID             registerBinding  ( Events::EventRecipient * sys,
                                                 const KeyCombination & keys,
                                                 const EventID & event_id,
                                                 bool bUseTheNewKeys = true  );
        bool                  unregisterBinding( const BindingID & bind_id,
                                                 Events::EventRecipient * sys);
        bool                  changeBinding    ( const BindingID & bind_id,
                                                 Events::EventRecipient *sys,
                                                 const KeyCombination & keys );

        // utilities
        void                  updateKeysState  ( const KeyCombination & dkeys);
        bool                  isKeyDown        ( const KeyCodeT & kc         );

        // core related methods
        void                  fireEvents       (                             );
    };


}; // end namespace Input


}; // end namespace Klaus

#endif // KEY_BINDINGS

