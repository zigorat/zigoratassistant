/*
*********************************************************************************
*            InputBase.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/17/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Key Utilities                                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file InputBase.h
<pre>
<b>File:</b>          InputBase.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/17/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Key utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/17/2008       Mahdi           Initial version created
</pre>
*/


#ifndef KLAUS_KEYS
#define KLAUS_KEYS


#include <string>                 // needed for string
#include <vector>                 // needed for vector
#include <map>                    // needed for map
#include <iostream>               // needed for iostream
#include <libZigorat/Singleton.h> // needed for Klaus::Singleton


#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  /*! BindingID is a type to handle IDs given to Bindings */
  typedef unsigned BindingID;

  /*! BindingIDList is a dynamic array of BindingIDs */
  typedef std::vector<BindingID> BindingIDList;

  namespace Input
  {

    /*! KeyCodeT is an enumeration of all the keys on ordinary keyboards. */
    enum KeyCodeT
    {
      KC_UNASSIGNED  = 0x00, /*!< Unknown key */
      KC_ESCAPE      = 0x01, /*!< Escape */
      KC_1           = 0x02, /*!< Main 1 */
      KC_2           = 0x03, /*!< Main 2 */
      KC_3           = 0x04, /*!< Main 3 */
      KC_4           = 0x05, /*!< Main 4 */
      KC_5           = 0x06, /*!< Main 5 */
      KC_6           = 0x07, /*!< Main 6 */
      KC_7           = 0x08, /*!< Main 7 */
      KC_8           = 0x09, /*!< Main 8 */
      KC_9           = 0x0A, /*!< Main 9 */
      KC_0           = 0x0B, /*!< Main 0 */
      KC_MINUS       = 0x0C, /*!< Minus on main keyboard */
      KC_EQUALS      = 0x0D, /*!< Equals */
      KC_BACK        = 0x0E, /*!< Backspace */
      KC_TAB         = 0x0F, /*!< Tab */
      KC_Q           = 0x10, /*!< Key Q */
      KC_W           = 0x11, /*!< Key W */
      KC_E           = 0x12, /*!< Key E */
      KC_R           = 0x13, /*!< Key R */
      KC_T           = 0x14, /*!< Key T */
      KC_Y           = 0x15, /*!< Key Y */
      KC_U           = 0x16, /*!< Key U */
      KC_I           = 0x17, /*!< Key I */
      KC_O           = 0x18, /*!< Key O */
      KC_P           = 0x19, /*!< Key P */
      KC_LBRACKET    = 0x1A, /*!< Left Bracket */
      KC_RBRACKET    = 0x1B, /*!< Right Bracket */
      KC_RETURN      = 0x1C, /*!< Enter on main keyboard */
      KC_LCONTROL    = 0x1D, /*!< Left Control modifier */
      KC_A           = 0x1E, /*!< Key A */
      KC_S           = 0x1F, /*!< Key S */
      KC_D           = 0x20, /*!< Key D */
      KC_F           = 0x21, /*!< Key F */
      KC_G           = 0x22, /*!< Key G */
      KC_H           = 0x23, /*!< Key H */
      KC_J           = 0x24, /*!< Key J */
      KC_K           = 0x25, /*!< Key K */
      KC_L           = 0x26, /*!< Key L */
      KC_SEMICOLON   = 0x27, /*!< Semicolon: ; */
      KC_APOSTROPHE  = 0x28, /*!< Apastrophe: ' */
      KC_GRAVE       = 0x29, /*!< ?! (accent key) */
      KC_LSHIFT      = 0x2A, /*!< Left Shift Modifier */
      KC_BACKSLASH   = 0x2B, /*!< Backslash: \ */
      KC_Z           = 0x2C, /*!< Key Z */
      KC_X           = 0x2D, /*!< Key X */
      KC_C           = 0x2E, /*!< Key C */
      KC_V           = 0x2F, /*!< Key V */
      KC_B           = 0x30, /*!< Key B */
      KC_N           = 0x31, /*!< Key N */
      KC_M           = 0x32, /*!< Key M */
      KC_COMMA       = 0x33, /*!< Comma: , */
      KC_PERIOD      = 0x34, /*!< Period (on main keyboard): . */
      KC_SLASH       = 0x35, /*!< Slash (on main keyboard): / */
      KC_RSHIFT      = 0x36, /*!< Right Shift Modifier */
      KC_MULTIPLY    = 0x37, /*!< Multpiply (on numeric keypad): * */
      KC_LMENU       = 0x38, /*!< Left Alt */
      KC_SPACE       = 0x39, /*!< Space bar */
      KC_CAPITAL     = 0x3A, /*!< Caps Lock */
      KC_F1          = 0x3B, /*!< Function Key 1 */
      KC_F2          = 0x3C, /*!< Function Key 2 */
      KC_F3          = 0x3D, /*!< Function Key 3 */
      KC_F4          = 0x3E, /*!< Function Key 4 */
      KC_F5          = 0x3F, /*!< Function Key 5 */
      KC_F6          = 0x40, /*!< Function Key 6 */
      KC_F7          = 0x41, /*!< Function Key 7 */
      KC_F8          = 0x42, /*!< Function Key 8 */
      KC_F9          = 0x43, /*!< Function Key 9 */
      KC_F10         = 0x44, /*!< Function Key 10 */
      KC_NUMLOCK     = 0x45, /*!< Num Lock */
      KC_SCROLL      = 0x46, /*!< Scroll Lock */
      KC_NUMPAD7     = 0x47, /*!< Numpad key 7 */
      KC_NUMPAD8     = 0x48, /*!< Numpad key 8 */
      KC_NUMPAD9     = 0x49, /*!< Numpad key 9 */
      KC_SUBTRACT    = 0x4A, /*!< Minus (on numeric keypad) */
      KC_NUMPAD4     = 0x4B, /*!< Numpad key 4 */
      KC_NUMPAD5     = 0x4C, /*!< Numpad key 5 */
      KC_NUMPAD6     = 0x4D, /*!< Numpad key 6 */
      KC_ADD         = 0x4E, /*!< Add on numeric keypad */
      KC_NUMPAD1     = 0x4F, /*!< Numpad key 1 */
      KC_NUMPAD2     = 0x50, /*!< Numpad key 2 */
      KC_NUMPAD3     = 0x51, /*!< Numpad key 3 */
      KC_NUMPAD0     = 0x52, /*!< Numpad key 0 */
      KC_DECIMAL     = 0x53, /*!< Period (decimal on numeric keypad) */
      KC_OEM_102     = 0x56, /*!< < > | on UK/Germany keyboards (OIS source code) */
      KC_F11         = 0x57, /*!< Function Key 11 */
      KC_F12         = 0x58, /*!< Function Key 12 */
      KC_F13         = 0x64, /*!< Function Key 13 (NEC PC98) */
      KC_F14         = 0x65, /*!< Function Key 14 (NEC PC98) */
      KC_F15         = 0x66, /*!< Function Key 15 (NEC PC98) */
      KC_KANA        = 0x70, /*!< Japanese key */
      KC_ABNT_C1     = 0x73, /*!< / ? on Portugese (Brazilian) keyboards (OIS source code) */
      KC_CONVERT     = 0x79, /*!< Japanese key */
      KC_NOCONVERT   = 0x7B, /*!< Japanese key */
      KC_YEN         = 0x7D, /*!< Japanese key */
      KC_ABNT_C2     = 0x7E, /*!< Numpad . on Portugese (Brazilian) keyboards (OIS source code) */
      KC_NUMPADEQUALS= 0x8D, /*!< Equals (on numeric keypad) */
      KC_PREVTRACK   = 0x90, /*!< Previous track */
      KC_AT          = 0x91, /*!< At sign: @ */
      KC_COLON       = 0x92, /*!< Colon: : */
      KC_UNDERLINE   = 0x93, /*!< Underline: _ */
      KC_KANJI       = 0x94, /*!< Japanese key */
      KC_STOP        = 0x95, /*!< Stop */
      KC_AX          = 0x96, /*!< Japanese key */
      KC_UNLABELED   = 0x97, /*!< (J3100) (OIS source code) */
      KC_NEXTTRACK   = 0x99, /*!< Next Track */
      KC_NUMPADENTER = 0x9C, /*!< Enter (on numeric keypad) */
      KC_RCONTROL    = 0x9D, /*!< Right Control modifier */
      KC_MUTE        = 0xA0, /*!< Mute */
      KC_CALCULATOR  = 0xA1, /*!< Calculator */
      KC_PLAYPAUSE   = 0xA2, /*!< Play/Pause */
      KC_MEDIASTOP   = 0xA4, /*!< Media Stop (not web stop) */
      KC_VOLUMEDOWN  = 0xAE, /*!< Decrease volume */
      KC_VOLUMEUP    = 0xB0, /*!< Increase volume */
      KC_WEBHOME     = 0xB2, /*!< Web home */
      KC_NUMPADCOMMA = 0xB3, /*!< Comman (on numeric keypad) */
      KC_DIVIDE      = 0xB5, /*!< Division (on numeric keypad) */
      KC_SYSRQ       = 0xB7, /*!< Print Screen */
      KC_RMENU       = 0xB8, /*!< Right Menu */
      KC_PAUSE       = 0xC5, /*!< Pause key (not media pause/play) */
      KC_HOME        = 0xC7, /*!< Home key (not web home) */
      KC_UP          = 0xC8, /*!< Up arrow */
      KC_PGUP        = 0xC9, /*!< Page up */
      KC_LEFT        = 0xCB, /*!< Left arrow */
      KC_RIGHT       = 0xCD, /*!< Right arrow */
      KC_END         = 0xCF, /*!< End key */
      KC_DOWN        = 0xD0, /*!< Down arrow */
      KC_PGDOWN      = 0xD1, /*!< Page Down */
      KC_INSERT      = 0xD2, /*!< Insert */
      KC_DELETE      = 0xD3, /*!< Delete */
      KC_LWIN        = 0xDB, /*!< Left Windows key modifier */
      KC_RWIN        = 0xDC, /*!< Right Windows key modifier */
      KC_APPS        = 0xDD, /*!< AppMenu */
      KC_POWER       = 0xDE, /*!< Power (shut down/restart) */
      KC_SLEEP       = 0xDF, /*!< Sleep */
      KC_WAKE        = 0xE3, /*!< Wake */
      KC_WEBSEARCH   = 0xE5, /*!< Web Search */
      KC_WEBFAVORITES= 0xE6, /*!< Web Favorites */
      KC_WEBREFRESH  = 0xE7, /*!< Web Refresh */
      KC_WEBSTOP     = 0xE8, /*!< Web Stop */
      KC_WEBFORWARD  = 0xE9, /*!< Web Forward */
      KC_WEBBACK     = 0xEA, /*!< Web Back */
      KC_MYCOMPUTER  = 0xEB, /*!< My Computer shortcut key */
      KC_MAIL        = 0xEC, /*!< Mail start key */
      KC_MEDIASELECT = 0xED, /*!< Media select */

      KC_MOUSE_LEFT  = 0xEE, /*!< Mouse left button */
      KC_MOUSE_RIGHT = 0xEF, /*!< Mouse right button */
      KC_MOUSE_MIDDLE= 0xF0, /*!< Mouse middle button */
      KC_MOUSE_MOVE  = 0xF1, /*!< Mouse move event */

      KC_MENU        = 0xF2, /*!< Both alt modifiers */
      KC_WIN         = 0xF3, /*!< Both win modifiers */
      KC_CONTROL     = 0xF4, /*!< Control modifiers, both */
      KC_SHIFT       = 0xF5, /*!< Shift modifiers, both of them */

      KC_MAX         = 0xF6  /*!< Count of key codes */
    };

    /*! TriggerType indicates possible ways of triggering an action. Possibilities are
        'when a key is held down', 'when a key is just pushed in' and 'when a key is went up'. */
    enum TriggerType
    {
      OnKeyDown,      /*!< When a key is held down */
      OnKeyPress,     /*!< When a key is just pushed down */
      OnKeyUp,        /*!< When a key has went up */
      UnknownTrigger  /*!< Illegal trigger type */
    };

    /*! SingleKey is a class for handling conversions between key codes
        and their string names. Could be written as static methods only,
        but needs initialization. Singleton pattern suits this kinda job.
    */
    class _DLLExportControl SingleKey : public Singleton<SingleKey>
    {
      private:
        /*! Type definition for mapping between code keys and their names */
        typedef std::map<KeyCodeT, std::string> KeyCodeMapping;

        KeyCodeMapping     mMappings; /*!< Mapping of key codes and strings */

        void               initializeMapping(                         );

                           SingleKey        (                         );
      public:
        static SingleKey * getSingleton     (                         );
        static void        freeSingleton    (                         );

        KeyCodeT           fromString       ( const std::string & str );
        std::string        toString         ( const KeyCodeT & code   );
    };

    /*! This class holds a combination of keys with a trigger type. Provides methods
        for different needed utilities on a key combination. */
    class _DLLExportControl KeyCombination
    {
      private:
        /*! KeyList is a list of key codes */
        typedef std::vector<KeyCodeT> KeyList;

        KeyList               mKeys;         /*!< All the keys in the combination */
        TriggerType           mTriggerType;  /*!< Trigger type of the combination */

      public:
                              KeyCombination ( const KeyCombination & keys   );
                              KeyCombination ( TriggerType trig =
                                               UnknownTrigger,
                                               KeyCodeT key1 = KC_UNASSIGNED,
                                               KeyCodeT key2 = KC_UNASSIGNED,
                                               KeyCodeT key3 = KC_UNASSIGNED );

        void                  clear          (                               );
        size_t                size           (                               ) const;
        bool                  empty          (                               ) const;
        void                  addKey         ( const KeyCodeT & key          );
        bool                  removeKey      ( KeyCodeT key                  );

        bool                  isSubSetOf     ( const KeyCombination & keys   ) const;
        bool                  hasKey         ( const KeyCodeT & kc           ) const;

        TriggerType           getTriggerType (                               ) const;
        void                  setTriggerType ( TriggerType trig              );

        KeyCombination      & operator =     ( const KeyCombination & keys   );
        bool                  operator ==    ( const KeyCombination & keys   ) const;
        KeyCodeT&             operator []    ( const size_t & i              );
        const KeyCodeT        operator []    ( const size_t & i              ) const;

        /**
          Overloaded version of writing to output streams. Writes the contents of the KeyList to the given output stream
          @param os Output stream to write the KeyList to
          @param keys KeyList instance to write it's keys
          @return Output stream instance to enable recursive use
        */
        friend std::ostream & operator <<    ( std::ostream & os,
                                               const KeyCombination & keys   );

        static KeyCombination fromString     ( std::string str               );
        static std::string    toString       ( const KeyCombination & keys   );

        /*! List is a dynamic array of Key Combinations */
        typedef std::vector<KeyCombination> List;

        /*! Enables KeyBindings use it's private members */
        friend class KeyBindingTree;
    };


  }; // end namespace Input

}; // end namespace Klaus


#endif // KLAUS_KEYS
