/*
*********************************************************************************
*            GUIPlugin.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/11/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base of all plugins for GUI system                                 *
*                                                                               *
*********************************************************************************
*/

/*! \file GUIPlugin.h
<pre>
<b>File:</b>          GUIPlugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base of all plugins for GUI system
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef PLUGIN_SYSTEM_GUI_PLUGIN
#define PLUGIN_SYSTEM_GUI_PLUGIN

#include <PluginSystem/PluginBase.h>  // needed for plugin
#include <InputSystem/KeyBindings.h>  // needed for KeyCodeT
#include <GUISystem/Element.h>        // needed for GUI::Element


namespace Klaus
{

  namespace GUI
  {

    /**
     * This class is a base for creating UI plugins. Any UI plugin which shall
       be registered into application should derive from this class and implement
       it's abstract methods.
     */
    class _DLLExport GUIPlugin : public Plugin
    {
      public:
        /**
         * This method initializes the UI plugin by giving render window and scene manager instance to it.
         * @return True if plugin could be initialized, false otherwise
         */
        virtual bool initialize() = 0;

        /**
         * This method returns the UI node which currently the mouse is on
         * @return The node instance which mouse is on it. If there was no node under mouse cursor, it will return NULL
         */
        virtual Element * getNodeContainingMouse() = 0;

        /**
         * This method returns the node which has input focus and all keyboard peripheral input shall go into it.
         * @return Active node
         */
        virtual Element * getActiveNode() = 0;

        /**
         * This method sets visibility of the mouse cursor on screen
         * @param visible Whether to show the mouse cursor or hide it
         */
        virtual void setCursorVisible(bool visible) = 0;

        /**
         * This method injects the input position of mouse to the UI system to update cursor and it's needed information
         * @param x The x coordinate of the mouse
         * @param y The y coordinate of the mouse
         */
        virtual void injectMousePosition(const int & x, const int & y) = 0;

        /**
         * This method will inject a key down event of keyboard input data
         * @param key Code of the key which was pushed down
         */
        virtual void injectKeyDown(const Klaus::Input::KeyCodeT & key) = 0;

        /**
         * This method will inject a key up event of keyboard input data
         * @param key Code of the key which went up
         */
        virtual void injectKeyUp(const Klaus::Input::KeyCodeT & key) = 0;
    };

  }; // end namespace GUI

}; // end namespace Klaus


#endif // PLUGIN_SYSTEM_GUI_PLUGIN
