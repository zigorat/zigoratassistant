/*
*********************************************************************************
*            GUISystem.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: GUI system interface                                               *
*                                                                               *
*********************************************************************************
*/

/*! \file GUISystem.cpp
<pre>
<b>File:</b>          GUISystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      GUI system interface of application
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#include "GUISystem.h"
#include <PluginSystem/PluginSystem.h>
#include <GraphicSystem/GraphicSystem.h>

/** GUISystem singleton instance */
template<> Klaus::GUI::GUISystem * Klaus::Singleton<Klaus::GUI::GUISystem>::mInstance = 0;


/**
  Constructor of GUISystem. Initializes KeyBindings instance and
  sets input plugin to uninitialized.
*/
Klaus::GUI::GUISystem::GUISystem()
{
  mPlugin = NULL;
  mLog = Logger::getSingleton();

  setupConfig("GUISystem");
  setRecipientName("GUISystem");

  // register for resize event
  EV_RESIZE = registerEvent("MainWindow\\Resized");
}

/**
  Destructor of GUISystem
*/
Klaus::GUI::GUISystem::~GUISystem()
{
  deletePlugin();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::GUI::GUISystem * Klaus::GUI::GUISystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new GUISystem;
    Logger::getSingleton()->log(1, "(GUISystem) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::GUI::GUISystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(GUISystem) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(GUISystem) removed singleton");
  }
}

/**
  This method deletes the GUI plugin (if loaded)
*/
void Klaus::GUI::GUISystem::deletePlugin()
{
  if(mPlugin)
    mPluginFactory->freePluginInstance(mPlugin);

  mPlugin = NULL;

  // mPluginFactory will not be freed here.
  // It will be freed in PluginSystem's destructor
}

/**
  This method loads all default configuration for GUISystem
*/
void Klaus::GUI::GUISystem::_loadDefaultConfig()
{
  mPluginToLoad = "CEGUI";
  mFallBackPlugins = true;
}

/**
  This method reads GUI system's configuration from a stream. If configurations were read
  successfully then this system will reinitialize itself with the new values.
  @param type The configuration type of the steam
  @return True if all configurations were successfully read, false otherwise
*/
bool Klaus::GUI::GUISystem::_readConfig(const Config::ConfigType & type)
{
  try
  {
    if(type == Config::BinaryConfig)
    {
      mPluginToLoad = readString();
      mFallBackPlugins = readBool();
    }
    else if(type == Config::SExpConfig)
    {
      SExpression expr;
      if(!readSExpression(expr))
        return false;

      mPluginToLoad = expr["loadPlugin"][1].str();
      mFallBackPlugins = expr["useFallbackPlugins"][1].boolean();
    }
    else
    {
      mLog->log(5, "(GUISystem) read: configuration type %d not supported", type);
      return false;
    }
  }
  catch(const char * strError)
  {
    mLog->log(5, "(GUISystem) exception: %s", strError);
    return false;
  }

  // shall not initialized the system
  // this shall be done after all other systems have been initialized
  return true;
}

/**
  This method writes the systems configuration to the given output stream
  @param type The configuration type to save the stream
*/
void Klaus::GUI::GUISystem::_writeConfig(const Config::ConfigType & type)
{
  if(type == Config::BinaryConfig)
  {
    writeString(mPluginToLoad);
    writeBool(mFallBackPlugins);
  }
  else if(type == Config::SExpConfig)
  {
    writeText("(loadPlugin %s)", mPluginToLoad.c_str());

    if(mFallBackPlugins)
      writeText("(useFallbackPlugins true)");
    else
      writeText("(useFallbackPlugins false)");
  }
  else
    mLog->log(5, "(GUISystem) write: configuration type %d not supported", type);
}

/**
  This method create's the UI plugin. GUI plugin will be created according
  to the configuration of the system.
  @return True if a plugin is loaded, false otherwise
*/
bool Klaus::GUI::GUISystem::setupUI()
{
  if(mPlugin && mPluginFactory->getName() == mPluginToLoad)
    return true;

  deletePlugin();

  StringArray plugins;

  if(mFallBackPlugins)
    PluginSystem::getSingleton()->findAllPluginsByRole(plugins, "GUIPlugin");
  plugins.insert(plugins.begin(), mPluginToLoad);

  for(size_t i = 0; i < plugins.size(); i++)
  {
    mPluginFactory = PluginSystem::getSingleton()->findPlugin(plugins[i], "GUIPlugin");
    if(!mPluginFactory)
      continue;

    Plugin * raw_plugin = mPluginFactory->createPluginInstance();
    if(!raw_plugin)
      continue;

    mPlugin = dynamic_cast<GUIPlugin*>(raw_plugin);
    if(!mPlugin)
    {
      mPluginFactory->freePluginInstance(raw_plugin);
      continue;
    }

    if(!mPlugin->initialize())
    {
      mPluginFactory->freePluginInstance(mPlugin);
      mPlugin = NULL;
      mPluginFactory = NULL;
      continue;
    }

    // set initial size
    Klaus::Rect r = Graphics::GraphicConfig::getSingleton()->getWindowExtents();
    mWinLength = Round(r.getLength());
    mWinWidth = Round(r.getWidth());
    Logger::getSingleton()->log(5, "(GUISystem) loaded plugin %s", mPluginFactory->getName());
    return true;
  }

  mPlugin = NULL;
  mPluginFactory = NULL;
  mLog->log(5, "(GUISystem) could not load any GUI plugin");
  return false;
}

/**
  This method returns the UI node which currently the mouse is on
  @return The node instance which mouse is on it. If there was no node under mouse cursor, it will return NULL
*/
Klaus::GUI::Element * Klaus::GUI::GUISystem::getNodeContainingMouse()
{
  if(mPlugin)
    return mPlugin->getNodeContainingMouse();

  return NULL;
}

/**
  This method returns the node which has input focus and all keyboard peripheral input shall go into it.
  @return Active node
*/
Klaus::GUI::Element* Klaus::GUI::GUISystem::getActiveNode()
{
  if(mPlugin)
    return mPlugin->getActiveNode();

  return NULL;
}

/**
  This method injects the input position of mouse to the UI system to update cursor and it's needed information
  @param x The x coordinate of the mouse
  @param y The y coordinate of the mouse
*/
void Klaus::GUI::GUISystem::injectMousePosition( const int & x, const int & y )
{
  if(mPlugin)
  {
    // inject the mouse position
    mPlugin->injectMousePosition(x, y);

    // hide the cursor if it has left application's window
    VecPosition p(x, y);
    p += Input::InputSystem::getSingleton()->getMouseDisplace();

    if((p[0] <= 0) || (p[0] >= (mWinLength - 10)) || (p[1] <= 0) || (p[1] >= (mWinWidth - 10)))
      mPlugin->setCursorVisible(false);
    else
      mPlugin->setCursorVisible(true);
  }
}

/**
  This method will inject a key down event of keyboard input to the UI plugin
  @param key Code of the key which was pushed down
*/
void Klaus::GUI::GUISystem::injectKeyDown( const Input::KeyCodeT & key )
{
  if(mPlugin)
    mPlugin->injectKeyDown(key);
}

/**
  This method will inject a key up event of keyboard input to the UI plugin
  @param key Code of the key which went up
*/
void Klaus::GUI::GUISystem::injectKeyUp( const Input::KeyCodeT & key )
{
  if(mPlugin)
    mPlugin->injectKeyUp(key);
}

/**
  This method processes events sent to this system
  @param ev The event to process
*/
void Klaus::GUI::GUISystem::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_RESIZE)
  {
    mWinLength = ev.mArguments.getAsInt("Width");
    mWinWidth = ev.mArguments.getAsInt("Height");
  }
  else
    mLog->log(5, "(GUISystem) recieved unknown event %s",
              Events::EventSystem::getSingleton()->getEventName(ev.mEventID).c_str());
}
