/*
*********************************************************************************
*             Element.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 11/11/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains decleration of Nodes for GUI systems. These will hold     *
*            information of UI elements in applicatoin.                         *
*                                                                               *
*********************************************************************************
*/

/*! \file Element.h
<pre>
<b>File:</b>          Element.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains decleration of Nodes for GUI systems. These will hold
            information of UI elements in applicatoin.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2008       Mahdi           Initial version created
</pre>
*/

#ifndef GUI_ELEMENTS
#define GUI_ELEMENTS


#include <libZigorat/Algebra.h>  // needed for Klaus::Rect
#include <CoreSystem/Types.h>    // needed for UINodeType
#include <EventSystem/Event.h>   // needed for EventID
#include <libZigorat/SLang.h>    // needed for SExpression
#include <libZigorat/Logger.h>   // needed for Logger


#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  namespace GUI
  {

    /*! This class holds information about parts of a UI scene. It is capable to read
        all its needed information from a S-Lang scene graph. The structure of UI scenes
        is a tree containing UI nodes, Each node has a number of information associated
        with it and may contain children nodes.
        These information are the stored for each node:
          - Type  of node (mandatory)
          - Name of the node (optional)
          - Caption of the node (optional)
          - Picture if the component type is capable
          - Event ID if the component is associated with a command
          - Extents of the component (mandatory)
          - Modality (optional)

        This class is used as a base for using UI elements in application, thus allowing
        different graphical interface systems be used within the application.

        @note There is no comment allowed in the scene graph containing the ui elements. This
              is set so that parsing the scene graph is done easier with cleaner code

        Example of such nodes:
        (Node Button
          (setName btnOpenLog)
          (setCaption 'Click Me!')
          (setExtents 10 10 100 100)
          (setAlign Center Center)
          (setCommand cmdOpenLog)
          (setPicture '/home/Klaus/foo.png')
        )
    */
    class _DLLExportControl Element
    {
      public:
        /*! Array is a dynamic array of UI nodes */
        typedef std::vector<Element*> Array;

      protected:
        std::string              mName;        /*!< Name of the component */
        UIType                   mType;        /*!< Type of the component */
        Klaus::Rect              mExtents;     /*!< Extents: size and position */
        std::string              mCaption;     /*!< Component's Caption */
        std::string              mPicture;     /*!< Picture to show */
        bool                     mModal;       /*!< Modality */
        bool                     mVisible;     /*!< Visibility */
        Element                * mParent;      /*!< Parent of the node */
        HorizontalAlignment      mHAlign;      /*!< Horizontal alignment */
        VerticalAlignment        mVAlign;      /*!< Vertical alignment */
        bool                     mHasBorder;   /*!< Window has border */
        UIEvents                 mEvents;      /*!< Command type to exec */
        Logger                 * mLog;         /*!< Logger instance */

        Array                    mChildren;    /*!< Children of this node */

        void                     clear         (                         );
        int                      addNode       ( SToken token            );

      public:
                                 Element       (                         );
        virtual                ~ Element       (                         );

        std::string              getName       (                         ) const;
        std::string              getUniqueName (                         ) const;
        UIType                   getType       (                         ) const;
        Klaus::Rect              getExtents    (                         ) const;
        std::string              getCaption    (                         ) const;
        std::string              getPictureName(                         ) const;
        bool                     isModal       (                         ) const;
        bool                     isVisible     (                         ) const;
        Element                * getParent     (                         ) const;
        HorizontalAlignment      getHAlignment (                         ) const;
        VerticalAlignment        getVAlignment (                         ) const;
        bool                     hasBorder     (                         ) const;

        UIEvents::const_iterator eventsStart   (                         ) const;
        UIEvents::const_iterator eventsEnd     (                         ) const;
        std::string              getEvent      ( EventTriggerType event  ) const;

        size_t                   getChildCount (                         ) const;
        Element                * getChild      ( size_t index            );

        bool                     parse         ( SToken token            );

        static std::string       getTypeStr    ( const UIType & type     );
        static UIType            getTypeFromStr( const std::string & str );


        static std::string       getTriggerTypeStr    ( const EventTriggerType & t );
        static EventTriggerType  getTriggerTypeFromStr( const std::string & str    );
    };

  }; // end namespace GUI

}; // end namespace Klaus


#endif // GUI_ELEMENTS
