/*
*********************************************************************************
*             Factory.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 04/18/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Forms (Windows) on UI.                                             *
*                                                                               *
*********************************************************************************
*/

/*! \file Factory.cpp
<pre>
<b>File:</b>          Factory.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       04/18/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Forms (Windows) on UI.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
04/18/2011       Mahdi           Initial version created
</pre>
*/


#include "Factory.h"

/*! FactoryManager singleton instance */
template<> Klaus::GUI::FactoryManager* Klaus::Singleton<Klaus::GUI::FactoryManager>::mInstance = 0;

/**
  This is the constructor for the FactoryManager class.
*/
Klaus::GUI::FactoryManager::FactoryManager()
{
}

/**
  Destructor of FactoryManager.
*/
Klaus::GUI::FactoryManager::~FactoryManager()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::GUI::FactoryManager * Klaus::GUI::FactoryManager::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new FactoryManager;
    Logger::getSingleton()->log(1, "(FactoryManager) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::GUI::FactoryManager::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(FactoryManager) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(FactoryManager) removed singleton");
  }
}

/**
  This method searches all registered factories for an instace of a parser
  capable of parsing the given type of UI element.
  @param strType Type of the UI element
  @return Instance of a parser for the element
*/
Klaus::GUI::Element* Klaus::GUI::FactoryManager::getInstanceFor(const std::string& strType)
{
  for(size_t i = 0; i < mFactories.size(); ++i)
  {
    Element * e = mFactories[i]->getInstanceFor(strType);
    if(e)
      return e;
  }

  return new Element;
}
