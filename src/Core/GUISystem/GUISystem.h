/*
*********************************************************************************
*            GUISystem.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: GUI system interface                                               *
*                                                                               *
*********************************************************************************
*/

/*! \file GUISystem.h
<pre>
<b>File:</b>          GUISystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      GUI system interface of application
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef GUI_SYSTEM
#define GUI_SYSTEM

#include "GUIPlugin.h"                // needed for GUIPlugin
#include <InputSystem/InputSystem.h>  // needed for Types, EventSystem and Configurable

namespace Klaus
{

  namespace GUI
  {

    /*! GUI system handles GUI affairs. It's main job is to initialize a plugin to
        handle UI elements on application. It will pass input events related to the
        application user interface to the plugin and will recieve input events of UI
        system and send it to the application's core for handling.
    */
    class _DLLExportControl GUISystem: public Singleton<GUISystem>,
                                       public Events::EventRecipient,
                                       public Config::Configurable
    {
      private:
        // events
        EventID            EV_RESIZE;            /*!< Main window resize event     */

        // members
        GUIPlugin        * mPlugin;              /*!< GUI plugin instance          */
        PluginFactory    * mPluginFactory;       /*!< PluginFactory instance       */
        Logger           * mLog;                 /*!< Logger instance              */
        unsigned           mWinLength;           /*!< Current length of window     */
        unsigned           mWinWidth;            /*!< Current width of window      */
        std::string        mPluginToLoad;        /*!< GUI Plugin to load           */
        bool               mFallBackPlugins;     /*!< Whether fall back to plugins */

        void               deletePlugin          (                                 );

        // config handler (from Configurable)
        virtual void       _loadDefaultConfig    (                                 );
        virtual bool       _readConfig           ( const Config::ConfigType & type );
        virtual void       _writeConfig          ( const Config::ConfigType & type );

                           GUISystem             (                                 );
                         ~ GUISystem             (                                 );

      public:
        static GUISystem * getSingleton          (                                 );
        static void        freeSingleton         (                                 );

        // Initialization
        bool               setupUI               (                                 );

        // Utility methods
        Element          * getNodeContainingMouse(                                 );
        Element          * getActiveNode         (                                 );

        // Inject peripheral input
        void               injectMousePosition   ( const int & x, const int & y    );
        void               injectKeyDown         ( const Input::KeyCodeT & key     );
        void               injectKeyUp           ( const Input::KeyCodeT & key     );

        // event handler (from EventRecipient)
        virtual void       processEvent          ( const Events::Event & ev        );
    };

  }; // end namespace GUI

}; // end namespace Klaus

#endif // GUI_SYSTEM
