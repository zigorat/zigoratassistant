/*
*********************************************************************************
*            Element.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/11/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Contains definition of Nodes for GUI systems. These will hold      *
*            information of UI elements in applicatoin.                         *
*                                                                               *
*********************************************************************************
*/

/*! \file Element.cpp
<pre>
<b>File:</b>          Element.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Contains definition of Nodes for GUI systems. These will hold
            information of UI elements in applicatoin.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2008       Mahdi           Initial version created
</pre>
*/

#include "Element.h"
#include "Factory.h"  // needed for Klaus::GUI::FactoryManager

/**
  Constructor of Element. Initializes default values
*/
Klaus::GUI::Element::Element()
{
  mLog       = Logger::getSingleton();
  mVisible   = true;
  mModal     = false;
  mType      = Unknown;
  mParent    = NULL;
  mHAlign    = HAlignLeft;
  mVAlign    = VAlignTop;
  mHasBorder = true;
}

/**
  Destructor of Element. Removes all children.
*/
Klaus::GUI::Element::~Element()
{
  clear();
}

/**
  This method clears the node by removing all of its children.
*/
void Klaus::GUI::Element::clear()
{
  for(size_t i = 0; i < mChildren.size(); ++i)
    delete mChildren[i];

  mChildren.clear();
}

/**
  This method adds a node to the childrens node
  @param token The token containing the information of the node
  @return Number of errors occured
*/
int Klaus::GUI::Element::addNode(Klaus::SToken token)
{
  if(token.size() < 3 || token[0].isAtom() == false)
  {
    mLog->log(43, "(Node) invalid UI element node: '%s'", token.str().c_str());
    return 1;
  }

  Element * nd = FactoryManager::getSingleton()->getInstanceFor(token[0]);
  int iErrors = nd->parse(token);

  if(nd->mType != Unknown)
    mChildren.push_back(nd);
  else
    delete nd;

  return iErrors;
}

/**
  This method returns the name of the node
  @return The name of the node
*/
std::string Klaus::GUI::Element::getName() const
{
  return mName;
}

/**
  This method creates a unique name for the node. The name is created by
  using the name of the node's parents, so there shall be no two ui
  elements with the same name in one element.
  @return A nearly unique name for the UI element
*/
std::string Klaus::GUI::Element::getUniqueName() const
{
  if(mParent)
    return mParent->getUniqueName() + '/' + mName;
  else
    return mName;
}

/**
  This method returns the type of the node.
  @return Type of the node
*/
Klaus::GUI::UIType Klaus::GUI::Element::getType() const
{
  return mType;
}

/**
  This method returns the position and size of the node
  @return Left-Top position of the node and it's size as a rectangle
*/
Klaus::Rect Klaus::GUI::Element::getExtents() const
{
  return mExtents;
}

/**
  This method returns the caption of the node
  @return Caption of the UI element
*/
std::string Klaus::GUI::Element::getCaption() const
{
  return mCaption;
}

/**
  This method returns the picture name of the node
  @return Picture name of the node
*/
std::string Klaus::GUI::Element::getPictureName() const
{
  return mPicture;
}

/**
  This method returns whether the element is a modal one or not
  @return Boolean value indicating whether the element is modal or not
*/
bool Klaus::GUI::Element::isModal() const
{
  return mModal;
}

/**
  This method returns whether the element is visible or not
  @return Whether the element is visible or not
*/
bool Klaus::GUI::Element::isVisible() const
{
  return mVisible;
}

/**
  This method returns the parent of the node
  @return Parent element of the node
*/
Klaus::GUI::Element* Klaus::GUI::Element::getParent() const
{
  return mParent;
}

/**
  This method returns horizontal alignment of the node
  @return Value indicates horizontal alignment of the node
*/
Klaus::GUI::HorizontalAlignment Klaus::GUI::Element::getHAlignment() const
{
  return mHAlign;
}

/**
  This method returns vertical alignment of the node
  @return vertical alignment of the node
*/
Klaus::GUI::VerticalAlignment Klaus::GUI::Element::getVAlignment() const
{
  return mVAlign;
}

/**
  This method returns whether the element shall be with border or without it
  @return Element pocesses border or not
*/
bool Klaus::GUI::Element::hasBorder() const
{
  return mHasBorder;
}

/**
  This method returns iterator object pointing to the first trigger_type - event_id
  pair of the events for the element
  @return First pair event of the element from its list of events
*/
Klaus::GUI::UIEvents::const_iterator Klaus::GUI::Element::eventsStart() const
{
  return mEvents.begin();
}

/**
  This method returns iterator object pointing to the one past last trigger_type - event_id
  pair of the events for the element
  @return One past last pair event of the element from its list of events
*/
Klaus::GUI::UIEvents::const_iterator Klaus::GUI::Element::eventsEnd() const
{
  return mEvents.end();
}

/**
  This method finds the requested event in the event list of the element
  and returns its event name string
  @param event The event type to get its name
  @return Name of the requested event type
*/
std::string Klaus::GUI::Element::getEvent(Klaus::GUI::EventTriggerType event) const
{
  UIEvents::const_iterator it = mEvents.find(event);
  return it == mEvents.end() ? "" : it->second;
}

/**
  This method returns the number of children this element has
  @return Count of the children for the element
*/
size_t Klaus::GUI::Element::getChildCount() const
{
  return mChildren.size();
}

/**
  This method returns the specified child instance
  @param index The index of the child
  @return instance of the specified child
*/
Klaus::GUI::Element* Klaus::GUI::Element::getChild(size_t index)
{
  return index < mChildren.size() ? mChildren[index] : NULL;
}

/**
  This method parses the given S-Expression and extracts the information the node from the expression.
  @param token The token containing the information
  @return True on successful parse, false otherwise
*/
bool Klaus::GUI::Element::parse(SToken token)
{
  // (TextBox 'txtName'
  mType = getTypeFromStr(token[0]);
  if(mType == Unknown)
  {
    mLog->log(43, "(Node) unknown type in '%s'", token.str().c_str());
    return false;
  }

  mName = token[1].str();

  for(size_t i = 2; i < token.size(); ++i)
  {
    size_t size = token[i].size();
    if(size > 0)
    {
      std::string str = token[i][0];

      // Example: (setCaption 'Hello World!')
      if(str == "setCaption" && size == 2)
        mCaption = token[i][1].str();

      // Example: (setExtents 10 10 50 21)
      else if(str == "setExtents" && size == 5)
      {
        mExtents.setPosLeftTop(VecPosition(token[i][1], token[i][2]));
        mExtents.setLength(token[i][3]);
        mExtents.setWidth(token[i][4]);
      }

      // Example: (setAlign Left Bottom)
      else if(str == "setAlign" && size == 3)
      {
        std::string s1 = token[i][1];
        std::string s2 = token[i][2];

        if(s1 == "Right")
          mHAlign = HAlignRight;
        else if(s1 == "Center")
          mHAlign = HAlignCenter;
        else
          mHAlign = HAlignLeft;

        if(s2 == "Bottom")
          mVAlign = VAlignBottom;
        else if(s2 == "Center")
          mVAlign = VAlignCenter;
        else
          mVAlign = VAlignTop;
      }

      // Example: (setPicture '/home/1.jpg')
      else if(str == "setPicture" && size == 2)
        mPicture = token[i][1].str();

      // Example: (setModal)
      else if(str == "setModal" && size == 1)
        mModal = true;

      // Example: (noBorder)
      else if(str == "noBorder" && size == 1)
        mHasBorder = false;

      // Example: (hide)
      else if(str == "hide" && size == 1)
        mVisible = false;

      // Example: (event OnClick 'SimSpark\Disconnect')
      else if(str == "event" && size == 3)
      {
        EventTriggerType t = getTriggerTypeFromStr(token[i][1]);
        if(t == UnknownEventTrigger)
          mLog->log(43, "(Element) unknown event trigger type: '%s'", token[i][1].str().c_str());
        else
          mEvents[t] = token[i][2].str();
      }

      else
        addNode(token[i]);
    }
  }

  return true;
}

/**
  string representations of UI element types
*/
std::string ElementTypes[] =
{
  "Window",
  "Button",
  "TextBox",
  "ComboBox",
  "Label",
  "MenuBar",
  "MenuItem",
  "Memo",
  "'Unknown UI Element'"
};

/**
  This method converts the given UI element type to human readable string
  @param type The type to convert into string
  @return string representation of the ui element type
*/
std::string Klaus::GUI::Element::getTypeStr(const Klaus::GUI::UIType& type)
{
  return ElementTypes[type];
}

/**
  This method extracts a ui element type from the given string
  @param str The string containing the ui element type
  @return Type of the UI element in the string
*/
Klaus::GUI::UIType Klaus::GUI::Element::getTypeFromStr(const std::string& str)
{
  for(size_t i = 0; i < Unknown; ++i)
    if(str == ElementTypes[i])
      return (UIType)i;

  return Unknown;
}

/**
  Event trigger type. Contains the event trigger type string representations
*/
std::string TriggerTypeStr[] =
{
  "OnMouseDown",
  "OnMouseClick",
  "OnMouseMove",
  "OnMouseUp",
  "OnKeyDown",
  "OnKeyPress",
  "OnKeyUp",
  "OnGainFocus",
  "OnLostFocus",
  "UnknownEvent",
};

/**
  This method gets an event trigger type and returns its string representation
  @param t The event trigger type to get its string representation
  @return String representation of the given event trigger type
*/
std::string Klaus::GUI::Element::getTriggerTypeStr(const Klaus::GUI::EventTriggerType& t)
{
  return TriggerTypeStr[t];
}

/**
  This method reads an event trigger type from a given string
  @param str The string containing the event trigger type
  @return The event trigger type in the string
*/
Klaus::GUI::EventTriggerType Klaus::GUI::Element::getTriggerTypeFromStr(const std::string& str)
{
  for(size_t i = 0; i < UnknownEventTrigger; ++i)
    if(str == TriggerTypeStr[i])
      return (EventTriggerType)i;

  return UnknownEventTrigger;
}
