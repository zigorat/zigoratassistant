/*
*********************************************************************************
*              Factory.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 04/18/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Forms (Windows) on UI.                                             *
*                                                                               *
*********************************************************************************
*/

/*! \file Factory.h
<pre>
<b>File:</b>          Factory.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       04/18/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Forms (Windows) on UI.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
04/18/2011       Mahdi           Initial version created
</pre>
*/

#ifndef GUI_FACTORY
#define GUI_FACTORY

#include "Element.h"

namespace Klaus
{

  namespace GUI
  {


    /**
      This class is the base factory for creating UI elements. Derived classes shall
      implement its behaviour by implementing getInstanceFor()
    */
    class _DLLExportControl Factory
    {
      public:
        /**
          This method is the core of creating parsers for UI elements. According
          to the given type of the UI node a valid object shall be created.
          @param strType Type of the node to create a parser instance for
          @return Instance of a parser for the given type of UI element
        */
        virtual Element * getInstanceFor( const std::string & strType ) = 0;

        /*! Array is a dynamic array of Factories*/
        typedef std::vector<Factory*> Array;
    };


    /**
      The manager for all factories. Instance of all factories shall be added to this manager.
      @note Whoever adds a factory instance shall take care of disposing it. Instaces are not cleared by destructor.
    */
    class _DLLExportControl FactoryManager : public Singleton<FactoryManager>
    {
      private:
        Factory::Array          mFactories;   /*!< All factory instances */

                                FactoryManager(                             );
                              ~ FactoryManager(                             );
      public:
        static FactoryManager * getSingleton  (                             );
        static void             freeSingleton (                             );

        Element               * getInstanceFor( const std::string & strType );
    };


  }; // end namespace GUI

};  // end namespace Klaus


#endif // GUI_FACTORY
