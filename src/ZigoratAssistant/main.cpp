/*
*********************************************************************************
*             main.cpp : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the core of project. It links every other part to other    *
*            and executes the program.                                          *
*                                                                               *
*********************************************************************************
*/

/*! \file ZigoratAssistant/main.cpp
<pre>
<b>File:</b>          main.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the main part of ZigoratAssistant which links and executes all the parts
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;

#include <libZigorat/Parse.h>                // needed to parse command line arguments
#include <CoreSystem/CoreSystem.h>           // needed for CoreSystem
#include <libZigorat/ApplicationSettings.h>  // needed for ApplicationSettings
#include "DefaultConfig.h"                   // needed for default configuration

string strHost = "127.0.0.1";       /*!< Host to connect to */
int    iPort   = 3200;              /*!< Port to which agent is connecting */

bool ConfigureArguments(int argc, char* argv[]);
void PrintOptions();

#define EXEC_MAIN      /*!< To Debug other parts, undef this */

/**
  This is the main function and creates and links all the different classes.
  First it reads in all the parameters from the command prompt and uses
  these values to create the classes. After all the classes are linked,
  the program starts execution.
  @param argc Number of command line arguments given to application
  @param argv Argument strings passed to application
  @return Error code of application execution
*/
#ifdef EXEC_MAIN
int main(int argc, char* argv[])
{
  using namespace Klaus::Core;

  if(!ConfigureArguments(argc, argv))
    return 1;

  if(!CoreSystem::getSingleton()->initialize())
    cerr << "(main) init failed" << endl;
  else
    CoreSystem::getSingleton()->run();

  CoreSystem::getSingleton()->freeSingleton();

  return 0;
}
#endif // EXEC_MAIN

/**
  Read in all the command options and change the associated variables
  in case of illegal options the agent doesn't start.
  @param argc Argument count of application
  @param argv Arguments of application
  @return bool indicating whether command line arguments were parsed successfully
*/
bool ConfigureArguments(int argc, char* argv[])
{
  char * str;
  for( int i = 1 ; i < argc ; i = i + 2  )
  {
    // help is only option that does not have to have an argument
    if( i + 1 >= argc && strncmp( argv[i], "-help", 3 ) != 0 )
    {
      cout << "Need argument for option: " << argv[i] << endl;
      exit( 0 );
    }
    // read a command option
    if( argv[i][0] == '-' && strlen( argv[i] ) > 1)
    {
      switch( argv[i][1] )
      {
        case 'h': // host server
            if( strlen( argv[i] ) > 2 && argv[i][2] == 'e' )
              PrintOptions();
            else
              strHost = argv[i+1];
          break;

        case 'p': // port
            str = &argv[i+1][0];
            iPort = Parse::parseFirstInt( &str );
          break;

        default:
          PrintOptions();
      }
    }
  }

  // initialize ApplicationSettings
  Klaus::ApplicationSettings * app = Klaus::ApplicationSettings::getSingleton();
  app->initialize(DEFAULT_APPLICATION_NAME, DEFAULT_DATA_ROOT, DEFAULT_IS_DEBUG);

  return true;
}

/**
  Prints any options that can be configured from the command line
*/
void PrintOptions()
{
  cout << "usage: ZigoratAssistant [-param_name param_value]" << endl;
  cout << "                                             " << endl;
  cout << "  -host       : Specify the server host to  connect " << endl;
  cout << "  -port       : Specify the server port to  connect " << endl << endl;
}
