@echo off

REM /*
REM *********************************************************************************
REM *             build.sh : Robocup 3D Soccer Simulation Team Zigorat              *
REM *                                                                               *
REM *  Date: 01/24/2009                                                             *
REM *  Author: Mahdi Hamdarsi                                                       *
REM *  Comments: Simple script to regenerate build files                            *
REM *                                                                               *
REM *********************************************************************************
REM */

REM shall configure with cmake first
REM Get the desired build environment

cd ..\..\
md build
cd build
cmake .. -G "Visual Studio 9 2008"
PAUSE

