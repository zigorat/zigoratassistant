# *********************************************************************************
# *           CMakeLists.txt : Robocup 3D Soccer Simulation Team Zigorat          *
# *                                                                               *
# *  Date: 11/01/2009                                                             *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: ZigoratAssistant build configuration with cmake                    *
# *                                                                               *
# *********************************************************************************

# Configure scripts to assume the correct folder of project root
# ================================================================================

if(CMAKE_HOST_UNIX)
  set(project_dir "${CMAKE_SOURCE_DIR}")

  # distclean.sh
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/distclean.sh.in"
                 "${CMAKE_SOURCE_DIR}/scripts/distclean.sh"
                 @ONLY)

  # build.sh
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/build.sh"
                 "${CMAKE_SOURCE_DIR}/scripts/build.sh"
                 @ONLY)
else(CMAKE_HOST_WIN32)
  # distclean.sh
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/distclean.bat.in"
                 "${CMAKE_SOURCE_DIR}/scripts/distclean.bat"
                 @ONLY)

endif(CMAKE_HOST_UNIX)
