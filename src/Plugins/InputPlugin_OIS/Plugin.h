/*
*********************************************************************************
*              Plugin.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: OIS input plugin                                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file InputPlugin_OIS/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      OIS Input plugin
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef OIS_INPUT_PLUGIN
#define OIS_INPUT_PLUGIN

#include <InputSystem/InputSystem.h>
using namespace Klaus::Input;

//Properly including OIS
#define OIS_DYNAMIC_LIB
#include <OIS/OIS.h>


/*********************************************************************************/
/*****************************  Class InputPlugin_OIS  ***************************/
/*********************************************************************************/

/*! InputPlugin_OIS is an input plugin. It will create necessary input
    devices and inform input manager of percepted peripheral input events.
*/
class _DLLExport InputPlugin_OIS : public InputPlugin,
                                   public OIS::MouseListener,
                                   public OIS::KeyListener,
                                   public Klaus::Events::EventRecipient
{
  private:
    // Events
    Klaus::EventID     EV_RESIZED;        /*!< Resized event id             */

    //OIS Input devices
    OIS::InputManager* mInputManager;     /*!< Input manager system of OIS  */
    OIS::Mouse*        mMouse;            /*!< Mouse input device           */
    OIS::Keyboard*     mKeyboard;         /*!< Keyboard input device        */

    // Internal data
    Klaus::Logger    * mLog;              /*!< Logger instance              */
    InputSystem      * mSystem;           /*!< Input system instance        */

    bool               mouseMoved         ( const OIS::MouseEvent &arg      );
    bool               mousePressed       ( const OIS::MouseEvent &arg,
                                            OIS::MouseButtonID id           );
    bool               mouseReleased      ( const OIS::MouseEvent &arg,
                                            OIS::MouseButtonID id           );

    bool               keyPressed         ( const OIS::KeyEvent &arg        );
    bool               keyReleased        ( const OIS::KeyEvent &arg        );

    KeyCodeT           getMouseButton     ( const OIS::MouseButtonID & btn  );

  public:
                       InputPlugin_OIS    (                                 );
    virtual          ~ InputPlugin_OIS    (                                 );

    virtual bool       initialize         (                                 );
    virtual void       checkForInputEvents(                                 );
    virtual void       processEvent       ( const Klaus::Events::Event & ev );
};



/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_OIS is a factory to create instances of InputPlugin_OIS class. */
class _DLLExport PluginFactory_OIS: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_OIS() : Klaus::PluginFactory("OIS", "InputPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new InputPlugin_OIS;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_OIS;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}



#endif // OIS_INPUT_PLUGIN
