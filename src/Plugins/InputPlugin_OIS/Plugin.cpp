/*
*********************************************************************************
*             Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: OIS input plugin                                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file InputPlugin_OIS/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      OIS Input plugin
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>                       // convert window handle
#include <InputSystem/InputSystem.h>     // upload window events
#include <GraphicSystem/GraphicConfig.h> // needed to get window handle

using namespace std;


/**
 * Constructor of InputPlugin_OIS. Sets all the devices to NULL
 */
InputPlugin_OIS::InputPlugin_OIS()
{
  mInputManager = NULL;
  mMouse = NULL;
  mKeyboard = NULL;

  setRecipientName("OIS");
  EV_RESIZED = registerEvent("MainWindow\\Resized");
}

/**
 * Destroys OIS
 */
InputPlugin_OIS::~InputPlugin_OIS()
{
  if(mInputManager)
  {
    mInputManager->destroyInputObject(mMouse);
    mInputManager->destroyInputObject(mKeyboard);

    OIS::InputManager::destroyInputSystem(mInputManager);
    mInputManager = NULL;
  }
}

/**
 * Event call back for mouse moves
 * @param arg OIS data of mouse move event
 * @return Always true
 */
bool InputPlugin_OIS::mouseMoved( const OIS::MouseEvent &arg )
{
  Klaus::Input::InputEvent ev;
  ev.Type = Klaus::Input::EV_MOUSE_MOVE;
  ev.Data.MouseMove.X = arg.state.X.rel;
  ev.Data.MouseMove.Y = arg.state.Y.rel;
  ev.Data.MouseMove.AbsX = arg.state.X.abs;
  ev.Data.MouseMove.AbsY = arg.state.Y.abs;
  mSystem->recordInputEvent(ev);
  return true;
}

/**
 * Event call back for mouse button presses
 * @param arg OIS data of mouse button presses
 * @param id Mouse button name
 * @return Always true
 */
bool InputPlugin_OIS::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
  Klaus::Input::InputEvent ev;
  ev.Type = Klaus::Input::EV_MOUSE_DOWN;
  ev.Data.MouseButtonDown.Button = getMouseButton(id);
  ev.Data.MouseButtonDown.X = mMouse->getMouseState().X.abs;
  ev.Data.MouseButtonDown.Y = mMouse->getMouseState().Y.abs;
  mSystem->recordInputEvent(ev);
  return true;
}

/**
 * Event call back for mouse button releases
 * @param arg OIS data of mouse button releases
 * @param id Mouse button name
 * @return Always true
 */
bool InputPlugin_OIS::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
  Klaus::Input::InputEvent ev;
  ev.Type = Klaus::Input::EV_MOUSE_UP;
  ev.Data.MouseButtonUp.Button = getMouseButton(id);
  ev.Data.MouseButtonUp.X = mMouse->getMouseState().X.abs;
  ev.Data.MouseButtonUp.Y = mMouse->getMouseState().Y.abs;
  mSystem->recordInputEvent(ev);
  return true;
}

/**
 * Event handler of OIS keyboard event on key presses
 * @param arg OIS data of the event
 * @return Always true
 */
bool InputPlugin_OIS::keyPressed( const OIS::KeyEvent &arg )
{
  Klaus::Input::InputEvent ev;
  ev.Type = Klaus::Input::EV_KEY_DOWN;
  ev.Data.KeyDown.KeyCode = (Klaus::Input::KeyCodeT)arg.key;
  mSystem->recordInputEvent(ev);
  return true;
}

/**
 * Event handler of OIS keyboard event on key releases
 * @param arg OIS data of the event
 * @return Always true
 */
bool InputPlugin_OIS::keyReleased( const OIS::KeyEvent &arg )
{
  Klaus::Input::InputEvent ev;
  ev.Type = Klaus::Input::EV_KEY_UP;
  ev.Data.KeyUp.KeyCode = (Klaus::Input::KeyCodeT)arg.key;
  mSystem->recordInputEvent(ev);
  return true;
}

/**
 * This method converts mouse button names from OIS names to Klaus::Input names
 * @param btn Name of the button as OIS says
 * @return Name of the mouse button in Klaus::Input
 */
Klaus::Input::KeyCodeT InputPlugin_OIS::getMouseButton( const OIS::MouseButtonID & btn )
{
  switch(btn)
  {
    case OIS::MB_Left:
      return Klaus::Input::KC_MOUSE_LEFT;

    case OIS::MB_Right:
      return Klaus::Input::KC_MOUSE_RIGHT;

    case OIS::MB_Middle:
      return Klaus::Input::KC_MOUSE_MIDDLE;

    default:
      return Klaus::Input::KC_UNASSIGNED;
  }
}

/**
 * This method initialize the plugin and OIS system. It passes the created
   window handle and it's size information to OIS and creates input devices
   to get their events.
 * @return True on successfull initialization of OIS input devices, false otherwise
 */
bool InputPlugin_OIS::initialize()
{
  try
  {
    mLog = Klaus::Logger::getSingleton();
    mSystem = InputSystem::getSingleton();

    mLog->log(11, "(OIS) creating input system");

    // create a input manager for the active render window instance
    size_t hnd;
    Klaus::Graphics::GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow()->getCustomAttribute("WINDOW", &hnd);
    ostringstream windowHndStr;
    windowHndStr << hnd;
    OIS::ParamList pl;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    // setup input paramaters
    #if defined OIS_WIN32_PLATFORM
      // We want non-exclusively need the keyboard to get input when the application has focus only
      pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
      pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));

      // We want need the mouse to get input when the application has focus only
      pl.insert(std::make_pair(std::string("w32_mouse"),    std::string("DISCL_FOREGROUND" )));
      if(mSystem->getMouseGrab())
        pl.insert(std::make_pair(std::string("w32_mouse"),  std::string("DISCL_EXCLUSIVE")));
      else
        pl.insert(std::make_pair(std::string("w32_mouse"),  std::string("DISCL_NONEXCLUSIVE")));


    #elif defined OIS_LINUX_PLATFORM
      pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
      pl.insert(std::make_pair(std::string("XAutoRepeatOn"),     std::string("true")));
      pl.insert(std::make_pair(std::string("x11_mouse_hide"),    std::string("true")));

      if(mSystem->getMouseGrab())
        pl.insert(std::make_pair(std::string("x11_mouse_grab"),  std::string("true")));
      else
        pl.insert(std::make_pair(std::string("x11_mouse_grab"),  std::string("false")));
    #endif

    mInputManager = OIS::InputManager::createInputSystem(pl);

    // create keyboard input object
    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
    if(!mKeyboard)
    {
      mLog->log(11, "(OIS) could not acquire keyboard input");
      return false;
    }
    mKeyboard->setEventCallback(this);

    // create mouse input object
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));
    if(!mMouse)
    {
      mLog->log(11, "(OIS) could not acquire mouse input");
      return false;
    }
    mMouse->setEventCallback(this);

    //Set initial mouse clipping size
    const OIS::MouseState &ms = mMouse->getMouseState();
    Klaus::Rect r = Klaus::Graphics::GraphicConfig::getSingleton()->getWindowExtents();
    ms.width = Klaus::Round(r.getLength());
    ms.height = Klaus::Round(r.getWidth());

    // finished
    mLog->log(11, "(OIS) created input system");

    return true;
  }
  catch(OIS::Exception & e)
  {
    mLog->log(11, "(OIS) exception: %s", e.eText);
    cerr << "(OIS) exception: " << e.eText << endl;
    return false;
  }
}

/**
 * This method is the main contact point of input system and the input
   plugin. It will be called between rendered frames and the plugin
   shall capture and store any percepted events on the given array.
 */
void InputPlugin_OIS::checkForInputEvents()
{
  if(!mInputManager)
    return;

  if(mKeyboard)
    mKeyboard->capture();

  if(mMouse)
    mMouse->capture();
}

/**
 * This method process all the events which this system has registered itself to.
 * @param ev The event sent from the core
 */
void InputPlugin_OIS::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_RESIZED)
  {
    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = ev.mArguments.getAsInt("Width");
    ms.height = ev.mArguments.getAsInt("Height");
  }
}

