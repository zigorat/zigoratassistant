/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Fixed camera implementation as a camera agent                      *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FixedCamera/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Fixed camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CAMERA_PLUGIN_FIXED_CAMERA
#define CAMERA_PLUGIN_FIXED_CAMERA

#include <map>
#include <string>
using namespace std;

#include <DisplaySystem/CameraPlugin.h>  // Needed for CameraPlugin
#include <GraphicSystem/GraphicSystem.h> // Needed for SceneManager
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient

using namespace Klaus;


/******************************************************************************/
/***********************  Class CameraPlugin_FixedCamera **********************/
/******************************************************************************/

/*! Fixed camera is a type of camera which acts as if it's hardly attached to a
    place in environment and it's job is to look in the direction of a position
    of always look at a subject.
*/
class _DLLExport CameraPlugin_FixedCamera: public Graphics::CameraPlugin, public Events::EventRecipient
{
  private:
    // Event IDs
    EventID            EV_INIT;                /*!< Initialization event id   */

    VecPosition        mOffset;                /*!< offset to parent node     */
    VecPosition        mPosition;              /*!< Position of the camera    */
    Ogre::Quaternion   mOrientation;           /*!< Orientation of the camera */
    Ogre::SceneNode  * mNode;                  /*!< Node to track             */

  public:
                       CameraPlugin_FixedCamera(                              );
    virtual          ~ CameraPlugin_FixedCamera(                              );

    virtual bool       activate                (                              );
    virtual void       deactivate              (                              );
    virtual void       update                  (                              );
    virtual void       processEvent            ( const Events::Event& ev      );

    void               initialize              ( Ogre::SceneNode * node,
                                                 const VecPosition & posCam,
                                                 const VecPosition & offset   );
    void               initialize              ( const VecPosition & posCam,
                                                 const Ogre::Quaternion& ori  );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_FixedCam is a factory to create instances of CameraPlugin_FixedCamera class. */
class _DLLExport PluginFactory_FixedCam: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_FixedCam() : Klaus::PluginFactory("FixedCamera", "CameraPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new CameraPlugin_FixedCamera;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_FixedCam;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}



#endif // CAMERA_PLUGIN_FIXED_CAMERA
