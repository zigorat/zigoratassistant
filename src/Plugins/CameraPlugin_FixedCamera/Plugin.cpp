/*
*********************************************************************************
*            Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Fixed camera implementation as a camera agent                      *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FixedCamera/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Fixed camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>
#include <CoreSystem/CoreSystem.h>
#include <InputSystem/InputSystem.h>


/**
  Constructor fo FixedCamera. Registers events only.
*/
CameraPlugin_FixedCamera::CameraPlugin_FixedCamera()
{
  mNode = NULL;
  mCamera = NULL;
  EV_INIT = registerEvent("FixedCamera\\Init");
  setRecipientName("FixedCamera");
}

/**
  Destructor of FixedCamera
*/
CameraPlugin_FixedCamera::~CameraPlugin_FixedCamera()
{

}

/**
  This method shall activate the camera.
  @return true if plugin activated with no problem, false otherwise
*/
bool CameraPlugin_FixedCamera::activate()
{
  if(!mCamera)
  {
    mLog->log(19, "(FixedCamera) camera instance is null");
    return false;
  }

  resetCamera();
  mCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
  mCamera->setPosition((Ogre::Real)mPosition[0], (Ogre::Real)mPosition[1], (Ogre::Real)mPosition[2]);

  if(mNode)
    mCamera->setAutoTracking(true, mNode, Ogre::Vector3((Ogre::Real)mOffset[0], (Ogre::Real)mOffset[1], (Ogre::Real)mOffset[2]));
  else
    mCamera->setOrientation(mOrientation);

  return true;
}

/**
  This method deactivates the camera.
*/
void CameraPlugin_FixedCamera::deactivate()
{
  if(mCamera)
    resetCamera();
}

/**
  Updates the camera between every cycle
*/
void CameraPlugin_FixedCamera::update()
{

}

/**
  Processes events sent from core.
  @param ev Event sent from core
*/
void CameraPlugin_FixedCamera::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_INIT)
  {
    string str = ev.mArguments.getAsString("Subject");
    Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
    if(!mgr->hasSceneNode(str))
    {
      mLog->log(19, "(FixedCamera) node does not exist: %s", str.c_str());
      return;
    }

    initialize(mgr->getSceneNode(str), ev.mArguments.getAsPoint("Position"), ev.mArguments.getAsPoint("Offset"));
  }
  else
  {
    mLog->log(19, "(FixedCamera) unknown command %d", ev.mEventID);
    return;
  }
}

/**
  This method sets all the parameters of the fixed camera which looks at a subject only.
  @param node The subject to look at
  @param posCam Position which the camera is placed on. This is in global coordinates.
  @param offset Position relative to the subject to look at.
*/
void CameraPlugin_FixedCamera::initialize(Ogre::SceneNode* node, const Klaus::VecPosition& posCam, const Klaus::VecPosition& offset)
{
  mNode = node;
  mPosition = posCam;
  mOffset = offset;
  activate();
}

/**
  This method is an overload for setting the parameters of a fixed camera which always looks at a position.
  @param posCam Position of the camera. This is in global coordinates
  @param ori Orienation of the camera
*/
void CameraPlugin_FixedCamera::initialize(const Klaus::VecPosition& posCam, const Ogre::Quaternion & ori)
{
  mNode = NULL;
  mPosition = posCam;
  mOffset.setPosition(0, 0, 0);
  mOrientation = ori;
  activate();
}
