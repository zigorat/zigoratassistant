/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Free camera implementation as a camera agent                       *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FreeCam/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Free camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CAMERA_PLUGIN_FREE_CAM
#define CAMERA_PLUGIN_FREE_CAM

#include <DisplaySystem/CameraPlugin.h>  // needed for CameraPlugin
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient

using namespace Klaus;


/************************************************************************/
/***********************  Class CameraPlugin_FreeCam  *******************/
/************************************************************************/

/*! FreeCam is a camera agent which can move freely in the environment.
    This camera is usually controlled by a player. Typical example of
    this kind of camera is in counter strike after a player is dead.
*/
class _DLLExport CameraPlugin_FreeCam: public Graphics::CameraPlugin, public Events::EventRecipient
{
  private:
    // Registered events:
    unsigned           EV_FORWARD;         /*!< Forward movement event  */
    unsigned           EV_BACKWARD;        /*!< Backward movement event */
    unsigned           EV_STRAFE_LEFT;     /*!< Strafe left event       */
    unsigned           EV_STRAFE_RIGHT;    /*!< Straf right event       */
    unsigned           EV_MOVE_UP;         /*!< Move up event id        */
    unsigned           EV_MOVE_DOWN;       /*!< Move down event id      */
    unsigned           EV_ROTATE;          /*!< Rotate event id         */
    unsigned           EV_YAW_LEFT;        /*!< Yaw camera left event   */
    unsigned           EV_YAW_RIGHT;       /*!< Yaw camera right event  */
    unsigned           EV_SET_PARAMS;      /*!< Configure event         */

    double             mLong;              /*!< Longitudal angle of cam */
    double             mLat;               /*!< Latitudal angle of cam  */
    Ogre::Real         mSpeed;             /*!< Movement speed          */
    double             mMouseSensitivity;  /*!< Mouse sensitivity       */
    bool               mInvalid;           /*!< Needs revalidation?     */

    void               rotate              ( double dpitch,
                                             double dyaw                );

  public:
                       CameraPlugin_FreeCam(                            );
    virtual          ~ CameraPlugin_FreeCam(                            );

    virtual bool       activate            (                            );
    virtual void       deactivate          (                            );
    virtual void       update              (                            );
    virtual void       processEvent        ( const Events::Event& ev    );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_FreeCam is a factory to create instances of CameraPlugin_FreeCam class. */
class _DLLExport PluginFactory_FreeCam: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_FreeCam() : Klaus::PluginFactory("FreeCamera", "CameraPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new CameraPlugin_FreeCam;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_FreeCam;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}


#endif // CAMERA_PLUGIN_FREE_CAM
