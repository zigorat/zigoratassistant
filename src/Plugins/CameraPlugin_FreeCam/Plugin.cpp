/*
*********************************************************************************
*             Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Free camera implementation as a camera agent                       *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FreeCam/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Free camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>
#include <GraphicSystem/GraphicSystem.h>
#include <InputSystem/InputSystem.h>


/**
  Constructor of free cam. Registers events.
*/
CameraPlugin_FreeCam::CameraPlugin_FreeCam()
{
  EV_FORWARD      = registerEvent("FreeCamera\\MoveForward");
  EV_BACKWARD     = registerEvent("FreeCamera\\MoveBackward");
  EV_STRAFE_LEFT  = registerEvent("FreeCamera\\StrafeLeft");
  EV_STRAFE_RIGHT = registerEvent("FreeCamera\\StrafeRight");
  EV_MOVE_UP      = registerEvent("FreeCamera\\MoveUp");
  EV_MOVE_DOWN    = registerEvent("FreeCamera\\MoveDown");
  EV_ROTATE       = registerEvent("FreeCamera\\Rotate");
  EV_YAW_LEFT     = registerEvent("FreeCamera\\YawLeft");
  EV_YAW_RIGHT    = registerEvent("FreeCamera\\YawRight");
  EV_SET_PARAMS   = registerEvent("FreeCamera\\SetParam");

  mLong = 90.0;
  mLat = 0.0;
  mSpeed = 15;
  mMouseSensitivity = 0.25;
  mInvalid = true;

  setRecipientName("FreeCamera");
}

/**
  Destructor of FreeCam. Does nothing.
*/
CameraPlugin_FreeCam::~CameraPlugin_FreeCam()
{

}

/**
  This method rotates the camera
*/
void CameraPlugin_FreeCam::rotate(double dpitch, double dyaw)
{
  dpitch *= mMouseSensitivity;
  dyaw *= mMouseSensitivity;

  mLong += dpitch;
  if(mLat + dyaw < 85 && mLat + dyaw > -85)
    mLat += dyaw;

  mInvalid = true;
}

/**
  This method shall activate the camera.
  @return true if plugin activated with no problem, false otherwise
*/
bool CameraPlugin_FreeCam::activate()
{
  if(!mCamera)
  {
    mLog->log(19, "(FreeCamera) camera instance not set");
    return false;
  }

  using namespace Input;
  registerBinding(KeyCombination(OnKeyDown, KC_W), EV_FORWARD);
  registerBinding(KeyCombination(OnKeyDown, KC_S), EV_BACKWARD);
  registerBinding(KeyCombination(OnKeyDown, KC_A), EV_STRAFE_LEFT);
  registerBinding(KeyCombination(OnKeyDown, KC_D), EV_STRAFE_RIGHT);
  registerBinding(KeyCombination(OnKeyDown, KC_PGUP), EV_MOVE_UP);
  registerBinding(KeyCombination(OnKeyDown, KC_PGDOWN), EV_MOVE_DOWN);
  registerBinding(KeyCombination(OnKeyDown, KC_LEFT), EV_YAW_LEFT);
  registerBinding(KeyCombination(OnKeyDown, KC_RIGHT), EV_YAW_RIGHT);
  registerBinding(KeyCombination(OnKeyDown, KC_MOUSE_LEFT, KC_MOUSE_MOVE), EV_ROTATE);

  resetCamera();
  mCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
  mCamera->setPosition(0, -20, 5);
  mCamera->lookAt(0, 0, 0);

  return true;
}

/**
  This method deactivates the camera.
*/
void CameraPlugin_FreeCam::deactivate()
{
  using namespace Input;
  unregisterBinding(EV_FORWARD);
  unregisterBinding(EV_BACKWARD);
  unregisterBinding(EV_STRAFE_LEFT);
  unregisterBinding(EV_STRAFE_RIGHT);
  unregisterBinding(EV_MOVE_UP);
  unregisterBinding(EV_MOVE_DOWN);
  unregisterBinding(EV_YAW_LEFT);
  unregisterBinding(EV_YAW_RIGHT);
  unregisterBinding(EV_ROTATE);

  if(mCamera)
    resetCamera();
}

/**
  Updates the camera between every cycle
*/
void CameraPlugin_FreeCam::update()
{
  if(mInvalid)
  {
    VecPosition p(1.0, mLong, mLat, POLAR);
    Ogre::Vector3 p2 = mCamera->getPosition();
    p[0] += p2[0];
    p[1] += p2[1];
    p[2] += p2[2];
    mCamera->lookAt((Ogre::Real)p[0], (Ogre::Real)p[1], (Ogre::Real)p[2]);
    mInvalid = false;
  }
}

/**
  Processes events sent from core.
  @param ev Event sent from core
*/
void CameraPlugin_FreeCam::processEvent(const Events::Event& ev)
{
  if(!mCamera)
  {
    Logger::getSingleton()->log(19, "(FreeCamera) camera not active but received core event");
    return;
  }

  Ogre::Vector3 p;
  p.x = p.y = p.z = 0.0;
  double rot_pitch = 0.0, rot_yaw = 0.0;

  if(ev == EV_FORWARD)
    p.z = -mSpeed;
  else if(ev == EV_BACKWARD)
    p.z = mSpeed;
  else if(ev == EV_STRAFE_LEFT)
    p.x = -mSpeed;
  else if(ev == EV_STRAFE_RIGHT)
    p.x = mSpeed;
  else if(ev == EV_MOVE_UP)
    p.y = mSpeed;
  else if(ev == EV_MOVE_DOWN)
    p.y = -mSpeed;
  else if(ev == EV_ROTATE)
  {
    VecPosition rc = Input::InputSystem::getSingleton()->getMouseDisplace();
    rot_pitch = -rc[0];
    rot_yaw   = -rc[1];
  }
  else if(ev == EV_YAW_LEFT)
  {
    double d = Graphics::GraphicSystem::getSingleton()->getLastFrameDuration();
    rot_pitch = d / 2.0 * mMouseSensitivity;
  }
  else if(ev == EV_YAW_RIGHT)
  {
    double d = Graphics::GraphicSystem::getSingleton()->getLastFrameDuration();
    rot_pitch = -d / 2.0 * mMouseSensitivity;
  }
  else if(ev == EV_SET_PARAMS)
  {
    StringArray vars;
    ev.mArguments.getAllNames(vars);

    for(size_t i = 0; i < vars.size(); ++i)
      if(vars[i] == "Position")
      {
        VecPosition p = ev.mArguments.getAsPoint(vars[i]);
        mCamera->setPosition((Ogre::Real)p[0], (Ogre::Real)p[1], (Ogre::Real)p[2]);
      }
      else if(vars[i] == "Longitude")
      {
        mLong = ev.mArguments[vars[i]];
        mInvalid = true;
      }
      else if(vars[i] == "Latitude")
      {
        mLat = ev.mArguments[vars[i]];
        mInvalid = true;
      }
      else if(vars[i] == "MouseSensitivity")
        mMouseSensitivity = (Ogre::Real)(ev.mArguments[vars[i]]);
      else if(vars[i] == "MovementSpeed")
        mSpeed = (Ogre::Real)(ev.mArguments[vars[i]]);
      else if(vars[i] == "LookPosition")
      {
        VecPosition p = ev.mArguments.getAsPoint(vars[i]);
        mCamera->lookAt((Ogre::Real)p[0], (Ogre::Real)p[1], (Ogre::Real)p[2]);
      }
  }
  else
  {
    Logger::getSingleton()->log(19, "(FreeCamera) unknown event %d", ev.mEventID);
    return;
  }

  if(!p.isZeroLength())
  {
    // Take in accout the elapsed time
    p *= (Ogre::Real)Graphics::GraphicSystem::getSingleton()->getLastFrameDuration();

    // Move the camera
    mCamera->moveRelative(p);
  }

  if(rot_yaw != 0.0 || rot_pitch != 0.0)
    rotate(rot_pitch, rot_yaw);
}
