/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A plugin to connect this system to SimSpark simulator              *
*                                                                               *
*********************************************************************************
*/

/*! \file SimSpark_Plain/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A plugin to connect this system to SimSpark simulator
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef SIMSPARK_PLAIN
#define SIMSPARK_PLAIN

#include <SceneSystem/SceneSystem.h>
#include <SceneGraphSystem/Node.h>

using namespace Klaus;

/*! A buffer size of 32 KBs */
const int uBufSize = 128 * 1024;



/*********************************************************************/
/**********************  Class SimSpark_Plain  ***********************/
/*********************************************************************/

/*! A plugin to connect this system to SimSpark simulator */
class _DLLExport SimSpark_Plain : public Scene::ScenePlugin,
                                  public Events::EventRecipient
{
  private:
    /*! FilePositions is a dynamic array to store
         starting positions of lines in the log file */
    typedef std::vector<std::streampos> FilePositions;

    /*! CycleType is an enumeration which indicates type of data in log file */
    enum CycleType
    {
      Delta,      /*!< Updated scene message  */
      Complete,   /*!< Complete scene message */
      Unknown     /*!< Unknown cycle type     */
    };

    // Registered event IDs
    EventID                EV_NEXT_FRAME;       /*!< Next frame                */
    EventID                EV_PREV_FRAME;       /*!< Previous frame            */
    EventID                EV_PLAYPAUSE;        /*!< Play/Pause                */

    // members
    Logger               * mLog;                /*!< Logger instance           */
    std::ifstream        * mStream;             /*!< Log file itself           */
    FilePositions          mCyclePositions;     /*!< Positions of cycle starts */
    size_t                 mCurrentCycle;       /*!< Current cycle number      */
    char                   mCycleBuf[uBufSize]; /*!< Buffer for current cycle  */
    VarList                mData;               /*!< Data of the simulation    */
    StringArray            mPlayModes;          /*!< All play modes            */
    RSG2::GraphicalNode  * mRootNode;           /*!< Root node of the cycle    */
    bool                   mPlaying;            /*!< Is log file being played  */
    double                 mLastFrameTime;      /*!< Last time the frame was played */

    // parsers
    void                   clearRootNode     (                             );

    bool                   parseCurrentCycle (                             );
    CycleType              parseCycleType    ( SToken token                );
    void                   parseData         ( SToken token                );
    void                   parseCompleteCycle( SToken token                );
    void                   applyDeltaCycle   ( SToken token,
                                               RSG2::Node * node = NULL    );

    // seek methods
    bool                   seekCycle         ( size_t iCycle, bool bJump   );
    bool                   seekNextCycle     (                             );
    bool                   seekPrevCycle     (                             );

    // playback
    void                   play              (                             );
    void                   pause             (                             );

    // log file related
    bool                   openLogFile       ( const std::string & strName );
    bool                   closeLogFile      (                             );

  public:
                           SimSpark_Plain    (                             );
                         ~ SimSpark_Plain    (                             );

    virtual bool           initialize        (                             );
    virtual void           updateScene       (                             );
    virtual void           processEvent      ( const Events::Event & ev    );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_Simspark is a factory to create instances of SimSpark_Plain class. */
class _DLLExport PluginFactory_Simspark: public Klaus::PluginFactory
{
  public:
    /**
      Constructor. Sets information of factory.
    */
    PluginFactory_Simspark() : Klaus::PluginFactory("SimSpark_Plain", "ScenePlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
      Returns a new instance of the plugin
      @return Instance of the plugin
    */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new SimSpark_Plain;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
  This method creates a factory instance for the plugin in this library.
  @return An instance of the plugin
*/
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_Simspark;
}

/**
  This method frees an instance of the plugin created by this library
  @param factory Factory instance to delete
*/
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}


#endif // SIMSPARK_PLAIN
