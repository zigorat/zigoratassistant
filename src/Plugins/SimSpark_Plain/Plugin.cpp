/*
*********************************************************************************
*             Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A plugin to connect this system to SimSpark simulator              *
*                                                                               *
*********************************************************************************
*/

/*! \file SimSpark_Plain/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A plugin to connect this system to SimSpark simulator
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <GraphicSystem/GraphicSystem.h>
#include <libZigorat/ApplicationSettings.h>
#include <libZigorat/SLang.h>
#include <SceneGraphSystem/NodeFactory.h>


/*********************************************************************/
/***********************  Class SamlePlugin  *************************/
/*********************************************************************/

/**
  Constructor of SimSpark plugin.
*/
SimSpark_Plain::SimSpark_Plain()
{
  mLog = Logger::getSingleton();
  mStream = NULL;
  mCurrentCycle = 0;
  mRootNode = NULL;

  setRecipientName("SimSpark_Plain");
  EV_NEXT_FRAME = registerEvent("SimSpark_Plain\\NextCycle");
  EV_PREV_FRAME = registerEvent("SimSpark_Plain\\PrevCycle");
  EV_PLAYPAUSE  = registerEvent("SimSpark_Plain\\PlayPuase");

  using namespace Klaus::Input;
  registerBinding(KeyCombination(OnKeyPress, KC_N),     EV_NEXT_FRAME);
  registerBinding(KeyCombination(OnKeyPress, KC_P),     EV_PREV_FRAME);
  registerBinding(KeyCombination(OnKeyPress, KC_SPACE), EV_PLAYPAUSE);
}

/**
  Destructor of SimSpark plugin. Closes log files and removes the graphical data
*/
SimSpark_Plain::~SimSpark_Plain()
{
  closeLogFile();
}

/**
  This method clears the root graphical node
*/
void SimSpark_Plain::clearRootNode()
{
  if(mRootNode)
  {
    delete mRootNode;
    mRootNode = NULL;
  }
}

/**
  This method parses current cycle information. Each cycle
  contains three important data:
    - The cycle's data like it's time
    - The cycle's graphical data type
    - The cycle's graphical data

  @return True on successfull parse, false otherwise
*/
bool SimSpark_Plain::parseCurrentCycle()
{
  // check for the stream
  if(!mStream)
  {
    mLog->log(27, "(SimSpark_Plain) no open streams");
    return false;
  }

  try
  {
    // clear the stream and jump to position
    mStream->clear();
    mStream->seekg(mCyclePositions[mCurrentCycle]);

    // get the string and parse it
    mStream->getline(mCycleBuf, uBufSize);
  }
  catch(std::ios::failure e)
  {
    // do nothing
  }

  try
  {
    std::string strLine = "(";
    strLine.append(mCycleBuf);
    strLine.append(1, ')');

    SExpression expr(strLine);
    if(expr.size() != 3)
    {
      mLog->log(27, "(SimSpark_Plain) not enough data to use for cycle %d: %s", mCurrentCycle, expr.str().c_str());
      return false;
    }

    // first token contains the data for the cycle
    parseData(expr[0]);

    // the second tells the type of the third token, RDS or RSG
    CycleType cycle_type = parseCycleType(expr[1]);

    // the third one is the cycle's graphical data
    if(cycle_type == Complete)
      parseCompleteCycle(expr[2]);
    else if(cycle_type == Delta)
      applyDeltaCycle(expr[2]);
    else
      return false;

    // update the graphical tree
    if(mRootNode)
      mRootNode->validateGraphics();
  }
  catch(SLangException e)
  {
    mLog->log(27, "(SimSpark_Plain) malformed s-expression at cycle %d", mCurrentCycle);
    mLog->log(27, e.str());
    return false;
  }

  mLog->log(27, "(SimSpark_Plain) cycle %d : success", mCurrentCycle);
  return true;
}

/**
  This method parses the given token and returns the cycle type based on its s-expression.
  Two cycle types are valid:
    - (RSG 0 1) for full data cycles
    - (RDS 0 1) for delta data cycles

  @param token The token containing the cycle type
  @return Type of the cycle, complete or delta
*/
SimSpark_Plain::CycleType SimSpark_Plain::parseCycleType(SToken token)
{
  if(token.size() != 3)
    mLog->log(27, "(SimSpark_Plain) cycle type expression is empty at cycle %d", mCurrentCycle);
  else if(token[2].number() != 1)  // third token is the minor version
    mLog->log(27, "(SimSpark_Plain) unknown minor version for cycle %d", mCurrentCycle, token[1].str().c_str());
  else if(token[1].number() != 0)  // second token is the major version
    mLog->log(27, "(SimSpark_Plain) unknown major version for cycle %d: %f", mCurrentCycle, token[2].str().c_str());
  else if(token[0] == "RSG")
    return Complete;
  else if(token[0] == "RDS")
    return Delta;
  else
    mLog->log(27, "(SimSpark_Plain) unknown cycle type '%s'", token.str().c_str());

  return Unknown;
}

/**
  This method parses the data in a cycle. The data contains time, playmode names,
  field size and some other things.
  @param token The token containing the data
*/
void SimSpark_Plain::parseData(SToken token)
{
  // add them all
  mData.clear();

  for(size_t i = 0; i < token.size(); ++i)
  {
    SToken t_inner = token[i];

    if(t_inner.size() != 2) // probably play modes
    {
      if(t_inner.empty())
        mLog->log(27, "(SimSpark_Plain) play modes empty");
      else if(t_inner[0] != "play_modes")
        mLog->log(27, "(SimSpark_Plain) not a play_modes commmand: %s", t_inner.str().c_str());
      else
      {
        // add all the play modes
        mPlayModes.clear();
        for(size_t j = 1; j < t_inner.size(); ++j)
          mPlayModes.push_back(t_inner[j]);
      }
    }

    // add the data
    else
      mData.addVar(t_inner[0].str(), t_inner[1].str());
  }
}

/**
  This method parses an absolute data cycle. This type of cycle data contains
  everything about the cycle in a complete form.
  @param token The token containing the cycle's graphical data
*/
void SimSpark_Plain::parseCompleteCycle(SToken token)
{
  // clean up the root graphical node
  clearRootNode();

  RSG2::Node * root_node;
  if(token.size() > 2 && token[1].isAtom())
    root_node = Klaus::RSG2::NodeFactoryCore::getSingleton()->createInstanceFor(token[1].str());
  else
    root_node = Klaus::RSG2::NodeFactoryCore::getSingleton()->createInstanceFor("RootSceneNode");

  if(!root_node)
  {
    mLog->log(27, "(SimSpark_Plain) RSG: could not allocate root node");
    return;
  }

  mRootNode = dynamic_cast<RSG2::GraphicalNode*>(root_node);
  if(!mRootNode)
  {
    mLog->log(27, "(SimSpark_Plain) RSG: root node is not visual: %s", root_node->getType().c_str());
    return;
  }

  // load from RSG
  if(!mRootNode->parse(token))
  {
    mLog->log(27, "(SimSpark_Plain) RSG: error in RSG2::parse");
    return;
  }

  // update graphical nodes
  RSG2::Node::Array nodes;
  mRootNode->queryNodes(nodes);
  for(size_t i = 0; i < nodes.size(); ++i)
  {
    RSG2::GraphicalNode * node = dynamic_cast<RSG2::GraphicalNode*>(nodes[i]);
    if(node)
      node->attachToParent();
  }
}

/**
  This method applies a delta cycle data. This type of cycle data contains
  information of the nodes relative to their previous cycle.
  @param token The token containing the cycle's graphical data
  @param node The node to apply the difference
*/
void SimSpark_Plain::applyDeltaCycle(SToken token, RSG2::Node * node)
{
  // root node exists?
  if((!mRootNode) || (mRootNode->getChildCount() == 0))
  {
    mLog->log(27, "(SimSpark_Plain) RDS: no usable root to apply delta cycle %d", mCurrentCycle);
    return;
  }

  // default parameter?
  if(!node)
    node = mRootNode;

  size_t cIndex = 0;

  // set the start index for updating
  size_t sIndex = 0;
  if(node->getType() != "RootSceneNode")
  {
    sIndex = 1;

    // check for nd token
    if(token.size() == 0 || token[0] != "nd")
    {
      mLog->log(27, "(SimSpark_Plain) RDS: no nd token for non root node: %s", token.str().c_str());
      return;
    }

    // check for node type
    if(token.size() >= 2 && token[1].isAtom())
    {
      std::string name = token[1];
      if((name != "SMN") && (name != "StaticMesh") && (name != "TRF") && (name != "Light") && (name != "BN"))
        mLog->log(27, "(SimSpark_Plain) RDS: unknown node in delta cycle %s", name.c_str());

      sIndex = 2;
    }
  }

  // apply updates
  for(size_t i = sIndex; i < token.size(); ++i)
  {
    SToken t_inner = token[i];
    if(t_inner.isNormal())
    {
      // another node within node
      if((!t_inner.empty()) && (t_inner[0].isAtom()) && (t_inner[0] == "nd"))
      {
        if(cIndex >= node->getChildCount())
        {
          mLog->log(27, "(SimSpark_Plain) RDS: more tokens than available children: %s for %d children",
                    token.str().c_str(), node->getChildCount());
          return;
        }

        applyDeltaCycle(t_inner, node->getChildByIndex(cIndex++));
      }
      else
      {
        // its a command, apply it
       RSG2::RDSNode * delta_node = dynamic_cast<RSG2::RDSNode*>(node);
       delta_node->parseRDSCommand(token[i]);
      }
    }
    else
      mLog->log(27, "(SimSpark_Plain) RDS: unknown token type %s at %d", token[i].str().c_str(), mCurrentCycle);
  }

  if(cIndex != node->getChildCount())
    mLog->log(27, "(SimSpark_Plain) RDS: not enough tokens: %d -> %d : %s", cIndex, node->getChildCount(), token.str().c_str());
}

/**
  This method seeks a given cycle in the stream. If an undirect seek is
  requested, the stream will be traversed to get to the desired cycle;
  Otherwise the stream will jump to the requested cycle.
  @param iCycle The cycle number to seek and show
  @param bJump Whether directly go to the requested cycle or undirectly traverse to it
  @return True if the cycle is valid and can be seeken, false otherwise
*/
bool SimSpark_Plain::seekCycle(size_t iCycle, bool bJump)
{
  if(iCycle >= mCyclePositions.size())
  {
    mLog->log(27, "(SimSpark_Plain) attempted to seek to a cycle out of range %d : %d",
                  iCycle, mCyclePositions.size());
    return false;
  }

  if(bJump)
  {
    mCurrentCycle = iCycle;
    return parseCurrentCycle();
  }
  else
    while(mCurrentCycle != iCycle)
    {
      bool bResult;
      if(iCycle > mCurrentCycle)
        bResult = seekNextCycle();
      else
        bResult = seekPrevCycle();

      if(!bResult)
      {
        mLog->log(27, "(SimSpark_Plain) seek: traverse to %d stock at %d", iCycle, mCurrentCycle);
        return false;
      }
    }

  return true;
}

/**
  This method is used to go to the next cycle and show it's data.
  @return True if another cycle existed and could be shown, false otherwise
*/
bool SimSpark_Plain::seekNextCycle()
{
  if((mCurrentCycle + 1) >= mCyclePositions.size())
  {
    mLog->log(27, "(SimSpark_Plain) seek next: no next cycle");
    return false;
  }

  mCurrentCycle++;
  return parseCurrentCycle();
}

/**
  This method is used to go to the previous cycle and show it's data.
  @return True if any previous cycle existed and could be shown, false otherwise
*/
bool SimSpark_Plain::seekPrevCycle()
{
  if(mCurrentCycle == 0)
  {
    mLog->log(27, "(SimSpark_Plain) seek prev: at the first cycle");
    return false;
  }

  mCurrentCycle--;
  return parseCurrentCycle();
}

void SimSpark_Plain::play()
{
  mPlaying = true;
  mLastFrameTime = Timing::now();
}

void SimSpark_Plain::pause()
{
  mPlaying = false;
}

/**
  This method opens a log file with a given name and shows it's first frame.
  The log file can then be traversed and shown.
  @param strName Name of the log file
  @return True if the logfile could be openned and shown, false otherwise
*/
bool SimSpark_Plain::openLogFile(const std::string& strName)
{
  // close the previous log file
  closeLogFile();

  // playback controls
  mPlaying = false;
  mLastFrameTime = 0;

  // open the new log file
  Timing time;
  time.restartTime();
  mStream = new std::ifstream(strName.c_str());
  if(!*mStream)
  {
    mLog->log(27, "(SimSpark_Plain) could not open log file '%s'", strName.c_str());
    return false;
  }
  else
    mLog->log(27, "(SimSpark_Plain) openned log file '%s'", strName.c_str());

  // find out start of all cycle positions
  std::streamsize size;
  while(!mStream->eof())
  {
    mCyclePositions.push_back(mStream->tellg());
    mStream->getline(mCycleBuf, uBufSize);

    // check how many charactors are read
    size = mStream->gcount();
    if(size == uBufSize)
    {
      mLog->log(27, "(SimSpark_Plain) buffer size is small");
      return false;
    }
    else if(size < 2)
      break;
  }

  // log till here
  mLog->log(27, "(SimSpark_Plain) read %d cycles in %2.2f seconds", mCyclePositions.size(), time.getElapsedTime());

  // clean up failure exceptions
  mStream->clear();
  mStream->exceptions(std::ios_base::failbit);

  // now seek for the first cycle
  if(!seekCycle(0, true))
  {
    mLog->log(27, "(SimSpark_Plain) could not parse first line of log file");
    closeLogFile();
    return false;
  }

  // start playback
  play();

  mLog->log(27, "(SimSpark_Plain) loaded log file");
  return true;
}

/**
  This method closes the log file and cleans up it's data
  @return True if any log files were open, false otherwise
*/
bool SimSpark_Plain::closeLogFile()
{
  if(!mStream)
    return false;

  // close the log file
  delete mStream;
  mStream = NULL;

  // the root graphical node
  clearRootNode();

  // reset information
  mCyclePositions.clear();
  mCurrentCycle = 0;

  // playback controls
  mPlaying = false;
  mLastFrameTime = 0;

  // finished
  mLog->log(27, "(SimSpark_Plain) closed log file");
  return true;
}

/**
  This method shall tell SceneSystem whether initialization of this plugin
  has been successfully completed or not. If not, SceneSystem will free
  this plugin.
  @return If false is returned SceneSystem will free this plugin
*/
bool SimSpark_Plain::initialize()
{
  // set camera attributes
  Events::Event ev("FreeCamera\\SetParam");
  ev.mArguments.addVar("Position", VecPosition(0, -20, 8));
  ev.mArguments.addVar("LookPosition", VecPosition(0, 0, 0));
  ev.broadcast();

  std::string s = ApplicationSettings::getSingleton()->getRSGPath() + "../src/Plugins/SimSpark_Plain/sparkmonitor.log";
  return openLogFile(s);
}

/**
  This method shall update the scene every frame
*/
void SimSpark_Plain::updateScene()
{
  if(!mPlaying)
    return;

  double now = Timing::now();
  if(now - mLastFrameTime < 0.1)
    return;

  mLastFrameTime += 0.1;
  if(!seekNextCycle())
    mPlaying = false;
}

/**
  Processes registered events. This includes key registered key combination events.
  @param ev The event sent by core
*/
void SimSpark_Plain::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_NEXT_FRAME)
    seekNextCycle();
  else if(ev == EV_PREV_FRAME)
    seekPrevCycle();
  else if(ev == EV_PLAYPAUSE)
  {
    if(mPlaying)
      pause();
    else
      play();
  }
}
