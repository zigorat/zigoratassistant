/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is sample plugin to demonstrate capabilities of scene system  *
*                                                                               *
*********************************************************************************
*/

/*! \file ScenePlugin_Sample/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is a sample plugin to demonstrate capabilities of scene system
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef SAMPLE_SCENE_PLUGIN
#define SAMPLE_SCENE_PLUGIN

#include <SceneSystem/SceneSystem.h>     // needed for ScenePlugin and SceneSystem
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient
#include <Ogre.h>                        // needed for Ogre::SceneNode and etc.

using namespace Klaus;


/*********************************************************************/
/***********************  Class SamlePlugin  *************************/
/*********************************************************************/

/*! A sample plugin class to demonstrate capabilites of SceneSystem. */
class _DLLExport SamplePlugin : public Scene::ScenePlugin, public Events::EventRecipient
{
  private:
    // Registered event IDs
    EventID                EV_WALK;       /*!< Forward walk event    */
    EventID                EV_WALK_BACK;  /*!< Backward walk event   */

    Ogre::Entity         * mNinja;        /*!< Ninja test instance   */
    Ogre::ParticleSystem * mRain;         /*!< Childish rain effect  */

  public:
                           SamplePlugin   (                          );
                         ~ SamplePlugin   (                          );

    virtual bool           initialize     (                          );
    virtual void           updateScene    (                          );
    virtual void           processEvent   ( const Events::Event & ev );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_Sample is a factory to create instances of SamplePlugin class. */
class _DLLExport PluginFactory_Sample: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_Sample() : Klaus::PluginFactory("Sample", "ScenePlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new SamplePlugin;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_Sample;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}

#endif // SAMPLE_SCENE_PLUGIN
