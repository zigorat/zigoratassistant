# *********************************************************************************
# *           CMakeLists.txt : Robocup 3D Soccer Simulation Team Zigorat          *
# *                                                                               *
# *  Date: 11/01/2009                                                             *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: ZigoratAssistant build configuration with cmake                    *
# *                                                                               *
# *********************************************************************************

# A sample plugin just to demonstrate simulator plugins
# =================================================================================

# Set the sources
set(
     Sources
     Plugin.h
     Plugin.cpp
   )

# Set the include folder
include_directories(
                     ${COMMON_INC_DIR}
                     ${OGRE_INCLUDE_DIR}
                     ${Boost_INCLUDE_DIR}
                   )

# Add the target
add_library(ScenePlugin_Sample MODULE ${Sources})

# Link to needed libraries
target_link_libraries(
                       ScenePlugin_Sample
                       Klaus
                     )

# set output folder
set_target_properties(
                       ScenePlugin_Sample
                       PROPERTIES
                       ARCHIVE_OUTPUT_DIRECTORY
                       ${PLUGINS_PATH}
                       LIBRARY_OUTPUT_DIRECTORY
                       ${PLUGINS_PATH}
                       RUNTIME_OUTPUT_DIRECTORY
                       ${PLUGINS_PATH}
                     )

