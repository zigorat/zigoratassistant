/*
*********************************************************************************
*          SamplePlugin.cpp : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is sample plugin to demonstrate capabilities of scene system  *
*                                                                               *
*********************************************************************************
*/

/*! \file ScenePlugin_Sample/Plugin.cpp
<pre>
<b>File:</b>          SamplePlugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is a sample plugin to demonstrate capabilities of scene system
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <GraphicSystem/GraphicSystem.h>



/*********************************************************************/
/***********************  Class SamlePlugin  *************************/
/*********************************************************************/

/**
  Constructor of sample plugin. Creates some stuff to demonstrate scene
  system capabilities.
*/
SamplePlugin::SamplePlugin()
{
  mLog = Logger::getSingleton();

  setRecipientName("SamplePlugin");

  EV_WALK = registerEvent("SamplePlugin\\Walk");
  EV_WALK_BACK = registerEvent("SamplePlugin\\WalkBack");

  using namespace Klaus::Input;
  registerBinding(KeyCombination(OnKeyDown, KC_W), EV_WALK);
  registerBinding(KeyCombination(OnKeyDown, KC_S), EV_WALK_BACK);

  // Get needed utilities
  Scene::Element * e0 = Scene::SceneSystem::getSingleton()->getScene()->mRootElement;

  // Add the ninja
  Scene::Element * e1 = e0->createChildNode();
  e1->getSceneNodeForEditing()->setScale(0.05f, 0.05f, 0.05f);
  e1->getSceneNodeForEditing()->setOrientation(0.0f, 0.0f, 0.7f, 0.7f);
  e1->getSceneNodeForEditing()->setPosition(0.0f, 10.0f, 0.0f);
  mNinja = mSceneMgr->createEntity("SamplePlugin\\Character", "ninja.mesh");
  mNinja->getAnimationState("Walk")->setEnabled(true);
  e1->getSceneNodeForEditing()->attachObject(mNinja);

  // set camera attributes
  Events::Event ev(Events::EventSystem::getSingleton()->registerEvent("FreeCamera\\SetParam"));
  ev.mArguments.addVar("Position", VecPosition(0, 20, 20));
  ev.mArguments.addVar("Latitude", -40);
  ev.mArguments.addVar("Longitude", -90);
  ev.broadcast();

  // Add the rain effect
  Scene::Element * e2 = e0->createChildNode();
  mRain = mSceneMgr->createParticleSystem("SamplePlugin\\Rain", "Particles/Rain");
  e2->getSceneNodeForEditing()->attachObject(mRain);
}

/**
  Destructor of sample plugin. Needs nothing to do, everything is freed by SceneSystem.
*/
SamplePlugin::~SamplePlugin()
{
}

/**
  This method shall tell SceneSystem whether initialization of this plugin
  has been successfully completed or not. If not, SceneSystem will free
  this plugin.
  @return If false is returned SceneSystem will free this plugin
*/
bool SamplePlugin::initialize()
{
  return true;
}

/**
  This method shall update the scene every frame
*/
void SamplePlugin::updateScene()
{
}

/**
  Processes registered events. This includes key registered key combination events.
  @param ev The event sent by core
*/
void SamplePlugin::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == EV_WALK)
  {
    Ogre::AnimationState * anim = mNinja->getAnimationState("Walk");
    anim->addTime((Ogre::Real)Graphics::GraphicSystem::getSingleton()->getLastFrameDuration());
    Ogre::SceneNode * node = Scene::SceneSystem::getSingleton()->getScene()->mRootElement->mChildren[0]->getSceneNodeForEditing();
    Ogre::Vector3 v = node->getPosition();
    v.y -= 0.1f;
    node->setPosition(v);
  }
  else if(ev == EV_WALK_BACK)
  {
    Ogre::AnimationState * anim = mNinja->getAnimationState("Walk");
    anim->addTime((Ogre::Real)-Graphics::GraphicSystem::getSingleton()->getLastFrameDuration());
    Ogre::SceneNode * node = Scene::SceneSystem::getSingleton()->getScene()->mRootElement->mChildren[0]->getSceneNodeForEditing();
    Ogre::Vector3 v = node->getPosition();
    v.y += 0.1f;
    node->setPosition(v);
  }
}
