/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This plugin is to test upcoming features.                          *
*                                                                               *
*********************************************************************************
*/

/*! \file TestPlugin/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This plugin is to test upcoming features.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#ifndef SAMPLE_SCENE_PLUGIN
#define SAMPLE_SCENE_PLUGIN

#include <SceneSystem/SceneSystem.h>     // needed for ScenePlugin and SceneSystem
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient
#include <Ogre.h>                        // needed for Ogre::SceneNode and etc.
#include <SceneGraphSystem/Graph.h>
#include <AgentBase/Trajectory.h>        // needed for EllipsoidTrajectory

using namespace Klaus;

class WalkEngine
{
  private:
    Logger            * mLog;
    RSG2::Graph         mGraph;
    InverseKinematics   mInverse;
    VecPosition         mLeftHip;
    VecPosition         mRightHip;

    double mPeriod;
    double mGaitLength;
    double mGaitHeight;
    double mThighLength;
    double mShankLength;

  public:
    WalkEngine();
    void initialize();

    void updateForTime(double t);
};


/*********************************************************************/
/************************  Class TestPlugin  *************************/
/*********************************************************************/

using Ogre::SceneNode;

/*! A sample plugin class to demonstrate capabilites of SceneSystem. */
class _DLLExport TestPlugin : public Scene::ScenePlugin, public Events::EventRecipient
{
  private:
    EventID       evToggleRun;

    double        mStartTime;
    double        mCurrentTime;
    bool          mRunning;

    WalkEngine    mEngine;


  public:
                  TestPlugin   (                          );
                ~ TestPlugin   (                          );

    virtual bool  initialize   (                          );
    virtual void  updateScene  (                          );
    virtual void  processEvent ( const Events::Event & ev );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_Sample is a factory to create instances of SamplePlugin class. */
class _DLLExport PluginFactory_Test: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_Test() : Klaus::PluginFactory("Test", "ScenePlugin", "Zigorat Team", 1, 0)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new TestPlugin;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_Test;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}

#endif // SAMPLE_SCENE_PLUGIN
