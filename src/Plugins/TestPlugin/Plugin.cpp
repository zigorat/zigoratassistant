/*
*********************************************************************************
*           TestPlugin.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 12/06/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This plugin is to test upcoming features.                          *
*                                                                               *
*********************************************************************************
*/

/*! \file TestPlugin/Plugin.cpp
<pre>
<b>File:</b>          TestPlugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       12/06/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This plugin is to test upcoming features.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
12/06/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <SceneGraphSystem/PerceptorNodes.h>
#include <AgentBase/Formations.h>
#include <AgentBase/WorldModel.h>


WalkEngine::WalkEngine()
{
  mLog = Logger::getSingleton();
}

void WalkEngine::initialize()
{
  mPeriod = 0.7;
  mGaitLength = 0.1;
  mGaitHeight = 0.07;

  mGraph.loadFromFile("rsg/agent/nao/nao.rsg");
  mGraph.setGlobalTransform(VecPosition(0, 0, 1), Klaus::Quaternion::identity);
  mGraph.getInverseKinematics(mInverse);
  mThighLength = mInverse["llj3"].mPivot.getDistanceTo(mInverse["llj4"].mPivot);
  mShankLength = mInverse["llj4"].mPivot.getDistanceTo(mInverse["llj5"].mPivot);

  mLeftHip[2] = (mShankLength + mThighLength - 0.05);
  mRightHip[2] = (mShankLength + mThighLength - 0.05);
}

void WalkEngine::updateForTime(double t)
{
  // get the current state of robot
  mGraph.getInverseKinematics(mInverse);
  mInverse["laj1"].mAngles[0] = -90;
  mInverse["raj1"].mAngles[0] = -90;

  VecPosition hip, foot, sol;
  Klaus::Degree angKnee, angHip, angFoot, angSol, angTorso = 0;
  Klaus::Radian angPhase;
  int sign;
  Klaus::Plane p;
  Line3 l;

  // left foot
  // find the phase
  angPhase = 2 * M_PI * t / mPeriod;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * angPhase.cos();
  foot[2] = Max(mGaitHeight / 2.0 * angPhase.sin(), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mLeftHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + sign * angSol.getDegree() + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;
  mInverse["llj3"].mAngles[0] = angHip;
  mInverse["llj4"].mAngles[0] = angKnee;
  mInverse["llj5"].mAngles[0] = angFoot;

  // right foot
  // find the phase
  angPhase = 2 * M_PI * t / mPeriod - M_PI;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * angPhase.cos();
  foot[2] = Max(mGaitHeight / 2.0 * angPhase.sin(), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mLeftHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + sign * angSol.getDegree() + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;

  // apply inverse kinematics
  mInverse["rlj3"].mAngles[0] = angHip;
  mInverse["rlj4"].mAngles[0] = angKnee;
  mInverse["rlj5"].mAngles[0] = angFoot;
  mGraph.applyInverseKinematicSheet(mInverse);
}




/*********************************************************************/
/************************  Class TestPlugin  *************************/
/*********************************************************************/

/**
 * Constructor of test plugin.
 */
TestPlugin::TestPlugin()
{
  mLog = Logger::getSingleton();

  mRunning = false;
  mCurrentTime = 0;
  mStartTime = 0;

  using namespace Input;
  evToggleRun = registerEvent("TestPlugin\\ToggleRun");
  registerBinding(KeyCombination(OnKeyPress, KC_SPACE), evToggleRun);
}

/**
  Destructor of sample plugin. Needs nothing to do, everything is freed by SceneSystem.
*/
TestPlugin::~TestPlugin()
{
}

/**
  This method shall tell SceneSystem whether initialization of this plugin
  has been successfully completed or not. If not, SceneSystem will free
  this plugin.
  @return If false is returned SceneSystem will free this plugin
*/
bool TestPlugin::initialize()
{
  Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
  Scene::Element *e0 = Scene::SceneSystem::getSingleton()->getScene()->mRootElement;

  // sky and plane
  Ogre::Entity * sky = mgr->createEntity("sky", "NaoSkyBox.mesh");
  Ogre::SceneNode * nd = e0->createChildNode()->getSceneNodeForEditing();
  double x = 1.0/sqrt(2.0);
  nd->setOrientation((Ogre::Real)x, (Ogre::Real)x, 0.0f, 0.0f);
  nd->attachObject(sky);
  Ogre::Plane p(Ogre::Vector3(0,1,0), 0);
  Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        p, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
  Ogre::Entity *pl = mgr->createEntity("griund","ground");
  pl->setMaterialName("SimSpark/NaoSoccerField");
  nd->attachObject(pl);

  mEngine.initialize();


  Events::Event ev("FreeCamera\\SetParam");
  ev.mArguments.addVar("Position", VecPosition(-0.5, 2, 1.5));
  ev.mArguments.addVar("Longitude", -70.0);
  ev.mArguments.addVar("Latitude", -10.0);
  ev.broadcast();

  return true;
}

/**
  This method shall update the scene every frame
*/
void TestPlugin::updateScene()
{
  if(!mRunning)
    return;

  double time = Timing::now();
  double delta = time - mStartTime;
  mStartTime = time;
  mCurrentTime += delta;

  mEngine.updateForTime(mCurrentTime);
}

/**
  Processes registered events. This includes key registered key combination events.
  @param ev The event sent by core
*/
void TestPlugin::processEvent(const Klaus::Events::Event& ev)
{
  if(ev == evToggleRun)
  {
    mRunning = !mRunning;

    if(mRunning)
      mStartTime = Timing::now();
  }
}
