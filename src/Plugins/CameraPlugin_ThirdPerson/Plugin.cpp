/*
*********************************************************************************
*            Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 07/15/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Third person camera implementation as a camera agent               *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_ThirdPerson/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/15/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Third person camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/15/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>
#include <InputSystem/InputSystem.h>


/**
  Constructor of third person camera. Initializes values and registers events.
*/
CameraPlugin_ThirdPerson::CameraPlugin_ThirdPerson()
{
  mNode = NULL;
  mRadius = 1;
  mAngLat = 0;
  mAngLong = 180;
  mSpeed = 0.25;
  mLatReverse = 1;
  mLongReverse = 1;

  setRecipientName("ThirdPersonCamera");

  EV_INIT = registerEvent("ThirdPersonCamera\\Init");
  EV_ROTATE = registerEvent("ThirdPersonCamera\\Rotate");
}

/**
  Destructor of third person camera. Does nothing
*/
CameraPlugin_ThirdPerson::~CameraPlugin_ThirdPerson()
{

}

/**
  This method updates camera position based on longitudal and latitudal current
  angles of camera. The camera can be controled with mouse. It rotates around
  the node which camera looks at.
*/
void CameraPlugin_ThirdPerson::setLocalPosition()
{
  VecPosition p(mRadius, mAngLong.getDegree(), mAngLat.getDegree(), POLAR);
  p += mLookOffset;
  mCamera->setPosition((Ogre::Real)p[0], (Ogre::Real)p[2], (Ogre::Real)p[1]);
}

/**
  This method shall activate the camera.
  @return true if plugin activated with no problem, false otherwise
*/
bool CameraPlugin_ThirdPerson::activate()
{
  if(!mCamera)
  {
    mLog->log(19, "(ThirdPersonCamera) camera not initialized");
    return false;
  }

  // Register bindings when activated
  using namespace Input;
  registerBinding(KeyCombination(OnKeyDown, KC_MOUSE_MOVE), EV_ROTATE);

  return true;
}

/**
  This method deactivates the camera.
*/
void CameraPlugin_ThirdPerson::deactivate()
{
  // unregister bindings when deactivated
  unregisterBinding(EV_ROTATE);

  if(mCamera)
    resetCamera();
}

/**
  Updates the camera between every cycle
*/
void CameraPlugin_ThirdPerson::update()
{
}

/**
  Processes events sent from core.
  @param ev Event sent from core
*/
void CameraPlugin_ThirdPerson::processEvent(const Events::Event& ev)
{
  if(ev == EV_INIT)
  {
    std::string str = ev.mArguments.getAsString("Subject");
    Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
    if(!mgr->hasSceneNode(str))
    {
      mLog->log(19, "(ThirdPersonCamera) setParams: node does not exist: %s", str.c_str());
      return;
    }

    initialize(
                mgr->getSceneNode(str),
                ev.mArguments.getAsPoint("Offset"),
                ev.mArguments["Radius"],
                ev.mArguments["Longitude"],
                ev.mArguments["Latitude"]
              );
  }
  else if(ev == EV_ROTATE)
  {
    VecPosition d = Input::InputSystem::getSingleton()->getMouseDisplace();
    d *= mSpeed;
    d[0] *= mLongReverse;
    d[1] *= mLatReverse;

    mAngLong += d[0];

    if(mAngLat + d[1] < 85 && mAngLat + d[1] > -85)
      mAngLat  += d[1];

    setLocalPosition();
  }
  else
    mLog->log(19, "(ThirdPersonCamera) unknown command %d", ev.mEventID);
}

/**
  This method sets the longitudal angle of camera around the subject which it looks.
  @param ang New longitudal angle of camera
*/
void CameraPlugin_ThirdPerson::setLongitudeAngle(const double& ang)
{
  mAngLong = ang;
}

/**
  This method sets the latitudal angle of camera around the subject
  @param ang New latitudal angle of the camera
*/
void CameraPlugin_ThirdPerson::setLatitudeAngle(const double& ang)
{
  mAngLat = ang;
}

/**
  This method returns whether the camera is controlled reversly as input arrives or not.
  For eg. If the mouse is moved right, then if reverse is active, the camera will rotate left.
  @param bLong Longitudal movement reverse factor will be copied here
  @param bLat Latitudal movement reverse factor will be copied here
*/
void CameraPlugin_ThirdPerson::getIsReverse(bool& bLong, bool& bLat) const
{
  bLong = mLongReverse == -1.0 ? true : false;
  bLat = mLatReverse == -1.0 ? true : false;
}

/**
  This method will set the reversion factor of camera movement
  @param bLong Whether longitudal movement should be reversed or not
  @param bLat Whether latitudal movement should be reversed or not
*/
void CameraPlugin_ThirdPerson::setIsReverse(const bool& bLong, const bool& bLat)
{
  mLongReverse = bLong ? -1 : 1;
  mLatReverse = bLat ? -1 : 1;
}

/**
  This method returns the current speed of camera rotation around the subject
  @return Speed of camera rotation around the subject
*/
double CameraPlugin_ThirdPerson::getSpeed() const
{
  return mSpeed;
}

/**
  This method sets the speed of rotation around the subject. Higher values
  result in faster rotation with input
  @param dSpeed New value for rotation speed
*/
void CameraPlugin_ThirdPerson::setSpeed(const double& dSpeed)
{
  mSpeed = dSpeed;
}

/**
  This method sets all needed parameters for this camera agent to work
  @param node The subject which the camera should always look
  @param offset Offset relative to the subject for camera to look
                (can be the position of head for example relative to the node center.)
  @param dRadius Distance of the camera to the subject
  @param angLong Initial longitudal angle of camera
  @param angLat Initial latitudal angle of camera
*/
void CameraPlugin_ThirdPerson::initialize(Ogre::SceneNode* node,
                                          const VecPosition& offset,
                                          const double& dRadius,
                                          const Klaus::Degree& angLong,
                                          const Klaus::Degree& angLat)
{
  if(!mCamera)
  {
    mLog->log(17, "(ThirdPersonCamera) camera instance is null");
    return;
  }

  resetCamera();
  mNode = node;
  mLookOffset = offset;
  mRadius = dRadius;
  mAngLong = angLong;
  mAngLat = angLat;

  // now execute them.
  mNode->attachObject(mCamera);
  mCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
  mCamera->setAutoTracking(true, mNode, Ogre::Vector3((Ogre::Real)mLookOffset[0], (Ogre::Real)mLookOffset[1], (Ogre::Real)mLookOffset[2]));
  setLocalPosition();
}
