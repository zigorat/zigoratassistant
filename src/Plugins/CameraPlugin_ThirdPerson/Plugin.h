/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 07/15/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Third person camera implementation as a camera agent               *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_ThirdPerson/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/15/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Third person camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/15/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CAMERA_PLUGIN_THIRD_PERSON
#define CAMERA_PLUGIN_THIRD_PERSON

#include <DisplaySystem/CameraPlugin.h>  // needed for CameraPlugin
#include <GraphicSystem/GraphicSystem.h> // needed for scene manager
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient

using namespace Klaus;


/***********************************************************************************/
/***************************  Class CameraPlugin_ThirdPerson ***********************/
/***********************************************************************************/

/*! Third person camera is a type of camera which looks at a target in subjective way.
    It always faces the target and is located outside it.
*/
class _DLLExport CameraPlugin_ThirdPerson: public Graphics::CameraPlugin, public Events::EventRecipient
{
  private:
    // Event IDs
    EventID            EV_INIT;                /*!< Initialization event id        */
    EventID            EV_ROTATE;              /*!< Mouse movement rotation event  */

    double             mRadius;                /*!< offset to parent node          */
    VecPosition        mPosition;              /*!< Current position of camera     */
    Klaus::Degree      mAngLat;                /*!< Latitudal angle                */
    Klaus::Degree      mAngLong;               /*!< Longitudal angle               */
    VecPosition        mLookOffset;            /*!< Offset for looking position    */
    Ogre::SceneNode  * mNode;                  /*!< Node to track                  */
    double             mSpeed;                 /*!< Mouse speed                    */
    int                mLongReverse;           /*!< Longitude reversion factor     */
    int                mLatReverse;            /*!< Lattide reversion factor       */

    void               setLocalPosition        (                                   );

  public:
                       CameraPlugin_ThirdPerson(                                   );
    virtual          ~ CameraPlugin_ThirdPerson(                                   );

    virtual bool       activate                (                                   );
    virtual void       deactivate              (                                   );
    virtual void       update                  (                                   );
    virtual void       processEvent            ( const Events::Event& ev           );

    void               setLongitudeAngle       ( const double & ang                );
    void               setLatitudeAngle        ( const double & ang                );
    void               getIsReverse            ( bool & bLong, bool & bLat         ) const;
    void               setIsReverse            ( const bool&bLong, const bool&bLat );
    double             getSpeed                (                                   ) const;
    void               setSpeed                ( const double & dSpeed             );

    void               initialize              ( Ogre::SceneNode * node,
                                                 const VecPosition & offset,
                                                 const double & dRadius,
                                                 const Klaus::Degree & angLong,
                                                 const Klaus::Degree & angLat      );
};


/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_TPC is a factory to create instances of CameraPlugin_ThirdPerson class. */
class _DLLExport PluginFactory_TPC: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_TPC() : Klaus::PluginFactory("ThirdPersonCamera", "CameraPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new CameraPlugin_ThirdPerson;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_TPC;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}


#endif // CAMERA_PLUGIN_THIRD_PERSON
