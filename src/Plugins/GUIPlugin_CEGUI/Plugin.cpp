/*
*********************************************************************************
*            Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 11/11/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A sample GUI plugin using CEGUI as it's base                       *
*                                                                               *
*********************************************************************************
*/

/*! \file GUIPlugin_CEGUI/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A sample GUI plugin using CEGUI as it's base
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2009       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <GUISystem/Factory.h>            // needed for Klaus::GUI::FactoryManager
#include <GraphicSystem/GraphicSystem.h>  // needed for GraphicSystem


/*****************************************************************************/
/******************************  Class CEGUILog  *****************************/
/*****************************************************************************/

/**
  Constructor of CEGUILog
*/
CEGUILog::CEGUILog()
{
}

/**
  Destructor of CEGUILog
*/
CEGUILog::~CEGUILog()
{
}

/**
  This method channels a log event of CEGUI system to Klaus::Logger
  @param message Log message
  @param level The level log belongs to
*/
void CEGUILog::logEvent(const CEGUI::String& message, CEGUI::LoggingLevel level)
{
  Klaus::Logger::getSingleton()->log(44, "(CEGUI) %s", message.c_str());
}

/**
  Set the name of the log file where all subsequent log entries
  should be written. Not used here, logs will be forwarded to Klaus::Logger
  There it will be decided whether or not logs shall be saved to files or not
*/
void CEGUILog::setLogFilename(const CEGUI::String& filename, bool append)
{
}


/*****************************************************************************/
/***************************  Class GUIPlugin_CEGUI  *************************/
/*****************************************************************************/

/**
  Constructor of GUIPlugin_CEGUI
*/
GUIPlugin_CEGUI::GUIPlugin_CEGUI()
{
  mLogCapsule = NULL;
  mRenderer = NULL;
  mSystem = NULL;

  mLog = Logger::getSingleton();
  mEventSystem = Events::EventSystem::getSingleton();

  setRecipientName("CEGUI");
}

/**
  Destructor GUIPlugin_CEGUI
*/
GUIPlugin_CEGUI::~GUIPlugin_CEGUI()
{
  #ifdef CEGUI_IS_OLD
    delete mSystem;
    delete mRenderer;

    // system or renderer removes logger on their destroy
  #else
    mRenderer->destroySystem();
    mRenderer = NULL;

    mSystem->destroy();
    mSystem = NULL;

    // remove logger at last
    delete mLogCapsule;
  #endif
}

/**
  This method returns height of titlebar
  @return Height of titlebar
*/
CEGUI::UDim GUIPlugin_CEGUI::getTitleBarHeight(CEGUI::Window * win)
{
  if(!win)
    return CEGUI::UDim(0,0);

  CEGUI::Window* titlebar = NULL;
  try
  {
    if(win)
      titlebar = CEGUI::WindowManager::getSingletonPtr()->getWindow(win->getName() + "/Form_auto_titlebar__");
  }
  catch(...)
  {
  }

  if(titlebar)
    return titlebar->getHeight();
  else if(win->getParent())
    return getTitleBarHeight(win->getParent());
  else
    return CEGUI::UDim(0,0);
}

/**
  This method converts a mouse button from Klaus::Input to a CEGUI::MouseButton enumeration
  @param btn Button name
  @return CEGUI representation of the button name
*/
CEGUI::MouseButton GUIPlugin_CEGUI::convertButton(const Klaus::Input::KeyCodeT& btn)
{
  switch(btn)
  {
    case KC_MOUSE_LEFT:
      return CEGUI::LeftButton;

    case KC_MOUSE_RIGHT:
      return CEGUI::RightButton;

    case KC_MOUSE_MIDDLE:
      return CEGUI::MiddleButton;

    default:
      return CEGUI::LeftButton;
  }
}

/**
  This method loads a UI in CEGUI
  @param str The file containing UI configuration
  @return True on successful loading of the UI file, false otherwise
*/
bool GUIPlugin_CEGUI::loadLayout(const std::string& str)
{
  clearUI();

  try
  {
    mLog->log(12, "(CEGUI) loading UI file '%s'", str.c_str());

    // loading the file with a buffer of 16 KBs
    SExpression expr(str, 16*1024);

    int index = expr.getFirstNonComment();
    if(index == -1)
    {
      mLog->log(12, "(CEGUI) no UI element in '%s'", str.c_str());
      return false;
    }

    int iErrors = 0;
    SToken main_token = expr[index];
    for(size_t i = 0; i < main_token.size(); ++i)
      if(main_token[i].size() > 2 && main_token[i][0].isAtom())
      {
        GUI::Element * node = GUI::FactoryManager::getSingleton()->getInstanceFor(main_token[i][0]);
        if(node->parse(main_token[i]))
          mUINodes.push_back(node);
        else
        {
          delete node;
          ++iErrors;
        }
      }

    if(iErrors > 0)
      mLog->log(12, "(CEGUI) %d errors occured during parsing ui file", iErrors, str.c_str());
    else
      mLog->log(12, "(CEGUI) successfully parsed ui file '%s'", str.c_str());


    // now create the UI
    iErrors = 0;
    for(size_t i = 0; i < mUINodes.size(); i++)
      if(!setupUI(mUINodes[i]))
      {
        mLog->log(12, "(CEGUI) failed to setup UI element '%s'", mUINodes[i]->getName().c_str());
        ++iErrors;
      }

    // activate the first ui element (considered to be the main window)
    if(mUINodes.size() != 0 && !setActiveForm(mUINodes[0]->getName()))
    {
      mLog->log(12, "(CEGUI) could not activate top most form");
      ++iErrors;
    }

    if(iErrors > 0)
      mLog->log(12, "(CEGUI) loaded UI, %d errors occured during load", iErrors);
    else
      mLog->log(12, "(CEGUI) successfully loaded UI");
  }
  catch(SLangException e)
  {
    mLog->log(12, "(CEGUI) SLangException thrown in loadLayout()");
    mLog->log(12, "%s", e.str());
    return false;
  }

  return true;
}

/**
  This method clears the UI raw nodes
*/
void GUIPlugin_CEGUI::clearUI()
{
  for(size_t i = 0; i < mUINodes.size(); i++)
    delete mUINodes[i];

  mUINodes.clear();
  mLog->log(12, "(CEGUI) cleared UI");
}

/**
  This method sets up the UI node to be able to be shown
  @param node Node to set up
  @param parent Parent of the CEGUI node
  @return True on successfull initialization, false otherwise
*/
bool GUIPlugin_CEGUI::setupUI(GUI::Element * node, CEGUI::Window * parent)
{
  using namespace Klaus::GUI;
  CEGUI::WindowManager *mgr = CEGUI::WindowManager::getSingletonPtr();
  CEGUI::Window * win;

  // Set up the Window type

  try
  {
    if(node->getType() == Window)
      win = mgr->createWindow("DefaultGUISheet", node->getUniqueName());
    else if(node->getType() == Button)
      win = mgr->createWindow("TaharezLook/Button", node->getUniqueName());
    else if(node->getType() == Label)
      win = mgr->createWindow("TaharezLook/StaticText", node->getUniqueName());
    else if(node->getType() == TextBox)
      win = mgr->createWindow("TaharezLook/Editbox", node->getUniqueName());
    else if(node->getType() == ComboBox)
      win = mgr->createWindow("TaharezLook/Combobox", node->getUniqueName());
    else if(node->getType() == MenuBar)
      win = mgr->createWindow("TaharezLook/Menubar", node->getUniqueName());
    else if(node->getType() == MenuItem)
      win = mgr->createWindow("TaharezLook/MenuItem", node->getUniqueName());
    else if(node->getType() == Memo)
      win = mgr->createWindow("TaharezLook/MultiLineEditbox", node->getUniqueName());
    else
    {
      mLog->log(12, "(CEGUI) setup: unknown node type: %d", node->getType());
      return false;
    }

    // Add to parent node
    if(parent)
      parent->addChildWindow(win);

    // Special operations for a form
    if(node->getType() == Window)
    {
      if(node->hasBorder())
      {
        CEGUI::FrameWindow * win_2 = static_cast<CEGUI::FrameWindow*>
                                      (mgr->createWindow("TaharezLook/FrameWindow", node->getUniqueName() + "/Form"));
        win->addChildWindow(win_2);
        win = win_2;
      }
    }
  }
  catch(CEGUI::Exception &e)
  {
    mLog->log(12, "(CEGUI) exception: %s", e.getMessage().c_str());
    return false;
  }

  // Set the caption
  win->setText(node->getCaption());

  // setup all events for the window
  for(Klaus::GUI::UIEvents::const_iterator it = node->eventsStart(); it != node->eventsEnd(); ++it)
  {
    // only MouseClick events are supported now
    if(it->first == Klaus::GUI::OnMouseClick)
      win->subscribeEvent(CEGUI::MenuItem::EventClicked, CEGUI::Event::Subscriber(&GUIPlugin_CEGUI::handleCEGUIEvent, this));
  }

  if(node->getType() == MenuBar)
  {
    win->setXPosition(CEGUI::UDim(0, 0));
    win->setYPosition(getTitleBarHeight(win));
    win->setWidth    (CEGUI::UDim(1.0f, 0.0f));
    win->setHeight   (CEGUI::UDim(0.0f, float(CEGUI::System::getSingletonPtr()->getDefaultFont()->getLineSpacing() * 1.5)));
  }
  else if((node->getType() == MenuItem))   // and special operations for menus
  {
    if(node->getChildCount() != 0)
    {
      CEGUI::Window* win_2 = mgr->createWindow("TaharezLook/PopupMenu", node->getName() + "/PopUpMenu");
      win_2->setProperty("Visible", "False");
      win_2->setProperty("ClippedByParent", "False");
      win_2->setProperty("AutoResizeEnabled", "True");
      win_2->setProperty("ItemSpacing", "2");
      win_2->setProperty("UnifiedAreaRect", "{{0,0},{0,40},{0,128},{0,215.572}}");
      win->addChildWindow(win_2);
      win = win_2;
    }
  }
  else
  {
    // Set modal state
    win->setModalState(node->isModal());

    // Setting the size
    Klaus::Rect r = node->getExtents();
    win->setXPosition(CEGUI::UDim(0.0f, float(r.getPosLeftTop()[0])));
    win->setYPosition(CEGUI::UDim(0.0f, float(r.getPosLeftTop()[1])));
    win->setWidth(CEGUI::UDim(0.0f, float(r.getLength())));
    win->setHeight(CEGUI::UDim(0.0f, float(r.getWidth())));

    // Setting the alignment
    CEGUI::HorizontalAlignment ha = CEGUI::HA_LEFT;
    if(node->getHAlignment() == Klaus::GUI::HAlignCenter)
      ha = CEGUI::HA_CENTRE;
    else if(node->getHAlignment() == Klaus::GUI::HAlignRight)
      ha = CEGUI::HA_RIGHT;
    win->setHorizontalAlignment(ha);

    CEGUI::VerticalAlignment va = CEGUI::VA_TOP;
    if(node->getVAlignment() == Klaus::GUI::VAlignCenter)
      va = CEGUI::VA_CENTRE;
    else if(node->getVAlignment() == Klaus::GUI::VAlignBottom)
      va = CEGUI::VA_BOTTOM;
    win->setVerticalAlignment(va);
  }

  // Now setup children
  for(unsigned i = 0; i < node->getChildCount(); i++)
    if(!setupUI(node->getChild(i), win))
      return false;

  mUIElements[win] = node;

  return true;
}

/**
  This method sets up all the given node tree and initializes it.
  @param root The root node of all UI nodes
  @return True if all the nodes were setup successfully, false otherwise
*/
bool GUIPlugin_CEGUI::setupUI(Klaus::GUI::Element * root)
{
  return setupUI(root, NULL);
}

/**
  This method selects a form as the active current form
  @param strForm Name of the form to activate
  @return True if requested form activated successfully, false otherwise
*/
bool GUIPlugin_CEGUI::setActiveForm(const std::string & strForm)
{
  if(!mSystem)
    return false;

  CEGUI::WindowManager *mgr = CEGUI::WindowManager::getSingletonPtr();

  try
  {
    CEGUI::Window * win = mgr->getWindow(strForm);

    if(!mSystem)
      return false;
    else if(win == NULL)
      return false;

    mSystem->setGUISheet(win);
    return true;
  }
  catch(CEGUI::UnknownObjectException e)
  {
    return false;
  }
}

/**
  Handler for all events in the CEGUI framework
  @param event The event information
  @return True to continue, false otherwise
*/
bool GUIPlugin_CEGUI::handleCEGUIEvent(const CEGUI::EventArgs& event)
{
  const CEGUI::WindowEventArgs* evt = static_cast<const CEGUI::WindowEventArgs*>(&event);
  GUIWindowMapping::iterator it = mUIElements.find(evt->window);
  if(it == mUIElements.end())
    return false;

  // only MouseClick events are supported now
  Events::Event ev(it->second->getEvent(GUI::OnMouseClick).c_str());
  ev.broadcast();
  return true;
}

/**
  This method initializes CEGUI system
  @return true on successfull initialization, false otherwise
*/
bool GUIPlugin_CEGUI::initialize()
{
  mLogCapsule = new CEGUILog;

  // Setup the default scheme: 'TaharezLook'
  try
  {
    #ifdef CEGUI_IS_OLD
      Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
      Ogre::RenderWindow * win = Graphics::GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow();
      mRenderer = new RendererClass(win, Ogre::RENDER_QUEUE_OVERLAY, false, 3000, mgr);
      mSystem   = new CEGUI::System(mRenderer);
      CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLookSkin.scheme");
      mSystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
      CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());
      mSystem->setDefaultFont((CEGUI::utf8*)"BlueHighway-10");
    #else
      Ogre::RenderTarget & target = *(Graphics::GraphicConfig::getSingleton()->getActiveRenderer()->getRenderWindow());
      mRenderer = &CEGUI::OgreRenderer::bootstrapSystem(target);
      mSystem = CEGUI::System::getSingletonPtr();
      CEGUI::Imageset::setDefaultResourceGroup("ImageSets");
      CEGUI::Font::setDefaultResourceGroup("Fonts");
      CEGUI::Scheme::setDefaultResourceGroup("Schemes");
      CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeels");
      CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
      CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");

      mSystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
      CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());
      mSystem->setDefaultFont((CEGUI::utf8*)"DejaVuSans-10");
    #endif

    registerEvent("MainWindow\\Resized");

    loadLayout("/home/Klaus/ui.conf");
    setActiveForm("MainWindow");
  }
  catch(CEGUI::Exception &e1)
  {
    mLog->log(12, "(CEGUI) exception: %s", e1.getMessage().c_str());
    mLog->flush();
    return false;
  }
  catch(Ogre::Exception &e2)
  {
    mLog->log(12, "(CEGUI) exception: %s", e2.getFullDescription().c_str());
    return false;
  }

  mLog->log(12, "(CEGUI) intialization finished");
  return true;
}

/**
  This method searches all the GUI elements and returns the one on which the mouse cursor is
  @return The node containing the mouse cursor
*/
Klaus::GUI::Element* GUIPlugin_CEGUI::getNodeContainingMouse()
{
  CEGUI::Window *win = CEGUI::System::getSingleton().getWindowContainingMouse();
  if(!win)
    return NULL;

  GUIWindowMapping::iterator it = mUIElements.find(win);
  return it == mUIElements.end() ? NULL : it->second;
}

/**
  This method returns the active node, ie. the node which has input focus
  @return Active node on screen
*/
Klaus::GUI::Element * GUIPlugin_CEGUI::getActiveNode()
{
  CEGUI::Window *win;
  win = CEGUI::System::getSingletonPtr()->getGUISheet();
  if(!win)
    return NULL;

  win = win->getActiveChild();

  GUIWindowMapping::iterator it = mUIElements.find(win);
  return it == mUIElements.end() ? NULL : it->second;
}

/**
  This method toggles visibility of the mouse cursor
  @param visible Whether to show or hide the cursor
*/
void GUIPlugin_CEGUI::setCursorVisible(bool visible)
{
  if(!visible)
    CEGUI::MouseCursor::getSingleton().hide();
  else
    CEGUI::MouseCursor::getSingleton().show();
}

/**
  This method injects mouse position into UI system
  @param x X Coordinate of cursor
  @param y Y Coordinate of cursor
*/
void GUIPlugin_CEGUI::injectMousePosition(const int & x, const int & y)
{
  CEGUI::System::getSingleton().injectMousePosition((float)x, (float)y);
}

/**
  This method injects a key down event into CEGUI core
  @param key Key that was pushed down
*/
void GUIPlugin_CEGUI::injectKeyDown(const KeyCodeT & key)
{
  if(key >= KC_MOUSE_LEFT && key < KC_MOUSE_MOVE)
  {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(key));

    Klaus::GUI::Element *n = getActiveNode();
    Klaus::GUI::Element *m = getNodeContainingMouse();
    if(n && (n->getType() == Klaus::GUI::TextBox || n->getType() == Klaus::GUI::Memo) && (n != m))
      CEGUI::System::getSingletonPtr()->getGUISheet()->getActiveChild()->deactivate();
  }
  else
  {
    CEGUI::System *sys = CEGUI::System::getSingletonPtr();
    sys->injectKeyDown(key);
    sys->injectChar(Klaus::Input::SingleKey::getSingleton()->toString(key)[0]);
  }
}

/**
  This method injects a key up event into CEGUI core
  @param key Key that was released
*/
void GUIPlugin_CEGUI::injectKeyUp(const Klaus::Input::KeyCodeT & key)
{
  if(key >= KC_MOUSE_LEFT && key < KC_MOUSE_MOVE)
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(key));
  else
    CEGUI::System::getSingleton().injectKeyUp(key);
}

/**
  This method analyzes sent events to this system and processes them.
  @param event The event to process
*/
void GUIPlugin_CEGUI::processEvent(const Klaus::Events::Event& event)
{
  // this class only registers for resize event, so:
  mRenderer->setDisplaySize(CEGUI::Size(float(event.mArguments["Width"]), float(event.mArguments["Height"])));
  mSystem->signalRedraw();
}

