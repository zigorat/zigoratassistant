/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 11/11/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A sample GUI plugin using CEGUI as it's base                       *
*                                                                               *
*********************************************************************************
*/

/*! \file GUIPlugin_CEGUI/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       11/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A sample GUI plugin using CEGUI as it's base
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
11/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef CEGUI_INPUT_PLUGIN
#define CEGUI_INPUT_PLUGIN

#include <map>
#include <string>
using namespace std;

#include <GUISystem/GUIPlugin.h>         // needed for GUIPlugin
#include <libZigorat/Logger.h>           // needed for Logger
#include <EventSystem/EventSystem.h>     // needed for Events::EventSystem


#include <CEGUI.h>

#if (CEGUI_VERSION_MAJOR < 1) && (CEGUI_VERSION_MINOR < 7)
  #define CEGUI_IS_OLD
#endif

#ifdef CEGUI_IS_OLD
  #include <OgreCEGUIRenderer.h>
  typedef CEGUI::OgreCEGUIRenderer RendererClass;
#else
  #include <CEGUIOgreRenderer.h>
  typedef CEGUI::OgreRenderer RendererClass;
#endif

using namespace Klaus;
using namespace Klaus::Input;


/*****************************************************************************/
/******************************  Class CEGUILog  *****************************/
/*****************************************************************************/

/*! CEGUILog is a wrapper for CEGUI logs. Channels all cegui output logs
    to standard Klaus::Logger
*/
class CEGUILog : public CEGUI::Logger
{
  public:
                 CEGUILog      (                                );
    virtual    ~ CEGUILog      (                                );

    virtual void logEvent      ( const CEGUI::String& message,
                                 CEGUI::LoggingLevel level =
                                              CEGUI::Standard   );
    virtual void setLogFilename( const CEGUI::String& filename,
                                 bool append = false            );
};



/*****************************************************************************/
/***************************  Class GUIPlugin_CEGUI  *************************/
/*****************************************************************************/

/*! GUIPlugin_CEGUI is a GUI plugin for ZigoratAssistant which
    creates a CEGUI system and puts in application's disposal.
*/
class _DLLExport GUIPlugin_CEGUI : public Klaus::GUI::GUIPlugin,
                                   public Klaus::Events::EventRecipient
{
  private:
    /*! GUIWindowMapping is a mapping between Klaus::GUI::Node* and CEGUI::Window*.
        This will ease searching for the sharable representation of windows on form. */
    typedef std::map<CEGUI::Window*, Klaus::GUI::Element*> GUIWindowMapping;

    RendererClass         * mRenderer;            /*!< GUI Renderer instance     */
    CEGUI::System         * mSystem;              /*!< CEGUI system instance     */
    GUIWindowMapping        mUIElements;          /*!< All nodes and windows     */
    CEGUILog              * mLogCapsule;          /*!< CEGUI log capsule         */
    Logger                * mLog;                 /*!< Logger instance           */
    GUI::Element::Array     mUINodes;             /*!< UI raw nodes              */
    Events::EventSystem   * mEventSystem;         /*!< EventSystem instance      */

    CEGUI::UDim             getTitleBarHeight     ( CEGUI::Window * win          );
    CEGUI::MouseButton      convertButton         ( const KeyCodeT & btn         );
    bool                    loadLayout            ( const std::string & str      );
    void                    clearUI               (                              );
    bool                    setupUI               ( GUI::Element * node,
                                                    CEGUI::Window * parent       );
    virtual bool            setupUI               ( GUI::Element * root          );
    virtual bool            setActiveForm         ( const std::string & strForm  );

    bool                    handleCEGUIEvent      ( const CEGUI::EventArgs& event);

  public:
                            GUIPlugin_CEGUI       (                              );
                          ~ GUIPlugin_CEGUI       (                              );

    // Initialization
    virtual bool            initialize            (                              );

    // Utitlities
    virtual GUI::Element  * getNodeContainingMouse(                              );
    virtual GUI::Element  * getActiveNode         (                              );

    virtual void            setCursorVisible      ( bool visible                 );
    // Injection methods
    virtual void            injectMousePosition   ( const int & x, const int & y );
    virtual void            injectKeyDown         ( const KeyCodeT & key         );
    virtual void            injectKeyUp           ( const KeyCodeT & key         );

    // event related
    virtual void            processEvent          ( const Events::Event & event  );
};



/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_CEGUI is a factory to create instances of GUIPlugin_CEGUI class. */
class _DLLExport PluginFactory_CEGUI: public Klaus::PluginFactory
{
  public:
    /**
      Constructor. Sets information of factory.
    */
    PluginFactory_CEGUI() : Klaus::PluginFactory("CEGUI", "GUIPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
      Returns a new instance of the plugin
      @return Instance of the plugin
    */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new GUIPlugin_CEGUI;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
  This method creates a factory instance for the plugin in this library.
  @return An instance of the plugin
*/
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_CEGUI;
}

/**
  This method frees an instance of the plugin created by this library
  @param factory Factory instance to delete
*/
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}



#endif // CEGUI_INPUT_PLUGIN
