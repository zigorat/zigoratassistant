/*
*********************************************************************************
*            Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: First person camera implementation as a camera agent               *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FirstPerson/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      First person camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>
#include <CoreSystem/CoreSystem.h>
#include <InputSystem/InputSystem.h>


/**
  Constructor of the camera plugin for first person camera type. Registers events
  associated with this camera type.
*/
CameraPlugin_FirstPerson::CameraPlugin_FirstPerson()
{
  mNode = NULL;
  mCamera = NULL;

  EV_INIT = registerEvent("FirstPersonCamera\\Init");
  setRecipientName("FirstPersonCamera");
}

/**
  Destructor of this class. Does nothing.
*/
CameraPlugin_FirstPerson::~CameraPlugin_FirstPerson()
{

}

/**
  This method shall activate the camera.
  If the node to attach was specified, the this plugin attaches the camera
  to the specified node and sets the offset relative to the center of the node.
  @return true if plugin activated with no problem, false otherwise
*/
bool CameraPlugin_FirstPerson::activate()
{
  if(mCamera)
    return true;

  mLog->log(19, "(FirstPersonCamera) camera instance is null");
  return false;
}

/**
  This method deactivates the camera. It restores all parameters to their default values.
*/
void CameraPlugin_FirstPerson::deactivate()
{
  if(mCamera)
    resetCamera();
}

/**
  Updates the camera between every cycle. Nothing to do in this agent.
*/
void CameraPlugin_FirstPerson::update()
{

}

/**
  Processes events sent from core.
  @param ev Event sent from core
*/
void CameraPlugin_FirstPerson::processEvent(const Events::Event& ev)
{
  if(ev == EV_INIT)
  {
    string str = ev.mArguments.getAsString("Subject");
    Ogre::SceneManager * mgr = Graphics::GraphicSystem::getSingleton()->getSceneManager();
    if(!mgr->hasSceneNode(str))
    {
      mLog->log(19, "(FirstPersonCamera) node does not exist: %s", str.c_str());
      return;
    }

    initialize(mgr->getSceneNode(str),ev.mArguments.getAsPoint("Offset"));
  }
  else
    mLog->log(19, "(FirstPersonCamera) unknown command %d", ev.mEventID);
}

/**
  This method sets all the needed parameters to activate this camera agent.
  @param node The node to attach to
  @param offset Offset of the camera relative to the node center
*/
void CameraPlugin_FirstPerson::initialize(Ogre::SceneNode* node, const Klaus::VecPosition& offset)
{
  if(!mCamera)
  {
    mLog->log(19, "(FirstPersonCamera) camera instance is null");
    return;
  }

  resetCamera();

  mNode = node;
  mOffset = offset;
  mCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
  mCamera->setNearClipDistance(0.2f);
  mCamera->setPosition((float)mOffset[0], (float)mOffset[1], (float)mOffset[2]);
  mNode->attachObject(mCamera);
}
