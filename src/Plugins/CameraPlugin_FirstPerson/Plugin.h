/*
*********************************************************************************
*             Plugin.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 07/11/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: First person camera implementation as a camera agent               *
*                                                                               *
*********************************************************************************
*/

/*! \file CameraPlugin_FirstPerson/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       07/11/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      First person camera implementation as a camera agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
07/11/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CAMERA_PLUGIN_FIRST_PERSON
#define CAMERA_PLUGIN_FIRST_PERSON

#include <map>
#include <string>
using namespace std;

#include <DisplaySystem/CameraPlugin.h>  // needed for CameraPlugin
#include <GraphicSystem/GraphicSystem.h> // needed for SceneManager
#include <EventSystem/EventRecipient.h>  // needed for EventRecipient

using namespace Klaus;


/*******************************************************************************/
/***********************  Class CameraPlugin_FirstPerson  **********************/
/*******************************************************************************/

/*! First person camera is a type of camera which views the environment as if
    it's being attach to someone/something. It's typically used in first person
    shooter and recing games which the player shall play from within a car or
    head of a player.
*/
class _DLLExport CameraPlugin_FirstPerson: public Graphics::CameraPlugin, Events::EventRecipient
{
  private:
    // Event IDs
    EventID            EV_INIT;                /*!< This sets all properties */

    VecPosition        mOffset;                /*!< offset to parent node    */
    Ogre::SceneNode  * mNode;                  /*!< Node to track            */

  public:
                       CameraPlugin_FirstPerson(                             );
    virtual          ~ CameraPlugin_FirstPerson(                             );

    virtual bool       activate                (                             );
    virtual void       deactivate              (                             );
    virtual void       update                  (                             );
    virtual void       processEvent            ( const Events::Event& ev     );

    void               initialize              ( Ogre::SceneNode * node,
                                                 const VecPosition & offset  );
};



/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_FPC is a factory to create instances of CameraPlugin_FirstPerson class. */
class _DLLExport PluginFactory_FPC: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_FPC() : Klaus::PluginFactory("FirstPersonCamera", "CameraPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new CameraPlugin_FirstPerson;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_FPC;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}


#endif // CAMERA_PLUGIN_FIRST_PERSON
