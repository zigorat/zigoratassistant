/*
*********************************************************************************
*             Plugin.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Overlay plugin for default ogre overlay                            *
*                                                                               *
*********************************************************************************
*/

/*! \file OverlayPlugin_OgreStats/Plugin.cpp
<pre>
<b>File:</b>          Plugin.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Overlay plugin for default ogre overlay
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#include "Plugin.h"
#include <iostream>
#include <sstream>
using namespace std;

#include <Ogre.h>

using namespace Ogre;


/**
  Constructor of OverlayPlugin_OgreStats. Nullifies the overlay instance
*/
OverlayPlugin_OgreStats::OverlayPlugin_OgreStats()
{
}

/**
  Destructor of OverlayPlugin_OgreStats
*/
OverlayPlugin_OgreStats::~OverlayPlugin_OgreStats()
{
}

/**
  This method initializes the overlay. The overlay plugin shall add resources of
  the overlay to application and update it's instance of ogre overlay.
  @return True on successfull initialization, false otherwise
*/
bool OverlayPlugin_OgreStats::initialize()
{
  Ogre::Overlay * o = Ogre::OverlayManager::getSingleton().getByName("Core/DebugOverlay");
  if(!o)
    return false;

  mOverlays.push_back(o);
  mLogger = Logger::getSingleton();
  return true;
}

/**
  This method finalizes the overlay. The overlay plugin shall remove resources of
  the overlay from ogre and remove the ogre overlay instance from scene manager.
*/
void OverlayPlugin_OgreStats::finalize()
{
  // since the overlay was not 'created', there is no need to destroy it.
  mOverlays.clear();
}


/**
  This method is used to update the overlay before each frame render starts
*/
void OverlayPlugin_OgreStats::update()
{
  static String currFps = "Current FPS: ";
  static String avgFps = "Average FPS: ";
  static String bestFps = "Best FPS: ";
  static String worstFps = "Worst FPS: ";
  static String tris = "Triangle Count: ";
  static String batches = "Batch Count: ";

  // update stats when necessary
  try
  {
    OverlayElement* guiAvg = OverlayManager::getSingleton().getOverlayElement("Core/AverageFps");
    OverlayElement* guiCurr = OverlayManager::getSingleton().getOverlayElement("Core/CurrFps");
    OverlayElement* guiBest = OverlayManager::getSingleton().getOverlayElement("Core/BestFps");
    OverlayElement* guiWorst = OverlayManager::getSingleton().getOverlayElement("Core/WorstFps");

    Ogre::Root * root = Ogre::Root::getSingletonPtr();
    const RenderTarget::FrameStats& stats = root->getRenderTarget("ZigoratAssistant")->getStatistics();
    guiAvg->setCaption(avgFps + StringConverter::toString(stats.avgFPS));
    guiCurr->setCaption(currFps + StringConverter::toString(stats.lastFPS));
    guiBest->setCaption(bestFps + StringConverter::toString(stats.bestFPS)
                          + " " + StringConverter::toString(stats.bestFrameTime)+" ms");
    guiWorst->setCaption(worstFps + StringConverter::toString(stats.worstFPS)
                          + " " + StringConverter::toString(stats.worstFrameTime)+" ms");

    OverlayElement* guiTris = OverlayManager::getSingleton().getOverlayElement("Core/NumTris");
    guiTris->setCaption(tris + StringConverter::toString(stats.triangleCount));

    OverlayElement* guiBatches = OverlayManager::getSingleton().getOverlayElement("Core/NumBatches");
    guiBatches->setCaption(batches + StringConverter::toString(stats.batchCount));

    OverlayElement* guiDbg = OverlayManager::getSingleton().getOverlayElement("Core/DebugText");

    // add logs
    using namespace Klaus;
    const LogCache::Cache & logs = mLogger->getCache();
    int count = 0;
    string strText;
    for(LogCache::Cache::const_reverse_iterator it = logs.rbegin(); it != logs.rend() && count < 6; it++)
    {
      if(it->mLevel == 1 || it->mLevel == 9)
        continue;

      strText.insert(0, it->mLog + "\n");
      count++;
    }

    guiDbg->setCaption(strText);
  }
  catch(...)
  {
    /* ignore */
  }
}

/**
  This method toggles visibility of the overlay
  @param bVisible Whether to show the overlay or hide it
*/
void OverlayPlugin_OgreStats::setVisibility(bool bVisible)
{
  if((bVisible) && (!mOverlays[0]->isVisible()))
    mOverlays[0]->show();
  else if((!bVisible) && (mOverlays[0]->isVisible()))
    mOverlays[0]->hide();
}

