/*
*********************************************************************************
*              Plugin.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 11/01/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Overlay plugin for default ogre overlay                            *
*                                                                               *
*********************************************************************************
*/

/*! \file OverlayPlugin_OgreStats/Plugin.h
<pre>
<b>File:</b>          Plugin.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/11/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Overlay plugin for default ogre overlay
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/11/2009       Mahdi           Initial version created
</pre>
*/

#ifndef OIS_INPUT_PLUGIN
#define OIS_INPUT_PLUGIN

#include <OverlaySystem/OverlayPlugin.h>
#include <libZigorat/Logger.h>    // needed for Logger

using namespace Klaus;

/***********************************************************************/
/********************  Class OverlayPlugin_OgreStats  ******************/
/***********************************************************************/

/**
  This plugin is used to create a simple overlay which shows ogre logo, rendering
  information and the zigorat logger's cache.
*/
class _DLLExport OverlayPlugin_OgreStats : public Overlay::OverlayPlugin
{
  private:
    Logger       * mLogger;               /*!< Logger instance */

  public:
                   OverlayPlugin_OgreStats(                    );
    virtual      ~ OverlayPlugin_OgreStats(                    );

    virtual bool   initialize             (                    );
    virtual void   finalize               (                    );
    virtual void   update                 (                    );
    virtual void   setVisibility          ( bool bVisible      );
};




/**************************************************************/
/*********************  Plugin's Factory  *********************/
/**************************************************************/

/*! PluginFactory_OgreStats is a factory to create instances of OverlayPlugin_OgreStats class. */
class _DLLExport PluginFactory_OgreStats: public Klaus::PluginFactory
{
  public:
    /**
     * Constructor. Sets information of factory.
     */
    PluginFactory_OgreStats() : Klaus::PluginFactory("OgreStats", "OverlayPlugin", "Zigorat Team", 0, 5)
    {
    }

    /**
     * Returns a new instance of the plugin
     * @return Instance of the plugin
     */
    virtual Klaus::Plugin * createPluginInstance()
    {
      return new OverlayPlugin_OgreStats;
    }
};



/*************************************************************/
/***************  Plugin mandatory functions *****************/
/*************************************************************/

/**
 * This method creates a factory instance for the plugin in this library.
 * @return An instance of the plugin
 */
_DLLExport Klaus::PluginFactory * getPluginFactory()
{
  return new PluginFactory_OgreStats;
}

/**
 * This method frees an instance of the plugin created by this library
 * @param factory Factory instance to delete
 */
_DLLExport void freePluginFactory(Klaus::PluginFactory * factory)
{
  if(factory)
    delete factory;
}



#endif // OIS_INPUT_PLUGIN
