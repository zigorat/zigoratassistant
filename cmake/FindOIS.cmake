# Tries to find OIS (Object Oriented Input System)
# http://sourceforge.net/projects/wgois/
#
# Once run this will define:
#
# OIS_FOUND
# OIS_INCLUDE_DIR
# OIS_LIBRARY
# OIS_LIBRARY_DIR
#
# Mahdi Hamdarsi <hamdarsi@gmail.com>
# 10/2009
# ======================================================================


# set search hint directories
set(
     OIS_POSSIBLE_ROOT_PATHS
     $ENV{OIS_ROOT}
     $ENV{OGRE_ROOT}
     /usr/local
     /usr
   )


# find OIS include directory
# ======================================================================

find_path(
  OIS_INCLUDE_DIR
  NAME          OIS/OIS.h
  HINTS         ${OIS_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include" "OIS/include" "include/OIS" "OIS"
)

if(NOT OIS_INCLUDE_DIR)
  message(STATUS "Checking for OIS... no")
  message(FATAL_ERROR "Could not find include path for OIS, try setting OIS_ROOT")
endif()


# find OIS libraries
# ======================================================================

# library for debug builds
find_library(
  OIS_LIBRARY_DEBUG
  NAMES          OIS_d
  HINTS          ${OIS_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64" "lib/debug"
  DOC            "OIS library for debug builds"
)

# library for release builds
find_library(
  OIS_LIBRARY_RELEASE
  NAMES          OIS
  HINTS          ${OIS_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64" "lib/release"
  DOC            "OIS library for release builds"
)

# create library name for linking
set(OIS_LIBRARY "")
if(OIS_LIBRARY_DEBUG AND OIS_LIBRARY_RELEASE)
  set(OIS_LIBRARY "optimized;${OIS_LIBRARY_RELEASE};debug;${OIS_LIBRARY_DEBUG}")
elseif(OIS_LIBRARY_DEBUG)
  set(OIS_LIBRARY "${OIS_LIBRARY_DEBUG}")
elseif(OIS_LIBRARY_RELEASE)
  set(OIS_LIBRARY "${OIS_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT OIS_LIBRARY)
  message(STATUS "Checking for OIS... no")
  message(STATUS "OIS include directory: ${OIS_INCLUDE_DIR}")
  message(FATAL_ERROR "Could not find OIS library")
endif()

# extract the library directory
set(OIS_LIBRARY_DIR_DEBUG   "")
set(OIS_LIBRARY_DIR_RELEASE "")
if(OIS_LIBRARY_DEBUG)
  get_filename_component(OIS_LIBRARY_DIR_DEBUG ${OIS_LIBRARY_DEBUG} PATH)
endif()
if(OIS_LIBRARY_RELEASE)
  get_filename_component(OIS_LIBRARY_DIR_RELEASE ${OIS_LIBRARY_RELEASE} PATH)
endif()
set(OIS_LIBRARY_DIR ${OIS_LIBRARY_DIR_DEBUG} ${OIS_LIBRARY_DIR_RELEASE})


# everything is found. just finish up
# ======================================================================

set(OIS_FOUND TRUE)
message(STATUS "Checking for OIS... yes")

set(OIS_INCLUDE_DIR ${OIS_INCLUDE_DIR} CACHE PATH "OIS include directory")
set(OIS_LIBRARY ${OIS_LIBRARY} CACHE FILEPATH "OIS library for linking against")
set(OIS_LIBRARY_DIR ${OIS_LIBRARY_DIR} CACHE PATH "OIS library directory")

mark_as_advanced(
                  OIS_INCLUDE_DIR
                  OIS_LIBRARY
                  OIS_LIBRARY_DIR
                )
