# Tries to find CEGUI (Crazy Eddie's GUI)
# http://cegui.org.uk/
#
# Once run this will define:
#
# CEGUI_INCLUDE_DIR
# CEGUI_LIBRARY
# CEGUI_LIBRARY_DIR
# CEGUI_RENDERER_INCLUDE_DIR
# CEGUI_RENDERER_LIBRARY
# CEGUI_RENDERER_LIBRARY_DIR
#
# Mahdi Hamdarsi <hamdarsi@gmail.com>
# 10/2009
# ======================================================================


# set search hint directories
set(
     CEGUI_POSSIBLE_ROOT_PATHS
     $ENV{CEGUI_ROOT}
     /usr/local
     /usr
     $ENV{OGRE_ROOT} # Also search ogre sdk folder in windows
   )



# find CEGUI include directory
# ======================================================================

find_path(
  CEGUI_INCLUDE_DIR
  NAMES CEGUI.h CEGUI/CEGUI.h CEGUI.h
  HINTS         ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include" "CEGUI/include" "CEGUI" "include/CEGUI"
)

if(NOT CEGUI_INCLUDE_DIR)
  message(STATUS "Checking for CEGUI... no")
  message(FATAL_ERROR "Could not find include path for CEGUI, try setting CEGUI_ROOT")
endif()


# find CEGUI libraries
# ======================================================================

# library for debug builds
find_library(
  CEGUI_LIBRARY_DEBUG
  NAMES          CEGUIBase_d
  HINTS          ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64"
)

# library for release builds
find_library(
  CEGUI_LIBRARY_RELEASE
  NAMES          CEGUIBase
  HINTS          ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64"
)

# create library name for linking
set(CEGUI_LIBRARY "")
if(CEGUI_LIBRARY_DEBUG AND CEGUI_LIBRARY_RELEASE)
  set(CEGUI_LIBRARY "optimized;${CEGUI_LIBRARY_RELEASE};debug;${CEGUI_LIBRARY_DEBUG}")
elseif(CEGUI_LIBRARY_DEBUG)
  set(CEGUI_LIBRARY "${CEGUI_LIBRARY_DEBUG}")
elseif(CEGUI_LIBRARY_RELEASE)
  set(CEGUI_LIBRARY "${CEGUI_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT CEGUI_LIBRARY)
  message(STATUS "Checking for CEGUI... no")
  message(STATUS "CEGUI include directory: ${CEGUI_INCLUDE_DIR}")
  message(FATAL_ERROR "Could not find CEGUI library")
endif()

# extract the library directory
set(CEGUI_LIBRARY_DIR_DEBUG   "")
set(CEGUI_LIBRARY_DIR_RELEASE "")
set(CEGUI_LIBRARY_DIR "")
if(CEGUI_LIBRARY_DEBUG)
  get_filename_component(CEGUI_LIBRARY_DIR_DEBUG ${CEGUI_LIBRARY_DEBUG} PATH)
endif()
if(CEGUI_LIBRARY_RELEASE)
  get_filename_component(CEGUI_LIBRARY_DIR_RELEASE ${CEGUI_LIBRARY_RELEASE} PATH)
endif()
set(CEGUI_LIBRARY_DIR ${CEGUI_LIBRARY_DIR_DEBUG} ${CEGUI_LIBRARY_DIR_RELEASE})


# find CEGUI's ogre renderer
# ======================================================================

# list of possible CEGUI ogre renderer path suffixes
set(
     POSSIBLE_RENDERER_SUFFIXES
     include
     CEGUI/include
     CEGUI
     CEGUI/RendererModules/Ogre
     CEGUI/include/RendererModules/Ogre
     include/CEGUI/RendererModules/Ogre
     include/CEGUI
     include/OGRE
     OGRE/include
     OGRE
     samples/include
     samples
   )

# find the include dir
find_path(
  CEGUI_RENDERER_INCLUDE_DIR
  NAMES         CEGUIOgreRenderer.h OgreCEGUIRenderer.h
  HINTS         ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES ${POSSIBLE_RENDERER_SUFFIXES}
)

# check it
if(NOT CEGUI_RENDERER_INCLUDE_DIR)
  message(STATUS "Checking for CEGUI... no")
  message(STATUS "CEGUI include directory: ${CEGUI_INCLUDE_DIR}")
  message(STATUS "CEGUI library directory: ${CEGUI_LIBRARY_DIR}")
  message(FATAL_ERROR "Could not find CEGUI ogre renderer include directory")
endif()

# renderer library for debug builds
find_library(
  CEGUI_RENDERER_LIBRARY_DEBUG
  NAMES         CEGUIOgreRenderer_d OgreGUIRenderer_d
  HINTS         ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "lib" "lib64"
)

# renderer library for release builds
find_library(
  CEGUI_RENDERER_LIBRARY_RELEASE
  NAMES         CEGUIOgreRenderer OgreGUIRenderer
  HINTS         ${CEGUI_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "lib" "lib64"
)

# create library name for linking
set(CEGUI_RENDERER_LIBRARY "")
if(CEGUI_RENDERER_LIBRARY_DEBUG AND CEGUI_RENDERER_LIBRARY_RELEASE)
  set(CEGUI_RENDERER_LIBRARY "optimized;${CEGUI_RENDERER_LIBRARY_RELEASE};debug;${CEGUI_RENDERER_LIBRARY_DEBUG}")
elseif(CEGUI_RENDERER_LIBRARY_DEBUG)
  set(CEGUI_RENDERER_LIBRARY "${CEGUI_RENDERER_LIBRARY_DEBUG}")
elseif(CEGUI_RENDERER_LIBRARY_RELEASE)
  set(CEGUI_RENDERER_LIBRARY "${CEGUI_RENDERER_LIBRARY_RELEASE}")
endif()

# check it
if(NOT CEGUI_RENDERER_LIBRARY)
  message(STATUS "Checking for CEGUI... no")
  message(STATUS "CEGUI include directory: ${CEGUI_INCLUDE_DIR}")
  message(STATUS "CEGUI library directory: ${CEGUI_LIBRARY_DIR}")
  message(STATUS "CEGUI renderer include directory: ${CEGUI_RENDERER_INCLUDE_DIR}")
  message(FATAL_ERROR "Could not find CEGUI ogre renderer library")
endif()

# extract the renderer library directory
set(CEGUI_RENDERER_LIBRARY_DIR_DEBUG   "")
set(CEGUI_RENDERER_LIBRARY_DIR_RELEASE "")
if(CEGUI_RENDERER_LIBRARY_DEBUG)
  get_filename_component(CEGUI_RENDERER_LIBRARY_DIR_DEBUG ${CEGUI_RENDERER_LIBRARY_DEBUG} PATH)
endif()
if(CEGUI_RENDERER_LIBRARY_RELEASE)
  get_filename_component(CEGUI_RENDERER_LIBRARY_DIR_RELEASE ${CEGUI_RENDERER_LIBRARY_RELEASE} PATH)
endif()
set(CEGUI_RENDERER_LIBRARY_DIR ${CEGUI_RENDERER_LIBRARY_DIR_DEBUG} ${CEGUI_RENDERER_LIBRARY_DIR_RELEASE})


# everything is found. just finish up
# ======================================================================

set(CEGUI_FOUND TRUE)
message(STATUS "Checking for CEGUI... yes")

set(CEGUI_INCLUDE_DIR ${CEGUI_INCLUDE_DIR} CACHE PATH "CEGUI include directory")
set(CEGUI_LIBRARY ${CEGUI_LIBRARY} CACHE FILEPATH "CEGUI library for linking against")
set(CEGUI_LIBRARY_DIR ${CEGUI_LIBRARY_DIR} CACHE PATH "CEGUI library directory")
set(CEGUI_RENDERER_INCLUDE_DIR ${CEGUI_RENDERER_INCLUDE_DIR} CACHE PATH "CEGUI ogre renderer include directory")
set(CEGUI_RENDERER_LIBRARY ${CEGUI_RENDERER_LIBRARY} CACHE FILEPATH "CEGUI ogre renderer library for linking against")
set(CEGUI_RENDERER_LIBRARY_DIR ${CEGUI_RENDERER_LIBRARY} CACHE PATH "CEGUI ogre renderer library directory")

mark_as_advanced(
                  CEGUI_INCLUDE_DIR
                  CEGUI_LIBRARY
                  CEGUI_LIBRARY_DIR
                  CEGUI_RENDERER_INCLUDE_DIR
                  CEGUI_RENDERER_LIBRARY
                  CEGUI_RENDERER_LIBRARY_DIR
                )
