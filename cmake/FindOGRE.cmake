# Find Module for Ogre
# Tries to find OGRE (Object Oriented Graphic Rendering engine)
# http://www.ogre3d.org/
#
# Once run this will define:
#
# OGRE_FOUND
# OGRE_INCLUDE_DIR
# OGRE_LIBRARY
# OGRE_LIBRARY_DIR
# OGRE_PLUGINS_PATH (RenderSystem_* and other ogre plugins)
#
# Mahdi Hamdarsi <hamdarsi@gmail.com>
# 10/2009
# ======================================================================


# set search hint directories
set(
     OGRE_POSSIBLE_ROOT_PATHS
     "$ENV{OGRE_ROOT}"
     /usr/local
     /usr
   )


# find OGRE include directory
# ======================================================================

find_path(
  OGRE_INCLUDE_DIR
  NAME          Ogre.h
  PATHS         ${OGRE_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include" "include/OGRE" "OGRE/include" "OGRE"
)

if(NOT OGRE_INCLUDE_DIR)
  message(STATUS "Checking for Ogre... no")
  message(FATAL_ERROR "Could not find include path for ogre, try setting OGRE_ROOT")
endif()



# find OGRE libraries
# ======================================================================

# library for debug builds
find_library(
  OGRE_LIBRARY_DEBUG
  NAMES          OgreMain_d
  HINTS          ${OGRE_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64" "lib/OGRE" "lib64/OGRE" "lib/debug"
  DOC            "Ogre library for debug builds"
)

# library for release builds
find_library(
  OGRE_LIBRARY_RELEASE
  NAMES         OgreMain
  HINTS         ${OGRE_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "lib" "lib64" "lib/OGRE" "lib64/OGRE" "lib/release"
  DOC           "Ogre library for release builds"
)

# create library name for linking
set(OGRE_LIBRARY "")
if(OGRE_LIBRARY_DEBUG AND OGRE_LIBRARY_RELEASE)
  set(OGRE_LIBRARY "optimized;${OGRE_LIBRARY_RELEASE};debug;${OGRE_LIBRARY_DEBUG}")
elseif(OGRE_LIBRARY_DEBUG)
  set(OGRE_LIBRARY "${OGRE_LIBRARY_DEBUG}")
elseif(OGRE_LIBRARY_RELEASE)
  set(OGRE_LIBRARY "${OGRE_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT OGRE_LIBRARY)
  message(STATUS "Checking for Ogre... no")
  message(STATUS "Ogre include directory: ${OGRE_INCLUDE_DIR}")
  message(FATAL_ERROR "Could not find ogre library")
endif()

# extract the library directory
set(OGRE_LIBRARY_DIR_DEBUG   "")
set(OGRE_LIBRARY_DIR_RELEASE "")
if(OGRE_LIBRARY_DEBUG)
  get_filename_component(OGRE_LIBRARY_DIR_DEBUG ${OGRE_LIBRARY_DEBUG} PATH)
endif()
if(OGRE_LIBRARY_RELEASE)
  get_filename_component(OGRE_LIBRARY_DIR_RELEASE ${OGRE_LIBRARY_RELEASE} PATH)
endif()
set(OGRE_LIBRARY_DIR ${OGRE_LIBRARY_DIR_DEBUG} ${OGRE_LIBRARY_DIR_RELEASE})


# find OGRE's necessary plugins' path (RenderSystems)
# ======================================================================

find_path(
  OGRE_PLUGINS_PATH_DEBUG
  NAMES         RenderSystem_GL_d.so RenderSystem_GL_d.dll
  HINTS         ${OGRE_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "lib" "lib64" "lib/OGRE" "lib64/OGRE" "bin/release" "bin/debug"
  DOC           "Path for necessary ogre rendering plugins for debug builds"
)

find_path(
  OGRE_PLUGINS_PATH_RELEASE
  NAMES         RenderSystem_GL.so RenderSystem_GL.dll
  HINTS         ${OGRE_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "lib" "lib64" "lib/OGRE" "lib64/OGRE" "bin/release" "bin/debug"
  DOC           "Path for necessary ogre rendering plugins for release builds"
)

if(NOT OGRE_PLUGINS_PATH_DEBUG AND NOT OGRE_PLUGINS_PATH_RELEASE)
  message(STATUS "Checking for Ogre... no")
  message(STATUS "Ogre include directory: ${OGRE_INCLUDE_DIR}")
  message(STATUS "Ogre library directory: ${OGRE_LIBRARY_DIR}")
  message(FATAL_ERROR "Could not find required ogre plugins")
endif()

if(UNIX)
  if(NOT OGRE_PLUGINS_PATH_DEBUG)
    set(OGRE_PLUGINS_PATH_DEBUG ${OGRE_PLUGINS_PATH_RELEASE})
  endif()
endif()


# everything is found. just finish up
# ======================================================================

set(OGRE_FOUND TRUE)
message(STATUS "Checking for Ogre... yes")

set(OGRE_INCLUDE_DIR ${OGRE_INCLUDE_DIR} CACHE PATH "Ogre include directory")
set(OGRE_LIBRARY ${OGRE_LIBRARY} CACHE FILEPATH "Ogre library for linking against")
set(OGRE_LIBRARY_DIR ${OGRE_LIBRARY_DIR} CACHE PATH "Ogre library directory")
set(OGRE_PLUGINS_PATH_DEBUG ${OGRE_PLUGINS_PATH_DEBUG} CACHE PATH "Debuggable ogre plugins directory")
set(OGRE_PLUGINS_PATH_RELEASE ${OGRE_PLUGINS_PATH_RELEASE} CACHE PATH "Non-Debugable ogre plugins directory")

mark_as_advanced(
                  OGRE_INCLUDE_DIR
                  OGRE_LIBRARY
                  OGRE_LIBRARY_DIR
                  OGRE_PLUGINS_PATH_DEBUG
                  OGRE_PLUGINS_PATH_RELEASE
                )

