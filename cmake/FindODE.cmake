# Tries to find ODE (Open Dynamics Engine)
# http://opende.sourceforge.net/
#
# Once run this will define:
#
# ODE_FOUND
# ODE_INCLUDE_DIR
# ODE_LIBRARIES
#
# Mahdi Hamdarsi 10/2009
#
# hamdarsi@gmail.com
# --------------------------------

# base dirs:
set(ODE_POSSIBLE_ROOT_PATHS
      ${ODE_ROOT_DIR}
      $ENV{ODE_ROOT_DIR}
      ${ODE_DIR}
      $ENV{ODE_DIR}
      ${ODE_HOME}
      $ENV{ODE_HOME}
      $ENV{ProgramFiles}/ode
      /usr/local
      /usr
      C:/library/ode
      C:/ode
      "C:/Program Files/ode"
      "C:/Program Files (x86)/ode"
   )

# These will be appended
set(ODE_POSSIBLE_INCDIR_SUFFIXES
      include
      ODE/include
   )

set(ODE_POSSIBLE_LIBDIR_SUFFIXES
      lib
      lib64
   )

# Now find the required information about ODE
find_path(ODE_INC_DIR
            NAMES ode/ode.h
            PATHS         ${ODE_POSSIBLE_ROOT_PATHS}
            PATH_SUFFIXES ${ODE_POSSIBLE_INCDIR_SUFFIXES}
         )

find_library(ODE_LIBS
               NAMES ode ode_double ode_single ode_doubled ode_singled
               PATHS         ${ODE_POSSIBLE_ROOT_PATHS}
               PATH_SUFFIXES ${ODE_POSSIBLE_LIBDIR_SUFFIXES}
            )

# --------------------------------

if(ODE_INC_DIR)
  if(ODE_LIBS)
    # Extract library directory
    get_filename_component(ODE_LIB_DIR ${ODE_LIBS} PATH)

    # Set ODE information
    set(ODE_FOUND TRUE)
    message(STATUS "Checking for ODE... yes")
  endif(ODE_LIBS)
endif(ODE_INC_DIR)

mark_as_advanced(
                  ODE_INC_DIR
                  ODE_LIBS
                  ODE_LIB_DIR
                )

# ==========================================
IF(NOT ODE_FOUND)
  message(STATUS "Checking for ODE... no")
  message(FATAL_ERROR "Could not find ODE - You should install ODE before compiling this application")
endif(NOT ODE_FOUND)

# backward compatibility
set(ODE_FOUND   ${ODE_FOUND})
set(ODE_LIBS    ${ODE_LIBS})
set(ODE_LIB_DIR ${ODE_LIB_DIR})
set(ODE_INC_DIR ${ODE_INCLUDE_DIR})
